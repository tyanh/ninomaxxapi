﻿using Newtonsoft.Json;
using System.Net;

namespace ninoInven.Models
{
    public class ApiCall
    {
        Appsetting app = new Appsetting();
        public dynamic LoginVFCApi(dynamic data)
        {
            string urlpost = app.urlvfc + "nguoi-dung/dang-nhap";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return JsonConvert.DeserializeObject<dynamic>(str);
        }
        public string GetData(string token, string url)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization", "Bearer " + token);
            string uurl = app.urlvfc + url;
            var obj = webClient.DownloadString(uurl);
           
            return obj.ToString();

        }
        public string PostDataVFCApi(string url, dynamic data, string token)
        {
            string urlpost = app.urlvfc + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            string toe = "Bearer " + token;
            myHttpWebRequest_.Headers.Add("Authorization", "Bearer " + token);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
    }
}
