﻿using System.Drawing;

namespace ninoInven.Models
{
    public class common
    {
        public static Image GetImgBitmap(string str)
        {
            string convert = str.Replace("data:image/png;base64,", String.Empty);
            convert = convert.Replace("data:image/jpeg;base64,", String.Empty);
            convert = convert.Replace(" ", "+");
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
            {
                using (Bitmap bm2 = new Bitmap(ms))
                {

                    //float scale = Math.Min(width / bm2.Width, height / bm2.Height);
                    //var scaleWidth = (int)(bm2.Width * scale);
                    //var scaleHeight = (int)(bm2.Height * scale);
                    Bitmap bitmap = new Bitmap(bm2, new Size(bm2.Width, bm2.Height));
                    return bitmap;
                }
            }

            //return name + "." + ext;
        }

    }
}
