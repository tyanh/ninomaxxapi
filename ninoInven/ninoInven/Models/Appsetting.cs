﻿namespace ninoInven.Models
{
    public class Appsetting
    {
        public string urlvfc = "";
        public Appsetting()
        {
            var config = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json").Build();
            urlvfc= config["AppSettings:linkvfc"];
        }
    }
}
