﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ninoInven.Models;
using System.Drawing;
using ZXing;

namespace ninoInven.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        [HttpPost("readbarcode")]
        public string readbarcode(string data)
        {
          
          var objs= JsonConvert.DeserializeObject<dynamic>(data.ToString());

            var imagea = common.GetImgBitmap(objs.imgstr.ToString());
           

            var reader = new BarcodeReaderGeneric();

            Bitmap image = (Bitmap)imagea;

            using (image)
            {
                LuminanceSource source;
                source = new ZXing.Windows.Compatibility.BitmapLuminanceSource(image);
                //decode text from LuminanceSource
                Result result = reader.Decode(source);
                if (result == null)
                    return "0";
                return result.Text;

            }

        }
    }
    public class postimg
    {
        public string imgstr { get; set; }
    }
}
