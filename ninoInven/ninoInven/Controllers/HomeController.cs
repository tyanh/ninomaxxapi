﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ninoInven.Models;
using System.Diagnostics;

namespace ninoInven.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        ApiCall api = new ApiCall();
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.data = "";
            ViewBag.search = "";
            ViewBag.nodata = "";
            return View();
        }
        [HttpPost]
        public IActionResult Index(string upc,string stores)
        {
            ViewBag.data = "";
            ViewBag.search = upc;
            ViewBag.nodata = "";
            var au = HttpContext.Session.GetString("au");
            var data = JsonConvert.DeserializeObject<dynamic>(au);
            string l = "inventory/get-onhand-by-mtk?desc1=" + upc + "&storeNo="+data.st.ToString()+ "&where="+ stores;
            try
            {
                ViewBag.data = api.GetData(data.au.ToString(), l);
            }
            catch
            {
                ViewBag.nodata = "No data";
            }
            ViewBag.stores = stores;
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Test()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            dynamic lg=api.LoginVFCApi(new
            {
                username = username,
                password = password
            });
            if (lg != null)
            {
                string tk = lg["apiToken"].ToString();
                string storeno = lg["storeNo"].ToString();
                var ss = new
                {
                    au = tk,
                    st = storeno,
                };
            
                HttpContext.Session.SetString("au", JsonConvert.SerializeObject(ss));
      
                return Redirect("/");
            }
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}