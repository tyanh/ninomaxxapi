﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using vfcweb.Models;

namespace vfcweb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        ApiCall api = new ApiCall();
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            dynamic lg = api.LoginVFCApi(new
            {
                username = username,
                password = password
            });
            if (lg != null)
            {
                string tk = lg["apiToken"].ToString();
                string storeno = lg["storeNo"].ToString();
                var ss = new
                {
                    au = tk,
                    st = storeno,
                };

                HttpContext.Session.SetString("au", JsonConvert.SerializeObject(ss));

                return Redirect("/");
            }
            return View();
        }
        [Route("/tim-ton-kho")]
        public IActionResult timtonkho()
        {
            ViewBag.data = "";
            ViewBag.search = "";
            ViewBag.nodata = "";
            return View();
        }
        [HttpPost]
        [Route("/tim-ton-kho")]
        public IActionResult timtonkho(string upc)
        {
            ViewBag.data = "";
            ViewBag.search = upc;
            ViewBag.nodata = "";
            var au = HttpContext.Session.GetString("au");
            var data = JsonConvert.DeserializeObject<dynamic>(au);
            string l = "inventory/get-onhand-by-mtk?desc1=" + upc + "&storeNo=" + data.st.ToString();
            try
            {
                ViewBag.data = api.GetData(data.au.ToString(), l);
            }
            catch
            {
                ViewBag.nodata = "No data";
            }
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}