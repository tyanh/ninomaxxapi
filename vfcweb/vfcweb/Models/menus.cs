﻿namespace vfcweb.Models
{
    public class menus
    {
        public static List<dynamic> getmenus()
        {
            var mnu = new List<dynamic>();
            mnu.Add(creatmenu("Tìm kiếm tồn kho", "tim-ton-kho","admin"));
            mnu.Add(creatmenu("Doanh thu cửa hàng", "tim-ton-kho","admin"));
            mnu.Add(creatmenu("Thông tin khách hàng", "tim-ton-kho", "admin"));
            return mnu;
           
        }
        public static dynamic creatmenu(string title, string link, string account) {
            return new
            {
                title = title,
                link = "/"+link,
                account = account,
            };
        }
    }
}
