﻿using DocumentFormat.OpenXml.Drawing.Diagrams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooCommerceNET;
using WooCommerceNET.Base;
using WooCommerceNET.WooCommerce.v2;
namespace appwoo
{
    class woodynamic
    {
        static RestAPI rest;
        WCObject wc;
        public woodynamic(string domain, string keya, string keyb)
        {
            rest = new RestAPI(domain, keya, keyb);
            wc = new WCObject(rest);
        }
        public async Task<bool> UpCateproduct(List<string> cate, Product p, List<ProductTagLine> tags)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();

            p.categories = ct;
            p.tags = tags;
            await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return true;
        }
        public async Task<String> Upproducts(Product p)
        {
            var a = await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return "";
        }
        public async Task inputProductvariable(Product p, string price, string color, string size, string imgs)
        {
            List<Variation> vrs = new List<Variation>();
            List<VariationAttribute> vatrib = new List<VariationAttribute>()
                { new VariationAttribute() {id=4, name="Màu sắc",option=color },
                  new VariationAttribute() {id=2,name="Kích thước",option=size }
            };

            Variation var1 = new Variation()
            {
                regular_price = decimal.Parse(price),
                visible = true,
                attributes = vatrib,
                stock_quantity = 5,
                manage_stock = false,

            };
            if (imgs != "")
            {
                string[] nameimg = imgs.Split('/');
                var img = new VariationImage();
                img.position = 1;
                img.src = imgs;
                img.name = nameimg[nameimg.Length - 1];
                var1.image = img;
            }
            vrs.Add(var1);
            var1 = await wc.Product.Variations.Add(var1,int.Parse(p.id.Value.ToString()));
            p.variations.Add(int.Parse(var1.id.Value.ToString()));
            await wc.Product.Update(int.Parse(p.id.Value.ToString()), p);
        }
        public List<ProductAttributeLine> listAttrID(List<string> color, List<string> size)
        {
            List<ProductAttributeLine> L_atr = new List<ProductAttributeLine>();

            ProductAttributeLine atr_color = new ProductAttributeLine();
            atr_color.id = 4;
            atr_color.options = color;
            atr_color.position = 0;
            atr_color.variation = true;
            L_atr.Add(atr_color);

            ProductAttributeLine atr_size = new ProductAttributeLine();
            atr_size.id = 2;
            atr_size.options = size;
            atr_size.position = 1;
            atr_size.variation = true;
            L_atr.Add(atr_size);
            return L_atr;
        }
        public async Task<Product> addproduct(List<ProductAttributeLine> L_atr, List<string> cates, string name, string short_des, string imgs)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            ct = createcate(cates);
            Product p = new Product()
            {
                name = name,
                short_description = "",
                description = short_des,
                slug = name,
                type = "variable",
                status = "publish",
                attributes = L_atr,
                categories = ct,

            };
            if (imgs != "")
            {
                List<ProductImage> iimhs = new List<ProductImage>();
                string[] nameimg = imgs.Split('/');
                var img = new ProductImage();
                img.position = 1;
                img.src = imgs;
                img.name = nameimg[nameimg.Length - 1];
                iimhs.Add(img);
                p.images = iimhs;
            }
            p = await wc.Product.Add(p);
            return p;
        }

        public async Task<List<ProductCategory>> Listcate()
        {
            List<ProductCategory> ct = await wc.Category.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },
                });
            return ct;
        }
        public List<ProductCategoryLine> createcate(List<string> cates)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            foreach (var i in cates)
            {
                try
                {
                    ProductCategoryLine cate = new ProductCategoryLine();
                    cate.id = ulong.Parse(i);
                    ct.Add(cate);
                }
                catch
                {

                }
            }
            return ct;
        }

        public async Task<String> UpListproducts(BatchObject<Product> p)
        {
            var a = await wc.Product.UpdateRange(p);
            return "";
        }
        public async Task<List<Product>> SearchProduct(string sku)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku },{ "per_page", "100" },
                });
            return ct;
        }
        public async Task<String> UpVariations(int id, BatchObject<Variation> v)
        {
            var a = await wc.Product.Variations.UpdateRange(id, v);
            return "";
        }
        public async Task<List<Variation>> Variationss(int idparent)
        {
            var ct = await wc.Product.Variations.GetAll(idparent, new Dictionary<string, string>() {
                        { "per_page", "100" },
                });

            return ct;
        }
        public async Task<List<Product>> productpublish(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },{"status","publish"},
                });

            return ct;
        }
        public async Task<List<Product>> productdiscount(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },{"on_sale","true"},{"sku","true"}
                });

            return ct;
        }
    }
}
