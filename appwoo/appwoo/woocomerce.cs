﻿using DocumentFormat.OpenXml.Drawing.Diagrams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooCommerce.NET.WordPress.v2;
using WooCommerceNET;
using WooCommerceNET.Base;
using WooCommerceNET.WooCommerce.v2;
namespace appwoo
{
    class woocomerce
    {
        string trimstr(string json)
        {
            return json.Trim(new char[] { '\uFEFF', '\u200B' });
        }
       // static RestAPI rest = new RestAPI("https://demo.devweb.vn/wp-json/wc/v2/", "ck_06b73e5a36be13e2cf44a2353e7bc2095468e5d5", "cs_9d277c3ea3cdb45bcdcd7f7162e6e9552ddd5f48");


        static RestAPI rest = new RestAPI("https://ninomaxx.com.vn/wp-json/wc/v2/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725");
        //static RestAPI rest = new RestAPI("http://localhost/wp-json/wc/v2/", "ck_1ad23b97b577e6c26ff4799a29030ee15dd8ff59", "cs_cb16495c602b13fba7e889e96f38382b10bdcf58");
         WCObject wc = new WCObject(rest);

        WPObject clientt = new WPObject(rest);
        public woocomerce()
        {
             //rest = new RestAPI("https://vantin.devweb.vn/wp-json/wc/v2/", "ck_b9e5fe92bb6f1430401e6629f9de3b7c9c38a8cb", "cs_763d638b89cf8beb06e279caadcb83b8bd19e2f0", jsonDeserializeFilter: trimstr);
            rest = new RestAPI("https://ninomaxx.com.vn/wp-json/wc/v2/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725", jsonDeserializeFilter: trimstr);

            wc = new WCObject(rest);
            WPObject clientt = new WPObject(rest);
        }
        public async Task<bool> UpCateproduct(List<string> cate, Product p, List<ProductTagLine> tags)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
          
            p.categories = ct;
            p.tags = tags;
            await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return true;
        }
        public async Task<Product> GetProduct(int id)
        {
            var a = await wc.Product.Get(id);
            return a;
        }
       
        public async Task<String> Upproducts(Product p)
        {
            var a = await wc.Product.Update(int.Parse(p.id.ToString()),p);
            return "";
        }
        public async Task inputProductvariable(Product p, string price, string color, string size, string imgs)
        {
            List<Variation> vrs = new List<Variation>();
            List<VariationAttribute> vatrib = new List<VariationAttribute>()
                { new VariationAttribute() {id=4, name="Màu sắc",option=color },
                  new VariationAttribute() {id=2,name="Kích thước",option=size }
            };
           
            Variation var1 = new Variation()
            {
                regular_price = decimal.Parse(price),
                visible = true,
                attributes = vatrib,
                stock_quantity = 5,
                manage_stock = false,
               
            };
            if (imgs != "")
            {
                string[] nameimg = imgs.Split('/');
                var img = new VariationImage();
                img.position = 1;
                img.src = imgs;
                img.name = nameimg[nameimg.Length - 1];
                var1.image = img;
            }
            vrs.Add(var1);
            var1 = await wc.Product.Variations.Add(var1,int.Parse(p.id.Value.ToString()));
            p.variations.Add(int.Parse(var1.id.Value.ToString()));
            await wc.Product.Update(int.Parse(p.id.Value.ToString()), p);
        }
        public List<ProductAttributeLine> listAttrID(List<string> color, List<string> size)
        {
            List<ProductAttributeLine> L_atr = new List<ProductAttributeLine>();

            ProductAttributeLine atr_color = new ProductAttributeLine();
            atr_color.id = 4;
            atr_color.options = color;
            atr_color.position = 0;
            atr_color.variation = true;
            L_atr.Add(atr_color);

            ProductAttributeLine atr_size = new ProductAttributeLine();
            atr_size.id = 2;
            atr_size.options = size;
            atr_size.position = 1;
            atr_size.variation = true;
            L_atr.Add(atr_size);
            return L_atr;
        }
        public async Task<string> upproduct(Product p)
        {
            await wc.Product.Update((int)p.id,p);
            return "";
        }
        public async Task<string> addproduct2(string price, string saleprice, List<ProductCategoryLine> cates, string name,  string imgs, List<ProductTagLine> tags, string type, string status)
        {

            Product p = new Product()
            {
                name = name,
                slug = name,
                type = type,
                status = status,
                tags = tags,
                categories = cates,
       
            };
            if (price != "" && price.ToLower()!="liên hệ")
                p.regular_price = decimal.Parse(price);
            if (saleprice != "" && saleprice.ToLower() != "liên hệ")
                p.sale_price = decimal.Parse(saleprice);
            if (imgs != "")
            {
                List<ProductImage> iimhs = new List<ProductImage>();
                var limg = imgs.Split(',');
                foreach (var i in limg)
                {
                    string[] nameimg = imgs.Split('/');
                    var img = new ProductImage();
                    img.position = 1;
                    img.src = i;
                    img.name = nameimg[nameimg.Length - 1];
                    iimhs.Add(img);
                }
                p.images = iimhs;

            }
             await wc.Product.Add(p);
            return "a";
        }
        public async Task<string> addproduct(List<ProductAttributeLine> L_atr, List<ProductCategoryLine> cates, string name, string short_des, string _des, string imgs,List<ProductMeta> meta, List<ProductTagLine> tags, string type,string status)
        {
          
            Product p = new Product()
            {
                name = name,
                short_description =short_des,
                description = _des,
                slug = name,
                type = type,
                status = status,
                //attributes = L_atr,
                tags=tags,
                categories = cates,
                meta_data= meta,

            };
            if (imgs != "")
            {
                List<ProductImage> iimhs = new List<ProductImage>();
                var limg = imgs.Split(',');
                foreach (var i in limg) { 
                string[] nameimg = imgs.Split('/');
                var img = new ProductImage();
                img.position = 1;
                img.src = i;
                img.name = nameimg[nameimg.Length - 1];
                iimhs.Add(img);
            }
                p.images = iimhs;
               
            }
            p = await wc.Product.Add(p);
            return "a";
        }
        public async Task<string> _createTag(string oldid, string name,string slug)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            ProductTag newCategory = new ProductTag
            {
                description = oldid,
                name=name,
                slug=slug,
                //id= (int)js["id"],
            };

            await wc.Tag.Add(newCategory);
            return "0";
        }
        public async Task<string> _upTag(int id,string oldid, string name, string slug)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            ProductTag newCategory = new ProductTag
            {
                description = oldid,
                name = name,
                slug = slug,
                //id= (int)js["id"],
            };

            await wc.Tag.Update(id,newCategory);
            return "0";
        }
        public async Task<ProductCategory> _upparentcateslug(int id, string slug)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            ProductCategory newCategory = new ProductCategory
            {
             
                slug = slug
                //id= (int)js["id"],
            };

            ProductCategory shoes = await wc.Category.Update(id, newCategory);
            return newCategory;
        }
        public async Task<ProductCategory> _upparentcate(int id, int parent)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            ProductCategory newCategory = new ProductCategory
            {
                 parent =ulong.Parse(parent.ToString()),
                // slug=slug
                //id= (int)js["id"],
            };

            ProductCategory shoes = await wc.Category.Update(id,newCategory);
            return newCategory;
        }
        public async Task<ProductCategory> _createcate(string name,string slug,string desc,int parent)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            ProductCategory newCategory = new ProductCategory
            {
                name = name,
                slug = slug,
                description =desc,
                parent=ulong.Parse(parent.ToString()),
                //id= (int)js["id"],
            };

            ProductCategory shoes = await wc.Category.Add(newCategory);
            return newCategory;
        }
        public async Task<Posts> _getPosttest()
        {
            var a= await clientt.Post.GetAll();
            return a[0];
        }
            public async Task<int> _createPosttest()
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            Posts newCategory = new Posts
            {
                title = "dasd",
              
                content = "dsds",
                status = "publish",
               
                //id= (int)js["id"],
            };
            await clientt.Post.Add(newCategory);
            return 0;
        }
        public async Task<int> _createPost(string name, string slug, string desc, string status,List<int> cate)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            Posts newCategory = new Posts
            {
                title = name,
                slug = slug,
                content = desc,
                status = status,
                categories=cate,
                //id= (int)js["id"],
            };
            await clientt.Post.Add(newCategory);
            return 0;
        }
        public async Task<int> _createPostVanTin(string name, string slug, string desc, string status, List<int> cate)
        {
            //var js = JsonConvert.DeserializeObject<dynamic>(cates);

            Posts newCategory = new Posts
            {
                title = name,
                slug = slug,
                content = desc,
                status = status,
                categories = cate,
             
                //id= (int)js["id"],
            };
            await clientt.Post.Add(newCategory);
            return 0;
        }
        public async Task<List<ProductTag>> ListTags(string page)
        {
            List<ProductTag> ct = await wc.Tag.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" }, { "page", page }
                });
            return ct;
        }
        
        public async Task<List<ProductCategory>> Listcate(string page)
        {
            List<ProductCategory> ct = await wc.Category.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" }, { "page", page }
                });
            return ct;
        }
            public List<ProductCategoryLine> createcate(List<string> cates)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            foreach (var i in cates)
            {
                try
                {
                    ProductCategoryLine cate = new ProductCategoryLine();
                    cate.id = ulong.Parse(i);
                    ct.Add(cate);
                }
                catch
                {

                }
            }
            return ct;
        }

        public async Task<String> UpListproducts(BatchObject<Product> p)
        {
            var a = await wc.Product.UpdateRange(p);
            return "";
        }
        public async Task<List<Product>> SearchProduct(string sku)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku },{ "per_page", "100" },
                });
            return ct;
        }
        public async Task<String> UpVariations(int id, BatchObject<Variation> v)
        {
            var a=await wc.Product.Variations.UpdateRange(id, v);
            return "";
        }
        public async Task<List<Variation>> Variationss(int idparent)
        {
            var ct = await wc.Product.Variations.GetAll(idparent, new Dictionary<string, string>() {
                        { "per_page", "100" },
                });

            return ct;
        }
        public async Task<List<Product>> productall(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100"},  { "page", page }
                });

            return ct;
        }
        public async Task<List<Product>> productpublish(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },{"status","publish"},
                });

            return ct;
        }
        public async Task<List<Product>> productdiscount(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },{"on_sale","true"},{"sku","true"}
                });

            return ct;
        }
    }
}
