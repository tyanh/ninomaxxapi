﻿using DocumentFormat.OpenXml.Drawing.Diagrams;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooCommerce.NET.WordPress.v2;
using WooCommerceNET;
using WooCommerceNET.Base;
using WooCommerceNET.WooCommerce.v2;
namespace appwoo
{
    class wooninomaxx
    {
        string trimstr(string json)
        {
            
            return json.Trim(new char[] { '\uFEFF', '\u200B' });
        }
        static WCObject wc;
        public async Task<List<Variation>> Variationss(int? idparent)
        {
            var ct = await wc.Product.Variations.GetAll(idparent, new Dictionary<string, string>() {
                        { "per_page", "100" },
                });

            return ct;
        }
        public async Task<String> UpVariations(int id, BatchObject<Variation> v)
        {
            var a = await wc.Product.Variations.UpdateRange(id, v);
            return "";
        }
        public wooninomaxx(string o)
        {
            if (o == "nino")
            {
                var rest = new RestAPI("https://ninomaxxconcept.com/wp-json/wc/v2/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725", jsonDeserializeFilter: trimstr);
                wc = new WCObject(rest);
            }
            else if (o == "newnino")
            {
                var rest = new RestAPI("https://nino.com.vn/wp-json/wc/v2/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725", jsonDeserializeFilter: trimstr);
                wc = new WCObject(rest);
            }
            else if (o == "nm")
            {
                var rest = new MyRestAPI("https://nmclub.ninomaxxconcept.com/wp-json/wc/v2/", "ck_2b401be2cb9e50eddb19b278943702abb6fa771c", "cs_71a433dbbb0485e36da8f7bebed0e4ceae2d7a20", jsonDeserializeFilter: trimstr);
                wc = new WCObject(rest);
            }
            else if (o == "cart")
            {
                var rest = new RestAPI("https://secure-www.ninomaxxconcept.com/wp-json/wc/v2/", "ck_5bc469ebc57b4813ea89ead0a87bcf45adf000a6", "cs_f7fbef6f1641c0e70b6c7da341397e9b082cb4a1");
                wc = new WCObject(rest);
            }
        }
        public async Task<int?> Addvariation(string idpro,Variation var1)
        {
            var rs =  await wc.Product.Variations.Add(var1,int.Parse(idpro));
            return int.Parse(rs.id.ToString());
        }
        public async Task<int?> Addcate(ProductCategory ct)
        {
            var rs=await wc.Category.Add(ct);
            return int.Parse(rs.id.ToString());
        }
        public async Task<List<ProductCategory>> Listcate(string page)
        {
            try
            {
                List<ProductCategory> ct = await wc.Category.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" }, { "page", page }
                });
                return ct;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public async Task<List<Product>> disablePr(string sku)
        {
            var cts = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku },{ "per_page", "1" },
                });
            if(cts.Count > 1)
            {
                var aa = "";
            }
            if (cts.Count > 0)
            {
                var ct = cts.FirstOrDefault(s => s.sku == sku);
                ct.status = "pending";
                await wc.Product.Update(int.Parse(ct.id.ToString()), ct);
            }
            return cts;
        }
        public async Task<List<Product>> SearchProduct(string sku)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku },{ "per_page", "100" },
                });
            return ct;
        }
        public async Task<List<Product>> productlist(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },
                });
            return ct;
        }
        public async Task<List<Order>> getOrder(string page)
        {
            var ct = await wc.Order.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },
                });
            return ct;
        }
        public async Task<string> getproductliststr(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                 { "page", page },
                });
            return JsonConvert.SerializeObject(ct);
        }
        public async Task<List<Product>> getproductlist(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                 { "page", page },
                });
            return ct;
        }
        public List<ProductMeta> listMetabrand(string brand)
        {
            List<ProductMeta> L_meta = new List<ProductMeta>();
            ProductMeta meta = new ProductMeta();
            meta.id = 1621807;
            meta.key = "thuong_hieu";
            meta.value = brand;
            L_meta.Add(meta);
            return L_meta;
        }
        public async Task<bool> updateproduct(Product p)
        {
            var ct = await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return true;
        }
        public async Task<List<ProductAttributeLine>> listAttrID(List<string> color, List<string> size)
        {
            List<ProductAttributeLine> L_atr = new List<ProductAttributeLine>();
            var ll = await wc.Attribute.GetAll();

            ProductAttributeLine atr_color = new ProductAttributeLine();
            atr_color.id = ll.FirstOrDefault(s => s.name.ToLower() == "color").id;
            atr_color.options = color;
            atr_color.position = 0;
            atr_color.variation = true;
            L_atr.Add(atr_color);

            ProductAttributeLine atr_size = new ProductAttributeLine();
            atr_size.id = ll.FirstOrDefault(s => s.name.ToLower() == "size").id;
            atr_size.options = size;
            atr_size.position = 1;
            atr_size.variation = true;
            L_atr.Add(atr_size);
            return L_atr;
        }
        public string replaceChar(string str)
        {

            List<string> c = new List<string>() { ".", " ", "/", "&" };
            foreach (var i in c)
            {
                str = str.Replace(i, "_");
            }
            return str;
        }
        public List<ProductCategoryLine> createcate(List<string> cates)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            foreach (var i in cates)
            {
                try
                {
                    ProductCategoryLine cate = new ProductCategoryLine();
                    cate.id = ulong.Parse(i);
                    ct.Add(cate);
                }
                catch
                {

                }
            }
            return ct;
        }
        public async Task<List<ProductAttribute>> listAttrwoo()
        {
            var ll = await wc.Attribute.GetAll();
            return ll;
        }
        public async Task<Product> addproduct(List<ProductAttributeLine> L_atr,string stt,List<ProductMeta> metas, string name,string slug, string short_des, string sku, List<string> cates)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            ct = createcate(cates);

            Product p = new Product()
            {
                name = name,
                short_description = short_des,
                description = "",
                type = "variable",
                status = stt,
                attributes = L_atr,
                sku = sku,
                categories = ct,
                menu_order = 0,
                slug= slug,
                meta_data= metas
            };
            p = await wc.Product.Add(p);
            return p;
        }
        public async Task<Variation> inputProductvariable(Product p, string price, string upc, string color, string size, string pricedown, string idcolor, string idsize)
        {
            List<Variation> vrs = new List<Variation>();
            List<VariationAttribute> vatrib = new List<VariationAttribute>()
                { new VariationAttribute() {id=ulong.Parse(idcolor), name="color",option=color },
                  new VariationAttribute() {id=ulong.Parse(idsize),name="Size",option=size }
            };

            //var img = new VariationImage();
            //img.position = 1;
            //img.src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2uiIkpYd2Nr39eLLzm-dmxKAAXiN1_cm4EZKyXwmnCkcL2Ks8Vw&s";
            //img.name = upc;
            Variation var1 = new Variation()
            {
                regular_price = decimal.Parse(price),
                visible = true,
                attributes = vatrib,
                stock_quantity = 5,
                manage_stock = false,
                sku = upc,
               
            };
            if (pricedown != "")
                var1.sale_price = decimal.Parse("50000");
            vrs.Add(var1);
            try
            {
                var1 = await wc.Product.Variations.Add(var1, int.Parse(p.id.Value.ToString()));
            }
            catch(Exception ex)
            {
                var dsds = ex.Message;
                UTF8Encoding utf8 = new UTF8Encoding();
                
                // Encode the string.
                Byte[] encodedBytes = utf8.GetBytes(dsds);
                // Decode bytes back to string.
                String decodedString = utf8.GetString(encodedBytes);

            }
            p.variations.Add(int.Parse(var1.id.Value.ToString()));
            await wc.Product.Update(int.Parse(p.id.Value.ToString()), p);
            return var1;
        }
        public async Task<bool> deleteproduct(int id)
        {
            var dele = await wc.Product.Delete(id);
            return true;
        }
    }
}
