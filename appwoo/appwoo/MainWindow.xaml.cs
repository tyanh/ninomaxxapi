﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClosedXML.Excel;
using Microsoft.Win32;
using Newtonsoft.Json;
using WooCommerceNET.Base;
using WooCommerceNET.WooCommerce.v2;


namespace appwoo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        woocomerce woo = new woocomerce();
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void exp_product_public_Click(object sender, RoutedEventArgs e)
        {
            DataTable table = new DataTable("product");
            table.Columns.Add("tên", typeof(string));
            table.Columns.Add("upc", typeof(string));
            table.Columns.Add("mtk ", typeof(string));
            table.Columns.Add("giá bán", typeof(int));
            table.Columns.Add("giá giảm", typeof(int));
            MessageBox.Show("bat dau");
            int page = 1;
            List<Product> objs = new List<Product>();
            do
            {

                var obj = await woo.productpublish(page.ToString());
                if (obj.Count <=0)
                {
                    page = -1;
                    break;
                }
                else
                {
                    objs.AddRange(obj);
                    page++;
                    count.Text = page.ToString();
                  
                }
               
            } while (page > 0);
            foreach (var i in objs)
            {
                var vari = await woo.Variationss(int.Parse(i.id.ToString()));
                foreach (var v in vari)
                {
                    table.Rows.Add(i.name, v.sku, i.sku, v.regular_price, v.sale_price);
                }
           
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(@"D:\sanphamwoo.xlsx");
                }
            }
            MessageBox.Show("xong");
        }

        private async void delete_down_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                fileName =openFileDialog.FileName;

                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws2 = ws1.Rows().Skip(1).GroupBy(s => s.Cell(1).Value).ToList();
                int bg = 1;

                List<Product> lkey = new List<Product>();

                foreach (var y in ws2)
                {
                    var sku = y.Key.ToString();
                    var pr = await woo.SearchProduct(sku);
                    if (pr.Count > 0)
                    {
                        pr[0].status = "pending";
                         await woo.Upproducts(pr[0]);

                    }
                    bg++;

                    count.Text = bg.ToString();
                }
                MessageBox.Show("ok");
            }
        
            //for(var i=0;i<= pag; i++)
            //{
            //    BatchObject<Product> b_ = new BatchObject<Product>();
            //    b_.update = lkey.Take(50).ToList();
            //    lkey = lkey.Skip(50).ToList();
            //    await woo.UpListproducts(b_);
            //}
            //int page = 1;
            //List<Product> objs = new List<Product>();
            //do
            //{

            //    var obj = await woo.SearchProduct("1906046");
            //    if (obj.Count <= 0)
            //    {
            //        page = -1;
            //        break;
            //    }
            //    else
            //    {
            //        objs.AddRange(obj);
            //        page = -1;
            //        break;
            //        page++;
            //    }
            //} while (page > 0);
            //foreach (var i in objs)
            //{

            //    List<Variation> vrdown = new List<Variation>();
            //    var vari = await woo.Variationss(int.Parse(i.id.ToString()));
            //    //foreach(var v in vari)
            //    //{
            //    //    v.price = 0;
            //    //    vrdown.Add(v);
            //    //}
            //    //BatchObject<Variation> b_ = new BatchObject<Variation>();
            //    //b_.update = vrdown;
            //    //await woo.UpVariations(int.Parse(i.id.ToString()), b_);
            //    break;
            //}
        }

        private async void up_down_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";

            if (openFileDialog.ShowDialog() == true)
            {
                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var example = ws1.Rows().FirstOrDefault();
                var ws2 = ws1.Rows().GroupBy(s => s.Cell(1).Value.ToString()).Skip(1).ToList();
                //var ws2 = ws1.Rows().Skip(1).GroupBy(s => s.Cell(3).Value).ToList();

                int pag = ws2.Count / 100;
                int bg = 01;
                wooninomaxx woonnm = new wooninomaxx("nm");
                List<string> lkey = new List<string>();
                List<string> rs = new List<string>();
                MessageBox.Show("bat dau");
                List<string> errw = new List<string>();
                var listdc = new List<string>();
                foreach (var y in ws2)
                {
                   
                    var sku = y.Key.ToString();
                     var pr = await woonnm.SearchProduct(sku);
                         if (pr.Count == 0)
                         {
                             //err.Text = err.Text + "</n>" + sku;
                             errw.Add(sku);
                             continue;
                         }
                        var oldprice = y.Select(s => s.Cell(2).Value.ToString()).ToList().FirstOrDefault();
                         var newprice_sale = y.Select(s => s.Cell(3).Value.ToString()).ToList().FirstOrDefault();
                    if (pr[0].status == "publish")
                    {
                        if (int.Parse(oldprice) > int.Parse(newprice_sale))
                        {
                            listdc.Add(y.Key.ToString());
                        }
                    }

                /*    List<Variation> vrdown = new List<Variation>();
                     var vari = await woonnm.Variationss(int.Parse(pr[0].id.ToString()));

                     foreach (var v in vari)
                     {
                             if (oldprice == "")
                                 oldprice = "0";
                             if (int.Parse(oldprice) < int.Parse(newprice_sale))
                             {

                                 v.regular_price = decimal.Parse(newprice_sale);

                             }
                             else if (int.Parse(oldprice) > int.Parse(newprice_sale))
                             {

                                 //v.regular_price = decimal.Parse(newprice_sale);
                                 v.sale_price = decimal.Parse(newprice_sale);
                                 if(pr[0].status=="publish")
                                 {
                                     var ff = "";
                                 }
                             }
                             v.image = null;
                         vrdown.Add(v);
                     }
                     BatchObject<Variation> b_ = new BatchObject<Variation>();
                     b_.update = vrdown;
                     await woonnm.UpVariations(int.Parse(pr[0].id.ToString()), b_);
                     bg++;
                     count.Text = bg.ToString();*/
                }
                string asas = string.Join(",", listdc);
                keyb.Text = asas;
            MessageBox.Show("ket thuc");
            }
        }

        private async void bbi_Click(object sender, RoutedEventArgs e)
        {
      
            if (domain.Text == "" || keya.Text == "" || keyb.Text == "")
            {
                MessageBox.Show("nhập thông tin");
                return;
            }
  
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
               // try
                //{
                    woodynamic wdnm = new woodynamic(domain.Text, keya.Text, keyb.Text);
                    //string miss = "Kính mũ ZEUS 205;Kính mũ ZEUS 613B;Kính LS2 MX436;Kính LS2 OF570;Kính LS2 OF597;Kính Yohe 632 Trong;KÍNH YOHE 851;Kính YOHE 967 NEW;Kính YOHE Goggle 137;Kính YOHE Goggle 138;Kính YOHE Goggle 138B;Kính YOHE Goggle 16;Kính Yohe Goggle 39;Mái che Bulldog;Beon Mask;Mũ 3/4 ANDES 111;Mũ 3/4 Andes 111 Luxury;Mũ 3/4 Royal M20;Mũ 3/4 ANDES 111 Version 2;Mũ 3/4 Andes 382;Mũ 3/4 Andes 382 Luxury;Mũ 3/4 Bulldog Beagle;Mũ 3/4 Bulldog JIS Nhật;Mũ 3/4 Bulldog Leather 2017;Mũ 3/4 Bulldog Perro 4U;MŨ 3/4 BULLDOG PERRO VERSION 3;Mũ 3/4 Bulldog Pom;Mũ 3/4 gắn hàm YOHE 863A;Mũ 3/4 LS2 CABRIO CARBON OF597;Mũ 3/4 LS2 VERSO OF570 2 Kính;Mũ 3/4 YOHE 878;Mũ 3/4 ZEUS 205;Mũ 3/4 Zeus 613B;Mũ Bảo Hiểm Fullface ROC;Mũ cào cào YOHE 632;Mũ Dual Sport Yohe 632A Adventure;MŨ FULLFACE BULLDOG CLASICO;Mũ Fullface Bulldog Knight 2 Kính;MŨ FULLFACE BULLDOG TORII;MŨ FULLFACE LS2 CHALLENGER CARBON FF327;MŨ FULLFACE LS2 CHALLENGER HPFC FF327;Mũ Fullface LS2 RAPID FF353;Mũ Fullface LS2 ROOKIE FF352;Mũ Fullface LS2 STREAM FF320;Mũ Fullface LS2 VECTOR CARBON FF397;Mũ Fullface LS2 VECTOR FF397;Mũ Fullface PREMIER TOURAN 1200;Mũ lật hàm Yohe 938;Mũ Fullface Yohe 970;Mũ Fullface YOHE 967 2019;Mũ Fullface Yohe 977;Mũ Fullface Yohe 978 Plus;Mũ Fullface Yohe 978 Plus 2020;Mũ Fullface Yohe 981;Mũ Lật Hàm LS2 METRO FF324;Mũ Lật Hàm LS2 VALIANT FF399;Mũ Lật Hàm LS2 VORTEX FF313;MŨ NỬA ĐẦU BULLDOG GANG;MŨ NỬA ĐẦU BULLDOG PUG;Mũ nửa đầu LS2 Rebellion HH590;Mũ off road LS2 FAST MX437;Mũ off road LS2 PIONEER MX436;Mũ 3/4 Yohe 851 1 kính;Mũ xe đạp Bulldog;Kính LS2 FF327 Challenger;Phụ kiện mũ ZEUS 613B;Quần Bảo Hộ ALPINESTARS MISSILE LEATHER Đen;Tai Nghe Bluetooth YOHE;Trùm đầu Ninja Bulldog";
                    //List<string> lmiss = miss.Split(';').ToList();
                    fileName = openFileDialog.FileName;

                    var workbook = new XLWorkbook(fileName);
                    var ws1 = workbook.Worksheet(1);
                    var ws2 = ws1.Rows().Skip(1).GroupBy(s => s.Cell(2).Value).ToList();
                    //ws2 = ws2.Where(s => lmiss.IndexOf(s.Key.ToString()) != -1).ToList();
                    // var catese = ws1.Rows().Skip(5).GroupBy(s => s.Cell(5).Value).ToList();
                    //List<string> cates = new List<string>();
                    //cates.AddRange(catese.Where(s =>s.Key.ToString() != "").Select(s => s.Key.ToString()).ToList());
                    //woo.createcate(cates);
                    int bg = 72;

                    List<Product> lkey = new List<Product>();
                    var cte = await wdnm.Listcate();
                    ws2 = ws2.Where(s => s.Key.ToString() != "").ToList();
                    List<string> err = new List<string>();
                    foreach (var y in ws2)
                    {

                        List<string> cates = new List<string>();
                        List<string> colors = new List<string>();
                        List<string> sizes = new List<string>();
                        List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
                        var cate = y.Select(s =>  s.Cell(5).Value.ToString()).FirstOrDefault();
                        var result = System.Text.Encoding.Unicode.GetBytes(cate);
                        if (cate == "")
                            cate = "khác";
                        cates = cte.Where(s => s.name.ToLower() == cate.ToLower()).Select(s => s.id.ToString()).ToList();
                        colors = y.Select(s =>  s.Cell(10).Value.ToString()).ToList();
                        sizes = y.Select(s => s.Cell(12).Value.ToString()).ToList();
                        string desc = y.Select(s => s.Cell(3).Value.ToString()).FirstOrDefault();
                        string imgm = y.Select(s => s.Cell(26).Value.ToString()).FirstOrDefault();
                        if (cates.Count <= 0)
                            continue;
                        Product p = new Product();

                        attrs = wdnm.listAttrID(colors, sizes);
                        //try
                        //{
                        p = await wdnm.addproduct(attrs, cates, y.Key.ToString(), desc, imgm);
                        foreach (var m in y)
                        {
                            string price = m.Cell(21).Value.ToString();
                            string color = m.Cell(10).Value.ToString();
                            string size = m.Cell(12).Value.ToString();
                            string img = m.Cell(26).Value.ToString();
                            await wdnm.inputProductvariable(p, price, color, size, img);
                        }
                        //}
                        //catch
                        //{
                        //    err.Add(y.Key.ToString());
                        //}
                        bg++;

                        count.Text = bg.ToString();

                    }
                    MessageBox.Show("ok");
                    var sss = string.Join(";", err);
                //}
                //catch
                //{
                //    MessageBox.Show("đóng file trước");
                //}
            }

        }

        private async void changeprice_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws2 = ws1.Rows().Skip(1).GroupBy(s => s.Cell(1).Value).ToList();
                int bg = 1;
                List<string> lkey = new List<string>();
                List<string> rs = new List<string>();
                MessageBox.Show("bat dau");
                List<string> errw = new List<string>();
                foreach (var y in ws2)
                {
                    var sku = y.Key.ToString();
                    var pr = await woo.SearchProduct(sku);
                    if (pr.Count != 0)
                    {
                        err.Text = err.Text + "</n>" + sku;
                       
                    }
                    else
                    {
                        errw.Add(sku);
                        continue;
                    }
                    var price = y.Select(s => s.Cell(2).Value.ToString()).ToList().FirstOrDefault();
                    List<Variation> vrdown = new List<Variation>();
                    var vari = await woo.Variationss(int.Parse(pr[0].id.ToString()));

                    foreach (var v in vari)
                    {
                        //v.regular_price = decimal.Parse(price);
                        v.sale_price = decimal.Parse(price);
                        vrdown.Add(v);
                    }
                    BatchObject<Variation> b_ = new BatchObject<Variation>();
                    b_.update = vrdown;
                    await woo.UpVariations(int.Parse(pr[0].id.ToString()), b_);
                    bg++;
                    count.Text = bg.ToString();
                }
                string asas = string.Join(",", errw);
                MessageBox.Show("ket thuc");
            }
        }
        public async void importcate(int _obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {

                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
            
                List<IXLRow> ws = new List<IXLRow>();
                if (_obj == 0)
                {
    
                    ws = ws1.Rows().Skip(1).ToList();
                }
                else
                    ws = ws1.Rows().Skip(1).Where(s => s.Cell(3).Value.ToString() != "0").ToList();
                int page = 1;
                List<ProductCategory> objs = new List<ProductCategory>();
                do
                {

                    var obj = await woo.Listcate(page.ToString());
                    if (obj.Count <= 0)
                    {
                        page = -1;
                        break;
                    }
                    else
                    {
                        objs.AddRange(obj);
                        page++;
                        //count.Text = page.ToString();

                    }

                } while (page > 0);
               
                int bg = 0;
                total_cate.Text = "Total: " + ws.Count();
                foreach (var y in ws)
                {
                    string splugs = y.Cell(5).Value.ToString();
                    var name =decode(y.Cell(2).Value.ToString());
                    var exit = objs.FirstOrDefault(s => s.slug == splugs);
                    
                    if(splugs== "ban-thiet-bi-bao-dong-ezviz-gia-re-nhat-hcm")
                    {
                        var asw= "sds";
                    }
                    if (_obj == 0)
                    {
                        if (exit == null)
                        {
                           
                            string splug = y.Cell(5).Value.ToString();
                            string desc = y.Cell(4).Value.ToString();
                            await woo._createcate(name, splug, desc, 0);
                        }
                        else
                        {
                            try
                            {
                               // await woo._upparentcateslug((int)exit.id, splugs);
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        if (exit != null)
                        {
                            ulong? idparent = 0;
                            string ip_ = y.Cell(3).Value.ToString();
                            var parent = ws1.Rows().FirstOrDefault(s => s.Cell(1).Value.ToString() == ip_).Cell(2).Value.ToString();
                            idparent = objs.FirstOrDefault(s => s.name ==decode(parent)).id;
                            await woo._upparentcate((int)exit.id, (int)idparent);
                        }
                    }
                    bg++;
                    count_cate.Text = bg.ToString()+ y.Cell(1).Value.ToString();
                    Thread.Sleep(300);
                }
                MessageBox.Show("xong");
            }
        }
        private void Move_catelogy_Click(object sender, RoutedEventArgs e)
        {
             importcate(0);
        }
        public String Decode(string sString)
        {
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(sString);
            byte[] win1252Bytes = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("Windows-1252"), utf8Bytes);
            string sConvertedString = Encoding.UTF8.GetString(win1252Bytes);
            return sConvertedString;
        }

        private void Move_catelogy_chile_Click(object sender, RoutedEventArgs e)
        {
            importcate(1);
        }

        private async void Move_tag_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {

                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);

                var ws = ws1.Rows().Skip(1).ToList();
                int page = 1;
                List<ProductTag> objs = new List<ProductTag>();
                do
                {

                    var obj = await woo.ListTags(page.ToString());
                    if (obj.Count <= 0)
                    {
                        page = -1;
                        break;
                    }
                    else
                    {
                        objs.AddRange(obj);
                        page++;
                        //count.Text = page.ToString();

                    }

                } while (page > 0);
                //MessageBox.Show("bat dau");
                int bg = 0;
                total_cate.Text = "Total: " + ws.Count();
                foreach (var y in ws)
                {
                    var name = decode(y.Cell(2).Value.ToString());
                    var exit = objs.FirstOrDefault(s => s.name.ToLower() == name.ToLower());
                    var oldid = y.Cell(3).Value.ToString();
                    var slug = y.Cell(4).Value.ToString();
                    if (exit==null)
                    {
                       
                        var t = await woo._createTag(oldid, name, slug);
                        bg++;
                    }
                    else
                    {
                        continue;
                        var t = await woo._upTag((int)exit.id, oldid, name, slug);
                        bg++;
                    }
                    count_cate.Text = bg.ToString() + y.Cell(1).Value.ToString();
                    Thread.Sleep(300);
                }
                MessageBox.Show("xong");
            }
        }
        public string decode(string o)
        {
            return WebUtility.HtmlDecode(o);
        }
        private void Move_new_Click(object sender, RoutedEventArgs e)
        {

            var ct =movedata.getCateNews();
            var catess = JsonConvert.DeserializeObject<List<dynamic>>(ct);
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {

                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                total_cate.Text = "Total: " + ws.Count();
                foreach (var y in ws)
                {
                    List<int> cte = new List<int>() { };
                    var id = y.Cell(1).Value.ToString();
                    var name = y.Cell(2).Value.ToString();
                    var slug = y.Cell(4).Value.ToString();
                    var desc = movedata.getContentPostNews(int.Parse(id));
                    var status = y.Cell(3).Value.ToString();
                    var cates = y.Cell(5).Value.ToString().Split(',');
                    foreach(var c in cates)
                    {
                        var idct = catess.FirstOrDefault(s => s["slug"].ToString() == c)["term_id"].ToString();
                        cte.Add(int.Parse(idct));
                    }
                    dynamic data = new System.Dynamic.ExpandoObject();
                    data.post_title = name;
                    data.post_content = desc;
                    data.post_status = status;
                    data.post_author = 1;
                    data.post_category = string.Join(",", cte);
                    data.post_name = slug;
                    movedata.PostNews(JsonConvert.SerializeObject(data));
                    count_cate.Text = name;
                    Thread.Sleep(300);
                }
                MessageBox.Show("xong");
            }
        }
        public async void addproduct() {
            int page = 1;
            int pagetag = 1;
            int pageproduct = 1;
            List<ProductCategory> objscate = new List<ProductCategory>();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                List<ProductTag> objstag = new List<ProductTag>();
                List<Product> objspro = new List<Product>();
                do
                {
                    var obj = await woo.productall(pageproduct.ToString());
                    if (obj.Count <= 0)
                    {
                        pageproduct = -1;
                        break;
                    }
                    else
                    {
                        objspro.AddRange(obj);
                        pageproduct++;

                    }

                } while (pageproduct > 0);
                do
                {
                    var obj = await woo.Listcate(page.ToString());
                    if (obj.Count <= 0)
                    {
                        page = -1;
                        break;
                    }
                    else
                    {
                        objscate.AddRange(obj);
                        page++;

                    }

                } while (page > 0);
                do
                {
                    var obj = await woo.ListTags(pagetag.ToString());
                    if (obj.Count <= 0)
                    {
                        pagetag = -1;
                        break;
                    }
                    else
                    {
                        objstag.AddRange(obj);
                        pagetag++;

                    }

                } while (pagetag > 0);
               

                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                //var sd = await movedata.UpPost();
                total_cate.Text = "Total: " + ws.Count();
                //var i280= objscate.FirstOrDefault(s => s.id == );
                foreach (var y in ws)
                {
           
                    var name = decode(y.Cell(6).Value.ToString());
                    var id = y.Cell(1).Value.ToString();
                    var slug = y.Cell(2).Value.ToString();
                    if (name.ToLower() == "bộ cấp nguồn & phân phối tín hiệu hikvision ds-kad606-p" || name.ToLower()== "Đầu Ghi hình HDTVI & IP Hikvision DS-7604HUHI-F1/N")
                    {
                        var ff = "";
                    }
                    var exit = objspro.FirstOrDefault(s => s.name.ToLower() == name.ToLower());
                    if (name == "" || exit!=null)
                        continue;

                   
                    var status = y.Cell(10).Value.ToString();
                    if (status == "")
                        status = "publish";
                    var galery = y.Cell(9).Value.ToString().Split(',');
                    var cate = y.Cell(3).Value.ToString().Split(',');
                    var tag = y.Cell(4).Value.ToString().Split(',');
                    
                    List<ProductCategoryLine> cates = new List<ProductCategoryLine>();
                    List<ProductTagLine> tags = new List<ProductTagLine>();
                 
                    foreach (var c in cate)
                    {
                        if (c != "")
                        {
                            ProductCategory _c = objscate.FirstOrDefault(s => s.slug == c);
                            ProductCategoryLine _cl = new ProductCategoryLine();

                            _cl.id = _c.id;
                            cates.Add(_cl);
                        }

                    }
                    foreach (var t in tag)
                    {
                        if (t != "")
                        {
                            ProductTag _c = objstag.FirstOrDefault(s => s.slug == t);
                            ProductTagLine _tl = new ProductTagLine();
                            _tl.id = _c.id;
                            tags.Add(_tl);
                        }
                    }


                    var _img = y.Cell(9).Value.ToString();
                    var giacu = y.Cell(8).Value.ToString();
                    var giamoi = y.Cell(7).Value.ToString();
                  
                    await woo.addproduct2(giacu, giamoi, cates, name, _img, tags, "simple", status);
                    count_cate.Text = name;
                    Thread.Sleep(200);
                }
                MessageBox.Show("xong");
            }
        }
        private async void Move_product_Click(object sender, RoutedEventArgs e)
        {
             addproduct();
            return;
            int page = 1;
            int pagetag = 1;
            int pagepro = 1;
            //var a = await woo.GetProduct(2642);
            List<ProductCategory> objscate = new List<ProductCategory>();
            
           
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                List<ProductTag> objstag = new List<ProductTag>();
                List<Product> objpro = new List<Product>();
                do
                {
                    var obj = await woo.Listcate(page.ToString());
                    if (obj.Count <= 0)
                    {
                        page = -1;
                        break;
                    }
                    else
                    {
                        objscate.AddRange(obj);
                        page++;

                    }

                } while (page > 0);
                do
                {
                    var obj = await woo.ListTags(pagetag.ToString());
                    if (obj.Count <= 0)
                    {
                        pagetag = -1;
                        break;
                    }
                    else
                    {
                        objstag.AddRange(obj);
                        pagetag++;

                    }

                } while (pagetag > 0);
                do
                {
                    var obj = await woo.productall(pagepro.ToString());
                    if (obj.Count <= 0)
                    {
                        pagepro = -1;
                        break;
                    }
                    else
                    {
                        objpro.AddRange(obj);
                        pagepro++;

                    }

                } while (pagepro > 0);

                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                //var sd = await movedata.UpPost();
                total_cate.Text = "Total: " + ws.Count();
                foreach (var y in ws)
                {
                    List<int> cte = new List<int>() { 1 };
                    var name = y.Cell(6).Value.ToString();
                    var id= y.Cell(1).Value.ToString();
                    if (name=="")
                        continue;
                    
                    var slug = y.Cell(2).Value.ToString();
                    var desc = y.Cell(14).Value.ToString();
                    var s_desc = y.Cell(13).Value.ToString();
                    var status = y.Cell(4).Value.ToString();
                    if (status == "")
                        status = "publish";
                    var galery = y.Cell(12).Value.ToString().Split(',');
                    var cate = y.Cell(10).Value.ToString().Split(',');
                    var tag = y.Cell(3).Value.ToString().Split(',');
                    var tech = y.Cell(15).Value.ToString();
                    var gift = y.Cell(16).Value.ToString();
                    List<ProductCategoryLine> cates = new List<ProductCategoryLine>();
                    List<ProductTagLine> tags = new List<ProductTagLine>();
                    List<ProductMeta> metas = new List<ProductMeta>();
                    ProductMeta mt = new ProductMeta();
                    mt.key = "noi_dung_khuyen_mai";
                    mt.value = gift;
                    ProductMeta mtt = new ProductMeta();
                    mt.key = "content_desc_prod";
                    mt.value = tech;
                    metas.Add(mt);
                    metas.Add(mtt);
                    var p_ = objpro.FirstOrDefault(s => s.name == name);
                    if (p_ != null)
                    {
                        continue;
                        p_.meta_data = metas;
                       await woo.upproduct(p_);
                       
                    }
                    foreach (var c in cate)
                    {
                        if (c != "")
                        {
                            ProductCategory _c = objscate.FirstOrDefault(s => s.slug == c);
                            ProductCategoryLine _cl = new ProductCategoryLine();

                            _cl.id = _c.id;
                            cates.Add(_cl);
                        }

                    }
                    foreach (var t in tag)
                    {
                        if (t != "")
                        {
                            ProductTag _c = objstag.FirstOrDefault(s => s.slug == t);
                            ProductTagLine _tl = new ProductTagLine();
                            _tl.id = _c.id;
                            tags.Add(_tl);
                        }
                    }
                  

                    var _img = y.Cell(12).Value.ToString();
                  
                    await woo.addproduct(null, cates, name,s_desc,desc, _img, metas,tags, "simple",status);
                    count_cate.Text = name;
                    Thread.Sleep(200);
                }
                MessageBox.Show("xong");
            }

        }

        private async void Move_product_sort_desc_Click(object sender, RoutedEventArgs e)
        {
            int pageproduct = 1;
            List<ProductCategory> objscate = new List<ProductCategory>();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                List<ProductTag> objstag = new List<ProductTag>();
                List<Product> objspro = new List<Product>();
                do
                {
                    var obj = await woo.productall(pageproduct.ToString());
                    if (obj.Count <= 0)
                    {
                        pageproduct = -1;
                        break;
                    }
                    else
                    {
                        objspro.AddRange(obj);
                        pageproduct++;

                    }

                } while (pageproduct > 0);
                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                //var sd = await movedata.UpPost();
                total_cate.Text = "Total: " + ws.Count();
                //var i280= objscate.FirstOrDefault(s => s.id == );
                int index = 1;
                foreach (var y in ws)
                {

              
                    var name =decode(y.Cell(3).Value.ToString()).ToLower();
                    var desc = y.Cell(2).Value.ToString();
                    var exit = objspro.FirstOrDefault(s => s.name.ToLower() ==name);
                    if (exit != null)
                    {
                        exit.short_description = desc;
                        await woo.upproduct(exit);
                        count_cate.Text = index+":"+ name;
                        index++;
                        Thread.Sleep(200);
                    }
                }
                MessageBox.Show("xong");
            }
        }

        private async void Move_product_desc_Click(object sender, RoutedEventArgs e)
        {

           // var sd =await woo.GetProduct(11226);
            //return;
            int pageproduct = 1;
            List<ProductCategory> objscate = new List<ProductCategory>();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
          
                List<Product> objspro = new List<Product>();
                do
                {
                    var obj = await woo.productall(pageproduct.ToString());
                    if (obj.Count <= 0)
                    {
                        pageproduct = -1;
                        break;
                    }
                    else
                    {
                        objspro.AddRange(obj);
                        pageproduct++;

                    }

                } while (pageproduct > 0);
                //objspro = objspro.OrderByDescending(s => s.date_modified).ToList();
                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                //var sd = await movedata.UpPost();
                total_cate.Text = "Total: " + ws.Count();
                //var i280= objscate.FirstOrDefault(s => s.id == );
                int index = 0;
                foreach (var y in ws)
                {


                    var name =decode(y.Cell(5).Value.ToString()).ToLower();
                    var desc = y.Cell(2).Value.ToString();
                    var thongso = y.Cell(3).Value.ToString();
                    var gift = y.Cell(4).Value.ToString();
                    var iditem = y.Cell(1).Value.ToString();
                    
                    var dikem = y.Cell(6).Value.ToString();
                    var exit = objspro.FirstOrDefault(s => s.name.ToLower() == name);
                    if (exit != null)
                    {
                        List<ProductMeta> metas = new List<ProductMeta>();
                        if (desc=="")
                            desc = movedata.getContentPost(int.Parse(iditem));
                        if(dikem!="")
                        {
                            var dk = dikem.Split('|');
                            List<object> iddikem = new List<object>();
                            foreach(var d in dk)
                            {
                                var namedi=ws.FirstOrDefault(s=>decode(s.Cell(1).Value.ToString())==d).Cell(5).Value.ToString().ToLower();
                                namedi = decode(namedi);
                                var iddk= objspro.FirstOrDefault(s => s.name.ToLower() == namedi).id.ToString();
                                iddikem.Add(int.Parse(iddk));
                            }
                            ProductMeta dkem = new ProductMeta();
                            dkem.key = "di_kem";
                            dkem.value = iddikem.ToArray();
                           
                            metas.Add(dkem);
                        }

                       
                            ProductMeta mt = new ProductMeta();
                            mt.key = "noi_dung_khuyen_mai";
                            mt.value = gift;
                            metas.Add(mt);
                       
                            ProductMeta mtt = new ProductMeta();
                            mtt.key = "content_desc_prod";
                            mtt.value = thongso;
                            metas.Add(mtt);
                 
                        exit.description = desc;
                        exit.meta_data = metas;
                        await woo.upproduct(exit);
                        count_cate.Text = index+":"+ name;
                        index++;
                        Thread.Sleep(200);
                    }
                }
                MessageBox.Show("xong");
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
           await movedata.GetStringdata("https://localhost:44384/Brand/GetAllBrands", "CfDJ8IZw7vTefDRKrLHsZjaQnsbTchWmE9VWKCDNFusBqg-r0SSqcAIAPiYq0rajuSLOHBbOMzQsBhjOnbXJAQSdsXw9lpTpeXdzlxE5irdP4or5Ek5T19GZMgl3FlkBvpYRlVvN3MhgiN2ILV2_dH-Dadr8pUddqpcRD18IFcPjIZho");
            return;
            var ps =await movedata.getallpost();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {

                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                total_cate.Text = "Total: " + ws.Count();
                int index = 1;
                foreach (var y in ws)
                {
                    
                  
                    var name =decode(y.Cell(1).Value.ToString());
                    var hinh = y.Cell(2).Value.ToString();
                    var poss = ps.FirstOrDefault(s => s.Title.Rendered.ToLower() == name.ToLower());
                    if (poss != null)
                    {
                        dynamic data = new System.Dynamic.ExpandoObject();
                        data.post_id = poss.Id;
                        data.linkimg = hinh;

                        movedata.PostNewsHinh(JsonConvert.SerializeObject(data));
                        count_cate.Text = index + ": " + name;
                        Thread.Sleep(300);
                    }
                }
                MessageBox.Show("xong");
            }
        }

        private void movepostsql_Click(object sender, RoutedEventArgs e)
        {
           
            sqlcommon sql = new sqlcommon();
            var datas = sql.getdataSQL("duhocnews", "");
            var ct = movedata.getCateNews();
            var catess = JsonConvert.DeserializeObject<List<dynamic>>(ct);
          
                foreach (var y in datas)
                {
                    List<int> cte = new List<int>() { };
                    string stt = "publish";
                    //if (y["xoa"].ToString() == "1")
                        //stt = "trash";
                    var linkimg = "https://duhoc.devweb.vn/wp-content/uploads/titlelethuy/" + y["image"].ToString();
                    var linkhtml = y["link"].ToString().Split('/');
                    var name = y["title"].ToString();
                    var slug = linkhtml[linkhtml.Length-2].Replace(".html","");
                    var desc = y["contents"].ToString().Replace("reaplaceimg", "/wp-content/uploads/contentimg");
                    var status = stt;
                    var cates = y["cate"].ToString().Split(',').ToList();
                    //var scates = y["catecon"].ToString().Split(',').ToList();
                //cates.AddRange(scates);
                foreach (var c in cates)
                    {
                    if (c == "")
                        continue;
                        var idct = catess.FirstOrDefault(s => s["name"].ToString().ToLower() == c.ToLower())["term_id"].ToString();
                        cte.Add(int.Parse(idct));
                    }
                     //var tags = y["tags"].ToString();
                    dynamic data = new System.Dynamic.ExpandoObject();
                    data.post_title = name;
                data.post_content = desc;
                    data.post_status = status;
                    data.post_author = 1;
                    data.post_category = string.Join(",", cte);
                    data.post_name = slug;
                    //data.tags = tags;
                    data.linkimg = linkimg;
                   string rs= movedata.PostNews(JsonConvert.SerializeObject(data));
                    count_cate.Text = name;
                    Thread.Sleep(300);
                }
                MessageBox.Show("xong");
            
        }

        private async void productnino_Click(object sender, RoutedEventArgs e)
        {
            wooninomaxx woos = new wooninomaxx("nm");
            List<Product> ps = new List<Product>();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                 fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                total_cate.Text = "Total: " + ws.Count();
                int id = 1;
                do
                {
                    var obj = await woos.productlist(id.ToString());
                    if (obj.Count <= 0)
                    {
                        id = -1;
                        break;
                    }
                    else
                    {
                        ps.AddRange(obj);
                        id++;
                   
                    }

                } while (id > 0);
               
                foreach (var i in ws)
                {
                    string sku = i.Cell(2).Value.ToString();
                    var obj_ = ps.FirstOrDefault(s => s.sku == sku);
                    string status = i.Cell(5).Value.ToString();
                    if (obj_!=null && status == "0")
                    {
                        await woos.deleteproduct(int.Parse(obj_.id.ToString()));
                    }
                    else if (obj_ != null && status != "0")
                    {
                        string brand = i.Cell(4).Value.ToString();
                        List<ProductMeta> meta = woos.listMetabrand(brand);
                        obj_.meta_data = meta;
                        await woos.updateproduct(obj_);
                    }
                }
            }
                  
        }
        public async Task<List<Variation>> variPrnm(int? id)
        {
            wooninomaxx woos = new wooninomaxx("nm");
            var vari = await woos.Variationss(id);
            return vari;
        }
        private async void move_sp_Click(object sender, RoutedEventArgs e)
        {
            wooninomaxx woos = new wooninomaxx("nm");
        
            List<Product> ps = new List<Product>();
            int page = 1;
            int pagecate = 1;
       
            var p = await woos.getproductlist(page.ToString());
            while (p.Count > 0)
            {
                page++;
                ps.AddRange(p);
                p = await woos.getproductlist(page.ToString());
            }
            var ctenm = await woos.Listcate(pagecate.ToString());
            List<ProductCategory> catesnm = new List<ProductCategory>();
            while (ctenm.Count > 0)
            {
                pagecate++;
                catesnm.AddRange(ctenm);
                ctenm = await woos.Listcate(pagecate.ToString());
            }

            wooninomaxx woosnino = new wooninomaxx("nino");
            List<ProductCategory> catesnino = new List<ProductCategory>();
            
            pagecate = 1;
            var ctenino = await woosnino.Listcate(pagecate.ToString());
            while (ctenino.Count > 0)
            {
                pagecate++;
                catesnino.AddRange(ctenino);
                ctenino = await woosnino.Listcate(pagecate.ToString());
            }
            var ctrootnmpr = catesnm.Where(s => s.parent == 0);
            foreach (var c in ctrootnmpr)
            {
                ulong? oldid = c.id;
                var nc = catesnino.FirstOrDefault(s => s.name.ToLower() == c.name.ToLower());
                if (nc == null)
                {
                    ProductCategory nct = new ProductCategory();
                    nct.name = c.name;
                    nct.parent = c.parent;
                    var newct =await woosnino.Addcate(nct);
                    c.id = ulong.Parse(newct.ToString());
                }
                else
                    c.id = nc.id;
                var child = catesnm.Where(s => s.parent == oldid);
                foreach (var ch in child)
                    ch.parent = c.id;
            }

            var ctrootnm = catesnm.Where(s => s.parent != 0);
            foreach (var c_ in ctrootnm)
            {
                var nc = catesnino.FirstOrDefault(s => s.name.ToLower() == c_.name.ToLower());
              
                if (nc == null)
                {
                    ProductCategory nct_ = new ProductCategory();
                    var paret= catesnino.FirstOrDefault(s => s.id == c_.parent);
                    nct_.name = c_.name;
                    nct_.parent = c_.parent;
                    var newct_ = await woosnino.Addcate(nct_);
                    c_.id = ulong.Parse(newct_.ToString()); 
                }
                else
                    c_.id = nc.id;
            }


            var Attribute = await woosnino.listAttrwoo();
            string idcolor = Attribute.FirstOrDefault(x => x.name.ToLower() == "color").id.ToString();
            string idsize = Attribute.FirstOrDefault(x => x.name.ToLower() == "size").id.ToString();
            foreach (Product i in ps)
            {
               // if (i.status != "publish")
                   // continue;
                woosnino = new wooninomaxx("nino");
                List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
                List<string> colors = new List<string>();
                List<string> sizes = new List<string>();
                var attrs_ = i.attributes;
                var color = attrs_.FirstOrDefault(s => s.name == "color");
                var size = attrs_.FirstOrDefault(s => s.name == "Size");
                foreach (var cl in color.options)
                {
                    colors.Add(cl);
                }
                foreach (var sz in size.options)
                {
                    sizes.Add(sz);
                }
                attrs = await woosnino.listAttrID(colors, sizes);
                List<string> ctes = new List<string>();
                foreach(var ct in i.categories)
                {
                    var idcate = catesnm.FirstOrDefault(s => s.name == ct.name);
                    ctes.Add(idcate.id.ToString());
                }
                try
                {
                    var newp = await woosnino.addproduct(attrs, i.status, i.meta_data, i.name, i.slug, "", i.sku.ToString(), ctes);

                    var vari = await variPrnm(int.Parse(i.id.ToString()));
                    foreach (var v in vari)
                    {
                        woosnino = new wooninomaxx("nino");
                        var clor = v.attributes.FirstOrDefault(s => s.name.ToLower() == "color").option;
                        var siz = v.attributes.FirstOrDefault(s => s.name.ToLower() == "size").option;
                        var a = await woosnino.inputProductvariable(newp, v.price.ToString(), v.sku.ToString(), clor, siz, v.sale_price.ToString(), idcolor, idsize);
                    }
                }
                catch (Exception ex) {
                
                }
            }
        }

        private async void dsb_sp_Click(object sender, RoutedEventArgs e)
        {
            wooninomaxx woos = new wooninomaxx("nino");
            List<Product> ps = new List<Product>();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fileName = "";
            if (openFileDialog.ShowDialog() == true)
            {
                fileName = openFileDialog.FileName;
                var workbook = new XLWorkbook(fileName);
                var ws1 = workbook.Worksheet(1);
                var ws = ws1.Rows().Skip(1).ToList();
                //WordPressClient client = await movedata.GetClient();
                total_cate.Text = "Total: " + ws.Count();
                int id = 1;
               
               

                foreach (var i in ws)
                {
                    string[] sku = i.Cell(4).Value.ToString().Split('-');
                    string skus = sku[sku.Length - 1];

                    await woos.disablePr(skus);
                    Thread.Sleep(3000);
                }
            }
        }
    }
}
