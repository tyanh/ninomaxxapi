﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WordPressPCL;
using WordPressPCL.Models;

namespace appwoo
{
   public class movedata
    {
        static string host = "https://duhoc.devweb.vn";
        public static async Task<WordPressClient> GetClient()
        {
            // JWT authentication
            var client = new WordPressClient("https://vantin.devweb.vn/wp-json/");
            client.AuthMethod = AuthMethod.JWT;
            await client.RequestJWToken("nubeauty", "Thientran@007");
            return client;
        }
        public static string getContentPost(int? id)
        {
          

            WebClient wc = new WebClient();
            wc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12");
            wc.Headers.Add("Accept", "*/*");
            wc.Headers.Add("Accept-Language", "en-gb,en;q=0.5");
            wc.Headers.Add("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");

            string urlData = wc.DownloadString("https://vantin.vn/wp-json/myplugin/v1/author/"+id);
            
            return urlData;
        }
        public async static Task<IEnumerable<Post>> getallpost() {
            var client = new WordPressClient("https://vantin.devweb.vn/wp-json/");
            var pos =await client.Posts.GetAll();
            return pos;
        }
        public static string getContentPostNews(int Id)
        {
           
            //var p = GetStringdata("https://vantin.vn/wp-json/ContentPost/v1/" + Id);
            return "";
        }
        public static string getCateNews()
        {

            //var p = GetStringdata(host+"/wp-json/GetDataCate/Cate","");
            var link = host + "/wp-json/GetDataCate/Cate";
            WebClient wc = new WebClient();
            var p = wc.DownloadString(host + "/wp-json/GetDataCate/Cate");
            return p;
        }
        public static string GetStringdataArduino()
        {
            WebClient wc = new WebClient();
               string urlData = wc.DownloadString("http://192.168.4.1/");

            return urlData;
        }
        public static async Task<string> GetStringdata(string url,string key)
        {
            /*WebClient wc = new WebClient();
           
            wc.Headers.Add("ContentType", "application/json; charset=UTF-8");
            wc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12");
            wc.Headers.Add("Accept-Language", "en-gb,en;q=0.5");
             wc.Headers.Add("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
            wc.Headers.Add("Authorization", "Bearer CfDJ8IZw7vTefDRKrLHsZjaQnsZJhbR8YkbuYokC2GncBH5UI85ozxjF8rKGrCM8Bf1rdOuAAZVOau7_C-EscdBPmI2_-PxwNai52tJ-BE4f0QkbUsDFnsOnlyZg2UpIkEGsyqdXqjOf7S2my7gnLfBYgA-8iM6VPJwStcTDWUFfYcXG");
            string urlData = wc.DownloadString(url);*/
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(url);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + key);
               
                    var response = await httpClient.GetStringAsync(url);
                    return response.ToString();
                


            }

            return "";
        }
        public static string PostNewsHinh(string data)
        {
            PostData("https://vantin.devweb.vn/wp-json/GetUpImgPost/Post", data);
            return "0";
        }
        public static string PostNews(string data)
        {
            return PostData(host+"/wp-json/addPost/v2", data);
           // return "0";
        }
            public static string PostData(string url, string data)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
          
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(data);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
    }
}
