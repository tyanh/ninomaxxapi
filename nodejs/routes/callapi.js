
var express = require('express');
var router = express.Router();
var cmd=require('./function');
const os = require("os");
const { json } = require('express/lib/response');
var fs = require('fs');

router.post('/postdata',function(req, res, next) {

    var data=JSON.parse(req.body.data);
    var dataip=JSON.parse(data.data);
    var title=data.obj.toLocaleLowerCase();
   
    var obj=cmd.getmaplist(title);
    var input=obj.data.input.map;
    var types=obj.data.input.types;
    var datamap={};
    var lpost=obj.data.obj;
    var lupdate=obj.data.obj;
    
    if(obj.data.apipost!=null)
        lpost=lpost+"/"+obj.data.apipost;
    
    if(typeof obj.data.mapfull=="undefined" ||  obj.data.mapfull=="0"){
        input.forEach(element => {
            
            var vl=cmd.searchdataJson(dataip,element,"obj","val");
            datamap[element]=cmd.searchdataJson(dataip,element,"obj","val");
            if(datamap[element]=="true")
                    datamap[element]=true;
            else if(datamap[element]=="false")
                    datamap[element]=false;
           
        });
    }
    else{
        datamap=dataip;
       
    }

    if(data.sid==""){

        cmd.postData(lpost,datamap).then(async function(results){
           
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ code:200,mess:results}, null, 2));
        })
    }
    else{
        datamap["sid"]=data.sid;
        
        var prsid="sid";
        if(obj.data.parasid!=null)
            prsid=obj.data.parasid;
        if(obj.data.apiupdate!=null)
            lupdate=lupdate+"/"+obj.data.apiupdate;
        else if(obj.data.apipost!=null)
            lupdate=lupdate+"/"+obj.data.apipost;
       
        var linkput=lupdate+"?"+prsid+"="+data.sid;
       
        if(obj.data.charupdate!=null)
            linkput=lupdate+"/"+data.sid;
           
        if(typeof obj.data.mtupdate =="undefined" || (obj.data.mtupdate!=null && obj.data.mtupdate!="post")){
            console.log(linkput)
            console.log(datamap)
            cmd.putData(linkput,datamap).then(async function(results){
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ code:200,mess:"0" }, null, 2));
            })
        }
        else{
            console.log(lupdate)
            console.log(datamap)
            cmd.postData(lupdate+"?"+prsid+"="+data.sid,datamap).then(async function(results){
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ code:200,mess:"0" }, null, 2));
            })
        }
    }
});
router.post('/inputJobinfor',function(req, res, next) {
    var map=cmd.getmaplist("job management");
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ code:200,mess:map }, null, 2));
})
router.post('/bg64toimg',function(req, res){
    const fs = require("fs");
// Create a base64 string from an image => ztso+Mfuej2mPmLQxgD ...
   // const base64 = fs.readFileSync("path-to-image.jpg", "base64");
// Convert base64 to buffer => <Buffer ff d8 ff db 00 43 00 ...
    var data=JSON.parse(req.body.data);
    var folder="./public/upload";
    if (!fs.existsSync(folder)){
        fs.mkdirSync(folder);
    }
    var name=folder+"/"+data.filename;
    var base64Data = data.base64.replace("data:image/jpeg;base64,", "").replace("data:image/png;base64,", "");

    const buffer = Buffer.from(base64Data, "base64");
    fs.writeFileSync(name, buffer);
    var rt="/upload/" + data.filename;
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ code:200,name: rt }, null, 2));
})
router.post('/uploadfile',function(req, res){

    var formidable = require('formidable');
    var form=new formidable.IncomingForm();
    var folder="./public/avatars";
   
    if (!fs.existsSync(folder)){
            fs.mkdirSync(folder);
        }

        form.multiples = true
        form.keepExtensions = true
        form.uploadDir=folder+"/";
        form.keepExtensions= true;
        form.multiples = true;
        form.maxFileSize =5 * 1024 * 1024; // 5MB
      
        form.parse(req, (err, fields, files) => {
           
            if (err) {
              res.writeHead(err.httpCode || 400, { 'Content-Type': 'text/plain' });
              res.end(String(err));
              return;
            }
            try{
            var oldpath = files.files0.filepath;
            fs.rename(oldpath, form.uploadDir+files.files0.originalFilename, function (err) {
                if (err) throw err;
                res.write('File uploaded!');
                res.end();
              });
              var linkimg="/avatars/"+files.files0.originalFilename;
            var link={
                link:linkimg,
                name:files.files0.originalFilename
            }
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ code:200, link }, null, 2));
        }
        catch(ex){
            console.log(ex)
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ code:-200,mess:"" }, null, 2));
        }
        });
      //res.json({code:200,mess:"586"});
      return ;
 });
router.post('/delete',function(req, res, next) {
    var data=JSON.parse(req.body.data);
 
    var title=data.obj.toLocaleLowerCase();
 
    var obj=cmd.getmaplist(title);

    var link=obj.data.obj+"/"+data.data;

    cmd.deleteData(link).then(async function(results){
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ code:200,mess:"0" }, null, 2));
    })
});
router.get('/exportexcel/:sidjob',async function(req, res, next) {
    const ExcelJS = require('exceljs');
    
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Micro');
    //worksheet.mergeCells('E1:B2');
    var fill = {
        type: 'pattern',
        pattern:'darkTrellis',
        bgColor:{argb:'ffffff'},
        fgColor:{argb:'ffffff'}
      }; 
     var border = {
        top: {style:'thin', color: {argb:'000000'}},
        left: {style:'thin', color: {argb:'000000'}},
        bottom: {style:'thin', color: {argb:'000000'}},
        right: {style:'thin', color: {argb:'000000'}}
    };
    var bghead={
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'3c763d'},
        bgColor:{argb:'3c763d'}
    }
    var alcenter={
        vertical: 'middle',
        horizontal: 'center'
    }
    var title=["No","referenceSid","jobSid","Name","description","notes","statusSid","fromDate","toDate","jobCategory"]
    const alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

  
    worksheet.getCell('A1').value = "BẢNG KÊ QUẢNG CÁO - STRONGBOW THEMATIC";
    worksheet.getCell('C3').value = "Bên cung cấp \n dich vụ";
    worksheet.getCell('D3').value ="CÔNG TY TNHH TRUYỀN THÔNG BIỂU TƯỢNG VIỆT NAM";
    worksheet.getCell('C4').value = "Địa chỉ";
    worksheet.getCell('D4').value ="Tầng 14, Toà nhà HM Town, 412 Nguyễn Thị Minh Khai, P. 5, Quận 3, TPHCM";
    worksheet.getCell('C5').value = "MST";
    worksheet.getCell('D5').value = "0315484869";
    worksheet.mergeCells('A1:N2');

    worksheet.mergeCells('A6:N7');
   
    for(var i=3;i<6;i++){
        worksheet.getRow(i).fill=fill;
        worksheet.getCell("C"+i).border=border;
        worksheet.getCell("D"+i).border=border;
    }
    worksheet.getRow(6).fill=fill;
    worksheet.getRow(1).font  = {
           color: { argb: 'cb1e1d' },
           size: 20,
            bold: true,
            
        }
    worksheet.getRow(1).alignment = {
            vertical: 'top',
            horizontal: 'center'
        }
   
    var columns=[]
    worksheet.getRow(8).fill=bghead;
    for(var i=0;i<title.length;i++){
        worksheet.getCell(alphabet[i]+'8').value =title[i];
        columns.push({ header:title[i], key: title[i], width: 10 })
    }
    worksheet.columns.push(columns)
  
   /* worksheet.mergeCells('A9:N9');
    worksheet.getCell('A9').value ="HOLIDAY";
    worksheet.getCell('A10').value ="1";
    worksheet.getCell('A12').value ="2";
    worksheet.getCell('A14').value ="3";
    worksheet.mergeCells('B10:B15');
    worksheet.getCell('B10').value ="ĐÀ NẴNG";
    worksheet.getCell('B10').alignment=alcenter;
    worksheet.mergeCells('C10:C15');
    worksheet.getCell('C10').value ="LOCAL MICRO INFLUENCERS";
    worksheet.getCell('C10').alignment=alcenter;
    */
    var sid=JSON.parse(req.cookies[cmd.getconfigname("cklogin")]).sid;

    cmd.getData("Job/GetJob/"+sid+"/"+req.params.sidjob).then(async function(resultdt){
        console.log(resultdt.details)
        for(var i=0;i<resultdt.details.length;i++){
        var d=resultdt.details[i];
        var exp=[];
        exp.push((i+1).toString());
        for(var j=1;j<title.length;j++){
            //console.log(d[title[j]])
                exp.push(d[title[j]]);
           }
           worksheet.addRow(exp);
        }
      
        var path = require("path");
    workbook.xlsx.writeFile('./public/Excels/'+resultdt.name+'.xlsx')
    .then(() => {
        res.redirect('/Excels/'+resultdt.name+'.xlsx');
 
        console.log('Excel file created!');
    })
    .catch((error) => {
      console.log(error);
    });
   // 
});
    
   
});
router.get('/getdata/:obj',function(req, res, next) {
    var link=req.url.replace('/getdata/','');
   
    cmd.getData(link).then(async function(resultdt){
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ code:200,mess:resultdt }, null, 2));
     });
});
router.get('/getdatasid/:obj/:sublink/:sid',function(req, res, next) {
    var link=req.params.obj;
    if(req.params.sublink!="")
        link=link+"/"+req.params.sublink;
    link=link+"?groupSid="+req.params.sid
   
    cmd.getData(link).then(async function(resultdt){
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ code:200,mess:resultdt }, null, 2));
     });
});
module.exports = router;