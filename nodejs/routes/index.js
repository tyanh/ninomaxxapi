var express = require('express');
var router = express.Router();
var cmd=require('./function');
var fs = require('fs');
/* GET home page. */
function btncommon(){

}

router.get('/', function(req, res, next) {
  cmd.getMenu(req,res).then(async function(result){
    var menus= result;
       res.render('index', { title: 'Index',menu:menus,job:cmd.getmaplist("job management"),payment:cmd.getmaplist("paymentrequest") });
  });
});
router.get('/login', function(req, res, next) {
  res.render('login', {layout: "login" ,mess:"sai thông tin"  });
});
router.get('/logout', function(req, res, next) {
  res.cookie(cmd.getconfigname("cklogin"),"", { maxAge: -1, httpOnly: true });
  res.cookie(cmd.getconfigname("avata"),"", { maxAge: -1, httpOnly: true });

  res.render('login', {layout: "login" ,mess:""  });
});
router.get('/profile', function(req, res, next) {
  cmd.getMenu(req,res).then(async function(result){
    var menus= result;
    
    res.render('profile', { title: 'profile',menu:menus });
  });
});
router.get('/listjob', function(req, res, next) {
  var sid=JSON.parse(req.cookies[cmd.getconfigname("cklogin")]).sid;
 
  var date = new Date(), y = date.getFullYear(), m = date.getMonth();
  var firstDay = new Date(y, m, 1).toLocaleDateString("en-GB");
  var lastDay = new Date(Date.now()).toLocaleDateString("en-GB");
  var map=cmd.getmaplist("job management");
  var statusId="";
  if(typeof req.query.firstDay!="undefined" && req.query.lastDay!="undefined"){
    firstDay=req.query.firstDay;
    lastDay=req.query.lastDay;
  }
  if(typeof req.query.statusSid!="undefined")
    statusId="&statusSid="+req.query.statusSid;
   var link=map.data.obj+"?userSid="+sid+"&fromDate="+firstDay+"&toDate="+lastDay+statusId;
  
  listjob_pay(map,link,"listjob",req,res);

});
router.get('/listpayment', function(req, res, next) {
  var map=cmd.getmaplist("paymentrequest");
  listjob_pay(map,map.data.obj,"listpayment",req,res);
 
});
function listjob_pay(map,link,page,req,res){
   cmd.getMenu(req,res).then(async function(result){
    var menus= result;

    cmd.getData(link).then(async function(resultdt){
          res.render(page, { title:map.title,menu:menus,resultdt:resultdt,titles:map.data.title,maps:map.data.map,maptype:map.data.maptype,input:map.data.input,groupbtn:map.data.groupbtn,job:cmd.getmaplist("job management"),payment:cmd.getmaplist("paymentrequest") });
          res.end();
    });
   });
}
router.get('/payment/:sid', function(req, res, next) {
  var map=cmd.getmaplist("paymentrequest");
  var mapdetail=cmd.getmaplist("paymentdetail");
  cmd.getMenu(req,res).then(async function(result){
    var menus= result;
    cmd.getData(map.data.obj+"/"+req.params.sid).then(async function(resultdt){
        var datareques=resultdt;
        cmd.getData(mapdetail.data.obj+"/GetDetailByPaymentSid/"+req.params.sid).then(async function(dtpay){
          var detailpay= dtpay;
          res.render('paydetail', {title: "pay detail",datareques:datareques,detailpay:detailpay,mapdetail:mapdetail.data,menu:menus,titles:map.data.title,maps:map.data.map,maptype:map.data.maptype,input:map.data.input,groupbtn:map.data.groupbtn,job:cmd.getmaplist("job management"),payment:cmd.getmaplist("paymentrequest") });
        });
    });
  }); 
}),
router.get('/listjob/:sid', function(req, res, next) {
  var map=cmd.getmaplist("job management");
  var mapdetail=cmd.getmaplist("job detail management");
  var mappaid=cmd.getmaplist("jobpaidview");
  var mapadditionfee=cmd.getmaplist("jobadditionfee");
  var history=cmd.getmaplist("jobactionhistory");
  var payment=cmd.getmaplist("paymentdetail");
  
  var sid=JSON.parse(req.cookies[cmd.getconfigname("cklogin")]).sid;
  cmd.getMenu(req,res).then(async function(result){
    var menus= result;

    cmd.getData(map.data.obj+"/GetJob/"+sid+"/"+req.params.sid).then(async function(resultdt){
       var result1= resultdt;
      
       res.render('jobdetail', {title: "listjob",paymentobj:payment.data,rspayment:result1.payments,history:history.data,rshistory:result1.actionHistories,rsfee:result1.additionFees,mapadditionfee:mapadditionfee.data,mappaid:mappaid.data,detail:result1.details,rspaid:result1.paidViews,mapdetail:mapdetail.data,menu:menus,resultdt:result1,titles:map.data.title,maps:map.data.map,maptype:map.data.maptype,input:map.data.input,groupbtn:map.data.groupbtn,job:cmd.getmaplist("job management"),payment:cmd.getmaplist("paymentrequest") });
    });
   });
});
router.get('/list/:link', function(req, res, next) {
  
  var link=req.params.link;
  var map=cmd.getmaplist(link.toLocaleLowerCase());

  if(typeof map == "undefined"){
    res.redirect('/');
    return false;
  }
  cmd.getMenu(req,res).then(async function(result){
      var menus= result;
    
       cmd.getData(map.data.obj).then(async function(resultdt){
       res.render('list', { title: link,menu:menus,resultdt:resultdt,titles:map.data.title,maps:map.data.map,maptype:map.data.maptype,input:map.data.input,groupbtn:map.data.groupbtn,job:cmd.getmaplist("job management"),payment:cmd.getmaplist("paymentrequest") });
    });
  });
});

router.post('/login',(req, res) => {

 cmd.getlogin(req.body.emailorusername,req.body.password).then(async function(results){
 
  if( results.statusCode=="400"){
    res.render('login', {layout: "login",mess:"sai thông tn"  });
    res.end();
  }
  else{
        var result=results.result;
        /*fs.writeFile('public\\avatars\\'+result.user.sid+".txt",result.user.imageData, function (err) {
          if (err) throw err;
          console.log('Saved!');
        });*/

        var infor={
          imageData:result.user.imageData,
          token:result.token,
          firstname:result.user.firstName,
          lastName:result.user.lastName,
          email:result.user.email,
          phone:result.user.phone,
          sid:result.user.sid,
          isBOD:result.user.isBOD
        }
        res.cookie(cmd.getconfigname("cklogin"),JSON.stringify(infor), { maxAge: 1000 * 60 * 3600, httpOnly: true });
        res.cookie(cmd.getconfigname("avata"),result.user.sid, { maxAge: 1000 * 60 * 3600, httpOnly: true });
      
        res.redirect('/');
    }
  });
 
// add new user here
})
module.exports = router;
