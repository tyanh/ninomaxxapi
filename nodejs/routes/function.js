var configs = require('../config.json');
var mapdata = require('../maplist.json');
const https = require('https');
const request = require('request-promise');
const axios = require('axios');
var fs = require('fs');
module.exports = {
    getData: function(link) {
       
        return new Promise(async function(resolve, reject) {
            request(configs.hostapi+link,async (error, response, html)  => {
               if(!error && response.statusCode == 200) {
                 var js=JSON.parse(html);
                 resolve(js.result);
                 return false;
                  
               }
               else {
                   console.log(error);
               }
                   });
         });
    },
    getMenu: function(req,res) {
        var rs=req.cookies[configs.cklogin];
        if(rs=="null" || rs==null){
            res.redirect('/login');
            return false;
        }
        return new Promise(async function(resolve, reject) {
            request(configs.hostapi+"IMRole",async (error, response, html)  => {
               if(!error && response.statusCode == 200) {
                 var js=JSON.parse(html);
                 js.result.avatar=req.cookies[configs.avata]+".txt";
                 try{
                 js.result.user=JSON.parse(req.cookies[configs.cklogin])
                 }
                 catch(ex){
                    js.result.user={}
                 }
                 resolve(js.result);
                 return false;
                  
               }
               else {
                   console.log(error);
               }
                   });
         });
    },
    searchdataJson: function(data,key,clcp,clval){
        try{
            let x = data.filter((a)=>{if(a[clcp]==key){return a}});
            return x[0][clval];
            }
            catch(ex){
                return null;
        }
        
    },
    getconfigname:function(name){
        return configs[name];
     },
     getmaplist:function(name){
        let x = mapdata.filter((a)=>{if(a["title"]==name){return a}});
        return x[0];
     
     },
     postData: function(link,data){
       
        return new Promise(async function(resolve, reject) {
            axios.post(configs.hostapi+link, data )
            .then(function (response) {
                resolve(response.data);
                return response.data;
            })
            .catch(function (error) {
                resolve(error);
                return 1;
            });
        
         });
     },
     putData: function(link,data){
        return new Promise(async function(resolve, reject) {
            axios.put(configs.hostapi+link, data )
            .then(function (response) {
                resolve(response.data);
                return response.data;
            })
            .catch(function (error) {
                return 1;
            });
        
         });
     },
     deleteData: function(link){
          const headers = {
            'accept': 'text/plain'
          }
      
        return new Promise(async function(resolve, reject) {
            axios.delete(configs.hostapi+link, {headers}).then(function (response) {
               
                resolve(response.data);
                return response.data;
            })
            .catch(function (error) {
                return 1;
            });
        });
     },
    getlogin: function(username,password){
        var data={
            "imUserName": username,
            "imPassword": password
        }
     
        return new Promise(async function(resolve, reject) {
           axios.post(configs.hostapi+"User/Login", data )
            .then(function (response) {
                resolve(response.data);
                return response.data;
            })
            .catch(function (error) {
                resolve({statusCode:"400"});
                  return 400;
            });
        
         });
        
        
    },
    checklogin:function(req, res){
            var rs=req.cookies[configs.cklogin];
            if(rs=="null" || rs==null){
                res.redirect('login');
                return false;
            }
            return true;
    },
    getavataprofile:function(){
        var rs=""
        fs.readFile('public\\avatars\\abc.txt', 'utf8', function(err, data) {
         
            rs=data;
          });
        return rs;
    }
}