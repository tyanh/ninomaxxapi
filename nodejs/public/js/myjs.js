function checknull(idform){
    var rs=0;
    $("#"+idform+" .intput-required").each(function(){
          if($(this).val()=="")
          {
            rs=1;
            $(this).focus();
            return false;
          }
    })
    return rs;
}
function filterjob(){
   var status=$("#select2-jobstatusoption-container");
   var link="";
   var para=[];

    if($(status).find('.select2-selection__placeholder').length==0){
        var statusSid="";
        $("#jobstatusoption option").each(function(){
            if($(this).text()==$(status).html()){
                statusSid=$(this).val();return false;
            }
        })
        para.push("statusSid="+statusSid)
    }
    if($("#searchfromdate").val()!="" && $("#seachtodate").val()!=""){
        para.push("firstDay="+$("#searchfromdate").val());
        para.push("lastDay="+$("#seachtodate").val())
    }
    if(para.length>0){
        link="/listjob?"+para.join("&");
        location.assign(link);
    }

   // location.assign(link);
}

function bindproject(){
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m, 1).toLocaleDateString("en-GB");
    var lastDay = new Date(Date.now()).toLocaleDateString("en-GB");
    let searchParams = new URLSearchParams(window.location.search)
    var para=[];
    if(searchParams.get('firstDay')!=null && searchParams.get('lastDay')!=null){
        firstDay=searchParams.get('firstDay');
        lastDay=searchParams.get('lastDay');
      
    }
    para.push("firstDay="+firstDay);
    para.push("lastDay="+lastDay)
    if(searchParams.get('statusSid')!=null)
        para.push("statusSid="+searchParams.get('statusSid'));

    var link="";
    if(para.length>0)
            link="&"+para.join("&");

    var usersid=$("#usersid").val();
    //console.log("/apiadmin/getdata/Job?usersid="+usersid+link)
    $.get("/apiadmin/getdata/Job?usersid="+usersid+link, function(data, status){
        
        data.mess.forEach(element => {
            var j='<a href="/listjob/'+element.sid+'" class="custom-list d-flex align-items-center px-5 py-4">';
                j+='<div class="symbol symbol-40px me-5">';
                j+='<span class="symbol-label">';
                if(element.imageData!="")
                    j+='<img src="'+element.imageData+'" style="max-width: 100%;"/>';
                else
                    j+=element.name[0];
                j+='</span>';
                j+='</div>';
                j+='<div class="d-flex flex-column flex-grow-1">';
                j+='<h5 class="custom-list-title fw-semibold text-gray-800 mb-1">'+element.name+'</h5>';
                j+='<span class="text-gray-400 fw-bold">'+element.description+'</span>';
                j+='</div>';
                j+='</a>';
                $(j).appendTo("#listjobmt");
        })
    })
}
function addrowpay(){
    var stt=$('.item-pay').length+1;
    var tr="<tr class='item-pay'><td>"+stt+"</td>";
    $('.tt-pay-detail').each(function(){
        var dsb="";
        var number="";
        var types="text";
        var isnull="";
        if($(this).attr('type')=="ipdisble")dsb="disabled";
        if($(this).attr('type')=="number")number="number";types="number"
        if($(this).attr('isnull')=="*")isnull="intput-required";

        if($(this).attr('type')=="texarea")
            tr+='<td><textarea name="w3review" rows="1" cols="0" obj="'+$(this).attr('obj')+'" class="input-data-ctm form-control form-control-solid '+isnull+'"></textarea></td>';
        else
            tr+='<td><input type="'+types+'" '+dsb+' class="'+number+ " "+ $(this).attr('obj') +' input-data-ctm form-control form-control-solid intput-required '+isnull+'" placeholder="" obj="'+$(this).attr('obj')+'" value=""></td>"';
      
    })
    tr+="<td><span style='cursor:pointer' onclick='$(this).parent().parent().remove()'>x</span></td></tr>";
    $(tr).appendTo("#bdy-pay");
    caculatortotalpay("bdy-pay");
}
function caculatortotalpayform(id){
    $("#"+id+" .number").keyup(function(){
            var pr=$("#"+id);
            var price=$(pr).find('.price input').val();
            var qlt=$(pr).find('.quantity input').val();
            var vat=$(pr).find('.vat input').val();
            if(price=="")
                price="0";
            if(qlt=="")
                qlt="0";
            if(vat=="")
                vat="0";
                console.log(price)
                console.log(qlt)
                console.log(vat)
            var sum=parseFloat(price)*parseFloat(qlt)+parseFloat(vat);
            $(pr).find('.subTotal input').val(sum);
      });
}
function caculatortotalpay(id){
    $("#"+id+" .number").keyup(function(){
            var pr=$(this).parent().parent();
            var price=$(pr).find('.price').val();
            var qlt=$(pr).find('.quantity').val();
            var vat=$(pr).find('.vat').val();
            if(price=="")
                price="0";
            if(qlt=="")
                qlt="0";
            if(vat=="")
                vat="0";
            var sum=parseFloat(price)*parseFloat(qlt)+parseFloat(vat);
            $(pr).find('.subTotal').val(sum);
      });
}
function bindpayment(){
    $.get("/apiadmin/getdata/PaymentRequest", function(data, status){
   
        data.mess.forEach(element => {
            var j='<a href="/payment/'+element.sid+'" class="custom-list d-flex align-items-center px-5 py-4">';
                j+='<div class="symbol symbol-40px me-5">';
                j+='<span class="symbol-label">';
                j+=element.title[0];
                j+='</span>';
                j+='</div>';
                j+='<div class="d-flex flex-column flex-grow-1">';
                j+='<h5 class="custom-list-title fw-semibold text-gray-800 mb-1">'+element.title+'</h5>';
                j+='<span class="text-gray-400 fw-bold">'+element.title+'</span>';
                j+='</div>';
                j+='</a>';
                $(j).appendTo("#listpayment");
        })
    })
}
function exportexcell(sid){
    $.ajax({
        url: "/apiadmin/exportexcel/"+sid,
        type: "GET",
        headers: { "Content-disposition" : "attachment; filename=tutorials.xlsx",
        "Content-Type" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
        async: false,
        success: function (data, textStatus, xhr) {
            var j=JSON.parse(data)
            console.log(j)
           // location.assign('/Excels/'+j.mess)
           
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });

  
}
function editprofile(o){
    var files = $("#changeavatar")[0].files;
    var formData = new FormData();
    formData.append('files0',files[0]);
    $.ajax({
        url: "/apiadmin/uploadfile",
        type: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        processData: false,
        async: false,
        success: function (data, textStatus, xhr) {
         if(data.code==200)
            $("#img-avatar-edit").attr("source",data["link"].link)
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
     var datapush=datapost("kt_modal_update_user_scroll",$(o).attr("obj"),$(o).attr("sid"));
           // console.log(datapush)
  
    $.post("/apiadmin/postdata", {data: JSON.stringify(datapush)}, function(result){
        if(result.code==200){
              $(o).find(".indicator-label").show();
          $(o).find(".indicator-progress").hide();
                  Swal.fire({
                  title: "Update thành công",
                  confirmButtonText: 'Ok,got it!',
              
              }).then((result) => {
              /* Read more about isConfirmed, isDenied below */
              if (result.isConfirmed) {
                  location.reload();
              } else if (result.isDenied) {
                  Swal.fire('Changes are not saved', '', 'info')
              }
              })
          }
          
      });
}
function binddatadropdown(){
    $(".select-mapdata").each(function(){
        var sl=$(this);
        var link=$(this).attr("table");
        if(link=="Job")
        link=link+"?usersid="+$("#usersid").val();
        $.get("/apiadmin/getdata/"+link, function(data, status){
         
         let root = data.mess.filter((a)=>{if(a["parentSid"]==null || a["parentSid"]==""){return a}});
            var clname=$(sl).attr("clname").split('+');

            root.forEach(element => {

               if(typeof element["active"]=="undefined" || element["active"].toString()!="false"){
                    var name="";
                    for(var n=0;n<clname.length;n++){
                        name=name+" "+element[clname[n]];
                    }
                var op="<option value="+element.sid+">"+name+"</option>";
              
                $("."+$(sl).attr("obj")+"#map"+element.sid).html(name)
                var rootLvl1 = data.mess.filter((a)=>{if(a["parentSid"]==element.sid){return a}});
                rootLvl1.forEach(element1 => {
                    var name1="";
                    for(var n=0;n<clname.length;n++){
                        name1=name1+" "+element1[clname[n]];
                    }
                     var rootLvl2 = data.mess.filter((a)=>{if(a["parentSid"]==element1.sid){return a}});
                     op+="<option value="+element1.sid+">--"+name1+"</option>";
                         rootLvl2.forEach(element2 => {
                            var name2="";
                            for(var n2=0;n2<clname.length;n2++){
                                name2=name2+" "+element2[clname[n2]];
                            }
                            op+="<option value="+element2.sid+">----"+name2+"</option>";
                        })
                })
                $(op).appendTo($(sl));
                }
            });
        });
        
    })
}
function editdata(o,f){
    $("#actiontt").html("Edit");
    var data=JSON.parse($(o).attr("dataedit"));
  
    $("#"+f+" .datepicker").each(function(){
        var obj=$(this).attr("obj");
        $(this).val(new Date(data[obj]).toLocaleDateString('en-GB'))
    })
    $("#"+f+" .imgobj").each(function(){
        var obj=$(this).attr("obj");
        $(this).attr("src",data[obj])
    })
    $(".passwordhidden").parent().hide();
    $("#"+f+" .input-data-ctm").each(function(){
        var obj=$(this).attr("obj");
        $(this).val(data[obj])
    })
    $("#"+f+" .avatar").each(function(){
        var obj=$(this).attr("obj");
        $(this).attr("src",data[obj]);
      
    })
    $("#"+f+" .check-ctm").each(function(){
        var obj=data[$(this).attr("obj")];
        $(this).prop('checked',false);
        if(obj.toString()=="true")
            $(this).prop('checked',true);
    })
    $("#"+f+" .select-mapdata").each(function(){
        var obj=$(this).attr("obj");
        $(this).find('option[value="'+data[obj]+'"]').prop('selected',true);
    })
    $("#"+f+" .btnsubmit").attr("sid",data.sid);
}
function bg64toimg(o){
    var obj={
        filename:$(o).attr('filename'),
        base64:$(o).attr('src')
    }
    var rs="";
    $.ajax({
        url: "/apiadmin/bg64toimg",
        type: 'POST',
        dataType: 'json',
        data: { data: JSON.stringify(obj) },
        async: false,
        success: function (data, textStatus, xhr) {
            rs=data.name;
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
    return rs;
}
function datapost(idform,obj,sid){
    var data=[];

    $("#"+idform+" .input-data-ctm").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).val()
        })
    })
    $("#"+idform+" .rootsid").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$("#rootsid").val()
        })
    })
    $("#"+idform+" .imgobj").each(function(){
        var src="";
        if($(this).attr("source")=="0")
            src=bg64toimg($(this))
        else
            src=$(this).attr("src")
            console.log(src)
        data.push({
            obj:$(this).attr("obj"),
            val:src
        })
    })
    $("#"+idform+" .img-obj").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).attr("source")
        })
    })
    $("#"+idform+" .img-avatar").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).val()
        })
    })
    $("#"+idform+" .getid").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$($(this).attr("data")).val()
        })
    })
  
   $("#"+idform+" .datepicker").each(function(){
    var dspl=$(this).val().toString().replaceAll("/","-").split('-');
    var date=new Date(dspl[1]+"/"+dspl[0]+"/"+dspl[2]);
    
    date.setDate(date.getDate() + 1)
        data.push({
            obj:$(this).attr("obj"),
            val:date
        })
       
    })
    $("#"+idform+" .userlog").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).attr("data")
        })
    })
    $("#"+idform+" .avatar").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).attr("src")
        })
    })
    $("#"+idform+" .select-mapdata").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).find(":selected").val()
        })
    })
    $("#"+idform+" .check-ctm").each(function(){
        data.push({
            obj:$(this).attr("obj"),
            val:$(this).is(":checked")
        })
    })
    var datapush={
        obj:obj,
        data:JSON.stringify(data),
        sid:sid,
    };
    return datapush;
}
function createpayment(o,idform){
    if(checknull(idform)==1)
           return false;
    if(checknull('bdy-pay')==1)
           return false; 
    var datapush=datapost(idform,$(o).attr("obj"),"");
    
    var rs=postdata(datapush,"đã thêm thành công");
    if(rs.code==200){
        var sid=rs.mess.result.sid;

        $('.item-pay').each(function(){
            var p_=[];
            $(this).find('.input-data-ctm').each(function(){
                p_.push({
                    obj:$(this).attr("obj"),
                    val:$(this).val()
                })
            })
            p_.push({
                obj:"paymentRequestSid",
                val:sid
            })
            var pdetail={
                obj:"paymentdetail",
                sid:"",
                data:JSON.stringify(p_)
            }     
            var rs=postdata(pdetail,"đã thêm thành công");
        })
       
    }
    Swal.fire({
        title: "đã thêm thành công",
        confirmButtonText: 'Ok,got it!',
    
    }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
        location.reload();
    } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
    }
    })
}
function createjob(o,idform){
if(checknull(idform)==1)
   return false;

var datapush=datapost(idform,$(o).attr("obj"),"");

var jobmember=["job leader","job employees","job viewer"];
var jobmembertag=["jobleader","jobemployee","jobviewer"];
    var rs=postdata(datapush,"đã thêm thành công");

    if(rs.code==200){
        var sidid=rs.mess.result.sid;
        for(var i=0;i<jobmember.length;i++){
            var jobip=[];
            $('.'+jobmembertag[i]+' tag').each(function(){
               jobip.push(sids[leader.indexOf($(this).attr("value"))])
            })
            if(jobip.length>0){
                var obj={
                    "jobSid": sidid,
                    "userSids": jobip
                }
                var datapush={
                    obj:jobmember[i],
                    data:JSON.stringify(obj),
                    sid:"",
                };
                postdata(datapush,"đã thêm thành công");
            }
        }
        Swal.fire({
            title: "đã thêm thành công",
            confirmButtonText: 'Ok,got it!',
        
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            location.reload();
        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    }
}
function postdata(datapush,al){
   var rs = -1;
    $.ajax({
        url: "/apiadmin/postdata",
        type: 'POST',
        dataType: 'json',
        data: { data: JSON.stringify(datapush) },
        async: false,
        success: function (data, textStatus, xhr) {
            rs = data;
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
    return rs;
    
}
function createdata(o,idform){
    if(checknull(idform)==1){
      
        return false;
    }
    $(o).find(".indicator-label").hide();
    $(o).find(".indicator-progress").show();
    var datapush=datapost(idform,$(o).attr("obj"),$(o).attr("sid"));
    //console.log(JSON.parse(datapush.data));return false;
    var al='Đã thêm thành công';
    if($(o).attr("sid")!="")
    al='Đã cập nhật thành công';
 
    $.post("/apiadmin/postdata", {data: JSON.stringify(datapush)}, function(result){
      if(result.code==200){
            $(o).find(".indicator-label").show();
        $(o).find(".indicator-progress").hide();
                Swal.fire({
                title: al,
                confirmButtonText: 'Ok,got it!',
            
            }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                location.reload();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
            })
        }
        
    });
}
function bindstatus(){
    $.get("/apiadmin/getdata/JobStatus", function(data, status){
        data.mess.forEach(element => {
            var opt="<option value='"+element.sid+"'>"+element.description+"</option>";
            $(opt).appendTo("#jobstatusoption");
            if(element.sid==searchParams.get('statusSid'))
            $("#select2-jobstatusoption-container").html(element.description);
        })
    })
}
let searchParams = new URLSearchParams(window.location.search)
function change_image(input) {
    if (input.files && input.files[0]) {
         for (var i = 0; i < input.files.length; i++) {
             var reader = new FileReader();
             reader.onload = function (e) {
              
                     $("#"+$(input).attr("obj")).attr("source","0");
                     $("#"+$(input).attr("obj")).attr("filename",input.files[0].name);
                     $("#"+$(input).attr("obj")).attr("src",e.target.result);
             };
 
             reader.readAsDataURL(input.files[i]);
         }
     }
 }
$(document).ready(function(){
  $("#changeavatar").on("change",function(){
    change_image(this);
})
    $(".datepicker").datepicker({dateFormat: 'dd-mm-yy'});
    bindstatus();
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m, 1).toLocaleDateString("en-GB");
    var lastDay = new Date(Date.now()).toLocaleDateString("en-GB");
    let searchParams = new URLSearchParams(window.location.search)
    if(searchParams.get('firstDay')!=null && searchParams.get('lastDay')!=null){
        firstDay=searchParams.get('firstDay');
        lastDay=searchParams.get('lastDay');
    }
    $("#searchfromdate").val(firstDay)
    $("#seachtodate").val(lastDay)
    bindproject();
    bindpayment();
    binddatadropdown();

})