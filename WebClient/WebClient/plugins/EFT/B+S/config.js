/**
 * ButtonHook to run before the posTenderTake and posTenderGive events
 * Checks the tender type and if credit runs through EFT provider
 * otherwise returns control to prism
 */
ButtonHooksManager.addHandler(
  ['before_posTenderTake', 'before_posTenderGive'],
  function (
    $q,
    $state,
    $translate,
    $http,
    DocumentPersistedData,
    NotificationService,
    ModelService,
    prismSessionInfo,
    TenderTypes,
    ShopperDisplay,
    ExternalDataService,
    BSService,
    Transaction,
    LoadingScreen
  ) {
    'use strict';
    var deferred = $q.defer();
    // Declare variables for future use
    var document = [], // To store document information
      tender = ExternalDataService.scope.tender,
      sessionInfo = prismSessionInfo.get(), // Configuring settings and calls to server module
      documentParams = {
        sid: DocumentPersistedData.DocumentInformation.Sid,
        cols: '*',
      }; // Parameters to pass when requesting document

    /**
     * Function to process modal for credit card transactions
     * @param overrideDupeChecking
     */
    function creditTransaction() {
      // Tender Rounding MUST be enforced if the client has it enabled in the admin console
      // The following function tries to make tender rounding as simple as possible,
      // use the example below, pass the tender and the document total and use the returned
      // amount for your processing.
      Transaction.Tender.roundTender(
        tender,
        document.transaction_total_amt
      ).then(function (resp) {
        tender.amount = resp.amount;
        var payload = {
          amount: parseFloat(tender.amount),
        };
        var take = tender.mode.toUpperCase() === 'TAKE';
        var path = take ? 'payment' : 'refund';
        var newTender = ModelService.create('Tender'); // create new tender object
        LoadingScreen.Enable = true;
        LoadingScreen.topMsg= $translate.instant('5281');
        LoadingScreen.bottomMsg= $translate.instant('5282');
        // NotificationService.ajaxLoader(
        //   $translate.instant('5281'),
        //   $translate.instant('5282')
        // );
        $http.post('/v1/rest/bs_' + path, payload).then(
          function (response) {
            if (response.status === 200) {
              var payment = angular.fromJson(response.data);
              if (!payment.ErrorCode && payment.OverallResult !== 'Failure') {
                if (take) {
                  newTender.taken = payment.PaymentAmount;
                } else {
                  newTender.given = payment.PaymentAmount;
                }
                newTender.tender_type = 2; // set tender type to 2 for Credit
                newTender.document_sid = document.sid; // set tender document sid
                newTender.authorization_code = payment.ApprovalCode; // set authorization code based on return authcode
                newTender.card_type_name = payment.CardCircuit;
                newTender.card_number = payment.CardPAN.substr(
                  payment.CardPAN.length - 6
                ); // set card_number to last 6 digits of returned cardnumber
                newTender.tender_name = payment.CardCircuit; // set tender name based on available card names
                newTender.eft_transaction_id = payment.STAN; // set eft_transaction_id based on return token
                newTender.eftdata0 = payment.TimeStamp;
                newTender.eftdata1 = payment.CardCircuit;
                newTender.eftdata2 = payment.ReturnCode;
                newTender.eftdata3 = payment.RequestID;
                newTender.eftdata4 = payment.Currency;
                newTender.eftdata5 = payment.STAN;
                newTender.eftdata6 = payment.ExpiryDate;
                newTender.eftdata7 = payment.TerminalID;
                newTender.eftdata8 = payment.ReceiptNumber;
                newTender.eftdata9 = payment.Merchant;
                newTender.eftdatabsmer = payment.MerchantReceipt;
                newTender.eftdatabscust = payment.CustomerReceipt;

                ModelService.get('Document', documentParams).then(function (
                  data
                ) {
                  document = data[0];
                  LoadingScreen.cancel();
                  // $('.modal').remove();
                  // $('.modal-backdrop').remove();
                  document.addTender(newTender).then(function () {
                    //add the tender to the document
                    ModelService.get('Document', documentParams).then(function (
                      data
                    ) {
                      document = data[0];
                      // Reload page to show added tenders
                      if (
                        document.due_amt === 0 &&
                        sessionInfo.preferences
                          .automatically_print_update_balanced_document ===
                          'true'
                      ) {
                        ExternalDataService.scope.updateDocument(true);
                      } else {
                        $state.go($state.current, {}, { reload: true });
                      }
                    });
                  });
                });
              } else {
                LoadingScreen.cancel();
                // $('.modal').remove();
                // $('.modal-backdrop').remove();
                if (payment.CustomerReceipt && payment.CustomerReceipt !== '') {
                  BSService.printDeclinedReceipt(payment, tender.mode);
                } else {
                  if (payment.ErrorCode || payment.ErrorText) {
                    // if there is actual error text, show that otherwise show generic error
                    NotificationService.addAlert(payment.ErrorText, '1185');
                  } else {
                    NotificationService.addAlert('5283', '1185');
                  }
                }
                ShopperDisplay.Redraw(); // redraw shopper display
              }
            } else {
              LoadingScreen.cancel();
              // $('.modal').remove();
              // $('.modal-backdrop').remove();
              NotificationService.addAlert('5283', '1185');
            }
          },
          function (response) {
            LoadingScreen.cancel();
            // $('.modal').remove();
            // $('.modal-backdrop').remove();
            var data = angular.fromJson(response.data);
            if (
              (data.MerchantReceipt && data.MerchantReceipt !== '') ||
              (data.CustomerReceipt && data.CustomerReceipt !== '')
            ) {
              BSService.printDeclinedReceipt(data, tender.mode);
            }
            if (data.ErrorCode || data.ErrorText) {
              // if there is actual error text, show that otherwise show generic error
              NotificationService.addAlert(data.ErrorText, '1185');
            } else {
              NotificationService.addAlert('5283', '1185');
            }
            ShopperDisplay.Redraw(); // redraw shopper display
          }
        );
      });
    }

    function getTenderRule(tenderPref, index) {
      // function to get value of specific tender rule
      var cut = tenderPref.split(','); // split preference into array by comma
      if (index === 'oTender') {
        //check if the index equals oTender
        if (cut[1]) {
          if (cut[1]) {
            return cut[1]; // return value of cut[1]
          } else {
            return 0; // return zero
          }
        }
        var bools = cut[0].split(''); // split cut
        return bools[index] === 'T'; // return true if value at index is T
      }
    }

    /** Logic check that starts the process if the tender amount is not zero and the tender type matches one of
     * the correct tender types supported by the EFT provider.
     **/
    var tenderType = TenderTypes.getTenderPosition(tender.tenderType);
    if (tenderType === 2) {
      //Verify that the tender amount is not zero
      if (parseFloat(tender.amount) === 0) {
        // Throw error if amount is zero
        //'Amount must be greater than 0', 'Missing Amount'// Untranslated values for the message
        NotificationService.addAlert('2400', '2401').then(function () {
          // exit the plugin and cancel tender process
          deferred.reject();
        });
      } else {
        ModelService.get('Document', documentParams).then(function (data) {
          document = data[0];
          if (TenderTypes.getPrefName(tenderType)) {
            var tName = TenderTypes.getPrefName(tenderType);
            var oTender = getTenderRule(
              sessionInfo.preferences['pos_tenders_rules_' + tName],
              'oTender'
            );
            if (tender.mode === 'Take') {
              if (tender.amount - document.due_amt > oTender) {
                // Untranslated Text ('Take Value Exceeds Maximum Over Tender Amount','Error')
                NotificationService.addAlert('3276', '1185');
                deferred.reject();
                return;
              }
            } else {
              if (tender.amount + document.due_amt > oTender) {
                // Untranslated Text ('Take Value Exceeds Maximum Over Tender Amount','Error')
                NotificationService.addAlert('3276', '1185');
                deferred.reject();
                return;
              }
            }
          }
          // process credit card transaction without CED
          creditTransaction();
        });
      }
    } else {
      // exit the plugin and process the tender normally in Prism
      deferred.resolve();
    }

    return deferred.promise;
  }
);

SideButtonsManager.addButton({
  label: 'B+S Reconciliation',
  icon: 'images/checked_32.png',
  sections: ['register'],
  handler: [
    '$http',
    'NotificationService',
    'Toast',
    '$translate',
    'LoadingScreen',
    function ($http, NotificationService, Toast, $translate, LoadingScreen) {
      'use strict';
      LoadingScreen.Enable = true;
      LoadingScreen.topMsg= $translate.instant('5288');
      LoadingScreen.bottomMsg= $translate.instant('5289');
      // NotificationService.ajaxLoader(
      //   $translate.instant('5288'),
      //   $translate.instant('5289')
      // );
      $http.post('/v1/rest/bs_reconciliation', {}).then(function (data) {
        LoadingScreen.cancel();
        // $('.modal').remove();
        // $('.modal-backdrop').remove();
        var response = angular.fromJson(data.data);
        if (response.OverallResult.toLowerCase() === 'success') {
          Toast.Success('5280', response.OverallResult, false, false);
        } else {
          Toast.Error('5280', response.OverallResult, false, true);
        }
      });
    },
  ],
});
