window.angular.module('prismApp.plugin.services.BSService', [])
  .factory('BSService', ['$q', 'PrismUtilities', 'prismSessionInfo','DocumentPersistedData','PrintersService', 'ModelService',
    function($q, PrismUtilities, prismSessionInfo,DocumentPersistedData,PrintersService, ModelService) {
      'use strict';
      var printData = {
        'storename': '',
        'cardtype': '',
        'cardnumber': '',
        'amount': '',
        'bstimestamp':'',
        'bscardcircuit':'',
        'bsreturncode':'',
        'bsrequestid':'',
        'bscurrency':'',
        'bsstan':'',
        'bsexpirydate':'',
        'bsterminalid':'',
        'bsreceiptnumber':'',
        'bsmerchantvunumber':''
      },
      printSpec = {
          quantity: '1',
          design: prismSessionInfo.get().preferences.peripherals_output_deniedreceipts_print_design,
          printer: prismSessionInfo.get().preferences.peripherals_output_deniedreceipts_print_printer,
          printOrder: '',
          email: DocumentPersistedData.BillToCustomerData.EmailAddress,
          whatToPrint:[]
      },
      API = {
        Data:{},
        /** function to handle the printing of declined receipts based on preferences in the admin console **/
        printDeclinedReceipt: function(EMVData,mode) {
          var deferred = $q.defer();
          // use the Model service to get the store information
          ModelService.get('Store', {
            sid: prismSessionInfo.get().storesid,
            cols: 'address1,address2,address3,address4,address5,phone1,store_name,store_code,store_number,zip' // specify the columns to return from the DB
          }).then(function (data) {
            //Set up the payload of data to print
            var dt = new Date(EMVData.TimeStamp);
            printData = {
              'storename': data[0].store_name,
              'tendermode': mode,
              'cardnumber': EMVData.CardPAN.substr(EMVData.CardPAN.length - 6),  // set card_number to last 6 digits of returned cardnumber,
              'amount': parseFloat(EMVData.PaymentAmount),
              'bsdate': dt.getDate() + '.' + (dt.getMonth() + 1) + '.' + dt.getFullYear(),
              'bstime': dt.getHours() + ':' + dt.getMinutes(),
              'bscardcircuit': EMVData.CardCircuit,
              'bsreturncode': EMVData.ReturnCode,
              'bsrequestid': EMVData.RequestID,
              'bscurrency': EMVData.Currency,
              'bsstan': EMVData.STAN,
              'bsexpirydate': EMVData.ExpiryDate,
              'bsterminalid': EMVData.TerminalID,
              'bsreceiptnumber': EMVData.ReceiptNumber,
              'bsmerchantvunumber': EMVData.Merchant,
              'eftdatabsmer': EMVData.MerchantReceipt,
              'eftdatabscust': EMVData.CustomerReceipt
            };
          }).then(function () {
            // specify the params to pass when getting the document
            DocumentPersistedData.PrintDesignData.Payload = printData; // add printData to the DPD print data Payload
            printSpec.whatToPrint.push(DocumentPersistedData.PrintDesignData.Payload); // push the entire Payload into the WhatToPrint array
            PrintersService.printAction(null, 'B+S EFT Information', printSpec.whatToPrint).then(function(){
              deferred.resolve();
            },function(error){
              deferred.reject(error);
            });

          });
          return deferred.promise;
        }
      };
      return API;
    }]);
