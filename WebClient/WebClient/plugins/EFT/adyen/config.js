/**
 * Primary configuration file for Adyen plugin
 *
 * ButtonHook to run before the posTenderTake and posTenderGive events
 * Checks the tender type and if credit runs through EFT provider
 * otherwise returns control to prism
 */
'use strict';
var executionInPending = false;

var resetExecutionInPending = function() {
  //reset execution pending status to false
  setTimeout(function() {
    executionInPending = false;
  }, 2000);
};

ButtonHooksManager.addHandler(
  ['before_posTenderTake', 'before_posTenderGive'],
  function(
    Adyen,
    AdyenData,
    AdyenCredit,
    AdyenGift,
    $q,
    Toast,
    DocumentPersistedData,
    ModelService,
    prismSessionInfo,
    CreditCardTypes,
    $timeout,
    $http,
    TenderTypes,
    PrintersService,
    $state,
    $translate,
    PrismUtilities,
    $uibModal,
    ExternalDataService,
    $stateParams,
    Transaction
  ) {
    var deferred = $q.defer();
    //Execution of first click should be continued
    //Otherwise skip any later clicks until the flag is reset
    console.log('adyen btn hook');
    if (!executionInPending) {
      executionInPending = true;
      // Declare variables for future use
      var document = {}, // To store document information
        sequence = '',
        sessionInfo = prismSessionInfo.get(), // Configuring settings and calls to server module
        documentParams = { sid: $stateParams.document_sid, cols: '*' };

      /**
       * Function to start the credit card transaction process (also processes debit)
       * Future processing happens in AdyenCredit service and adyen-credit-modal or adyen-credit-refund-modal
       */
      function creditTransaction() {
        var ctPromise = $q.defer();
        AdyenCredit.Initialize('payment').then(
          function() {
            ctPromise.resolve();
          },
          function(error) {
            ctPromise.reject();
          }
        );
        return ctPromise.promise;
      }
      /**
       * Function to start the gift card transaction process
       * Future processing happens in AdyenGift service and adyen-gift-modal
       */
      function giftCardTransaction() {
        var gctPromise = $q.defer();
        AdyenGift.Initialize('payment').then(
          function() {
            gctPromise.resolve();
          },
          function(error) {
            gctPromise.reject();
          }
        );
        return gctPromise.promise;
      }

      /**
       * Function to get the tender rules from preferences
       *
       */
      function getTenderRule(tenderPref, index) {
        // function to get value of specific tender rule
        var cut = tenderPref.split(','); // split preference into array by comma
        if (index === 'oTender') {
          //check if the index equals oTender
          if (cut[1]) {
            return cut[1]; // return value of cut[1]
          } else {
            return 0; // return zero
          }
        }
        var bools = cut[0].split(''); // split cut
        return bools[index] === 'T'; // return true if value at index is T
      }

      $timeout(function() {
        var tenderType = TenderTypes.getTenderPosition(
          ExternalDataService.scope.tender.tenderType
        );
        /** Logic check that starts the process if the tender amount is not zero and the tender type matches one of
         * the correct tender types supported by the EFT provider.
         **/
        if (
          // Credit Card
          tenderType === 2 ||
          // Gift Card
          tenderType === 10 ||
          // Debit Card
          tenderType === 11
        ) {
          //Verify that the tender amount is not zero
          if (
            !ExternalDataService.scope.tender.amount ||
            parseFloat(ExternalDataService.scope.tender.amount) === 0
          ) {
            // Throw error if amount is zero
            //'Amount must be greater than 0', 'Missing Amount'// Untranslated values for the message
            Toast.Error('2401', '2400', null, true);
            // exit the plugin and cancel tender process
            deferred.reject();
            return deferred.promise;
          } else {
            // get document information to check for due amount.
            ModelService.get('Document', documentParams).then(function(data) {
              document = data[0];
              sequence = document.eft_invoice_number + '';
              // Initialize the Adyen plugin base data
              Adyen.Initialize().then(
                function() {
                  if (TenderTypes.getPrefName(AdyenData.Tender.type)) {
                    // get name of tender
                    var tName = TenderTypes.getPrefName(AdyenData.Tender.type);
                    // get the rules for the tender type
                    var oTender = getTenderRule(
                      sessionInfo.preferences['pos_tenders_rules_' + tName],
                      'oTender'
                    );
                    if (AdyenData.Tender.mode === 'Take') {
                      if (
                        AdyenData.Tender.amount - document.due_amt >
                        oTender
                      ) {
                        // Untranslated Text ('Take Value Exceeds Maximum Over Tender Amount','Error')
                        Toast.Error('1185', '3276', null, true);
                        deferred.reject();
                        return deferred.promise;
                      }
                    } else {
                      if (
                        AdyenData.Tender.amount + document.due_amt >
                        oTender
                      ) {
                        // Untranslated Text ('Take Value Exceeds Maximum Over Tender Amount','Error')
                        Toast.Error('1185', '3276', null, true);
                        deferred.reject();
                        return deferred.promise;
                      }
                    }
                    switch (AdyenData.Tender.type) {
                      // Credit Card
                      case 2:
                      // Debit Card
                      case 11:
                        creditTransaction().then(function() {
                          deferred.reject();
                        });
                        break;
                      // Gift Card
                      case 10:
                        giftCardTransaction().then(function() {
                          deferred.reject();
                        });
                        break;
                      default:
                        // not credit, debit or gift so exiting plugin and returning to Prism for tender process
                        deferred.resolve();
                        break;
                    }
                  }
                },
                function(error) {
                  deferred.reject(error);
                }
              );
            });
          }
        } else {
          deferred.resolve();
        }

        resetExecutionInPending();
      
      }, 500);
    }
    return deferred.promise;
  }
);

/**
 * ButtonHook to run before the posTenderGiftCardBalance event
 * Processes gift card information through EFT provider and returns with the balance of gift card
 */
ButtonHooksManager.addHandler(
  ['before_posTenderGiftCardBalance', 'before_posOptionsGiftCardBalance'],
  function(AdyenGift, $q) {
    var deferred = $q.defer();
    //Execution of first click should be continued
    //Otherwise skip any later clicks until the flag is reset
    if (!executionInPending) {
      executionInPending = true;
      AdyenGift.Initialize('balance').then(
        function() {
          deferred.resolve();
        },
        function(error) {
          deferred.reject(error);
        }
      );
      resetExecutionInPending();
    }
    return deferred.promise;
  }
);
/**
 * ButtonHook to run before the posTenderGiftCardAddValue event
 * Processes gift card information through EFT provider to add value to an already activated card
 */
ButtonHooksManager.addHandler(['before_posTenderGiftCardAddValue'], function(
  AdyenGift,
  $q
) {
  var deferred = $q.defer();
  //Execution of first click should be continued
  //Otherwise skip any later clicks until the flag is reset
  if (!executionInPending) {
    executionInPending = true;
    AdyenGift.Initialize('addvalue').then(
      function() {
        deferred.resolve();
      },
      function(error) {
        deferred.reject(error);
      }
    );
    resetExecutionInPending();
  }
  return deferred.promise;
});
/**
 * ButtonHook to run before the posTenderGiftCardPurchase event
 * Processes gift card information through EFT provider and activate a new unactivated card (with or without balance)
 */
ButtonHooksManager.addHandler(['before_posTenderGiftCardPurchase'], function(
  AdyenGift,
  $q
) {
  var deferred = $q.defer();
  //Execution of first click should be continued
  //Otherwise skip any later clicks until the flag is reset
  if (!executionInPending) {
    executionInPending = true;
    AdyenGift.Initialize('activate').then(
      function() {
        deferred.resolve();
      },
      function(error) {
        deferred.reject(error);
      }
    );
    resetExecutionInPending();
  }
  return deferred.promise;
});

/**
 * Places the Sidebutton for the Adyen Configuration in the Admin Console.
 *
 */
var admin = JSON.parse(sessionStorage.getItem('session')).permissions.accessadminconsole;
if (admin && admin.toUpperCase() === 'ALLOW') {
  SideButtonsManager.addButton({
    label: 'Adyen Configuration',
    icon: 'images/checked_32.png',
    sections: ['adminconsole'],
    handler: [
      'AdyenConfig',
      function(AdyenConfig) {
        AdyenConfig.Initialize();
      }
    ]
  });
}
