'use strict';
window.angular.module('prismApp').component('adyenConfiguration', {
  templateUrl: 'plugins/EFT/adyen/adyen-configuration/adyen-configuration.htm',
  bindings: { tender: '=' },
  controllerAs: 'adyenConfig',
  controller: [
    '$q',
    '$scope',
    'Adyen',
    'AdyenData',
    'AdyenConfig',
    'AdyenGift',
    function($q, $scope, Adyen, AdyenData, AdyenConfig, AdyenGift) {
      var adyenConfig = this;

      adyenConfig.$onInit = function() {
        // if there is no configuration present, load the defaults

        $scope.form = AdyenConfig.Config;

        $scope.saveConfig = function() {
          AdyenConfig.SaveConfig($scope.form)
            .then(function() {
              $scope.$parent.$close({ method: 'success' });
            })
            .catch(function(error) {
              console.log(error);
            });
        };

        // $scope.deactivateGiftCard = function() {
        //   AdyenGift.DeactivateCard($scope.form.cardno);
        // };

        // $scope.balanceGiftCard = function() {
        //   var params = { cardnumber: $scope.form.cardno };
        //   AdyenGift.GetBalance(params);
        // };

        // $scope.loadGiftCard = function() {
        //   AdyenData.Tender.type = '10';
        //   AdyenData.Tender.amount = $scope.form.gcvalue;
        //   var params = { cardnumber: $scope.form.cardno };
        //   AdyenGift.AddValue(params).then(function() {});
        // };

        // $scope.redeemGiftCard = function() {
        //   AdyenData.Tender.type = '10';
        //   AdyenData.Tender.amount = $scope.form.gcvalue;
        //   var params = { cardnumber: $scope.form.cardno };
        //   AdyenGift.ProcessPayment(params).then(function() {});
        // };

        $scope.abortConfig = function(skip) {
          $scope.$parent.$close({ method: 'cancel' });
        };
      };
    }
  ]
});
