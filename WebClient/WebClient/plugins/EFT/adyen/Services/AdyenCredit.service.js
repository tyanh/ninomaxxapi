/**
 * Primary service for the Adyen Credit and Debit processing
 */
window.angular
  .module('prismApp.plugin.services.AdyenCredit', [])
  .factory('AdyenCredit', [
    '$q',
    '$http',
    '$filter',
    '$state',
    'Adyen',
    'AdyenData',
    'PrismUtilities',
    '$translate',
    'prismSessionInfo',
    'ModelService',
    'DocumentPersistedData',
    '$uibModal',
    'Transaction',
    'ExternalDataService',
    'Toast',
    'PrintersService',
    function(
      $q,
      $http,
      $filter,
      $state,
      Adyen,
      AdyenData,
      PrismUtilities,
      $translate,
      prismSessionInfo,
      ModelService,
      DocumentPersistedData,
      $uibModal,
      Transaction,
      ExternalDataService,
      Toast,
      PrintersService
    ) {
      'use strict';

      var scope = ExternalDataService.scope;
      var prefs = prismSessionInfo.get().preferences;
      /** defining the printing preferences for declined receipts **/
      var printSpec = {
          quantity: '1',
          design: prefs.peripherals_output_deniedreceipts_print_design,
          printer: prefs.peripherals_output_deniedreceipts_print_printer,
          printOrder: '',
          email: DocumentPersistedData.BillToCustomerData.EmailAddress,
          whatToPrint: []
        },
        printData = {
          store_name: '',
          store_number: '',
          store_code: '',
          store_address_line1: '',
          store_address_line2: '',
          store_address_line3: '',
          store_address_line4: '',
          store_address_line5: '',
          store_address_zip: '',
          store_phone: '',
          created_datetime: new Date().toISOString(), // now
          card_type: '',
          card_number: '',
          amount: '',
          emv_appinfo_aid: '',
          emv_appinfo_applicationlabel: '',
          emv_cardinfo_cardexpirydate: '',
          emv_crypto_cryptogram: '',
          emv_crypto_cryptogramtype: '',
          emv_pinstatement: '',
          cashier_login_name: '',
          employee1_login_name: ''
        };
      /**
       * Primary function for adding credit and debit tenders to Prism
       */
      var addTender = function(response, rbr, oldTender) {
        var deferred = $q.defer(),
          newTender = ModelService.create('Tender');
        if (!rbr) {
          // Only continue if not Refund by Reference
          var take = AdyenData.Tender.mode.toLowerCase() === 'take',
            payment = angular.fromJson(response.data),
            paymentResponse = payment.SaleToPOIResponse.PaymentResponse,
            instrumentData =
              paymentResponse.PaymentResult.PaymentInstrumentData;

          if (take) {
            newTender.taken =
              paymentResponse.PaymentResult.AmountsResp.AuthorizedAmount;
          } else {
            newTender.given =
              paymentResponse.PaymentResult.AmountsResp.AuthorizedAmount;
          }

          newTender.tender_name = instrumentData.CardData.PFaymentBrand; // set tender name based on available card names
          newTender.tender_type = AdyenData.Tender.type;
          // Adyen does not respond with tender type in payload, need to parse AdditionalResponse field
          // then if the cardSchema is set to one of the below values the tender is a debit card.
          if (
            paymentResponse.Response &&
            paymentResponse.Response.AdditionalResponse
          ) {
            var eq = AdyenData.decodeText(
              paymentResponse.Response.AdditionalResponse
            );
            if (eq.cardScheme) {
              switch (eq.cardScheme.toLowerCase()) {
                case 'accel':
                case 'affn':
                case 'alaska':
                case 'cu24':
                case 'interlink':
                case 'maestro':
                case 'nets':
                case 'nyce':
                case 'pulse':
                case 'star':
                case 'shazam':
                case 'bcmc':
                case 'cartebancaire':
                case 'eftpos_australia':
                  newTender.tender_type = 11;
                  break;
                default:
                  newTender.tender_type = 2;
                  break;
              }
              if (eq.cardScheme.indexOf('debit') !== -1) {
                newTender.tender_type = 11;
              }
              newTender.tender_name = eq.cardScheme; // set tender name based on available card names
            }
            //if PED returns that giftcard was used
            if (
              eq.hasOwnProperty('backendGiftcardIndicator') &&
              PrismUtilities.toBoolean(eq.backendGiftcardIndicator)
            ) {
              newTender.tender_type = 10;
            }
          }

          newTender.prevent_void = false;
          newTender.card_type_name = instrumentData.CardData.PaymentBrand;
          newTender.document_sid = Transaction.Document.active.sid; // set tender document sid
          newTender.card_number = instrumentData.CardData.MaskedPan.substr(
            instrumentData.CardData.MaskedPan.length - 4
          ); // set card_number to last 6 digits of returned cardnumber

          newTender.eft_transaction_id =
            payment.SaleToPOIResponse.MessageHeader.ServiceID; // set eft_transaction_id based on return token
          // If the payload indicates the user changed the currency being used display that to the cashier
          if (
            paymentResponse.PaymentResult.CurrencyConversion &&
            paymentResponse.PaymentResult.CurrencyConversion[0]
              .CustomerApprovedFlag
          ) {
            var conv =
              paymentResponse.PaymentResult.CurrencyConversion[0]
                .ConvertedAmount;
            AdyenData.GetCurrency(conv.Currency).then(
              function(data) {
                var currency = $filter('foreign_currency')(
                  conv.AmountValue,
                  AdyenData.Tender.basecurrency
                );
                newTender.eftdata0 =
                  paymentResponse.PaymentResult.CurrencyConversion;
                newTender.eftdata18 = conv.Currency;
                newTender.eftdata19 = currency;
                // title: Non-Local Currency, message: Customer paid with a non-local currency
                Toast.Warning(
                  '6651',
                  $translate.instant('6652') + ' ' + currency,
                  null,
                  false
                );
              },
              function(error) {
                deferred.resolve(error);
              }
            );
          }
          newTender.eftdata9 =
            paymentResponse.POIData.POITransactionID.TimeStamp;
          newTender.eftdata10 =
            paymentResponse.POIData.POITransactionID.TransactionID;
          newTender.eftdata16 = payment.SaleToPOIResponse.MessageHeader.POIID;
          newTender.eftdata17 =
            payment.SaleToPOIResponse.MessageHeader.ServiceID;
          // Parse the Merchant and Customer receipts into a format that can be printed
          AdyenData.parseEMV(
            paymentResponse.PaymentReceipt[0].OutputContent.OutputText
          ).then(
            function() {
              if (paymentResponse.PaymentReceipt[0]) {
                AdyenData.ProcessReceiptData(
                  paymentResponse.PaymentReceipt[0].OutputContent.OutputText
                ).then(
                  function(mer) {
                    newTender.eftdatabsmer = mer;
                    AdyenData.ProcessReceiptData(
                      paymentResponse.PaymentReceipt[1].OutputContent.OutputText
                    ).then(
                      function(cust) {
                        newTender.eftdatabscust = cust;
                        newTender.emv_ai_aid = AdyenData.EMVData.emv_ai_aid;
                        newTender.emv_ai_applabel =
                          AdyenData.EMVData.emv_ai_applabel;
                        newTender.eftdata1 = AdyenData.EMVData.eftdata1;
                        newTender.emv_pinstatement =
                          AdyenData.EMVData.emv_pinstatement;
                        newTender.authorization_code =
                          AdyenData.EMVData.authorization_code;
                        newTender.eftdata3 =
                          AdyenData.EMVData.authorization_code;

                        finalizeTender(newTender).then(function() {
                          deferred.resolve();
                        });
                      },
                      function(error) {
                        deferred.resolve(error);
                      }
                    );
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              }
            },
            function(error) {
              deferred.resolve(error);
            }
          );
        } else {
          var reversed = angular.fromJson(response.data);
          if (reversed.SaleToPOIResponse.ReversalResponse.ReversedAmount) {
            newTender.given =
              reversed.SaleToPOIResponse.ReversalResponse.ReversedAmount;
          } else {
            newTender.given = oldTender.taken;
          }

          newTender.prevent_void = rbr;
          newTender.tender_type = oldTender.tender_type;
          newTender.document_sid = Transaction.Document.active.sid; // set tender document sid
          newTender.card_type_name = oldTender.card_type_name;
          newTender.card_number = oldTender.card_number; // set card_number to last 6 digits of returned cardnumber
          newTender.tender_name = oldTender.tender_name; // set tender name based on available card names

          newTender.eft_transaction_id =
            reversed.SaleToPOIResponse.MessageHeader.ServiceID; // set eft_transaction_id based on return token

          newTender.eftdata0 = oldTender.eftdata0;
          newTender.eftdata1 = oldTender.eftdata1;
          newTender.eftdata3 = oldTender.eftdata3;
          newTender.eftdata9 =
            reversed.SaleToPOIResponse.ReversalResponse.POIData.POITransactionID.TimeStamp;
          newTender.eftdata10 =
            reversed.SaleToPOIResponse.ReversalResponse.POIData.POITransactionID.TransactionID;
          newTender.eftdata16 = reversed.SaleToPOIResponse.MessageHeader.POIID;
          newTender.eftdata17 =
            reversed.SaleToPOIResponse.MessageHeader.ServiceID;
          newTender.eftdata18 = oldTender.eftdata18;
          newTender.eftdata19 = oldTender.eftdata19;
          newTender.emv_ai_aid = oldTender.emv_ai_aid;
          newTender.emv_ai_applabel = oldTender.emv_ai_applabel;

          newTender.emv_pinstatement = oldTender.emv_pinstatement;
          newTender.authorization_code = oldTender.authorization_code;
          AdyenData.ProcessReceiptData(
            reversed.SaleToPOIResponse.ReversalResponse.PaymentReceipt[0].OutputContent.OutputText
          ).then(
            function(mer) {
              newTender.eftdatabsmer = mer;
              AdyenData.ProcessReceiptData(
                reversed.SaleToPOIResponse.ReversalResponse.PaymentReceipt[1].OutputContent.OutputText
              ).then(
                function(cust) {
                  newTender.eftdatabscust = cust;
                  finalizeTender(newTender).then(function() {
                    deferred.resolve();
                  });
                },
                function(error) {
                  deferred.resolve(error);
                }
              );
            },
            function(error) {
              deferred.resolve(error);
            }
          );
        }
        return deferred.promise;
      };

      /**
       * Final tender function (to prevent duplicate code reuse)
       * Adds tender to document and if the preeference has been set to automatically finalize
       * balanced documents, calls the updateDocument function from Prisms Tender Controllers scope
       */
      var finalizeTender = function(newTender) {
        var deferred = $q.defer();
        Transaction.Document.active.addTender(newTender).then(
          function(crt) {
            //add the tender to the document
            AdyenData.LastTender = crt;
            // Reload page to show added tenders
            // ExternalDataService.scope.init();
            deferred.resolve({ method: 'success' }); // close the notification modal, reject and void
          },
          function(error) {
            deferred.resolve({ method: 'cancel' }); // close the notification modal, reject and void
          }
        );
        return deferred.promise;
      };

      /** Defines the visible API for the Service **/
      var API = {
        /**
         * Function to open and process the response from the Credit Modal
         */
        CreditModal: function() {
          var deferred = $q.defer();
          var adyenCreditModalPromise = $uibModal.open(
            {
              backdrop: 'static',
              keyboard: false,
              template: '<adyen-credit-modal close="$ctrl.close" dismiss="$ctrl.dismiss"></adyen-credit-modal>',
              controllerAs: '$ctrl',
              controller: function() {
                var $ctrl = this;
                $ctrl.close = adyenCreditModalPromise.close;
                $ctrl.dismiss = adyenCreditModalPromise.dismiss;
              }
            },
            function(error) {
              deferred.resolve(error);
            }
          );
          adyenCreditModalPromise.result.then(
            function(result) {
              switch (result.method) {
                case 'complete':
                  deferred.resolve(result);
                  break;
                case 'cancel':
                  //Adyen.Cancel().then(function(){
                  deferred.resolve(result);
                  //});
                  break;
              }
            },
            function(error) {
              // this usually isnt an error, the error case is thrown when the modal is dismissed.
              deferred.resolve(error);
            }
          );
          return deferred.promise;
        },
        /**
         * Function to open and process the response from the Refund by Reference modal
         */
        ReferenceModal: function() {
          var deferred = $q.defer();
          var adyenCreditRefundModalPromise = $uibModal.open(
            {
              backdrop: 'static',
              keyboard: false,
              template:
                '<adyen-credit-refund-modal close="$ctrl.close" dismiss="$ctrl.dismiss"></adyen-credit-refund-modal>',
              controllerAs: '$ctrl',
              controller: function() {
                var $ctrl = this;
                $ctrl.close = adyenCreditRefundModalPromise.close;
                $ctrl.dismiss = adyenCreditRefundModalPromise.dismiss;
              }
            },
            function(error) {
              deferred.resolve(error);
            }
          );
          adyenCreditRefundModalPromise.result.then(
            function(result) {
              switch (result.method) {
                case 'complete':
                  deferred.resolve(result);
                  break;
                case 'cancel':
                  //Adyen.Cancel().then(function(){
                  deferred.resolve(result);
                  //});
                  break;
              }
            },
            function(error) {
              // this usually isnt an error, the error case is thrown when the modal is dismissed.
              deferred.resolve(error);
            }
          );
          return deferred.promise;
        },
        /**
         * Function to handle primary processing of Refund by Reference transactions
         */
        ReferenceRefund: function(tender) {
          var deferred = $q.defer(),
            // take a deep copy of the default payload
            rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
          AdyenData.GetCurrency().then(function() {
            // configures the payload for Reversal
            rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
            rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
              rawData.SaleToPOIRequest.MessageHeader.SaleID
            );
            rawData.SaleToPOIRequest.MessageHeader.POIID = JSON.parse(
              JSON.stringify(AdyenData.Config.serialnumber)
            );
            rawData.SaleToPOIRequest.MessageHeader.MessageCategory = 'Reversal';
            rawData.SaleToPOIRequest.ReversalRequest = {};
            rawData.SaleToPOIRequest.ReversalRequest.OriginalPOITransaction = {
              POITransactionID: {
                TimeStamp: tender.eftdata9,
                TransactionID: tender.eftdata10
              }
            };
            rawData.SaleToPOIRequest.ReversalRequest.ReversalReason =
              'MerchantCancel';
            // If reversal is being done from different register than originating register include
            // POIID of original register. Adyen throws unknown error if the POIID is included from
            // originating register.
            if (tender.eftdata16 !== AdyenData.Config.serialnumber) {
              rawData.SaleToPOIRequest.ReversalRequest.OriginalPOITransaction.POIID =
                tender.eftdata16;
            }

            // Extra processing for when the amount being refunded is not the total amount taken
            if (tender.taken !== AdyenData.Tender.amount) {
              rawData.SaleToPOIRequest.ReversalRequest.ReversedAmount =
              (AdyenData.Tender.amount>tender.taken)?tender.taken:AdyenData.Tender.amount;
              rawData.SaleToPOIRequest.ReversalRequest.SaleData = JSON.parse(
                JSON.stringify(AdyenData.SaleData)
              );
              rawData.SaleToPOIRequest.ReversalRequest.SaleData.SaleToAcquirerData += 'currency=' +
                JSON.parse(
                  JSON.stringify(AdyenData.Tender.basecurrency.alphabeticcode)
                );
            }

            Adyen.Post(rawData).then(
              function(response) {
                var resp = angular.fromJson(response.data);
                if (
                  resp.SaleToPOIResponse &&
                  resp.SaleToPOIResponse.ReversalResponse.Response.Result !==
                    'Failure'
                ) {
                  API.AddTenderToDocument(response, true, tender).then(
                    function(response) {
                      // Reload page to show added tenders
                      ExternalDataService.scope.init();
                      deferred.resolve(response);
                    },
                    function(error) {
                      deferred.resolve(error);
                    }
                  );
                } else {
                  Adyen.ProcessFailure(response).then(function(response) {
                    deferred.resolve(response); // close the notification modal, reject and void
                  });
                }
              },
              function(error) {
                deferred.resolve(error);
              }
            );
          });
          return deferred.promise;
        },
        /**
         * Initialize the credit card processing and defaults         *
         */
        Initialize: function() {
          var deferred = $q.defer();
          Adyen.Initialize().then(
            function(response) {
              if (response) {
                Adyen.UpdateCurrency().then(
                  function() {
                    if (!Transaction.Document.active.ref_sale_sid) {
                      API.CreditModal().then(function() {
                        deferred.resolve();
                      });
                    } else {
                      API.GetRefundTenders().then(function(data) {
                        if (data.length > 0) {
                          AdyenData.RefundTenders = data;
                          API.ReferenceModal().then(function() {
                            deferred.resolve();
                          });
                        } else {
                          API.CreditModal().then(function() {
                            deferred.resolve();
                          });
                        }
                      });
                    }
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              } else {
                deferred.reject();
              }
            },
            function(error) {
              deferred.resolve(error);
            }
          );

          return deferred.promise;
        },
        /**
         * Primary function for Manual keyed entry and MOTO for credit and debit processing
         */
        ManualEntry: function(method) {
          var deferred = $q.defer();
          //waiting on cancel functionality.
          Transaction.Tender.roundTender(
            ExternalDataService.scope.tender,
            document.transaction_total_amt
          ).then(function(resp) {
            AdyenData.Tender.amount = resp.amount;
            // take a deep copy of the default payload
            var rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
            // configure payload for MKE or MOTO
            rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
            rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
              rawData.SaleToPOIRequest.MessageHeader.SaleID
            );
            rawData.SaleToPOIRequest.MessageHeader.POIID =
              AdyenData.Config.serialnumber;
            rawData.SaleToPOIRequest.PaymentRequest = {};
            rawData.SaleToPOIRequest.PaymentRequest.SaleData = JSON.parse(
              JSON.stringify(AdyenData.SaleData)
            );
            rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction = JSON.parse(
              JSON.stringify(AdyenData.PaymentTransaction)
            );
            AdyenData.SetTenderOptions().then(function(data) {
              if (data) {
                rawData.SaleToPOIRequest.PaymentRequest.SaleData.SaleToAcquirerData += data;
              }
              // MOTO is for when processing credit card transaction and the customer AND card are not present.
              if (method === 'moto') {
                if (data) {
                  rawData.SaleToPOIRequest.PaymentRequest.SaleData.SaleToAcquirerData +=
                    ',MOTO';
                } else {
                  rawData.SaleToPOIRequest.PaymentRequest.SaleData.SaleToAcquirerData +=
                    'tenderOption=MOTO';
                }
              } else {
                rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction.TransactionConditions = {
                  ForceEntryMode: ['Keyed']
                };
              }
              Adyen.Post(rawData).then(
                function(response) {
                  Adyen.MKE = false;
                  API.AddTenderToDocument(response).then(
                    function(response) {
                      // Reload page to show added tenders
                      ExternalDataService.scope.init();
                      deferred.resolve(response);
                    },
                    function(error) {
                      deferred.resolve(error);
                    }
                  );
                },
                function(response) {
                  Adyen.MKE = false;
                  Adyen.ProcessFailure(response).then(
                    function(response) {
                      deferred.resolve(response);
                    },
                    function(error) {
                      deferred.resolve(error);
                    }
                  );
                }
              );
            });
          });

          return deferred.promise;
        },
        /**
         * Function to process a credit or debit card payment through the PED
         */
        ProcessPayment: function() {
          var deferred = $q.defer(),
            take = AdyenData.Tender.mode.toLowerCase() === 'take',
            // take a deep copy of the default payload
            rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
          AdyenData.GetCurrency().then(function() {
            // Tender Rounding MUST be enforced if the client has it enabled in the admin console
            // The following function tries to make tender rounding as simple as possible,
            // use the example below, pass the tender and the document total and use the returned
            // amount for your processing.
            Transaction.Tender.roundTender(
              ExternalDataService.scope.tender,
              document.transaction_total_amt
            ).then(function(resp) {
              AdyenData.Tender.amount = resp.amount;
              // conifgure the payload for credit/debit
              rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
              rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
                rawData.SaleToPOIRequest.MessageHeader.SaleID
              );
              rawData.SaleToPOIRequest.MessageHeader.POIID =
                AdyenData.Config.serialnumber;
              rawData.SaleToPOIRequest.PaymentRequest = {};
              rawData.SaleToPOIRequest.PaymentRequest.SaleData = JSON.parse(
                JSON.stringify(AdyenData.SaleData)
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction = JSON.parse(
                JSON.stringify(AdyenData.PaymentTransaction)
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction.AmountsReq.Currency = JSON.parse(
                JSON.stringify(AdyenData.Tender.basecurrency.alphabeticcode)
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction.AmountsReq.RequestedAmount = parseFloat(
                AdyenData.Tender.amount
              );
              if (!take) {
                rawData.SaleToPOIRequest.PaymentRequest.PaymentData = {
                  PaymentType: 'Refund'
                };
              }
              AdyenData.SetTenderOptions().then(
                function(data) {
                  if (data) {
                    rawData.SaleToPOIRequest.PaymentRequest.SaleData.SaleToAcquirerData += data;
                  }

                  AdyenData.RequestPayload = rawData;
                  Adyen.Post(rawData).then(
                    function(response) {
                      API.AddTenderToDocument(response).then(
                        function(response) {
                          // Reload page to show added tenders
                          ExternalDataService.scope.init();
                          deferred.resolve(response);
                        },
                        function(error) {
                          deferred.resolve(error);
                        }
                      );
                    },
                    function(response) {
                      Adyen.ProcessFailure(response).then(
                        function(response) {
                          deferred.resolve(response);
                        },
                        function(error) {
                          deferred.resolve(error);
                        }
                      );
                    }
                  );
                },
                function(error) {
                  deferred.resolve(error);
                }
              );
            });
          });

          return deferred.promise;
        },
        /**
         * RPC call to retrieve tenders that are eligible to choose for Refund by Reference
         */
        GetRefundTenders: function() {
          var deferred = $q.defer();

          var payload = [
            {
              Params: {
                DocSid: AdyenData.DocSid
              },
              MethodName: 'CCTender'
            }
          ];
          $http.post('/v1/rpc', payload).then(
            function(response) {
              if (response.status === 200) {
                var ccData = PrismUtilities.responseParser(
                  response.data[0].params.tenderlist
                );
                deferred.resolve(ccData);
              } else {
                deferred.resolve();
              }
            },
            function(error) {
              deferred.resolve(error);
            }
          );

          return deferred.promise;
        },
        /**
         * API Function for adding tender to the document.
         */
        AddTenderToDocument: function(response, rbr, oldTender) {
          var deferred = $q.defer(),
            // take a deep copy of the default payload
            payment = JSON.parse(response.data);
          if (payment.SaleToPOIResponse) {
            var paymentResponse = rbr ? payment.SaleToPOIResponse.ReversalResponse
              : payment.SaleToPOIResponse.PaymentResponse;

            if (paymentResponse.Response.Result !== 'Failure') {
              if (!rbr && paymentResponse.PaymentResult.CapturedSignature) {
                addTender(response, rbr).then(
                  function() {
                    API.DisplaySignature(
                      paymentResponse.PaymentResult.CapturedSignature
                        .SignaturePoint
                    ).then(
                      function(data) {
                        if (data) {
                          deferred.resolve();
                        } else {
                          Adyen.VoidPayment(AdyenData.LastTender, true).then(
                            function() {
                              deferred.resolve({ method: 'cancel' }); // close the notification modal, reject and void
                            },
                            function(error) {
                              deferred.resolve(error);
                            }
                          );
                        }
                      },
                      function(error) {
                        deferred.resolve(error);
                      }
                    );
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              } else {
                addTender(response, rbr, oldTender).then(
                  function() {
                    deferred.resolve();
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              }
            } else {
              var instrumentData = null;
              if (paymentResponse.PaymentResult) {
                instrumentData =
                  paymentResponse.PaymentResult.PaymentInstrumentData;
              }
              if (
                paymentResponse.Response.Result === 'Failure' &&
                instrumentData
              ) {
                API.PrintDeclinedReceipt(payment).then(
                  function() {
                    Adyen.ProcessFailure(payment).then(
                      function(data) {
                        //Adyen.CloseModals();
                        deferred.resolve(data); // close the notification modal, reject and void
                      },
                      function(error) {
                        //Adyen.CloseModals();
                        deferred.resolve(error);
                      }
                    );
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              } else {
                Adyen.ProcessFailure(payment).then(
                  function(response) {
                    //Adyen.CloseModals();
                    deferred.resolve(response); // close the notification modal, reject and void
                  },
                  function(error) {
                    //Adyen.CloseModals();
                    deferred.resolve(error);
                  }
                );
              }
            }
          } else {
            // title: Error, message: Something went wrong and the response from the CED was blank.
            Toast.Error('1185', '6562', null, true);
            deferred.resolve({ method: 'cancel' }); // close the notification modal, reject and void
          }
          return deferred.promise;
        },
        /**
         * Print the Declined receipt when a failure occurs from the PED
         */
        PrintDeclinedReceipt: function(payment) {
          var deferred = $q.defer(),
            paymentResponse = payment.SaleToPOIResponse.PaymentResponse;
          if (paymentResponse.PaymentReceipt) {
            ModelService.get('Store', {
              sid: prismSessionInfo.get().storesid,
              cols:
                'address1,address2,address3,address4,address5,phone1,store_name,store_code,store_number,zip' // specify the columns to return from the DB
            }).then(
              function(data) {
                //Set up the payload of data to print
                var printData = {
                  storename: data[0].store_name,
                  storenumber: data[0].store_number,
                  storecode: data[0].store_code,
                  storeaddressline1: data[0].address1,
                  storeaddressline2: data[0].address2,
                  storeaddressline3: data[0].address3,
                  storeaddressline4: data[0].address4,
                  storeaddressline5: data[0].address5,
                  storeaddresszip: data[0].zip,
                  storephone: data[0].phone1
                };
                AdyenData.parseEMV(
                  paymentResponse.PaymentReceipt[0].OutputContent.OutputText
                ).then(
                  function() {
                    // For the most part while this data is included in the print data and payload, it isnt used in
                    // the design. We only use the eftdatabsmer and eftdatabscust fields, but the data may be needed
                    // so its easier to just leave it in.
                    printData.aid = AdyenData.EMVData.emv_ai_aid;
                    printData.cardType = AdyenData.EMVData.emv_ai_applabel;
                    printData.cvmRes = AdyenData.EMVData.emv_pinstatement;
                    printData.authCode = AdyenData.EMVData.authorization_code;
                    printData.preferredName = AdyenData.EMVData.preferredName;
                    printData.mid = AdyenData.EMVData.mid;
                    printData.tid = AdyenData.EMVData.tid;
                    printData.ptid = AdyenData.EMVData.ptid;
                    printData.panSeq = AdyenData.EMVData.panSeq;
                    printData.paymentMethod = AdyenData.EMVData.paymentMethod;
                    printData.paymentMethodVariant =
                      AdyenData.EMVData.paymentMethodVariant;
                    printData.pan = AdyenData.EMVData.pan;
                    printData.txdate = AdyenData.EMVData.txdate;
                    printData.txtime = AdyenData.EMVData.txtime;
                    printData.posEntryMode = AdyenData.EMVData.posEntryMode;
                    printData.atc = AdyenData.EMVData.atc;
                    printData.txref = AdyenData.EMVData.txref;
                    printData.mref = AdyenData.EMVData.mref;
                    printData.txtype = AdyenData.EMVData.txtype;
                    printData.approved = AdyenData.EMVData.approved;
                    printData.retain = AdyenData.EMVData.retain;
                    printData.thanks = AdyenData.EMVData.thanks;
                    printData.cardHolderHeader =
                      AdyenData.EMVData.cardHolderHeader;
                    printData.merchantTitle = AdyenData.EMVData.merchantTitle;

                    AdyenData.ProcessReceiptData(
                      paymentResponse.PaymentReceipt[0].OutputContent.OutputText
                    ).then(
                      function(mer) {
                        printData.eftdatabsmer = mer;
                        AdyenData.ProcessReceiptData(
                          paymentResponse.PaymentReceipt[1].OutputContent
                            .OutputText
                        ).then(
                          function(cust) {
                            printData.eftdatabscust = cust;
                            // specify the params to pass when getting the document
                            DocumentPersistedData.PrintDesignData.Payload = printData; // add printData to the DPD print data Payload
                            printSpec.whatToPrint.push(
                              DocumentPersistedData.PrintDesignData.Payload
                            ); // push the entire Payload into the WhatToPrint array
                            PrintersService.printAction(
                              null,
                              'Adyen EFT Information',
                              printSpec.whatToPrint
                            );
                            deferred.resolve();
                          },
                          function(error) {
                            deferred.resolve(error);
                          }
                        );
                      },
                      function(error) {
                        deferred.resolve(error);
                      }
                    );
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              },
              function(error) {
                deferred.resolve(error);
              }
            );
          } else {
            deferred.resolve(payment);
          }
          return deferred.promise;
        },
        /**
         * Allow cashier to verify signature on card matches signature from PED
         * takes signature coordinate data and processes into line drawing on canvas
         */
        VerifySignature: function(sig) {
          var c = document.getElementById('signatureCanvas'),
            ctx = c.getContext('2d'),
            json = sig,
            skip;

          for (var i in json) {
            if (skip) {
              ctx.moveTo(parseInt(json[i].X, 16), parseInt(json[i].Y, 16));
            }
            skip = sig[i].X === 'FFFF';
            if (!skip) {
              ctx.lineTo(parseInt(json[i].X, 16), parseInt(json[i].Y, 16));
            }
          }
          ctx.stroke();
        },
        /**
         * Opens modal to display Signature data from PED and allow approval
         * or rejection of transaction.
         * (This may need to be updated in the future to be enabled or disabled
         * via a setting in the Adyen Configuration screen)
         */
        DisplaySignature: function(signature) {
          var deferred = $q.defer();
          var adyenGiftCardModalPromise = $uibModal.open({
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            template:
              '<adyen-signature-modal signature="$ctrl.signature"></adyen-signature-modal>',
            controllerAs: '$ctrl',
            controller: function() {
              this.signature = signature;
            }
          });
          adyenGiftCardModalPromise.result.then(
            function(response) {
              deferred.resolve(response);
            },
            function(error) {
              deferred.resolve(error);
            }
          );

          return deferred.promise;
        }
      };
      return API;
    }
  ]);
