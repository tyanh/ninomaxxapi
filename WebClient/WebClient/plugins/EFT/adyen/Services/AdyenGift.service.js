/**
 * Primary service for the Adyen Card processing
 */
window.angular
  .module('prismApp.plugin.services.AdyenGift', [])
  .factory('AdyenGift', [
    'Adyen',
    'AdyenData',
    '$q',
    '$uibModal',
    '$uibModalStack',
    '$state',
    '$translate',
    'PrismUtilities',
    'NotificationService',
    'ModelService',
    'prismSessionInfo',
    'Transaction',
    'Toast',
    'ExternalDataService',
    '$filter',
    function(
      Adyen,
      AdyenData,
      $q,
      $uibModal,
      $uibModalStack,
      $state,
      $translate,
      PrismUtilities,
      NotificationService,
      ModelService,
      prismSessionInfo,
      Transaction,
      Toast,
      ExternalDataService,
      $filter
    ) {
      'use strict';

      // JSON.parse(JSON.stringify used to copy data and break reference to parent.
      var prefs = prismSessionInfo.get().preferences;
      var sessionInfo = prismSessionInfo.get();

      var API = {
        Mode: '', // stores the current type of giftcard function being used
        /**
         * Initialize the Adyen Plugin and update the base currency
         * opens the gift card modal to begin processing
         */
        Initialize: function(mode) {
          var deferred = $q.defer();
          // Declare variables for future use

          this.Mode = mode;
          Adyen.Initialize().then(
            function(response) {
              if (response) {
                Adyen.UpdateCurrency().then(
                  function() {
                    var adyenGiftCardModalPromise = $uibModal.open({
                      backdrop: 'static',
                      keyboard: false,
                      template: '<adyen-gift-modal close="$ctrl.close" dismiss="$ctrl.dismiss"></adyen-gift-modal>',
                      controllerAs: '$ctrl',
                      controller: function() {
                        var $ctrl = this;
                        $ctrl.close = adyenGiftCardModalPromise.close;
                        $ctrl.dismiss = adyenGiftCardModalPromise.dismiss;
                      }
                    });
                    adyenGiftCardModalPromise.result.then(
                      function(response) {
                        deferred.resolve(response);
                      },
                      function(error) {
                        // this usually isnt an error, the error case is thrown when the modal is dismissed.
                        deferred.reject(error);
                      }
                    );
                  },
                  function(error) {
                    deferred.reject(error);
                  }
                );
              } else {
                Adyen.ProcessFailure(response);
              }
            },
            function(error) {
              deferred.reject(error);
            }
          );

          return deferred.promise;
        },
        /**
         * Core gift card function, used to Add Value to an already activated gift card
         */
        AddValue: function(params) {
          var deferred = $q.defer();
          // take a deep copy of the default payload
          var rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
          //Verify that the tender amount is not zero
          if (
            !ExternalDataService.scope.tender.amount ||
            parseFloat(ExternalDataService.scope.tender.amount) === 0
          ) {
            // Throw error if amount is zero
            //'Amount must be greater than 0', 'Missing Amount'// Untranslated values for the message
            Toast.Error('2401', '2400', null, true);
            // exit the plugin and cancel tender process
            //Adyen.CloseModals();
            deferred.reject();
            return deferred.promise;
          } else {
            AdyenData.GetCurrency().then(
              function() {
                Transaction.Tender.roundTender(
                  ExternalDataService.scope.tender,
                  document.transaction_total_amt
                ).then(function(resp) {
                  AdyenData.Tender.amount = resp.amount;
                  rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
                  rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
                    rawData.SaleToPOIRequest.MessageHeader.SaleID
                  );
                  rawData.SaleToPOIRequest.MessageHeader.MessageCategory =
                    'StoredValue';
                  rawData.SaleToPOIRequest.MessageHeader.POIID = JSON.parse(
                    JSON.stringify(AdyenData.Config.serialnumber)
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest = {};
                  rawData.SaleToPOIRequest.StoredValueRequest.SaleData = JSON.parse(
                    JSON.stringify(AdyenData.SaleData)
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData = [
                    { StoredValueTransactionType: 'Load' }
                  ];
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID = JSON.parse(
                    JSON.stringify(AdyenData.StoredValueAccountID)
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].ExpiryDate =
                    '';
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].ItemAmount = parseFloat(
                    AdyenData.Tender.amount
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].Currency = JSON.parse(
                    JSON.stringify(AdyenData.Tender.basecurrency.alphabeticcode)
                  );

                  if (params) {
                    rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.EntryMode = [
                      'Keyed'
                    ];
                    rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.StoredValueID =
                      params.cardnumber;
                    if (params.cardnumber !== '') {
                      rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.EntryMode = [
                        'Scanned'
                      ];
                      rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.StoredValueProvider = params.cardtype;
                    }
                  }
                  AdyenData.Processing = true;
                  Adyen.Post(rawData).then(
                    function(response) {
                      processResponse({
                        response: response,
                        type: 'addvalue'
                      }).then(
                        function(data) {
                          if (data.success) {
                            API.AddTenderToDocument({
                              data: data.data,
                              success: true
                            }).then(
                              function(data2) {
                                deferred.resolve({
                                  data: data2,
                                  success: true
                                });
                              },
                              function(error) {
                                Adyen.DisplayError(error).then(function() {
                                  deferred.resolve({
                                    data: error,
                                    success: false
                                  });
                                });
                              }
                            );
                          } else {
                            deferred.resolve({
                              data: data.data,
                              success: false
                            });
                          }
                        },
                        function(error) {
                          Adyen.DisplayError(error).then(function() {
                            deferred.resolve({ data: error, success: false });
                          });
                        }
                      );
                    },
                    function(error) {
                      Adyen.DisplayError(error).then(function() {
                        deferred.resolve({ data: error, success: false });
                      });
                    }
                  );
                });
              },
              function(error) {
                Adyen.DisplayError(error).then(function() {
                  deferred.resolve({ data: error, success: false });
                });
              }
            );
          }
          return deferred.promise;
        },
        /**
         * Core gift card function, used to Activate a new gift card (with or without a balance)
         */
        ActivateCard: function(params) {
          var deferred = $q.defer();
          // take a deep copy of the default payload
          var rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
          //Verify that the tender amount is not zero
          if (
            !ExternalDataService.scope.tender.amount ||
            parseFloat(ExternalDataService.scope.tender.amount) === 0
          ) {
            // Throw error if amount is zero
            //'Amount must be greater than 0', 'Missing Amount'// Untranslated values for the message
            Toast.Error('2401', '2400', null, true);
            // exit the plugin and cancel tender process
            //Adyen.CloseModals();
            deferred.reject();
            return deferred.promise;
          } else {
            AdyenData.GetCurrency().then(
              function() {
                Transaction.Tender.roundTender(
                  ExternalDataService.scope.tender,
                  document.transaction_total_amt
                ).then(function(resp) {
                  AdyenData.Tender.amount = resp.amount;
                  rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
                  rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
                    rawData.SaleToPOIRequest.MessageHeader.SaleID
                  );
                  rawData.SaleToPOIRequest.MessageHeader.MessageCategory =
                    'StoredValue';
                  rawData.SaleToPOIRequest.MessageHeader.POIID =
                    AdyenData.Config.serialnumber;
                  rawData.SaleToPOIRequest.StoredValueRequest = {};
                  rawData.SaleToPOIRequest.StoredValueRequest.SaleData = JSON.parse(
                    JSON.stringify(AdyenData.SaleData)
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData = [
                    { StoredValueTransactionType: 'Activate' }
                  ];
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID = JSON.parse(
                    JSON.stringify(AdyenData.StoredValueAccountID)
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].ItemAmount = parseFloat(
                    AdyenData.Tender.amount
                  );
                  rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].Currency = JSON.parse(
                    JSON.stringify(AdyenData.Tender.basecurrency.alphabeticcode)
                  );

                  if (params) {
                    rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.EntryMode = [
                      'Keyed'
                    ];
                    rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.StoredValueID =
                      params.cardnumber;
                    if (params.cardnumber !== '') {
                      rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.EntryMode = [
                        'Scanned'
                      ];
                      rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.StoredValueProvider = params.cardtype;
                    }
                  }
                  AdyenData.Processing = true;
                  Adyen.Post(rawData).then(
                    function(response) {
                      AdyenData.Processing = false;
                      processResponse({
                        response: response,
                        type: 'activate'
                      }).then(
                        function(data) {
                          if (data.success) {
                            API.AddTenderToDocument(data).then(
                              function(response) {
                                // Reload page to show added tenders
                                ExternalDataService.scope.init();
                                deferred.resolve({ success: true });
                              },
                              function(error) {
                                Adyen.DisplayError(error).then(function() {
                                  deferred.resolve({
                                    data: error,
                                    success: false
                                  });
                                });
                              }
                            );
                          } else {
                            deferred.resolve({ success: false });
                          }
                        },
                        function(error) {
                          Adyen.DisplayError(error).then(function() {
                            deferred.resolve({ data: error, success: false });
                          });
                        }
                      );
                    },
                    function(error) {
                      Adyen.DisplayError(error).then(function() {
                        deferred.resolve({ data: error, success: false });
                      });
                    }
                  );
                });
              },
              function(error) {
                Adyen.DisplayError(error).then(function() {
                  deferred.resolve({ data: error, success: false });
                });
              }
            );
          }
          return deferred.promise;
        },
        /**
         * Core gift card function, used to Deactivate a gift card.
         * (once deactivated the card can never be used again)
         * This function has had the least testing of the others, currently must be
         * done from the admin console.
         */
        DeactivateCard: function(params) {
          var deferred = $q.defer();
          // take a deep copy of the default payload
          var rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
          AdyenData.GetCurrency().then(function() {
            rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
            rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
              rawData.SaleToPOIRequest.MessageHeader.SaleID
            );
            rawData.SaleToPOIRequest.MessageHeader.MessageCategory =
              'StoredValue';
            rawData.SaleToPOIRequest.MessageHeader.POIID = JSON.parse(
              JSON.stringify(AdyenData.Config.serialnumber)
            );
            rawData.SaleToPOIRequest.StoredValueRequest = {};
            rawData.SaleToPOIRequest.StoredValueRequest.SaleData = JSON.parse(
              JSON.stringify(AdyenData.SaleData)
            );
            rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData = [
              { StoredValueTransactionType: 'Unload' }
            ];
            rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID = JSON.parse(
              JSON.stringify(AdyenData.StoredValueAccountID)
            );
            rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].ItemAmount = 0;
            rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].Currency = JSON.parse(
              JSON.stringify(AdyenData.Tender.basecurrency.alphabeticcode)
            );

            if (params) {
              rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.EntryMode = [
                'Keyed'
              ];
              rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.StoredValueID =
                params.cardnumber;
              if (params.cardnumber !== '') {
                rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.EntryMode = [
                  'Scanned'
                ];
                rawData.SaleToPOIRequest.StoredValueRequest.StoredValueData[0].StoredValueAccountID.StoredValueProvider = params.cardtype;
              }
            }
            AdyenData.Processing = true;
            Adyen.Post(rawData).then(
              function(response) {
                AdyenData.Processing = false;
                processResponse({
                  response: response,
                  type: 'deactivate'
                }).then(
                  function(data) {
                    // API.AddTenderToDocument().then(function (response) {
                    //   deferred.resolve(data);
                    //   $state.go($state.current, {}, {reload: true});
                    // }, function (error) {
                    //   deferred.reject(error);
                    // });
                  },
                  function(error) {
                    deferred.reject(error);
                  }
                );
              },
              function(error) {
                deferred.reject(error);
              }
            );
          });

          return deferred.promise;
        },
        /**
         * Core gift card function, used to retrieve the current balance for the gift card displays
         * balance both on PED and in Prism.
         */
        GetBalance: function(params) {
          var deferred = $q.defer();
          // take a deep copy of the default payload
          var rawData = JSON.parse(JSON.stringify(AdyenData.RawData));
          rawData.SaleToPOIRequest.MessageHeader.MessageCategory =
            'BalanceInquiry';
          rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
          rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
            rawData.SaleToPOIRequest.MessageHeader.SaleID
          );
          rawData.SaleToPOIRequest.MessageHeader.POIID = JSON.parse(
            JSON.stringify(AdyenData.Config.serialnumber)
          );
          rawData.SaleToPOIRequest.BalanceInquiryRequest = {};
          rawData.SaleToPOIRequest.BalanceInquiryRequest.PaymentAccountReq = JSON.parse(
            JSON.stringify(AdyenData.PaymentAccountReq)
          );
          rawData.SaleToPOIRequest.BalanceInquiryRequest.PaymentAccountReq.PaymentInstrumentData.StoredValueAccountID = JSON.parse(
            JSON.stringify(AdyenData.StoredValueAccountID)
          );
          if (params) {
            rawData.SaleToPOIRequest.BalanceInquiryRequest.PaymentAccountReq.PaymentInstrumentData.StoredValueAccountID.EntryMode = [
              'Keyed'
            ];
            rawData.SaleToPOIRequest.BalanceInquiryRequest.PaymentAccountReq.PaymentInstrumentData.StoredValueAccountID.StoredValueID =
              params.cardnumber;
            if (params.cardnumber !== '') {
              rawData.SaleToPOIRequest.BalanceInquiryRequest.PaymentAccountReq.PaymentInstrumentData.StoredValueAccountID.EntryMode = [
                'Scanned'
              ];
              rawData.SaleToPOIRequest.BalanceInquiryRequest.PaymentAccountReq.PaymentInstrumentData.StoredValueAccountID.StoredValueProvider = params.cardtype;
            }
          }
          AdyenData.Processing = true;
          Adyen.Post(rawData).then(
            function(response) {
              AdyenData.Processing = false;
              processResponse({ response: response, type: 'balance' }).then(
                function(data) {
                  deferred.resolve(data);
                }
              );
            },
            function(error) {
              deferred.reject(error);
            }
          );

          return deferred.promise;
        },
        /**
         * Core gift card function, used to remove balance from a gift card to use as a Tender
         */
        ProcessPayment: function(params) {
          var deferred = $q.defer();
          // take a deep copy of the default payload
          var rawData = JSON.parse(JSON.stringify(AdyenData.RawData));

          AdyenData.GetCurrency().then(function() {
            Transaction.Tender.roundTender(
              ExternalDataService.scope.tender,
              document.transaction_total_amt
            ).then(function(resp) {
              AdyenData.Tender.amount = resp.amount;
              rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID();
              rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
                rawData.SaleToPOIRequest.MessageHeader.SaleID
              );
              rawData.SaleToPOIRequest.MessageHeader.MessageCategory =
                'Payment';
              rawData.SaleToPOIRequest.MessageHeader.POIID = JSON.parse(
                JSON.stringify(AdyenData.Config.serialnumber)
              );
              rawData.SaleToPOIRequest.PaymentRequest = {};
              rawData.SaleToPOIRequest.PaymentRequest.SaleData = JSON.parse(
                JSON.stringify(AdyenData.SaleData)
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction = JSON.parse(
                JSON.stringify(AdyenData.PaymentTransaction)
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction.AmountsReq.Currency = JSON.parse(
                JSON.stringify(AdyenData.Tender.basecurrency.alphabeticcode)
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentTransaction.AmountsReq.RequestedAmount = parseFloat(
                AdyenData.Tender.amount
              );
              rawData.SaleToPOIRequest.PaymentRequest.PaymentData = JSON.parse(
                JSON.stringify(AdyenData.PaymentData)
              );

              if (params) {
                rawData.SaleToPOIRequest.PaymentRequest.PaymentData.PaymentInstrumentData.StoredValueAccountID.EntryMode = [
                  'Keyed'
                ];
                rawData.SaleToPOIRequest.PaymentRequest.PaymentData.PaymentInstrumentData.StoredValueAccountID.StoredValueID =
                  params.cardnumber;
                if (params.cardnumber !== '') {
                  rawData.SaleToPOIRequest.PaymentRequest.PaymentData.PaymentInstrumentData.StoredValueAccountID.EntryMode = [
                    'Scanned'
                  ];
                  rawData.SaleToPOIRequest.PaymentRequest.PaymentData.PaymentInstrumentData.StoredValueAccountID.StoredValueProvider = params.cardtype;
                }
              }

              AdyenData.SetTenderOptions().then(function(data) {
                if (data) {
                  rawData.SaleToPOIRequest.PaymentRequest.SaleData.SaleToAcquirerData += data;
                }
                AdyenData.RequestPayload = rawData;
                AdyenData.Processing = true;
                Adyen.Post(rawData).then(
                  function(response) {
                    AdyenData.Processing = false;
                    processResponse({
                      response: response,
                      type: API.Mode
                    }).then(
                      function(data1) {
                        if (data1.success) {
                          API.AddTenderToDocument(data1).then(
                            function(data2) {
                              deferred.resolve({ success: true });
                            },
                            function(error) {
                              Adyen.DisplayError(error).then(function() {
                                deferred.resolve({
                                  data: error,
                                  success: false
                                });
                              });
                            }
                          );
                        } else {
                          deferred.resolve({ success: false });
                        }
                      },
                      function(error) {
                        Adyen.DisplayError(error).then(function() {
                          deferred.resolve({ success: false });
                        });
                      }
                    );
                  },
                  function(error) {
                    Adyen.DisplayError(error).then(function() {
                      deferred.resolve({ success: false });
                    });
                  }
                );
              });
            });
          });
          return deferred.promise;
        },
        /**
         * Manual entry processign header, needs to keep track of type of gift card action
         * being performed so that it can continue the same processing after reciving the
         * response from the PED.
         */
        ManualEntry: function(params) {
          var deferred = $q.defer();
          //waiting on cancel functionality.
          // var params = { cardnumber: '' };
          // if (cardnumber) {
          //   params.cardnumber = cardnumber;
          // }
          switch (API.Mode) {
            case 'deactivate':
              API.DeactivateCard(params).then(
                function(response) {
                  deferred.resolve(response);
                },
                function(error) {
                  Adyen.ProcessFailure(error).then(
                    function(response) {
                      deferred.resolve(response); // close the notification modal, reject and void
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                }
              );
              break;
            case 'activate':
              API.ActivateCard(params).then(
                function(response) {
                  deferred.resolve(response);
                },
                function(error) {
                  Adyen.ProcessFailure(error).then(
                    function(response) {
                      deferred.resolve(response); // close the notification modal, reject and void
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                }
              );
              break;
            case 'addvalue':
              API.AddValue(params).then(
                function(response) {
                  deferred.resolve(response);
                },
                function(error) {
                  Adyen.ProcessFailure(error).then(
                    function(response) {
                      deferred.resolve(response); // close the notification modal, reject and void
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                }
              );
              break;
            case 'payment':
              API.ProcessPayment(params).then(
                function(response) {
                  deferred.resolve(response);
                },
                function(error) {
                  Adyen.ProcessFailure(error).then(
                    function(response) {
                      deferred.resolve(response); // close the notification modal, reject and void
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                }
              );
              break;
            case 'balance':
            default:
              API.GetBalance(params).then(
                function(response) {
                  deferred.resolve(response);
                },
                function(error) {
                  Adyen.ProcessFailure(error).then(
                    function(response) {
                      deferred.resolve(response); // close the notification modal, reject and void
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                }
              );
              break;
          }

          // ShopperDisplay.Redraw(); // redraw shopper display
          return deferred.promise;
        },
        /**
         * Adds new tender to document based on response from PED
         */
        AddTenderToDocument: function(response) {
          var deferred = $q.defer();
          var tenderType = AdyenData.Tender.type;
          if (response.data) {
            var payment = angular.fromJson(response.data),
              newTender = ModelService.create('Tender'),
              paymentResponse,
              cardNumber,
              amtResp,
              transID,
              storedValue,
              addResponse;

            if (payment && payment.SaleToPOIResponse) {
              switch (this.Mode) {
                case 'addvalue':
                case 'activate':
                  storedValue = true;
                  paymentResponse =
                    payment.SaleToPOIResponse.StoredValueResponse;
                  addResponse = AdyenData.decodeText(
                    paymentResponse.Response.AdditionalResponse
                  );
                  newTender.given =
                    paymentResponse.StoredValueResult[0].ItemAmount;
                  newTender.alphabetic_code =
                    paymentResponse.StoredValueResult[0].Currency;
                  newTender.tender_name = addResponse.cardType;
                  if (
                    paymentResponse.StoredValueResult[0]
                      .StoredValueAccountStatus.StoredValueAccountID
                      .StoredValueID !== ''
                  ) {
                    cardNumber =
                      paymentResponse.StoredValueResult[0]
                        .StoredValueAccountStatus.StoredValueAccountID
                        .StoredValueID;
                  } else {
                    // If there is no stored value ID (card number) returned, use the "Additional Response" field to retrieve one.
                    cardNumber = addResponse.giftcardPAN;
                  }

                  break;
                default:
                  storedValue = false;
                  paymentResponse = payment.SaleToPOIResponse.PaymentResponse;
                  addResponse = AdyenData.decodeText(
                    paymentResponse.Response.AdditionalResponse
                  );
                  amtResp = paymentResponse.PaymentResult.AmountsResp;
                  cardNumber = paymentResponse.PaymentResult.PaymentInstrumentData.StoredValueAccountID.StoredValueID;
                  newTender.tender_name = addResponse.cardType;
                  newTender.alphabetic_code = amtResp.Currency;
                  if (AdyenData.Tender.mode.toLowerCase() === 'take') {
                    // set taken or given based on return mode
                    newTender.taken = amtResp.AuthorizedAmount;
                  } else {
                    newTender.given = amtResp.AuthorizedAmount;
                  }
                  break;
              }

              transID = paymentResponse.POIData.POITransactionID;

              newTender.tender_type = tenderType; // set tender type to 2 for Credit
              newTender.document_sid = AdyenData.DocSid; // set tender document sid
              newTender.card_number = (cardNumber)?cardNumber.substring(cardNumber.length - 4):addResponse.cardSummary; // set card_number to last 6 digits of returned cardnumber
              newTender.eft_transaction_id =
                payment.SaleToPOIResponse.MessageHeader.ServiceID; // set eft_transaction_id based on return token
              newTender.eftdata0 = transID.TransactionID;
              newTender.eftdata9 =
                  paymentResponse.POIData.POITransactionID.TimeStamp;
              newTender.eftdata10 =
                paymentResponse.POIData.POITransactionID.TransactionID;
              newTender.eftdata16 =
                payment.SaleToPOIResponse.MessageHeader.POIID;
              newTender.eftdata17 =
                payment.SaleToPOIResponse.MessageHeader.ServiceID;

              if (paymentResponse.PaymentReceipt[0].OutputContent.OutputText) {
                AdyenData.parseEMV(
                  paymentResponse.PaymentReceipt[0].OutputContent.OutputText
                ).then(
                  function() {
                    if (paymentResponse.PaymentReceipt[0]) {
                      AdyenData.ProcessReceiptData(
                        paymentResponse.PaymentReceipt[0].OutputContent
                          .OutputText
                      ).then(
                        function(mer) {
                          newTender.eftdatabsmer = mer;
                          AdyenData.ProcessReceiptData(
                            paymentResponse.PaymentReceipt[1].OutputContent
                              .OutputText
                          ).then(
                            function(cust) {
                              newTender.eftdatabscust = cust;
                              if (AdyenData.EMVData) {
                                newTender.emv_ai_aid =
                                  AdyenData.EMVData.emv_ai_aid;
                                newTender.emv_ai_applabel =
                                  AdyenData.EMVData.emv_ai_applabel;
                                newTender.eftdata1 = AdyenData.EMVData.eftdata1;
                                newTender.emv_pinstatement =
                                  AdyenData.EMVData.emv_pinstatement;
                                newTender.authorization_code =
                                  AdyenData.EMVData.authorization_code;
                                newTender.eftdata3 =
                                  AdyenData.EMVData.authorization_code;
                              }

                              if (
                                !storedValue &&
                                paymentResponse.PaymentResult.CurrencyConversion
                              ) {
                                var conv =
                                  paymentResponse.PaymentResult
                                    .CurrencyConversion[0].ConvertedAmount;
                                AdyenData.GetCurrency(conv.Currency).then(
                                  function(data) {
                                    var currency = $filter('foreign_currency')(
                                      conv.AmountValue,
                                      AdyenData.Tender.basecurrency
                                    );
                                    newTender.eftdata0 =
                                      paymentResponse.PaymentResult.CurrencyConversion;
                                    newTender.eftdata18 = conv.Currency;
                                    newTender.eftdata19 = currency;
                                    finalizeTender(newTender).then(function() {
                                      deferred.resolve();
                                    });
                                    // title: Non-Local Currency, message: Customer paid with a non-local currency
                                    Toast.Warning(
                                      '6651',
                                      $translate.instant('6652') +
                                        ' ' +
                                        currency,
                                      null,
                                      false
                                    );
                                  },
                                  function(error) {
                                    deferred.resolve(error);
                                  }
                                );
                              } else {
                                finalizeTender(newTender).then(function() {
                                  deferred.resolve();
                                });
                              }
                            },
                            function(error) {
                              deferred.resolve(error);
                            }
                          );
                        },
                        function(error) {
                          deferred.resolve(error);
                        }
                      );
                    }
                  },
                  function(error) {
                    deferred.resolve(error);
                  }
                );
              } else {

                // for LOGGING purposes
                // newTender.eftdata0 = {
                //   request: AdyenData.RequestPayload,
                //   response: payment
                // };

                if (
                  !storedValue &&
                  paymentResponse.PaymentResult.CurrencyConversion
                ) {
                  var conv =
                    paymentResponse.PaymentResult.CurrencyConversion[0]
                      .ConvertedAmount;
                  AdyenData.GetCurrency(conv.Currency).then(
                    function(data) {
                      var currency = $filter('foreign_currency')(
                        conv.AmountValue,
                        AdyenData.Tender.basecurrency
                      );
                      newTender.eftdata0 =
                        paymentResponse.PaymentResult.CurrencyConversion;
                      newTender.eftdata18 = conv.Currency;
                      newTender.eftdata19 = currency;
                      finalizeTender(newTender).then(function() {
                        deferred.resolve();
                      });
                      // title: Non-Local Currency, message: Customer paid with a non-local currency
                      Toast.Warning(
                        '6651',
                        $translate.instant('6652') + ' ' + currency,
                        null,
                        false
                      );
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                } else {
                  finalizeTender(newTender).then(function() {
                    deferred.resolve();
                  });
                }
              }
            } else {
              deferred.resolve({ method: 'cancel' }); // close the notification modal, reject and void
            }
          } else {
            deferred.resolve('failure');
          }

          return deferred.promise;
        }
      };

      /**
       * Finalizes adding tender to document, uses tender screens scope to finalize the document if the
       * preference is enabled to update balanced documents automatically
       * function extracted to reduce duplication
       */
      var finalizeTender = function(newTender) {
        var deferred = $q.defer();
        Transaction.Document.active
          .addTender(newTender)
          .then(function(document) {
            // Reload page to show added tenders
            ExternalDataService.scope.init();
            deferred.resolve({ method: 'success' }); // close the notification modal, reject and void
          });
        return deferred.promise;
      };

      /**
       * Handles initial response from PED and returns the proper response data
       * based on the type of gift card action.
       * function extracted to reduce duplication
       */
      var processResponse = function(obj) {
        var deferred = $q.defer();
        var response = angular.fromJson(obj.response.data),
          data;
        if (response.SaleToPOIResponse) {
          switch (obj.type) {
            case 'addvalue':
            case 'activate':
            case 'deactivate':
              data = response.SaleToPOIResponse.StoredValueResponse;
              break;
            case 'payment':
              data = response.SaleToPOIResponse.PaymentResponse;
              break;
            case 'balance':
            default:
              data = response.SaleToPOIResponse.BalanceInquiryResponse;
              break;
          }
          if (data && data.Response.Result !== 'Failure') {
            deferred.resolve({ data: response, success: true });
          } else {
            Adyen.ProcessFailure(data).then(
              function(response) {
                deferred.resolve({ data: response, success: false });
              },
              function(error) {
                deferred.reject(error);
              }
            );
          }
        } else {
          Adyen.ProcessFailure(response).then(
            function() {
              deferred.reject();
            },
            function(error) {
              deferred.reject(error);
            }
          );
        }
        return deferred.promise;
      };

      return API;
    }
  ]);
