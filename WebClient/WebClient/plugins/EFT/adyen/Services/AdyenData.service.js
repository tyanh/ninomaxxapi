/**
 * Primary service for the Adyen Data processing
 * defines default data, and data related functions
 */
window.angular
  .module('prismApp.plugin.services.AdyenData', [])
  .factory('AdyenData', [
    '$http',
    '$q',
    '$stateParams',
    '$state',
    '$translate',
    'prismSessionInfo',
    'DocumentPersistedData',
    'PrismUtilities',
    'Toast',
    'Transaction',
    'ModelService',
    'ModelService2',
    'ExternalDataService',
    'TenderTypes',
    function(
      $http,
      $q,
      $stateParams,
      $state,
      $translate,
      prismSessionInfo,
      DocumentPersistedData,
      PrismUtilities,
      Toast,
      Transaction,
      ModelService,
      ModelService2,
      ExternalDataService,
      TenderTypes
    ) {
      'use strict';

      var sessionInfo = prismSessionInfo.get();
      var prefs = prismSessionInfo.get().preferences;
      var adyenData = sessionStorage.getItem('AdyenData');

      /**
       * Used to properly format strings for printing by adding spaces to fill each row
       */
      function loopVal(value) {
        var retVal = '';
        if (value) {
          var center = (31 - value.length) / 2;
          for (var r = 0; r < center; r++) {
            retVal += ' ';
          }
          retVal += value;
          for (var s = 0; s < center; s++) {
            retVal += ' ';
          }
          retVal += '\\r\\n';
        }
        return retVal;
      }
      /**
       * Gets currency information for provided code
       */
      function getCurrency(code) {
        var deferred = $q.defer();
        ModelService2.get('Currency2', {
          cols: '*',
          filter: 'alphabeticcode,eq,' + code
        }).then(
          function(data) {
            API.Tender.basecurrency = data[0];
            API.PaymentTransaction.AmountsReq.Currency =
              API.Tender.basecurrency.alphabeticcode;
            if (
              ExternalDataService.scope &&
              ExternalDataService.scope.tender &&
              ExternalDataService.scope.tender.amount !== 0
            ) {
              API.Tender.amount = ExternalDataService.scope.tender.amount;
              API.PaymentTransaction.AmountsReq.RequestedAmount = parseFloat(
                ExternalDataService.scope.tender.amount
              );
            }
            deferred.resolve();
          },
          function(error) {
            deferred.reject(error);
          }
        );
        return deferred.promise;
      }

      var API = {
        DocSid: '',
        Document: null,
        Config: {},
        RequestPayload: {},
        RefundTenders: [],
        LastTender: {},
        EMVData: {},
        StoreData: {},
        CurrentServiceID: null,
        CurrentSaleID: null,
        MKE: false,
        Processing: false,
        /**
         * Initialize the default data and values for the API raw data
         * The Payloads sent to the PED are always entirely different data structures
         * so the payloads are built dynamically based on what type of transaction is
         * being done, always get a deep copy of this information ie JSON.parse(JSON.stringify(value))
         * to prevent the base information from changing during the transaction process
         */
        Initialize: function() {
          var deferred = $q.defer();
          // Initializing Nested Values
          API.LastTender = {};
          // use sid of active document in service or state params if not present
          if (Transaction.Document.active) {
            API.DocSid = Transaction.Document.active.sid;
          } else {
            API.DocSid = $stateParams.document_sid;
          }
          ModelService.get('Store', {
            sid: prismSessionInfo.get().storesid,
            cols:
              'address1,address2,address3,address4,address5,phone1,store_name,store_code,store_number,zip' // specify the columns to return from the DB
          }).then(
            function(data2) {
              API.StoreData = data2[0];
              API.SaleData.SaleTransactionID.TransactionID = API.DocSid ? API.DocSid
                : API.GetServiceID();
              API.StoredValue.StoredValueAccountID = JSON.parse(
                JSON.stringify(API.StoredValueAccountID)
              );
              API.PaymentInstrumentData.StoredValueAccountID = JSON.parse(
                JSON.stringify(API.StoredValueAccountID)
              );
              API.PaymentData.PaymentInstrumentData = JSON.parse(
                JSON.stringify(API.PaymentInstrumentData)
              );
              API.PaymentAccountReq.PaymentInstrumentData = JSON.parse(
                JSON.stringify(API.PaymentInstrumentData)
              );
              if (
                ExternalDataService.scope &&
                ExternalDataService.scope.tender
              ) {
                API.Tender.amount = ExternalDataService.scope.tender.amount;
                API.Tender.mode = ExternalDataService.scope.tender.mode;
                API.Tender.type = TenderTypes.getTenderPosition(
                  ExternalDataService.scope.tender.tenderType
                );
              }

              API.GetConfig().then(
                function(response) {
                  //verify that adyen device is configured and fetch its serial #
                  if (response) {
                    //API.StoredValueAccountID.StoredValueProvider =
                      //(API.Config.adyenconfig.cardtype.toString() === '-1')?'':API.Config.adyenconfig.cardtype;
                    sessionStorage.setItem('AdyenData', JSON.stringify(API));
                    deferred.resolve(response);
                  } else {
                    Toast.Error('1185', '6648', null, true);
                    deferred.reject(response);
                  }
                },
                function(error) {
                  deferred.reject(error);
                }
              );
            },
            function(error) {
              deferred.reject(error);
            }
          );

          return deferred.promise;
        },
        Tender: {
          amount: 0, // Tender amount from tender screen
          mode: 'take', // Tender Mode (take/give)
          type: 0, // Tender type (as integer)
          forceCreditAuth: false,
          basecurrency: {}
        },
        RawData: {
          SaleToPOIRequest: {
            MessageHeader: {
              ProtocolVersion: '3.0',
              MessageClass: 'Service',
              MessageCategory: 'Payment',
              MessageType: 'Request',
              SaleID: sessionInfo.workstation.substring(0, 30) // Adyen verified that sale ID can be at least 40 characters so limiting length sent to prevent potential errors
            }
          }
        },
        SaleData: {
          SaleTransactionID: {
            TransactionID: '',
            TimeStamp: new Date()
          },
          SaleToAcquirerData: 'applicationInfo.merchantApplication.name=Prism Adyen Link&applicationInfo.merchantApplication.version=&applicationInfo.externalPlatform.integrator=Retail Pro&applicationInfo.externalPlatform.name=Retail Pro Prism&applicationInfo.externalPlatform.version=&applicationInfo.merchantDevice.os=&applicationInfo.merchantDevice.osVersion=&applicationInfo.merchantDevice.reference=&'
        },
        PaymentTransaction: {
          AmountsReq: {
            Currency: 'USD',
            RequestedAmount: 0
          }
        },
        PaymentData: {
          PaymentInstrumentData: {}
        },
        StoredValue: {
          StoredValueAccountID: {}
        },
        StoredValueAccountID: {
          EntryMode: ['MagStripe'],
          IdentificationType: 'PAN',
          StoredValueAccountType: 'GiftCard',
          StoredValueProvider: '',
          StoredValueID: ''
        },
        PaymentAccountReq: {
          PaymentInstrumentData: {}
        },
        PaymentInstrumentData: {
          PaymentInstrumentType: 'StoredValue',
          StoredValueAccountID: {}
        },
        /**
         * Get the configuration data from the Proxy
         */
        GetConfig: function() {
          var deferred = $q.defer();
          if (!API.Config.hasOwnProperty('adyenconfig')) {
            $http.get('/v1/rest/adyen').then(
              function(response) {
                if (response) {
                  API.Config = PrismUtilities.responseParser(response.data);
                  deferred.resolve(true);
                } else {
                  // title: Error, message: Serial Number has not been configured
                  Toast.Error('1185', '6647', null, true);
                  deferred.resolve(false);
                }
              },
              function(error) {
                deferred.reject(error);
              }
            );
          } else {
            deferred.resolve(true);
          }
          return deferred.promise;
        },
        /**
         * Generates a random number or alphanumeric key and sets the value for the current ServiceID and SaleID
         * returns a string
         */
        GetServiceID: function(numberOnly, workstation) {
          var serviceID;
          if (numberOnly) {
            serviceID =
              Math.floor(1000000000 + Math.random() * 9000000000) + '';
            if (workstation) {
              API.CurrentSaleID = workstation + serviceID;
            } else {
              API.CurrentServiceID = serviceID;
            }
          } else {
            serviceID = Math.random()
              .toString(36)
              .replace('0.', '')
              .substring(0, 9);
            if (workstation) {
              API.CurrentSaleID = workstation + serviceID;
            } else {
              API.CurrentServiceID = serviceID;
            }
          }
          return serviceID + '';
        },
        GetSaleID: function(workstation) {
          if (API.Config.randomizesaleid) {
            return workstation + '-' + API.GetServiceID(false, workstation);
          } else {
            return workstation;
          }
        },
        /**
         * Sets the tender options value based on the adyen configuration
         */
        SetTenderOptions: function() {
          var deferred = $q.defer(),
            useTenderOptions = false,
            tenderOption = 'tenderOption=';
          if (API.Config.adyenconfig.allowpartialauthorizations) {
            useTenderOptions = true;
            tenderOption += 'AllowPartialAuthorisation';
          }
          if (API.Config.adyenconfig.askgratuity) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'AskGratuity';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.bypasspin) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'BypassPin';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.dontprintreceipt) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'DontPrintReceipt';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.enablemagstripefallback) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'EnableMagstripeFallback';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.forceddecline) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'ForcedDecline';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.forcedonline) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'ForcedOnline';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.getadditionaldata) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'GetAdditionalData';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.keyedentry) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'KeyedEntry';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.moto) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'Keyed';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.noctls) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'NoCTLS';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.receipthandler) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'ReceiptHandler';
            useTenderOptions = true;
          }
          if (API.Config.adyenconfig.skipaidpriority) {
            if (useTenderOptions) {
              tenderOption += ',';
            }
            tenderOption += 'SkipAIDPriority';
            useTenderOptions = true;
          }
          if (useTenderOptions) {
            deferred.resolve(tenderOption);
          } else {
            deferred.resolve(null);
          }
          return deferred.promise;
        },
        /**
         * Decodes the text contained in the AdditionalResponse field that they send essentially converting
         * &key=value into key value pairs, returns a JSON object
         */
        decodeText: function(string) {
          var g = string
            .replace('%3d', '->')
            .replace('%0c', '')
            .replace('%c2%a0', ' ');
          var str =
            '{"' +
            decodeURIComponent(g)
              .replace(/"/g, '\\"')
              .replace(/&/g, '","')
              .replace(/=/g, '":"') +
            '"}';
          return JSON.parse(str);
        },
        /**
         * Gets the currency information for the provided alphabetic code and sets the base currency
         * property in the AdyenData service
         *
         */
        GetCurrency: function(code) {
          var deferred = $q.defer();
          if (code) {
            getCurrency(code).then(
              function() {
                deferred.resolve();
              },
              function(error) {
                deferred.reject(error);
              }
            );
          } else {
            ModelService.get('Subsidiary', {
              sid: sessionInfo.subsidiarysid,
              cols: 'base_currency_sid,currency_code'
            }).then(
              function(data) {
                var bc = PrismUtilities.responseParser(data);
                if (bc[0]) {
                  getCurrency(bc[0].currency_code).then(
                    function() {
                      deferred.resolve();
                    },
                    function(error) {
                      deferred.reject(error);
                    }
                  );
                } else {
                  deferred.resolve();
                }
              },
              function(error) {
                deferred.reject(error);
              }
            );
          }
          return deferred.promise;
        },
        /**
         * Parses the EMV data from the receipts and stores for future use.
         */
        parseEMV: function(data) {
          var deferred = $q.defer();
          // Loop through the merchant receipt to retrieve EMV data.
          for (var i = 0; i < data.length; i++) {
            // decode the text fields to get relevant data
            var message = API.decodeText(data[i].Text);
            switch (message.key) {
              case 'header1':
                API.EMVData.header1 = message.value;
                break;
              case 'header2':
                API.EMVData.header2 = message.value;
                break;
              case 'totalAmount':
                API.EMVData.totalAmount = message.value;
                break;
              case 'accountType':
                API.EMVData.accountType = message.value;
                break;
              case 'aid':
                API.EMVData.emv_ai_aid = message.value;
                break;
              case 'cardType':
                API.EMVData.emv_ai_applabel = message.value;
                API.EMVData.eftdata1 = message.value;
                break;
              case 'cvmRes':
                API.EMVData.emv_pinstatement = message.value;
                break;
              case 'authCode':
                API.EMVData.authorization_code = message.value;
                break;
              case 'preferredName':
                API.EMVData.preferredName = message.value;
                break;
              case 'mid':
                API.EMVData.mid = message.value;
                break;
              case 'tid':
                API.EMVData.tid = message.value;
                break;
              case 'ptid':
                API.EMVData.ptid = message.value;
                break;
              case 'panSeq':
                API.EMVData.panSeq = message.value;
                break;
              case 'paymentMethod':
                API.EMVData.paymentMethod = message.value;
                break;
              case 'paymentMethodVariant':
                API.EMVData.paymentMethodVariant = message.value;
                break;
              case 'pan':
                API.EMVData.pan = message.value;
                break;
              case 'txdate':
                API.EMVData.txdate = message.value;
                break;
              case 'txtime':
                API.EMVData.txtime = message.value;
                break;
              case 'posEntryMode':
                API.EMVData.posEntryMode = message.value;
                break;
              case 'atc':
                API.EMVData.atc = message.value;
                break;
              case 'txref':
                API.EMVData.txref = message.value;
                break;
              case 'mref':
                API.EMVData.mref = message.value;
                break;
              case 'txtype':
                API.EMVData.txtype = message.value;
                break;
              case 'approved':
                API.EMVData.approved = message.value;
                break;
              case 'retain':
                API.EMVData.retain = message.value;
                break;
              case 'thanks':
                API.EMVData.thanks = message.value;
                break;
              case 'cardHolderHeader':
                API.EMVData.cardHolderHeader = message.value;
                break;
              case 'merchantTitle':
                API.EMVData.merchantTitle = message.value;
                break;
              default:
                break;
            }
            if (i === data.length - 1) {
              deferred.resolve();
            }
          }

          return deferred.promise;
        },
        /**
         * Takes the string data receipt provided in the payload and reformats it into a string that
         * will be properly formatted by the print engine
         */
        ProcessReceiptData: function(data) {
          var deferred = $q.defer();
          var string = '\\r\\n';
          //Set up the payload of data to print
          string += loopVal(API.StoreData.store_name);
          string += loopVal(API.StoreData.store_number);
          string += loopVal(API.StoreData.store_code);
          string += loopVal(API.StoreData.address1);
          string += loopVal(API.StoreData.address2);
          string += loopVal(API.StoreData.address3);
          string += loopVal(API.StoreData.address4);
          string += loopVal(API.StoreData.address5);
          string += loopVal(API.StoreData.zip);
          string += loopVal(API.StoreData.phone1);
          for (var i = 0; i < data.length; i++) {
            // decode the text fields to get relevant data
            var message = API.decodeText(data[i].Text);
            if (message.value) {
              message.name = message.name ? message.name : ''; // if the name is undefined make it a blank string
              var spaces = 32 - (message.name.length + message.value.length);
              string += message.name;
              if (message.name !== '') {
                string += ':';
              }
              for (var q = 0; q < spaces; q++) {
                string += ' ';
              }
              string += message.value;
              string += '\\r\\n';
            } else if (message.name) {
              string += loopVal(message.name);
            } else {
              string += '\\r\\n';
            }
            if (i === data.length - 1) {
              deferred.resolve(string);
            }
          }

          return deferred.promise;
        }
        /**
         * Tried creating one AddTender function for all the modules, but had problems getting it to work
         * so both Credit and Gift have their own function.  Left here in case someone wants to get it working
         * to simplify the code
         */
        // AddTenderToDocument: function(response,tenderType){
        //   var deferred = $q.defer(),
        //     payment = angular.fromJson(response.data),
        //     newTender = ModelService.create('Tender'),
        //     paymentResponse, cardNumber, amtResp, transID;
        //
        //   if(payment && payment.SaleToPOIResponse) {
        //     if(this.Mode === 'addvalue'){
        //       paymentResponse = payment.SaleToPOIResponse.StoredValueResponse;
        //       newTender.given = paymentResponse.StoredValueResult[0].ItemAmount;
        //       newTender.alphabetic_code = paymentResponse.StoredValueResult[0].Currency;
        //       cardNumber = paymentResponse.StoredValueResult[0].StoredValueAccountStatus.StoredValueAccountID.StoredValueID;
        //     } else {
        //       paymentResponse = payment.SaleToPOIResponse.PaymentResponse;
        //       amtResp = paymentResponse.PaymentResult.AmountsResp;
        //       cardNumber = paymentResponse.PaymentResult.PaymentInstrumentData.StoredValueAccountID.StoredValueID;
        //       newTender.alphabetic_code = amtResp.Currency;
        //       if(API.Tender.mode.toLowerCase() === 'take'){ // set taken or given based on return mode
        //         newTender.taken = amtResp.AuthorizedAmount;
        //       } else {
        //         newTender.given = amtResp.AuthorizedAmount;
        //       }
        //     }
        //
        //     transID = paymentResponse.POIData.POITransactionID;
        //     newTender.tender_type = tenderType; // set tender type to 2 for Credit
        //     newTender.document_sid = API.DocSid; // set tender document sid
        //     newTender.card_number = cardNumber.substr(cardNumber.length - 4);  // set card_number to last 6 digits of returned cardnumber
        //     newTender.eft_transaction_id = payment.SaleToPOIResponse.MessageHeader.ServiceID; // set eft_transaction_id based on return token
        //     newTender.eftdata0 = transID.TransactionID;
        //     newTender.eftdatabsmer = paymentResponse.PaymentReceipt[0].OutputContent.OutputText;
        //     newTender.eftdatabscust = paymentResponse.PaymentReceipt[1].OutputContent.OutputText;
        //
        //     API.parseEMV(newTender.eftdatabsmer).then(function(){
        //       newTender.tender_name = API.EMVData.preferredName;
        //       newTender.emv_ai_aid = API.EMVData.emv_ai_aid;
        //       newTender.emv_ai_applabel = API.EMVData.emv_ai_applabel;
        //       newTender.eftdata1 =  API.EMVData.eftdata1;
        //       newTender.emv_pinstatement = API.EMVData.emv_pinstatement;
        //       newTender.authorization_code = API.EMVData.authorization_code;
        //       newTender.eftdata3 = API.EMVData.authorization_code;
        //
        //       newTender.eftdata9 =  paymentResponse.POIData.POITransactionID.TimeStamp;
        //       newTender.eftdata10 = paymentResponse.POIData.POITransactionID.TransactionID;
        //       newTender.eftdata16 = payment.SaleToPOIResponse.MessageHeader.POIID;
        //       newTender.eftdata17 = payment.SaleToPOIResponse.MessageHeader.ServiceID;
        //
        //
        //       // for LOGGING purposes
        //       newTender.eftdata0 = {
        //         request: API.RequestPayload,
        //         response: payment
        //       };
        //
        //
        //
        //       if(paymentResponse.PaymentResult.CurrencyConversion){
        //         var conv = paymentResponse.PaymentResult.CurrencyConversion[0].ConvertedAmount;
        //         API.GetCurrency(conv.Currency).then(function() {
        //           var currency = $filter('foreign_currency')(conv.AmountValue, API.Tender.basecurrency);
        //           newTender.eftdata0 = paymentResponse.PaymentResult.CurrencyConversion;
        //           newTender.eftdata18 = conv.Currency;
        //           newTender.eftdata19 = currency;
        //           // title: Non-Local Currency, message: Customer paid with a non-local currency
        //           Toast.Warning('6651',$translate.instant('6652')+' '+currency, null, false);
        //         },function(error){
        //           deferred.reject(error);
        //         });
        //       }
        //     }, function(error){
        //       deferred.reject(error);
        //     });
        //
        //
        //     Transaction.Document.active.addTender(newTender).then(function (document) { //add the tender to the document
        //       Transaction.Document.get(API.DocSid).then(function (data) {
        //         document = data;
        //         // Reload page to show added tenders
        //         deferred.resolve({'method': 'success'}); // close the notification modal, reject and void
        //         if ((document.due_amt === 0) && (prefs.automatically_print_update_balanced_document === 'true')) {
        //           ExternalDataService.scope.updateDocument(true);
        //         } else {
        //           $state.go($state.current, {}, {reload: true})
        //         }
        //       }, function (error) {
        //         deferred.resolve({'method': 'cancel'}); // close the notification modal, reject and void
        //       });
        //     });
        //   } else {
        //     deferred.resolve({'method': 'cancel'}); // close the notification modal, reject and void
        //   }
        //
        //   return deferred.promise;
        // }
      };

      // if browser has been reloaded, load data from session.
      if (adyenData) {
        var data = JSON.parse(adyenData);
        if (
          Transaction.Document.active &&
          Transaction.Document.active.sid !== $stateParams.document_sid
        ) {
          ModelService.get('Document', {
            sid: $stateParams.document_sid,
            cols: '*'
          }).then(function(resp) {
            Transaction.Document.active = resp[0];
            API.DocSid = $stateParams.document_sid;
          });
        } else {
          API.DocSid = $stateParams.document_sid;
        }
        API.Document = data.Document;
        API.Config = data.Config;
        API.CurrentServiceID = data.CurrentServiceID;
        API.Tender = data.Tender;
      }

      return API;
    }
  ]);
