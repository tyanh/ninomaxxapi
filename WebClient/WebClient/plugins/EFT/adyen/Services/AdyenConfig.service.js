/**
 * Primary service for the Adyen Configuration screen
 * defines default data, and saves data to the proxy HAL
 */
window.angular
  .module('prismApp.plugin.services.AdyenConfig', [])
  .factory('AdyenConfig', [
    '$q',
    '$http',
    '$uibModal',
    '$translate',
    'Adyen',
    'prismSessionInfo',
    'ModelService',
    'Toast',
    function(
      $q,
      $http,
      $uibModal,
      $translate,
      Adyen,
      prismSessionInfo,
      ModelService,
      Toast
    ) {
      'use strict';

      // default data to load on new configuration
      var blankExtension = {
          ipaddress: '127.0.0.1',
          ipport: 5551,
          asyncport: 5552,
          passphrase: '',
          keyidentifier: '',
          loglevel: 0,
          retainxlogs: 9,
          loggingbuffer: 0,
          livelogging: true,
          codesiteactive: true,
          debugstacktraceactive: true,
          endpoint: '',
          timeout: 20000,
          maxTimeout: 30000,
          deviceip: '',
          serialnumber: '',
          adyenconfig: {
            account: '',
            username: '',
            password: '',
            allowmagstripefallback: false,
            allowpartialauthorizations: false,
            currency: '',
            promptondifferentcurrency: false,
            cardtype: '',
            tenderoptions: '',
            applicationname: '',
            integratorname: '',
            applicationid: '',
            server: 0,
            overridetenderoptions: '',
            overrideautomaticappname: '',
            displayadyendebuglog: '',
            pedmain: 0,
            webconnect: 0,
            webread: 0,
            amount: 0,
            tipamt: 0,
            type: 0,
            tipcur: 0,
            email: '',
            shopperref: '',
            recurringcontract: '',
            recurringcontractdetail: '',
            merchantref: '',
            orderref: '',
            mask: '',
            cardno: '',
            minimum: '',
            expmonth: '',
            maximum: '',
            expyear: ''
          }
        },
        configObject = {},
        extension = [],
        sessionInfo = prismSessionInfo.get(); // Configuring settings and calls to server module
      // obfuscating openModal from API
      var openModal = function() {
        var deferred = $q.defer();

        var adyenConfigModalPromise = $uibModal.open({
          backdrop: 'static',
          keyboard: false,
          windowClass: 'full',
          template: '<adyen-configuration close="$ctrl.close" dismiss="$ctrl.dismiss"></adyen-configuration>',
          controllerAs: '$ctrl',
          controller: function() {
            var $ctrl = this;
            $ctrl.close = adyenConfigModalPromise.close;
            $ctrl.dismiss = adyenConfigModalPromise.dismiss;
          }
        });

        adyenConfigModalPromise.result
          .then(function(result) {
            deferred.resolve();
          })
          .catch(function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      };

      var API = {
        Extension: [],
        Config: {},
        /**
         * Set the configuration datas to the blank defaults and save.
         */
        LoadConfig: function() {
          var deferred = $q.defer();
          this.Config = blankExtension;
          this.SaveConfig(null, true).then(
            function() {
              deferred.resolve();
            },
            function(error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        /**
         * Initialize the configuration data plugin, retrieve the information from the ProxyExtension
         * and check if the configuration already exits, if not load the blank data otherwise parse
         * the configuration data into the JSON object Config
         */
        Initialize: function() {
          var deferred = $q.defer();
          ModelService.get('Proxy', {
            filter: 'workstation_name,eq,' + sessionInfo.workstation,
            cols: 'sid'
          }).then(function(data) {
            if(data && data.length>0){
              API.proxy_sid = data[0].sid;
              ModelService.get('ProxyExtension', {
                proxy_sid: data[0].sid,
                cols: '*'
              }).then(function(data) {
                if (data && data.length>0 && data[0].configuration) {
                  API.Extension = data[0]; // read the proxy configuration data
                  var configuration = data[0].configuration.split('\n'); // split the configuration string
                  for (var i = 0; i < configuration.length; i++) {
                    var split = configuration[i].split('='); // create the keys and values
                    if (split[0].length > 0) {
                      configObject[split[0].trim()] = split[1].trim(); // add the keys and values to the object
                    }
                  }
                  configObject.timeout = parseInt(configObject.timeout);
                  configObject.maxTimeout = (API.Extension.timeout>10000)?(API.Extension.timeout-10000) : 0;
                  API.Config = configObject;
                  if (typeof API.Config.adyenconfig === 'string') {
                    API.Config.adyenconfig = JSON.parse(API.Config.adyenconfig);
                  }
  
                  if (API.Config.adyenconfig.hasOwnProperty('cardtype')) {
                    openModal()
                      .then(function() {
                        deferred.resolve();
                      })
                      .catch(function(error) {
                        deferred.reject(error);
                      });
                  } else {
                    API.LoadConfig().then(
                      function() {
                        API.Initialize()
                          .then(function() {
                            deferred.resolve();
                          })
                          .catch(function(error) {
                            deferred.reject(error);
                          });
                      },
                      function(error) {
                        deferred.reject(error);
                      }
                    );
                  }
                } else {
                  Toast.Error('1185', 'No intial adyen configuration in workstation HAL settings');
                  // API.Extension = ModelService.create('ProxyExtension');
                  // API.Config = blankExtension;
                  // openModal()
                  // .then(function() {
                  //   deferred.resolve();
                  // })
                  // .catch(function(error) {
                  //   deferred.reject(error);
                  // });
                }
              });
            }else{
              Toast.Error('1185', '"'+sessionInfo.workstation+'"'+' - Not a proxy workstation', null, true);
            }
            
          });

          return deferred.promise;
        },
        /**
         * Parse the information from the Config object into a string and save to the ProxyExtension table
         */
        SaveConfig: function(form, bypass) {
          var deferred = $q.defer();
          if (form) {
            // properly configure the format of the device IP for the CED endpoint
            form.endpoint = 'https://' + form.deviceip + ':8443/nexo/';
            API.Config = form;
          }
          API.Extension.configuration = ''; // clear the current information before writing new information.
          for (var key in API.Config) {
            var val = API.Config[key];
            if (API.Config.hasOwnProperty(key)) {
              if (key === 'adyenconfig') {
                val = JSON.stringify(API.Config[key]);
              }
              API.Extension.configuration += key + '=' + val + '\n';
            }
          }

          if(API.Extension.sid){
            API.Extension.save().then(
              function(data) {
                if (!bypass) {
                  Toast.Info('1845', '6649', null, false);
                }
                deferred.resolve();
              },
              function(error) {
                deferred.reject(error);
                Toast.Error('1185', '6650', null, true);
              }
            );
          }else{
            API.Extension.insert({proxy_sid:API.proxy_sid}).then(
              function(data) {
                if (!bypass) {
                  Toast.Info('1845', '6649', null, false);
                }
                deferred.resolve();
              },
              function(error) {
                deferred.reject(error);
                Toast.Error('1185', '6650', null, true);
              }
            );
          }
          
          return deferred.promise;
        }
      };

      // if there is no document sid, exit out of plugin.
      return API;
    }
  ]);
