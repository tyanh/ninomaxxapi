/**
 * Primary service for the Adyen EFT plugin, contains common functions used by all other services.
 */
window.angular.module('prismApp.plugin.services.Adyen', []).factory('Adyen', [
  '$q',
  '$http',
  '$state',
  '$uibModal',
  '$uibModalStack',
  '$translate',
  'AdyenData',
  'PrismUtilities',
  'ModelService',
  'prismSessionInfo',
  'Toast',
  'Transaction',
  'ExternalDataService',
  'LoadingScreen',
  function(
    $q,
    $http,
    $state,
    $uibModal,
    $uibModalStack,
    $translate,
    AdyenData,
    PrismUtilities,
    ModelService,
    prismSessionInfo,
    Toast,
    Transaction,
    ExternalDataService,
    LoadingScreen
  ) {
    'use strict';

    var sessionInfo = prismSessionInfo.get();
    var scope = ExternalDataService.scope;

    var API = {
      MKE: false,
      Config: {},
      Cancelling: false,
      /**
       * Gets the current base currency for the subsidiary and sets the property in the AdyenData service
       * (this may need to be updated in the future to pass in specific currencies from settings)
       */
      UpdateCurrency: function() {
        var deferred = $q.defer();
        ModelService.get('Subsidiary', {
          sid: sessionInfo.subsidiarysid,
          cols: 'base_currency_sid,currency_code'
        }).then(
          function(data) {
            var baseCurrency = PrismUtilities.responseParser(data);
            AdyenData.Tender.basecurrency = baseCurrency[0];
            deferred.resolve();
          },
          function(error) {
            deferred.reject(error);
          }
        );
        return deferred.promise;
      },
      /**
       * Core POST function to send the EFT payload to the Adyen proxy plugin for encryption
       * Returns the unencrypted response from the PED
       */
      Post: function(json) {
        var deferred = $q.defer();
        $http.post('/v1/rest/adyen', JSON.stringify(json)).then(
          function(response) {
            deferred.resolve(response);
          },
          function(error) {
            deferred.reject(error);
          }
        );
        // Return the promise to the controller
        return deferred.promise;
      },
      /**
       * Core CANCEL function, sends an abort command to Adyen, to cancel a transaction that
       * is already in process.  (primarily used when doing keyed entry to cancel the first
       * transaction that started by default so a new one can begin)
       * Returns a promise that should be waited upon before starting a new transaction.
       */
      Cancel: function() {
        var deferred = $q.defer();
        // take a deep copy of the default payload
        var payload = JSON.parse(JSON.stringify(AdyenData.RawData));
        // configure the payload for an Abort (per their documentation)
        payload.SaleToPOIRequest.MessageHeader.MessageCategory = 'Abort';
        payload.SaleToPOIRequest.AbortRequest = {
          AbortReason: 'MerchantAbort',
          MessageReference: {
            SaleID: AdyenData.CurrentSaleID, // set the sale ID to the ID of the transaction that we are cancelling
            ServiceID: AdyenData.CurrentServiceID, // set the service ID to the ID of the transaction we are cancelling
            MessageCategory: 'Payment'
          }
        };

        payload.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID(); // generate a unique ServiceID
        payload.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
          payload.SaleToPOIRequest.MessageHeader.SaleID
        );
        payload.SaleToPOIRequest.MessageHeader.POIID =
          AdyenData.Config.serialnumber;
        if (!API.Cancelling) {
          API.Cancelling = true;
          if (!API.MKE) {
            //API.CloseModals();
            LoadingScreen.topMsg = 'Waiting on Adyen Device';
            LoadingScreen.Enable = true;
          }
          $http
            .post('/v1/rest/adyen2?action=abort', JSON.stringify(payload))
            .then(
              function(response) {
                LoadingScreen.topMsg = null;
                LoadingScreen.Enable = false;
                deferred.resolve(response);
                API.Cancelling = false;
              },
              function(error) {
                API.Cancelling = false;
                deferred.reject(error);
              }
            );
        }

        // Return the promise to the controller
        return deferred.promise;
      },
      /**
       * Takes failure responses from the PED and retrieves the error messages from
       * the AdditionalResponse and generates a Toast displaying the error
       */
      ProcessFailure: function(response) {
        var deferred = $q.defer();
        var error;
        if (response && typeof response === 'object') {
          var data = angular.fromJson(response);
          if (data.Response) {
            if (data.Response.AdditionalResponse) {
              var eq = AdyenData.decodeText(data.Response.AdditionalResponse);
              error = eq.errors ? eq.errors : eq.message;
            } else {
              error = data.Response.ErrorCondition;
            }
            if (error.includes('104')) {
              Toast.Warning('1.2', error, null, false);
            } else {
              Toast.Error('1185', error, null, true);
            }
          }

          if (
            data.SaleToPOIResponse &&
            data.SaleToPOIResponse.PaymentResponse
          ) {
            data.PaymentResponse = data.SaleToPOIResponse.PaymentResponse;
          }

          if (data.PaymentResponse) {
            error = AdyenData.decodeText(
              data.PaymentResponse.Response.AdditionalResponse
            );
            if (error.message.includes('104')) {
              Toast.Warning('1.2', error.message, null, false);
            } else {
              Toast.Error('1185', error.message, null, true);
            }
          }
        } else {
          Toast.Error('1185', '6562', null, true);
        }
        if (!API.MKE) {
          deferred.resolve({ method: 'cancel' }); // close the notification modal.
        }

        return deferred.promise;
      },
      /**
       * Core function for Voiding payments in Prism.
       */
      VoidPayment: function(tender, refusal) {
        var deferred = $q.defer(),
          // take a deep copy of the default payload
          rawData = JSON.parse(JSON.stringify(AdyenData.RawData));

        // configure the payload for a Reversal
        rawData.SaleToPOIRequest.MessageHeader.ServiceID = AdyenData.GetServiceID(); // generate a unique ServiceID
        rawData.SaleToPOIRequest.MessageHeader.SaleID = AdyenData.GetSaleID(
          rawData.SaleToPOIRequest.MessageHeader.SaleID
        );
        rawData.SaleToPOIRequest.MessageHeader.POIID = JSON.parse(
          JSON.stringify(AdyenData.Config.serialnumber)
        );
        rawData.SaleToPOIRequest.MessageHeader.MessageCategory = 'Reversal';
        rawData.SaleToPOIRequest.ReversalRequest = {};

        rawData.SaleToPOIRequest.ReversalRequest.OriginalPOITransaction = {
          POITransactionID: {
            TimeStamp: tender.eftdata9, // retrieves the timestamp from the tender being voided
            TransactionID: tender.eftdata10 // retrieves the transactionID from the tender being voided
          }
        };
        rawData.SaleToPOIRequest.ReversalRequest.ReversalReason =
          'MerchantCancel';

        API.Post(rawData).then(
          function(response) {
            var resp = angular.fromJson(response.data);
            if (
              resp.SaleToPOIResponse &&
              resp.SaleToPOIResponse.ReversalResponse.Response.Result !==
                'Failure'
            ) {
              if (refusal) {
                tender.remove().then(function() {
                  // Reload page to show added tenders
                  scope.init();
                  deferred.resolve(response);
                });
              } else {
                deferred.resolve(response);
                // Reload page to show added tenders
                scope.init();
              }
            } else {
              API.ProcessFailure(response).then(function(response) {
                deferred.resolve(response); // close the notification modal, reject and void
              });
            }
          },
          function(error) {
            deferred.reject(error);
          }
        );
        return deferred.promise;
      },
      /**
       * Primary initialization function for the Adyen EFT plugin
       * Verifies transaction sids, retrieves configuration data and initializes data
       */
      Initialize: function() {
        var deferred = $q.defer();
        if (!Transaction.Document.active.sid) {
          Transaction.Document.get(AdyenData.DocSid).then(
            function(data) {
              Transaction.Document.active = data;
              AdyenData.Initialize()
                .then(function(response) {
                  deferred.resolve(response);
                })
                .catch(function(error) {
                  deferred.reject(error);
                });
            },
            function(error) {
              deferred.reject(error);
            }
          );
        } else {
          AdyenData.GetConfig().then(
            function(response) {
              //verify that adyen device is configured and fetch its serial #
              if (response) {
                AdyenData.Initialize()
                  .then(function(response) {
                    deferred.resolve(response);
                  })
                  .catch(function(error) {
                    deferred.reject(error);
                  });
              } else {
                Toast.Error('1185', '6648', null, true);
                deferred.reject();
              }
            },
            function(error) {
              deferred.reject(error);
            }
          );
        }
        return deferred.promise;
      },
      /**
       * Error processing based on type of error received.
       */
      DisplayError: function(error) {
        var deferred = $q.defer();
        if (error) {
          if (Array.isArray(error)) {
            Toast.Error('1185', error[0].httpmessage, null, true);
          } else {
            if (typeof error === 'object') {
              Toast.Error('1185', error.errormsg, null, true);
            } else {
              Toast.Error('1185', error, null, true);
            }
          }
        } else {
          Toast.Error('1185', 'An unknown error occured.', null, true);
        }
        deferred.resolve();
        return deferred.promise;
      }
    };

    return API;
  }
]);
