'use strict';
window.angular.module('prismApp').component('adyenCreditRefundModal', {
  templateUrl:
    'plugins/EFT/adyen/adyen-credit-refund-modal/adyen-credit-refund-modal.htm',
  bindings: { 
    tender: '=',
    close: '<',
    dismiss: '<'
  },
  controllerAs: 'adyenCreditRefund',
  controller: [
    'Adyen',
    'AdyenData',
    'AdyenCredit',
    '$q',
    '$scope',
    function(Adyen, AdyenData, AdyenCredit, $q, $scope) {
      var adyenCredit = this;

      $scope.chooseRFR = false;
      $scope.retrievingTenders = false;
      $scope.processingRefund = false;

      adyenCredit.$onInit = function() {
        $scope.referenceRefund = function() {
          $scope.chooseRFR = true;
          $scope.retrievingTenders = true;
          $scope.ccTenders = AdyenData.RefundTenders;
          $scope.retrievingTenders = false;
        };

        $scope.refundTender = function(tender) {
          $scope.processingRefund = true;
          var deferred = $q.defer();
          AdyenCredit.ReferenceRefund(tender).then(
            function() {
              $scope.processingRefund = false;
              $scope.$parent.$close({ method: 'credit' });
              deferred.resolve();
            },
            function(error) {
              $scope.processingRefund = false;
              $scope.$parent.$close({ method: 'credit', error: error });
            }
          );
          return deferred.promise;
        };

        $scope.regularRefund = function() {
          $scope.$parent.$close();
          AdyenCredit.CreditModal().then(
            function() {
              $scope.$parent.$close({ method: 'credit' });
            },
            function(error) {
              $scope.$parent.$close({ method: 'credit', error: error });
              adyenCreditRefund.dismiss('cancel');
            }
          );
        };

        $scope.abortCED = function() {
          $scope.$parent.$close();
        };
      };
    }
  ]
});
