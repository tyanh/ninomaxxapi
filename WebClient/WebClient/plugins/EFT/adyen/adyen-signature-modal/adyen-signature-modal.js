'use strict';
window.angular.module('prismApp').component('adyenSignatureModal', {
  templateUrl:
    'plugins/EFT/adyen/adyen-signature-modal/adyen-signature-modal.htm',
  bindings: { signature: '<' },
  controllerAs: 'adyenSignature',
  controller: [
    'Adyen',
    '$q',
    '$scope',
    'AdyenCredit',
    function(Adyen, $q, $scope, AdyenCredit) {
      var adyenSignature = this;

      adyenSignature.$onInit = function() {
        var sig = adyenSignature.signature;
        AdyenCredit.VerifySignature(sig);

        $scope.approveSignature = function() {
          $scope.$parent.$close(true);
        };

        $scope.rejectSignature = function() {
          $scope.$parent.$close(false);
        };
      };
    }
  ]
});
