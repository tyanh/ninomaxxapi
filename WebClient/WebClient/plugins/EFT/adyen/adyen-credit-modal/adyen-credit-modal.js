'use strict';
window.angular.module('prismApp').component('adyenCreditModal', {
  templateUrl: 'plugins/EFT/adyen/adyen-credit-modal/adyen-credit-modal.htm',
  bindings: { 
    tender: '=',
    close: '<',
    dismiss: '<'
  },
  controllerAs: 'adyenCredit',
  controller: [
    'Adyen',
    'AdyenData',
    'AdyenCredit',
    '$q',
    '$scope',
    function(Adyen, AdyenData, AdyenCredit, $q, $scope) {
      var adyenCredit = this;

      $scope.deviceInitialized = true;
      $scope.deviceValidating = false;
      $scope.manualEntry = false;
      $scope.manualEntryProcessing = false;
      $scope.tenderData = AdyenData.Tender;
      adyenCredit.$onInit = function() {
        AdyenCredit.ProcessPayment().then(
          function() {
            if (!$scope.manualEntry) {
              adyenCredit.dismiss();
            }
          },
          function(error) {
            adyenCredit.dismiss();
          }
        );

        $scope.setManualEntry = function() {
          Adyen.MKE = true;
          Adyen.Cancel().then(
            function() {
              $scope.manualEntry = true;
            },
            function(error) {
              adyenCredit.dismiss();
            }
          );
        };

        $scope.keyedEntry = function(method) {
          $scope.manualEntryProcessing = true;
          AdyenCredit.ManualEntry(method).then(function() {
            Adyen.MKE = false;
            adyenCredit.dismiss();
          });
        };

        $scope.abortCED = function() {
          Adyen.Cancel().then(
            function() {
              $scope.$parent.$close({ method: 'cancel' });
            },
            function(error) {
              adyenCredit.dismiss();
            }
          );
        };
      };
    }
  ]
});
