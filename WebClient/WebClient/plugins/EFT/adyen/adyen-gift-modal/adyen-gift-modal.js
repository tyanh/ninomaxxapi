'use strict';
window.angular.module('prismApp').component('adyenGiftModal', {
  templateUrl: 'plugins/EFT/adyen/adyen-gift-modal/adyen-gift-modal.htm',
  bindings: {
    close: '<',
    dismiss: '<'
  },
  controllerAs: 'adyenGift',
  controller: [
    'Adyen',
    'AdyenData',
    'AdyenGift',
    '$q',
    '$scope',
    '$state',
    '$timeout',
    '$filter',
    '$translate',
    'prismSessionInfo',
    'ModelService',
    'PrintersService',
    'DocumentPersistedData',
    function(
      Adyen,
      AdyenData,
      AdyenGift,
      $q,
      $scope,
      $state,
      $timeout,
      $filter,
      $translate,
      prismSessionInfo,
      ModelService,
      PrintersService,
      DocumentPersistedData
    ) {
      var adyenGift = this;

      adyenGift.init = function() {
        $scope.form = {
          // Defines form data for future use
          card_number: '', // sets sub variable for card number,
          card_type: 'svs'
        };
        // get store data to be able to send to custom print job
        ModelService.get('Store', {
          sid: prismSessionInfo.get().storesid,
          cols:
            'address1,address2,address3,address4,address5,phone1,store_name,store_code,store_number,zip'
        }).then(function(data) {
          $scope.store = data[0];
          $scope.printData =
            // define data fields to send to custom print job
            {
              store_number: data[0].store_number,
              store_code: data[0].store_code,
              store_name: data[0].store_name,
              store_address1: data[0].address1,
              store_address2: data[0].address2,
              store_address3: data[0].address3,
              store_address4: data[0].address4,
              store_address5: data[0].address5,
              store_zip: data[0].zip,
              store_phone: data[0].phone1,
              card_number: null, // leave card number blank will be filled later
              card_balance: null, // leave card balance blank will be filled later
              cardmax: null, // not used by cayan but required by design
              cardexpire: null // not used by cayan but required by design
            };
        });

        $scope.showBalance = false;
        $scope.showKeyedMode = false; // defines value as false for future use
        $scope.processingGiftcard = true; // defines value as false for future use
        $scope.focusSwipe = true; // defines value as true for future use
        $scope.submitOnce = false;
        $scope.timer = 0;

        $scope.useSVS = AdyenData.Config.adyenconfig.cardtype === 'svs';

        $scope.doFocus = function() {
          // defines function to control focus on carddata
          $scope.focusSwipe = false; // after function is called set to false
          $timeout(function() {
            // create 200 ms delay
            $scope.focusSwipe = true; // after delay set the focus value back to true
          }, 200);
        };

        // function to process keyedEntry
        $scope.keyedEntry = function() {
          $scope.showKeyedMode = true;
          Adyen.MKE = true;
          Adyen.Cancel().then(
            function() {
              $scope.processingGiftcard = false;
            },
            function(error) {
              adyenGift.dismiss();
            }
          );
        };

        $scope.pedKeyedEntry = function() {
          $scope.processingGiftcard = true;
          $scope.showKeyedMode = false;
          AdyenGift.ManualEntry({cardnumber:$scope.form.card_number,cardtype:$scope.form.card_type}).then(
            function(response) {
              Adyen.MKE = false;
              $scope.form.card_number = '';
              switch (AdyenGift.Mode) {
                case 'balance':
                  displayBalance(response);
                  break;
                case 'addvalue':
                case 'activate':
                case 'payment':
                case 'deactivate':
                default:
                  adyenGift.dismiss();
                  break;
              }
            },
            function(error) {
              $scope.form.card_number = '';
              adyenGift.dismiss();
            }
          );
        };

        $scope.manualAbort = function(prevReq) {
          Adyen.Cancel().then(function() {}, function(error) {});
        };

        var activateCard = function() {
          AdyenGift.ActivateCard().then(
            function() {
              adyenGift.dismiss();
            },
            function(error) {
              $scope.abortCED();
            }
          );
        };

        var deactivateCard = function() {
          AdyenGift.DeactivateCard().then(
            function() {
              adyenGift.dismiss();
            },
            function(error) {
              $scope.abortCED();
            }
          );
        };

        var displayBalance = function(response) {
          if (
            response.data.SaleToPOIResponse &&
            response.data.SaleToPOIResponse.BalanceInquiryResponse
          ) {
            var accountStatus =
              response.data.SaleToPOIResponse.BalanceInquiryResponse
                .PaymentAccountStatus;
            AdyenData.GetCurrency(accountStatus.Currency).then(
              function() {
                $scope.showBalance = true;
                $scope.remainingBalance = $filter('foreign_currency')(
                  accountStatus.CurrentBalance,
                  accountStatus.Currency
                );

                var eq = AdyenData.decodeText(
                  response.data.SaleToPOIResponse.BalanceInquiryResponse
                    .Response.AdditionalResponse
                );
                $scope.printData.card_number = eq.giftcardPAN;
                $scope.printData.cardexpire = new Date(
                  eq.expiryYear,
                  eq.expiryMonth
                );
                $scope.printData.currency_code = accountStatus.Currency;
                $scope.printData.card_balance = accountStatus.CurrentBalance; // set card balance for printing
              },
              function(error) {
                adyenGift.dismiss();
              }
            );
          } else {
            $scope.abortCED();
          }
        };

        var balanceCheck = function() {
          AdyenGift.GetBalance().then(
            function(response) {
              displayBalance(response);
            },
            function(error) {
              $scope.abortCED();
            }
          );
        };

        var addValue = function() {
          AdyenGift.AddValue().then(
            function() {
              adyenGift.dismiss();
            },
            function(error) {
              $scope.abortCED();
            }
          );
        };

        var cardPayment = function() {
          if (AdyenData.Tender.mode.toLowerCase() === 'take') {
            AdyenGift.ProcessPayment().then(
              function() {
                adyenGift.dismiss();
              },
              function(error) {
                $scope.abortCED();
              }
            );
          } else {
            AdyenGift.Mode = 'addvalue';
            AdyenGift.AddValue().then(
              function() {
                adyenGift.dismiss();
              },
              function(error) {
                $scope.abortCED();
              }
            );
          }
        };

        $scope.disableSubmit = function() {
          return (
            $scope.useSVS && (!$scope.form.card_pin && $scope.form.card_number && $scope.form.card_type)
          );
        };

        $scope.abortCED = function(skip) {
          if (AdyenData.Processing) {
            Adyen.Cancel().then(
              function() {
                adyenGift.dismiss();
              },
              function(error) {
                adyenGift.dismiss();
              }
            );
          } else {
            adyenGift.dismiss();
          }
        };

        $scope.printBalance = function() {
          DocumentPersistedData.PrintDesignData.Payload = $scope.printData;
          $scope.printSpec = [$scope.printData]; // add print payload to printSpec data inside an array
          PrintersService.printAction(
            null,
            'Gift Card Balance',
            $scope.printSpec
          ); // initiate print action to
          adyenGift.dismiss();
        };

        switch (AdyenGift.Mode) {
          case 'deactivate':
            deactivateCard();
            break;
          case 'activate':
            activateCard();
            break;
          case 'addvalue':
            addValue();
            break;
          case 'payment':
            cardPayment();
            break;
          case 'balance':
          default:
            balanceCheck();
            break;
        }
      };
      adyenGift.init();
    }
  ]
});
