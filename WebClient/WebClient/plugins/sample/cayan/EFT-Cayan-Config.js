/**
 * ButtonHook to run before the posTenderTake and posTenderGive events
 * Checks the tender type and if credit, debit or gift runs through EFT provider
 * otherwise returns control to prism
 */
'use strict';
var executionInPending = false;
ButtonHooksManager.addHandler(
  ['before_posTenderTake', 'before_posTenderGive'],
  function(
    $q,
    $window,
    $uibModal,
    DocumentPersistedData,
    PrismUtilities,
    NotificationService,
    ModelService,
    prismSessionInfo,
    eftCayanService,
    $timeout,
    $http,
    TenderTypes,
    PrintersService,
    ShopperDisplay,
    Toast,
    Transaction,
    $state,
    ExternalDataService,
    LoadingScreen
  ) {
    var deferred = $q.defer();
    //Execution of first click should be continued
    //Otherwise skip any later clicks until the flag is reset
    if(!executionInPending){
      executionInPending = true;
    // Declare variables for future use
    var document = [], // To store document information
      cayanData = [],
      scope = ExternalDataService.scope, // To store data from EFT Provider
      sequence, // Gets assigned below, after document is retrieved
      documentParams = {
        sid: DocumentPersistedData.DocumentInformation.Sid,
        cols: '*'
      },
      printData = {
        store_name: '',
        store_number: '',
        store_code: '',
        store_address_line1: '',
        store_address_line2: '',
        store_address_line3: '',
        store_address_line4: '',
        store_address_line5: '',
        store_address_zip: '',
        store_phone: '',
        created_datetime: new Date().toISOString(), // now
        card_type: '',
        card_number: '',
        amount: '',
        emv_appinfo_aid: '',
        emv_appinfo_applicationlabel: '',
        emv_cardinfo_cardexpirydate: '',
        emv_crypto_cryptogram: '',
        emv_crypto_cryptogramtype: '',
        emv_pinstatement: '',
        cashier_login_name: '',
        employee1_login_name: ''
      },
      prefs = prismSessionInfo.get().preferences,
      validConfig =
        prefs.eft_mw_merchant_name.length > 0 &&
        prefs.eft_mw_merchant_site_id.length > 0 &&
        prefs.eft_mw_merchant_key.length > 0,
      printSpec = {
        quantity: '1',
        design: prefs.peripherals_output_deniedreceipts_print_design,
        printer: prefs.peripherals_output_deniedreceipts_print_printer,
        printOrder: '',
        email: DocumentPersistedData.BillToCustomerData.EmailAddress,
        whatToPrint: []
      },
      creditAction = 'CREDIT_' + scope.tender.mode.toUpperCase(),
      authcode = '',
      dummyObj = ModelService.create('Tender');
    scope.tender.type = dummyObj.typeFromName(scope.tender.tenderType);

    /**
     * Function to process modal for credit card transactions
     * @param overrideDupeChecking
     */
    function creditTransaction(overrideDupeChecking) {
      var errorCond = false;
      if (PrismUtilities.toBoolean(scope.tender.forceCreditAuth)) {
        creditAction = 'CREDIT_FORCE';
        authcode = scope.tender.creditAuth;
      }

      eftCayanService.Credit.Initiate({
        document_sid: document.sid,
        eft_invc_no: sequence,
        actiontype: creditAction,
        amount: scope.tender.amount,
        usegeniusdevice: false,
        forcedupcheck: overrideDupeChecking,
        authcode: authcode,
        web_redirect: location.href + '/eftmw/' + scope.tender.mode
      })
        .then(
          function(creditInit) {
            cayanData = creditInit[0];
            return eftCayanService.Credit.Update(
              {
                web_redirect:
                  location.href +
                  '/eftmw/' +
                  cayanData.sid +
                  '/' +
                  scope.tender.mode +
                  '/' +
                  sessionStorage.getItem('PRISMAUTH'),
                row_version: cayanData.row_version,
                sid: cayanData.sid
              },
              function(error) {
                eftCayanService.ShowError(error.data);
              }
            );
          },
          function(error) {
            errorCond = true;
            Toast.Error('1185', error.data[0].errormsg);
          }
        )
        .then(
          function() {
            ShopperDisplay.Redraw(); // redraw shopper display
            if (!errorCond) {
              //doing this outside of angular for now.
              location.href =
                prefs.eft_mw_web_transaction_server + cayanData.transportkey;
            }
          },
          function(error) {
            eftCayanService.ShowError(error.data);
          }
        );
    }

    /**
     * Function to process modal for gift card transactions
     * @param mode
     */
    function processGiftCardTransaction(useCedFlag) {
      eftCayanService.cayanAPI
        .openGiftCardProcessDialog({
          useCED: useCedFlag,
          actionType: (scope.tender.mode.toLowerCase() === 'take')?'GIFT_TAKE':'GIFT_GIVE',
          titleId: (scope.tender.mode.toLowerCase() === 'take')?'1481':'1482',
          forTender: true
        })
        .then(
          function(giftCardResponsePayload) {
            if (giftCardResponsePayload) {
              eftCayanService.cayanAPI.addGiftCardPurchaseTenderToDocument(
                giftCardResponsePayload
              );
            }
          },
          function(error) {
            eftCayanService.cayanAPI.ShowError(error.data);
          }
        );
    }

    /**
     * Function to process credit card transactions that returned a duplicate transaction error
     * from EFT provider
     */
    function duplicateTransactionPrompt() {
      //('The requested transaction was determined to be a duplicate of a previous transaction. Would you like to resend the transaction attempt without duplicate detection?', 'DECLINED, DUPLICATE'); // Untranslated values for the message
      var runNoDupes = NotificationService.addConfirm('2414', '2415');
      runNoDupes.then(function(decision) {
        if (decision) {
          geniusDevicePrompt(true);
        }
      });
    }

    /**
     *
     * @param overrideDupeChecking
     */
    function geniusDevicePrompt(overrideDupeChecking) {
      var dupCheck =
        overrideDupeChecking ||
        DocumentPersistedData.DocumentInformation.DuplicateTransaction;
      DocumentPersistedData.DocumentInformation.DuplicateTransaction = false; // reset value
      if (PrismUtilities.toBoolean(scope.tender.forceCreditAuth)) {
        // check if forceCreditAuth is true
        creditAction = 'CREDIT_FORCE'; // set creditAction
        authcode = scope.tender.creditAuth; // set authcode
      }

      eftCayanService.Credit.Initiate({
        // Initiate credit transaction with the EFT service
        document_sid: document.sid, // set document_sid
        eft_invc_no: sequence, // set eft_invc_no
        actiontype: creditAction, // set actiontype
        amount: scope.tender.amount, // set amount
        usegeniusdevice: true, // set usegeniusdevice
        forcedupcheck: dupCheck, // set forcedupcheck
        authcode: authcode // set authcode
      }).then(
        function(creditInit) {
          // set what to do after CED responds with creditInit value
          creditInit[0].debit = parseInt(scope.tender.type) === 11; // set debit to true if tender type = 11
          var cayanCreditModalPromise = $uibModal.open(
            {
              // create promise and open modal
              backdrop: 'static',
              keyboard: false,
              //windowClass: 'full', // sets class on modal to full makes modal fill screen
              templateUrl: '/plugins/sample/cayan/EFT-Cayan-Device.htm', //set template url
              controller: 'cayanDeviceController', // set controller for modal
              resolve: {
                MWData: function() {
                  return angular.copy(creditInit[0]);
                } // send creditInit to controller as MWData
              }
            },
            function(error) {
              Toast.Error('1185', error.data[0].errormsg);
              return false;
            }
          );
          cayanCreditModalPromise.result.then(
            function(returnObject) {
              // check if the eft provider returned with a positive response to add the tender
              if (returnObject.addtender) {
                //Start process of creating new tender entry in the document tender table
                var newTender = ModelService.create('Tender');
                //Sets the tender type
                newTender.tender_type = returnObject.mwdata.tendertype;
                //Sets the document_sid
                newTender.document_sid = document.sid;
                //Updates the documents amount due (this may need to be removed based on condensed code)
                document.due_amt -= scope.tender.amount;
                //Sets the Authorization Code
                newTender.authorization_code = returnObject.mwdata.authcode;
                //Sets the Card Number
                newTender.card_number = returnObject.mwdata.cardnumber;
                //Sets the tender amount
                if (scope.tender.mode.toLowerCase() === 'take') {
                  newTender.taken = returnObject.mwdata.amount;
                } else {
                  newTender.given = returnObject.mwdata.amount;
                }
                //Sets the tender name
                if (returnObject.mwdata.card_type_desc.length > 0) {
                  newTender.tender_name = returnObject.mwdata.card_type_desc.substr(
                    0,
                    25
                  );
                } else {
                  newTender.tender_name = newTender.printName();
                }
                //Sets the Transaction ID from the Returned Token
                newTender.eft_transaction_id = returnObject.mwdata.token;
                //Stores the returned card type into EFT Data field
                newTender.eftdata1 = returnObject.mwdata.card_type;
                //Stores the returned EMV AI AID
                newTender.emv_ai_aid = returnObject.mwdata.emv_ai_aid;
                //Stores the returned EMV AI APP LABEL
                newTender.emv_ai_applabel = returnObject.mwdata.emv_ai_applabel;
                //Stores the returned EMV Card Exp Date
                newTender.emv_ci_cardexpirydate =
                  returnObject.mwdata.emv_ci_cardexpirydate;
                //Stores the returned EMV Crypto Cryptogram Type
                newTender.emv_crypto_cryptogramtype =
                  returnObject.mwdata.emv_crypto_cryptogramtype;
                //Stores the returned EMV Crypto Cryptogram
                newTender.emv_crypto_cryptogram =
                  returnObject.mwdata.emv_crypto_cryptogram;
                //Stores the returned EMV Pin Statement
                newTender.emv_pinstatement =
                  returnObject.mwdata.emv_pinstatement;

                if (
                  returnObject.mwdata.errormessage === 'APPROVED_No_Signature'
                ) {
                  //'Transaction Approved, No Signature Collected', 'No Signature' // Untranslated values for the message
                  NotificationService.addNotification('2412', '2413');
                }
                //Adds tender to the document object using the document BO
                document.addTender(newTender).then(function() {
                  //Get the existing document
                  ModelService.get('Document', documentParams).then(function(
                    data
                  ) {
                    document = data[0];
                    //Check if returned object was a gift card and had an approved amount that was less than the tendered amount
                    if (
                      returnObject.mwdata.amount < scope.tender.amount &&
                      returnObject.mwdata.card_type === 'GIFT'
                    ) {
                      // Untranslated text 'Giftcard balance is insufficient to complete transaction.  An additional payment source is required.'
                      if (
                        !returnObject.mwdata.errormessage &&
                        returnObject.mwdata.amount < scope.tender.amount
                      ) {
                        // Untranslated text 'Giftcard balance is insufficient to complete transaction.  An additional payment source is required.'
                        NotificationService.addAlert(
                          '3231',
                          returnObject.mwdata.approvalstatus
                        ).then(function() {
                          // Reload page to show added tenders
                          scope.init();
                        });
                      } else if (
                        !returnObject.mwdata.errormessage &&
                        returnObject.mwdata.approvalstatus === 'DECLINED'
                      ) {
                        NotificationService.addAlert(
                          '4299',
                          returnObject.mwdata.approvalstatus
                        ).then(function() {
                          // Reload page to show added tenders
                          scope.init();
                        });
                      } else {
                        Toast.Error(
                          returnObject.mwdata.approvalstatus,
                          returnObject.mwdata.errormessage
                        ).then(function() {
                          // Reload page to show added tenders
                          scope.init();
                        });
                      }
                    } else {
                      // Reload page to show added tenders
                      if (
                        document.due_amt === 0 &&
                        prefs.automatically_print_update_balanced_document ===
                          'true'
                      ) {
                        scope.updateDocument(true);
                      } else {
                        scope.init();
                      }
                    }
                  });
                });
              } else {
                // if response from eft provider is to run without the CED
                if (returnObject.runWithoutDevice) {
                  if (parseInt(scope.tender.type) === 10) {
                    // If tender type is 10 run gift card process
                    //giftCardTransaction();
                    processGiftCardTransaction();
                  } else {
                    // run credit transaction instead
                    creditTransaction(dupCheck);
                  }
                  deferred.reject();
                } else {
                  if (
                    returnObject.mwdata.approvalstatus === 'DECLINED_DUPLICATE'
                  ) {
                    $timeout(function() {
                      duplicateTransactionPrompt();
                    }, 500);
                  } else {
                    // Throw error containing response from the EFT Provider
                    //check the EMV fields - if any are not blank, print the declined receipt...
                    if (returnObject) {
                      console.log(returnObject);
                      var EMVDataExists =
                        returnObject.mwdata.emv_ai_aid ||
                        returnObject.mwdata.emv_ai_applabel ||
                        returnObject.mwdata.emv_ci_cardexpirydate ||
                        returnObject.mwdata.emv_crypto_cryptogramtype ||
                        returnObject.mwdata.emv_crypto_cryptogram ||
                        returnObject.mwdata.emv_pinstatement;
                      if (
                        returnObject.mwdata.amount < scope.tender.amount &&
                        returnObject.mwdata.card_type === 'GIFT'
                      ) {
                        if (
                          !returnObject.mwdata.errormessage &&
                          returnObject.mwdata.amount < scope.tender.amount
                        ) {
                          // Untranslated text 'Giftcard balance is insufficient to complete transaction.  An additional payment source is required.'
                          NotificationService.addAlert(
                            '3231',
                            returnObject.mwdata.approvalstatus
                          ).then(function() {
                            if (EMVDataExists) {
                              printDeclinedReceipt(returnObject.mwdata);
                            }
                          });
                        } else if (
                          !returnObject.mwdata.errormessage &&
                          (returnObject.mwdata.approvalstatus === 'DECLINED' ||
                            returnObject.mwdata.approvalstatus ===
                              'DECLINED;MP')
                        ) {
                          NotificationService.addAlert(
                            '4299',
                            returnObject.mwdata.approvalstatus
                          ).then(function() {
                            if (EMVDataExists) {
                              printDeclinedReceipt(returnObject.mwdata);
                            }
                          });
                        } else {
                          NotificationService.addAlert(
                            returnObject.mwdata.errormessage,
                            returnObject.mwdata.approvalstatus
                          ).then(function() {
                            if (EMVDataExists) {
                              printDeclinedReceipt(returnObject.mwdata);
                            }
                          });
                        }
                      } else {
                        NotificationService.addAlert(
                          returnObject.mwdata.errormessage,
                          returnObject.mwdata.approvalstatus
                        ).then(function() {
                          if (EMVDataExists) {
                            printDeclinedReceipt(returnObject.mwdata);
                          }
                        });
                      }
                    }
                    deferred.reject();
                  }
                }
              }
            },
            function(error) {
              if (error) {
                eftCayanService.ShowError(error.data);
              }
            }
          );
        },
        function(error) {
          if (error) {
            eftCayanService.ShowError(error.data);
          }
        }
      );
    }

    /** function to handle the printing of declined receipts based on preferences in the admin console **/
    function printDeclinedReceipt(EMVData) {
      DocumentPersistedData.PrintDesignData.PrintType = 'creditinfo'; // set PrintType in the documentpersisteddata
      DocumentPersistedData.PrintDesignData.Title = 'Declined Receipt'; // set Title to print in the documentpersisteddata
      DocumentPersistedData.PrintDesignData.Design =
        prefs.peripherals_output_deniedreceipts_print_design; // set the design based on stored preference data
      DocumentPersistedData.PrintDesignData.Printer =
        prefs.peripherals_output_documents_print_printer; // set the printer bsaed on stored preference data

      // use the Model service to get the store information
      ModelService.get('Store', {
        sid: prismSessionInfo.get().storesid,
        cols:
          'address1,address2,address3,address4,address5,phone1,store_name,store_code,store_number,zip' // specify the columns to return from the DB
      })
        .then(function(data) {
          //Set up the payload of data to print
          printData = {
            store_name: data[0].store_name,
            store_number: data[0].store_number,
            store_code: data[0].store_code,
            store_address_line1: data[0].address1,
            store_address_line2: data[0].address2,
            store_address_line3: data[0].address3,
            store_address_line4: data[0].address4,
            store_address_line5: data[0].address5,
            store_address_zip: data[0].zip,
            store_phone: data[0].phone1,
            card_type: EMVData.card_type,
            card_number: EMVData.cardnumber,
            amount:
              DocumentPersistedData.DocumentInformation.Tenders.CurrentAmt,
            emv_appinfo_aid: EMVData.emv_ai_aid,
            emv_appinfo_applicationlabel: EMVData.emv_ai_applabel,
            emv_cardinfo_cardexpirydate: EMVData.emv_ci_cardexpirydate,
            emv_crypto_cryptogram: EMVData.emv_crypto_cryptogram,
            emv_crypto_cryptogramtype: EMVData.emv_crypto_cryptogramtype,
            emv_pinstatement: EMVData.emv_pinstatement
          };
        })
        .then(function() {
          // specify the params to pass when getting the document
          var docParams = {
            sid: DocumentPersistedData.DocumentInformation.Sid,
            cols: 'document_number,cashier_login_name,employee1_login_name' // specify the columns to return from the DB
          };

          ModelService.get('Document', docParams) // use the Model service to get the current document
            .then(function(data) {
              printData.cashier_login_name = data[0].cashier_login_name; // set the returned cashier to printData
              printData.employee1_login_name = data[0].employee1_login_name; // set the returned employee to printData
              DocumentPersistedData.PrintDesignData.Payload = printData; // add printData to the DPD print data Payload
              printSpec.whatToPrint.push(
                DocumentPersistedData.PrintDesignData.Payload
              ); // push the entire Payload into the WhatToPrint array
            })
            .then(function() {
              PrintersService.printAction(
                null,
                'EFT Information',
                printSpec.whatToPrint
              );
            });
        });
    }

    function getTenderRule(tenderPref, index) {
      // function to get value of specific tender rule
      var cut = tenderPref.split(','); // split preference into array by comma
      if (index === 'oTender') {
        //check if the index equals oTender
        if (cut[1]) {
          return cut[1]; // return value of cut[1]
        } else {
          return 0; // return zero
        }
      } else if (index === 'rMutiple') {
        if (cut[2]) {
          return cut[2];
        } else {
          return 0;
        }
      }
      var bools = cut[0].split(''); // split cut
      return bools[index] === 'T'; // return true if value at index is T
    }

    /***
     * Call to genius mini url to open genius mini app on ios
     */
    function initGeniusMini(overrideDupeChecking) {
      var dupCheck =
        overrideDupeChecking ||
        DocumentPersistedData.DocumentInformation.DuplicateTransaction;
      DocumentPersistedData.DocumentInformation.DuplicateTransaction = false; // reset value
      if (PrismUtilities.toBoolean(scope.tender.forceCreditAuth)) {
        // check if forceCreditAuth is true
        creditAction = 'CREDIT_FORCE'; // set creditAction
        authcode = scope.tender.creditAuth; // set authcode
      }

      $window.showMiniModal = true;
      var miniModal = $uibModal.open({
        // create promise and open modal
        backdrop: 'static',
        keyboard: false,
        //windowClass: 'full', // sets class on modal to full makes modal fill screen
        templateUrl: '/plugins/sample/cayan/EFT-Cayan-Mini-Device.htm', //set template url
        controller: [
          '$scope',
          '$window',
          '$uibModalInstance',
          'DocumentPersistedData',
          function($scope, $window, $uibModalInstance, DocumentPersistedData) {
            $scope.giveTender =
              DocumentPersistedData.DocumentInformation.Tenders.CurrentMode.toLowerCase() ===
              'give';

            $scope.swipe = function() {
              eftCayanService.Credit.Initiate({
                // Initiate credit transaction with the EFT service
                document_sid: document.sid, // set document_sid
                eft_invc_no: sequence, // set eft_invc_no
                entrymode: 'SWIPED',
                actiontype: creditAction, // set actiontype
                amount: scope.tender.amount, // set amount
                usegeniusdevice: true, // set usegeniusdevice
                forcedupcheck: dupCheck, // set forcedupcheck
                authcode: authcode // set authcode
              }).then(
                function(creditInit) {
                  // set what to do after CED responds with creditInit value
                  console.log('swipe', creditInit);
                  $timeout(function() {
                    window.location =
                      'Genius://v2/pos/?request={"Transportkey":"' +
                      creditInit[0].transportkey +
                      '","Format":"JSON","CallbackURL":"rpprism://geniusMini"}';
                    $uibModalInstance.close();
                  }, 100);
                },
                function(error) {
                  Toast.Error('1185', error.data[0].errormsg);
                  return false;
                }
              );
            };

            $scope.keyed = function() {
              eftCayanService.Credit.Initiate({
                // Initiate credit transaction with the EFT service
                document_sid: document.sid, // set document_sid
                eft_invc_no: sequence, // set eft_invc_no
                entrymode: 'KEYED',
                actiontype: creditAction, // set actiontype
                amount: scope.tender.amount, // set amount
                usegeniusdevice: true, // set usegeniusdevice
                forcedupcheck: dupCheck, // set forcedupcheck
                authcode: authcode // set authcode
              }).then(
                function(creditInit) {
                  // set what to do after CED responds with creditInit value
                  console.log('keyed', creditInit);
                  $timeout(function() {
                    window.location =
                      'Genius://v2/pos/?request={"Transportkey":"' +
                      creditInit[0].transportkey +
                      '","Format":"JSON","CallbackURL":"rpprism://geniusMini"}';
                    $uibModalInstance.close();
                  }, 100);
                },
                function(error) {
                  Toast.Error('1185', error.data[0].errormsg);
                  return false;
                }
              );
            };

            $scope.bypass = function() {
              $uibModalInstance.close('bypass');
            };

            $scope.cancel = function() {
              $uibModalInstance.dismiss();
            };

            // put watch on global variable to keep track of when to close modal
            $scope.watch = $scope.$watch(
              function() {
                return $window.showMiniModal;
              },
              function(newVal, oldVal) {
                if (!newVal) {
                  console.log('CayanConfig', newVal);
                  $uibModalInstance.close('close');
                }
              }
            );

            $scope.$on('$destroy', function() {
              $scope.watch();
            });
          }
        ]
      });

      miniModal.result.then(
        function(str, transportkey) {
          if (str === 'bypass') {
            creditTransaction(false);
          }
        },
        function() {
          angular.noop();
        }
      );
    }

    function init() {
      /** Added timeout to wait for TenderRound RPC call to finish and return
       * results in prism so that correct amount is being processed.
       */
      $timeout(function() {
        /** Logic check that starts the process if the tender amount is not zero and the tender type matches one of
         * the correct tender types supported by the EFT provider.
         **/
        if (
          // Credit Card
          (scope.tender.type === 2 ||
            // Gift Card
            scope.tender.type === 10 ||
            // Debit Card
            scope.tender.type === 11) &&
          prefs.eft_provider === '1'
        ) {
          //Verify that the tender amount is not zero
          if (parseFloat(scope.tender.amount) === 0) {
            // Throw error if amount is zero
            //'Amount must be greater than 0', 'Missing Amount'// Untranslated values for the message
            NotificationService.addAlert('2400', '2401').then(function() {
              // exit the plugin and cancel tender process
              deferred.reject();
            });
          } else {
            ModelService.get('Document', documentParams).then(function(data) {
              document = data[0];
              sequence = document.eft_invoice_number;

              if (TenderTypes.getPrefName(scope.tender.type)) {
                var tName = TenderTypes.getPrefName(scope.tender.type);

                var oTender =
                  getTenderRule(
                    prefs['pos_tenders_rules_' + tName],
                    'oTender'
                  ) * 1;
                var rMutiple =
                  getTenderRule(
                    prefs['pos_tenders_rules_' + tName],
                    'rMutiple'
                  ) * 1;
                if (scope.tender.mode.toLowerCase() === 'take') {
                  var checkVal =
                    scope.tender.amount - document.due_amt - rMutiple;
                  //avoid rounding errors with some extra math
                  var multiplicator = Math.pow(10, 2); //will be use decimal
                  var newVal = parseFloat(
                    (checkVal * multiplicator).toFixed(11)
                  );
                  checkVal = (Math.round(newVal) / multiplicator).toFixed(2);

                  // DT14129 - Allow overtender in excess of any rounding multiple
                  if (checkVal > oTender) {
                    // Untranslated Text ('Take Value Exceeds Maximum Over Tender Amount','Error')
                    NotificationService.addAlert('3276', '1185');
                    deferred.reject();
                    return;
                  }
                }
              }
              // Tender Rounding MUST be enforced if the client has it enabled in the admin console
              // The following function tries to make tender rounding as simple as possible,
              // use the example below, pass the tender and the document total and use the returned
              // amount for your processing.
              Transaction.Tender.roundTender(
                scope.tender,
                document.transaction_total_amt
              ).then(function(resp) {
                scope.tender.amount = resp.amount;
                switch (scope.tender.type) {
                  case 2:
                    // check preferences to see if merchant key has been defined
                    if (validConfig) {
                      // check preferences to see if CED has been enabled
                      if (
                        PrismUtilities.toBoolean(prefs.eft_mw_use_genius_device)
                      ) {
                        // check to see if workstation is configured to use genius mini
                        if (
                          PrismUtilities.toBoolean(
                            prefs.eft_mw_use_genius_mini_device
                          )
                        ) {
                          // hand off to genius mini device
                          initGeniusMini(false);
                        } else {
                          //hand off to genius device
                          geniusDevicePrompt(false);
                        }
                      } else {
                        // process credit card transaction without CED
                        creditTransaction(false);
                      }
                    } else {
                      // 'Configuration Error', The EFT Server is not configured correctly, use a different tender or update the configuration. // Untranslated values for the message
                      Toast.Error('6501', '6502');
                      deferred.reject();
                    }
                    break;
                  case 10:
                    // check preferences to see if merchant key has been defined
                    if (validConfig) {
                      // check preferences to see if CED has been enabled
                      eftCayanService.cayanAPI.checkUseGeniusDevicePref().then(function(response) {
                        processGiftCardTransaction(response);
                      });
                    } else {
                      Toast.Error('6501', '6502');
                      deferred.reject();
                    }
                    break;
                  case 11:
                    // check preferences to see if merchant key has been defined
                    if (validConfig) {
                      if (scope.tender.mode.toLowerCase() === 'give') {
                        //'Debit returns are not allowed.', 'No Debit Returns'//Untranslated values for the message
                        NotificationService.addNotification(
                          '2406',
                          '2407',
                          true,
                          true
                        );
                      } else {
                        //genius mini does not support debit
                        if (
                          PrismUtilities.toBoolean(
                            prefs.eft_mw_use_genius_mini_device
                          )
                        ) {
                          Toast.Error('1185', '6788');
                        } else {
                          //hand off to genius device
                          geniusDevicePrompt(false);
                        }
                      }
                    } else {
                      // exit the plugin and process the tender normally in Prism
                      Toast.Error('6501', '6502');
                      deferred.reject();
                    }
                    break;
                }
              });
            });
          }
        } else {
          // exit the plugin and process the tender normally in Prism
          deferred.resolve();
        }

        //reset execution pending status to false
        $timeout(function(){
          executionInPending = false;
        },2000);

      }, 500);
    }

    init();

    ////////////////////////////////////////////////////////
    /* callback from iOS shell when Cayan app completes  */
    window.geniusMiniCallback = function(data) {
      LoadingScreen.Enable = true;
      LoadingScreen.topMsg = 'Waiting for host.';
      scope.$apply(function() {
        var mData = JSON.parse(data.Result);
        var payload = [
          {
            MethodName: 'LogEvent',
            Params: {
              LogLevel: 3,
              LogType: 0,
              Message: 'geniusMiniCallback: ' + data.Result //print the stringified
            }
          }
        ];
        $http.post('v1/rpc', payload);
        if (mData.ValidationKey) {
          //need to pull from eftmv table to update status and check for transport details as needed
          $http
            .get(
              '/v1/rest/eftmw?filter=(validationkey,eq,' +
                mData.ValidationKey +
                ')&cols=*'
            )
            .then(
              function(response) {
                if (response && response.data && response.data.length) {
                  //update the status
                  var eftmvRecord = response.data[0];
                  eftmvRecord.approvalstatus = mData.Status;
                  eftCayanService.SetGeniusMiniStatus(eftmvRecord);

                  handleGeniusMiniStatus(mData, eftmvRecord);
                } else {
                  handleGeniusMiniStatus(mData);
                }
              },
              function(err) {
                handleGeniusMiniStatus(mData);
              }
            );
        } else {
          handleGeniusMiniStatus(mData);
        }
      });
    };

    function geniusDetailsByTransportKeyLoop(
      eftmvRecord,
      msg,
      record,
      index,
      defObj
    ) {
      if (!defObj) {
        defObj = $q.defer();
      }
      index = index ? index : 0;
      msg =
        msg !== 'Timeout waiting for host.' ? msg : 'Timeout waiting for host.';
      if (index < 30) {
        index++;
        var validConfig =
          eftmvRecord &&
          eftmvRecord.transportkey &&
          prefs.eft_mw_merchant_name.length > 0 &&
          prefs.eft_mw_merchant_site_id.length > 0 &&
          prefs.eft_mw_merchant_key.length > 0;
        if (validConfig) {
          $http
            .post('/api/backoffice?action=DetailsByTransportKey', {
              data: [{ TransportKey: eftmvRecord.transportkey }]
            })
            .then(
              function(response) {
                if (
                  'data' in response &&
                  response.data.data &&
                  response.data.data.length > 0
                ) {
                  var record =
                    response.data.data[0].detailsbytransportkeyresult;
                  var payload = [
                    {
                      MethodName: 'LogEvent',
                      Params: {
                        LogLevel: 3,
                        LogType: 0,
                        Message:
                          'TransportKey-'+eftmvRecord.transportkey+', DetailsByTransportKey: (' +
                          index +
                          ')' +
                          JSON.stringify(record)
                      }
                    }
                  ];
                  $http.post('v1/rpc', payload);
                  if (record.status.toLowerCase() !== 'approved') {
                    $timeout(function() {
                      geniusDetailsByTransportKeyLoop(
                        eftmvRecord,
                        msg,
                        record,
                        index,
                        defObj
                      );
                    }, 2000);
                  } else {
                    defObj.resolve(record);
                  }
                } else {
                  $timeout(function() {
                    geniusDetailsByTransportKeyLoop(
                      eftmvRecord,
                      msg,
                      record,
                      index,
                      defObj
                    );
                  }, 2000);
                }
              },
              function(err) {
                defObj.resolve('error');
                Toast.Error(
                  'Payment Error',
                  'An unknown error occurred.',
                  0,
                  true
                );
                $window.showMiniModal = false;
              }
            );
        } else {
          Toast.Error(
            'Payment Error',
            'Invalid payment configuration.',
            0,
            true
          );
          $window.showMiniModal = false;
          defObj.resolve('error');
        }
      } else {
        Toast.Error(
          'Payment Error',
          'Transaction failed, and details not available.',
          0,
          true
        );
        $window.showMiniModal = false;
        defObj.resolve('error');
      }

      return defObj.promise;
    }

    function handleGeniusMiniStatus(mData, eftmvRecord) {
      switch (mData.Status) {
        case 'UserCancelled': {
          Toast.Error('Payment Error', 'Payment cancelled by User.', 0, true);
          $window.showMiniModal = false;
          LoadingScreen.Enable = false;
          break;
        }
        case 'DECLINED_DUPLICATE': {
          Toast.Error(
            'Payment Error',
            'Declined due to duplicate transaction.',
            0,
            true
          );
          $window.showMiniModal = false;
          LoadingScreen.Enable = false;
          break;
        }
        case 'DECLINED': {
          Toast.Error('Payment Error', mData.ErrorMessage, 0, true);
          LoadingScreen.Enable = false;
          var EMVData = {};
          EMVData.card_type = mData.PaymentType ? mData.PaymentType : '';
          EMVData.cardnumber = mData.AccountNumber ? mData.AccountNumber : '';

          if (
            mData.AdditionalParameters &&
            mData.AdditionalParameters.hasOwnProperty('EMV')
          ) {
            //Stores the returned EMV Pin Statement
            EMVData.emv_pinstatement =
              mData.AdditionalParameters.EMV.PINStatement;

            if (
              mData.AdditionalParameters.EMV.hasOwnProperty(
                'ApplicationCryptogram'
              )
            ) {
              //Stores the returned EMV Crypto Cryptogram Type
              EMVData.emv_crypto_cryptogramtype =
                mData.AdditionalParameters.EMV.ApplicationCryptogram.CryptogramType;
              //Stores the returned EMV Crypto Cryptogram
              EMVData.emv_crypto_cryptogram =
                mData.AdditionalParameters.EMV.ApplicationCryptogram.Cryptogram;
            }
            if (
              mData.AdditionalParameters.EMV.hasOwnProperty(
                'ApplicationInformation'
              )
            ) {
              //Stores the returned EMV AI AID
              EMVData.emv_ai_aid =
                mData.AdditionalParameters.EMV.ApplicationInformation.Aid;
              //Stores the returned EMV AI APP LABEL
              EMVData.emv_ai_applabel =
                mData.AdditionalParameters.EMV.ApplicationInformation.ApplicationLabel;
              //Stores the returned EMV Card Exp Date
              EMVData.emv_ci_cardexpirydate =
                mData.AdditionalParameters.EMV.ApplicationInformation.ApplicationExpiryDate;
            }
          }
          if (
            EMVData.emv_ai_aid ||
            EMVData.emv_ai_applabel ||
            EMVData.emv_ci_cardexpirydate ||
            EMVData.emv_crypto_cryptogramtype ||
            EMVData.emv_crypto_cryptogram ||
            EMVData.emv_pinstatement
          ) {
            printDeclinedReceipt(EMVData);
          }
          $window.showMiniModal = false;
          break;
        }
        case 'REFERRAL': {
          Toast.Error('Payment Error', 'Declined due to referral.', 0, true);
          $window.showMiniModal = false;
          LoadingScreen.Enable = false;
          break;
        }
        case 'FAILED': {
          var msg = '';
          if (mData.ErrorMessage && mData.ErrorMessage.length) {
            msg = mData.ErrorMessage;
          } else {
            msg = 'Timeout waiting for host.';
          }

          geniusDetailsByTransportKeyLoop(eftmvRecord, msg).then(function(
            record
          ) {
            if (record !== 'error') {
              if (record.status === 'APPROVED') {
                if (eftmvRecord) {
                  eftmvRecord.approvalstatus = 'APPROVED';
                  eftmvRecord.row_version = eftmvRecord.row_version + 1; //second update

                  eftCayanService.SetGeniusMiniStatus(eftmvRecord);
                }

                if (record.paymentdetails && record.paymentdetails.length) {
                  // create tender
                  var newTender = ModelService.create('Tender');
                  // add values to this tender

                  // set tender type
                  newTender.tender_type = mData.PaymentType === 'GIFT' ? 10 : 2;
                  // set document sid
                  newTender.document_sid = document.sid;
                  // set authorization code
                  newTender.authorization_code =
                    record.paymentdetails[0].authorizationcode;
                  // tender mode
                  if (
                    record.paymentdetails[0].transactiontype === 'SALE' ||
                    record.paymentdetails[0].transactiontype === 'FORCE'
                  ) {
                    newTender.taken = record.totalamountapproved;
                  } else if (
                    record.paymentdetails[0].transactiontype === 'REFUND'
                  ) {
                    newTender.given = record.totalamountapproved;
                  }
                  newTender.card_number =
                    record.paymentdetails[0].accountnumber;
                  // set tender name
                  newTender.tender_name = record.paymentdetails[0].paymenttype;
                  // sets transaction id from returned token
                  newTender.eft_transaction_id = record.paymentdetails[0].token;
                  //Stores the returned card type into EFT Data field
                  newTender.eftdata1 = record.paymentdetails[0].paymenttype;

                  document.addTender(newTender).then(
                    function() {
                      LoadingScreen.Enable = false;
                      // Reload page to show added tenders
                      scope.init();
                    },
                    function(error) {
                      LoadingScreen.Enable = false;
                      // handle error
                    }
                  );
                }
                $window.showMiniModal = false;
              } else {
                LoadingScreen.Enable = false;
                $window.showMiniModal = false;
              }
            } else {
              LoadingScreen.Enable = false;
              $window.showMiniModal = false;
            }
          });
          break;
        }
        case 'APPROVED': {
          // create tender
          var newTender = ModelService.create('Tender');
          // add values to this tender

          // set tender type
          newTender.tender_type = mData.PaymentType === 'GIFT' ? 10 : 2;

          // set document sid
          newTender.document_sid = document.sid;
          // set authorization code
          newTender.authorization_code = mData.AuthorizationCode;
          // tender mode
          if (
            mData.TransactionType === 'SALE' ||
            mData.TransactionType === 'FORCE'
          ) {
            newTender.taken = mData.AmountApproved;
          } else if (mData.TransactionType === 'REFUND') {
            newTender.given = mData.AmountApproved;
          }
          newTender.card_number = mData.AccountNumber;
          // set tender name
          newTender.tender_name = mData.PaymentType;
          // sets transaction id from returned token
          newTender.eft_transaction_id = mData.Token;
          //Stores the returned card type into EFT Data field
          newTender.eftdata1 = mData.PaymentType;

          if (
            mData.AdditionalParameters &&
            mData.AdditionalParameters.hasOwnProperty('EMV')
          ) {
            //Stores the returned EMV Pin Statement
            newTender.emv_pinstatement =
              mData.AdditionalParameters.EMV.PINStatement;

            if (
              mData.AdditionalParameters.EMV.hasOwnProperty(
                'ApplicationCryptogram'
              )
            ) {
              //Stores the returned EMV Crypto Cryptogram Type
              newTender.emv_crypto_cryptogramtype =
                mData.AdditionalParameters.EMV.ApplicationCryptogram.CryptogramType;
              //Stores the returned EMV Crypto Cryptogram
              newTender.emv_crypto_cryptogram =
                mData.AdditionalParameters.EMV.ApplicationCryptogram.Cryptogram;
            }
            if (
              mData.AdditionalParameters.EMV.hasOwnProperty(
                'ApplicationInformation'
              )
            ) {
              //Stores the returned EMV AI AID
              newTender.emv_ai_aid =
                mData.AdditionalParameters.EMV.ApplicationInformation.Aid;
              //Stores the returned EMV AI APP LABEL
              newTender.emv_ai_applabel =
                mData.AdditionalParameters.EMV.ApplicationInformation.ApplicationLabel;
              //Stores the returned EMV Card Exp Date
              newTender.emv_ci_cardexpirydate =
                mData.AdditionalParameters.EMV.ApplicationInformation.ApplicationExpiryDate;
            }
          }

          document.addTender(newTender).then(
            function() {
              // Reload page to show added tenders
              scope.init();
            },
            function(error) {
              // handle error
            }
          );
          LoadingScreen.Enable = false;
          $window.showMiniModal = false;
          break;
        }
      }
    }
    }
    return deferred.promise;
  }
);

/**
 * ButtonHook to run before the posTenderGiftCardBalance event
 * Processes gift card information through EFT provider and returns with the balance of gift card
 */
ButtonHooksManager.addHandler(
  ['before_posTenderGiftCardBalance', 'before_posOptionsGiftCardBalance'],
  function($q, eftCayanService) {
    var deferred = $q.defer();
    eftCayanService.cayanAPI.checkUseGeniusDevicePref().then(function(response) {
      eftCayanService.cayanAPI
        .openGiftCardProcessDialog({
          useCED: response,
          actionType: 'GIFT_BALANCE',
          titleId: '932'
        })
        .then(function(giftCardResponsePayload) {
          //???
        });
    });
    return deferred.promise;
  }
);

/**
 * ButtonHook to run before the posTenderGiftCardAddValue event
 * Processes gift card information through EFT provider and adds the inserted value to the indicated gift card
 */
ButtonHooksManager.addHandler('before_posTenderGiftCardAddValue', function(
  $q,
  eftCayanService
) {
  var deferred = $q.defer();
  eftCayanService.cayanAPI.checkUseGeniusDevicePref().then(function(response) {
    eftCayanService.cayanAPI
      .openGiftCardProcessDialog({
        useCED: response,
        actionType: 'GIFT_GIVE',
        titleId: '931'
      })
      .then(
        function(giftCardResponsePayload) {
          if (giftCardResponsePayload) {
            eftCayanService.cayanAPI.addGiftCardPurchaseTenderToDocument(
              giftCardResponsePayload
            );
          }
        },
        function(error) {
          eftCayanService.cayanAPI.ShowError(error.data);
        }
      );
  });
  return deferred.promise;
});

/**
 * ButtonHook to run before the posTenderGiftCardPurchase event
 * Processes gift card information through EFT provider and adds the inserted value to a new gift card
 */
ButtonHooksManager.addHandler('before_posTenderGiftCardPurchase', function(
  $q,
  eftCayanService
) {
  var deferred = $q.defer();
  eftCayanService.cayanAPI.checkUseGeniusDevicePref().then(function(response) {
    eftCayanService.cayanAPI
      .openGiftCardProcessDialog({
        useCED: response,
        actionType: 'GIFT_ACTIVATE',
        titleId: '930'
      })
      .then(
        function(giftCardResponsePayload) {
          if (giftCardResponsePayload) {
            eftCayanService.cayanAPI.addGiftCardPurchaseTenderToDocument(
              giftCardResponsePayload
            );
          }
        },
        function(error) {
          eftCayanService.cayanAPI.ShowError(error.data);
        }
      );
  });
  return deferred.promise;
});

/**
 * ButtonHook to run before the posOptionsGiftCardBalance event
 * Processes gift card information through EFT provider and returns with the balance of gift card and allows for printing
 */

/** Angular Service to monitor the URL in the browser to match the return URL from Cayan **/
angular
  .module('prismPluginsSample.module.cayanRouteModule', [], null)
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider // run the state provider service
      .state('transactionEditTenderMWTenderStatus', {
        // name the state were watching for
        url:
          '/register/pos/:screen/:document_sid/:mode/tender/eftmw/:eftmwSid/:tendermode/:auth', // specify the URL that we are using : indicates a variable
        templateUrl: function() {
          return '/plugins/sample/cayan/EFT-Cayan-SigCap.htm'; // set the URL for the modal that will pop
        },
        controller: 'cayanSigCapController', // set the controller for modal
        resolve: {
          Document: [
            'ModelService',
            '$stateParams',
            function(ModelService, $stateParams) {
              // send the modelservice and stateparams into the modal
              var docParams = {
                // specify data to send inside the Document variable
                sid: $stateParams.document_sid, // set doc sid from the URL
                cols: { sid: $stateParams.document_sid, cols: '*' } // specify cols argument to send
              };

              return ModelService.get('Document', docParams).then(function(
                data
              ) {
                // specify what to return
                var doc = data[0];
                doc.params = docParams;
                return doc;
              });
            }
          ]
        }
      });
  });
