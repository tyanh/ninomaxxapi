window.angular.module('prismPluginsSample.controller.cayanCancelController', [])
  .controller('cayanCancelController', ['$scope', '$uibModalInstance',
    function ($scope, $uibModalInstance) {
      'use strict';

      $scope.closeForm = function (closeAction) { // define function to close modal
        $uibModalInstance.close(closeAction); // close the modal, pass the closeAction variable
      };
    }
  ]);
