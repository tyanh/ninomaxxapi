window.angular
  .module('prismPluginsSample.controller.cayanGiftCardController', [])
  .controller('cayanGiftCardController', [
    '$scope',
    '$uibModalInstance',
    'CayanData',
    'NotificationService',
    '$timeout',
    'DocumentPersistedData',
    '$translate',
    'ModelService',
    'prismSessionInfo',
    'PrintersService',
    'ShopperDisplay',
    'eftCayanService',
    'Toast',
    '$window',
    'ExternalDataService',
    'PrismUtilities',
    '$http',
    'CreditCardTypes',
    function(
      $scope,
      $uibModalInstance,
      CayanData,
      NotificationService,
      $timeout,
      DocumentPersistedData,
      $translate,
      ModelService,
      prismSessionInfo,
      PrintersService,
      ShopperDisplay,
      eftCayanService,
      Toast,
      $window,
      ExternalDataService,
      PrismUtilities,
      $http,
      CreditCardTypes
    ) {
      'use strict';

      $scope.cayanData = CayanData;
      $scope.device = {
        ready: false,
        validating: false
      };
      $scope.cedDataInProgress = {};
      $scope.timeoutID = 0;
      $scope.cedApproved = false;
      $scope.showBalance = false;
      $scope.giftCardBalance = 0;
      $scope.keyInMode = false;
      $scope.form = {
        addValue: $scope.cayanData.forTender
          ? ExternalDataService.scope.tender.amount
          : 0,
        swipeRawTrackData: '',
        keyedInCardNo: ''
      };
      $scope.actionButtons = [];
      $scope.printData = {};
      $scope.byPassFlag = false;
      $scope.processing = false;
      $scope.inProcess = {
        titleMsg: '',
        detailMsg: ''
      };
      $scope.showSuggestion = false;
      $scope.infoMessage = '';
      $scope.focussedElement = null;
      $scope.useGeniusMini = PrismUtilities.toBoolean(
        prismSessionInfo.get().preferences.eft_mw_use_genius_mini_device
      );
      var mwCardTypes = CreditCardTypes.getMWCardTypes();

      $scope.handleModalClose = function(data) {
        if (data) {
          if (
            data.mwdata.errormessage.length > 0 ||
            data.mwdata.approvalstatus.toLowerCase() === 'declined'
          ) {
            //if error message then toast error
            Toast.Error(data.mwdata.approvalstatus, data.mwdata.errormessage);
            $uibModalInstance.close();
          }
          if ($scope.cayanData.actionType === 'GIFT_BALANCE') {
            //show balance
            //set card balance in print payload
            $scope.showBalance = true;
            $scope.giftCardBalance = data.mwdata.balance;
            $scope.printData.card_number = data.mwdata.cardnumber;
            $scope.printData.card_balance = data.mwdata.balance;
            $scope.processing = false;
            $scope.showSuggestion = false;
            $scope.setButtons();
          } else {
            //append document info to response payload
            data.document = $scope.document;
            $uibModalInstance.close(data);
          }
        } else {
          $uibModalInstance.close();
        }
      };

      $scope.abortCED = function() {
        //CANCEL CED action
        eftCayanService.cayanAPI
          .cancelCedProcess({
            cedSid: $scope.cedDataInProgress.cedSid,
            eftmwSid: $scope.cedDataInProgress.eftmwData.sid
          })
          .then(function(data) {
            //SHOW warning if the CED action is successfully cancelled ???
            $scope.cedDataInProgress = {};
            $timeout.cancel($scope.timeoutID);
            if ($scope.byPassFlag) {
              $scope.processWithCardReaderOrKeyin();
            } else {
              $scope.handleModalClose();
            }
          });
      };

      // checks the status of current Eftmw record
      $scope.checkStatus = function() {
        if ($scope.cedDataInProgress.eftmwData) {
          var addtender =
            $scope.cayanData.actionType !== 'GIFT_BALANCE' ? true : false;
          var mode =
            $scope.cayanData.actionType === 'GIFT_TAKE' ? 'take' : 'give';
          eftCayanService.cayanAPI
            .getEftmwRecord($scope.cedDataInProgress.eftmwData.sid)
            .then(function(eftmwData) {
              var status = eftmwData.approvalstatus.toLowerCase();
              // check if the prism is still waiting on response from CED
              if (status !== 'prism_awaiting_ced') {
                switch (status) {
                  case 'approved':
                    // if approved & action type is not gift balance check
                    // notify to add tender & return data from CED
                    $scope.handleModalClose({
                      addtender: addtender,
                      mode: mode,
                      mwdata: eftmwData
                    });
                    break;
                  case 'cancelled':
                  case 'poscancelled':
                    // if cancelled, send redraw command to shopper display plugin
                    // set time out id to cancel function
                    // close modal advising not to add the tender
                    ShopperDisplay.Redraw();
                    $timeout.cancel($scope.timeoutID);
                    $scope.handleModalClose({
                      addtender: addtender,
                      mode: mode,
                      mwdata: {
                        errormessage: 'POSCANCELLED',
                        approvalstatus: 'Cancelled'
                      }
                    });
                    break;
                  case 'ced_already_approved':
                    if (!$scope.cedApproved) {
                      // make sure the already approved message hasnt been seen before
                      // 'You may not Bypass or Cancel at this time.'
                      // 'Transaction Already Approved'
                      // set flag for alert has been shown once.
                      NotificationService.addAlert('2910', '2911');
                      $scope.device.ready = false;
                      $scope.setInProcessInfo();
                      $scope.cedApproved = true;
                    }
                    $timeout.cancel($scope.timeoutID);
                    $scope.timeoutID = $timeout(function() {
                      $scope.checkStatus();
                    }, 500);
                    break;
                  case 'validating':
                    $scope.timeoutID = $timeout(function() {
                      $scope.device.validating = true;
                      $scope.setInProcessInfo();
                      $scope.checkStatus();
                    }, 500);
                    break;

                  default:
                    $scope.handleModalClose({
                      addtender: addtender,
                      mode: mode,
                      mwdata: eftmwData
                    });
                    break;
                }
              } else {
                // if still waiting for the CED to respond, then poll every half second
                $scope.timeoutID = $timeout(function() {
                  $scope.checkStatus();
                }, 500);
              }
              $scope.setButtons();
            });
        }
      };

      $scope.printBalance = function() {
        $scope.processing = true;
        $scope.setInProcessInfo();
        // get store data to be able to send to custom print job
        ModelService.get('Store', {
          sid: prismSessionInfo.get().storesid,
          cols:
            'address1,address2,address3,address4,address5,phone1,store_name,store_code,store_number,zip'
        }).then(function(data) {
          $scope.store = data[0];
          $scope.printData.store_number = data[0].store_number;
          $scope.printData.store_code = data[0].store_code;
          $scope.printData.store_name = data[0].store_name;
          $scope.printData.store_address1 = data[0].address1;
          $scope.printData.store_address2 = data[0].address2;
          $scope.printData.store_address3 = data[0].address3;
          $scope.printData.store_address4 = data[0].address4;
          $scope.printData.store_address5 = data[0].address5;
          $scope.printData.store_zip = data[0].zip;
          $scope.printData.store_phone = data[0].phone1;
          $scope.printData.cardmax = '';
          $scope.printData.cardexpire = '';
          DocumentPersistedData.PrintDesignData.Payload = $scope.printData;
          PrintersService.printAction(null, 'Gift Card Balance', [
            $scope.printData
          ]);
          $scope.handleModalClose();
        });
      };

      $scope.setInProcessInfo = function() {
        if ($scope.processing && $scope.cayanData.useCED) {
          if (!$scope.device.ready) {
            $scope.inProcess.titleMsg = $translate.instant('3353');
            $scope.inProcess.detailMsg = $translate.instant('3354');
          } else if (!$scope.device.validating) {
            $scope.inProcess.titleMsg = $translate.instant('1940');
            $scope.inProcess.detailMsg = $translate.instant('1941');
          } else if ($scope.validating) {
            $scope.inProcess.titleMsg = $translate.instant('3353');
            $scope.inProcess.detailMsg = $translate.instant('6353');
          }
        } else if ($scope.processing && !$scope.cayanData.useCED) {
          if (!$scope.keyInMode) {
            $scope.inProcess.titleMsg = $translate.instant('1982');
            $scope.inProcess.detailMsg = '';
          } else {
            $scope.inProcess.titleMsg = $translate.instant('1982');
            $scope.inProcess.detailMsg = '';
          }
        }
      };

      $scope.removedFocus = function() {
        $scope.focussedElement = null;
        $scope.updateInfoMessage();
      };

      $scope.updateInfoMessage = function() {
        if (!$scope.keyInMode || !$scope.processing) {
          if ($scope.focussedElement) {
            if ($scope.cayanData.actionType !== 'GIFT_BALANCE') {
              if ($scope.form.addValue > 0) {
                $scope.infoMessage =
                  $scope.focussedElement.id === 'txtCardNumber'
                    ? $translate.instant('Use card reader now')
                    : $translate.instant('Click process to use card reader');
              } else {
                $scope.infoMessage = $translate.instant(
                  'Enter amount and click process'
                );
              }
            } else {
              $scope.infoMessage =
                $scope.focussedElement.id === 'txtCardNumber'
                  ? $translate.instant('Use card reader now')
                  : $translate.instant('Click process to use card reader');
            }
          } else {
            $scope.infoMessage = $translate.instant(
              'Click process to use card reader'
            );
          }
        } else {
          $scope.focussedElement = null;
        }
      };

      $scope.focusInput = function(inputTypeFlag) {
        //set focus using inputTypeFlag
        //0=add value,
        //1=key-in card number input,
        //2=hidden card reader number input
        switch (inputTypeFlag) {
          case 0:
            $scope.focussedElement = $window.document.getElementById(
              'addValueInput'
            );
            break;
          case 1:
            $scope.focussedElement = $window.document.getElementById(
              'txtKeyedCardNumber'
            );
            break;
          case 2:
            $scope.focussedElement = $window.document.getElementById(
              'txtCardNumber'
            );
            break;
          default:
            break;
        }
        $scope.focussedElement.select();
      };

      $scope.processWithCardReaderOrKeyin = function(keyInFlag) {
        $scope.keyInMode = $scope.useGeniusMini ? true : !!keyInFlag;
        $scope.processing = false;
        $scope.byPassFlag = false;
        $scope.cayanData.useCED = false;
        $scope.showBalance = false;
        $scope.showSuggestion = !$scope.keyInMode ? true : false;
        $scope.setInProcessInfo();
        $scope.setButtons();
        $timeout(function() {
          if ($scope.cayanData.actionType === 'GIFT_BALANCE') {
            if ($scope.keyInMode) {
              $scope.focusInput(1);
            } else {
              $scope.focusInput(2);
            }
          } else if ($scope.form.addValue > 0) {
            if ($scope.keyInMode) {
              $scope.focusInput(1);
            } else {
              $scope.focusInput(2);
            }
          } else {
            $scope.focusInput(0);
          }
          if (!$scope.infoMessage) {
            $scope.updateInfoMessage();
          }
        }, 100);
      };

      $scope.processWithCed = function() {
        $scope.processing = true;
        $scope.setInProcessInfo();
        $scope.setButtons();
        //POST EFTMW record
        var eftmwPayload = {
          sid: DocumentPersistedData.DocumentInformation.Sid,
          actionType: $scope.cayanData.actionType,
          useCED: $scope.cayanData.useCED,
          amount: $scope.form.addValue
        };
        eftCayanService.cayanAPI
          .postEftmwRecord(eftmwPayload)
          .then(function(eftmwData) {
            $scope.cedDataInProgress.eftmwData = eftmwData;
            var transportkey = eftmwData.transportkey;
            //GET CED record
            eftCayanService.cayanAPI.getCedRecord().then(function(ceddata) {
              if (ceddata) {
                //Use genius device
                $scope.cedDataInProgress.cedSid = ceddata.sid;
                //POST CED record with EFTMW sid & transportkey
                var cedPayload = {
                  cedSid: $scope.cedDataInProgress.cedSid,
                  eftmwSid: $scope.cedDataInProgress.eftmwData.sid,
                  transportkey: transportkey
                };
                eftCayanService.cayanAPI
                  .postCedWithEftmw(cedPayload)
                  .then(function(cedWithEftmwdata) {
                    //SHOW waiting msg for customer
                    $scope.device.ready = true;
                    $scope.setInProcessInfo();
                    $scope.setButtons();
                    //START polling using 'eftmwData.sid'
                    $scope.timeoutID = $timeout(function() {
                      $scope.checkStatus();
                    }, 5000);
                  },
                  function(errorData) {
                    Toast.Error(
                      $translate.instant('1185'),
                      errorData.data[0].errormsg
                    );
                    $scope.handleModalClose();
                  });
              } else {
                if ($scope.useGeniusMini) {
                  //Use genius mini and watch for response
                  $scope.device.ready = true;
                  $scope.setInProcessInfo();
                  $scope.setButtons();
                  $timeout(function() {
                    window.location =
                      'Genius://v2/pos/?request={"Transportkey":"' +
                      transportkey +
                      '","Format":"JSON","CallbackURL":"rpprism://geniusMini"}';
                  }, 100);
                } else {
                  $scope.cedDataInProgress = {};
                  //Below code block could be re-usable?
                  NotificationService.addConfirm(
                    'No Genius device was found. Do you wish to proceed with bypass option?',
                    '2798'
                  ).then(function(response) {
                    if (response) {
                      $scope.processWithCardReaderOrKeyin();
                    } else {
                      $scope.handleModalClose();
                    }
                  });
                }
              }
            });
          },
          function(errorData) {
            Toast.Error(
              $translate.instant('1185'),
              errorData.data[0].errormsg
            );
            $scope.handleModalClose();
          });
      };

      $scope.$watch('form.swipeRawTrackData', function(newValue, oldValue) {
        if (newValue !== oldValue && newValue.length === 479) {
          $scope.processKeyedEntry();
        }
      });

      $scope.processKeyedEntry = function() {
        if (!$scope.isProcessActionDisabled()) {
          $scope.processing = true;
          //reset msgs
          $scope.setInProcessInfo();
          //reset Buttons
          $scope.setButtons();
          //POST EFTMW with keyed card info based on swipe/key-in
          var eftmwPayload = {
            sid: DocumentPersistedData.DocumentInformation.Sid,
            actionType: $scope.cayanData.actionType,
            useCED: $scope.cayanData.useCED,
            cardnumber: $scope.form.keyedInCardNo,
            rawtrackdata: $scope.form.swipeRawTrackData,
            amount: $scope.form.addValue
          };
          eftCayanService.cayanAPI.postEftmwRecord(eftmwPayload).then(
            function(eftmwData) {
              $scope.processing = false;
              $scope.setInProcessInfo();
              //Update showBalance & giftCardBalance values to display if actionType is GIFT_BALANCE
              $scope.handleModalClose({
                addtender:
                  $scope.cayanData.actionType !== 'GIFT_BALANCE' ? true : false,
                mode:
                  $scope.cayanData.actionType === 'GIFT_TAKE' ? 'take' : 'give',
                mwdata: eftmwData
              });
            },
            function(errorData) {
              Toast.Error(
                $translate.instant('1185'),
                errorData.data[0].errormsg
              );
              $scope.handleModalClose();
            }
          );
        }
      };

      $scope.isProcessActionDisabled = function() {
        if ($scope.cayanData.actionType !== 'GIFT_BALANCE') {
          if ($scope.cayanData.useCED) {
            return $scope.form.addValue <= 0;
          } else {
            return $scope.keyInMode
              ? $scope.form.addValue <= 0 ||
                  $scope.form.keyedInCardNo.length < 13
              : $scope.form.addValue <= 0;
          }
        } else {
          if (!$scope.cayanData.useCED) {
            if ($scope.keyInMode) {
              return $scope.form.keyedInCardNo.length < 13;
            } else {
              return false;
            }
          } else {
            return true;
          }
        }
      };

      $scope.setButtons = function() {
        $scope.actionButtons = [
          {
            id: 'processActionButton',
            name: 'Process',
            eventName: 'posMWGiftCardProcess',
            show: !$scope.showBalance,
            disabled: $scope.isProcessActionDisabled() || $scope.processing,
            translateId: '1427',
            action: function() {
              if ($scope.cayanData.useCED) {
                $scope.processWithCed();
              } else if ($scope.keyInMode) {
                $scope.processKeyedEntry();
              } else if (
                $scope.cayanData.actionType === 'GIFT_BALANCE' ||
                (!$scope.keyInMode &&
                  (!$scope.focussedElement ||
                    $scope.focussedElement.id !== 'txtCardNumber'))
              ) {
                $scope.focusInput(2);
              }
              $scope.updateInfoMessage();
            }
          },
          {
            id: 'bypassButton',
            name: 'Bypass Device',
            show: $scope.cayanData.useCED && !$scope.showBalance,
            disabled: $scope.useGeniusMini
              ? $scope.device.ready
              : $scope.cayanData.useCED && !$scope.device.ready,
            translateId: '1942',
            action: function() {
              if (!$scope.useGeniusMini) {
                $scope.byPassFlag = true;
                $scope.abortCED();
              } else {
                $scope.cedDataInProgress = {};
                $scope.processWithCardReaderOrKeyin();
              }
            }
          },
          {
            id: 'cayanGiftCardChangeEntryMethodButton',
            name: 'Key in Card Number',
            show:
              !$scope.cayanData.useCED &&
              !$scope.showBalance &&
              !$scope.keyInMode,
            disabled: $scope.processing,
            translateId: 'Switch to Key in',
            action: function() {
              $scope.processWithCardReaderOrKeyin(true);
            }
          },
          {
            id: 'cayanGiftCardChangeEntryMethodButton',
            name: 'Card Reader Swipe',
            show:
              !$scope.cayanData.useCED &&
              !$scope.showBalance &&
              $scope.keyInMode,
            disabled: $scope.processing || $scope.useGeniusMini,
            translateId: 'Switch to Swipe',
            action: function() {
              $scope.processWithCardReaderOrKeyin();
            }
          },
          {
            id: 'cayanPrintBalanceButton',
            name: 'Print',
            show: $scope.showBalance,
            disabled: false,
            translateId: '522',
            action: function() {
              $scope.printBalance();
            }
          },
          {
            id: 'cancelButton',
            name: 'Cancel',
            show: true,
            disabled: $scope.cayanData.useCED
              ? $scope.useGeniusMini && $scope.device.ready
                ? true
                : $scope.processing && !$scope.device.ready
              : $scope.processing,
            translateId: '1.2',
            action: function() {
              if ($scope.cedDataInProgress.eftmwData) {
                $scope.abortCED();
              } else {
                $scope.handleModalClose();
              }
            }
          }
        ];
      };

      $scope.init = function() {
        $scope.setButtons();
        $scope.document = {};
        //ActionType is GIFT_ACTIVATE(purchase) or GIFT_GIVE(add value)
        if ($scope.cayanData.actionType !== 'GIFT_BALANCE') {
          //GET document info => This call can be moved to later part of code i.e., in service???
          ModelService.get('Document', {
            sid: DocumentPersistedData.DocumentInformation.Sid,
            cols: 'sid,transaction_total_amt'
          }).then(function(data) {
            $scope.document = data[0];
            //end of GET
            $scope.focusInput(0);
            if ($scope.cayanData.useCED && $scope.form.addValue > 0) {
              $scope.processWithCed();
            }
            if (!$scope.cayanData.useCED) {
              $scope.processWithCardReaderOrKeyin();
            }
          });
        } else {
          //ActionType is GIFT_BALANCE(balance check)
          if ($scope.cayanData.useCED) {
            $scope.processWithCed();
          } else {
            $scope.processWithCardReaderOrKeyin();
          }
        }
      };

      $scope.init();

      ////////////////////////////////////////////////////////
      /* callback from iOS shell when Cayan app completes  */
      window.geniusMiniCallback = function(data) {
        ExternalDataService.scope.$apply(function() {
          var mData = JSON.parse(data.Result);
          var payload = [
            {
              MethodName: 'LogEvent',
              Params: {
                LogLevel: 3,
                LogType: 0,
                Message: mData
              }
            }
          ];
          $http.post('v1/rpc', payload);
          $scope.device.ready = false;
          $scope.processing = false;
          if (
            $scope.cedDataInProgress.eftmwData &&
            mData.Status.toLowerCase() === 'approved'
          ) {
            //UPDATE EFTMW record and show results
            var eftmvRecord = $scope.cedDataInProgress.eftmwData;
            eftmvRecord.authcode = mData.AuthorizationCode;
            eftmvRecord.cardnumber = mData.AccountNumber;
            eftmvRecord.amount =
              parseFloat(mData.AmountApproved) > 0
                ? parseFloat(mData.AmountApproved)
                : parseFloat($scope.form.addValue);
            eftmvRecord.balance = mData.AdditionalParameters.Loyalty.Balances
              ? parseFloat(
                  mData.AdditionalParameters.Loyalty.Balances.AmountBalance
                )
              : 0;
            for (var i = 0; i < mwCardTypes.length; i++) {
              if (mwCardTypes[i].sid === mData.PaymentType) {
                eftmvRecord.paymenttype = i;
              }
            }
            eftmvRecord.token = mData.Token;
            eftmvRecord.approvalstatus = mData.Status;
            eftmvRecord.errormessage = mData.ErrorMessage;
            $http({
              method: 'PUT',
              url:
                '/v1/rest/eftmw/' +
                eftmvRecord.sid +
                '?cols=*&filter=(row_version,eq,' +
                eftmvRecord.row_version +
                ')',
              data: [eftmvRecord]
            }).then(
              function(data) {
                eftCayanService.cayanAPI
                  .getEftmwRecord(eftmvRecord.sid)
                  .then(function(updatedEftmwData) {
                    $scope.cedDataInProgress = {};
                    $scope.handleModalClose({
                      addtender:
                        $scope.cayanData.actionType !== 'GIFT_BALANCE'
                          ? true
                          : false,
                      mode:
                        $scope.cayanData.actionType === 'GIFT_TAKE'
                          ? 'take'
                          : 'give',
                      mwdata: updatedEftmwData
                    });
                  });
              },
              function(error) {
                $scope.handleModalClose();
                Toast.Error('1185', error[0].errormsg);
              }
            );
          } else if (
            $scope.cedDataInProgress.eftmwData &&
            mData.Status.toLowerCase() === 'usercancelled'
          ) {
            //clear in progress Ced Data
            //set buttons
            //show suggestion to use bypass option
            $scope.cedDataInProgress = {};
            $scope.setButtons();
            $scope.showSuggestion = true;
            $scope.infoMessage = $translate.instant(
              'Process cancelled by user, try using bypass device'
            );
          } else {
            $scope.handleModalClose();
            Toast.Error(mData.Status, mData.ErrorMessage);
          }
        });
      };
    }
  ]);
