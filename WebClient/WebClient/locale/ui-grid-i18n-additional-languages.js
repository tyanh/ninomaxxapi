// AR - arabic - AR-UE
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate','$translate', function($delegate,$translate) {
      $delegate.add('ar', {
        aggregate: {
          label: 'العناصر'
        },
        groupPanel: {
          description: 'اسحب رأس العمود هنا وأسقطه على مجموعة من هذا العمود.'
        },
        search: {
          placeholder: 'بحث',
          showingItems: 'عرض البنود:',
          selectedItems: 'الأصناف المختارة:',
          totalItems: 'مجموع الوحدات:',
          size: 'حجم الصفحة:',
          first: 'الصفحة الأولى',
          next: 'الصفحة التالية',
          previous: 'الصفحة السابقة',
          last: 'آخر صفحة'
        },
        menu: {
          text: 'اختيار الأعمدة:'
        },
        sort: {
          ascending: 'فرز تصاعدي',
          descending: 'ترتيب تنازلي',
          remove: 'إزالة ترتيب'
        },
        column: {
          hide: 'إخفاء العمود'
        },
        aggregation: {
          count: 'إجمالي الصفوف:',
          sum: 'مجموع:',
          avg: 'متوسط:',
          min: 'مين:',
          max: 'الأقصى:'
        },
        pinning: {
          pinLeft: 'دبوس اليسار',
          pinRight: 'دبوس اليمين',
          unpin: 'إزالة التثبيت'
        },
        gridMenu: {
          columns: 'الأعمدة:',
          importerTitle: 'استيراد ملف',
          exporterAllAsCsv: 'تصدير جميع البيانات كما CSV',
          exporterVisibleAsCsv: 'تصدير البيانات مرئية كما CSV',
          exporterSelectedAsCsv: 'تصدير مختارة البيانات كما CSV',
          exporterAllAsPdf: 'تصدير جميع البيانات وقوات الدفاع الشعبي',
          exporterVisibleAsPdf: 'تصدير بيانات مرئية لقوات الدفاع الشعبي',
          exporterSelectedAsPdf: 'تصدير مختارة البيانات وقوات الدفاع الشعبي'
        },
        importer: {
          noHeaders: 'وكانت أسماء الأعمدة غير قادرة على أن تستمد، لا ملف لديهم رأس؟',
          noObjects: 'كانت كائنات غير قادرة على أن تستمد، كان هناك البيانات في ملف آخر من رؤوس؟',
          invalidCsv: 'كان الملف غير قادر على معالجتها، هو CSV صالحة؟',
          invalidJson: 'كان الملف غير قادر على معالجتها، هل هو صحيح سلمان؟',
          jsonNotArray: 'يجب أن يحتوي ملف سلمان المستوردة صفيف، إجهاض.'
        },
        pagination: {
          sizes: 'مواد لكل صفحة',
          totalItems: 'العناصر'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//EL - Greek - EL-GR
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('el', {
        aggregate: {
          label: 'αντικειμένων'
        },
        groupPanel: {
          description: 'Σύρετε μια κεφαλίδα στήλης εδώ και αφήστε το στην ομάδα από αυτή τη στήλη.'
        },
        search: {
          placeholder: 'Ψάξιμο...',
          showingItems: 'Εμφάνιση Είδη:',
          selectedItems: 'Επιλεγμένα στοιχεία:',
          totalItems: 'Σύνολο Στοιχείων:',
          size: 'Μέγεθος σελίδας:',
          first: 'Πρώτη σελίδα',
          next: 'Επόμενη σελίδα',
          previous: 'Προηγούμενη σελίδα',
          last: 'Τελευταία σελίδα'
        },
        menu: {
          text: 'Επιλέξτε Στήλες:'
        },
        sort: {
          ascending: 'Ταξινόμηση Αύξουσα',
          descending: 'Φθίνουσα Ταξινόμηση',
          remove: 'Κατάργηση Ταξινόμηση'
        },
        column: {
          hide: 'Απόκρυψη στήλης'
        },
        aggregation: {
          count: 'συνολικές σειρές:',
          sum: 'σύνολο:',
          avg: 'Μέσος όρος:',
          min: 'ελάχιστο:',
          max: 'Μέγιστη:'
        },
        pinning: {
          pinLeft: 'pin αριστερά',
          pinRight: 'pin Δεξιά',
          unpin: 'Ξεκαρφιτσώνω'
        },
        gridMenu: {
          columns: 'Στήλες:',
          importerTitle: 'αρχείο εισαγωγής',
          exporterAllAsCsv: 'Εξαγωγή όλων των δεδομένων ως CSV',
          exporterVisibleAsCsv: 'Εξαγωγή ορατά δεδομένα, όπως CSV',
          exporterSelectedAsCsv: 'Εξαγωγή δεδομένων επιλεγεί ως CSV',
          exporterAllAsPdf: 'Εξαγωγή όλων των δεδομένων ως pdf',
          exporterVisibleAsPdf: 'Εξαγωγή ορατά δεδομένα ως pdf',
          exporterSelectedAsPdf: 'Εξαγωγή δεδομένων επιλεγεί ως pdf'
        },
        importer: {
          noHeaders: 'ονόματα των στηλών δεν ήταν σε θέση να προέρχονται, δεν το αρχείο έχει μια κεφαλίδα;',
          noObjects: 'Τα αντικείμενα δεν ήταν σε θέση να προέρχονται, υπήρξε δεδομένα στο αρχείο, εκτός από τις κεφαλίδες;',
          invalidCsv: 'Αρχείο δεν ήταν σε θέση να υποστούν επεξεργασία, είναι έγκυρη CSV;',
          invalidJson: 'Αρχείο δεν ήταν σε θέση να υποστούν επεξεργασία, είναι έγκυρο το JSON;',
          jsonNotArray: 'Εισαγόμενο αρχείο JSON πρέπει να περιέχει μια σειρά, να ματαιωθεί.'
        },
        pagination: {
          sizes: 'αντικείμενα ανά σελίδα',
          totalItems: 'αντικειμένων'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//EN-GB
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('en-gb', {
        aggregate: {
          label: 'items'
        },
        groupPanel: {
          description: 'Drag a column header here and drop it to group by that column.'
        },
        search: {
          placeholder: 'Search...',
          showingItems: 'Showing Items:',
          selectedItems: 'Selected Items:',
          totalItems: 'Total Items:',
          size: 'Page Size:',
          first: 'First Page',
          next: 'Next Page',
          previous: 'Previous Page',
          last: 'Last Page'
        },
        menu: {
          text: 'Choose Columns:'
        },
        sort: {
          ascending: 'Sort Ascending',
          descending: 'Sort Descending',
          remove: 'Remove Sort'
        },
        column: {
          hide: 'Hide Column'
        },
        aggregation: {
          count: 'total rows: ',
          sum: 'total: ',
          avg: 'avg: ',
          min: 'min: ',
          max: 'max: '
        },
        pinning: {
          pinLeft: 'Pin Left',
          pinRight: 'Pin Right',
          unpin: 'Unpin'
        },
        gridMenu: {
          columns: 'Columns:',
          importerTitle: 'Import file',
          exporterAllAsCsv: 'Export all data as csv',
          exporterVisibleAsCsv: 'Export visible data as csv',
          exporterSelectedAsCsv: 'Export selected data as csv',
          exporterAllAsPdf: 'Export all data as pdf',
          exporterVisibleAsPdf: 'Export visible data as pdf',
          exporterSelectedAsPdf: 'Export selected data as pdf'
        },
        importer: {
          noHeaders: 'Column names were unable to be derived, does the file have a header?',
          noObjects: 'Objects were not able to be derived, was there data in the file other than headers?',
          invalidCsv: 'File was unable to be processed, is it valid CSV?',
          invalidJson: 'File was unable to be processed, is it valid Json?',
          jsonNotArray: 'Imported json file must contain an array, aborting.'
        },
        pagination: {
          sizes: 'items per page',
          totalItems: 'items'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//ES-ES
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('es-es', {
        aggregate: {
          label: 'artículos'
        },
        groupPanel: {
          description: 'Arrastre un encabezado de columna aquí y colóquelo en el grupo por esa columna.'
        },
        search: {
          placeholder: 'Buscar...',
          showingItems: 'Mostrando artículos:',
          selectedItems: 'Artículos seleccionados:',
          totalItems: 'Articulos totales:',
          size: 'Tamaño de página:',
          first: 'Primera página',
          next: 'Siguiente página',
          previous: 'Pagina anterior',
          last: 'Última página'
        },
        menu: {
          text: 'Elija columnas:'
        },
        sort: {
          ascending: 'Orden ascendente',
          descending: 'Orden descendiente',
          remove: 'Eliminar clasificación'
        },
        column: {
          hide: 'Ocultar columna'
        },
        aggregation: {
          count: 'Filas totales:',
          sum: 'total: ',
          avg: 'avg: ',
          min: 'min: ',
          max: 'max: '
        },
        pinning: {
          pinLeft: 'Pin izquierda',
          pinRight: 'Pin Derecha',
          unpin: 'Desprender'
        },
        gridMenu: {
          columns: 'Columnas:',
          importerTitle: 'Importar archivo',
          exporterAllAsCsv: 'Exportar todos los datos como csv',
          exporterVisibleAsCsv: 'Exportar datos visibles como csv',
          exporterSelectedAsCsv: 'Exportar datos seleccionados como csv',
          exporterAllAsPdf: 'Exportar todos los datos como pdf',
          exporterVisibleAsPdf: 'Exportar datos visibles como pdf',
          exporterSelectedAsPdf: 'Exportar datos seleccionados como pdf'
        },
        importer: {
          noHeaders: 'Los nombres de columna no se pudieron derivar, ¿el archivo tiene un encabezado?',
          noObjects: 'No se pudieron derivar objetos, ¿había datos en el archivo que no fueran encabezados?',
          invalidCsv: 'No se pudo procesar el archivo, ¿es CSV válido?',
          invalidJson: 'No se pudo procesar el archivo, ¿es válido Json?',
          jsonNotArray: 'El archivo json importado debe contener una matriz, abortar.'
        },
        pagination: {
          sizes: 'Artículos por página',
          totalItems: 'artículos'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//FR-CA
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('fr-ca', {
        aggregate: {
          label: 'articles'
        },
        groupPanel: {
          description: 'Faites glisser un en-tête de colonne ici et déposez-le vers un groupe par cette colonne.'
        },
        search: {
          placeholder: 'Recherche...',
          showingItems: 'Articles Affichage des:',
          selectedItems: 'Éléments Articles:',
          totalItems: 'Nombre total d\'articles:',
          size: 'Taille de page:',
          first: 'Première page',
          next: 'Page Suivante',
          previous: 'Page précédente',
          last: 'Dernière page'
        },
        menu: {
          text: 'Choisir des colonnes:'
        },
        sort: {
          ascending: 'Trier par ordre croissant',
          descending: 'Trier par ordre décroissant',
          remove: 'Enlever le tri'
        },
        column: {
          hide: 'Cacher la colonne'
        },
        aggregation: {
          count: 'total lignes: ',
          sum: 'total: ',
          avg: 'moy: ',
          min: 'min: ',
          max: 'max: '
        },
        pinning: {
          pinLeft: 'Épingler à gauche',
          pinRight: 'Épingler à droite',
          unpin: 'Détacher'
        },
        gridMenu: {
          columns: 'Colonnes:',
          importerTitle: 'Importer un fichier',
          exporterAllAsCsv: 'Exporter toutes les données en CSV',
          exporterVisibleAsCsv: 'Exporter les données visibles en CSV',
          exporterSelectedAsCsv: 'Exporter les données sélectionnées en CSV',
          exporterAllAsPdf: 'Exporter toutes les données en PDF',
          exporterVisibleAsPdf: 'Exporter les données visibles en PDF',
          exporterSelectedAsPdf: 'Exporter les données sélectionnées en PDF'
        },
        importer: {
          noHeaders: 'Impossible de déterminer le nom des colonnes, le fichier possède-t-il un en-tête ?',
          noObjects: 'Aucun objet trouvé, le fichier possède-t-il des données autres que l\'en-tête ?',
          invalidCsv: 'Le fichier n\'a pas pu être traité, le CSV est-il valide ?',
          invalidJson: 'Le fichier n\'a pas pu être traité, le JSON est-il valide ?',
          jsonNotArray: 'Le fichier JSON importé doit contenir un tableau. Abandon.'
        },
        pagination: {
          sizes: 'articles par page',
          totalItems: 'articles'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//HI - Hindi - HI_IN
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('hi', {
        aggregate: {
          label: 'आइटम'
        },
        groupPanel: {
          description: 'एक स्तंभ शीर्ष लेख यहां खींचें और उस स्तंभ द्वारा समूह के लिए ड्रॉप।'
        },
        search: {
          placeholder: 'खोज',
          showingItems: 'आइटम दिखा रहा है:',
          selectedItems: 'चयनित उत्पाद:',
          totalItems: 'कुल सामान:',
          size: 'पृष्ठ आकार:',
          first: 'पहला पन्ना',
          next: 'अगला पृष्ठ',
          previous: 'पिछला पृष्ठ',
          last: 'अंतिम पृष्ठ'
        },
        menu: {
          text: 'कॉलम चुनें:'
        },
        sort: {
          ascending: 'छोटे से बड़े क्रम में क्रमबद्ध करें',
          descending: 'अवरोही क्रम',
          remove: 'हटाये क्रमबद्ध'
        },
        column: {
          hide: 'स्तंभ छुपाने'
        },
        aggregation: {
          count: 'कुल पंक्तियाँ:',
          sum: 'कुल:',
          avg: 'औसत:',
          min: 'न्यूनतम:',
          max: 'मैक्स:'
        },
        pinning: {
          pinLeft: 'पिन वाम',
          pinRight: 'पिन का अधिकार',
          unpin: 'अनपिन'
        },
        gridMenu: {
          columns: 'कॉलम:',
          importerTitle: 'फ़ाइल आयात करें',
          exporterAllAsCsv: 'सीएसवी के रूप में सभी डेटा निर्यात',
          exporterVisibleAsCsv: 'सीएसवी के रूप में निर्यात दिखाई डेटा',
          exporterSelectedAsCsv: 'सीएसवी के रूप में निर्यात चयनित डेटा',
          exporterAllAsPdf: 'पीडीएफ के रूप में सभी डेटा निर्यात',
          exporterVisibleAsPdf: 'पीडीएफ के रूप में निर्यात दिखाई डेटा',
          exporterSelectedAsPdf: 'पीडीएफ के रूप में निर्यात चयनित डेटा'
        },
        importer: {
          noHeaders: 'स्तंभ नाम प्राप्त किया जा करने में असमर्थ थे, फ़ाइल एक शीर्षक है?',
          noObjects: 'वस्तुओं, प्राप्त किया जा करने में सक्षम नहीं थे वहाँ डेटा था हेडर के अलावा अन्य फ़ाइल में?',
          invalidCsv: 'फ़ाइल संसाधित करने में असमर्थ था, यह वैध सीएसवी है?',
          invalidJson: 'फ़ाइल संसाधित करने में असमर्थ था, यह वैध Json है?',
          jsonNotArray: 'आयातित JSON फ़ाइल एक सरणी शामिल होना चाहिए, निरस्त।'
        },
        pagination: {
          sizes: 'आइटम प्रति पेज',
          totalItems: 'आइटम'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//HU - Hungarian - HU-HU
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('hu', {
        aggregate: {
          label: 'tételek'
        },
        groupPanel: {
          description: 'Húzzon egy oszlop fejléc itt és vidd azt a csoportot, amely oszlop.'
        },
        search: {
          placeholder: 'Keresés...',
          showingItems: 'A következő elemek:',
          selectedItems: 'Kijelölt elemek:',
          totalItems: 'Összes elemek',
          size: 'Oldalméret:',
          first: 'Első oldal',
          next: 'Következő oldal',
          previous: 'Előző oldal',
          last: 'Utolsó oldal'
        },
        menu: {
          text: 'Válasszon oszlopok:'
        },
        sort: {
          ascending: 'rendezés Növekvő',
          descending: 'Csökkenő rendezés',
          remove: 'Távolítsuk rendezése'
        },
        column: {
          hide: 'oszlop elrejtése'
        },
        aggregation: {
          count: 'teljes sorok:',
          sum: 'teljes: ',
          avg: 'átlagos: ',
          min: 'minimális: ',
          max: 'maximális: '
        },
        pinning: {
          pinLeft: 'pin Bal',
          pinRight: 'pin Jobb',
          unpin: 'Kibont'
        },
        gridMenu: {
          columns: 'Oszlopok:',
          importerTitle: 'fájl importálása',
          exporterAllAsCsv: 'Export az összes adatot csv',
          exporterVisibleAsCsv: 'Export látható adatokat csv',
          exporterSelectedAsCsv: 'Export kiválasztott adatokat csv',
          exporterAllAsPdf: 'Export az összes adatot pdf',
          exporterVisibleAsPdf: 'Export látható adatok pdf',
          exporterSelectedAsPdf: 'Export kiválasztott adatok pdf'
        },
        importer: {
          noHeaders: 'Oszlop nevét nem tudták, hogy nyert-e a fájlt egy fejlécet?',
          noObjects: 'Tárgyakat nem tudja, hogy nyert, ott volt a fájl adatai eltérő fejlécet?',
          invalidCsv: 'Fájl képtelen volt feldolgozni, ez érvényes CSV?',
          invalidJson: 'Fájl képtelen volt feldolgozni, ez érvényes JSON?',
          jsonNotArray: 'Az importált json fájlnak tartalmaznia kell egy tömb, megszakítás.'
        },
        pagination: {
          sizes: 'oldalankénti tételek',
          totalItems: 'tételek'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//KO - Korean - KO-KR
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('ko', {
        aggregate: {
          label: '항목'
        },
        groupPanel: {
          description: '여기에서 열 머리글을 끌어서 해당 열별로 그룹화하십시오.'
        },
        search: {
          placeholder: '수색',
          showingItems: '항목 표시 :',
          selectedItems: '선택한 항목 :',
          totalItems: '총 항목 :',
          size: '페이지 크기 :',
          first: '첫 페이지',
          next: '다음 페이지',
          previous: '이전 페이지',
          last: '마지막 페이지'
        },
        menu: {
          text: '열 선택 :'
        },
        sort: {
          ascending: '오름차순 정렬',
          descending: '내림차순 정렬',
          remove: '정렬 제거'
        },
        column: {
          hide: '열 숨기기'
        },
        aggregation: {
          count: '총 행 수 :',
          sum: '합계:',
          avg: '평균:',
          min: '최소:',
          max: '최대:'
        },
        pinning: {
          pinLeft: '왼쪽 핀',
          pinRight: '오른쪽 핀',
          unpin: '고정 해제'
        },
        gridMenu: {
          columns: '열:',
          importerTitle: '파일 가져 오기',
          exporterAllAsCsv: '모든 데이터를 CSV로 내보내기',
          exporterVisibleAsCsv: '표시 데이터를 CSV로 내보내기',
          exporterSelectedAsCsv: '선택한 데이터를 CSV로 내보내기',
          exporterAllAsPdf: 'pdf로 모든 자료를 수출하십시오',
          exporterVisibleAsPdf: '보이는 데이터를 pdf로 내보내기',
          exporterSelectedAsPdf: '선택한 데이터를 pdf로 내보내기'
        },
        importer: {
          noHeaders: '열 이름을 도출 할 수 없습니다. 파일에 헤더가 있습니까?',
          noObjects: '개체는 파생 될 수 없으며 머리글 이외의 파일에 데이터가 있습니까?',
          invalidCsv: '파일을 처리 할 수 없습니다. 올바른 CSV입니까?',
          invalidJson: '파일을 처리 할 수 없습니다. 유효한 Json입니까?',
          jsonNotArray: '가져온 json 파일에는 배열이 있어야하며 중단해야합니다.'
        },
        pagination: {
          sizes: '페이지 당 항목 수',
          totalItems: '항목'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//PL - Polish - PL-PL
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('pl', {
        aggregate: {
          label: 'przedmiotów'
        },
        groupPanel: {
          description: 'Przeciągnij nagłówek kolumny tutaj i upuść go do grupy według tej kolumny.'
        },
        search: {
          placeholder: 'Szukanie...',
          showingItems: 'Pokazuje pozycje:',
          selectedItems: 'Wybrane przedmioty:',
          totalItems: 'Razem pozycji:',
          size: 'Rozmiar strony:',
          first: 'Pierwsza strona',
          next: 'Następna strona',
          previous: 'Poprzednia strona',
          last: 'Ostatnia strona'
        },
        menu: {
          text: 'Wybierz kolumny:'
        },
        sort: {
          ascending: 'Sortuj rosnąco',
          descending: 'Sortuj malejąco',
          remove: 'Usuń Sortuj'
        },
        column: {
          hide: 'Ukryj kolumny'
        },
        aggregation: {
          count: 'Suma wierszy: ',
          sum: 'całkowity: ',
          avg: 'Średnia: ',
          min: 'minimum: ',
          max: 'maksymalny: '
        },
        pinning: {
          pinLeft: 'pin w lewo',
          pinRight: 'pin z prawej',
          unpin: 'Odpiąć'
        },
        gridMenu: {
          columns: 'kolumny:',
          importerTitle: 'Importować plik',
          exporterAllAsCsv: 'Wyeksportować wszystkie dane w formacie CSV',
          exporterVisibleAsCsv: 'Eksport widocznych danych w formacie CSV',
          exporterSelectedAsCsv: 'Eksportuj wybrane dane jako CSV',
          exporterAllAsPdf: 'Wyeksportować wszystkie dane w formacie PDF',
          exporterVisibleAsPdf: 'Eksport widoczne dane jako pdf',
          exporterSelectedAsPdf: 'Eksportuj wybrane dane jako pdf'
        },
        importer: {
          noHeaders: 'Nazwy kolumn nie byli w stanie wyprowadzić, czy plik ma nagłówek?',
          noObjects: 'Obiekty nie były w stanie wyprowadzić, nie było dane w pliku innego niż nagłówków?',
          invalidCsv: 'Plik nie mógł zostać przetworzony, jest to ważne CSV?',
          invalidJson: 'Plik nie mógł zostać przetworzony, jest to ważne Json?',
          jsonNotArray: 'Importowany plik json musi zawierać tablicę, przerywanie.'
        },
        pagination: {
          sizes: 'przedmioty na stronę',
          totalItems: 'przedmiotów'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//PT - Portuguese - PT-PT
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('pt', {
        aggregate: {
          label: 'Unid'
        },
        groupPanel: {
          description: 'Arraste um cabeçalho de coluna aqui e solte-o para agrupar por essa coluna.'
        },
        search: {
          placeholder: 'Pesquisa...',
          showingItems: 'Mostrando Itens:',
          selectedItems: 'Itens selecionados:',
          totalItems: 'Total de itens:',
          size: 'Tamanho da página:',
          first: 'Primeira página',
          next: 'Próxima página',
          previous: 'Página anterior',
          last: 'Última página'
        },
        menu: {
          text: 'Escolher Colunas:'
        },
        sort: {
          ascending: 'Ordernar ascendente',
          descending: 'Classificar em ordem decrescente',
          remove: 'Remover Classificar'
        },
        column: {
          hide: 'Ocultar coluna'
        },
        aggregation: {
          count: 'Total de linhas: ',
          sum: 'total: ',
          avg: 'Média: ',
          min: 'min: ',
          max: 'max: '
        },
        pinning: {
          pinLeft: 'Pino esquerdo',
          pinRight: 'Botão direito',
          unpin: 'Soltar'
        },
        gridMenu: {
          columns: 'Colunas:',
          importerTitle: 'Importar arquivo',
          exporterAllAsCsv: 'Exportar todos os dados como csv',
          exporterVisibleAsCsv: 'Exportar dados visíveis como csv',
          exporterSelectedAsCsv: 'Exportar dados selecionados como csv',
          exporterAllAsPdf: 'Exportar todos os dados em formato pdf',
          exporterVisibleAsPdf: 'Exportar dados visíveis como pdf',
          exporterSelectedAsPdf: 'Exportar dados selecionados como pdf'
        },
        importer: {
          noHeaders: 'Não foi possível derivar nomes de colunas, o arquivo tem um cabeçalho?',
          noObjects: 'Objetos não foram capazes de ser derivado, havia dados no arquivo diferente de cabeçalhos?',
          invalidCsv: 'O arquivo não pôde ser processado, ele é CSV válido?',
          invalidJson: 'O arquivo não pôde ser processado, ele é Json válido?',
          jsonNotArray: 'O arquivo json importado deve conter uma matriz, abortando.'
        },
        pagination: {
          sizes: 'itens por página',
          totalItems: 'Unid'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//VI - Vietnamese - VI-VN
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('vi', {
        aggregate: {
          label: 'mặt hàng'
        },
        groupPanel: {
          description: 'Kéo một tiêu đề cột ở đây và thả nó vào nhóm theo cột đó.'
        },
        search: {
          placeholder: 'Tìm kiếm...',
          showingItems: 'Hiển thị mục:',
          selectedItems: 'Các mục đã chọn:',
          totalItems: 'Tổng số đầu mục:',
          size: 'Kích thước trang:',
          first: 'Trang đầu tiên',
          next: 'Trang tiếp theo',
          previous: 'Trang trước',
          last: 'Trang cuối'
        },
        menu: {
          text: 'Chọn Cột:'
        },
        sort: {
          ascending: 'Sắp xếp tăng dần',
          descending: 'Sắp xếp giảm dần',
          remove: 'Di chuyển Sắp xếp'
        },
        column: {
          hide: 'Ẩn cột'
        },
        aggregation: {
          count: 'hàng tổng: ',
          sum: 'toàn bộ: ',
          avg: 'trung bình: ',
          min: 'tối thiểu: ',
          max: 'tối đa: '
        },
        pinning: {
          pinLeft: 'pin còn lại',
          pinRight: 'pin phải',
          unpin: 'tháo kim găm'
        },
        gridMenu: {
          columns: 'Cột:',
          importerTitle: 'nhập tập tin',
          exporterAllAsCsv: 'Xuất tất cả dữ liệu dưới dạng csv',
          exporterVisibleAsCsv: 'Xuất dữ liệu hiển thị dưới dạng csv',
          exporterSelectedAsCsv: 'Xuất dữ liệu đã chọn dưới dạng csv',
          exporterAllAsPdf: 'Xuất tất cả dữ liệu dưới dạng pdf',
          exporterVisibleAsPdf: 'Xuất dữ liệu hiển thị dưới dạng pdf',
          exporterSelectedAsPdf: 'Xuất dữ liệu đã chọn như pdf'
        },
        importer: {
          noHeaders: 'tên cột không thể được bắt nguồn, hiện các tập tin có tiêu đề?',
          noObjects: 'Đối tượng không thể được bắt nguồn, đã có dữ liệu trong các file khác với tiêu đề?',
          invalidCsv: 'Tập tin không thể được xử lý, là nó CSV hợp lệ?',
          invalidJson: 'Tập tin không thể được xử lý, nó là hợp lệ JSON?',
          jsonNotArray: 'Nhập khẩu tập tin JSON phải chứa một mảng, hủy bỏ.'
        },
        pagination: {
          sizes: 'mỗi trang',
          totalItems: 'mặt hàng'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//ZH-HANS - Chinese Simplified - ZH-CN
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('zh-hans', {
        aggregate: {
          label: '项目'
        },
        groupPanel: {
          description: '在此处拖动列标题，并将其拖放到该列的组中。'
        },
        search: {
          placeholder: '搜索...',
          showingItems: '显示项目:',
          selectedItems: '所选项目:',
          totalItems: '总项目:',
          size: '页面大小:',
          first: '第一页',
          next: '下一页',
          previous: '上一页',
          last: '最后一页'
        },
        menu: {
          text: '选择列:'
        },
        sort: {
          ascending: '升序排序',
          descending: '排序降序',
          remove: '删除排序'
        },
        column: {
          hide: '隐藏列'
        },
        aggregation: {
          count: '总行数: ',
          sum: '总: ',
          avg: '平均值: ',
          min: '最小值: ',
          max: '最大值: '
        },
        pinning: {
          pinLeft: '引脚左',
          pinRight: '引脚右',
          unpin: '取消固定'
        },
        gridMenu: {
          columns: '列:',
          importerTitle: '导入文件',
          exporterAllAsCsv: '将所有数据导出为csv',
          exporterVisibleAsCsv: '将可视数据导出为csv',
          exporterSelectedAsCsv: '将所选数据导出为csv',
          exporterAllAsPdf: '将所有数据导出为pdf',
          exporterVisibleAsPdf: '将可见数据导出为pdf',
          exporterSelectedAsPdf: '将所选数据导出为pdf'
        },
        importer: {
          noHeaders: '列名无法导出，文件是否有标题？',
          noObjects: '对象不能导出，文件中有除头之外的数据？',
          invalidCsv: '文件无法处理，是否有效的CSV？',
          invalidJson: '文件无法处理，是否有效的Json？',
          jsonNotArray: '导入的json文件必须包含数组，中止。'
        },
        pagination: {
          sizes: '每页项目',
          totalItems: '项目'
        }
      });
      return $delegate;
    }]);
  }]);
})();

//ZH-HANT - Chinese Traditional - ZH-TW
(function () {
  angular.module('ui.grid').config(['$provide', function($provide) {
    $provide.decorator('i18nService', ['$delegate', function($delegate) {
      $delegate.add('zh-hant', {
        aggregate: {
          label: '項目'
        },
        groupPanel: {
          description: '在此處拖動列標題，並將其拖放到該列的組中。'
        },
        search: {
          placeholder: '搜索...',
          showingItems: '顯示項目:',
          selectedItems: '所選項目:',
          totalItems: '總項目:',
          size: '頁面大小:',
          first: '第一頁',
          next: '下一頁',
          previous: '上一頁',
          last: '最後一頁'
        },
        menu: {
          text: '選擇列:'
        },
        sort: {
          ascending: '升序排序',
          descending: '排序降序',
          remove: '刪除排序'
        },
        column: {
          hide: '隱藏列'
        },
        aggregation: {
          count: '總行數: ',
          sum: '總: ',
          avg: '平均值: ',
          min: '最小: ',
          max: '最大值: '
        },
        pinning: {
          pinLeft: '引腳左',
          pinRight: '引腳右',
          unpin: '取消固定'
        },
        gridMenu: {
          columns: '列:',
          importerTitle: '導入文件',
          exporterAllAsCsv: '將所有數據導出為csv',
          exporterVisibleAsCsv: '將可視數據導出為csv',
          exporterSelectedAsCsv: '將所選數據導出為csv',
          exporterAllAsPdf: '將所有數據導出為pdf',
          exporterVisibleAsPdf: '將可見數據導出為pdf',
          exporterSelectedAsPdf: '將所選數據導出為pdf'
        },
        importer: {
          noHeaders: '列名無法導出，文件是否有標題？',
          noObjects: '對像不能導出，文件中有除頭之外的數據？',
          invalidCsv: '文件無法處理，是否有效的CSV？',
          invalidJson: '文件無法處理，是否有效的Json？',
          jsonNotArray: '導入的json文件必須包含數組，中止。'
        },
        pagination: {
          sizes: '每頁項目',
          totalItems: '項目'
        }
      });
      return $delegate;
    }]);
  }]);
})();

