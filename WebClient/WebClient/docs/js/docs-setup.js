NG_DOCS={
  "sections": {
    "api": "Prism Client API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "index",
      "shortName": "index",
      "type": "overview",
      "moduleName": "index",
      "shortDescription": "The Prism Client API provides an abstracted layer of tools enabling simplified access to server side resources through the client web application. Use of these objects requires some knowledge and understanding of javascript and Angularjs.",
      "keywords": "$q $scope abstracted abstracting access action add adding addition addlistener address allows angular angularjs api application asynchronous asynchronously automatically basemodel call callback changes client code codes common commonjs completed console construction consumption control controller create created creating crud customer customermodel customers defer define defined delete demonstrates dependency desc descending describes details determine document doe easy enable enabling encapsulation eq error event events example execute executed execution existing extensive facilitate failed failure familiarizing filter finally finished fires first_name flow framework function functions greeter handler handlerafter handlerbefore handling heavy http inherited inject injected injecting injection insert instance interacting interface inversion javascript joe john js knowledge last_name layer list listen listener listeners listening load loading log matching method methods model modelevent models modelservice module mycontroller newly object objects onafterinsert onafterremove onaftersave onbeforeinsert onbeforeremove onbeforesave operation operations order org overview parameters params perfect perform performed phone place point post prism promise promises promote proposal provide provided range reject rejecting rejects remove renamedgreeter represents request requests requires resolve resolves resolving resource resources result return reuse save saved scope second segue server service services share sid side sidenote simplified sort specific successful testing time tools topic understanding var web window work works",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "batchReceivingFilterResults",
      "shortName": "batchReceivingFilterResults",
      "type": "directive",
      "moduleName": "batchReceivingFilterResults",
      "shortDescription": ".",
      "keywords": "api batchreceivingfilterresults batchreceivingfilterresultsctrl components description directive examples formatting htm html javascript js path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "batchReceivingFilterResultsCtrl",
      "shortName": "batchReceivingFilterResultsCtrl",
      "type": "function",
      "moduleName": "batchReceivingFilterResultsCtrl",
      "shortDescription": "Controller of batchReceivingFilterResults",
      "keywords": "api batchreceivingfilterresults batchreceivingfilterresultsctrl controller description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "batchReceivingFilters",
      "shortName": "batchReceivingFilters",
      "type": "directive",
      "moduleName": "batchReceivingFilters",
      "shortDescription": ".",
      "keywords": "api batchreceivingfilters batchreceivingfiltersctrl components description directive examples formatting htm html javascript js path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "batchReceivingFiltersCtrl",
      "shortName": "batchReceivingFiltersCtrl",
      "type": "function",
      "moduleName": "batchReceivingFiltersCtrl",
      "shortDescription": "Controller of batchReceivingFilters",
      "keywords": "api batchreceivingfilters batchreceivingfiltersctrl controller description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "batchReceivingMarked",
      "shortName": "batchReceivingMarked",
      "type": "directive",
      "moduleName": "batchReceivingMarked",
      "shortDescription": ".",
      "keywords": "api batchreceivingmarked batchreceivingmarkedctrl components description directive examples formatting htm html javascript js path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "batchReceivingMarkedCtrl",
      "shortName": "batchReceivingMarkedCtrl",
      "type": "function",
      "moduleName": "batchReceivingMarkedCtrl",
      "shortDescription": "Controller of batchReceivingMarked",
      "keywords": "api batchreceivingmarked batchreceivingmarkedctrl controller description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerFinalConfirm",
      "shortName": "customerFinalConfirm",
      "type": "directive",
      "moduleName": "customerFinalConfirm",
      "shortDescription": ".",
      "keywords": "api components customerfinalconfirm customerfinalconfirmctrl description directive examples formatting htm html javascript js path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerFinalConfirmCtrl",
      "shortName": "customerFinalConfirmCtrl",
      "type": "function",
      "moduleName": "customerFinalConfirmCtrl",
      "shortDescription": "Controller of customerFinalConfirm",
      "keywords": "api controller customerfinalconfirm customerfinalconfirmctrl description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerMiniLookup",
      "shortName": "customerMiniLookup",
      "type": "directive",
      "moduleName": "customerMiniLookup",
      "shortDescription": ".",
      "keywords": "api components customerminilookup customerminilookupctrl description directive examples formatting htm html javascript js path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerMiniLookupBtn",
      "shortName": "customerMiniLookupBtn",
      "type": "directive",
      "moduleName": "customerMiniLookupBtn",
      "shortDescription": ".",
      "keywords": "api components customerminilookupbtn customerminilookupbtnctrl description directive examples formatting htm html javascript js path proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerMiniLookupCtrl",
      "shortName": "customerMiniLookupCtrl",
      "type": "function",
      "moduleName": "customerMiniLookupCtrl",
      "shortDescription": "Controller of customerMiniLookup",
      "keywords": "api controller customerminilookup customerminilookupctrl description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerMiniLookupFilter",
      "shortName": "customerMiniLookupFilter",
      "type": "directive",
      "moduleName": "customerMiniLookupFilter",
      "shortDescription": ".",
      "keywords": "api components customerminilookupfilter customerminilookupfilterctrl description directive examples formatting htm html javascript js path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerMiniLookupFilterCtrl",
      "shortName": "customerMiniLookupFilterCtrl",
      "type": "function",
      "moduleName": "customerMiniLookupFilterCtrl",
      "shortDescription": "Controller of customerMiniLookupFilter",
      "keywords": "api controller customerminilookupfilter customerminilookupfilterctrl description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "customerMiniLookupFilterCtrl",
      "shortName": "customerMiniLookupFilterCtrl",
      "type": "function",
      "moduleName": "customerMiniLookupFilterCtrl",
      "shortDescription": "Controller of customerMiniLookupFilter",
      "keywords": "api controller customerminilookupfilter customerminilookupfilterctrl description examples formatting function html javascript js proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "Customizations.ButtonHooksManager",
      "shortName": "Customizations.ButtonHooksManager",
      "type": "service",
      "moduleName": "Customizations",
      "shortDescription": "ButtonHooksManager handles the registration of custom handlers that will be invoked when the click event of existing buttons fires within the Retail Pro Prism client.",
      "keywords": "$inject addalert addhandler after_ api array attached before_ before_postransactiontendertransaction button buttonhooksmanager buttons click client code custom customizations defined dependency_1 dependency_2 dependency_3 event event-name events executed existing fired fires formats function handler handlers handles hello indicate invoked js message minified myhandler names notificationservice option parameter prefixed prism pro provided register registers registration retail second service sidebuttonsmanager third usage var world",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "Customizations.ConfigurationManager",
      "shortName": "Customizations.ConfigurationManager",
      "type": "service",
      "moduleName": "Customizations",
      "shortDescription": "ConfigurationManager handles the registration of custom plugin code that will be invoked when the prismApp initializes. For example, registration of handlers using the ModelEvent service. See the usage example below:",
      "keywords": "$inject $q addhandler addlistener adds adhere angularjs api capitalize cased charat code common configuration configurationmanager custom customer customerlettercasehandler customizations defer deferred dependency dependency_1 dependency_2 dependency_3 di example first_name formats function handler handlerbefore handlers handles includes initialization initializes injected invocation invoked js last_name letter listener minified modelevent myhandler names onbeforeinsert onbeforesave opteration option org parameters plugin prismapp promise registration required resolve return second service services slice str third touppercase usage var",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "Customizations.SideButtonsManager",
      "shortName": "Customizations.SideButtonsManager",
      "type": "service",
      "moduleName": "Customizations",
      "shortDescription": "SideButtonsManager handles the registration of custom buttons that load within the sidebar when the Retail Pro button is clicked. This allows third party developers to add custom actions that can be performed in addition to the default actions provided by Prism.",
      "keywords": "$inject actions activation activation-details add addalert addbutton addition adjmemo adjustment allows annotation api array assign batch button buttons clicked closing code config configuration creating custom customer customizations dcs default department dependency_1 dependency_2 dependency_3 details developers edit editing existing formats function general general-details handler handlers handles hello icon images info inventory inventory2 invoices invoked item js label literal load loaded lookup main memo merchantwarehouse message minified myhandler non-structured notificationservice object opening operations option optional options order party path performed physical pi pisheet png po pos priorities prism pro promotions provided purchase receiving receiving-batch register registers registration relative rendered report retail returning reward reward-rules rules running screen second sections service sheet sidebar sidebuttonsmanager slip store store-ops structured tendering text third transaction transactionedit transactionedittender transactionedittendermwtenderstatus transactionreturns transactionroot transactionview transfer transferorder transferorders transfers translation usage valid validation validation-rules var vendor vendor-invoices vendor2 view viewing voucher world xout zone zone-sheet zones zout zoutclose zoutopen",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "Document",
      "shortName": "Document",
      "type": "service",
      "moduleName": "Document",
      "shortDescription": "",
      "keywords": "_batchreceiving api controller document formatting function js mycontroller prismapp proper service usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "ItemColumns",
      "shortName": "ItemColumns",
      "type": "service",
      "moduleName": "ItemColumns",
      "shortDescription": "Service that allows the inclusion of additional fields when doing item lookups.",
      "keywords": "addcolumn adding addition additional allows alu angular api array children columns config current currently database example extended fields filters formatting impact inclusion item itemcolumns items js list lookup lookups method module mymodule negative null performance proper query representing returns search service strings table udf1_string usage valid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "onPollingLoadingStatus",
      "shortName": "onPollingLoadingStatus",
      "type": "component",
      "moduleName": "onPollingLoadingStatus",
      "shortDescription": ".",
      "keywords": "api component components description examples formatting htm html javascript js onpollingloadingstatus path proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "paging",
      "shortName": "paging",
      "type": "directive",
      "moduleName": "paging",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js paging pagingctrl path portrait proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "pagingCtrl",
      "shortName": "pagingCtrl",
      "type": "function",
      "moduleName": "pagingCtrl",
      "shortDescription": "Controller of paging",
      "keywords": "api controller description examples formatting function html javascript js paging pagingctrl proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "posSelectTender",
      "shortName": "posSelectTender",
      "type": "directive",
      "moduleName": "posSelectTender",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait posselecttender posselecttenderctrl proper usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "posSelectTenderCtrl",
      "shortName": "posSelectTenderCtrl",
      "type": "function",
      "moduleName": "posSelectTenderCtrl",
      "shortDescription": "Controller of posSelectTender",
      "keywords": "api controller description examples formatting function html javascript js posselecttender posselecttenderctrl proper usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.directives:barCode",
      "shortName": "barCode",
      "type": "directive",
      "moduleName": "prismApp.common",
      "shortDescription": "This directive when implemented on form input control will enable the control",
      "keywords": "$event accept activator angular api bar bar-code boolean broken called camera carriage code common complete completed control controls declaration devices directive directives doesn enable example explained focus focusme form formatting function handheld implemented input inputs inside ios js keydownfunction manually matter model modelval ng-focuser ng-keydown ng-model optional parameter pass plan primary prism prismapp proper return returns scan scanned scanner scanners sections single standard store tag text type usage variable watch work",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models",
      "shortName": "prismApp.common.models",
      "type": "overview",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Namespace containing all Model Objects in the prismApp module",
      "keywords": "api common model models module namespace objects overview prismapp",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AcpublisherModel",
      "shortName": "prismApp.common.models.AcpublisherModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Acpublisher.",
      "keywords": "accessing acpublisher acpublishermodel addition address api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts installation_id installation_name installation_type instance js log methods model models modelservice object objects operation operations parameters port post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable xxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AcsubscriptiongroupModel",
      "shortName": "prismApp.common.models.AcsubscriptiongroupModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Acsubscriptiongroup.",
      "keywords": "accessing acsubscriptiongroup acsubscriptiongroupmodel addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper recurrence remove removes representing request requested required requires result returned save saves server service services sid side specific subscription_group_id subscription_group_name supports test usage var variable xxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AcsubscriptionModel",
      "shortName": "prismApp.common.models.AcsubscriptionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Acsubscription.",
      "keywords": "accessing acsubscription acsubscriptionmodel addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires resource_name result returned save saves server service sid side specific subscription_group_sid subscription_name supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AcsubscriptionpublisherModel",
      "shortName": "prismApp.common.models.AcsubscriptionpublisherModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Acsubscriptionpublisher.",
      "keywords": "accessing acsubscriptionpublisher acsubscriptionpublishermodel addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper publisher_sid remove removes representing request requested required requires result returned save saves server service services sid side specific subscription_group_sid subscription_sid supports test usage var variable xxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AddedtendersModel",
      "shortName": "prismApp.common.models.AddedtendersModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Addedtenders (tenders added to zout close).",
      "keywords": "accessing addedtenders addedtendersmodel addition api assign basemodel business change child close cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable zout",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AddressModel",
      "shortName": "prismApp.common.models.AddressModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Address.",
      "keywords": "accessing addition address addressmodel api assign business change child cols columns common console convenience create created crud current customer_sid customerchildmodel data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AddressTypeBackofficeModel",
      "shortName": "prismApp.common.models.AddressTypeBackofficeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side AddressTypeBackoffice.",
      "keywords": "accessing addition addresstypebackoffice addresstypebackofficemodel adminconsole allocationpattern api assign basemodel2 business change child cols columns common console contextstateservice convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AddresstypeModel",
      "shortName": "prismApp.common.models.AddresstypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Addresstype.",
      "keywords": "accessing addition addresstype addresstypemodel api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AdjCommentModel",
      "shortName": "prismApp.common.models.AdjCommentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side AdjComment.",
      "keywords": "accessing addition adjcomment adjcommentmodel adjsid api assign basemodel business change child cols columns commentno common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AdjItemModel",
      "shortName": "prismApp.common.models.AdjItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side AdjItem.",
      "keywords": "accessing addition adjitem adjitemmodel adjsid api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance itempos itemsid js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AdjustmentModel",
      "shortName": "prismApp.common.models.AdjustmentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Adjustments.",
      "keywords": "accessing addition adjustment adjustmentmodel adjustments api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance item js log methods model models modelservice2 newly object objects operation operations parameters patternqty post prismapp promise proper remove removeitem removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AdjustmentModel",
      "shortName": "prismApp.common.models.AdjustmentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Adjustments.",
      "keywords": "accessing addition adjustment adjustmentmodel adjustments api basemodel2 business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice2 object objects operations parameters prismapp proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AllocationpatternModel",
      "shortName": "prismApp.common.models.AllocationpatternModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Allocationpattern.",
      "keywords": "accessing addition allocationpattern allocationpatternmodel api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AllocationPatternModel",
      "shortName": "prismApp.common.models.AllocationPatternModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side AllocationPattern.",
      "keywords": "accessing addition allocationpattern allocationpatternmodel api assign basemodel2 business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ApplicationModel",
      "shortName": "prismApp.common.models.ApplicationModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Application.",
      "keywords": "accessing addition api application applicationmodel assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AssociateModel",
      "shortName": "prismApp.common.models.AssociateModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Associate.",
      "keywords": "accessing addition api associate associatemodel associates basemodel business child columns common console convenience crud eq field filter filters formatting function handles including insert js log methods models modelservice object objects operations parameters pisheetmodel prismapp proper remove requested requires save server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.AssociateModel",
      "shortName": "prismApp.common.models.AssociateModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Associate.",
      "keywords": "accessing addition api associate associatemodel basemodel business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters prismapp proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BaseModel",
      "shortName": "prismApp.common.models.BaseModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Base Model object - All other models inherit from this base.",
      "keywords": "api base basemodel common inherit model modelattributeevent modelevent models object prismapp service services",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BaseModel",
      "shortName": "prismApp.common.models.BaseModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Base Model all other models inherit from this base.",
      "keywords": "$http $scope alert alert-error angular api attribute attributes auth-session base basemodel bootstrap changed class code common controller create current customer defaults delete dependecies dependencies dirty empty example examplecontroller failed false first_name function getcustomer getitem headers html http icon-warning-sign icon-white inherit insert insertcustomer inserted inserts instance js json key last_name loggedin login modal model modelattributeevent modelevent modelobjectdeleteexample modelobjectinsertexample modelobjectsaveexample models modelservice module ng-click ng-controller ng-disabled ng-hide ng-model ngresource novalidate null object operation params post previous prismapp prismauth promise property remove removecustomer removed representing reset response result save savecustomer saved saves server service services sessionstorage sid simple-form text true type ui values",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BiometricsModel",
      "shortName": "prismApp.common.models.BiometricsModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Biometrics.",
      "keywords": "accessing addition adminconsole allocationpattern api assign basemodel2 biometrics biometricsmodel business change child cols columns common console contextstateservice convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BOVendorAddressModel",
      "shortName": "prismApp.common.models.BOVendorAddressModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side BOVendorAddress.",
      "keywords": "accessing addition api basemodel bovendoraddress bovendoraddressmodel business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters prismapp proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BOVendorEmailModel",
      "shortName": "prismApp.common.models.BOVendorEmailModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side BOVendorEmail.",
      "keywords": "accessing addition api basemodel bovendoremail bovendoremailmodel business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters prismapp proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BOVendorModel",
      "shortName": "prismApp.common.models.BOVendorModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side BOVendor.",
      "keywords": "accessing active addition api assign basemodel2 bovendor bovendormodel business change child cols columns common console convenience create created crud current data deactivate endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove representing request requested requires result returned save saves server service sid side specific supports test usage var variable vendors",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BOVendorPhoneModel",
      "shortName": "prismApp.common.models.BOVendorPhoneModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side BOVendorPhone.",
      "keywords": "accessing addition api basemodel bovendorphone bovendorphonemodel business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters prismapp proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.BusinessunitModel",
      "shortName": "prismApp.common.models.BusinessunitModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Businessunit.",
      "keywords": "accessing addition api assign basemodel business businessunit businessunitmodel change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CalendarModel",
      "shortName": "prismApp.common.models.CalendarModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Calendar.",
      "keywords": "accessing addition api assign basemodel business calendar calendarmodel change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ChargetermModel",
      "shortName": "prismApp.common.models.ChargetermModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Chargeterms.",
      "keywords": "accessing addition api assign basemodel2 business change chargeterm chargetermmodel chargeterms child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CommentsBackOfficeModel",
      "shortName": "prismApp.common.models.CommentsBackOfficeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side CommentsBackOffices.",
      "keywords": "accessing active addition api assign basemodel2 business change child cols columns comments commentsbackoffice commentsbackofficemodel commentsbackoffices common console convenience create created crud current data deactivated endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CommentsModel",
      "shortName": "prismApp.common.models.CommentsModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Comments.",
      "keywords": "accessing addition api assign basemodel business change child cols columns comments commentsmodel common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CommissionModel",
      "shortName": "prismApp.common.models.CommissionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Commission.",
      "keywords": "accessing addition api assign basemodel business change child cols columns commission commissionmodel common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CompanyModel",
      "shortName": "prismApp.common.models.CompanyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Company.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common company companymodel console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ContacttypeModel",
      "shortName": "prismApp.common.models.ContacttypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Contacttype.",
      "keywords": "accessing addition api assign basemodel2 business change child cintextstateservuce cols columns common console contacttype contacttypemodel convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ControllerModel",
      "shortName": "prismApp.common.models.ControllerModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Controller.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console controller controllermodel controllers convenience crud current data endpoint eq example field filter filters formatting function generating handles http including insert inserted instance js log methods model models modelservice modelservice2 object objects operation operations parameters prismapp promise proper remove removed representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CountryModel",
      "shortName": "prismApp.common.models.CountryModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Country.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience country country_code countrymodel create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CouponSetCouponModel",
      "shortName": "prismApp.common.models.CouponSetCouponModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Coupon.",
      "keywords": "accessing addition api assign base2model business change child cols columns common console convenience coupon couponsetcoupon couponsetcouponmodel create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CouponSetModel",
      "shortName": "prismApp.common.models.CouponSetModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Coupon.",
      "keywords": "accessing addition api assign base2model business change child cols columns common console convenience coupon couponset couponsetmodel create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CreditcardModel",
      "shortName": "prismApp.common.models.CreditcardModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Creditcard.",
      "keywords": "accessing addition api assign basemodel business card_type change child cols columns common console convenience create created creditcard creditcardmodel crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CreditcardtypeModel",
      "shortName": "prismApp.common.models.CreditcardtypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Creditcardtype.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created creditcardtype creditcardtypemodel crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CreditcardvalidationruleModel",
      "shortName": "prismApp.common.models.CreditcardvalidationruleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Creditcardvalidationrule.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created creditcardvalidationrule creditcardvalidationrulemodel crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CurrencyModel",
      "shortName": "prismApp.common.models.CurrencyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Currency.",
      "keywords": "$filter $http $q accessing active adddenomination addition adds alphabetic_code amount api assign associated basemodel business change child cols columns common console convenience convertcurrency converts create created crud currency currencymodel currencysid current data decimals delete denomination denomination_name denominationmodel documentmodel endpoint eq example field filter filtered filtering filters formatcurrency formats formatted formatting function generating getdenomination getdenominations handles http included including insert inserts instance js literal loads log methods mode model models modelservice multiplier newden object objects operation operations parameters params passed post prismapp promise proper remove removes representing request requested required requires result returned rounding save saves selected server service sid side sort specific starting submitted supports target targetcurrencysid test transaction transactionmode true usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CustomerclassModel",
      "shortName": "prismApp.common.models.CustomerclassModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Customerclass.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current cust_class_name customerclass customerclassmodel data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CustomerModel",
      "shortName": "prismApp.common.models.CustomerModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Customer.",
      "keywords": "$http $q accessing active addaddress addemail addition addphone address addresses adds api appropriate assign balance basemodel boolean business call central centralcopycustomerfromcentral centralcopycustomertocentral centralcreditbalalanceispositive centralcreditbalance centralcreditcustomerhaspositivebalance centralcreditgetbalancebycustomer centralcreditgetvaluebyid centrals change check checks child cols columns common concatenating console convenience copy copycustomerfromcentral copycustomertocentral create created credit creditid crud current customer customerapi customerid customermodel customers customersid data database delete document email emails endpoint eq example field field1 field2 filter filtering filters formatting fullname function generating getaddresses getemails getphones handles http including indicated insert inserts instance invoicesid ispositive issued js json key literal local log methods model models modelservice names newadd newcust neweml newphn object objects operation operations parameters params payment paymentid permission phone phones places post prism prismapp prismutilities promise proper properties provided remove removes representing request requested requires resolution result retrieve retrieves returned rpc save saves server service services sid side sort specific string supplied supports test toggleactive toggles transmits true usage user var variable voucher",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CustomerudfModel",
      "shortName": "prismApp.common.models.CustomerudfModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Customerudf.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current customer_sid customerudf customerudfmodel data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves sbs_sid server service sid side specific supports test udf_no usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CustomerudfoptionModel",
      "shortName": "prismApp.common.models.CustomerudfoptionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Customerudfoption.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current customerudf_sid customerudfchildmodel customerudfoption customerudfoptionmodel data delete endpoint eq example field filter filters flag_id formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test udf_option usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CustomizationModel",
      "shortName": "prismApp.common.models.CustomizationModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Customization.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console control_address convenience create created crud current customization customization_id customizationmodel data delete developer_id endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable version xxxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.CustomSchemaModel",
      "shortName": "prismApp.common.models.CustomSchemaModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side CustomSchema.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current customschema customschemamodel data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves schema_name server service services sid side specific supports test usage var variable xxxxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.Dcs2Model",
      "shortName": "prismApp.common.models.Dcs2Model",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Dcs.",
      "keywords": "accessing active addition api assign basemodel2 business change child cols columns common console convenience create created crud current data dcs dcs2 dcs2model deactivated endpoint eq example field fields filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove representing request requested required requires result returned save saves server service services sid side specific supports test three usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DcsModel",
      "shortName": "prismApp.common.models.DcsModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Dcs.",
      "keywords": "accessing addition api assign basemodel business change child code cols columns common console convenience create created crud current data dcs dcsmodel delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DenominationModel",
      "shortName": "prismApp.common.models.DenominationModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Denomination.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud currency_sid current data delete denomination denomination_id denomination_name denominationmodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice multiplier object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DepartmentModel",
      "shortName": "prismApp.common.models.DepartmentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Departments.",
      "keywords": "accessing active addition api assign basemodel2 business change child cols columns common console convenience create created crud current data deactivated department departmentmodel departments endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DepositModel",
      "shortName": "prismApp.common.models.DepositModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Deposits.",
      "keywords": "accessing addition api assign best business change child cols columns common console convenience crud current data delete deposit depositadd depositmodel deposits document documentchildmodel documentmodel endpoint eq example field filter filters formatting function generating handles http including insert instance js log methods methods_depositadd model models modelservice object objects operation operations parameters prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DocumentCouponModel",
      "shortName": "prismApp.common.models.DocumentCouponModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side DocumentCoupon.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete documentcoupon documentcouponmodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DocumentDiscountModel",
      "shortName": "prismApp.common.models.DocumentDiscountModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side DocumentDiscount.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete documentdiscount documentdiscountmodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DocumentfeetypeModel",
      "shortName": "prismApp.common.models.DocumentfeetypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Documentfeetype.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete documentfeetype documentfeetypemodel endpoint eq example fee_type field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DocumentModel",
      "shortName": "prismApp.common.models.DocumentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Documents.",
      "keywords": "accessing add addbilltocustomer adddeposit adding additem addition adds addshiptocustomer amount api applies applyassociatechangetoitems applycustomerdiscount applyemployeeinfotoitems applypricelevelchange applytaxrebate assign associated balance basemodel bill boolean bt_cuid business buy call canceldocument cash central centralcopycustomerfromcentral centrals change checkcentralbalance checks child cols columns common console convenience copy copycustomerfromcentral create created credit crud current customer customerid customers data database delete deposit depositadd depositmodel deprecated deprectated desc detax detax_flag discount document document_sid documentmodel documentpricelevelchangeapply documents employee endpoint eq example existing feemode fees field filter filtering filters flg formatting fulfillment function generates generating getalltenders getdeposits getgivetenders getitems gettaketenders handles http including info insert inserts instance inventory invn_sbs_item_sid item item_description1 itemadd itemdata itemmodel itemremove items itemupdate js json level literal loads local log match methods model models modelservice newdeposit newitem newly newtender null object objects operation operations order package parameters params persisttaxrebate places post preparefulfillingdoc preparefulfillingdocument price prism prismapp process promise proper provided re-submits remove removecustomers removeitem removes removetender representing request requested require required requires resendsendsale resolution resolvepackagecomponents result retrieves return returned rpc runs sale save saves send server service services set sets setting ship shippingmode sid side sort specific spreaddiscount spreaddocumentdiscount spreads spreadto st_cuid status supports taken_deposit_amount tax_rebate_persisted tells tender tender_name tender_type tenderadd tendermodel tenderremove tenders test transaction true uitem undone unspreaddiscount unspreaddocumentdiscount unspreadfrom unspreads update updateitem updates usage usedsubtotal valid values var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DrawerEventCurrencyModel",
      "shortName": "prismApp.common.models.DrawerEventCurrencyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side DrawerEventCurrency.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud currency_event_type currency_sid current data delete drawer_event_sid drawereventcurrency drawereventcurrencymodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DrawerEventModel",
      "shortName": "prismApp.common.models.DrawerEventModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Drawerevent.",
      "keywords": "accessing addition api assign basemodel business cashier_sid change child cols columns common console convenience create created crud current data delete drawer_number drawerevent drawereventmodel endpoint eq event_type example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable workstation_sid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.DrawerModel",
      "shortName": "prismApp.common.models.DrawerModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Drawer.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete drawer drawermodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable workstation_sid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.EmailModel",
      "shortName": "prismApp.common.models.EmailModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Email.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current customer_sid customerchildmodel data delete email email_address emailmodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.EmailTypeBackofficeModel",
      "shortName": "prismApp.common.models.EmailTypeBackofficeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side EmailTypeBackoffice.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console contextstateservice convenience create created crud current data delete emailtype emailtypebackoffice emailtypebackofficemodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.EmailtypeModel",
      "shortName": "prismApp.common.models.EmailtypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Emailtype.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete email_type emailtype emailtypemodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves sbs_sid server service sid side specific supports test usage var variable xxxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.EmployeeModel",
      "shortName": "prismApp.common.models.EmployeeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Employee.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete employee employee_name employeemodel endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ExchangerateModel",
      "shortName": "prismApp.common.models.ExchangerateModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Exchangerate.",
      "keywords": "accessing addition api assign base_currency_sid basemodel business change child cols columns common console convenience create created crud currency_sid current data delete endpoint eq example exchangerate exchangeratemodel field filter filters formatting function generating give_rate handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports take_rate test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ExtendedModel",
      "shortName": "prismApp.common.models.ExtendedModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Extended.",
      "keywords": "accessing addition api business child columns common console convenience create created crud current customer customer_sid customerchildmodel endpoint eq example extended extendedmodel field filter filters formatting function generating handles http including insert inserted inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removed representing request requested requires resources result save server service sid side specific supports test usage var",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ExternalserviceModel",
      "shortName": "prismApp.common.models.ExternalserviceModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Externalservice.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete eft endpoint eq example externalservice externalservicemodel field filter filters formatting function generating handles hardware http including insert inserts instance ip_address js log methods model models modelservice object objects operation operations parameters port post prismapp promise proper remove removes representing request requested required requires result returned save saves server service service_type services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InventoryModel",
      "shortName": "prismApp.common.models.InventoryModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Inventory.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance inventory inventorymodel js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InventorystyleModel",
      "shortName": "prismApp.common.models.InventorystyleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Inventorystyle.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance inventorystyle inventorystylemodel js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InventoryudfModel",
      "shortName": "prismApp.common.models.InventoryudfModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Inventoryudf.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance inventoryudf inventoryudfmodel js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves sbs_sid server service services sid side specific supports test udf_no usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InvnLotModel",
      "shortName": "prismApp.common.models.InvnLotModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side InvnSerial.",
      "keywords": "accessing addition api assign business change child cols columns common console convience create crud current data delete employee endpoint eq example field filter filters formatting function generating handles http including insert inserts instance inventory2childmodel invnlot invnlotmodel invnsbsitemsid invnserial invnserialmodel js log methods models modelservice2 object objects operation operations parameter parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InvnLotQtyModel",
      "shortName": "prismApp.common.models.InvnLotQtyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side InvnLotQty.",
      "keywords": "accessing addition api assign attibute attr attribute basemodel business change child cols columns common console convience create crud current data delete employee endpoint eq example field filter filters formatting function generating handles http including inherited insert inserts instance inventory2childmodel invnlot invnlotqty invnlotqtymodel invnsbsitemlotsid invnsbsitemsid js log method methods models modelservice2 object objects operation operations parameter parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service set setattr sets sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InvnSerialModel",
      "shortName": "prismApp.common.models.InvnSerialModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side InvnSerial.",
      "keywords": "accessing addition api assign attibute attr attribute basemodel business change child cols columns common console convenience create crud current data delete employee endpoint eq example field filter filters formatting function generating handles http including inherited insert inserts instance inventory2childmodel invnsbsitemsid invnserial invnserialmodel js log method methods models modelservice2 object objects operation operations parameter parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service set setattr sets sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.InvnSerialQtyModel",
      "shortName": "prismApp.common.models.InvnSerialQtyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side InvnSerialQty.",
      "keywords": "accessing addition api assign attibute attr attribute basemodel business change child cols columns common console convience create crud current data delete employee endpoint eq example field filter filters formatting function generating handles http including inherited insert inserts instance inventory2childmodel invnsbsitemsid invnsbsitemsnsid invnserial invnserialqty invnserialqtymodel js log method methods models modelservice2 object objects operation operations parameter parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service set setattr sets sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ItemDiscountModel",
      "shortName": "prismApp.common.models.ItemDiscountModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Item Discounts.",
      "keywords": "accessing addition allow api basemodel business child cols columns common console convenience crud current data delete discount discounts doesn edit endpoint eq example explicitly field filter filters formatting function generating handles http including insert instance item itemdiscount itemdiscountmodel js log methods models modelservice object objects operation operations parameters prismapp promise proper records remove removes representing request requested requires resource result save server service services sid side specific supports test usage var",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ItemModel",
      "shortName": "prismApp.common.models.ItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Items.",
      "keywords": "$http $q accessing addition api assign based basemodel business change child cols columns common console convenience create created crud current customheaders data delete desc document_sid endpoint eq example field filter filtering filters formatting function generating handles header http including insert inserts instance inventory inventory_sid inventorylookup invn_sbs_item_sid item itemmodel items js literal log lookupvalue methods model models modelservice object objects operation operations parameters params post price prismapp prismsessioninfo prismutilities promise proper remove removes representing request requested required requires result returned save saves server service services setitemtype sets sid side sort specific supports test type usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.JobModel",
      "shortName": "prismApp.common.models.JobModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Job.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance job job_name jobmodel js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.KitcomponentModel",
      "shortName": "prismApp.common.models.KitcomponentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Kitcomponent.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common component_name console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js kitcomponent kitcomponentmodel log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.LanguageModel",
      "shortName": "prismApp.common.models.LanguageModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Language.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js language language_name languagemodel log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.LinkqueueModel",
      "shortName": "prismApp.common.models.LinkqueueModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Linkqueue.",
      "keywords": "accessing addition address api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq event_type example field filter filters formatting function generating handles http including insert inserts instance js linkqueue linkqueuemodel log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.LtyLevelModel",
      "shortName": "prismApp.common.models.LtyLevelModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side LtyLevels.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log ltylevel ltylevelmodel ltylevels methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.LtyProgramModel",
      "shortName": "prismApp.common.models.LtyProgramModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side LtyPrograms.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log ltylvlsid ltyprogram ltyprogrammodel ltyprograms methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test test1 usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.MarkdownModel",
      "shortName": "prismApp.common.models.MarkdownModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Markdowns.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http identifier including insert inserts instance item js log markdown markdownmodel markdowns methods model models modelservice2 newly object objects operation operations order parameters poitem post prismapp promise proper purchase remove removed removeitem removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.MediaTypeModel",
      "shortName": "prismApp.common.models.MediaTypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side MediaType.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log mediatype mediatypemodel methods model models modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.MultiplierbuttonsModel",
      "shortName": "prismApp.common.models.MultiplierbuttonsModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Multiplierbuttons.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete document_type endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice multiplierbuttons multiplierbuttonsmodel object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PapersizeModel",
      "shortName": "prismApp.common.models.PapersizeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Papersize.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations paper_size_code papersize papersizemodel parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PermissionModel",
      "shortName": "prismApp.common.models.PermissionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Permission.",
      "keywords": "accessing addition api application_area_sid application_sid assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters permission permission_id permissionmodel post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PhoneModel",
      "shortName": "prismApp.common.models.PhoneModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Phone.",
      "keywords": "accessing addition api assign business change child columns common console convenience create created crud current customer_sid customerchildmodel data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters phone phone_no phonemodel post prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PhoneTypeBackofficeModel",
      "shortName": "prismApp.common.models.PhoneTypeBackofficeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PhoneTypeBackoffice.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console contextstateservice convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters phonetype phonetypebackoffice phonetypebackofficemodel post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PhonetypeModel",
      "shortName": "prismApp.common.models.PhonetypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Phonetype.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters phone_type phonetype phonetypemodel post prismapp promise proper remove removes representing request requested required requires result returned save saves sbs_sid server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PiimportModel",
      "shortName": "prismApp.common.models.PiimportModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Piimport.",
      "keywords": "accessing addition api basemodel business child columns common console convenience create created crud current endpoint eq example field file filecontent filter filters formatting function generating handles http including insert inserts instance js log map_sid methods model models modelservice object objects operation operations parameters pi_map_sid piimport piimportmodel post prismapp promise proper remove representing request requested requires result save server service services sid side specific supports test usage var zone_sid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PimapModel",
      "shortName": "prismApp.common.models.PimapModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Pimap.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log map_name methods model models modelservice object objects operation operations parameters pimap pimapmodel post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PiSheetBadScanModel",
      "shortName": "prismApp.common.models.PiSheetBadScanModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PiSheetBadScan.",
      "keywords": "accessing addition api business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters pisheetbadscan pisheetbadscanmodel pisheetzonechildmodel prismapp proper requested requires server service sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PiSheetItemModel",
      "shortName": "prismApp.common.models.PiSheetItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PiSheetItem.",
      "keywords": "accessing addition api basemodel business changes child columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pisheetitem pisheetitemmodel pisheetitems pisheetmodel post prismapp promise promotionactivationpricelevel promotionactivationpricelevelmodel proper remove removes representing request requested requires result romotionactivationpricelevel save server service services sid side specific supports test time usage var",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PisheetModel",
      "shortName": "prismApp.common.models.PisheetModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Pisheet.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pisheet pisheetmodel post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PiSheetZoneModel",
      "shortName": "prismApp.common.models.PiSheetZoneModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PiSheetZone.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pisheetmodel pisheetzone pisheetzonemodel post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PosflagModel",
      "shortName": "prismApp.common.models.PosflagModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Posflag.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters flag_no formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters posflag posflagmodel post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific subsidiary_sid supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PosoptionModel",
      "shortName": "prismApp.common.models.PosoptionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Posoption.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pisheetmodel posoption posoptionmodel post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PostalCodesModel",
      "shortName": "prismApp.common.models.PostalCodesModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Postal Codes.",
      "keywords": "accessing addition api basemodel2 business child codes columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice2 object objects operations parameters postal postalcodes postalcodesmodel prismapp proper requested requires server service sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PriceadjustingModel",
      "shortName": "prismApp.common.models.PriceadjustingModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Priceadjusting.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post priceadjusting priceadjustingmodel prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PricelevelModel",
      "shortName": "prismApp.common.models.PricelevelModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Pricelevel.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters post price_level pricelevel pricelevel2 pricelevelmodel prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PricelevelModel2",
      "shortName": "prismApp.common.models.PricelevelModel2",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Pricelevel.",
      "keywords": "accessing addition api basemodel business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice modelservice2 object objects operations parameters pricelevel pricelevel2 pricelevelmodel2 prismapp proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PriceroundingModel",
      "shortName": "prismApp.common.models.PriceroundingModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Pricerounding.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post pricerounding priceroundingmodel prismapp promise proper remove removes representing request requested required requires result returned rounding_level save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PrintAreaModel",
      "shortName": "prismApp.common.models.PrintAreaModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PrintArea.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters post printarea printareamodel prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PrintAreaValuesModel",
      "shortName": "prismApp.common.models.PrintAreaValuesModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PrintArea.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters post printarea printareavalues printareavaluesmodel prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PrinterModel",
      "shortName": "prismApp.common.models.PrinterModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Printers.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post printer printermodel printers prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable workstation workstationsid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PrinterTypeAssignmentModel",
      "shortName": "prismApp.common.models.PrinterTypeAssignmentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PrinterTypeAssignments.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post printersid printertypeassignment printertypeassignmentmodel printertypeassignments printertypesid prismapp promise proper remove removes representing request requested required requires result returned save saves server service sid side specific supports test usage var variable workstationsid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PrinterTypeModel",
      "shortName": "prismApp.common.models.PrinterTypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PrinterType.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post printertype printertypemodel prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PrinterTypeSettingModel",
      "shortName": "prismApp.common.models.PrinterTypeSettingModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PrinterTypeSetting.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice modelservice2 object objects operation operations parameters post printertypesetting printertypesettingmodel prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromodataModel",
      "shortName": "prismApp.common.models.PromodataModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Promodata.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters plugin_id post prismapp promise promodata promodatamodel proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionActivationPriceLevelModel",
      "shortName": "prismApp.common.models.PromotionActivationPriceLevelModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionActivationPriceLevel.",
      "keywords": "accessing addition api business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters prismapp promotionactivationpricelevel promotionactivationpricelevelmodel promotionchildmodel proper requested requires server service services sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionActivationStoreModel",
      "shortName": "prismApp.common.models.PromotionActivationStoreModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionActivationStore.",
      "keywords": "accessing addition api business child columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationstore promotionactivationstoremodel promotionchildmodel proper remove removes representing request requested requires result save server service sid side specific supports test usage var",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionModel",
      "shortName": "prismApp.common.models.PromotionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Promotion.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotion promotionmodel proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionRewardFilterElementModel",
      "shortName": "prismApp.common.models.PromotionRewardFilterElementModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionRewardFilterElement.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationregionmodel promotionrewardfilterelement promotionrewardfilterelementmodel proper remove removes representing request requested requires result returned rewardotheritemrulechildmodel rule_sid save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionRewardOtherItemRuleModel",
      "shortName": "prismApp.common.models.PromotionRewardOtherItemRuleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionRewardOtherItemRule.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pos post prismapp promise promotionactivationregionmodel promotionchildmodel promotionrewardotheritemrule promotionrewardotheritemrulemodel proper remove removes representing request requested required requires result returned rule_sid save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.promotionsListModel",
      "shortName": "prismApp.common.models.promotionsListModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side promotionsLists.",
      "keywords": "$http $q $stateparams $timeout accessing addition api basemodel2 business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice modelservice2 object objects operations parameters prismapp promotionslist promotionslistmodel promotionslists proper requested requires server service services shareddata sid side specific supports usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionValidationCouponModel",
      "shortName": "prismApp.common.models.PromotionValidationCouponModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionValidationCoupon.",
      "keywords": "aaa accessing addition api assign business change child cols columns common console convenience coupon_code create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationregionmodel promotionchildmodel promotionvalidationcoupon promotionvalidationcouponmodel proper remove removes representing request requested required requires result returned rule_sid save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionValidationFilterElementModel",
      "shortName": "prismApp.common.models.PromotionValidationFilterElementModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionValidationFilterElement.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationregionmodel promotionvalidationfilterelement promotionvalidationfilterelementmodel proper remove removes representing request requested requires result returned rule_sid save saves server service sid side specific supports test usage validationitemrulechildmodel var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PromotionValidationItemRuleModel",
      "shortName": "prismApp.common.models.PromotionValidationItemRuleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PromotionValidationItemRule.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationregionmodel promotionchildmodel promotionvalidationitemrule promotionvalidationitemrulemodel proper remove removes representing request requested requires result returned rule_sid save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ProxyHardwareModel",
      "shortName": "prismApp.common.models.ProxyHardwareModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side ProxyHardware.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pisheetmodel post prismapp promise proper proxychildmodel proxyhardware proxyhardwaremodel remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ProxyModel",
      "shortName": "prismApp.common.models.ProxyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Proxy.",
      "keywords": "accessing addition api assign associated basemodel boolean business change child client_type cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating gethardware handles hardware http including insert inserts instance js list local_port log methods model models modelservice object objects operation operations parameters post prismapp promise proper proxy proxymodel remove removes representing request requested required requires result retrieves returned save saves server service services sid side specific supports test usage var variable workstation_name",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PurchaseorderModel",
      "shortName": "prismApp.common.models.PurchaseorderModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Purchaseorders.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http identifier including insert inserts instance item js log methods model models modelservice2 newly object objects operation operations order parameters poitem post prismapp promise proper purchase purchaseorder purchaseordermodel purchaseorders remove removed removeitem removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.PurchFeeTypeModel",
      "shortName": "prismApp.common.models.PurchFeeTypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side PurchFeeTypes.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience crud current data endpoint eq example fee field filter filters formatting function generating handles http including insert js log methods models modelservice2 object objects operation operations order parameters prismapp promise proper purchase purchfeetype purchfeetypemodel purchfeetypes remove representing request requested requires result returned save saves server service sid side specific supports test types usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ReasonModel",
      "shortName": "prismApp.common.models.ReasonModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Reason.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper reason reason_id reason_type reasonmodel remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.RegionModel",
      "shortName": "prismApp.common.models.RegionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Region.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper region region_name regionmodel remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.RemoteConnectionModel",
      "shortName": "prismApp.common.models.RemoteConnectionModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Transfer Fee Types.",
      "keywords": "api assign basemodel2 business change cols columns common console controller create created crud current data delete endpoint eq example fee field filter filters formatting function generating handles http including insert inserts instance js local log model models modelservice2 object operation operations parameters post prismapp promise proper remoteconnection remoteconnectionmodel remotecontrollersid remove removes representing request requested required requires result returned save saves server service sid side specific supports test transfer types usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ReportGroupItemModel",
      "shortName": "prismApp.common.models.ReportGroupItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side ReportGroupItems.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes reportgroupitem reportgroupitemmodel reportgroupitems representing request requested required requires result returned save saves server service sid side specific supports test usage var variable workstation workstationsid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ReportGroupModel",
      "shortName": "prismApp.common.models.ReportGroupModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side ReportGroups.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes reportgroup reportgroupmodel reportgroups representing request requested required requires result returned save saves server service sid side specific supports test usage var variable workstation workstationsid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.RewardItemDiscountTierModel",
      "shortName": "prismApp.common.models.RewardItemDiscountTierModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side RewardItemDiscountTier.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete disc_value endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationregionmodel promotionchildmodel proper qty remove removes representing request requested required requires result returned rewarditemdiscounttier rewarditemdiscounttiermodel save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.RewardTransactionDiscountTierModel",
      "shortName": "prismApp.common.models.RewardTransactionDiscountTierModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side RewardTransactionDiscountTier.",
      "keywords": "accessing addition api assign business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise promotionactivationregionmodel promotionchildmodel proper remove removes representing request requested requires result returned rewardtransactiondiscounttier rewardtransactiondiscounttiermodel save saves server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryExtendedModel",
      "shortName": "prismApp.common.models.SBSInventoryExtendedModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryExtended.",
      "keywords": "access accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example extended field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventoryextended sbsinventoryextendedmodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryKitModel",
      "shortName": "prismApp.common.models.SBSInventoryKitModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryKit.",
      "keywords": "access accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventorykit sbsinventorykitmodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryLtyModel",
      "shortName": "prismApp.common.models.SBSInventoryLtyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryLty.",
      "keywords": "access accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventorylty sbsinventoryltymodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryPriceModel",
      "shortName": "prismApp.common.models.SBSInventoryPriceModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryPrice.",
      "keywords": "access accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventoryprice sbsinventorypricemodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryQtyModel",
      "shortName": "prismApp.common.models.SBSInventoryQtyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryQty.",
      "keywords": "access accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventoryqty sbsinventoryqtymodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryStoreQtyModel",
      "shortName": "prismApp.common.models.SBSInventoryStoreQtyModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryStoreQty.",
      "keywords": "access accessing addition api assign basemodel bovendoraddress bovendoremail bovendorphone business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventorystoreqty sbsinventorystoreqtymodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SBSInventoryVendorModel",
      "shortName": "prismApp.common.models.SBSInventoryVendorModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SBSInventoryVendor.",
      "keywords": "access accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example extended field filter filters formatting function functions generating handles http including insert inserts instance inventory invn_sbs_item_sid item js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested require requires result returned save saves sbsinventoryvendor sbsinventoryvendormodel server service services sid side specific supports test usage var variable working",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ScaleModel",
      "shortName": "prismApp.common.models.ScaleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Scale.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves scale scale_name scalemodel server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SeasonModel",
      "shortName": "prismApp.common.models.SeasonModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Season.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves season season_code season_id seasonmodel server service services sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SequencingModel",
      "shortName": "prismApp.common.models.SequencingModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Sequencing.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete documentsequence endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves seq_level sequencing sequencingmodel server service sid side specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ShiftModel",
      "shortName": "prismApp.common.models.ShiftModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Shift.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services shift shiftmodel sid side specific supports test time_shift_name time_shift_number usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ShippingmethodModel",
      "shortName": "prismApp.common.models.ShippingmethodModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Shippingmethod.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log method methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services shippingmethod shippingmethodmodel sid side specific subsidiary_sid supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SlipCommentModel",
      "shortName": "prismApp.common.models.SlipCommentModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SlipComment.",
      "keywords": "accessing addition api assign basemodel business change child cols columns comments common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side slipcomment slipcommentmodel slipsid specific supports test text usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SlipFeeModel",
      "shortName": "prismApp.common.models.SlipFeeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SlipFee.",
      "keywords": "accessing addition amt api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side slipfee slipfeemodel slipsid specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SlipItemModel",
      "shortName": "prismApp.common.models.SlipItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side SlipItem.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance itemsid js log methods model models modelservice object objects operation operations parameters post prismapp promise proper qty remove removes representing request requested required requires result returned save saves server service services sid side slipitem slipitemmodel slipsid specific supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.StoreModel",
      "shortName": "prismApp.common.models.StoreModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Store.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific store store_code store_number storemodel supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SubscriberModel",
      "shortName": "prismApp.common.models.SubscriberModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Subscriber.",
      "keywords": "accessing addition address api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific subscriber subscriber_id subscriber_name subscribermodel supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SubsidiaryModel",
      "shortName": "prismApp.common.models.SubsidiaryModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Subsidiary.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific subsidiary subsidiary_name subsidiary_number subsidiarymodel supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.SuffixModel",
      "shortName": "prismApp.common.models.SuffixModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Suffix.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific suffix suffixmodel supports test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TagcodeModel",
      "shortName": "prismApp.common.models.TagcodeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Tagcode.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific store_sid supports tagcode tagcodemodel test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TagtypeModel",
      "shortName": "prismApp.common.models.TagtypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Tagtype.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports tag_type tagtype tagtypemodel test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TaxareaModel",
      "shortName": "prismApp.common.models.TaxareaModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Taxarea.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports taxarea taxareamodel test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TaxcodeModel",
      "shortName": "prismApp.common.models.TaxcodeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Taxcode.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports tax_code taxcode taxcodemodel test usage var variable xxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TaxruleModel",
      "shortName": "prismApp.common.models.TaxruleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Taxrule.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports tax_area_sid tax_rule_code taxrule taxrulemodel test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TenderModel",
      "shortName": "prismApp.common.models.TenderModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Tender.",
      "keywords": "$http accessing activates addition allowed api assign availabletenders balance based basemodel business card cards central centralcardnumber centralgiftcardactivate centralgiftcardbalance centralgiftcardchangeexpire centralgiftcardchangevalue centralgiftcardvalidate change child cols columns common console convenience create created crud current customer customermodel customermodelobject custrec data database delete digit document endpoint entered eq example existing existingtenders expiration field filter filters formatted formatting function generatecentralgiftcardid generates generating gift handles http included including insert inserts instance invoicesid js key literal log methods mode model models modelservice namefromtype number object objects operation operations optionally param parameters params post pre-authorizes print printing printname prismapp prismsessioninfo prismutilities promise proper remove removes representing request requested requires result retrieves returned returns save saves server service services sets sid side specific stating string supports tender tender_type tendermode tendermodel tendername tenders tendertype test type typefromname types update usage valid var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TendertypeModel",
      "shortName": "prismApp.common.models.TendertypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Tendertype.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific subsidiary_sid supports tender_type tendertype tendertypemodel test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TermsModel",
      "shortName": "prismApp.common.models.TermsModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Terms.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports terms termsmodel test usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.Till2Model",
      "shortName": "prismApp.common.models.Till2Model",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Till2.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific store_sid subsidiary_sid supports test till till2 till2model tillmodel tillmodel2 usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TillModel",
      "shortName": "prismApp.common.models.TillModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Till.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific store_sid subsidiary_sid supports test till tillmodel usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TimeclockModel",
      "shortName": "prismApp.common.models.TimeclockModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Timeclock.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete employee_sid endpoint eq event_datetime event_type example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific store_sid subsidiary_sid supports test timeclock timeclockmodel usage var variable workstation_sid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TitleModel",
      "shortName": "prismApp.common.models.TitleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Title.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific subsidiary_sid supports test title title_id titlemodel usage var variable xxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TitleTypeBackofficeModel",
      "shortName": "prismApp.common.models.TitleTypeBackofficeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TitleTypeBackoffices.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves sbssid server service sid side specific supports test title titleid titletypebackoffice titletypebackofficemodel titletypebackoffices usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TouchButtonModel",
      "shortName": "prismApp.common.models.TouchButtonModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TouchButton.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters function generating handles http including insert inserts instance js log methods model models modelservice2 object objects operation operations parameters post prismapp promise remove removes representing request requested requires result returned save saves server service sid side specific supports test touchbutton touchbuttonmodel usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TouchmenuModel",
      "shortName": "prismApp.common.models.TouchmenuModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Touchmenu.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters function generating handles http including insert inserts instance js log methods model models modelservice2 object objects operation operations parameters post prismapp promise remove removes representing request requested requires result returned save saves server service sid side specific supports test touchmenu touchmenumodel usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TranFeeTypeModel",
      "shortName": "prismApp.common.models.TranFeeTypeModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TranFeeTypes.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience crud current data endpoint eq example fee field filter filters formatting function generating handles http including insert js log methods models modelservice2 object objects operation operations parameters prismapp promise proper remove representing request requested requires result returned save saves server service sid side specific supports test tranfeetype tranfeetypemodel tranfeetypes transfer types usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TransferOrderItemModel",
      "shortName": "prismApp.common.models.TransferOrderItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TransferOrderItem.",
      "keywords": "accessing addition api basemodel business child columns common console convenience crud eq field filter filters formatting handles including js log methods models modelservice object objects operations parameters prismapp proper requested requires server service services sid side specific supports transferorderitem transferorderitemmodel usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TransferOrderItemModel",
      "shortName": "prismApp.common.models.TransferOrderItemModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TransferOrderItemQty.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance itempos itemsid js log methods model models modelservice modelservice2 object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test tosid transferorderitem transferorderitemmodel transferorderitemqty usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TransferOrderModel",
      "shortName": "prismApp.common.models.TransferOrderModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TransferOrders.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test transferorder transferordermodel transferorders usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TransferslipModel",
      "shortName": "prismApp.common.models.TransferslipModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Transferslips.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http identifier including insert inserts instance item js log methods model models modelservice2 newly object objects operation operations parameters poitem post prismapp promise proper remove removed removeitem removes representing request requested requires result returned save saves server service sid side slip specific supports test transfer transferslip transferslipmodel transferslips usage var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TransformDesignModel",
      "shortName": "prismApp.common.models.TransformDesignModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TransformDesign.",
      "keywords": "accessing addition api assign basemodel bocolumns business change child cols columns common console convenience create created crud current data delete design_name designcontent endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice namespace object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires resource_name result returned save saves server service services sid side specific supports test transformdesign transformdesignmodel usage userdefined_flag var variable xxxxxxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.TransRuleModel",
      "shortName": "prismApp.common.models.TransRuleModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side TransRule.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test transrule transrule_name transrulemodel usage var variable xxxxx",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.UsergroupModel",
      "shortName": "prismApp.common.models.UsergroupModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Usergroup.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage user_group_name usergroup usergroupmodel var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.UserModel",
      "shortName": "prismApp.common.models.UserModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side User.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage user user_name usermodel var variable",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.VendorEmailModel",
      "shortName": "prismApp.common.models.VendorEmailModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side VendorEmail.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable vendoremail vendoremailmodel",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.VendorinvoiceModel",
      "shortName": "prismApp.common.models.VendorinvoiceModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Vendorinvoices.",
      "keywords": "accessing active addition api assign basemodel2 business change child cols columns common console convenience create created crud current data deactivate endpoint eq example field filter filters formatting function gen generating handles http including insert inserts instance invoices js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove representing request requested required requires result returned save saves server service sid side specific supports test test1 usage var variable vendcode vendinvcid vendinvcno vendor vendorinvoice vendorinvoicemodel vendorinvoices",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.VendorModel",
      "shortName": "prismApp.common.models.VendorModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Vendor.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable vendor vendormodel",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.VendorsUdfModel",
      "shortName": "prismApp.common.models.VendorsUdfModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side VendorsUdfs.",
      "keywords": "accessing addition api assign basemodel2 business change child cols columns common console convenience create created crud current data delete document endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice2 newly object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service sid side specific supports test usage var variable vendorsudf vendorsudfmodel vendorsudfs",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.WorkstationModel",
      "shortName": "prismApp.common.models.WorkstationModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Workstation.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested required requires result returned save saves server service services sid side specific supports test usage var variable workstation workstation_name workstationmodel",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ZoutControlModel",
      "shortName": "prismApp.common.models.ZoutControlModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side ZoutControl.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable zoutcontrol zoutcontrolmodel",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ZoutFilterModel",
      "shortName": "prismApp.common.models.ZoutFilterModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side ZoutFilter.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters pisheetmodel post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable zoutfilter zoutfiltermodel",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.models.ZouttenderModel",
      "shortName": "prismApp.common.models.ZouttenderModel",
      "type": "service",
      "moduleName": "prismApp.common.models",
      "shortDescription": "Business Object that handles CRUD operations on server side Zouttender.",
      "keywords": "accessing addition api assign basemodel business change child cols columns common console convenience create created crud current data delete endpoint eq example field filter filters formatting function generating handles http including insert inserts instance js log methods model models modelservice object objects operation operations parameters post prismapp promise proper remove removes representing request requested requires result returned save saves server service services sid side specific supports test usage var variable zouttender zouttendermodel",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services",
      "shortName": "prismApp.common.services",
      "type": "overview",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Namespace containing all Service Objects in the prismApp module",
      "keywords": "api common module namespace objects overview prismapp service services",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.AdjMemo",
      "shortName": "prismApp.common.services.AdjMemo",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles Adjustment Memo operations.",
      "keywords": "$http $q $rootscope $scope $state $stateparams $timeout $translate $uibmodal _associates adjmemo adjustment adjustmetn api automatically common controller copy copymemo cost create created current currently documentpersisteddata example formatting forwards function generates handles http indicated indicating integer js memo memotools memotype modelservice modelservice2 mycontroller newly newmemo notificaitonservice object operation operations pos price pricelevels prismapp prismutilities promise proper qty representing resourcenotification result reversememo reverses sernolotnoservice service services type usage viewed viewing",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.Biometrics",
      "shortName": "prismApp.common.services.Biometrics",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles Cash Drawer operations.",
      "keywords": "$scope api biometrics cash common controller drawer formatting function handles js mycontroller operations prismapp proper service services usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.CashDrawer",
      "shortName": "prismApp.common.services.CashDrawer",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles Cash Drawer operations.",
      "keywords": "$scope alert api associated calls cash cashdrawer check checks closed cols columns comma common controller current displays drawer drawers drawersid endpoint entry eq example exists field1 field2 filter formatting function generates getdrawers getsingledrawer handles hardware http initial initiates iscashdrawer isdraweropen isopen issues js list literal method mycontroller object open operation operations parameters params popdrawer primary prismapp promise proper properties property proxy query recurring registered repeats representing request response result retrieve runs seconds separated service services sessionstorage sets sid sort specific string usage v1 workstation",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.CreditCardTypes",
      "shortName": "prismApp.common.services.CreditCardTypes",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles retrieval of Credit Card Types.",
      "keywords": "$scope admin api array card cardsid common console controller credit creditcardtypes defined example formatting function getmwcardtypes getmwprintname getprismcardtypes getprismprintname handles js list merchantware method mwcardtypes mwprintname mycontroller objects print-friendly prism prismapp prismprintname prismsessioninfo proper representing retrieval service services sid string type types usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.LineDisplay",
      "shortName": "prismApp.common.services.LineDisplay",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Handles sending data to the line display associated with the current workstation.",
      "keywords": "$scope acceptable alignment api associated blank boolean called center cleardisplay cleared clears client cols column comma-delimited common content controller current data default details direct display displayed displaymessage displays displaysid eq false field field1 field2 filter filtering formatting function getdetails getlist handles inclusion indicate js left linedisplay linenumber list literal long message messages method mycontroller notification number object onfirstline onlastline onspecificline operation parameters params posted pre-configured prism prismapp promise proper provided querystring representing request reset result returned selected sending server service services set sid sort specific string text truncate usage valid values workstation",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.LoadingScreen",
      "shortName": "prismApp.common.services.LoadingScreen",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that Shows or Hides a default loading screen that prevents interaction with the screen while enabled.",
      "keywords": "$scope api common controller default enabled formatting function hides interaction js loading loadingscreen mycontroller prevents prismapp proper screen service services usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.loginProvider",
      "shortName": "prismApp.common.services.loginProvider",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Handles the login/logout and seating functions in Prism.",
      "keywords": "$scope address api attempt attempts authenticate boolean claim common controller credentials current ends entered erases format formatting function functions handles hostname http indicating ip isv9 js json log logged login loginprovider logout logs method mycontroller object password prism prismapp pro promise proper provided pwd release remote remotelogin remotelogout requests result retail ril seat seating server service services session sessionstorage sit stand stored string usage user username usr valid values",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ModelEvent",
      "shortName": "prismApp.common.services.ModelEvent",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "The ModelEvent service can be used to add listeners that will execute a specific handler whenever a certain Model executes a save, insert, or delete operation.",
      "keywords": "$http $q $scope add addbeforelistener addhandler addlistener alert alert-error angular api array auth-session bootstrap bound class code common console controller create customer defaults defer delete dependecies dependencies desired document empty event events example examplecontroller execute executes failed false fires first_name formatting function getitem handler handlerbefore handlers headers html icon-warning-sign icon-white insert insertcustomer inserted joe js json last_name len length listen listener listeners listens log loggedin login modal model modelevent modeleventaddlistenerexample modeleventremovelistenerexample models modelservice module multiple mycontroller ng-click ng-controller ng-hide ng-model ngresource novalidate object onafterinsert onafterremove onaftersave onbeforeinsert onbeforeremove onbeforesave operation operations parameter prismapp prismauth promise proper push rejected remove removelistener removelisteners removes removing reset resolve resolved return returned save saved service services sessionstorage simple-form specific string text token tokens true type ui usage var",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ModelService",
      "shortName": "prismApp.common.services.ModelService",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "ModelService factory provides and interface for retriving resources from the server and creating new resources to be sent to the server.",
      "keywords": "$http $injector $resource $scope alert alert-error amount angular api array asc auth-session bootstrap cached class code cols common comply console controller converted create createresource creates creating defaults dependecies dependencies desc document_sid drawerevent eq example examplecontroller existing factory false filter formated fromcache function getitem getresource headers html http icon-warning-sign icon-white instances interface js json length literal log loggedin login method modal model modelinstance modelname models modelservice modelservicecreateexample modelserviceexample modelservicegetexample module ng-click ng-controller ng-hide ng-model ngresource novalidate object objects operation parameter parameters params post prismapp prismauth promise provided raw representation representing request requested resource resources response result retrieves retriving returns server serverside service services sessionstorage sid simple-form sort string subsidiary text true type ui undefined usage var wrapresponse",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ModelService",
      "shortName": "prismApp.common.services.ModelService",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "ModelService factory provides an interface for retrieving resources from the server and creating new resources to be sent to the server.",
      "keywords": "$injector $resource $scope api common controller creating factory formatting function interface js modelservice mycontroller prismapp proper resources retrieving server service services usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ModuleLookupData",
      "shortName": "prismApp.common.services.ModuleLookupData",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "This Service handles Module lookup data.",
      "keywords": "$scope api array common controller customer data department employee example formatting function getselecteditems grid group handles inventory items js list lookup method module modulelookupdata modules mycontroller prismapp proper representing respective returns selected service services supported usage user vendor",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.Notification",
      "shortName": "prismApp.common.services.Notification",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Used to display notifications on the screen. Each notification may be dismissed individually, regardless of the order in which it appears.",
      "keywords": "$scope action actions add addalert addconfirm addconfirmok addcustom additional addnotification ajax ajaxloader alert alertbackdrop alertclass alertheader alertkeyboard alerts alerttext allow api appear appears authorization authorized backdrop background button buttons calling cancel class classes clear cleared clicking close common completed configobject configuration configured confirm confirmation confirmbackdrop confirmclass confirmheader confirms confirmtext controller creates credentials css custom default delay desired dialog dismiss dismissed dismisses display displayed esc example formatting function header http individually input javascript js key keyboard loading message method mimics modal modal-backdrop msg mycontroller non-blocking notification notificationbackdrop notificationclass notificationheader notificationkeyboard notifications notificationservice notificationtext notificationtimeout notify number object occurring operation order override overrideprompt password perform performed permission permissionname place prismapp process processing promise prompt prompts proper properties provided remove representing requested required result screen seconds self-clears self-dismiss self-dismissing separated service services sets showcancel showclose showno showok showyes space submits text timeout title true usage user username valid",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.PrintAction",
      "shortName": "prismApp.common.services.PrintAction",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles print actions in Prism.",
      "keywords": "$scope actions additional address airprint allows api attachment binded bluetooth choose collate collatecopies common configured controller copies custom data default design designsidtouse device dialog display document documentpersisteddata documenttitle email emailaddress example formatting function handles http included indicating ios iosprint job js method mobile mycontroller number numberofcopies object opens operation passed payload pdf performs preview print printaction printdataservice printed printer printeractionservice printersservice printertype printing prism prismapp promise proper report representing requests requires result returns send sends server service services sid sidtotransform string title transform transformed transformtype type usage valid version whattoprint xzout",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.Printers",
      "shortName": "prismApp.common.services.Printers",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles printing in Prism.",
      "keywords": "$scope active additional address airprint allows api attachment binded bluetooth choose collate collatecopies cols column columns common configured controller copies corrected current currently custom data default design designsidtouse details device dialog display document documentpersisteddata documenttitle email emailaddress eq example field1 field2 filter filtering formatting function functionality future getlist getsingle handles http included indicating ios iosprint job js list literal method mobile mycontroller number numberofcopies object opens operation parameters passed payload pdf performs preview print printdata printed printer printers printersid printersservice printertype printesr printing prism prismapp promise proper relied report representing request requests requires result return returned returns send sends server service services sid sidtotransform sort sorting specific string time title transform transformed transformtype type usage valid version whattoprint workstation xzout",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.prismSessionInfo",
      "shortName": "prismApp.common.services.prismSessionInfo",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Manages the session information for the current login session.",
      "keywords": "$scope allow api array boolean called change checked checks clearcollection collected collectpermission common comparison controller current example formatting function getcollectedpermissions getsession indicating internal internally isallowed js list login manages method mycontroller needsoverride newsession object override permission permissionname permissions permname preferences prismapp prismsessioninfo proper provided removes result retrieval retrieves returns saved saves service services session set setsession storage true usage values",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.PrismUtilities",
      "shortName": "prismApp.common.services.PrismUtilities",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "A collection of utility functions used throughout Prism.",
      "keywords": "$scope accepted add addelement addmultiple adds api applied appropriate arr array boolean booleans boolvalue case changed characters check checked click collection column combine common controller convert convertjsdateobjtodbdateval data database dates datevalue db defaults dependencyobject determine determines displaying dom double doubles element elements eliminates empty encodes event eventarray eventname events example exist expected false field fifo filter filters find flg format formatted formatting formed friendly function functions generate generated generateguid generates getfilter gettimezoneoffset guid indicating input integer isclickinobject isnullorempty isnumber iso isodatestring isvaliddate javascript joiner js key layer list makearray matches method mouse multiple mycontroller names non-numeric nonnumeric notnull null number numbers obj object objectarrayindexof objects offset operate operated operator operators order original outer override pair pairs param parenthesis parse parseisostringtodateobject passed passing places plugin pluginevents position pre-defined preference prism prismapp prismutilities process promise prop proper properly property quotes random regex removed replace replaces representing requiredtotal resolve resolves responseparser return returns rps rpsfilters search searched server service services space spaces specific statement statements str string strip takes target targetobject term testvalue time titlecase toboolean total totitlecase type unicodefilter url-unsafe usable usage useless utc utilarray utility val valid validatetotal validdate values valuesarray wraps zone",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.PurchaseOrder",
      "shortName": "prismApp.common.services.PurchaseOrder",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles Purchase Order operations.",
      "keywords": "$http $interval $q $scope $state $stateparams add allocationpattern api array common controller example feetypes formatting function generates handles http indicated item items js mycontroller object objects operation operations order permissions prismapp prismutilities promise proper purchase purchaseorder representing result service services usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ReceivingVoucherTools",
      "shortName": "prismApp.common.services.ReceivingVoucherTools",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service that handles Receiving and Voucher operations.",
      "keywords": "$http $q $scope $translate amount api common controller decreases example field formatting function generates generatetofromvoucher handles http included increase increases incrementitemqty indicated item js modelservice mycontroller object operation operations order place prismapp promise proper qty quantity receiving receivingvouchertools reduce reduceitemqty removeitemfromvoucher removes representing resourcenotification result service services shareddata transfer update updateitemfromvoucher usage voucher",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ResourceNotificationService",
      "shortName": "prismApp.common.services.ResourceNotificationService",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "This service has been deprecated, use the Toast Service instead.",
      "keywords": "api common deprecated error function method prismapp resourcenotificationservice service services showerror showsuccessfulmessage showwarning success toast warning",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ShareTypes",
      "shortName": "prismApp.common.services.ShareTypes",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Contains the pre-defined customer Share Types used in Prism.",
      "keywords": "$scope api array common controller customer display formatting function getsharetypes js list method mycontroller objects pre-defined prism prismapp proper service services share sharetypes types usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.ShopperDisplay",
      "shortName": "prismApp.common.services.ShopperDisplay",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Handles sending data to the shopper display associated with the current workstation.",
      "keywords": "$scope api associated based check checks clear clears common contacts controller current data display exists formatting function handles hardware isshopperdisplay js messages method mycontroller notification object prismapp promise proper proxy redraw redraws registered representing result returns selected sending service services sessionsstorage sessionstorage set sets shopper shopperdisplay usage workstation",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.common.services.TaxService",
      "shortName": "prismApp.common.services.TaxService",
      "type": "service",
      "moduleName": "prismApp.common.services",
      "shortDescription": "Service provides controls and storage for Tax information",
      "keywords": "$scope api common controller controls formatting function js mycontroller prismapp proper service services storage tax taxservice usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.pos.services.DocumentPersistedData",
      "shortName": "prismApp.pos.services.DocumentPersistedData",
      "type": "service",
      "moduleName": "prismApp.pos.services",
      "shortDescription": "Service that handles storage and retrieval of document (transaction) data between controllers.",
      "keywords": "$scope address1 address2 address3 allowedtenders api automatically bill bill-to billtocustomerdata blankbillto blankdocinfo blankprintdata browser bt_cuid called card change changing contained controller controllers copies currentamt currentmode currenttype custom customer data default depositamountrequired deposittaken deposittendered depositused design designer desired displaying document document_sid documentinformation documentpersisteddata duplicatetransaction eftinvoicenumber emailaddress example exists fields forcecreditauth formatting fullname function handles items itemsids js lastname lastpurchase loads local lost loyalty method mycontroller needed newdocument newflag object objects orderbalancedue overriddenpermissions payload phone portal portion pos postalcode preserving pricelevel primary print printdata printer printing printtype prism prismapp processors proper property representing reset resets retrieval retrieving return rowversion seasonsid selecteditem service services sets sid storage storecredit storecreditpromptshown storeforeft storesid tag tagcopytype tenders tendersource title usage values var web",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "prismApp.services.Toast",
      "shortName": "prismApp.services.Toast",
      "type": "service",
      "moduleName": "prismApp.services",
      "shortDescription": "Used to display toast notifications on the screen. Each notification may be dismissed individually, regardless of the order in which it appears.",
      "keywords": "$scope _handlehttperror api appears body bottom clicked closed color controller corner delay dismissed display displayed error example force forceread formatting function generate generated header http indicating individually info js message method milliseconds mycontroller notification notifications number object onclose order primary prism prismapp proper remain rest scheme screen service services setting success text title toast toat translationid usage version visible warning",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "resourceLookupFilterTypes",
      "shortName": "resourceLookupFilterTypes",
      "type": "directive",
      "moduleName": "resourceLookupFilterTypes",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper resourcelookupfiltertypes resourcelookupfiltertypesctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "rpSelectInput",
      "shortName": "rpSelectInput",
      "type": "component",
      "moduleName": "rpSelectInput",
      "shortDescription": ".",
      "keywords": "api basecurrencysid bindings boolean bound call callback component components controls currencies currencyname current data display displayed entire event event-hook field_1 field_2 field_3 fields filter filtering formatting function htm html key key-string list modal model ng-if ng-model ngmodel null object optional paging path plugins posselecttender proper record repeater repeater-data repeaterdata repeaterobj required revert rpselectinput saved search select selected selection sid subsidiary syntax tells thing trigger true unselected usage value-key view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shippingPackages",
      "shortName": "shippingPackages",
      "type": "directive",
      "moduleName": "shippingPackages",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper shippingpackages shippingpackagesctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shippingPackagesCtrl",
      "shortName": "shippingPackagesCtrl",
      "type": "function",
      "moduleName": "shippingPackagesCtrl",
      "shortDescription": "Controller of shippingPackages",
      "keywords": "api controller description examples formatting function html javascript js proper shippingpackages shippingpackagesctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shippingPackagesMultiMatch",
      "shortName": "shippingPackagesMultiMatch",
      "type": "directive",
      "moduleName": "shippingPackagesMultiMatch",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper shippingpackagesmultimatch shippingpackagesmultimatchctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shippingPackagesMultiMatchCtrl",
      "shortName": "shippingPackagesMultiMatchCtrl",
      "type": "function",
      "moduleName": "shippingPackagesMultiMatchCtrl",
      "shortDescription": "Controller of shippingPackagesMultiMatch",
      "keywords": "api controller description examples formatting function html javascript js proper shippingpackagesmultimatch shippingpackagesmultimatchctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shipPkgAdd",
      "shortName": "shipPkgAdd",
      "type": "directive",
      "moduleName": "shipPkgAdd",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper shippkgadd shippkgaddctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shipPkgAddCtrl",
      "shortName": "shipPkgAddCtrl",
      "type": "function",
      "moduleName": "shipPkgAddCtrl",
      "shortDescription": "Controller of shipPkgAdd",
      "keywords": "api controller description examples formatting function html javascript js proper shippkgadd shippkgaddctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shipPkgDelete",
      "shortName": "shipPkgDelete",
      "type": "directive",
      "moduleName": "shipPkgDelete",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper shippkgdelete shippkgdeletectrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shipPkgDeleteCtrl",
      "shortName": "shipPkgDeleteCtrl",
      "type": "function",
      "moduleName": "shipPkgDeleteCtrl",
      "shortDescription": "Controller of shipPkgDelete",
      "keywords": "api controller description examples formatting function html javascript js proper shippkgdelete shippkgdeletectrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shipToggleReceive",
      "shortName": "shipToggleReceive",
      "type": "directive",
      "moduleName": "shipToggleReceive",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper shiptogglereceive shiptogglereceivectrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "shipToggleReceiveCtrl",
      "shortName": "shipToggleReceiveCtrl",
      "type": "function",
      "moduleName": "shipToggleReceiveCtrl",
      "shortDescription": "Controller of shipToggleReceive",
      "keywords": "api controller description examples formatting function html javascript js proper shiptogglereceive shiptogglereceivectrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "soAddItemAction",
      "shortName": "soAddItemAction",
      "type": "directive",
      "moduleName": "soAddItemAction",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path proper soadditemaction soadditemactionctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "soAddItemActionCtrl",
      "shortName": "soAddItemActionCtrl",
      "type": "function",
      "moduleName": "soAddItemActionCtrl",
      "shortDescription": "Controller of soAddItemAction",
      "keywords": "api controller description examples formatting function html javascript js proper soadditemaction soadditemactionctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "soCreateItem",
      "shortName": "soCreateItem",
      "type": "directive",
      "moduleName": "soCreateItem",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path proper socreateitem socreateitemctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "soCreateItemCtrl",
      "shortName": "soCreateItemCtrl",
      "type": "function",
      "moduleName": "soCreateItemCtrl",
      "shortDescription": "Controller of soCreateItem",
      "keywords": "api controller description examples formatting function html javascript js proper socreateitem socreateitemctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "soGetVendor",
      "shortName": "soGetVendor",
      "type": "directive",
      "moduleName": "soGetVendor",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path proper sogetvendor sogetvendorctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "soGetVendorCtrl",
      "shortName": "soGetVendorCtrl",
      "type": "function",
      "moduleName": "soGetVendorCtrl",
      "shortDescription": "Controller of soGetVendor",
      "keywords": "api controller description examples formatting function html javascript js proper sogetvendor sogetvendorctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "transRuleEdit",
      "shortName": "transRuleEdit",
      "type": "directive",
      "moduleName": "transRuleEdit",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path proper transruleedit transruleeditctrl usage view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "transRuleEditCtrl",
      "shortName": "transRuleEditCtrl",
      "type": "function",
      "moduleName": "transRuleEditCtrl",
      "shortDescription": "Controller of transRuleEdit",
      "keywords": "api controller description examples formatting function html javascript js proper transruleedit transruleeditctrl usage",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "zoutAudits",
      "shortName": "zoutAudits",
      "type": "directive",
      "moduleName": "zoutAudits",
      "shortDescription": ".",
      "keywords": "api components description directive examples formatting htm html javascript js path portrait proper usage view zoutaudits zoutauditsctrl",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "zoutAuditsCtrl",
      "shortName": "zoutAuditsCtrl",
      "type": "function",
      "moduleName": "zoutAuditsCtrl",
      "shortDescription": "Controller of zoutAudits",
      "keywords": "api controller description examples formatting function html javascript js proper usage zoutaudits zoutauditsctrl",
      "isDeprecated": false
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": false,
  "startPage": "/api",
  "scripts": [
    "b2d9a6dd.modules.js",
    "viewport-units-buggyfill.js"
  ]
};