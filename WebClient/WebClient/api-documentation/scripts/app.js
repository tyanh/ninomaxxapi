(function(ng) {
    var configHandler = ['$routeProvider', function($routeProvider) {
        var routeOptions = {
            templateUrl: 'partials/main.partial.html',
            controller: 'appCtrl',
            reloadOnSearch: false,
            resolve: {
                navigation: function() {
                    return window.navigationData;
                }
            }
        };

        $routeProvider.when('/', { redirectTo: '/Overview'});
        $routeProvider.when('/:name', routeOptions);
        $routeProvider.when('/:name/:child', routeOptions);
        $routeProvider.otherwise('/', { redirectTo: '/Overview'});

        var renderer = new marked.Renderer();

        renderer.table = tableRenderer;
        renderer.link = linkRenderer;
        renderer.code = codeRenderer;


        marked.setOptions({
            renderer: renderer,
            gfm: true,
            tables: true,
            breaks: true,
            pedantic: false,
            /*sanitize: true,*/
            smartLists: true,
            smartypants: false
        });
    }];

    var appController = ['$scope', 'navigation', '$http', '$sce', '$location', '$routeParams', function($scope, navigation, $http, $sce, $location, $routeParams) {
        $scope.navigation = navigation;
        $scope.jumpto = {};
        $scope.breadcrumb = [];

        //jumps to anchor on the page
        $scope.jumpTo = function(id) {
            $location.hash(id);
        };

        //loads the markdown file for current route
        function loadMarkdown() {
            if ($scope.activePage.markdown) {

                $http.get($scope.activePage.markdown)
                    .then(function(response) {
                        $scope.content = $sce.trustAsHtml(marked(response.data));
                    });

            } else {
                $scope.content = $sce.trustAsHtml('<p>No page content specified.</p>');
            }
        }

        $scope.init = function() {
            $scope.activePage = $scope.navigation.filter(function(val) {
                return val.name === $routeParams.name;
            })[0];

            if ($routeParams.child && $scope.activePage.children) {
                $scope.activePage = $scope.activePage.children.filter(function(val) {
                    if (val.name === $routeParams.child) {
                        $scope.activePage.isCollapsed = false;
                    }
                    return val.name === $routeParams.child;
                })[0];

            }

            for (key in $routeParams) {
                $scope.breadcrumb.push($routeParams[key]);
            }

            loadMarkdown();
        };

        //initialize the controller
        $scope.init();
    }];

    //renders all tables with bootstrap classes
    function tableRenderer(header, body) {
        var output = '<table class="table table-striped table-bordered">';
        output += header;
        output += body;
        output += "</table>";

        return output;
    };

    //prevents rendering of links that have {SERVERNAME} in them
    function linkRenderer(href, title, text) {
        if (href.indexOf('{SERVERNAME}') !== -1) {
            return href;
        } else {
            title = (title === null) ? '' : title;
            return '<a href="' + href + '" title="' + title + '">' + text + '</a>';
        }
    }

    //renders markdown code blocks to support highlighting and latex
    function codeRenderer(code, language) {
        var output;

        if (language === 'latex') {
            output = LatexIT.pre(code);
        } else {
            output = '<pre><code class="lang-' + language + '">';
            output += hljs.highlight(language, code).value;
            output += '</code></pre>';
        }

        return output;
    }


    //register modules
    var app = ng.module('prismApiDocs', ['ngRoute', 'ui.bootstrap']);
    app.config(configHandler)
    app.controller('appCtrl', appController);


    //bootstrap angular
    $(document).ready(function() {
        $.get('navigation.json', function(data) {
            window.navigationData = data;
            ng.bootstrap(document, ['prismApiDocs']);
        });
    });
})(window.angular)