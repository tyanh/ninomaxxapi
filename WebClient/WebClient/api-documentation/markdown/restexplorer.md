#REST API Explorer#

####Summary####
The API Explorer is a a tool that is provided as part of the SDK. API Explorer allows interactive exploration of the services, resources, and RPC calls, available through the REST API.

####Locating the REST API-Explorer####
The tool can be accessed through the left menu in this documentation by selecting REST API Explorer, or by pointing you browser to https://{SERVERNAME}/api-explorer

When you first browse to the API-Explorer you might get a warning from your browser it you have not installed the security certificate for the server. You can install the security certificate for the server or you can allow the exception each time you come to the server.


####Authentication####
While it is possible to browse many of the resources without a login, a user session allows full use of the API-Explorer functionality. So the first screen you see when using the API-Explorer is the login screen.

<img src="images/explorer/login.png">

Fill out the credentials you want to browse with and select the Login button on the right side.


#Services#

####Summary####

The services list is on the left side of the screen. Each Server Module installed shows up with a folder next to it. Depending on the options you selected when you installed your Retail Pro Prism server, you list might have different entries than the on at the left side of the screen. Additionally if you create custom server modulesthey will show up here as well.

<img src="images/explorer/servermodules.png">

If you click the folder next to a service it will list the RPC and REST resources served up by that server module. Clicking the RPC Methods or REST Resource containers will expand the list and show you a list of the methods or resources.

<img src="images/explorer/servicecontainers.png">


#RPC Method Introspection#

<img src="images/explorer/rpcmethods.png">

####Summary####

Looking at the list of the RPC methods that are provided by a service module is somewhat helpful, but you can also ask that method to describe itself by selecting the name of the list of RPC Methods.

For example this is the result of selecting the InvnLookupItem method from the RPS Service Module.

<img src="images/explorer/introspection.png">

Lets look at some of the information presented in this view. The method name is showed at the top with a description of the intent of the methods shown below that. For each parameter there will be a section that indicates the following information.

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `DataType`                      | The data type for the parameter. While all data is passed through XML or JSON as strings this indicates the actual type of data for the parameter.
| `Type`                          | Indicates if the parameter is for input or output. A value if "in" indicates a parameter for calling the RPC method with where a value of "out" indicates a parameter returned from the call to the RPC method.
| `Min. Length`                   | The minimum length of a values that can be passed in the parameter.
| `Max. Length`                   | The maximum length of a value that can be passed in the parameter.
| `Optional`                      | A flag that indicates if the parameter is optional. This is only applicable for "in" parameters.
| `Default Value`                 | If the parameter has a default value, the default value will be indicated here.
| `Encoded`                       | The encoded value indicates the character encoding for the parameter.

You can get this same information by executing a POST to https://{SERVERNAME}/v1/rpc with a payload asking for introspection.

####XML Payload####

```xml
<METHODCALLS>
  <METHODCALL NAME="InvnLookupItem" INTROSPECTION="TRUE" />
</METHODCALLS>
```

####JSON Payload####
```json
{
    "methodcalls": [
        {
            "methodcall": {
                "name": "InvnLookupItem",
                "introspection": "true"
            }
        }
    ]
}
```

#REST Resource Metadata#

<img src="images/explorer/restresources.png">

####Summary####

Just like getting a list of the RPC calls you can get a list of the REST resources served by a service module. Resources can be thought of as interfaces to tables or views in the database. Some resources are not related directly to any others. While other resources have sub-resources or "child" resources. Resources with a folder next to them have sub-resources. Selecting a resource will display the metadata for that resource.

For example the customer resources is shown below.

<img src="images/explorer/customermeta.png">

At the top of the resource window you can see the resource you are browsing. Below that you can see the HTTP method used, the accepts header for the call the server you are using and an option to apply a filter. Below that is the attribute grid. With a toggle to show or hide the default columns that all tables in the Retail Pro Prism have.

The attribute grid shown above has a row for every attribute present in the resource. There are columns for a lot of information related to the attributes, below is a list of some of the key attributes and the meaning of them.

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `attributesize`                 | For text attributes this is the maximum size of an entry that is allowed. For all other attributes this entry will be 0.
| `attributetype`                 | This indicates the source of the attribute. Persisted attributes are directly bound to database fields. Lookup attributes are information provided as a courtesy by linking to other resources. PersistedLookup attributes also show information from an other resources, but once the information is looked up it is stored locally on this resource. Collection attributes represent a link to a sub-resource. Calculated attributes provide a derived value that is not stored in the database. PersistedCalcualted attributes provide a derived value that is stored in the database.
| `datatype`                      | The data type for an attribute provides some context for the value that the attribute will hold. Possible values for data type are; Blob, Boolean, Byte, Clob, Currency, Date, DateTime, Float, Identifier, Image, Int16, Int32, Int64, String, and UPC
| `readonly`                      | If the readonly flag is TRUE the attribute cannot be set from the client applications through the API. If the readonly attribute is FALSE then the attribute can be set.
| `required`                      | The required flag indicates that a value for the attribute must be present at all times. For example when inserting a new entity the required attribute must have a value, and when updating an entity the required attribute must not be cleared.
| `valuelist`                     | For some of the integer (Int16, Int32, Int64) attributes the values represent specific things. The Value list provides a semicolon separated Value=Name list of the possible options for that attribute value.
| `comment`                       | The comment for each attribute provides some documentation as to the intent of the attribute.


You can sort the grid by clicking any of the column headers. You can also show and hide columns by clicking the down triangle in the upper right corner of the grid then clearing the checkboxes for the columns that you do not want to see. The columns you hide are remembered across each view so you do not need to hide them as you go from resource to resource.

<img src="images/explorer/columnselector.png">

#REST Get#

####Summary####

You can also use the grid to interactively build a submit a request for the server. Select the rows that represent the data you want to see returned from the server.

<img src="images/explorer/attributeselection.png">

In the example shown here we have selected Last_Name and First_Name from the list of attributes presented. This is the same as adding a "Cols" parameter to the GET request with the Last_Name and First_Name attributes. We can then click the "Send" button to sent the GET request to the server. The UI will then change to show to results of the GET request in a grid.

<img src="images/explorer/sendresults.png">

Notice the Request URL shows what the full request sent to the server was. Also note the Record Count at the bottom of the UI. There are also some new tabs at the top of the grid.

<img src="images/explorer/requestheaders.png">

The Request Headers tab shows the headers that were sent as part of the GET request.

<img src="images/explorer/requestpayload.png">

The Request Payload tab shows the payload that was sent to the server as part of the request. In this case it was empty as there is no payload for a GET request. On a PUT or a POST request the Request Payload tab would show the body of the request.

<img src="images/explorer/responseheaders.png">

The Response Headers tab shows the headers that came from the server in response to the request.

<img src="images/explorer/responsetree.png">

While the Table View shows the response parsed into a table you can use the Response Payload to view the actual response from the server. You can view the response in an interactive tree.

<img src="images/explorer/responseformatted.png">

By clicking the formatted link you can see the full response payload beautified.

<img src="images/explorer/responseraw.png">

By clicking the raw link you can see the full response payload as it was returned by the server.

<img src="images/explorer/editmode.png">

In the table view you can double click a record to take it into edit mode. This will allow you to modify the values of the attributes returned, and save them to the server.

#REST Put#

####Summary####

I addition to using the PUT form shown above you can also construct a PUT call manually by changing the REST method to PUT and using the form shown to here.

<img src="images/explorer/putform.png">

#REST Post#

####Summary####

For sending a new record to the server you can change the REST method to a POST and you can use either the POST form shown here.

<img src="images/explorer/postform.png">

Alternately if you want to construct the POST manually you can click the link to perform the post in RAW mode.

<img src="images/explorer/postraw.png">

#REST Delete#

####Summary####

You can also use the UI to perform a DELETE. Just change the REST method to DELETE and fill out the record SID.

<img src="images/explorer/deleteform.png">


