#UI Configurations#
- - -

The UI Configurations API provides the ability to register custom code that will be invoked when the Retail Pro Prism client initializes. There are many possible uses for these configurations such as loading assets or making http request to load data that will be used in other areas of the application.

Registraion of your handlers is done using the [`ConfigurationsManager`](../docs/#/api/Customizations.ConfigurationManager) provided through the [Client API](../docs). In the example below we will configure a [`ModelEvent`](../docs/#/api/prismApp.common.services.ModelEvent) handler that ensures when saving or updating a customer the first and last names are letter cased.




#Configuration Example#
- - - 

This entire example can be downloaded [here](examples/prism-plugins-sample.zip).

####config.js####

```js
var customerLetterCaseHandler = ['ModelEvent', function(ModelEvent){
    var handlerBefore = function($q, customer){
        var deferred = $q.defer();

        function capitalize(str){
            return str.charAt(0).toUpperCase() + str.slice(1);
        }

        //ensure first and last names are Letter Cased 
        customer.first_name = capitalize(customer.first_name);
        customer.last_name = capitalize(customer.last_name);

        //resolve the deferred operation
        deferred.resolve();

        //return the deferred promise
        return deferred.promise;
    };

    var listener = ModelEvent.addListener('customer', ['onBeforeSave', 'onBeforeInsert'], handlerBefore);
}]

ConfigurationManager.addHandler(customerLetterCaseHandler);
```

The code above registers a `customerLetterCaseHandler` that will be invoked when ever a [`CustomerModel`](../docs/#/api/prismApp.common.models.CustomerModel) broadcasts an *onBeforeSave* or *onBeforeInsert* event. The handler is passed the [`$q`](https://docs.angularjs.org/api/ng/service/$q) service as the first parameter and the [`CustomerModel`](../docs/#/api/prismApp.common.models.CustomerModel) object that broadcasted the event as the second parameter.

####Promise Resolution####

In order for the [`CustomerModel`](../docs/#/api/prismApp.common.models.CustomerModel) to determine when the `customerLetterCaseHandler` has finished its operation the handler must return a promise using angulars [`$q`](https://docs.angularjs.org/api/ng/service/$q) service. The `deferred.resolve();` notifies the [`CustomerModel`](../docs/#/api/prismApp.common.models.CustomerModel) that it is safe to proceed with downstream operations. 

Promises can also be rejected with `.reject()` this will halt any further execution, in the case of *onBeforeSave* and *onBeforeInsert* events a promise rejection would prevent the operation from ever being dispatched to the server.




####Adding the Script Reference####

In order for the config.js file to be loaded when the Retail Pro Prism client loads you must add a script referecne to the customizations.html. You can locate this file on your Prism server in the default directory ***C:\ProgramData\RetailPro\Server\WebClient***.

```html
<script type="text/javascript" src="/path/to/config.js"></script>
```
<div class="alert alert-danger" role="alert">**Note:** *This file is shared between all customizations. To ensure all customization continue functioning do not alter lines other then your own.*</div>