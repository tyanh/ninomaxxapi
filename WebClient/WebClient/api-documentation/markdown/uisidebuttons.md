#UI Side Buttons#
- - -

The UI Side Buttons API provides the ability to define buttons that load in the side bar of the Retail Pro Prism client. This allows third party developers to add custom actions that can be performed in addition to the default actions provided by the Retail Pro Prism client.

Registering Side Buttons is handled through the [`SideButtonsManager`](../docs/#/api/Customizations.SideButtonsManager). In the example below we will create a side button that opens a modal window that will search for employees by name.


#Side Buttons Example#
- - -

This entire example can be downloaded [here](examples/prism-plugins-sample.zip). It may be helpful to run through the [angularjs](https://angularjs.org/) tutorial before proceeding.  You can extract the files and place them in your Plugins directory located in your Prism installation located in ***C:\ProgramData\RetailPro\Server\WebClient\plugins***.

##config.js##
- - -

The **config.js** file registers a new side button with the [`SideButtonsManager`](../docs/#/api/Customizations.SideButtonsManager), defining the button label, icon, sections and a handler to be invoked when the button is clicked. 

**Note**: See [`SideButtonsManager`](../docs/#/api/Customizations.SideButtonsManager) for a detailed overview of the various parameters available when using `addButton()`.

In our handler for the side button we inject the `$uibModal` service which allows us to define a modal and open it using `$uibModal.open()`. Once `$uibModal.open()` is invoked control is passed to the controller defined in the `modalOptions` object.

```js
SideButtonsManager.addButton({
    label: 'Employee Search',
    icon: 'images/checked_32.png',
    sections: ['register', 'transactionRoot', 'transactionEdit'],
    handler: ['$uibModal', function($uibModal) {
       
        var modalOptions = {
            backdrop: 'static',
            keyboard: false,
            windowClass: 'full',
            templateUrl: '/plugins/prism-plugins-sample/side-buttons/employee-search-sample.htm',
            controller: 'employeeSearchSampleCtrl'
        };

        $uibModal.open(modalOptions);
    }]
});
```

##employee-search-sample-ctrl.js##
- - -

The **employee-search-sample-ctrl.js** provides the functionality for the employee search sample. It loads data using the `ModelService` and exposes `$scope` variables and methods that can be consumed by the **employee-search-sample.htm** view.

```js
 var employeeSearchSample = ['$scope', '$uibModalInstance', 'ModelService',
    function ($scope, $uibModalInstance, ModelService) {
        'use strict';
        $scope.criteria = {};

        $scope.search = function(){
            var filter = 'full_name,lk,' + $scope.criteria.searchText + '*';
            ModelService.get('Employee', { filter: filter })
                .then(function(employees){
                    $scope.employees = employees;
                });
        };
    }
];


window.angular.module('prismPluginsSample.controller.employeeSearchSampleCtrl', [])
    .controller('employeeSearchSampleCtrl', employeeSearchSample);
```

##Adding the Script Reference and Registering your Controller##
- - -

In order for the **config.js** and **employee-search-sample-ctrl.js** files to be loaded when the Retail Pro Prism client loads you must add a script reference to the **customizations.html**. You can locate this file on your Prism server in the default directory ***C:\ProgramData\RetailPro\Server\WebClient***.

```html
<script type="text/javascript" src="plugins/prism-plugins-sample/side-buttons/config.js"></script>
<script type="text/javascript" src="plugins/prism-plugins-sample/side-buttons/employee-search-sample-ctrl.js"></script>
```
Once the javascript files are included you must register your controllers module as a dependency of the application. This is done by adding the name of your dependency to the array of dependencies defined in the **customizations.js** file. You can locate this file on your Prism server in the default directory ***C:\ProgramData\RetailPro\Server\WebClient***.

```js
(function(ng) {
    var dependencies = [];

    /*DO NOT MODIFY ABOVE THIS LINE!*/

    //Usage example:
    //dependencies.push('dependencyName');
    

    dependencies.push('prismPluginsSample.controller.employeeSearchSampleCtrl');


    /*DO NOT MODIFY BELOW THIS LINE!*/
    ng.module('prismApp.customizations', dependencies, null);
})(angular);
```

The `dependencies.push('prismPluginsSample.controller.employeeSearchSampleCtrl');` line above registers our module as a dependency of the application. You will need to add this line for each module you create.

<div class="alert alert-danger" role="alert">**Note:** *These customization files are shared between all customizations. To ensure all customizations continue functioning do not alter lines other then your own.*</div>



##employee-search-sample.htm##
- - - 

The **employee-search-sample.htm** view defines the interface the user will see when the modal is opened. These views are essentially plain old html with angularjs attributes(directives) that allow data exposed by the controller to be bound to certain elements.

```html
<form name="employeeSearchForm">
    <div class="modal-content">
        <div class="modal-header">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Employee Search</h4>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="search">Employee Name</label>
                            <input tabindex="1" type="text" id="search" name="search" class="form-control" ng-model="criteria.searchText" required>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-lg btn-success" ng-click="search()" ng-disabled="!employeeSearchForm.$valid">Search</button>
                        </div>
                    </div>
                    <div class="col-md-8">
                       <table class="table table-bordered table-striped" ng-show="employees">
                            <caption>{{employees.length}} matches found.</caption>
                            <thead>
                                <tr>
                                    <th class="col-xs-3">Employee Name</th>
                                    <th class="col-xs-3">First Name</th>
                                    <th class="col-xs-3">Last Name</th>
                                    <th class="col-xs-1">Max Discount</th>
                                    <th class="col-xs-2">Hire Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="employee in employees">
                                <td>
                                    {{employee.employee_name}}
                                </td>
                                 <td>
                                    {{employee.first_name}}
                                </td>
                                 <td>
                                    {{employee.last_name}}
                                </td>
                                 <td>
                                    {{employee.max_discount_percent}} %
                                </td>
                                <td>
                                    {{employee.hire_date | date: 'short'}} 
                                </td>
                            </tr>
                            </tbody>
                       </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-lg btn-default" ng-click="$close()">Close</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
```
