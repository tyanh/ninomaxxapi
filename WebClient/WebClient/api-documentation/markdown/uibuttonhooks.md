#UI Button Hooks#
- - -

The UI Button Hooks API provides the ability to add custom behavior to existing buttons within the Retail Pro Prism client.
The Button Hooks API also adds the same custom behavior to the rpi-select-input custom directive using the event-hook tag instead of event-name.

Registering ButtonHooks is handled through the [`ButtonHooksManager`](../docs/#/api/Customizations.ButtonHooksManager). In the example below we register a handler to hook into the Tender button on the trasation screen. The handler will prompt the user asking if they would like to add a customer to the document.


#Button Hooks Example#
- - -
This entire example can be downloaded [here](examples/prism-plugins-sample.zip). It may be helpful to run through the [angularjs](https://angularjs.org/) tutorial before proceeding.

#HTML Example#
Creating your own button hooks in plugins is as simple as adding the attribute event-name to your buttons or event-hook to rpi-select-input tags.
&lt;button event-name="testEvent"&gt;Test Button&lt;button&gt;
&lt;rpi-select-input event-hook="testEvent"&gt;.. options ...&lt;rpi-select-input&gt;

##config.js##
- - -
The *config.js* file registers the handler to the Tender Transaction button with the [`ButtonHooksManager`](../docs/#/api/Customizations.ButtonHooksManager).

In order to register our handler to a specifc button we need the event associated with that button. This event can be found as an attribute of the button defined in the HTML, in our case it would be `event-name="posTransactionTenderTransaction"`.

```js
ButtonHooksManager.addHandler(['before_posTransactionTenderTransaction'],
    function($q, DocumentPersistedData, NotificationService, $uibModal, Templates, ModelService,$rootScope,HookEvent) {
        var deferred = $q.defer(), 
        //retrieve the active document from the cache
        
        activeDocument = ModelService.fromCache('Document')[0],
        //The HookEvent resource holds a reference to the button that triggered the event 
        $rootScope.hookEvent=HookEvent;
        //to access the button access the target property of HookEvent
        var activatingButton=$rootScope.hookEvent.target;
        
        hasSaleItems, 
        hasOrderItems, 
        hasCustomer;
       
        //filter sales items
        hasSaleItems = activeDocument.items.filter(function(item){
            return item.item_type === 1;
        }).length > 0;

        //filter order items
        hasOrderItems = activeDocument.items.filter(function(item){
            return item.item_type === 3;
        }).length > 0;

        //document has a customer
        hasCustomer = activeDocument.bt_cuid.length;

        //if document does not have a customer or order items and has sale items
        if(!hasCustomer && !hasOrderItems && hasSaleItems){
          
          //diplay confimation window 
          var confirm = NotificationService.addConfirm('Would you like to add a customer before tendering this transation?', 'Add Customer');

          //resolve the result of the confirmation
          confirm.then(function(confirmation){
            if(confirmation){
                //user chose to add a customer
                //reject promise
                deferred.reject();
            }
            else{
                //user chose not to add a customer
                //resolve the promise
                deferred.resolve();
            }
          });
          
        }else{
          //no need to prompt to add a customer
          //resolve the promise;
          deferred.resolve();
        }
        
        return deferred.promise;
    }
);
```


##Adding the Script Reference##
- - -

In order for the **config.js** file to be loaded when the Retail Pro Prism client loads you must add a script referecne to the **customizations.html**. You can locate this file on your Prism server in the default directory ***C:\ProgramData\RetailPro\Server\WebClient***.

```html
<script type="text/javascript" src="plugins/prism-plugins-sample/button-hooks/config.js"></script>
```
<div class="alert alert-danger" role="alert">**Note:** *These customization files are shared between all customizations. To ensure all customizations continue functioning do not alter lines other then your own.*</div>
