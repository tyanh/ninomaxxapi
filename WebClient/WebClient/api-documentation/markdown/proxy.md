#Communications API (Proxy)#
- - - 

####Summary####
There is also a need to have customizations in the lane or at the workstation being used. This type of customization leverages an exposed API on the Prism Proxy. This API allows an approved customization to sit between the clients and the server and perform tasks based on the traffic that goes back and forth. This section of the SDK details the API and the messaging protocols that is needed to create a communication customization.

####Structure####
As Retail Pro Prism uses a distributed communication model, communication customizations leverage the Prism Proxy communications node as an attachment point for custom code. Customizations can be an application or service running on the same machine as the proxy, or running on a remote machine. Customizations can be written in any language as long as the customization can host a ZeroMQ response client using TCP for transport.

For more information on 0MQ please see the following resources

+ http://zeromq.org/
+ http://nichol.as/zeromq-an-introduction
+ http://www.codeproject.com/Articles/488207/ZeroMQ-via-Csharp-Introduction
+ http://learning-0mq-with-pyzmq.readthedocs.org/en/latest/

####Message Patterns####
At this time we use a request/response to allow customizations to subscribe to the REST calls that travel back and forth between the clients and the server. When the customization "intercepts" a message it can view and or edit the parameters of the URI and the payload.

In the future we will support additional messaging types as needed.

####Installation or Registration####
The prism Proxy will only attempt to use customizations that it "knows" about. For the Proxy to know about a customization it needs to be registered with the server and assigned to a proxy for use. A best practice for customization developers to follow would be to create an installer for their customization, but if you do not have an installer for the customization you can add the customization manually using the Administrative Console in the Retail Pro Prism Client.

You will need to create a customization record to represent your customization in general, and then add the customization to a proxy that you want to work with.

####Session Management####
If your customization is an EXE that is on the same machine as the proxy you can set the proxy to start and stop the customization, otherwise starting and stopping the customization is an external task that the proxy is not responsible for.

####Command Messages####
After the proxy starts and discovers the customizations assigned to it. The proxy then tries to open a 0MQ request-reply channel with the customization to pass command and control messages to the customization. All of the messages use an XML request and response payload. See below for details of each message.

#Get Info#

####Summary####

The call to Get Info is the first call into the customization. The purpose of this call is for the customization to confirm the details about the version information and license data for the customization.

####Request####

```xml
<CONTROL>
  <COMMAND>GetInfo</COMMAND>
</CONTROL> 
```

####Response####

```xml
<CONTROL>
  <NAME>Customization Name</NAME>
  <VERSION>0.0.0.0</VERSION>
  <DEVELOPERID>0</DEVELOPERID>
  <CUSTOMIZATIONID>0</CUSTOMIZATIONID>
</CONTROL>
```
####Parameter Details####

In the response the following values are expected

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Name`                          | The friendly name for the customization. This is used for display purposes from the proxy in logging and for error messages.
| `Version`                       | The version for the customization. Version numbers can be a single integer or a series of integers segmented with a . (period)
| `DeveloperID`                   | The ID issued by Retail Pro to identify a customization developer.
| `CustomizationID`               | The ID issued by Retail Pro to identify a single customization.


#Set Config Info#
- - - 

####Summary####

The Set Config Info is called to provide the customization with both global and workstation specific configuration data.

####Request####

```xml
<CONTROL>
  <COMMAND>SetConfig</COMMAND>
  <BASECONFIG></BASECONFIG>
  <PROXYCONFIG></PROXYCONFIG>
</CONTROL>
```

####Response####

```xml
<ACK/>
```
####Parameter Details####

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Base Config`                   | This is the global configuration for the customization for all workstations in a given installation.
| `Proxy Config`                  | This is the local configuration for the customization that is specific to the proxy and the customization it loads.


#Get Subscriptions#
- - - 

####Summary####

The Get Subscriptions call is issued to the customization is to get the list of subscriptions the customization is interested in.

####Request####

```xml
<CONTROL>
  <COMMAND>GetSubscriptions</COMMAND>
</CONTROL>
```

####Response####

```xml
<SUBSCRIPTIONS>
  <SUBSCRIPTION>
    <NAME></NAME>
    <DIRECTION></DIRECTION>
    <HTTPGET></HTTPGET>
    <HTTPPUT></HTTPPUT>
    <HTTPPOST></HTTPPOST>
    <HTTPDELETE></HTTPDELETE>
    <RESOURCE></RESOURCE>
    <PATTERN></PATTERN>
    <PAYLOAD></PAYLOAD>
    <REDIRECTED></REDIRECTED>
  </SUBSCRIPTION>
</SUBSCRIPTIONS> 
```

####Parameter Details####

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Name`                          | This is the name for the subscription used in logging and for error messages.
| `Direction`                     | This is the direction of the traffic you are interested in. Possible value are dToClient and dFromClient
| `HTTPGet`                       | This Boolean flag indicates if you are interested in GET operations.
| `HTTPPut`                       | This Boolean flag indicates if you are interested in PUT operations.
| `HTTPPost`                      | This Boolean flag indicates if you are interested in POST operations.
| `HTTPDelete`                    | This Boolean flag indicates if you are interested in DELETE operations.
| `Resource`                      | This is the resource you are interested in. sub resources are separated with a / (forward slash or stroke) e.g. customer/address
| `Pattern`                       | The 0MQ message pattern to use for communication. Currently only mpRR is supported.
| `Payload`                       | This Boolean flag indicates that you want the payload passed in with the notification. If you do not need the payload setting this to false will provide better performance.
| `Redirected`                    | When Direction is set to dFromClient, setting this Boolean flag to TRUE indicates that the communication will route only through the customization and never reach the server. Even when the Redirected flag is set to false you can choose on a per notification basis to stop the communication from reaching the server. At this time Redirected is ignored.

#Event Notifications#

####Summary####

Whenever a call goes from in or out of the proxy the subscriptions are evaluated and if the criteria for the subscription matches the message the customizations are notified of the event.

####Request####

```xml
<EVENT>
  <DIRECTION></DIRECTION>
  <STATUSCODE></STATUSCODE>
  <HTTPVERB></HTTPVERB>
  <RESOURCENAME></RESOURCENAME>
  <URI></URI>
  <HEADERS></HEADERS>
  <PARAMS></PARAMS>
  <PAYLOAD></PAYLOAD>
  <CONTENTTYPE></CONTENTTYPE>
</EVENT>
```

####Response####

```xml
<EVENT>
  <HEADERS></HEADERS>
  <PARAMS></PARAMS>
  <PAYLOAD></PAYLOAD>
  <CONTINUE></CONTINUE>
  <STATUSCODE></STATUSCODE>
</EVENT>
```

####Parameter Details####

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Direction`                     | The value of Direction indicates the direction of the message. Possible values are FromClient or ToClient
| `StatusCode`                    | For messages that have a Direction of ToClient the StatusCode value will indicate the HTTP status of the response from the server. For messages that have a Direction FromClient and a Continue of FALSE the status code can be set by the customization. See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10 for more information.
| `HTTPVerb`                      | This is the HTTP Verb that is used for the request. Possible values for HTTPVerb are GET, PUT, POST, or DELETE.
| `ResourceName`                  | The name of the resource that triggered the message. This will match the subscription resource.
| `URI`                           | The actual URI sent to the server including and record specific SID values.
| `Headers`                       | The headers of the request or response sent to/from the server. This value may be modified and returned by a customization. If you are bypassing the server many of the headers are required to be provided by the customization.
| `Params`                        | The URI parameters sent to the server as part of the request. This is basically everything after the ? in the URI. This value may be modified and returned by a customization.
| `Payload`                       | This is the body of the clients request or response. This value may be modified and returned by a customization. NOTE: GET calls have no request body and DELETE calls have no request or response body.
| `ContentType`                   | This indicates the format of the Payload. Possible values are text/json, text/xml, application/json or application/xml
| `Continue`                      | This Boolean parameter can be included in responses for messages that have a Direction of FromClient. Setting Continue to FALSE will cause the message to not be sent to the server and instead the payload will then be returned to the client as a response. Remember to also set a StatusCode.

#Set User Token#

####Summary####

When a user authenticates they get an authentication token to bind them to a user session. The SetUserToken call will notify that token of the assignment of the token. This can be used to perform out of band requests of the server using the users security session.

####Request####

```xml
<CONTROL>
  <COMMAND>SetUserToken</COMMAND>
  <TOKEN></TOKEN>
  <PRISMSERVER></PRISMSERVER>
  <PRISMSERVERPORT></PRISMSERVERPORT>
</CONTROL>
```

####Response####

```xml
<ACK/>
```

####Parameter Details####

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `Token`                         | Token contains the Prism Auth Token for the logged in users security session.
| `Prism Server`                  | Prism Server contains the address of the prism server the Prism Proxy is connected to.
| `Prism Server Port`             | Prism Server Port contains the port the prism server is listening on.


#Shutdows#

####Summary####

When a user exits the proxy, the proxy will notify the customization that it is closing. This gives customizations a change to do any cleanup, and in the case of AutoStarted application this is where the application can terminate itself.

####Request####

```xml
<CONTROL>
  <COMMAND>Shutdown</COMMAND>
</CONTROL>
```

####Response####

```xml
<ACK/>
```

####Parameter Details####

There are no parameters for the shutdown command.

