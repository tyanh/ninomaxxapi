#REST Customization API#
- - -
Retail Pro Prism provides an HTTP server that implements both a REST API and an RPC API. While the REST API is dominant, both are covered in this section. Developers of utilities and third party clients can both interface with the REST API to interact with the Retail Pro Prism server.

To make the most of this section you should have a basic understanding of REST and the basic REST methods GET, PUT, POST, and DELETE. For more information related to REST please see...

* [http://rest.elkstein.org/](http://rest.elkstein.org/)
* [http://www.slideshare.net/nitin_stephens/understanding-rest](http://www.slideshare.net/nitin_stephens/understanding-rest)
* [http://en.wikipedia.org/wiki/Representational_state_transfer](http://en.wikipedia.org/wiki/Representational_state_transfer)

**Note:** *In this section there are a number of interactive examples that you can run against a Retail Pro server please note that {SERVERNAME} should be replaced by your Retail Pro server. Also note that Retail Pro requires HTTPS for communication and defaults to port 443.*


#Authentication#
- - -
####Summary####

While there is some information you can get from a server without authenticating to it, most of the operations a custom developer will be interested in require you to authenticate to the server you want to work with. Very specifically to access most data on the server a user session is required. To establish a user session you will need to authenticate (log in). The authentication process is performed in a series of steps documented here.

####Authentication Process#####

1. Perform a GET on https://{SERVERNAME}/v1/rest/auth In the response look in the HTTP header for the Auth-Nonce
2. Generate an Auth-Nonce-Response using the following calculation caculation 
```latex
$$Auth\textrm{-}Nonce\textrm{-}Response  = ( trunc(\frac{Auth\textrm{-}Nonce}{13}) \bmod 99999 ) \times 17$$ 
```    
3. Perform a GET on https://{SERVERNAME}/v1/rest/auth?usr={USER}&pwd={PASSWORD} with both the Auth-Nonce and the Auth-Nonce-Response values in the header. Replace the {USER} and {PASSWORD} with actual values.
4. Read the Auth-Session value from the response. This is your session token and is used to identify your connection. It must be present in the header of all calls made for your session.
5. Perform a GET on https://{SERVERNAME}/v1/rest/sit?ws={WORKSTATIONNAME} to claim a seat. (Don't forget to add the Token to the request header from step 4.)

####Closing the session####

When you are finished you can perform a GET on https://{SERVERNAME}/v1/rest/stand to release your user session.


#URL Components#
- - -

####Summary####

Constructing a Prism URL is similar to constructing any other internet address. Consider the following url:

https://{SERVERNAME}/v1/rest/customer/987239487293874?cols=first_name,last_name

####Address Components####

|URL Element               | Purpose                                                                                                                  |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------|
|https://                  | Protocol. We always use HTTPS with Prism                                                                                 |                                                                                                     
|{SERVERNAME}              | The address of your server                                                                                               |                                                                                      
|v1                        | The version of the API                                                                                                   |                                                                                  
|rest                      | The REST or RPC indicator                                                                                                |                                                                                     
|customer                  | The RESOURCE container (think table name)                                                                                |                                                                                                     
|987239487293874           | The SID or resource identifier (think record id)                                                                         |                                                                                                            
|?                         | The first parameter separator. The first parameter is separated by a ? all other parameters are separated by a &         |                                                                                                                                                                            
|Cols=first_name,last_name | Parameters (Cols, Filter, Sort)                                                                                          |                                                                                           


All of the REST calls you make will start with https://{SERVERNAME}/v1/rest/

####Multiple Parameters####

Multiple parameters can of course be used as stated above use the first parameter is separated from the rest of the URL with a ? and any additional parameters are separated with an &. For example if I want to filter and use a sort in the same URI the following syntax can be used. e.g. https://{SERVERNAME}/v1/rest/customer?cols=first_name,last_name&filter=last_name,eq,harkness


#Resource Identifiers#
- - - 

####Location####

The resource identifiers follow the "/v1/rest/" portion and continue until a ? or the end of the URI. They will always be a combination of RESOURCE and SID elements and may appear in any of the following patterns.

* RESOURCE
* RESOURCE\SID
* RESOURCE\SID\RESOURCE
* RESOURCE\SID\RESOURCE\SID
* RESOURCE\SID\RESOURCE\SID\RESOURCE
* RESOURCE\SID\RESOURCE\SID\RESOURCE\SID

####Resources####

The resource name indicates the collection you want to get data from. All of the resources we discovered above using services can be accessed. Accessing a resource is like getting a list of all the records in a table. For example if you do a GET on https://{SERVERNAME}/v1/rest/customer you will get all the records in the customer container.

####System Identifiers (SID)####

To get a specific record from the collection you can specify the SID for that record as well. e.g. https://{SERVERNAME}/v1/rest/customer/87239847928734 will fetch just the customer record with the SID of 87239847928734. In Prism a SID is the primary way to identify a given record.

**Note:** *a SID is an INT64 number that may be positive or negative in your data.*

####Sub-Resources####

There are some resources that have "child" resources or sub-resources. These resources represent collections of related data to the "parent" resource. Accessing these sub-resources uses the same syntax that we have just covered. For example https://{SERVERNAME}/v1/rest/customer/87239847928734/address returns us all the address records for customer SID of 87239847928734. And https://{SERVERNAME}/v1/rest/customer/87239847928734/address/8723984798234 will return us the address with SID 8723984798234 for the customer with SID 87239847928734.

#Cols Parameter#
- - - 

####Summary####

By default the only value that is returned when you get a resource is a list of SIDs for that collection. To get additional data you can use the Cols parameter to control what attributes (fields) are returned in the payload. Essentially Cols is a comma separated list of the attribute names that you want returned.

####Syntax####

The basic syntax for the cols parameter is cols=attribute1,attribute2,attribute3. e.g. https://{SERVERNAME}/v1/rest/customer?Cols=first_name,last_name This would return the first name and last name for each customer. For a listing of the attributes that you can use refer to the META for each of the resources.

####All Attributes####

To return all the attributes for a resource you can specify a * as an attribute. e.g. https://{SERVERNAME}/v1/rest/customer?Cols=* This will returns all the attributes for a given resource. While this is simple it should not be used in production as there is a performance penalty.

####Attributes from child resources####

To return attributes from sub resources you can specify the resource name and attribute name separated by a . (period) e.g. https://{SERVERNAME}/v1/rest/customer?Cols=last_name,address.address_line_1


#Filter Parameter#
- - -
####Summary####

SIDs are a great way to identify a specific record in a collection, but to use the SID you first have to find it. This is where filters come in. Filters allow you to apply some logic to the collection that comes back from the server.

####Syntax####

The basic syntax for a filter is filter={ATTRIBUTE},{OPERATOR},{CRITERIA} e.g. to get the SID for a customer with a last name "Harkness" you would construct a filter similar to the following. https://{SERVERNAME}/v1/rest/customer?filter=last_name,eq,harkness The attribute is the name of the attribute to search in, the operator provides the type of comparison, and the criteria is the value to look for.

####Comparison Operators####

The current operators you can use in a filter are as follows

* EQ - Equal to
* NE - Not equal to
* LT - Less than
* GT - Greater than
* LE - Less than or equal to
* GE - Greater than or equal to
* NL - Is null
* NN - Not Null
* LK - Like

While most of these operators use the pattern mentioned above, there are a couple of exceptions.

* NL and NN do not accept a criteria element e.g. filter=active,nn
* LK filter uses a * as a wildcard. For example s* matches all entries starting with s

####Compound Filters####

Compound filters can take a couple of different formats. The most basic of which is just including multiple basic filter parameters. This format of compound filters will apply the filters with an exclusive or XOR e.g. https://.../customer?filter=first_name,eq,jack&filter=last_name,eq,harkness

Compound filters can also use AND and OR clauses with parenthesis to establish grouping. e.g. https://{SERVERNAME}/v1/rest/customer?filter=(first_name,eq,jack)AND(last_name,eq,harkness)

#Sort Parameter#
- - - 
####Summary####

The sort parameter allows you to determine the order the records in the collection are presented.

####Syntax####

The basic syntax for a sort is sort={ATTRIBUTE},{DIRECTION} e.g. to return the customer records sorted in ascending order by last name you would use the parameter https://{SERVERNAME}/v1/rest/customer?sort= last_name,asc

####Direction Modifiers####

The current directional modifiers you can use in a sort are as follows

* ASC - Ascending
* DESC - Descending

#Count Parameter#
- - - 
####Summary####

The count parameter allows you to indicate to the server that you want the total count of records returned. This is usefull for paging, but a costly operation on larger tables. The count is returned in the ContentRange header on the response.

####Syntax####

The only syntax for counting the records is count=true e.g. to return the count of customer records you would use the parameter https://{SERVERNAME}/v1/rest/customer?count=true

#Page_Size Parameter#
- - - 
####Summary####

The page_size parameter allows you to indicate to the server the number of records you want back in each page of results. Page_size in conjunction with the page_no parameter implement paging in Prism. The record range is returned in the ContentRange header on the response.

####Syntax####

The syntax for a specifying a page size is page_size={number of records} e.g. to return the customer records 15 at a time you would use the parameter https://{SERVERNAME}/v1/rest/customer?page_size=15

#Page_No Parameter#
- - - 
####Summary####

The page_no parameter allows you to indicate to the server the page in a paginated set you want back in the results. Page_no in conjunction with the page_size parameter implement paging in Prism. Page_no is 1 based. 

####Syntax####

The syntax for a specifying a page no is page_no={page number} e.g. to return the third page of customer records you would use the parameter https://{SERVERNAME}/v1/rest/customer?page_no=3


#HTTP Methods#
- - -

####Summary####

The Prism server supports the basic REST methods GET, POST, PUT, and DELETE. Some of the basics are explained here, and more details are given below.

*GET*

The HTTP get is for fetching data from the server. The GET allows no request payload, but does return a response payload.

*POST*

The HTTP POST method is used to insert data into the system. The POST method sends a request payload and returns a response payload. Inserting a record may have other requirements, but one requirement that all resources have in common is the ORIGIN_APPLICATION attribute. You must provide an origin application in the payload for a POST to succeed.

*PUT*

The HTTP PUT is used to update data on the server. The PUT method sends a request payload and returns a response payload. There is a filter requirement for PUT calls. When you GET a record from the server one of the attributes is row_version. This is used for opportunistic locking of records. To perform a PUT you must include a filter that includes the row_version. e.g. https://{SERVERNAME}/v1/rest/customer?filter=row_version,eq,37

*DELETE*

The HTTP Delete method is used to remove a record from a collection. The Delete method accepts no request payload and returns no response payload.

#HTTP Headers#
- - -

####Summary####

There are a number of headers that the Prism server looks at to determine the format of the data in the payloads.

####ACCEPT Header####

Prism supports both XML and JSON format for request and response payloads. To work with this you need to be able to modify (or add) header fields to your HTTP requests. Similar to adding the token for authentication you will need to tell the server the format of the data you want to work with. You do this with the accept header.

The ACCEPT header indicates the format of data you want returned for the response of a request you are making to the server. Simply put when you make a request modify the ACCEPT header to specify the format you want the response in.

Available values for the ACCEPT header are

* application/xml
* application/json
* text/xml
* text/json

**Note:** *Currently Prism does not provide formatted output for text/xml or text/json.*

####CONTENT-TYPE####

In addition to the expected response format mentioned above, when sending a request to the server you need to specify the type of payload you are sending. This is done with the CONTENT-TYPE header.

Available values for the CONTENT-TYPE header are

* application/xml
* application/json
* text/xml
* text/json

#Payload Samples#
- - -
####Summary####

The menu links to the left under REST provide some request and response payloads samples in XML and JSON format.
