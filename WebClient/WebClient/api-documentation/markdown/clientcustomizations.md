#Prism Client Customizations#
- - - 

Prism provides a the ability to customize the Retail Pro Prism client appearance and behavior using the tools and APIs provide in UI Themes, UI Side Buttons, UI Button Hooks and UI Configurations.


The UI Side Buttons, and UI Button Hooks make use of certain [angularjs](https://angularjs.org/) elements. Basic knowledge of [angularjs](https://angularjs.org/) is recommended before attempting to implement these customizations.  

####Customizations Overview####

* [UI Themes](#/Client%20Customizations/UI%20Themes) - The Retail Pro Prism clients appearance is determined using a set of css rules that are applied to the HTML elements within the application. These css rules can be overridden in order to customize appearance of the application as a whole.

* [UI Side Buttons](#/Client%20Customizations/UI%20Side%20Buttons) - This API allows the addition of buttons that appear in the sidebar of the Retail Pro Prism and allow custom code to be invoked when the button is clicked. Examples of its use can be found in the UI Side Buttons section.

* [UI Button Hooks](#/Client%20Customizations/UI%20Button%20Hooks) - This API allows the registration of custom code to be invoked when existing buttons within the Retail Pro Prism client are interacted with by the user. Examples of its use can be found in the UI Button Hooks section. 

* [UI Configurations](#/Client%20Customizations/UI%20Configurations) - This API allows the registration of custom code that will be invoked when the Retail Pro Prism client is first initialized. Examples of its use can be found in the UI Configurations section. 

* [Prism Plugin Examples](#/Prism%20Plugin%20Examples) - Contains information about Plugin Examples and sample code that have been created. 
