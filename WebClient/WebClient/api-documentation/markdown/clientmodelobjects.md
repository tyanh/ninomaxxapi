#Client Model Objects#
- - - 

The Prism Client API provides an abstracted layer of tools enabling simplified access to server side resources through the client web application. Use of these objects requires some knowledge and understanding of javascript and Angularjs.

#Dependency Injection or Inversion of Control#
- - - 

The angularjs framework makes heavy use of dependency injection to promote reuse and testing. Familiarizing yourself with how Dependency Injection works in angular will facilitate easy consumption of the the objects.

####Example of Dependency Injection####

 ```js
 var MyController = ['$scope', 'greeter', function(renamed$scope, renamedGreeter) {
     //...
 }];
 ConfigurationManager.addHandler(MyController);
 ```

#Using Prism Model Objects#
- - - 

The Model Objects enable the encapsulation of server side resources e.g.(Customer, Document, Address, etc.) abstracting away the details of CRUD(GET, POST, PUT, DELETE) operations to and from the server.

####Injecting the `ModelService`####

In order to begin using the `ModelService` it must first be injected into your controller or service. The example below demonstrates how to inject the `ModelService` into a controller.

 ```js
 var MyController = ['ModelService', function(ModelService) {
     //do work with model service...
 }];
 ConfigurationManager.addHandler(MyController);
 ```
####Creating Objects####

Once we have injected the `ModelService` into our controller we can then use `ModelService.create()` to create a new instance of a `CustomerModel`. After that we call the `insert()` method which will POST that customer to the server via an ansynhronous http request.

 ```js
 var MyController = ['ModelService', function(ModelService) {
     var customer = ModelService.create('Customer');

     customer.first_name = 'John';
     customer.last_name = 'Doe';
     customer.insert();
 }];
 ConfigurationManager.addHandler(MyController);
 ```
The above example is the perfect segue into our next topic, promises.

####Asynchronous Requests and Promises####

The [CommonJS Promise proposal](http://wiki.commonjs.org/wiki/Promises) describes a promise as an interface for interacting with an object that represents the result of an action that is performed asynchronously, and may or may not be finished at any given point in time.

Angularjs makes extensive use of promises to control the flow of execution within the application. The Model Objects make use of this promise interface in order to provide a way define callback functions that will execute if and when a promise resolves or rejects its asynchronous operation(s).

Using the code defined in the above example for creating a new customer we can see how this promise interface works.

 ```js
 var MyController = ['ModelService', function(ModelService) {
     var customer = ModelService.create('Customer');

     customer.first_name = 'John';
     customer.last_name = 'Doe';
     customer.insert()
         .then(function(){
             //this callback will execute if the insert operation was successful
             //we can then access the newly created customers sid value
             console.log(customer.sid);
         },
         function(){
             //this callback will exectute is the insert operation failed
             //examples of a failure would be http error codes in the 400, and 500 range
             //do error handling here
         });
 }];
 ConfigurationManager.addHandler(MyController);
 ```
####Model Objects Methods####

All Model Objects share the following methods, `insert()`, `save()`, `remove()`. These methods are inherited from the [`BaseModel`](/docs/#/api/prismApp.common.models.BaseModel) Object.

####Loading Existing Resources with `ModelService`####

In addition to creating new Model Objects the `ModelService` can be used to load existing server side resources using `ModelService.get()`.


 ```js
 var MyController = ['ModelService', '$scope', function(ModelService, scope) {
     //get a customer with the sid 123456
     //change the first_name and save the customer
     ModelService.get('Customer', { sid: 123456 })
         .then(function(customers){
             scope.customer = customers[0];
             scope.customer.first_name = 'Joe';
             scope.customer.save();
          });


     var params = {
         filter: '(first_name,eq,John)AND(last_name,eq,Doe)',
         // get all customers with first_name 'John' and last_name 'Doe'
         sort: 'last_name, desc'
         // sort list by last_name descending
     }

     ModelService.get('Customer', params)
         .then(function(customers){
             scope.customers = customers;
         });

 }];
 ConfigurationManager.addHandler(MyController);
 ```

#Listening for Events with `ModelEvent`#
- - - 

The `ModelEvent` service can be used to add listeners that will execute a specific handler whenever a certain Model fires a `save`, `insert`, or `remove` operation.

In order for the Model Object to determine when the provided handler has completed its work, the handler must return a promise and resolve or reject this promise. The handler function will be automatically provided with two parameters, the first $q is a services that allows the construction of promises and the second is an instance of the Model Object that the operation is taking place upon. A brief sidenote should you need to perform any asynchronous operations you should make sure that all operations have completed before resolving or rejecting your promise within your handler.

When adding a listener you must provide the resource to listen for changes e.g(customer, address, phone, etc). The event to listen for *onBeforeSave, onAfterSave, onBeforeInsert, onAfterInsert, onBeforeRemove, and onAfterRemove*. Finally a handler function must be provided that contains the code to be executed when an event fires matching your listener.

 ```js
 var MyController = ['ModelEvent', function(ModelEvent) {
     var handlerBefore = function($q, customer){
         var d = $q.defer();
         console.log('Customer Begin Save');

           //change the first_name of the customer before it is saved
         customer.first_name = 'Joe';

         d.resolve();
         return d.promise;
    };

    var handlerAfter = function($q, customer){
        var d = $q.defer();
        console.log('Customer End Save');

        //change the first_name of the customer after it is saved
        customer.first_name = 'John';

        d.resolve();
        return d.promise;
    };

    ModelEvent.addListener('customer', 'onBeforeSave', handlerBefore);
    ModelEvent.addListener('customer', 'onAfterSave', handlerAfter);
 }];
 ConfigurationManager.addHandler(MyController);
 *  ```
