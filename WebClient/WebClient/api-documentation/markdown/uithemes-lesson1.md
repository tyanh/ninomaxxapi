#UI Theme Customization Hands On#
- - - 

##Summary##
In this hands on tutorial we are going to create a custom branded Prism client for one of our merchants ISC. 

###Introduction###
The Prism client allows customization of the CSS that is used to control the look and feel (Theme) of the application. You can find references for most of the lessons in the SDK related to the UI API. The lesson below will modify the prism software on your server. This will have effect for anyone connected to the server. To prevent problems with you operations this should not be done to your production server while in use.

###Best Practices###
Do NOT modify the HTML files or any other CSS files in your prism install. While those files can be changed by you, those changes will be overwritten when the next update is installed. Instead use the customer.css that is located in C:\ProgramData\RetailPro\Server\WebClient\styles\.

###Setup###
This lesson has a few prerequisites that you will need before we begin. First and foremost make sure you have access to the customer.css file on your server. Additionally there are some custom resources we need and we need to set a preference in the admin console. You can use the following steps to complete those parts of the prerequisites.

####Resources####
There are some custom images we are going to use for this hands on and we need to unpack them to the proper location.

1. Start by downloading the resources for this exercise from <A href="examples\uithemes-tutorial1.7z">HERE</A>
1. Create a folder named custom on your Prism server in C:\ProgramData\RetailPro\Server\WebClient\images
1. Unpack the resources to the C:\ProgramData\RetailPro\Server\WebClient\images\custom folders on your Prism server

####Layouts####
We need to make sure our client is configured for the "Simple Customer" follow the steps below to set this.

1. Open your web browser and browse to the root of your Prism server.
1. Login to the system using your administrator login.
1. Enter the Admin Console
1. Select Preferences & Settings > Themes & Layouts
1. Change the Transaction Screen Layout to the "Simple Customer" option
1. Click Save 
1. Click Exit

##Customer.css##
As mentioned before we want to put all of our changes in the customer.css file located in C:\ProgramData\RetailPro\Server\WebClient\styles\customer.css

Open your text editor of choice and open the file customer.css We will be using this file throughout the rest of the exercises below.

##Backgrounds##

<img src="images/uithemes-lesson1/stock.png" class="table-bordered">

Lets start by changing the background of the Prism application. We have a background that will fit nicely we just need to add some custom CSS to tell the application to use it. 

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
body
{
  background-image: url('/images/custom/back-dk.jpg');
  background-position: center top;
  background-size: 100%;
  background-repeat: no-repeat;
  background-color: #060C1C;
}
```

This sets the background image to one of the files we unpacked earlier, and sets a number of other properties related to the way the image is presented.

For specifics on each of the CSS properties used here you can check out http://www.w3schools.com/css/default.asp

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

The background should now be the custom NASCAR background.

<img src="images/uithemes-lesson1/background.png" class="table-bordered">

##Logo##

Lets now change the prism logo on the login page to a NASCAR logo

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
body > div > div > div > div > div > div.panel-body > div.col-md-5 > img
{
 background: url('/images/custom/badge.png') no-repeat;
 display: block;
 -moz-box-sizing: border-box;
 box-sizing: border-box;
 width: 200px; /* Width of new image */
 height: 199px; /* Height of new image */
 padding-left: 200px; /* Equal to width of new image */
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)
 
<img src="images/uithemes-lesson1/logo.png" class="table-bordered">

##Changing Title Colors##

Notice the top of the login area has a blue gradient. For the NASCAR layout we are going to change this to a black gradient.

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
body > div > div > div > div > div > div.panel-heading
{
  background-image: none;
  background: -webkit-linear-gradient(#0A0A0A, #484848, #FFFFFF); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(#0A0A0A, #484848, #FFFFFF); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(#0A0A0A, #484848, #FFFFFF); /* For Firefox 3.6 to 15 */
  background: linear-gradient(#0A0A0A, #484848, #FFFFFF); /* Standard syntax */
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/colors.png" class="table-bordered">

At this point we are going to call the login screen done, but lets see what happens when we actually login.

Enter the username/password you need to login to the system.

##Menu Screens##

<img src="images/uithemes-lesson1/mainmenu.png" class="table-bordered">

Notice that the top of this panel is still the old colors and that the logo is still the Prism logo. The panel header is back to the old colors because our CSS selector was too specific. It was 

```css
body > div > div > div > div > div > div.panel-heading
```

indicating a specific instance of a div with the class panel-heading. If we want to change all the div's that have the class panel heading we can remove some of the specifics we used earlier.

1.  Switch to your text editor that you have customer.css open in.
1.  Locate the line that selects the div.panel-heading
```css
body > div > div > div > div > div > div.panel-heading
```
1.  Remove the beginning of the line until it reads      
```css 
    div.panel-heading
```    
1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/mainmenucolored.png" class="table-bordered">

For the image we cannot do the same trick or we would be replacing a number of images that we do not want to. Instead we can turn our one image we want to change into a list.

1.  Switch to your text editor that you have customer.css open in.
1.  Locate the line that selects the img
```css
    body > div > div > div > div > div > div.panel-body > div.col-md-5 > img
```    
1.  Add a comma, and then add the following 
```css
    body > div > div > div > div.panel-body > div > div.col-xs-2 > img
```    
1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/mainmenulogo.png" class="table-bordered">

Now lets take a look at the Point of Sale module and see what else we need to change there. Click the "Point of Sale" button to enter that module.

<img src="images/uithemes-lesson1/pos.png" class="table-bordered">

Notice the header is already themed with the correct colors. 

##Hiding Elements##

For this installation we do not need the "Check In" and "Check Out" buttons, so lets take a look at hiding them.

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(3) > div:nth-child(1)
{
    visibility: hidden;
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/poscheckin.png" class="table-bordered">

Notice the Check In button is gone. Now we could add the CSS selector for the Check Out button similar to the way we added the second image to the list to replace the logo, but notice that both buttons are in the same row. So by not just targeting a specific button, but by targeting the row we can hide both buttons with one selector.

1.  Switch to your text editor that you have customer.css open in.
1.  Locate the line that selects the button
```css    
    body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(3) > div:nth-child(1)
```    
1.  Remove the last element selector so the selector references the row, and not the buton.
```css
    body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(3)
```    
1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/poscheckout.png" class="table-bordered">

Notice that the buttons are gone, but that the space for them remains. This is due to setting the visibility property in CSS, but by changing our selector for the entire row, and using the display property we can instead remove the entire row, buttons and the space they would have taken up.

1.  Switch to your text editor that you have customer.css open in.
1.  Locate the line that selects the buttons
```css
    body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(3)
```
1.  Change the line to select the row
```css
    body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div.row.row-margin
```
1.  Locate the line that sets the visibility of the button 
```css
    visibility: hidden;
```
1.  Change the line to remove the entire block
```css
    display: none;
```
1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/posblock.png" class="table-bordered">

Lets click on the new transaction button and go into the documents module. 

<img src="images/uithemes-lesson1/trans.png" class="table-bordered">

There are a couple of things we can change here to continue our branding. First lets change the Prism logo at the top. Because of the space, we will need to use a different graphic and CSS selector.

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
#transactionForm > div.container-fluid > div:nth-child(1) > div:nth-child(1) > img
{
 background: url('/images/custom/logo.png') no-repeat;
 display: block;
 -moz-box-sizing: border-box;
 box-sizing: border-box;
 width: 332px; /* Width of new image */
 height: 63px; /* Height of new image */
 padding-left: 332px; /* Equal to width of new image */
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/translogo.png" class="table-bordered">

##Button Colors##

Now lets look at changing the blue buttons to be lighter. Similar to the blue color in the NASCAR logo.

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
.btn-info
{
  color: #000;
  text-shadow:0 -1px 0 rgba(0,0,0,0.25);
  border-color: transparent !important;
  background-image: none;
  background: -webkit-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Firefox 3.6 to 15 */
  background: linear-gradient(#085687, #0B7AC0, #0B7AC0); /* Standard syntax */
}

.btn-info:active,
.btn-info.active,
.open .dropdown-toggle.btn-info
{
  background-image: none;
}

.btn-info:hover,
.btn-info:focus,
.btn-info:active,
.btn-info.active,
.open .dropdown-toggle.btn-info
{
  color: #000;
  text-shadow:0 -1px 0 rgba(0,0,0,0.25);
  border-color: transparent !important;
  background-image: none;
  background: -webkit-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Firefox 3.6 to 15 */
  background: linear-gradient(#085687, #0B7AC0, #0B7AC0); /* Standard syntax */
}

.btn-info.disabled,
.btn-info[disabled],
fieldset[disabled] .btn-info,
.btn-info.disabled:hover,
.btn-info[disabled]:hover,
fieldset[disabled] .btn-info:hover,
.btn-info.disabled:focus,
.btn-info[disabled]:focus,
fieldset[disabled] .btn-info:focus,
.btn-info.disabled:active,
.btn-info[disabled]:active,
fieldset[disabled] .btn-info:active,
.btn-info.disabled.active,
.btn-info[disabled].active,
fieldset[disabled] .btn-info.active
{
  color: #000
  text-shadow:0 -1px 0 rgba(0,0,0,0.25);
  border-color: transparent !important;
  background-image: none;
  background: -webkit-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(#085687, #0B7AC0, #0B7AC0); /* For Firefox 3.6 to 15 */
  background: linear-gradient(#085687, #0B7AC0, #0B7AC0); /* Standard syntax */
  cursor: not-allowed;
  pointer-events: none;
  -webkit-box-shadow: none;
  box-shadow: none;
  filter: alpha(opacity=65); /* internet explorer */
  -khtml-opacity: 0.65; /* khtml, old safari */
  -moz-opacity: 0.65; /* mozilla, netscape */
  opacity: 0.65; /* fx, safari, opera */
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/transcolors.png" class="table-bordered">

##Spacing##

Lastly lets look at the spacing on the document screen. If your resolution supports it you might want to make better use of the screen space to see more items, and have more spacing between details in the items list. Lets take a look at that. 

First lets expand the items list. This requires that we take space away from the right side summary and give it to the items list.

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
#transactionForm > div.container-fluid > div.row.flex > div.col-xs-4.col-xs-offset-1
{
  margin-left: 0;
}

#transactionForm > div.container-fluid > div.row.flex > div.col-xs-7
{
  width: 66.666666666666666%;
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/transspacingwide.png" class="table-bordered">

Now lets expand the items list itself to show more items.

1. Switch to your text editor that you have customer.css open in.
1. Down arrow to the bottom of the file and hit enter to start a new line.
1. Add the following code

```css
#transactionItems
{
 height: 630px;
}
```

1. Save the file
1. switch back to your browser
1. Refresh the page (F5)

<img src="images/uithemes-lesson1/transspacingtall.png" class="table-bordered">


##Appendix A: Capturing a CSS Selector##

To identify the element on the page that will be changed in the lesson you will need to be able to use the developer tools in your browsers to capture a CSS selector. The quick tutorial will make sure you are comfortable with the steps. This is easiest done in Google Chrome but can be done in other browsers as well. In this example we will be capturing the CSS selector for the Prism logo on the left side of the login screen.

###Capturing a CSS Selector with Google Chrome###

Open notepad 
Open your browser
1.  Browse to the login page for your server
1.  Right click on the Prism logo on the left side of the login dialog 
1.  Click "Inspect Element"
    A window will open and a line of HTML will be select it should look something like the following...
    ```html
    <img src="/images/RetailProLogo160.png" alt="Retail Pro Prism" class="">
    ```
1.  Right click on the highlighted line and select "Copy CSS Path"
1.  Switch over to notepad and paste
    You should see a line similar to the line below.
    ```css
    body > div:nth-child(1) > div > div > div > div > div.panel-body > div.col-md-5 > img
    ```

###Capturing a CSS Selector with Mozilla Firefox###

Firefox allows you to explore the CSS paths, but does not provide a good functionality to copy a path. If you are using Firefox this tutorial assumes you are using the Firebug Extension to make capturing the CSS selector easier.

Open notepad 
Open your browser
1.  Browse to the login page for your server
1.  Right click on the Prism logo on the left side of the login dialog 
1.  Click "Inspect Element with Firebug"
    A panel will open from the bottom of the window and a line of HTML will be select it should look something like the following...
    ```html
    <img style="transition: transform 1s ease 0s;" src="/images/RetailProLogo160.png" alt="Retail Pro Prism"></img>
    ```
1.  Right click on the highlighted line and select "Copy CSS Path"
1.  Switch over to notepad and paste
    You should see a line similar to the line below.
    ```css
    html.no-js.ng-scope body div div.container-fluid.ng-scope div.row-fluid div.col-md-6.col-md-offset-3 div.panel.panel-default div.panel-body div.col-md-5 img
    ```

Notice the path from Chrome and the path from Firefox are different. They are both valid, but the path from Chrome is compacted. This reduces page weight and is one of the reasons we recommend using Google Chrome to make these sort of changes. 






