#UI Themes#
- - - 

The Retail Pro Prism client is themed using CSS that is applied to the HTML views that make up the application. Changing those themes requires a knowledge of CSS and HTML and an understanding of how to use the debug views in a modern browser.

This section of the API will cover some of the most common questions related to customizing the look of the Retail Pro Prism client. This is part of the UI API that Retail Pro provides for the UI customizations.


#customer.css#

####Summary####

To ensure that changes you make to the Retail Pro Prism client remain even after a new version of the client comes out. We have created a customer.css file dedicated to changes that will override the default Retail Pro themes.

All changes you make to the themes of retail pro should be made in this customer.css file. You can locate this file on your Prism server in the default directory C:\ProgramData\RetailPro\Server\WebClient\styles\customer.css

<div class="alert alert-danger" role="alert">Warning! This is a live file. Changes you save to it take immediate effect on anyone else using the Prism client or Admin Console on this server.</div>

One last reminder. The browser keeps a cache of the last pages and css it fetched. You may need to refresh to force the browser to go back to the server instead of using the CSS file from the cache.


#Changing the background#

####Summary####

Changing the background of the Retail Pro Prism client is one of the simpler tasks you can do to customize the theme. You can change the background color, or replace the background with an image. The instructions below provide the basic information for you to do so.

####Background Color####

The background color is set in the style for the body tag. To change the background color add the following to your customer.css

You can specify any of the predefined web colors or you can use a hex value for precise control.

```css
body
  {
    background-color: #FF0000;
  }
```

####Background Image####

To use an image as a background you can add the following code. The path to the image is relative to the CSS file. The one shown here is included in the images directory.

```css
body
  {
    background: url(../images/PrismMap.png) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }
```

#Replacing Images#
- - - 

####Summary####

There are a number of images that you might want to replace in the Prism client. This can be done through CSS but takes a little more work to select the correct image. Make sure to test your changes in all of the browsers you will be using.

1. In your browser load the page that you want to change the image of.
1. Right click on the image you want to change and select inspect element.
1. Make sure the tag that you are taken to is an "img" tag.
1. Copy the full CSS path to the element. *
1. Using the full CSS path add the following block to your CSS

The code below replaces the Retail Pro bag on the login screen.

```css
body > div:nth-child(1) > div > div > div > div > div.panel-body > div.col-md-5 > img
  {
    background: url(../images/bag-image.png) no-repeat;
    display: block;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 187px; /* Width of new image */
    height: 184px; /* Height of new image */
    padding-left: 187px; /* Equal to width of new image */
  }
```
  
The code below replaces the button image on the New Transaction button in the Point of Sale main screen.

```css
body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(1) > div:nth-child(1) > button > table > tbody > tr > td:nth-child(1) > img
  {
    background: url(../images/RetailProLogo48.png) no-repeat;
    display: block;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 48px; /* Width of new image */
    height: 48px; /* Height of new image */
    padding-left: 48px; /* Equal to width of new image */
  }
```
  
*Pro-Tip*
+ In Chrome you can right click and select Copy CSS path.
+ In Firefox get the firebug extension to do the same thing.
+ In IE there is no shortcut for copying the path.

#Button Colors#
- - - 

####Summary####

Changing button colors is a more complicated theme adjustment. Mostly because the Bootstrap platform that Prism is created on provides different button types, and Retail Pro has introduced several new button types. Changing the color of one of those types effects all the buttons of that type. In addition there is a CSS entry for each state that you may need to provide an override for including default, hover, disabled, and active states.

The buttons in the main Point of Sale menu use btn-primary buttons, while the Tender Transaction button uses the btn-success class.

####Changing all of a certain type of button####

Once you have located the button type you need to change the CSS to indicate a new color for that button. See the example below to change all the primary buttons to use a red background.

The first thing you see is that this is that we are selecting a lot more CSS classes with this change. Also note that we set the active color to a brighter red than the default color and disabled colors.

```css
.btn-primary 
  {
    color:#ffffff;
    text-shadow:0 -1px 0 rgba(0,0,0,0.25);
    background-color: #CC0000 !important;
    border-color: transparent !important;
    background-image: none;
  }
.btn-primary:active,
.btn-primary.active,
.open .dropdown-toggle.btn-primary 
  {
    background-image: none;
  }
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.open .dropdown-toggle.btn-primary 
  {
    color:#ffffff;
    text-shadow:0 -1px 0 rgba(0,0,0,0.25);
    background-color: #ff0000 !important;
    border-color: transparent !important;
    background-image: none;
  }
.btn-primary.disabled,
.btn-primary[disabled],
fieldset[disabled] .btn-primary,
.btn-primary.disabled:hover,
.btn-primary[disabled]:hover,
fieldset[disabled] .btn-primary:hover,
.btn-primary.disabled:focus,
.btn-primary[disabled]:focus,
fieldset[disabled] .btn-primary:focus,
.btn-primary.disabled:active,
.btn-primary[disabled]:active,
fieldset[disabled] .btn-primary:active,
.btn-primary.disabled.active,
.btn-primary[disabled].active,
fieldset[disabled] .btn-primary.active 
  {
    color:#ffffff;
    text-shadow:0 -1px 0 rgba(0,0,0,0.25);
    background-color: #CC0000 !important;
    border-color: transparent !important;
    background-image: none;
    cursor: not-allowed;
    pointer-events: none;
    -webkit-box-shadow: none;
    box-shadow: none;
    filter: alpha(opacity=65); /* internet explorer */
    -khtml-opacity: 0.65;      /* khtml, old safari */
    -moz-opacity: 0.65;       /* mozilla, netscape */
    opacity: 0.65;           /* fx, safari, opera */
  }
```

####Changing a specific button####

Many buttons in the Prism client have specific IDs that make it easier to change the CSS for those buttons. Consider the following CSS changes that alter the color of the "Tender Transaction" button to be green.

When a button does not have a specific ID you can use the same method mentioned in the previous section to copy the CSS path to the specific button.

```css
#tenderbutton
  {
    color:#ffffff;
    text-shadow:0 -1px 0 rgba(0,0,0,0.25);
    background-color: #00CC00 !important;
    border-color: transparent !important;
    background-image: none;
  }
#tenderbutton:hover,
#tenderbutton:focus,
#tenderbutton:active
  {
    background-color: #00FF00 !important;
    background-image: none;
  }
#tenderbutton:disabled
  {
    color:#ffffff;
    text-shadow:0 -1px 0 rgba(0,0,0,0.25);
    background-color: #00CC00 !important;
    border-color: transparent !important;
    background-image: none;
    cursor: not-allowed;
    pointer-events: none;
    -webkit-box-shadow: none;
    box-shadow: none;
    filter: alpha(opacity=65); /* internet explorer */
    -khtml-opacity: 0.65;      /* khtml, old safari */
    -moz-opacity: 0.65;       /* mozilla, netscape */
    opacity: 0.65;           /* fx, safari, opera */
  }
```

#Hiding Elements#
- - -

####Summary####

You may want to remove some element that you do not use. You can do this in two different ways.

1. visibility: hidden;
1. display: none;

visibility:hidden hides an element, but it will still take up the same space as before. The element will be hidden, but still affect the layout.

display:none hides an element, and it will not take up any space. The element will be hidden, and the page will be displayed as if the element is not there.

Consider the array of buttons on the main POS screen. If we apply the visibility: hidden; using the following CSS. The button will disappear but there will be a gap in the button grid.

```css
body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(2) > div:nth-child(2)
  {
    visibility: hidden;
  }
```
  
Where using display: none; will remove the button from the layout collapsing the space and re-flowing the buttons.

```css
body > div:nth-child(1) > div.container-fluid.ng-scope > div:nth-child(2) > div > div.panel-body > div:nth-child(2) > div:nth-child(2)
  {
    display: none;
  }
```  