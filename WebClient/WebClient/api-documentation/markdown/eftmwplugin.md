#EFTMW Cayan Plugin#
- - -
The EFTMW Cayan Plugin functions as the default EFT provider for Prism and serves as an example to use when creating other EFT plugins. This plugin makes use of both ButtonHooks and UI Configurations as well has having extensive angular coding including Services and Controllers. The EFTMW Cayan Plugin is enabled by default although will not function unless the Merchantware/Cayan settings are configured within Prism.

##Removing/Enabling the Plugin##
- - - 
You must edit two files that are located in the root of the Prism folder (this is normally located in C:\ProgramData\RetailPro\Server\WebClient)
Open the customizations.html file and Remove or Comment out the following lines:
<pre>&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-Config.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-Device-Controller.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-Gift-Controller.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-SigCap-Controller.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-Cancel-Controller.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-Void-Config.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="/plugins/sample/cayan/EFT-Cayan-Service.js"&gt;&lt;/script&gt;</pre>

Open the customizations.js file and Remove or Comment out the following lines:
<pre>dependencies.push('prismPluginsSample.module.cayanRouteModule');
dependencies.push('prismPluginsSample.service.eftCayanService');
dependencies.push('prismPluginsSample.controller.cayanDeviceController');
dependencies.push('prismPluginsSample.controller.cayanGiftCardController');
dependencies.push('prismPluginsSample.controller.cayanSigCapController');
dependencies.push('prismPluginsSample.controller.cayanCancelController');</pre>

To enable the plugin, add the above lines to their respective files.
