#Server Module Customization API#
- - -

<div class="alert alert-warning" role="alert">Warning! This API is being deprecated for *Retail Pro Prism*. It will be replaced with a new service oriented API in a future release.</div>

####Summary####
The Retail Pro Prism server is made up of service modules that each perform specific tasks. RPS for example is the core Retail Pro Service while DRS is the Prism Data Replication Service. Each module can provide resources, have custom database connections, and perform any other tasks needed server side.

####Structure####
Each of the service modules is a 32bit Windows DLL with a specific interface. You can create this DLL Using any programming language that is capable of creating a 32bit Windows DLL.

####Exports requirements####
You may omit one of the dispatch methods, either DispatchREST or DispatchRPC, based on your needs. Other than that all of the methods listed below are required to be exported in your custom service module or the module will fail to load.


####DLL Location####
The DLL does not need any special registration to function, but must be located in the Prism Server Modules folder. Typically this is C:\ProgramData\RetailPro\Server\Modules\

####Interface####
DLL's export methods that other applications call. The following methods must be exported from your DLL for it to function correctly within the Prism framework.

<div class="alert alert-info" role="alert">Note: All code signatures shown here are in Delphi/Pascal.</div>


#Discover#
- - - 

####Summary####

Called once to register/discover the module at load time. This is the first method called in the DLL.

####Signature####

```delphi
Procedure DISCOVER(
  var ADescription : PChar;
  var AVersion : PChar;
  var ARESTful : WordBool;
  var AURIVersion : Integer;
  var AServices : PPCharArray;
  var AMethods : PPCharArray
); stdcall;
```

####Parameter Details####


|Parameter                 |Purpose                                                                                                                                                                                    |  
|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ADescription : PChar`   | A text description of the service. Used for debugging and display.                                                                                                                        |
| `AVersion : PChar`       | A text version for the service. Used for display and debugging.                                                                                                                           | 
| `ARESTful : WordBool`    | A flag indicating whether the service supports a REST interface. TRUE to indicate the service does provides a REST interface FALSE if the service does not.                               | 
| `AURIVersion : Integer`  | An integer value that is used to designate the version of the URI pattern this module expects.                                                                                            | 
| `AServices : PPCharArray`| A text version for the service. Used for display and debugging.                                                                                                                           | 
| `AVersion : PChar`       | A text array of resources the service provides.                                                                                                                                           | 
| `AMethods : PPCharArray` | A text array of methods the service provides.                                                                                                                                             | 

#Prepare#
- - -

####Summary####

Called once to initialize the module. Called after discover.

####Signature####

```delphi
function PREPARE(
  AExePath : PChar
) : Integer; stdcall;
```

####Parameter Details####

|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AExePath : PChar`              | The path the Retail Pro folder in based on CSIDL_COMMON_APPDATA. Typically this resolves to C:\ProgramData\RetailPro\.                                      |


####Returns####

* `0` to indicate the module prepared correctly
* `-1` to indicate the module failed to prepare correctly.


#Configure#
- - -

####Summary####

Called to modify the configuration of the module.

####Signature####

```delphi
function CONFIGURE(
  AMethod: PChar;
  Accepts: PChar;
  var AConfig: PChar
): Integer; stdcall;
```

####Parameter Details####


|Parameter                        |Purpose                                                                                                                                                      |  
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AMethod : PChar`               | GET indicating the expectation to load and return the configuration data in AConfig or PUT indicating AConfig contains configuration data to save           |
| `Accepts : PChar`               | text/xml or text/json indicating the payload format                                                                                                         |
| `var AConfig : PChar`           | The configuration data in XML or JSON format.                                                                                                               |

####Returns####

The call to configure returns an HTTP status code. 200 for success, 404 not found, etc... 

#Describe#
- - -
####Summary####

Called to get a text XML or JSON payload describing the module and the services and methods it provides.

####Signature####

```delphi
function DESCRIBE(
  AWillAccept: PChar
) : PChar; stdcall;
```

####Parameter Details####

| Parameter             | Purpose                                   |
|-----------------------|-------------------------------------------|
|`AWillAccept : PChar` | Indicates acceptable content formats.      | 

####Returns####

XML or JSON payload describing the module and the services provided.

This is a segment of the describe response from the v9 Publisher module shown in XML.

```xml
<Service>
  <MODULE name="Retail Pro v9 publisher" description="Retail Pro v9 Pub Services" version="10.0.0.1" uriversion="1">
    <METHODS/>
    <RESOURCES>     
      <RESOURCE name="Configuration">
        <SUBRESOURCES>
          <SUBRESOURCE name="ConfigParameters"/>
        </SUBRESOURCES>
      </RESOURCE>
      <RESOURCE name="ErrorLog"/>
      <RESOURCE name="pubsubresource">
        <SUBRESOURCES>
          <SUBRESOURCE name="SubPubResourceLink_Mapping"/>
        </SUBRESOURCES>
      </RESOURCE>
      { Content Omitted ... }
      <RESOURCE name="usergroup_Mapping">
        <SUBRESOURCES>
          <SUBRESOURCE name="groupuser_Mapping"/>
        </SUBRESOURCES>
      </RESOURCE>
    </RESOURCES>
  </MODULE>
</Service>
```

#DispatchREST#
- - -

####Summary####

Called when a REST request needs to be processed.

####Signature####

```delphi
function DISPATCHREST(
  AHTTPVerb: PChar;
  AServiceName: PChar;
  AQualifiers: PChar;
  AParams: PPCharArray;
  ARequestHeaders: PPCharArray;
  ARequestCookies: PPCharArray;
  ARequestContentType: PChar;
  ARequestContentLength: Integer;
  ARequestContent: PChar;
  AWillAccept: PChar;
  AResponseContentHandle: THandle;
  AResponseHeaders: PChar;
  AResponseCookies: PChar
): Integer; stdcall;
```

####Parameter Details####


|Parameter                          |Purpose                                                                                                                                                      |  
|-----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AHTTPVerb : PChar`               | The HTTP verb for the call. e.g. GET, PUT, POST, DELETE, HEAD, TRACE                                                                                        |
| `AServiceName : PChar`            | The primary resource for the call.                                                                                                                          |
| `AQualifiers : PChar`             | The sub-resources for the call.                                                                                                                             |
| `AParams : PPCharArray`           | The HTTP parameters for the call in a string array. Basically everything after the "?" in the URL.                                                          |
| `ARequestHeaders : PPCharArray`   | The request headers for the call as name=value pairs in a string array.                                                                                     |
| `ARequestCookies : PPCharArray`   | The cookies for the call as name=value pairs in a string array. (Not in use, always NIL)                                                                    |
| `ARequestContentType : PChar`     | application/xml or application/json indicating the content type for PUT and POST calls. (Can be NIL)                                                        |
| `ARequestContentLength : Integer` | The size in bytes of the request content.                                                                                                                   |
| `ARequestContent : PChar`         | The "body" of the request for POST and PUT operations Nil for all others.                                                                                   |
| `AWillAccept : PChar`             | text/xml or text/json indicating the acceptable content formats for the response.                                                                           |
| `AResponseContentHandle : THandle`| THandle to the response content. (Can be empty)                                                                                                             |
| `AResponseHeaders : PChar`        | The response headers for the call in CR delimited name=value pairs op to 1024 bytes.                                                                        |
| `AResponseCookies : PChar`        | The response cookies for the call in CR delimited name=value pairs op to 1024 bytes. (Not in use, always NIL)                                               |

####Returns####

HTTP response code.


#DispatchRPC#

####Summary####

Called when an RPC request needs to be processed

####Signature####

```delphi
function DISPATCHRPC(
  ARequestHeaders        : PPCharArray;
  ARequestCookies        : PPCharArray;
  ARequestContentType    : PChar;
  ARequestContentLength  : Integer;
  ARequestContent        : PChar;
  AWillAccept            : PChar;
  AResponseContentHandle : THandle;
  AResponseHeaders       : PChar;
  AResponseCookies       : PChar
): Integer; stdcall;
```

####Parameter Details####

|Parameter                          |Purpose                                                                                                                                                      |  
|-----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
|`ARequestHeaders : PPCharArray`    |  The request headers for the call as name=value pairs in a string array.                                                                                    |
|`ARequestCookies : PPCharArray`    |  The cookies for the call as name=value pairs in a string array. (Not in use, always NIL)                                                                   |
|`ARequestContentType :  PChar`     |  application/xml or application/json indicating the content type for POST calls. (Can be NIL)                                                               |
|`ARequestContentLength : Integer`  |  The size in bytes of the request content.                                                                                                                  |
|`ARequestContent : PChar`          |  The "body" of the request for POST.                                                                                                                        |
|`AWillAccept : PChar`              |  text/xml or text/json indicating the acceptable content formats for the response.                                                                          |
|`AResponseContentHandle : THandle` |  THandle to the response content. (Can be empty)                                                                                                            |
|`AResponseHeaders : PChar`         |  The response headers for the call in CR delimited name=value pairs op to 1024 bytes.                                                                       |
|`AResponseCookies : PChar`         |  The response cookies for the call in CR delimited name=value pairs op to 1024 bytes. (Not in use, always NIL)                                              |

####Returns###

HTTP response code.


#Cleanup#
- - -

####Summary####

Called once to finalize the module. This method can be used for garbage collection and general shutdown.

####Signature####

```delphi
function CLEANUP : Integer; stdcall;
```

####Returns####

* `-1` to indicate the module cleanup correctly 
* `0` to indicate the module failed to cleanup correctly.


