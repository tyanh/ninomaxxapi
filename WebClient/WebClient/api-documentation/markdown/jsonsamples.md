#JSON Payload Samples#
- - - 

####GET Customer####

The following exchange gets all the customers with a first name starting with P

####URL####

https://{SERVERNAME}/v1/rest/customer?cols=last_name,first_name&filter=first_name,lk,p*

####Response####

```json
[
  {
    "link" : "\/v1\/rest\/customer\/-7266891299261313028",
    "last_name" : "Customer",
    "first_name" : "PriceLevel"
  },
  {
    "link" : "\/v1\/rest\/customer\/-5511489783963643908",
    "last_name" : "Customer II",
    "first_name" : "PriceLevel"
  },
  {
    "link" : "\/v1\/rest\/customer\/362143298000103171",
    "last_name" : "Clerk I",
    "first_name" : "Pos"
  },
  {
    "link" : "\/v1\/rest\/customer\/362143298000157182",
    "last_name" : "Buyer",
    "first_name" : "Product"
  },
  {
    "link" : "\/v1\/rest\/customer\/362143301000128231",
    "last_name" : "Clerk II",
    "first_name" : "Pos"
  }
]
```

####POST Customer####

The following example creates a new customer.

####URL####

https://{SERVERNAME}/v1/rest/customer

####Request####

```json
[            
  {
    "origin_application": "RProPrismWeb",
    "first_name": "Jack",
    "last_name": "Harkness"
  }
]  
```

####Response####

```json
[
  {
    "link" : "\/v1\/rest\/customer\/362436872000153006",
    "sid" : "362436872000153006",
    "created_by" : "sysadmin",
    "created_datetime" : "2014-12-04T10:14:32.000-08:00",
    "modified_by" : null,
    "modified_datetime" : null,
    "controller_sid" : "362142344000018255",
    "origin_application" : "RProPrismWeb",
    "post_date" : "2014-12-04T10:14:32.000-08:00",
    "row_version" : 1,
    "tenant_sid" : "362142344000195000",
    "subsidiary_sid" : "362142345000106001",
    "region_sid" : null,
    "district_sid" : null,
    "store_sid" : "362142346000113009",
    "company_sid" : null,
    "customer_id" : 100000058,
    "title" : null,
    "last_name" : "Harkness",
    "first_name" : "Jack",
    "suffix" : null,
    "customer_active" : true,
    "marketing_flag" : null,
    "birth_day" : null,
    "birth_month" : null,
    "birth_year" : null,
    "customer_type" : 0,
    "customer_class_sid" : null,
    "title_sid" : null,
    "suffix_sid" : null,
    "gender" : null,
    "tax_area_sid" : null,
    "tax_area2_sid" : null,
    "credit_limit" : null,
    "credit_used" : null,
    "store_credit" : null,
    "accept_checks" : true,
    "check_limit" : null,
    "detax" : false,
    "suggested_discount_percent" : null,
    "household_code" : null,
    "mark1" : null,
    "mark2" : null,
    "max_discount_percent" : 100,
    "ar_flag" : null,
    "related_customer_sid" : null,
    "shipping_priority" : null,
    "primary_clerk_sid" : null,
    "first_sale_date" : null,
    "last_sale_date" : null,
    "last_sale_amount" : null,
    "webstoreconnect_username" : null,
    "webstoreconnect_password" : null,
    "accounting_system_id" : null,
    "udffield01" : null,
    "udffield02" : null,
    "udffield03" : null,
    "udffield04" : null,
    "udffield05" : null,
    "udfclob01" : "",
    "udfclob02" : "",
    "notes" : "",
    "image" : null,
    "subsidiary_number" : 1,
    "addresses" : [
    ],
    "phones" : [
    ],
    "subsidiary_name" : "001",
    "emails" : [
    ],
    "share_type" : 0,
    "security_level" : null,
    "price_level_sid" : null,
    "info1" : null,
    "info2" : null,
    "employee_as_customer" : false,
    "customer_class_name" : null,
    "district_name" : null,
    "price_level_name" : null,
    "region_name" : null,
    "tax_area_name" : null,
    "tax_area2_name" : null,
    "primary_clerk_last_name" : null,
    "primary_clerk_first_name" : null,
    "last_order_date" : null,
    "order_item_count" : null,
    "station" : null,
    "ytd_sale" : null,
    "last_return_date" : null,
    "total_transactions" : null,
    "sale_item_count" : null,
    "return_item_count" : null,
    "store_name" : "Sub1 Str1 Rack #001",
    "store_code" : "1s1",
    "customer_title" : null,
    "primary_phone_no" : null,
    "country_sid" : null,
    "email_address" : null,
    "store_number" : "1",
    "allowed_tenders" : "TTTTTTTTTTTTTTT",
    "country" : null,
    "primary_address_line_1" : null,
    "primary_address_line_2" : null,
    "primary_address_line_3" : null,
    "primary_address_line_4" : null,
    "primary_address_line_5" : null,
    "primary_address_line_6" : null,
    "primary_city" : null,
    "primary_address_type" : null,
    "primary_phone_type" : null,
    "primary_email_type" : null,
    "postal_code" : null,
    "company" : null,
    "promo_custlistname" : null,
    "hrefs" : {
      "store_sid" : "\/v1\/rest\/store\/362142346000113009",
      "subsidiary_sid" : "\/v1\/rest\/subsidiary\/362142345000106001",
      "controller_sid" : "\/v1\/rest\/controller\/362142344000018255"
    }
  }
]
```

####PUT Customer####

The following sample changes the last name of a customer

####URL####

https://{SERVERNAME}/v1/rest/customer/362436872000153006?filter=row_version,eq,1

####Request####

```json
[            
  {
    "last_name": "AlteredLastName" 
  }
]  
```

####Response####

```json
[
  {
    "link" : "\/v1\/rest\/customer\/362436872000153006",
    "row_version" : 2,
    "last_name" : "AlteredLastName"
  }
]
```     