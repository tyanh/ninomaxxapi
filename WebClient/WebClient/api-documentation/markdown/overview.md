
#Summary#
- - -
There are a number of ways to extend and customize the Retail Pro Prism product. This document attempts to provide more information as to the capabilities of each opportunity.

####Prism Architecture####
Retail Pro Prism uses an N-tier architecture that provides several points where APIs are exposed that will allow customization of the Prism product. Each numbered point in the figure below corresponds to an exposed API that can be used to customize Retail Pro Prism.

<img src="images/architectureoverview.png" class="table-bordered">

####Customization access points####
Below you can find a more detailed description of each of the solutions numbered above as well as examples of when you might use that solution. While each of these solutions provides a lot of flexibility for developers they may not be the right fit for all situations and some of the advantages and potential drawbacks of using each solution are listed below as well. Finally, each solution listed below may depend on different technical knowledge to implement and those requirements are also detailed below.

#Client Customization API#
- - -

The Prism Client is a web application that provides the UI for accessing the Retail Pro services like RPS and DRS. The client lives on the Prism Server and is accessed using a web browser. Developers can add to the existing UI, introducing side buttons, or custom forms that capture additional data. Developers can modify the layout of existing UI screens, moving controls around on the screen to fit the look and feel they need. Developers can also use the UI API to get notifications of UI events such as a button click or the change of an attribute.

####Examples####

* Change the theme to support corporate branding.
* Add custom functionality through side buttons.
* Get notifications of UI events like saving a customer or updating a document.

####Advantages####

* UI customization provides access to the direct interaction between the users and Retail Pro Prism.
* UI customizations allow developers to tailor the Retail Pro Prism UI to their specific needs.
* UI customizations allow developers to extend the functionality of the already successful Retail Pro Prism UI.

####Disadvantages####

* Customizations cannot work directly with external web services.
* Customizations are deployed in clear text there is no protection for any IP in UI customizations.

####Technology Requirements####

* Knowledge of developing HTML5 and CSS3 applications using bootstrap.
* Knowledge of developing using JavaScript.
* Knowledge of developing web applications in an AngularJS framework.


#REST API#
- - -

The Retail Pro Prism Web Application communicates to the Retail Pro server using a REST API. This REST API provides access to all the data and business logic that makes up Retail Pro Prism. Developers can create custom applications that leverage the REST API provided by Retail Pro Prism.

####Examples####

* Permit real time sampling if inventory levels for vendors.
* Create an application for a different market such as rental.
* Create an application that uses different workflows.

####Advantages####

* Complete control of all aspects of the UI and workflow.
* Develop native applications for whatever platform you need.

####Disadvantages####

* Changes to the Retail Pro REST API can be breaking to the custom applications.

####Technology Requirements####

* Knowledge of developing applications that consume REST services.
* Knowledge of reading and writing XML and JSON REST payloads.


#Communications API (Proxy)#
- - -

Retail Pro Prism clients connect through the Prism Launcher and/or Prism Proxy. The Prism Proxy performs a number of tasks for the clients on the local workstation. All local hardware is controlled through the Prism Proxy for example. Developers can create customizations for the Prism Proxy. If these customizations control hardware they can be put in the *lane* otherwise all of the Prism Proxy installs can share a central customization.

####Examples####

* Communicating with a fiscal printer for the workstation.
* Redirecting requests for customers to a CRM solution.
* Imposing additional business rules for an installation.

####Advantages####

* Can intercept data before it gets to the server.
* Can communicate with local hardware.

####Disadvantages####

* While an in lane customization of this type can provide UI and/or user interaction, it will not be embedded in the Prism Client.

####Technology Requirements####

* Any programming language that supports 0MQ communications.
* For centralized customizations, knowledge of building multithreaded applications.

