(function(ng) {
    var dependencies = [];

    /*DO NOT MODIFY ABOVE THIS LINE!*/

    //Usage example:
    //dependencies.push('dependencyName');

    /*DO NOT MODIFY BELOW THIS LINE!*/
    ng.module('prismApp.customizations', dependencies, null);
})(angular);
