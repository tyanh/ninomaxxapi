window.prismApp = angular.module(
  'prismApp',
  [
    'prismApp.common.controllers',
    'prismApp.common.services',
    'prismApp.common.directives',
    'prismLogin',
    'ui.bootstrap.tabs',
    'ui.bootstrap.accordion',
    'ui.router',
    'ngGrid',
    'prismApp.factories.Logger'
  ],
  null
);

//prismApp.pos.controllers module definition and its dependencies
window.angular.module(
  'prismApp.common.controllers',
  [
    'prismApp.controllers.testController',
    'prismApp.controllers.ac_services_ctrl'
  ],
  null
);

//prismApp.common.services module definition and its dependencies
window.angular.module(
  'prismApp.common.services',
  [
    'prismApp.common.services.prismSessionInfo',
    'prismApp.common.services.PrismUtilities',
    'prismApp.common.services.loginProvider',
    'prismApp.common.services.EFT',
    'prismApp.services.RPServices',
    'prismApp.common.services.LoggingService'
  ],
  null
);
window.angular.module(
  'prismApp.common.directives',
  ['prismApp.common.directives.treeMenu'],
  null
);

window.prismApp.config([
  '$stateProvider',
  '$httpProvider',
  function($stateProvider, $httpProvider) {
    'use strict';

    $httpProvider.defaults.headers.post = {
      'Content-Type': 'application/json; charset=UTF-8'
    };
    $httpProvider.defaults.headers.put = {
      'Content-Type': 'application/json; charset=UTF-8'
    };
    $httpProvider.defaults.headers.delete = {
      'Content-Type': 'application/json; charset=UTF-8'
    };

    $stateProvider
      .state('root', {
        url: '',
        templateUrl: 'views/apitests-partial.htm'
      })
      .state('services', {
        url: '/services',
        templateUrl: 'views/ac-services-partial.htm',
        controller: 'ac_services_ctrl'
      })
      .state('service', {
        url: '/services?resource',
        templateUrl: 'views/ac-services-partial.htm',
        controller: 'ac_services_ctrl'
      });
  }
]);
