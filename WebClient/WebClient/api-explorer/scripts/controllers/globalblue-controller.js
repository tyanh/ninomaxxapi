window.angular
  .module('prismApp.common.controllers.globalblue_ctrl', [])
  .controller('globalblue_ctrl', [
    '$scope',
    'prismSessionInfo',
    function($scope, prismSessionInfo) {
      'use strict';
      $scope.setUrl = function() {
        $scope.gb.success = (!$scope.gb.successtothispage)? location.pathname.substring(1): $scope.gb.success;
        $scope.gb.error = (!$scope.gb.errorstothispage)? location.pathname.substring(1)
          : $scope.gb.error;
      };

      $scope.doGBTransaction = function() {
        localStorage.setItem('gbtabs', 'true');
        var callback = 'mpa://x-callback-url/';
        callback += $scope.gb.action + '?amount=' + $scope.gb.amount;
        callback += '&confirmamount=' + $scope.gb.confirmamount;
        if ($scope.gb.reference.length > 0) {
          callback += '&reference=' + $scope.gb.reference;
        }
        if ($scope.gb.receiptno.length > 0) {
          callback += '&receiptno=' + $scope.gb.receiptno;
        }
        callback += '&x-source=launcher';
        callback +=
          '&x-success=launcher://x-callback-url/success?' + $scope.gb.success;
        callback +=
          '&x-error=launcher://x-callback-url/error?' + $scope.gb.error;
        location.href = callback;
      };

      $scope.changeButtonText = function() {
        $scope.showRawResponse = !$scope.showRawResponse;
        var preface = $scope.showRawResponse ? 'Hide' : 'Show';
        $scope.rawResponseButtonText = preface + ' Raw Response';
      };

      $scope.checkForGBResponse = function() {
        if (
          location.search.length > 0 &&
          localStorage.getItem('gbtabs') !== null
        ) {
          //reacting to GB return
          $scope.tabs.gb.active = true;
          $scope.tabs.gb.initiate.active = false;
          $scope.tabs.gb.response.active = true;
          $scope.gbResponse = qsParser(location.search.substring(1));
          $scope.rawGBResponse = location.search;
          if ($scope.gbResponse.rendered !== undefined) {
            $scope.receipt = atob($scope.gbResponse.rendered);
          }
          localStorage.removeItem('gbtabs');
        }
      };
    }
  ]);
