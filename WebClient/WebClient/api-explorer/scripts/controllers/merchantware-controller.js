window.angular.module('prismApp.common.controllers.merchantware_ctrl', [])
    .controller('merchantware_ctrl',
        [
            '$scope','LoggingService','$timeout','$q','$http','prismSessionInfo','PrismUtilities','Logger','EFT','$stateParams','$location',
            function($scope,LoggingService,$timeout,$q,$http,prismSessionInfo,PrismUtilities,Logger,EFT,$stateParams,$location){
                'use strict';
                $scope.mw = {
                    loaded:false,
                    configured:false,
                    defaultConfig:false,
                    error:false,
                    rpData:{
                        eft_mw_credit_endpoint:'https://ps1.merchantware.net/Merchantware/ws/RetailTransaction/v4/Credit.asmx',
                        eft_mw_credit_schema:'https://transport.merchantware.net/v4/transportService.asmx',
                        eft_mw_gift_endpoint:'https://ps1.merchantware.net/Merchantware/ws/ExtensionServices/v4/GiftcardMagensa10.asmx',
                        eft_mw_gift_schema:'http://schemas.merchantwarehouse.com/merchantware/40/GiftcardMagensa10',
                        eft_mw_logo_location:'',
                        eft_mw_merchant_key:'BQFVM-2KUEA-BHPJH-EJGYM-KT1A4',
                        eft_mw_merchant_name:'RetailPro Genius',
                        eft_mw_merchant_site_id:'86GK4K2A',
                        eft_mw_redirect_location:'',
                        eft_mw_software_name:'',
                        eft_mw_software_version:'',
                        eft_mw_web_transaction_server:'https://transport.merchantware.net/v4/transportweb.aspx?transportKey='
                    },
                    mwData:{
                        eft_mw_credit_endpoint:'',
                        eft_mw_credit_schema:'',
                        eft_mw_gift_endpoint:'',
                        eft_mw_gift_schema:'',
                        eft_mw_logo_location:'',
                        eft_mw_merchant_key:'',
                        eft_mw_merchant_name:'',
                        eft_mw_merchant_site_id:'',
                        eft_mw_redirect_location:'',
                        eft_mw_software_name:'',
                        eft_mw_software_version:'',
                        eft_mw_web_transaction_server:''
                    }
                };
                $scope.isActiveTab=function(tab){
                    console.log(tab +"|"+ $scope.activeTab)
                    return (tab===$scope.activeTab)?'active':'';
                }
                $scope.setTabs=function(tab){
                    $scope.activeTab=tab;
                    $scope.tabs.mw = {
                        config:false,
                        ced:false,
                        web:false,
                        gift:false
                    };
                    $scope.tabs.mw[tab]=true;

                }
                $scope.setTabs($stateParams.tab);
                $scope.form = {
                    giftcard:{
                        carddata:'',
                        amount:'',
                        completed:false
                    },
                    ced:{
                        amount:'',
                        type:'CREDIT_TAKE',
                        initiated:false,
                        completed:false
                    },
                    web:{
                        amount:'',
                        type:'CREDIT_TAKE',
                        initiated:false,
                        dupes:false,
                        sid:0,
                        mode:'',
                        completed:false
                    }
                };
                $scope.resetTabs = function(makeActive){
                    var defObj = $q.defer();
                    if($scope.tabs.mw.active){
                        var mwConf = $scope.checkMWConfig();
                        mwConf.then(function(){
                            defObj.resolve();
                        }, function(){
                            defObj.resolve();
                        });
                    } else {
                        defObj.resolve();
                    }

                    return defObj.promise;
                };
                $scope.dataLog = LoggingService.LogData;
                $scope.checkForMWResponse = function(){
                    if(location.search.length > 0 && localStorage.getItem('mwtabs') !== null){
                        //reacting to MW return
                        var responseString = qsParser(location.search.substring(1));
                        $scope.form.web.sid = responseString.sid;
                        $scope.form.web.mode = responseString.mode;

                        $q.when($scope.resetTabs('mw'))
                            .then(function(){
                                $scope.doMWTransaction('mwwebrecover');
                            });
                    }
                };
                $scope.checkMWConfig = function(){
                    var defObj = $q.defer();

                    $scope.mw.configured = false;
                    var mwPrefs = $http({
                        method:'GET',
                        url:'/v1/rest/preference/workstation/' + prismSessionInfo.get().workstationid._value_  + '?cols=eft_mw_credit_endpoint,eft_mw_credit_schema,eft_mw_gift_endpoint,eft_mw_gift_schema,eft_mw_logo_location,eft_mw_merchant_key,eft_mw_merchant_name,eft_mw_merchant_site_id,eft_mw_web_transaction_server,eft_mw_redirect_location,eft_mw_software_name,eft_mw_software_version,row_version'
                    });
                    mwPrefs.then(function(data){
                        $scope.mw.loaded = true;
                        var mwData = PrismUtilities.responseParser(data);
                        if(mwData.workstations[0].workstation.eft_mw_merchant_name._value_.length > 0){
                            $scope.mw.configured = true;
                            $scope.mw.defaultConfig = true;
                            Object.keys($scope.mw.mwData).forEach(function(key){
                                $scope.mw.mwData[key] = mwData.workstations[0].workstation[key]._value_;
                            });
                            if(localStorage.getItem('mwtabs') !== null){
                                localStorage.removeItem('mwtabs');
                            }
                        }

                        defObj.resolve();
                    }, function(){
                        $scope.mw.error = true;
                        defObj.resolve();
                    });

                    return defObj.promise;
                };
                $scope.checkMWConfig();

                $scope.validateManualMWConfig = function(){
                    var credSch = $scope.mw.mwData.eft_mw_credit_schema.length > 0,
                        credEnd = $scope.mw.mwData.eft_mw_credit_endpoint.length > 0,
                        giftSch = $scope.mw.mwData.eft_mw_gift_schema.length > 0,
                        giftEnd = $scope.mw.mwData.eft_mw_gift_endpoint.length > 0,
                        merchName = $scope.mw.mwData.eft_mw_merchant_name.length > 0,
                        merchSId = $scope.mw.mwData.eft_mw_merchant_site_id.length > 0,
                        merchKey = $scope.mw.mwData.eft_mw_merchant_key.length > 0,
                        webTrS = $scope.mw.mwData.eft_mw_web_transaction_server.length > 0;
                    //credit schema and credit endpoint or gift schema and gift endpoint
                    //merchant name
                    //merchant site id
                    //merchant key
                    //web transaction server
                    return ((credSch && credEnd) || (giftSch && giftEnd)) && merchName && merchSId && merchKey && webTrS;
                };

                $scope.validateCreditMWConfig = function(){
                    var credSch = $scope.mw.mwData.eft_mw_credit_schema.length > 0,
                        credEnd = $scope.mw.mwData.eft_mw_credit_endpoint.length > 0,
                        merchName = $scope.mw.mwData.eft_mw_merchant_name.length > 0,
                        merchSId = $scope.mw.mwData.eft_mw_merchant_site_id.length > 0,
                        merchKey = $scope.mw.mwData.eft_mw_merchant_key.length > 0,
                        webTrS = $scope.mw.mwData.eft_mw_web_transaction_server.length > 0;
                    //credit schema and credit endpoint or gift schema and gift endpoint
                    //merchant name
                    //merchant site id
                    //merchant key
                    //web transaction server
                    return credSch && credEnd && merchName && merchSId && merchKey && webTrS;
                };

                $scope.validateGiftMWConfig = function(){
                    var giftSch = $scope.mw.mwData.eft_mw_gift_schema.length > 0,
                        giftEnd = $scope.mw.mwData.eft_mw_gift_endpoint.length > 0,
                        merchName = $scope.mw.mwData.eft_mw_merchant_name.length > 0,
                        merchSId = $scope.mw.mwData.eft_mw_merchant_site_id.length > 0,
                        merchKey = $scope.mw.mwData.eft_mw_merchant_key.length > 0,
                        webTrS = $scope.mw.mwData.eft_mw_web_transaction_server.length > 0;
                    //credit schema and credit endpoint or gift schema and gift endpoint
                    //merchant name
                    //merchant site id
                    //merchant key
                    //web transaction server
                    return giftSch && giftEnd && merchName && merchSId && merchKey && webTrS;
                };
                $scope.dataLog = LoggingService.LogData;

                $scope.$watch(function() { return LoggingService.LogData; }, function(){
                    $scope.dataLog = LoggingService.LogData;
                });
                $scope.fillEFT = function(){
                    $scope.mw.mwData = angular.copy($scope.mw.rpData);
                };
                $scope.saveMWConfig = function(revert){
                    var defObj = $q.defer();

                    if(!$scope.mw.defaultConfig){
                        var configLog = new Logger();
                        configLog.label = revert ? 'Removing MW Workstation Preferences' : 'Adding MW Workstation Preferences';

                        var updatedPrefs = {
                            workstations:[
                                {workstation:{}}
                            ]
                        };
                        Object.keys($scope.mw.mwData).forEach(function(key){
                            if(revert){
                                updatedPrefs.workstations[0].workstation[key] = {_value_:''};
                            } else {
                                updatedPrefs.workstations[0].workstation[key] = {_value_:$scope.mw.mwData[key]};
                            }
                        });
                        var prefsConfig = {
                            method:'PUT',
                            url:'/v1/rest/preference/workstation/' + prismSessionInfo.get().workstationid._value_ ,
                            data:updatedPrefs
                        };

                        configLog.data.url = prefsConfig.url;
                        configLog.data.method = prefsConfig.method;
                        configLog.data.sent = updatedPrefs;

                        var mwPrefs = $http(prefsConfig);
                        mwPrefs.then(function(data){
                            configLog.data.received = data;
                            LoggingService.LogData.push(configLog.output());
                            defObj.resolve(data);
                        }, function(data){
                            configLog.data.received = data;
                            LoggingService.LogData.push(configLog.output());
                            defObj.reject(data);
                        });
                    } else {
                        defObj.resolve();
                    }

                    return defObj.promise;
                };

                $scope.doMWTransaction = function(transType){
                    $scope.resetLogger();
                    var prefs = $scope.saveMWConfig(false);
                    $q.when(prefs.then(function(){}))
                        .then(function(){
                            EFT.SetProvider('MerchantWare', prismSessionInfo.get().workstation._value_);
                            var defObj = $q.defer();
                            switch(transType){
                                case 'balance':
                                    var balance = EFT.Gift.Initiate('1234567890', 'GIFT_BALANCE', null, undefined, $scope.form.giftcard.carddata, false, true);
                                    balance.then(function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    }, function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    });
                                    break;
                                case 'value':
                                    var newBalance = EFT.Gift.Initiate('1234567890', 'GIFT_GIVE', $scope.form.giftcard.amount, undefined, $scope.form.giftcard.carddata, false, true);
                                    newBalance.then(function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    }, function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    });
                                    break;
                                case 'activate':
                                    var newCard = EFT.Gift.Initiate('1234567890', 'GIFT_ACTIVATE', $scope.form.giftcard.amount, undefined, $scope.form.giftcard.carddata, false, true);
                                    newCard.then(function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    }, function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    });
                                    break;
                                case 'tender':
                                    var giftCard = EFT.Gift.Initiate('1234567890', 'GIFT_TAKE', $scope.form.giftcard.amount, undefined, $scope.form.giftcard.carddata, false, true);
                                    giftCard.then(function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    }, function(){
                                        $scope.form.giftcard.completed = true;
                                        defObj.resolve();
                                    });
                                    break;
                                case 'initced':
                                    //init transaction
                                    var geniusInitialization = EFT.Credit.Initiate('1234567890', $scope.form.ced.type, $scope.form.ced.amount, 'True', false);
                                    $q.when(geniusInitialization.then(function(creditInit){
                                            $scope.mwData = creditInit;
                                        }, function(){
                                            defObj.resolve();
                                        })).then(function(){
                                            //get device list
                                            var cedDef = $q.defer();

                                            var deviceList = EFT.ListCardReaders();
                                            deviceList.then(function(data){
                                                $scope.deviceList = data;

                                                if ($scope.deviceList.sid === undefined){
                                                    cedDef.reject();
                                                } else {
                                                    cedDef.resolve();
                                                }
                                            });

                                            return cedDef.promise;
                                        }, function(){
                                            defObj.resolve();
                                        }).then(function(){
                                            //init device
                                            var initDevice = EFT.Credit.RemoteDevice($scope.mwData.transportkey, $scope.deviceList.sid, $scope.mwData.sid);
                                            initDevice.then(function(){
                                                //change buttons
                                                $scope.form.ced.initiated = true;
                                                //ping
                                                $scope.timeoutID = $timeout(function(){
                                                    $scope.checkStatus();
                                                }, 10000);
                                                defObj.resolve();
                                            }, function(){
                                                defObj.resolve();
                                            });
                                        }, function(){
                                            defObj.resolve();
                                        });
                                    break;
                                case 'cancelced':
                                    //check device list
                                    if($scope.deviceList.sid !== undefined){
                                        //abort ced
                                        var abortDevice = EFT.Credit.CancelRemoteDevice($scope.deviceList.sid);
                                        abortDevice.then(function(){
                                            $timeout.cancel($scope.timeoutID);
                                            //change buttons
                                            $scope.form.ced.completed = true;
                                            defObj.resolve();
                                        }, function(){
                                            $scope.form.ced.completed = true;
                                            defObj.resolve();
                                        });
                                    }
                                    break;
                                case 'initweb':
                                    var creditMode = $scope.form.web.type.substring($scope.form.web.type.indexOf('_') + 1);
                                    var CreditInitialization = EFT.Credit.Initiate('1234567890', $scope.form.web.type, $scope.form.web.amount, 'False', $scope.form.web.dupes, null, null, null, location.href + '?mode=' + creditMode);

                                    $q.when(CreditInitialization.then(function(creditInit){
                                            $scope.mwData = creditInit;
                                        }, function(){
                                            defObj.resolve();
                                        })).then(function(){
                                            var updateData = {
                                                web_redirect:location.href.split('?')[0] + '?mode=' + creditMode + '&sid=' + $scope.mwData.sid,
                                                'row_version':$scope.mwData.row_version
                                            };
                                            return EFT.Credit.Update($scope.mwData.sid, updateData);
                                        }, function(){
                                            defObj.resolve();
                                        }).then(function(){
                                            $scope.form.web.initiated = true;
                                            defObj.resolve();
                                        }, function(){
                                            defObj.resolve();
                                        });
                                    break;
                                case 'sendtoweb':
                                    //DocumentPersistedData.StoreForEFT();
                                    localStorage.setItem('mwtabs', 'true');
                                    location.href = prismSessionInfo.get().preferences.eft_mw_web_transaction_server._value_ + $scope.mwData.transportkey;
                                    break;
                                case 'mwwebrecover':
                                    var EFTMWStatus = EFT.Credit.Status($scope.form.web.sid, 'approvalstatus,errormessage,authcode,tendertype,token,row_version,amount,card_type_desc,card_type,cardnumber');
                                    EFTMWStatus.then(function(){
                                        $scope.form.web.initiated = true;
                                        $scope.form.web.completed = true;
                                        defObj.resolve();
                                    }, function(){
                                        $scope.form.web.initiated = true;
                                        $scope.form.web.completed = true;
                                        defObj.resolve();
                                    });
                                    break;
                            }

                            return defObj.promise;
                        })
                        .then(function(){
                            var remPrefs = $scope.saveMWConfig(true);
                            remPrefs.then(function(){});
                        });
                };

                $scope.newMWTransaction = function(activeTab){
                    $scope.form = {
                        giftcard:{
                            carddata:'',
                            amount:'',
                            completed:false
                        },
                        ced:{
                            amount:'',
                            type:'CREDIT_TAKE',
                            initiated:false,
                            completed:false
                        },
                        web:{
                            amount:'',
                            type:'CREDIT_TAKE',
                            initiated:false,
                            dupes:false,
                            sid:0,
                            mode:'',
                            completed:false
                        }
                    };
                    $scope.resetLogger();
                };


                $scope.checkStatus = function(){
                    var statusCheck = EFT.Credit.Status($scope.mwData.sid, 'approvalstatus,errormessage,authcode,paymenttype,token,row_version,amount,card_type_desc,card_type,tendertype,cardnumber');
                    statusCheck.then(function(data){
                        if(data.approvalstatus !== 'PRISM_AWAITING_CED'){
                            var logger = new Logger();
                            logger.label = 'EFT: Credit Transaction Status Results';
                            logger.data.url = '';
                            logger.data.method = 'GET';
                            logger.data.sent = '';
                            var approvalstatus = PrismUtilities.responseParser(data);
                            logger.data.received = approvalstatus;
                            LoggingService.LogData.push(logger.output());
                            if($scope.tabs.mw.ced){
                                $scope.form.ced.completed = true;
                            }
                        } else {
                            $scope.timeoutID = $timeout(function(){
                                $scope.checkStatus();
                            }, 1000);
                        }
                    });
                };
            }

        ]
);
