window.angular.module('prismApp.common.controllers.raw_http_ctrl', [])
    .controller('raw_http_ctrl',['$scope','$http','RPServices',
            function($scope,$http,RPServices){
                'use strict';
                $scope.form={};
                $scope.activeTab='';
                $scope.enablePaging=true;
                $scope.loadingResults=false;
                $scope.servicesList=[];
                var resetApiUrl = function(){
                    $scope.requestApiUrl=location.protocol+'//'+window.location.href.split('/')[2]+'/';
                };
                $scope.services = RPServices;
                $scope.services.get().then(
                    function (data) {
                      for(var i=0;i<data.length;i++){
                        if(data[i].name.toLowerCase()!=='replication'){
                          $scope.servicesList.push(data[i]);
                        }
                      }
                    }
                );
                $scope.resetForm=function(){
                    resetApiUrl();
                    $scope.responseData=false;
                    $scope.activeTab='';
                    $scope.form={
                        method:'GET',
                        cType:'application/json',
                        serviceModule:undefined,
                        //methodName:undefined,
                        resourceName:undefined,
                        params:'',
                        payload:''
                    };
                    $scope.enablePaging=true;
                };
                $scope.setServiceModule=function(){
                    //$scope.form.methodName='';
                    $scope.form.resourceName='';
                    $scope.resourceList = ($scope.form.serviceModule)?$scope.form.serviceModule.resources:[];
                    $scope.methodList = ($scope.form.serviceModule)?$scope.form.serviceModule.methods:[];
                    $scope.constructApiUrl();
                };
                $scope.isActiveTab=function(tab){
                    return (tab===$scope.activeTab)?'active':'';
                };
                $scope.setTab=function(tab){
                    $scope.activeTab=tab;
                };
                var appendResourceToUrl = function(){
                    // if($scope.form.methodName){
                    //   $scope.requestApiUrl+=$scope.form.methodName.name;
                    // }
                    if($scope.form.resourceName){
                        $scope.requestApiUrl+=$scope.form.resourceName.name;
                    }
                    if($scope.form.params){
                        $scope.requestApiUrl+=$scope.form.params;
                    }
                };
                $scope.constructApiUrl = function(){
                    resetApiUrl();
                    if($scope.form.serviceModule){
                        if($scope.form.serviceModule.uriversion==='1'){
                            $scope.requestApiUrl+='v1/rest/';
                            appendResourceToUrl();
                        }
                        if($scope.form.serviceModule.uriversion==='2'){
                            $scope.requestApiUrl+='api/'+$scope.form.serviceModule.name+'/';
                            appendResourceToUrl();
                        }
                    }
                };
                $scope.send=function(){
                    var version='';
                    $scope.activeTab='response';
                    $scope.loadingResults=true;
                    $scope.constructApiUrl();
                    if(!$scope.form.params&&$scope.form.resourceName){
                        $scope.requestApiUrl+='?cols=*';
                    }
                    if($scope.enablePaging){
                        $scope.requestApiUrl+='&page_no=1&page_size=10';
                    }
                    if($scope.form.serviceModule.uriversion==='2'){
                        version=',version=2';
                    }
                    var req=$http({
                        method:$scope.form.method,
                        url:$scope.requestApiUrl,
                        data:$scope.form.payload,
                        headers:{
                            'Accept':$scope.form.cType + version,
                            'Auth-Session': sessionStorage.getItem('PRISMAUTH')
                        }
                    });
                    req.then(function(data){
                        switch($scope.form.cType){
                            case 'application/json':
                                if($scope.requestApiUrl.indexOf('/api')>-1){
                                    $scope.responseData=data.data.data;
                                }else{
                                    $scope.responseData=data.data;
                                }
                            break;
                        }
                        $scope.loadingResults=false;
                    },function(errorData){
                        $scope.responseData=errorData;
                        $scope.loadingResults=false;
                    });
                };
                resetApiUrl();
            }
        ]
);
