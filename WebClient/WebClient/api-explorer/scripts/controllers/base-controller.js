﻿window.angular
  .module('prismApp.controllers.base_ctrl', [])
  .controller('base_ctrl', [
    '$scope',
    '$http',
    'loginProvider',
    '$location',
    'prismSessionInfo',
    'PrismUtilities',
    '$q',
    'Logger',
    'EFT',
    'LoggingService',
    function(
      $scope,
      $http,
      loginProvider,
      $location,
      prismSessionInfo,
      PrismUtilities,
      $q,
      Logger,
      EFT,
      LoggingService
    ) {
      'use strict';

      $scope.notLoggedIn = true;
      $scope.notSeated = true;
      $scope.changetotab = 'detailed';
      $scope.sitaction = 'sit';
      $scope.showRawResponse = false;
      $scope.rawResponseButtonText = 'Show Raw Response';

      $scope.credentials = {
        username: '',
        password: ''
      };

      $scope.gb = {
        action: 'sale',
        amount: '',
        reference: '',
        receiptno: '',
        confirmamount: '',
        demo: true,
        success: location.pathname.substring(1),
        error: location.pathname.substring(1),
        successtothispage: true,
        errorstothispage: true
      };

      if (!$http.defaults.headers.common['Auth-Session']) {
        if (sessionStorage.getItem('PRISMAUTH') !== null) {
          $scope.credentials.username = sessionStorage.getItem('username');
          $http.defaults.headers.common[
            'Auth-Session'
          ] = sessionStorage.getItem('PRISMAUTH');
          $scope.notLoggedIn = false;
          $scope.notSeated = false;
        }
      }

      $scope.logIn = function() {
        $scope.loginDebug = '';
        var credentials = {
          username: $scope.credentials.username,
          password: $scope.credentials.password
        };
        sessionStorage.setItem('username', $scope.credentials.username);
        loginProvider.login(credentials).then(
          function() {
            switch ($scope.sitaction) {
              case 'sittab':
                $scope.tabs.sit.active = true;
                break;
              case 'detailtab':
                $scope.tabs.detailed.active = true;
                break;
              default:
                $scope.sitDown();
                break;
            }
            $scope.loginDebug = '';
          },
          function(data) {
            $scope.loginDebug = data.error;
          }
        );
      };
      $scope.tabs = {
        gb: {
          active: false,
          initiate: { active: false },
          response: { active: false }
        },
        mw: {
          active: false,
          config: false,
          ced: false,
          web: false,
          gift: false
        }
      };
      $scope.logOut = function() {
        $scope.loginDebug = '';
        loginProvider.logout();
        delete $http.defaults.headers.common['Auth-Session'];

        $scope.notLoggedIn = true;
      };

      $scope.sitDown = function() {
        $scope.loginDebug = '';
        loginProvider.sit().then(
          function() {
            $scope.notLoggedIn = false;
            $scope.loginDebug = '';
            $scope.notSeated = false;
          },
          function(data) {
            $scope.loginDebug = data.error;
          }
        );
      };

      $scope.resetLogger = function() {
        LoggingService.LogData = LoggingService.CleanLog();
      };

      function qsParser(queryString) {
        var queryElements = queryString.split('&');
        var output = {};
        for (var qe = 0; qe < queryElements.length; qe++) {
          var kvp = queryElements[qe].split('=');
          if (kvp.length > 1) {
            output[kvp[0]] = decodeURIComponent(kvp[1]);
          }
        }
        return output;
      }
    }
  ]);
