window.angular.module('prismApp.controllers.rpc_ctrl', [])

    .controller('rpc_ctrl',
        [
            '$scope','RPServices','$http',
            function($scope,RPServices,$http){
                'use strict';

                $scope.sendRPC=function(){
                    //console.log($scope.rpcConfig.data);
                    $http.post('/v1/rpc',[{MethodName:$scope.rpcConfig.data.methodname,Params:$scope.rpcConfig.data.params}]).then(function(data){
                        $scope.rpc.rpcResponse=data.data;
                        $scope.setTab('rpcresponse');
                    },function(error){
                        angular.noop();
                    });
                }
                $scope.parse=function(x){
                  return x;
                };
                $scope.grids={};
                $scope.getRPCData=function(obj){
                    var data=[];
                    var json={}
                    for(var y in obj){
                        json[y]= (obj[y]!=='')?obj[y]:'- -';
                    }
                    data.push(json);
                    //console.log(data);
                    return JSON.stringify(data);
                }
            }
        ]
    );
