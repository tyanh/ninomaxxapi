window.angular
  .module('prismApp.controllers.ac_services_ctrl', [])

  .controller('ac_services_ctrl', [
    '$scope',
    'RPServices',
    '$http',
    function($scope, RPServices, $http) {
      'use strict';
      $scope.services = RPServices;
      $scope.activeResource = null;
      $scope.rpcName = null;
      $scope.currentUrl = null;
      $scope.sampleJsonPayload = null;
      $scope.gridType = -1;
      $scope.toolTipText = 'Copy to Clipboard';
      //Get all the services
      $scope.services.get().then(function(data) {
        $scope.serviceList = data;
        $scope.Rpartial = '';
      });
      var protocol = location.protocol + '//';

      $scope.getRPC = function(rpcName, version, moduleName) {
        $scope.activeResource = null;
        $scope.sampleJsonPayload = null;
        $scope.rpcV1GridData = [];
        $scope.rpcV2GridData = [];
        if (version === '2') {
          $scope.currentUrl = 'api/' + moduleName + '?action=' + rpcName;
          $scope.services
            .getRPC(rpcName, version, moduleName)
            .then(function(data) {
              $scope.rpcName = rpcName;
              $scope.rpcData = data.data[0];
              $scope.rpcData.comments = data.comment;
              $scope.rpcV2GridData = $scope.rpcData.parameters;
              $scope.gridType = version;
              $scope.setJsonPayload($scope.rpcV2GridData, '2');
              $scope.rpcV2GridApi.core.clearAllFilters();
              $scope.refreshGridView();
            });
        } else {
          $scope.rpcConfig = {
            method: 'POST',
            url: '/v1/rpc/',
            data: [
              {
                params: {},
                methodname: rpcName
              }
            ]
          };
          $scope.currentUrl = $scope.rpcConfig.url.substring(1) + rpcName;
          $scope.services.getRPC(rpcName).then(
            function(data) {
              $scope.rpcName = rpcName;
              $scope.rpcData = data.data[0];
              var params = $scope.rpcData.params;
              for (var x in params) {
                if (x !== 'ismetadata') {
                  $scope.rpcV1GridData.push(params[x]);
                }
              }
              $scope.gridType = version;
              $scope.setJsonPayload($scope.rpcV1GridData, '1');
              $scope.rpcV1GridApi.core.clearAllFilters();
              $scope.refreshGridView();
            },
            function() {
              alert('You must be logged in to view RPC calls.');
            }
          );
        }
      };

      function dynamicSort(property) {
        var sortOrder = 1;
        if (property[0] === '-') {
          sortOrder = -1;
          property = property.substr(1);
        }
        return function(a, b) {
          var result =
            a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
          return result * sortOrder;
        };
      }
      //Gets the meta data for the selected service
      $scope.getMeta = function(url) {
        $scope.myData = [];
        $scope.rpcData = undefined;
        $scope.sampleJsonPayload = null;
        $scope.defaultCols = [];
        //$scope.Rpartial = 'views/rest-resources.htm';

        $scope.services.getMeta(url).then(
          function(data) {
            if (data) {
              //theres only ever one key at the top level
              var dta = data.data;
              for (var i = 0; i < dta.length; i++) {
                //delete unused meta data
                delete dta[i].value;
                delete dta[i].accesstorefattribute;
                delete dta[i].alignment;
                delete dta[i].colDef;
                //add the column to the grid data
                $scope.myData.push(dta[i]);
              }
              var version = '';
              if (url.indexOf('introspection') !== -1) {
                $scope.currentUrl = url.substring(
                  0,
                  url.indexOf('?introspection')
                );
              } else {
                $scope.currentUrl = url.substring(0, url.indexOf('/meta'));
              }
              $scope.activeResource = $scope.myData[0].attributesource;
              $scope.myData.sort(dynamicSort('attributename'));
              $scope.toggleDefaultCols();
              $scope.gridType = '0';
              $scope.setJsonPayload(dta, '0');
              $scope.restGridApi.core.clearAllFilters();
              $scope.refreshGridView();
            }
          },
          function(error) {
            angular.noop();
          }
        );
      };

      $scope.setJsonPayload = function(payload, type) {
        var jsonPayload = {};
        var key = '';
        var value = 'datatype';
        switch (type) {
          case '1':
            key = 'name';
            break;
          case '2':
            key = 'paramname';
            break;
          default:
            key = 'attributename';
            break;
        }
        for (var i = 0; i < payload.length; i++) {
          jsonPayload[payload[i][key].toLowerCase()] = payload[i][value]? payload[i][value].toLowerCase(): payload[i][value];
        }
        $scope.sampleJsonPayload = JSON.stringify(jsonPayload, undefined, '\t');
      };

      $scope.copyToClipboard = function() {
        var copyText = document.getElementById('sampleJsonPayload');
        copyText.select();
        document.execCommand('copy');
        $scope.toolTipText = 'Copied!';
      };

      $scope.resetTooltip = function(){
        $scope.toolTipText = 'Copy to Clipboard';
      };

      $scope.refreshGridView = function(){
        if($scope.rpcV1GridApi){
          $scope.rpcV1GridApi.core.refresh(true);
        }
        if($scope.rpcV2GridApi){
          $scope.rpcV2GridApi.core.refresh(true);
        }
        if($scope.restGridApi){
          $scope.restGridApi.core.refresh(true);
        }
      };

      var default_cols = [
        'tenant_sid',
        'controller_sid',
        'modified_datetime',
        'created_datetime',
        'sid',
        'row_version',
        'rowversion',
        'origin_application',
        'modified_by',
        'post_date',
        'created_by',
        'tenantsid',
        'controllersid',
        'modifieddatetime',
        'createddatetime',
        'sid',
        'rowversion',
        'rowversion',
        'originapplication',
        'modifiedby',
        'postdate',
        'createdby'
      ];
      $scope.toggleDefaultCols = function() {
        var newCols = [];
        if ($scope.defaultCols.length === 0) {
          for (var i = 0; i < $scope.myData.length; i++) {
            if ($scope.myData[i].attributename) {
              if (
                default_cols.indexOf(
                  $scope.myData[i].attributename.toLowerCase()
                ) !== -1
              ) {
                $scope.defaultCols.push($scope.myData[i]);
              } else {
                newCols.push($scope.myData[i]);
              }
            }
          }
          $scope.myData = newCols;
          $scope.colTxt = 'Show';
        } else {
          for (var i = 0; i < $scope.defaultCols.length; i++) {
            $scope.myData.unshift($scope.defaultCols[i]);
          }
          $scope.defaultCols = [];
          $scope.colTxt = 'Hide';
        }
      };
      $scope.reset = function(url, flg) {
        $scope.rpcName = null;
        $scope.responseData = undefined;
        $scope.actualResponseData = undefined;
        $scope.putRecords = undefined;
        $scope.putData = [];
        $scope.tab = 'request';
        if (!flg) {
          $scope.getMeta(url);
        }
      };
      $scope.active = {
        url: ''
      };

      //this holds the grid data
      $scope.myData = [];
      //Default column Definitions
      var colDefs = [
        { field: 'attributename', width: 120, pinned: true,filter: {placeholder: 'search...'}},
        { field: 'comment', width: 150, pinned: false },
        { field: 'required', width: 80, pinned: false },
        { field: 'uidatatype', width: 100, pinned: false },
        { field: 'valuelist', width: 180, pinned: false },
        { field: 'valuelistname', width: 120, pinned: false },
        { field: 'lookupwhere', width: 120, pinned: false },
        { field: 'attributesize', width: 100, pinned: false },
        { field: 'datatype', width: 80, pinned: false },
        { field: 'refentityname', width: 120, pinned: false },
        { field: 'attributetype', width: 120, pinned: false },
        { field: 'fieldname', width: 120, pinned: false },
        { field: 'identifyingattribute', width: 120, pinned: false },
        { field: 'readonly', width: 80, pinned: false }
      ];

      //Meta Grid Parameters
      $scope.gridOptions = {
        data: 'myData',
        enableSorting: true,
        enableColumnResizing: true,
        enableGridMenu: true,
        enableFiltering: true,
        rowHeight: 30,
        minRowsToShow: 15,
        columnDefs: colDefs,
        onRegisterApi: function(gridApi){
          $scope.restGridApi = gridApi;
        }
      };

      //this holds the grid data
      $scope.rpcV1GridData = [];
      $scope.rpcV2GridData = [];
      //Default column Definitions
      var rpcV1ColDefs = [
        { field: 'name', width: 120, pinned: true, filter: {placeholder: 'search...'}},
        { field: 'datatype', width: 120, pinned: false },
        { field: 'comments', width: 180, pinned: false },
        { field: 'minlength', width: 80, pinned: false },
        { field: 'maxlength', width: 80, pinned: false },
        { field: 'optional', width: 80, pinned: false },
        { field: 'encoded', width: 80, pinned: false },
        { field: 'inouttype', width: 80, pinned: false },
        { field: 'minvalue', width: 120, pinned: false },
        { field: 'maxvalue', width: 120, pinned: false },
        { field: 'default', width: 120, pinned: false }
      ];
      var rpcV2ColDefs = [
        { field: 'paramname', width: 150, pinned: true, filter: {placeholder: 'search...'} },
        { field: 'datatype', width: 120, pinned: false },
        { field: 'comment', width: 240, pinned: false },
        { field: 'translationid', width: 140, pinned: false }
      ];

      //RPC Meta Grid Parameters
      $scope.rpcV1GridOptions = {
        data: 'rpcV1GridData',
        enableSorting: true,
        enableColumnResizing: true,
        enableGridMenu: true,
        enableFiltering: true,
        rowHeight: 30,
        minRowsToShow: 15,
        columnDefs: rpcV1ColDefs,
        onRegisterApi: function(gridApi){
          $scope.rpcV1GridApi = gridApi;
        }
      };

      $scope.rpcV2GridOptions = {
        data: 'rpcV2GridData',
        enableSorting: true,
        enableColumnResizing: true,
        enableGridMenu: true,
        enableFiltering: true,
        rowHeight: 30,
        minRowsToShow: 15,
        columnDefs: rpcV2ColDefs,
        onRegisterApi: function(gridApi){
          $scope.rpcV2GridApi = gridApi;
        }
      };

      $scope.parse = function(x) {
        return x;
      };
      $scope.grids = {};
      $scope.getRPCData = function(obj) {
        var data = [];
        var json = {};
        for (var y in obj) {
          json[y] = obj[y] !== '' ? obj[y] : '- -';
        }
        data.push(json);
        //console.log(data);
        return JSON.stringify(data);
      };

      $scope.$on('$locationChangeSuccess', function(event) {
        if (window.location.href.indexOf('resource') !== -1) {
          $scope.getMeta($scope.active.url);
        }
        if (window.location.href.indexOf('rpc') !== -1) {
          $scope.active.url = unescape(
            window.location.href.toString().split('?rpc=')[1]
          );
          $scope.getRPC($scope.active.url, $scope.version, $scope.moduleName);
        }
      });
      $scope.setVersion = function(version, moduleName) {
        $scope.version = version;
        $scope.moduleName = moduleName;
      };
      if (window.location.href.indexOf('resource') !== -1) {
        $scope.active.url = unescape(
          window.location.href.toString().split('?resource=')[1]
        );
        $scope.getMeta($scope.active.url);
      }
      if (window.location.href.indexOf('rpc') !== -1) {
        $scope.active.url = unescape(
          window.location.href.toString().split('?rpc=')[1]
        );
        if ($scope.version === '2') {
          $scope.getMeta($scope.active.url + '=true');
        } else {
          $scope.getRPC($scope.active.url);
        }
      }
      if (
        window.location.href.indexOf('introspection') !== -1 &&
        window.location.href.indexOf('rpc') !== -1
      ) {
        $scope.active.url = unescape(
          window.location.href.toString().split('?resource=')[1]
        );
        $scope.getMeta($scope.active.url + '=true');
      }
      //Saves the grid state when the grid state changes
      $scope.$on('ngGridEventColumns', function(newColumns) {
        localStorage.removeItem('serviceGridSettings');
        if (
          newColumns.targetScope.columns[0] &&
          newColumns.targetScope.columns[0].displayName === 'attributename'
        ) {
          var cols = [];
          for (var i = 0; i < newColumns.targetScope.columns.length; i++) {
            cols.push(JSON.stringify(newColumns.targetScope.columns[i]) + '|');
          }

          localStorage.setItem('serviceGridSettings', cols);
        }
      });

      //set the grid back to the saved state if the settings exist
      if (localStorage.getItem('serviceGridSettings')) {
        colDefs = [];
        var cut = localStorage.getItem('serviceGridSettings').split('|,');
        for (var i = 0; i < cut.length; i++) {
          var obj;
          if (i === cut.length - 1) {
            obj = JSON.parse(cut[i].replace('|', ''));
          } else {
            obj = JSON.parse(cut[i]);
          }
          colDefs.push(obj);
        }
      }
      //var serverName = window.location.href.split('/');
      //serverName = serverName[2];
      // $scope.putData = [];
      // $scope.request = {
      //   method: 'GET',
      //   server: serverName,
      //   payload: '',
      //   params: '',
      //   accept: 'application/json',
      //   filter: '',
      //   cols: [],
      //   send: function() {
      //     var version = '';
      //     if ($scope.activeResource.indexOf('api/') !== -1) {
      //       version = ',version=2';
      //       $scope.request.cols.push('rowversion');
      //     }
      //     $scope.request.params =
      //       '?cols=row_version,sid,' + $scope.request.cols.join(',');
      //     $scope.request.params +=
      //       '&filter=' + encodeURIComponent($scope.request.filter);
      //     $http({
      //       method: 'GET',
      //       url:
      //         protocol +
      //         $scope.request.server +
      //         '/' +
      //         $scope.activeResource +
      //         $scope.request.params,
      //       headers: {
      //         Accept: $scope.request.accept + version,
      //         'Auth-Session': sessionStorage.getItem('PRISMAUTH')
      //       }
      //     }).then(
      //       function(data) {
      //         if (data.data.data) {
      //           data.data.config = data.config;
      //           data.data.headers = data.headers;
      //           data = data.data;
      //         }
      //         switch ($scope.request.method) {
      //           case 'GET':
      //             $scope.responseData = data;
      //             $scope.actualResponseData = data.data;
      //             $scope.tab = 'response';
      //             $scope.putRecords = data.data;
      //             $scope.putData = [];
      //             for (var i = 0; i < $scope.putRecords.length; i++) {
      //               var ar = $scope.putRecords[i];
      //               var row = {};
      //               for (var x in ar) {
      //                 row[x] = ar[x];
      //               }
      //               $scope.putData.push(row);
      //             }
      //             break;
      //           case 'PUT':
      //             break;
      //         }
      //       },
      //       function(error) {
      //         angular.noop();
      //       }
      //     );
      //   }
      // };
      // $scope.doDelete = function() {
      //   if (
      //     confirm(
      //       'Are you sure you want to delete this record?  This action cannot be undone.'
      //     )
      //   ) {
      //     $http({
      //       method: 'DELETE',
      //       url:
      //         protocol +
      //         $scope.request.server +
      //         '/' +
      //         $scope.activeResource +
      //         '/' +
      //         $scope.request.sid,
      //       headers: {
      //         Accept: $scope.request.accept,
      //         'Auth-Session': sessionStorage.getItem('PRISMAUTH')
      //       }
      //     }).then(
      //       function(data) {
      //         $scope.tab = 'response';
      //         $scope.responseData = data;
      //         $scope.actualResponseData = data.data;
      //         $scope.putRecord = undefined;
      //         $scope.putRecords = undefined;
      //         $scope.tab = 'response';
      //         $scope.setTabs(4);
      //       },
      //       function(error) {
      //         angular.noop();
      //       }
      //     );
      //   }
      // };
      // $scope.postTab = function(tab) {
      //   $scope.postForm = false;
      //   $scope.postRaw = false;
      //   $scope[tab] = true;
      // };
      // $scope.doPost = function() {
      //   var payload = $scope.postRaw? $scope.request.postStr
      //     : $scope.postObject;
      //   $http({
      //     method: 'POST',
      //     url: protocol + $scope.request.server + '/' + $scope.activeResource,
      //     headers: {
      //       Accept: $scope.request.accept,
      //       'Auth-Session': sessionStorage.getItem('PRISMAUTH')
      //     },
      //     data: payload
      //   }).then(
      //     function(data) {
      //       $scope.tab = 'response';
      //       $scope.responseData = data;
      //       $scope.actualResponseData = data.data;
      //       $scope.requestPayload = payload;
      //       $scope.putRecord = undefined;
      //       $scope.putRecords = undefined;
      //       $scope.tab = 'response';

      //       $scope.setTabs(4);
      //     },
      //     function(error) {
      //       angular.noop();
      //     }
      //   );
      // };
      // $scope.putRecord;
      // $scope.cancelPut = function() {
      //   $scope.putRecord = undefined;
      // };
      // $scope.doPut = function(payload) {
      //   var version = '',
      //     rowV = 'row_version';

      //   if ($scope.activeResource.indexOf('api/') !== -1) {
      //     version = ',version=2';
      //     rowV = 'rowversion';
      //   }
      //   var defaultPayLoad = [$scope.putRecord];
      //   var payload = payload ? payload : defaultPayLoad;
      //   var sid = $scope.putRecord ? $scope.putRecord.sid : $scope.request.sid;
      //   var row_version = $scope.putRecord? $scope.putRecord[rowV]
      //     : $scope.request[rowV];
      //   var filter =
      //     version === '' ? '?filter=row_version,EQ,' + row_version : '';

      //   payload.rowversion = row_version;
      //   if (version !== '') {
      //     payload = {
      //       data: payload
      //     };
      //   }

      //   $http({
      //     method: 'PUT',
      //     url:
      //       protocol +
      //       $scope.request.server +
      //       '/' +
      //       $scope.activeResource +
      //       '/' +
      //       sid +
      //       filter,
      //     headers: {
      //       Accept: $scope.request.accept + version,
      //       'Auth-Session': sessionStorage.getItem('PRISMAUTH')
      //     },
      //     data: payload
      //   }).then(
      //     function(data) {
      //       if (data.data.data) {
      //         data.data.config = data.config;
      //         data.data.headers = data.headers;
      //         data = data.data;
      //       }
      //       $scope.tab = 'response';
      //       $scope.responseData = data;
      //       $scope.actualResponseData = data.data;
      //       $scope.requestPayload = payload;
      //       $scope.tab = 'response';
      //       $scope.putRecord = undefined;
      //       $scope.putRecords = undefined;
      //       $scope.setTabs(4);
      //     },
      //     function(error) {
      //       angular.noop();
      //     }
      //   );
      // };
      // $scope.checkTab = function(tab) {
      //   if (tab.name === 'Table View') {
      //     if ($scope.putRecords) {
      //       return true;
      //     }
      //     return false;
      //   }
      //   return true;
      // };
      // $scope.tab = 'request';
      // $scope.checkDataType = function(colName) {
      //   for (var i = 0; i < $scope.myData.length; i++) {
      //     var obj = $scope.myData[i];
      //     if (
      //       colName.toLowerCase() === 'sid' ||
      //       colName.toLowerCase() === 'row_version' ||
      //       colName.toLowerCase() === 'rowversion' ||
      //       colName.toLowerCase() === 'link' ||
      //       colName.toLowerCase() === 'originapplication' ||
      //       colName.toLowerCase() === 'origin_application'
      //     ) {
      //       return true;
      //     }
      //     if (obj.attributename.toLowerCase() === colName.toLowerCase()) {
      //       if (
      //         obj.readonly === true ||
      //         obj.attributetype === 'LOOKUP' ||
      //         ($scope.request.method === 'PUT' && obj.immutable === true)
      //       ) {
      //         return true;
      //       }
      //     }
      //   }
      //   return false;
      // };
      //PUT Records Grid
      // $scope.putGridOptions = {
      //   data: 'putData',
      //   enableSorting: true,
      //   enableFullRowSelection: true,
      //   enableColumnResizing: true,
      //   enableCellEditOnFocus: false,
      //   enableGridMenu: true,
      //   enableSelectAll: false,
      //   multiSelect: false
      // };
      // $scope.putGridOptions.onRegisterApi = function(gridApi) {
      //   gridApi.selection.on.rowSelectionChanged($scope, function(row) {
      //     if (row.isSelected) {
      //       for (var i = 0; i < $scope.putRecords.length; i++) {
      //         if ($scope.putRecords[i].sid === row.entity.sid) {
      //           $scope.putRecord = $scope.putRecords[i];
      //         }
      //       }
      //     }
      //   });
      // };
      // $scope.tabs = [
      //   {
      //     name: 'Table View',
      //     isOn: true,
      //     className: 'active'
      //   },
      //   {
      //     name: 'Request Headers',
      //     isOn: false,
      //     className: ''
      //   },
      //   {
      //     name: 'Request Payload',
      //     isOn: false,
      //     className: ''
      //   },
      //   {
      //     name: 'Response Headers',
      //     isOn: false,
      //     className: ''
      //   },
      //   {
      //     name: 'Response Payload',
      //     isOn: false,
      //     className: ''
      //   }
      // ];
      // $scope.setTabs = function(index) {
      //   for (var i = 0; i < $scope.tabs.length; i++) {
      //     $scope.tabs[i].isOn = false;
      //     $scope.tabs[i].className = '';
      //   }
      //   $scope.tabs[index].isOn = true;
      //   $scope.tabs[index].className = 'active';
      // };
      // $scope.fixURL = function(url) {
      //   var re = /,/g;
      //   if (url) {
      //     return url.replace(re, ', ');
      //   }
      // };

      // $scope.gridOptions.onRegisterApi = function(gridApi) {
      //   gridApi.selection.on.rowSelectionChanged($scope, function(row) {
      //     if (row.isSelected) {
      //       $scope.request.cols.push(row.entity.attributename);
      //     } else {
      //       for (var i = 0; i < $scope.request.cols.length; i++) {
      //         if ($scope.request.cols[i] === row.entity.attributename) {
      //           $scope.request.cols.splice(i);
      //           break;
      //         }
      //       }
      //     }
      //   });
      // };
      // $scope.sendRPC=function(){
      //   //console.log($scope.rpcConfig.data);
      //   $http.post('/v1/rpc',[{MethodName:$scope.rpcConfig.data.methodname,Params:$scope.rpcConfig.data.params}]).then(function(data){
      //       $scope.rpc.rpcResponse=data.data;
      //       $scope.setTab('rpcresponse');
      //   },function(error){
      //       angular.noop();
      //   });
      // };
    }
  ]);
