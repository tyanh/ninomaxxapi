//base object factory
window.angular.module('prismApp.factories.Logger', ['Class'])
	.factory('Logger', ['Class',
		function (Class) {
			'use strict';

			return Class.extend({
				init: function() {
					this.label = '';
					this.data = {
						url:null,
						method:null,
						sent:null,
						received:null
					};
				},
				output:function(){
					return angular.fromJson(this);
				}
			});
		}
	]);