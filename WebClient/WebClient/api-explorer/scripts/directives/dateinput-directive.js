/////////////////////////////////////////////////////////////////////////////////////////
// USAGE:
// ng-model MUST be a javascript date object assigned to a scope variable
// the $scope variable must be declared either on the scope directly, or one parent down.
// ex.  $scope.dateObject=new Date() OR $scope.myObject.dateObject=new Date()
// OPTIONAL ATTRIBUTE:
// is-time='true' if set, time will be included in the calendar
// FULL EXAMPLE:
// <dateinput ng-model='dateObject' is-time='true' />
/////////////////////////////////////////////////////////////////////////////////////////
window.angular.module('prismApp.common.directives.dateinput', [])
	.directive('dateinput', ['PrismUtilities','DateInput',function (PrismUtilities,DateInput) {
		'use strict';
        var dtck = new Date();
        dtck.setHours(13);
        var tm = dtck.toLocaleTimeString();

        //This checks the time format
        var cut = tm.split(':');
        var timePreference;
        if(cut[0].length>2){
            cut[0]=cut[0].charAt(1).toString() + cut[0].charAt(2).toString();
        }
        timePreference = (cut[0] === '13') ? 1 : 0;
		return {
			restrict: 'E',
            scope:{ngModel:'='},
			link: function ($scope, $element, $attrs) {
                var i;
                $scope.timePref=timePreference;
                $scope.isTime=$attrs.isTime;
                $scope.btnClass=($scope.isTime)?'cal-btn':'cal-btn-sm';

                var dt;
                if($scope.ngModel===''){
                    dt=new Date();
                }else{
                    dt=$scope.ngModel;
                }
                //This is the ID of the calendars outer div we use it to do click over detection
                $scope.cId=cut[cut.length-1];

                //Whether or no the calendar is displayed
                $scope.calendar_on={
                    stat:false
                };

                //Toggles the calendar
                var blankFlg=false;
                $scope.calToggle=function($event){
                    $scope.calendar_on.stat=!$scope.calendar_on.stat;

                    if($scope.calendar_on.stat){

                        if($scope.ngModel===''){
                            blankFlg=true;
                        }
                        $scope.ngModel=$scope.dInfo.dObj;
                        $scope.orig=$scope.ngModel;
                        if(DateInput.calendar_on===true){
                            DateInput.scope.calendar_on.stat=false;
                        }
                        DateInput.calendar_on=true;
                        DateInput.scope=$scope;
                        document.getElementById($scope.cId + '_bg').onclick=function(event){
                            if(!PrismUtilities.isClickInObject(document.getElementById($scope.cId),event)){
                                $scope.$apply(function () {
                                    $scope.calendar_on.stat=false;
                                    DateInput.calendar_on=false;
                                });
                            }
                        };
                    }else{

                        DateInput.calendar_on=false;
                    }
                    $event.stopPropagation();
                };

                //This keeps track of
                $scope.dInfo={
                    dObj:dt,
                    month:dt.getMonth(),
                    day:dt.getDate(),
                    year:dt.getFullYear(),
                    hours:[],
                    hour:'',
                    ampm:'',
                    minute:'',
                    minutes:[],
                    months : [
                        {'month':'January',index:0},
                        {'month':'February',index:1},
                        {'month':'March',index:2},
                        {'month':'April',index:3},
                        {'month':'May',index:4},
                        {'month':'June',index:5},
                        {'month':'July',index:6},
                        {'month':'August',index:7},
                        {'month':'September',index:8},
                        {'month':'October',index:9},
                        {'month':'November',index:10},
                        {'month':'December',index:11}
                    ],
                    years:[]
                };

                //Fill the minutes array
                for(i=0;i<60;i=i+5){
                    var min = (i<10)?'0' + i:i;
                    $scope.dInfo.minutes.push(min);
                }

                //Fill the hours array
                //When using standard time we have to call this every time AM/PM changes.
                $scope.fillHours=function(){
                    $scope.dInfo.hours.length=0;
                    //If it's 24 hour time it's super easy
                    var hourStart,hourEnd,offset,i;
                    if(timePreference===1){
                        hourStart=0;
                        hourEnd=24;
                        for(i=hourStart;i<hourEnd;i++){
                            $scope.dInfo.hours.push({
                                txt:(i<10)?'0'+i:i,
                                val:i
                            });
                        }
                    }else{
                        //standard time will show only 1-12, but the val will contain
                        // the equivalent 24 hour index for setting the hour
                        hourStart=1;
                        hourEnd=13;
                        offset=0;

                        //if it's PM then then the txt value is -12 from val unless it's 12AM or PM
                        if($scope.dInfo.dObj.getHours()>11){
                            offset=-12;
                            hourStart=13;
                            hourEnd=25;
                        }
                        for(i=hourStart;i<hourEnd;i++){
                            if(i===12&&$scope.dInfo.dObj.getHours()<12){
                                $scope.dInfo.hours.push({
                                    txt:12,
                                    val:0
                                });
                            }else{
                                if(i===24&&$scope.dInfo.dObj.getHours()>11){
                                    $scope.dInfo.hours.push({
                                        txt:12,
                                        val:12
                                    });
                                }else{
                                    $scope.dInfo.hours.push({
                                        txt:i+offset,
                                        val:i
                                    });
                                }
                            }
                        }
                    }
                };
                $scope.fillHours();

                //Fill the year array, we only go back to 2007 so we don't have to deal with daylight savings time
                //issues in Chrome, when localizing date and time.
                for(i=(2007);i<($scope.dInfo.year+10);i++){
                    $scope.dInfo.years.push(i);
                }

                //Fills calendar, called whenever month or year changes.
                $scope.processDays=function(){
                    var pre_mo_end,day,i;
                    $scope.days=[];
                    $scope.rows=[];
                    var oDate=new Date($scope.dInfo.dObj);

                    //Set to the first day of the month
                    oDate.setDate(1);
                    day = oDate.getDay();

                    //if it's not Sunday
                    if(day!==0){

                        // go to the last day of the previous month
                        oDate.setDate(0);
                        var dif = oDate.getDay();
                        pre_mo_end=oDate.getDate();

                        //fill the row from the first day of the week, to the end of the previous month
                        for(i = (pre_mo_end-dif);i<=pre_mo_end;i++){
                            oDate.setDate(i);
                            $scope.rows.push(
                                {
                                    'date_str':i,
                                    'date_obj':new Date(oDate)
                                }
                            );
                        }

                        //sets the date back to the current month.
                        oDate.setDate(32);

                        //set the date back to the first day of the month
                        oDate.setDate(1);
                    }

                    //Now we process the current month
                    var mo = oDate.getMonth();
                    i=0;

                    //Loop through the incrementing the date month until the month flips
                    while(oDate.getMonth()===mo){

                        //increment the date
                        i++;
                        oDate.setDate(i);

                        //make sure we're still in the same month
                        if(oDate.getMonth()===mo){

                            //if it's the first day of the week, add the row to the days array and clear rows
                            if(oDate.getDay()===0){
                                $scope.days.push($scope.rows);
                                $scope.rows=[];
                            }

                            //add the day to the calendar if it's active it gets btn-info class
                            $scope.rows.push(
                                {
                                    'date_str':i,
                                    'date_obj':new Date(oDate),
                                    'active':(i===$scope.dInfo.dObj.getDate())?'btn-info':''
                                }
                            );
                        }
                    }
                    if(oDate.getDay()===0){
                        $scope.days.push($scope.rows);
                        $scope.rows=[];
                    }
                    //Now we are in the next month
                    //if we're at the beginning of the week we're done!
                    //otherwise fill the rest of the week
                    if(oDate.getDay()!==0){
                        if(oDate.getDay()<6){
                            $scope.rows.push(
                                {
                                    'date_str':oDate.getDate(),
                                    'date_obj':new Date(oDate)
                                }
                            );
                            i=1;
                            while(oDate.getDay()!==6){
                                i++;
                                oDate.setDate(i);
                                if(oDate.getDay()===0){
                                    $scope.days.push($scope.rows);
                                    $scope.rows=[];
                                }
                                $scope.rows.push(
                                    {
                                        'date_str':oDate.getDate(),
                                        'date_obj':new Date(oDate)
                                    }
                                );
                            }
                        }
                        if($scope.rows.length!==7){
                            $scope.rows.push(
                                {
                                    'date_str':oDate.getDate(),
                                    'date_obj':new Date(oDate)
                                }
                            );
                        }
                        $scope.days.push($scope.rows);
                    }
                };
                $scope.processDays();

                //Sets the year
                $scope.setYears=function(yr){
                    $scope.dInfo.year=yr;
                    $scope.dInfo.dObj.setFullYear(yr,$scope.dInfo.dObj.getMonth(),$scope.dInfo.dObj.getDate());
                    $scope.processDays();
                    $scope.setDisplay();
                };

                //Sets the month
                $scope.setMonth=function(num){
                    $scope.dInfo.dObj.setMonth(num);
                    $scope.processDays();
                    $scope.setDisplay();
                };

                //Sets the date
                $scope.selectDate=function(d){
                    $scope.dInfo.dObj.setFullYear(d.getFullYear(),d.getMonth(),d.getDate());
                    $scope.processDays();
                    $scope.setDisplay();
                    $scope.calToggle();
                };

                //Sets hours
                $scope.setHours=function(hr){
                    $scope.dInfo.dObj.setHours(hr.val);
                    $scope.setDisplay();
                };

                //Set minutes
                $scope.setMinutes=function(min){
                    $scope.dInfo.dObj.setMinutes(min);
                    $scope.setDisplay();
                };

                //Sets am/pm
                $scope.setAmPm=function(){
                    $scope.dInfo.ampm=($scope.dInfo.ampm==='AM')?'PM':'AM';
                    var txt = $scope.dInfo.ampm;
                    if(timePreference===0){
                        var dhr=$scope.dInfo.hour;
                        if(txt==='AM'){
                            dhr=(dhr.toString()==='12')?dhr=0:dhr;
                        }else{
                            dhr=(dhr.toString()==='12')?dhr:(dhr*1)+12;
                        }
                        $scope.dInfo.dObj.setHours(dhr);
                    }
                    $scope.fillHours();
                    $scope.setDisplay();
                };
                $scope.reset=function(){
                   $scope.ngModel=$scope.original;
                    $scope.setDisplay();
                };
                $scope.clear=function(){
                    $scope.ngModel='';
                    $scope.displayDate='Click to Select Date';
                    $scope.calToggle();
                }
                //Sets the model's date object and the 'display' values
                $scope.setDisplay=function(flg){
                    //if it's time do the timey wimey wibbly wobbly stuff
                    if($attrs.isTime){
                        if($scope.ngModel!==''){
                            $scope.displayDate=$scope.dInfo.dObj.toLocaleString();
                        }else{
                                if(!flg){
                                    $scope.ngModel=$scope.dInfo.dObj;
                                }
                                $scope.displayDate='Click to Select Date';

                        }
                        var mins=$scope.dInfo.dObj.getMinutes();
                        var hrs=$scope.dInfo.dObj.getHours();
                        $scope.dInfo.minute=(mins<10)?'0'+mins:mins;
                        var hr=(timePreference===1)?hrs:(hrs>12&&hrs!==0)?hrs-12:(hrs===0)?12:hrs;
                        $scope.dInfo.hour=(hr<10)?'0'+hr:hr;
                        if($scope.dInfo.ampm===''){
                            $scope.dInfo.ampm=($scope.dInfo.dObj.getHours()>12)?'PM':'AM';
                        }
                        if($scope.ngModel.setHours){
                            $scope.ngModel.setHours($scope.dInfo.dObj.getHours());
                            $scope.ngModel.setMinutes($scope.dInfo.dObj.getMinutes());
                        }else{
                            $scope.displayDate='Click to Select Date';
                        }


                    }else{
                        if($scope.ngModel!==''){
                            $scope.displayDate=$scope.dInfo.dObj.toLocaleDateString();
                        }else{
                            if(!flg){
                                $scope.ngModel=$scope.dInfo.dObj;
                            }
                            $scope.displayDate='Click to Select Date';
                        }
                    }
                    $scope.dInfo.year=$scope.dInfo.dObj.getFullYear();
                    $scope.dInfo.date=$scope.dInfo.dObj.getDate();
                    $scope.dInfo.month=$scope.dInfo.dObj.getMonth();

                    if($scope.ngModel.setFullYear){
                        $scope.ngModel.setFullYear(
                            $scope.dInfo.dObj.getFullYear(),
                            $scope.dInfo.dObj.getMonth(),
                            $scope.dInfo.dObj.getDate());
                    }

                };
                $scope.setDisplay(true);
			},
			replace: false,
			templateUrl: 'template/date_picker.tpl.htm'
		};
	}
]);