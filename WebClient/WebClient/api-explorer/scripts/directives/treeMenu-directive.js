window.angular.module('prismApp.common.directives.treeMenu', [])
	.directive('treeMenu', ['$timeout', function (timer) {
		'use strict';
		return {
			restrict: 'C',
			link: function (scope, element) {
				function initTreeMenu() {
					var ul = element[0].getElementsByTagName('ul'), i,
						links = element[0].getElementsByTagName('li'),
						clickHandler = function () {
                            var bros=this.parentNode.parentNode.getElementsByTagName('ul');
                            var links = this.parentNode.parentNode.getElementsByTagName('a');
                            for(var i=0;i<links.length;i++){
                                if(links[i].className.indexOf('glyphicon')!==-1){
                                    links[i].className='glyphicon glyphicon-folder-close tree-folder';
                                }
                            }
                            for(var i=0;i<bros.length;i++){

                                if(bros[i]!==this.parentNode.getElementsByTagName('ul')[0]){
                                    bros[i].style.display='';
                                }
                            }
							var ul = this.parentNode.getElementsByTagName('ul')[0];
							ul.style.display = (ul.style.display === '') ? 'block' : '';
							this.className = (ul.style.display === '') ? 'glyphicon glyphicon-folder-close tree-folder' : 'glyphicon glyphicon-folder-open tree-folder';
						};

					for (i = 0; i < ul.length; i++) {
						ul.className += ' nav nav-list';
					}

					element[0].className += ' nav nav-list';

					for (i = 0; i < links.length; i++) {

						if (links[i].getElementsByTagName('li').length !== 0) {

							var a = links[i].getElementsByTagName('a')[0];
                            a.className='glyphicon glyphicon-folder-close tree-folder'
							a.href = ' javascript:void(0);'.replace(' ','');
							a.onclick = clickHandler;
						}
					}
				}

				timer(function () {
					initTreeMenu();
				}, 0);
			}
		};
	}]);
