if (window.location.hash === '') {
  window.location.hash = '/services';
}
window.prismApp = angular.module(
  'prismApp',
  [
    'prismApp.common.controllers',
    'prismApp.common.services',
    'prismApp.common.directives',
    'prismLogin',
    'ui.router',
    'ui.bootstrap',
    'ui.grid.resizeColumns',
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.cellNav',
    'ui.grid.autoResize',
    'ngSanitize',
    'ngAnimate',
    'jsonFormatter',
    'prismApp.factories.Logger'
  ],
  null
);

//prismApp.pos.controllers module definition and its dependencies
window.angular.module(
  'prismApp.common.controllers',
  [
    'prismApp.controllers.base_ctrl',
    'prismApp.controllers.ac_services_ctrl',
    'prismApp.common.controllers.merchantware_ctrl',
    'prismApp.controllers.rpc_ctrl',
    'prismApp.common.controllers.globalblue_ctrl',
    'prismApp.common.controllers.raw_http_ctrl'
  ],
  null
);

//prismApp.common.services module definition and its dependencies
window.angular.module(
  'prismApp.common.services',
  [
    'prismApp.common.services.prismSessionInfo',
    'prismApp.common.services.PrismUtilities',
    'prismApp.common.services.loginProvider',
    'prismApp.common.services.EFT',
    'prismApp.services.RPServices',
    'prismApp.common.services.DateInput',
    'prismApp.common.services.LoggingService'
  ],
  null
);
window.angular.module(
  'prismApp.common.directives',
  [
    'prismApp.common.directives.treeMenu',
    'prismApp.common.directives.dateinput'
  ],
  null
);

window.prismApp.config([
  '$stateProvider',
  '$httpProvider',
  '$locationProvider',
  '$provide',
  'JSONFormatterConfigProvider',
  function(
    $stateProvider,
    $httpProvider,
    $locationProvider,
    $provide,
    JSONFormatterConfigProvider
  ) {
    'use strict';

    //suppress promise warning from 1.6 update
    //$qProvider.errorOnUnhandledRejections(false);
    $provide.decorator('$exceptionHandler', [
      '$delegate',
      '$injector',
      function($delegate, $injector) {
        return function(exception, cause) {
          var exceptionsToIgnore = [
            'Possibly unhandled rejection: $uibUnscheduledDestruction',
            'Possibly unhandled rejection: backdrop click',
            'Possibly unhandled rejection: cancel',
            'Possibly unhandled rejection: canceled',
            'Possibly unhandled rejection: escape key press',
            'Possibly unhandled rejection: undefined'
          ];
          if (exceptionsToIgnore.indexOf(exception) >= 0) {
            return;
          }
          $delegate(exception, cause);
        };
      }
    ]);

    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('');

    $httpProvider.defaults.headers.post = {
      'Content-Type': 'application/json; charset=UTF-8'
    };
    $httpProvider.defaults.headers.put = {
      'Content-Type': 'application/json; charset=UTF-8'
    };
    $httpProvider.defaults.headers.delete = {
      'Content-Type': 'application/json; charset=UTF-8'
    };

    // Enable the hover preview feature
    JSONFormatterConfigProvider.hoverPreviewEnabled = true;

    $stateProvider
      .state('services', {
        url: '/services',
        templateUrl: 'views/ac-services-partial.htm',
        controller: 'ac_services_ctrl'
      })
      .state('service', {
        url: '/services?resource',
        templateUrl: 'views/ac-services-partial.htm',
        controller: 'ac_services_ctrl'
      })
      // .state('eft_mw', {
      //   url: '/eft/mw/:tab',
      //   templateUrl: 'views/merchantware-partial.htm',
      //   controller: 'merchantware_ctrl'
      // })
      // .state('eft_gb', {
      //   url: '/eft/gb',
      //   templateUrl: 'views/globalblue-partial.htm'
      // })
      .state('raw', {
        url: '/raw',
        templateUrl: 'views/raw-http-partial.htm'
      });
  }
]);
