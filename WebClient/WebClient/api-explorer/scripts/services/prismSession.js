/*
 Service: prismSessionInfo
 Provides a single source for accessing the session information for the currently logged in session. The session
 information is contained in a JSON object and can be accessed using standard notation.

 Usage:
 > var X = prismSessionInfo.get().subsidiarysid;

 Returns:
 A JSON object that contains the session information for the currently logged in session.
 */
window.angular.module('prismApp.common.services.prismSessionInfo', [])
    .factory('prismSessionInfo', [
        function () {
            'use strict';
            if (!sessionStorage.getItem('session')) {
                sessionStorage.setItem('session', JSON.stringify(
                    {
                        workstation: 'webclient',
                        application: 'RProPrismWeb'
                    }
                ));
            }

            var permissionCollection = [];

            function getSession() {
                return angular.fromJson(sessionStorage.getItem('session'));
            }

            function setSession(newSession) {
                newSession.application = 'RProPrismWeb';
                sessionStorage.setItem('session', JSON.stringify(newSession));
            }

            function checkForPermissionExistence(permissionName){
                var returnValue, permissionValue;

                try{
                    permissionValue = getSession().permissions[permissionName.toLowerCase()];
                    returnValue = permissionValue === 'ALLOW' || permissionValue === 'OVERRIDE';
                } catch(e){
                    returnValue = false;
                }

                return returnValue;
            }

            function needsOverride(permissionName){
                var returnValue, permissionValue = getSession().permissions[permissionName.toLowerCase()];
                returnValue = permissionValue  === 'OVERRIDE';
                return returnValue;
            }

            function collectPermission(permName){
                if(permissionCollection.indexOf(permName) === -1){
                    permissionCollection.push(permName);
                }
            }

            function clearPermissionCollection(){
                permissionCollection = [];
            }

            function getPermissionsCollection(){
                var permissionsToReturn = permissionCollection;
                permissionCollection = [];
                return permissionsToReturn;
            }

            return {
                get: getSession,
                set: setSession,
                permissions:{
                    isAllowed:checkForPermissionExistence,
                    needsOverride:needsOverride,
                    collectPermission: collectPermission,
                    getCollectedPermissions:getPermissionsCollection,
                    clearCollection: clearPermissionCollection
                }
            };
        }]);
