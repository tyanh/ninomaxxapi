/**
 * @ngdoc service
 * @name prismApp.common.services.loginProvider
 * @description
 * Handles the login/logout and seating functions in Prism.
 */
window.angular.module('prismApp.common.services.loginProvider', ['prismLogin'])
    .factory('loginProvider', ['$prismLogin', function($prismLogin){
        'use strict';
        return $prismLogin();
    }]);

angular.module('prismLogin', ['ngResource', 'prismApp.common.services.prismSessionInfo']).
    factory('$prismLogin', ['$resource', '$http', '$q', 'prismSessionInfo', 'PrismUtilities',
        function($resource, $http, $q, prismSessionInfo, PrismUtilities){
            'use strict';

            function convertNonceToValidationCode(ANonce) {
                return ((parseInt((ANonce / 13)) % 99999) * 17);
            }

            function prismLoginResource(){
                var loginResource = $resource(
                    //no default url
                    '',
                    //no parameters
                    {},
                    //returning these methods
                    {
                        login:{},
                        sit:{},
                        stand:{},
                        logout:{}
                    }
                );

                /**
                 * @ngdoc method
                 * @name login
                 * @methodOf prismApp.common.services.loginProvider
                 * @description
                 *
                 * Attempts to log in the user with the provided credentials.
                 *
                 * @returns {Promise} Promise object as a result of the http requests.
                 *
                 * @params {Object} credentials - JSON object containing the user entered username and password in the
                 * following format: {'username':'entered username','password':'entered password'}
                 *
                 **/
                loginResource.login = function(credentials){
                    delete $http.defaults.headers.common['Auth-Session'];
                    sessionStorage.removeItem('PRISMAUTH');
                    var defObj = $q.defer();

                    //Call #1 - First step in the AUTH process - Get a token from the server
                    $http({ method: 'GET', url: '/v1/rest/auth' })
                        .then(function (data, status, headers) {
                            //In a successful call we should get a back a header called Auth-Nonce that has an integer value in it.

                            if (data.headers('Auth-Nonce')) {
                                //We have a populated header. We now perform a calculation on the value from the header and send that
                                //result back in a new header called Auth-Nonce-Response. The username and password that the visitor
                                //entered are passed in the params object and subsequently attached to the URL in a querystring.

                                //Call #2 - Second step in the AUTH process - Pass back the token and get a new one from the server
                                $http({ method: 'GET', url: '/v1/rest/auth', params: { 'usr': credentials.username, 'pwd': credentials.password }, headers: { 'Auth-Nonce-Response': convertNonceToValidationCode(data.headers('Auth-Nonce')) + '', 'Auth-Nonce': data.headers('Auth-Nonce')} })
                                    .then(function (data, status, headers) {
                                        //In a successful call we should get a header called Auth-Session that has a GUID value with the '-'
                                        //characters stripped out of it.

                                        if (data.headers('Auth-Session')) {
                                            //We have both a populated header. We now have everything we need to claim a seat in the system,
                                            //though at this point we will not do so.

                                            //Because the Auth-Session value needs to be passed back to the server in all future calls to
                                            //the system, we set the value into a sessionStorage (so that it expires as soon as the browser
                                            //is closed) so that we can get the value from any page in the app.

                                            sessionStorage.setItem('PRISMAUTH', data.headers('Auth-Session'));
                                            $http.defaults.headers.common['Auth-Session'] = sessionStorage.getItem('PRISMAUTH');
                                            //Call #3 - Store the session information in local storage
                                            $http({ method: 'GET', url: '/v1/rest/session' })
                                                .then(function (data) {
                                                    prismSessionInfo.set(PrismUtilities.responseParser(data)[0]);
                                                    defObj.resolve();
                                                }, function () {
                                                    //This is the error section for Call #3. We output the data and status to the page (if the fields
                                                    //are there) and show the login error message on the page (if it is on the page.)
                                                    defObj.reject({'error':'Session Info Failed to load.'});
                                                });
                                        }
                                        else {
                                            //We do not have a populated header so we need to return an error object
                                            defObj.reject({'error':'Unable to process your login, please contact your system administrator.'});
                                        }
                                    },function(){
                                        //This is the error section for Call #2. A status of something other than 200 was returned.
                                        //We need to return an error object.
                                        defObj.reject({'error':'Username or password was not correct.'});
                                    });
                            }
                            else {
                                //We do not have a populated header so we need to return an error object
                                defObj.reject({'error':'Unable to process your login, please contact your system administrator.'});
                            }
                        },function(){
                            //This is the error section for Call #1. A status of something other than 200 was returned.
                            //We need to return an error object
                            defObj.reject({'error':'Unable to process your login, please contact your system administrator.'});
                        });

                    return defObj.promise;
                };

                /**
                 * @ngdoc method
                 * @name sit
                 * @methodOf prismApp.common.services.loginProvider
                 * @description
                 *
                 * Attempts to claim a seat on the server.
                 *
                 * @returns {Promise} Promise object as a result of the http requests.
                 *
                 **/
                loginResource.sit = function(){
                    var defObj = $q.defer(),
                        tabletName = '';

                    var workstation = prismSessionInfo.get();
                    if(!workstation.workstation){
                        if(sessionStorage.getItem('tabletName')){
                            tabletName = sessionStorage.getItem('tabletName');
                        } else {
                            tabletName = 'webclient';
                        }
                        workstation.workstation = tabletName;
                        prismSessionInfo.set(workstation);
                    }
                    var sitObj = $http({ method: 'GET', url: '/v1/rest/sit', params: { 'ws': prismSessionInfo.get().workstation } });
                    sitObj.then(function () {
                        //We have successfully claimed a seat in the system.
                        //get the session after sitting
                        var ppObj = $http({ method: 'GET', url: '/v1/rest/session' });
                        ppObj.then(function(data){
                            prismSessionInfo.set(PrismUtilities.responseParser(data)[0]);
                            defObj.resolve({'success':true,'error':''});
                        });
                    }, function () {
                        //We were unable to claim a seat. We need to return an error object.
                        defObj.reject({'success':false,'error':'Unable to assign a seat. Please contact your system administrator.'});
                    });

                    return defObj.promise;
                };

                /**
                 * @ngdoc method
                 * @name stand
                 * @methodOf prismApp.common.services.loginProvider
                 * @description
                 *
                 * Attempts to release a seat on the server.
                 *
                 * @returns {Promise} Promise object as a result of the http requests.
                 *
                 **/
                loginResource.stand = function(){
                    var defObj = $q.defer();
                    //Tell the server that we are done with this seat
                    var stnd = $http({ method: 'GET', url: '/v1/rest/stand', params: { 'ws': prismSessionInfo.get().workstation }});
                    stnd.then(function () {
                        defObj.resolve();
                    }, function () {
                        //Something happened during the process. The error message needs to be given to the visitor.
                        defObj.reject({'error':'An error occurred while logging you out. Please contact your customer support representative for assistance.'});
                    });

                    return defObj.promise;
                };

                /**
                 * @ngdoc method
                 * @name stand
                 * @methodOf prismApp.common.services.loginProvider
                 * @description
                 *
                 * Ends the current login session erases any values stored in sessionStorage.
                 *
                 * @returns {Promise} Promise object as a result of the http requests.
                 *
                 **/
                loginResource.logout = function(){
                    //remove the auth value
                    delete $http.defaults.headers.common['Auth-Session'];
                    sessionStorage.removeItem('PRISMAUTH');
                    //remove the session info
                    sessionStorage.removeItem('session');
                    prismSessionInfo.set({workstation:'webclient'});
                };

                /**
                 * @ngdoc method
                 * @name remoteLogin
                 * @methodOf prismApp.common.services.loginProvider
                 * @description
                 *
                 * Attempts to log a user into a remote server with the provided credentials.
                 *
                 * @returns {Promise} Promise object as a result of the http requests.
                 *
                 * @params {String} server - String containing a valid hostname or ip address (this is the server to
                 * authenticate with.)
                 * @params {Object} credentials - JSON object containing the user entered username and password in the
                 * following format: {'usr':'entered username','pwd':'entered password'}
                 * @params {Boolean} isV9 - Boolean value indicating if this login attempt will be against a Retail Pro 9
                 * server.
                 *
                 **/
                loginResource.remoteLogin = function (server, credentials, isV9) {
                    //Create the resource, begin querying that resource and construct a defer object to return to the caller

                    var endpoint =  isV9 ? 'authv9' : 'auth',
                        header = isV9 ? 'Auth-Session-v9' : 'Auth-Session',
                        LoginResource = $resource(window.location.protocol + '//' + server + '/v1/rest/' + endpoint, {}, { query: { method: 'GET' } }),
                        deferedLogin = $q.defer();

                    //remove any existing Auth-Session headers
                    delete $http.defaults.headers.common['Auth-Session-v9'];
                    delete $http.defaults.headers.common['Auth-Session'];

                    LoginResource.query(function (auth, authResponseHeaders) {
                        auth.$promise.then(function () {
                            //if we successfully received an Auth-Nonce header
                            if (authResponseHeaders('Auth-Nonce').length > 0) {
                                //set the Auth-Nonce-Response and Auth-Nonce headers
                                $http.defaults.headers.common['Auth-Nonce-Response'] = convertNonceToValidationCode(authResponseHeaders('Auth-Nonce'));
                                $http.defaults.headers.common['Auth-Nonce'] = authResponseHeaders('Auth-Nonce');

                                //query the authentication resource again and pass along the provided credentials
                                LoginResource.query(credentials, function (innerAuth, innerAuthResponseHeaders) {
                                    console.log(innerAuthResponseHeaders(header));

                                    innerAuth.$promise.then(function () {
                                        //if the Auth-Session header exist and it is not empty
                                        if (innerAuthResponseHeaders(header) && innerAuthResponseHeaders(header).length > 0) {

                                            //add the Auth-Session received to sessionStorage using the server value with AUTH_ prepended as the key
                                            sessionStorage.setItem(isV9 ? 'AUTHV9_' + server : 'AUTH_' + server, innerAuthResponseHeaders(header));
                                            //resolve the promise as we have successfully logged into the remote server
                                            deferedLogin.resolve('Authentication Success');
                                        } else {
                                            //something went wrong in the auth process, there was no Auth-Session or it was empty
                                            deferedLogin.resolve('Authentication Failed');
                                        }
                                    }, authenticationErrorCallback);

                                    //clear these headers
                                    delete $http.defaults.headers.common['Auth-Nonce-Response'];
                                    delete $http.defaults.headers.common['Auth-Nonce'];
                                });


                            }
                        }, authenticationErrorCallback);
                    });

                    function authenticationErrorCallback(response) {
                        var rejectMessage = (response.data.length > 0 ) ? response.data : 'Authentication Failed';
                        deferedLogin.reject(rejectMessage);
                    }


                    return deferedLogin.promise;
                };

                /**
                 * @ngdoc method
                 * @name remoteLogout
                 * @methodOf prismApp.common.services.loginProvider
                 * @description
                 *
                 * Logs out the logged in user from a remote server.
                 *
                 * @params {String} server - String containing a valid hostname or ip address (this is the server that we
                 * authenticate with.)
                 *
                 **/
                loginResource.remoteLogout = function(server){
                    sessionStorage.removeItem('AUTH_' + server);
                };

                return loginResource;
            }

            return prismLoginResource;
        }]);
