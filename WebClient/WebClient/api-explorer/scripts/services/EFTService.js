/*
 Service: EFT
 Handles the EFT CRUD operations for all card providers in Prism. Uses an Angular Resource to abstract away the details
 of the http requests. Each method hands back a standardized JSON data object regardless of which provider is used.

 Injection:
 prismApp.controller('myController', ['$scope', 'EFT', function($scope, EFT){}]);
 */
window.angular.module('prismApp.common.services.EFT', [])
	.factory('EFT', ['$resource', '$q', 'PrismUtilities', 'Logger', 'LoggingService',
    function($resource, $q, PrismUtilities, Logger, LoggingService) {
		'use strict';

        var CardService = 'MerchantWare';

        var ApplicationName = '';

          //MerchantWare settings
        var eftmwAPI = '/v1/rest/eftmw/:sid';

        var mwResource = $resource(eftmwAPI, {}, {query: { method: 'GET'},create: { method: 'POST'}, update: { method: 'PUT'}, remove: { method: 'DELETE'}});

		var mwCEDAPI = '/v1/rest/ced/:sid';

		var CEDResource = $resource(mwCEDAPI, {}, {query:{method:'GET'},create:{method:'POST'}, update: { method: 'PUT'}});

        //main credit functions
        function initiateCreditTransaction(documentSid, actionType, amount, useGeniusDevice, forceDupeOverride, tokenNumber, swipeData, keyedData, webRedirect){
            var params = {sid:''};
            var CreditInitiateResource;
            var defObj = $q.defer();
			var logger = new Logger();
			logger.label = 'EFT: Initiate Credit Transaction';

            switch (CardService){
                default:
                    //MerchantWare
                    var initData = {eftmws:[
                        {eftmw:{
                            'origin_application':{_value_:ApplicationName},
                            'document_sid':{_value_:documentSid},
                            'actiontype':{_value_:actionType},
                            'usegeniusdevice':{_value_:useGeniusDevice},
                            'forcedupcheck':{_value_:forceDupeOverride}}
                        }]};
                    if(amount){
                        initData.eftmws[0].eftmw.amount = {_value_:amount};
                    }
                    if(tokenNumber){
                        initData.eftmws[0].eftmw.token = {_value_:tokenNumber};
                    }
                    if(swipeData){
                        initData.eftmws[0].eftmw.rawtrackdata = {_value_:swipeData};
                    }
                    if(keyedData){
                        initData.eftmws[0].eftmw.cardnumber = {_value_:keyedData};
                    }
                    if(webRedirect){
                        initData.eftmws[0].eftmw.web_redirect = {_value_:webRedirect};
                    }

                    mwResource = $resource(eftmwAPI, params, {query: { method: 'GET'},create: { method: 'POST'}, update: { method: 'PUT'}, remove: { method: 'DELETE'}});

                    CreditInitiateResource = mwResource.create(initData);
                    CreditInitiateResource.$promise.then(function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'POST';
						logger.data.sent = initData;
						var initiateResponse = PrismUtilities.responseParser(data);
						logger.data.received = initiateResponse;
						LoggingService.LogData.push(logger.output());
                        defObj.resolve(buildEFTMWReturnObject(initiateResponse));
                    }, function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'POST';
						logger.data.sent = initData;
						logger.data.received = data.data;
						LoggingService.LogData.push(logger.output());
                        defObj.reject(data);
                    });
                    break;
            }

            return defObj.promise;
        }

        function getCreditTransactionStatus(transactionSid, columns, filters){
            var params = {sid:transactionSid};
            var CreditStatusResource;
            var defObj = $q.defer();
			var logger = new Logger();
			logger.label = 'EFT: Credit Transaction Status';

            if(columns){
                params = angular.extend(params, { cols: columns });
            }
            if(filters){
				PrismUtilities.RPSFilters.addMultiple(filters);
                params = angular.extend(params, { filter: PrismUtilities.RPSFilters.getFilter()});
            }

            switch (CardService){
                default:
                    //MerchantWare
                    CreditStatusResource = mwResource.query(params);
                    CreditStatusResource.$promise.then(function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'GET';
						logger.data.sent = '';
						var approvalstatus = PrismUtilities.responseParser(data);
						logger.data.received = approvalstatus;
						LoggingService.LogData.push(logger.output());
                        defObj.resolve(buildEFTMWReturnObject(approvalstatus));
                    }, function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'GET';
						logger.data.sent = '';
						logger.data.received = data.data;
						LoggingService.LogData.push(logger.output());
                        defObj.reject(data);
                    });
                break;
            }

            return defObj.promise;
        }

        function updateCreditTransaction(requestSid, updateData){
            if(!updateData.row_version){
                throw new Error('EFT Service - Credit - Update - row_version must not be empty!');
            }
            var params = {sid:requestSid};
            var CreditUpdateResource;
            var defObj = $q.defer();
			var logger = new Logger();
			logger.label = 'EFT: Update Credit Transaction';

            switch (CardService){
                default:
                    //MerchantWare
					PrismUtilities.RPSFilters.addElement('row_version', PrismUtilities.RPSFilters.operators.equal, updateData.row_version);
                    params = angular.extend(params, { filter: PrismUtilities.RPSFilters.getFilter() });

                    mwResource = $resource(eftmwAPI, params, {query: { method: 'GET'},create: { method: 'POST'}, update: { method: 'PUT'}, remove: { method: 'DELETE'}});
                    var mwData = {eftmws:[{eftmw:{}}]};
                    if(updateData.sigcapdata){
                        mwData.eftmws[0].eftmw.sigcapdata = {_value_:updateData.sigcapdata};
                    }
                    if(updateData.web_redirect){
                        mwData.eftmws[0].eftmw.web_redirect = {_value_:updateData.web_redirect};
                    }
                    CreditUpdateResource = mwResource.update(mwData);
                    CreditUpdateResource.$promise.then(function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'PUT';
						logger.data.sent = updateData;
						var approvalstatus = PrismUtilities.responseParser(data);
						logger.data.received = approvalstatus;
						LoggingService.LogData.push(logger.output());
                        defObj.resolve(buildEFTMWReturnObject(approvalstatus));
                    }, function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'PUT';
						logger.data.sent = updateData;
						logger.data.received = data.data;
						LoggingService.LogData.push(logger.output());
                        defObj.reject(data);
                    });
                    break;
            }

            return defObj.promise;
        }

		function deviceCreditTransaction(transactionKey, deviceID, transactionSid){
			if(!transactionKey){
				throw new Error('EFT Service - Credit - Device Transaction - transactionKey must not be empty!');
			}
			if(!deviceID){
				throw new Error('EFT Service - Credit - Device Transaction - deviceID must not be empty!');
			}
			if(!transactionSid){
				throw new Error('EFT Serivce - Credit - Device Transaction - transactionSid must not be empty!');
			}
			var params = {sid:deviceID};
			var defObj = $q.defer();
			var CreditDeviceResource;
			var logger = new Logger();
			logger.label = 'EFT: Initiate Device';

			switch (CardService){
				default:
					//MerchantWare
					CEDResource = $resource(mwCEDAPI, params, {query:{method:'GET'},create:{method:'POST'}});
					var CEDdata = {params:[{param:{transportkey:{_value_:transactionKey},sid:{_value_:transactionSid}}}]};
					CreditDeviceResource = CEDResource.create(CEDdata);
					CreditDeviceResource.$promise.then(function(){
						logger.data.url = mwCEDAPI;
						logger.data.method = 'POST';
						logger.data.sent = CEDdata;
						logger.data.received = '';
						LoggingService.LogData.push(logger.output());
						defObj.resolve();
					}, function(data){
						logger.data.url = mwCEDAPI;
						logger.data.method = 'POST';
						logger.data.sent = CEDdata;
						logger.data.received = data.data;
						LoggingService.LogData.push(logger.output());
						defObj.reject(data);
					});
			}

			return defObj.promise;
		}

        //main debit functions
        function initiateDebitTransaction(){

        }

        function getDebitTransactionStatus(){

        }

        function updateDebitTransaction(){

        }

		function deviceDebitTransaction(){

		}

        //main gift functions
        /*
            Method: Gift.Initiate
            This method is used to initiate *all* Gift Card transactions.

            Parameters:
            documentSid - {String}(REQUIRED) The SID of the document that this Gift Card amount will be applied to.
            giftAction - {String}(REQUIRED) The action to be performed. Acceptable values vary by processor. See each
            processor for details.
            giftAmount - {String}(REQUIRED) The amount of the transaction you are trying to get authorized.
            swipeData - {String}(OPTIONAL) The raw track data from the encrypted card reader. Either this parameter or
            the keyedData parameter must be populated. The default value is null.
            keyedData - {String}(OPTIONAL) The hand-keyed Gift Card number. Either this parameter or the swipeData
            parameter must be populated. The default value is null.
            useDevice - {Boolean}(OPTIONAL) Used to tell the initialization process that it can expect the card
            processors device to complete the transaction. The default value is false.
            forceDupeOverride - {Boolean}(OPTIONAL) Used to tell the initialization process override checking for
            duplicate transactions. The default value is false.
            transactionIdentifier - {String}(OPTIONAL) Used in referencing an existing transaction for further action.
            The default value is null.

            Usage:
            > var giftInit = EFT.Gift.Initiate(docSid, giftAction, giftAmount, swipeData, keyedData, useDevice, forceDupeOverride, transactionIdentifier);
            > giftInit.then(function(initResult){
            >     //actions for when the transaction is successful
            > }, function(initFailure){
            >     //actions for when the transaction is not successful
            > });

            See Also:
            <MerchantWare>
         */
        function initiateGiftTransaction(documentSid, actionType, amount, swipeData, keyedData, useDevice, forceDupeOverride, transactionIdentifier){
            var logger = new Logger();
			logger.label = 'EFT: Initiate Gift Transaction';
            //choose which service API and Resource to use
            var GiftResource, params;
            //prepare the outer promise
            var defObg = $q.defer();

            switch (CardService){
                default:
                    //MerchantWare
                    var initData = {eftmws:[
                        {eftmw:{
                            'origin_application':{_value_:ApplicationName},
                            'document_sid':{_value_:documentSid},
                            'actiontype':{_value_:actionType},
                            'usegeniusdevice':{_value_:useDevice},
                            'forceDupeOverride':{_value_:forceDupeOverride}}
                        }]};
                    if(amount){
                        initData.eftmws[0].eftmw.amount = {_value_:amount};
                    }
                    if(transactionIdentifier){
                        initData.eftmws[0].eftmw.token = {_value_:transactionIdentifier};
                    }
                    if(swipeData){
                        initData.eftmws[0].eftmw.rawtrackdata = {_value_:swipeData};
                    }
                    if(keyedData){
                        initData.eftmws[0].eftmw.cardnumber = {_value_:keyedData};
                    }

                    params = {sid:''};

                    mwResource = $resource(eftmwAPI, params, {query: { method: 'GET'},create: { method: 'POST'}, update: { method: 'PUT'}, remove: { method: 'DELETE'}});

                    GiftResource = mwResource.create(initData);

                    //run the transaction and resolve the outer promise
                    GiftResource.$promise.then(function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'POST';
						logger.data.sent = initData;
                        var giftResponse = PrismUtilities.responseParser(data);
						logger.data.received = giftResponse;
						LoggingService.LogData.push(logger.output());
                        defObg.resolve(buildEFTMWReturnObject(giftResponse));
                    }, function(data){
						logger.data.url = eftmwAPI;
						logger.data.method = 'POST';
						logger.data.sent = initData;
						logger.data.received = data.data;
						LoggingService.LogData.push(logger.output());
                        defObg.reject(data);
                    });
                    break;
            }

            //return the outer promise
            return defObg.promise;
        }

		function deviceGiftTransaction(){

		}

        /*
            Method: SetProvider
            This method must be called *before* you attempt to use the service for processing transactions of *ANY* type.

            Parameters:
            cardServiceProvider - The name of the gift/credit card processor you want to use. Acceptable values are:
            'MerchantWare'.
            applicationName - The name of the workstation running the prism application. Generally retrieved from the
            <prismSessionInfo> values.

            Usage:
            > EFT.SetProvider(cardServiceProviderName, prismSessionInfo.get().application._value_);

            See Also:
            <prismSessionInfo>
         */
        function setProvider(cardServiceProvider, applicationName){
            CardService = cardServiceProvider;
            ApplicationName = applicationName;
        }

		function deviceList(){
			var defObj = $q.defer();
			var DeviceResource;
			var logger = new Logger();
			logger.label = 'EFT: Device List';

			switch (CardService){
				default:
					//MerchantWare
					CEDResource = $resource(mwCEDAPI, {}, {query:{method:'GET'},create:{method:'POST'}});
					DeviceResource = CEDResource.query();
					DeviceResource.$promise.then(function(data){
						logger.data.url = mwCEDAPI;
						logger.data.method = 'GET';
						logger.data.sent = '';
						var deviceList = PrismUtilities.responseParser(data);
						logger.data.received = deviceList;
						LoggingService.LogData.push(logger.output());
						if(deviceList.ceds.length === 0){
							defObj.reject(deviceList);
						} else {
							defObj.resolve(buildEFTMWDeviceListObject(deviceList));
						}
					}, function(data){
						logger.data.url = mwCEDAPI;
						logger.data.method = 'GET';
						logger.data.sent = '';
						logger.data.received = data.data;
						LoggingService.LogData.push(logger.output());
						defObj.reject(data);
					});
			}

			return defObj.promise;
		}

        function buildEFTMWReturnObject(data){
            var ReturnObject = {
                actiontype:'',
                amount:'',
                approvalstatus:'',
                authcode:'',
                avsresult:'',
                balance:'',
                card_type:'',
                card_type_desc:'',
                cardnumber:'',
                created_by:'',
                created_datetime:'',
                customercode:'',
                cvvresult:'',
                document_sid:'',
                errormessage:'',
                expirationdate:'',
                forcedupcheck:'',
                merchanttxnid:'',
                miscmessage:'',
                modified_by:'',
                modified_datetime:'',
                paymenttype:'',
                post_date:'',
                responsemessage:'',
                row_version:'',
                sid:'',
                sigcapdata:'',
                tax_amount:'',
                tenant_sid:'',
				tendertype:'',
                token:'',
                transactiondate:'',
                transactiontype:'',
                transportkey:'',
                usegeniusdevice:'',
                validationkey:'',
                webredirect:''
            };

            if(data.eftmws[0].eftmw.actiontype){
                ReturnObject.actiontype = data.eftmws[0].eftmw.actiontype._value_;
            }
            if(data.eftmws[0].eftmw.amount){
                ReturnObject.amount = data.eftmws[0].eftmw.amount._value_;
            }
            if(data.eftmws[0].eftmw.approvalstatus){
                ReturnObject.approvalstatus = data.eftmws[0].eftmw.approvalstatus._value_;
            }
            if(data.eftmws[0].eftmw.authcode){
                ReturnObject.authcode = data.eftmws[0].eftmw.authcode._value_;
            }
            if(data.eftmws[0].eftmw.avsresult){
                ReturnObject.avsresult = data.eftmws[0].eftmw.avsresult._value_;
            }
            if(data.eftmws[0].eftmw.balance){
                ReturnObject.balance = data.eftmws[0].eftmw.balance._value_;
            }
            if(data.eftmws[0].eftmw.card_type){
                ReturnObject.card_type = data.eftmws[0].eftmw.card_type._value_;
            }
            if(data.eftmws[0].eftmw.card_type_desc){
                ReturnObject.card_type_desc = data.eftmws[0].eftmw.card_type_desc._value_;
            }
            if(data.eftmws[0].eftmw.cardnumber){
                ReturnObject.cardnumber = data.eftmws[0].eftmw.cardnumber._value_;
            }
            if(data.eftmws[0].eftmw.created_by){
                ReturnObject.created_by = data.eftmws[0].eftmw.created_by._value_;
            }
            if(data.eftmws[0].eftmw.created_datetime){
                ReturnObject.created_datetime = data.eftmws[0].eftmw.created_datetime._value_;
            }
            if(data.eftmws[0].eftmw.customercode){
                ReturnObject.customercode = data.eftmws[0].eftmw.customercode._value_;
            }
            if(data.eftmws[0].eftmw.cvvresult){
                ReturnObject.cvvresult = data.eftmws[0].eftmw.cvvresult._value_;
            }
            if(data.eftmws[0].eftmw.document_sid){
                ReturnObject.document_sid = data.eftmws[0].eftmw.document_sid._value_;
            }
            if(data.eftmws[0].eftmw.errormessage){
                ReturnObject.errormessage = data.eftmws[0].eftmw.errormessage._value_;
            }
            if(data.eftmws[0].eftmw.expirationdate){
                ReturnObject.expirationdate = data.eftmws[0].eftmw.expirationdate._value_;
            }
            if(data.eftmws[0].eftmw.forcedupcheck){
                ReturnObject.forcedupcheck = data.eftmws[0].eftmw.forcedupcheck._value_;
            }
            if(data.eftmws[0].eftmw.merchanttxnid){
                ReturnObject.merchanttxnid = data.eftmws[0].eftmw.merchanttxnid._value_;
            }
            if(data.eftmws[0].eftmw.miscmessage){
                ReturnObject.miscmessage = data.eftmws[0].eftmw.miscmessage._value_;
            }
            if(data.eftmws[0].eftmw.modified_by){
                ReturnObject.modified_by = data.eftmws[0].eftmw.modified_by._value_;
            }
            if(data.eftmws[0].eftmw.modified_datetime){
                ReturnObject.modified_datetime = data.eftmws[0].eftmw.modified_datetime._value_;
            }
            if(data.eftmws[0].eftmw.paymenttype){
                ReturnObject.paymenttype = data.eftmws[0].eftmw.paymenttype._value_;
            }
            if(data.eftmws[0].eftmw.post_date){
                ReturnObject.post_date = data.eftmws[0].eftmw.post_date._value_;
            }
            if(data.eftmws[0].eftmw.responsemessage){
                ReturnObject.responsemessage = data.eftmws[0].eftmw.responsemessage._value_;
            }
            if(data.eftmws[0].eftmw.row_version){
                ReturnObject.row_version = data.eftmws[0].eftmw.row_version._value_;
            }
            if(data.eftmws[0].eftmw.sid){
                ReturnObject.sid = data.eftmws[0].eftmw.sid._value_;
            }
            if(data.eftmws[0].eftmw.sigcapdata){
                ReturnObject.sigcapdata = data.eftmws[0].eftmw.sigcapdata._value_;
            }
            if(data.eftmws[0].eftmw.tax_amount){
                ReturnObject.tax_amount = data.eftmws[0].eftmw.tax_amount._value_;
            }
            if(data.eftmws[0].eftmw.tenant_sid){
                ReturnObject.tenant_sid = data.eftmws[0].eftmw.tenant_sid._value_;
            }
			if(data.eftmws[0].eftmw.tendertype){
				ReturnObject.tendertype = data.eftmws[0].eftmw.tendertype._value_;
			}
            if(data.eftmws[0].eftmw.token){
                ReturnObject.token = data.eftmws[0].eftmw.token._value_;
            }
            if(data.eftmws[0].eftmw.transactiondate){
                ReturnObject.transactiondate = data.eftmws[0].eftmw.transactiondate._value_;
            }
            if(data.eftmws[0].eftmw.transactiontype){
                ReturnObject.transactiontype = data.eftmws[0].eftmw.transactiontype._value_;
            }
            if(data.eftmws[0].eftmw.transportkey){
                ReturnObject.transportkey = data.eftmws[0].eftmw.transportkey._value_;
            }
            if(data.eftmws[0].eftmw.usegeniusdevice){
                ReturnObject.usegeniusdevice = data.eftmws[0].eftmw.usegeniusdevice._value_;
            }
            if(data.eftmws[0].eftmw.validationkey){
                ReturnObject.validationkey = data.eftmws[0].eftmw.validationkey._value_;
            }
            if(data.eftmws[0].eftmw.web_redirect){
                ReturnObject.webredirect = data.eftmws[0].eftmw.web_redirect._value_;
            }

            return ReturnObject;
        }

		function buildEFTMWDeviceListObject(data){
			var deviceData = {
				ipaddress:'',
				port:'',
				name:'',
				sid:''
			};

			if(data.ceds[0].ced.ipaddr){
				deviceData.ipaddress = data.ceds[0].ced.ipaddr._value_;
			}
			if(data.ceds[0].ced.port){
				deviceData.port = data.ceds[0].ced.port._value_;
			}
			if(data.ceds[0].ced.name){
				deviceData.name = data.ceds[0].ced.name._value_;
			}
			if(data.ceds[0].ced.sid){
				deviceData.sid = data.ceds[0].ced.sid._value_;
			}

			return deviceData;
		}

		function cancelRemoteDevice(deviceID){
			if(!deviceID){
				throw new Error('EFT Service - Credit - Device Transaction - deviceID must not be empty!');
			}
			var params = {sid:deviceID};
			var defObj = $q.defer();
			var CreditDeviceResource;
			var logger = new Logger();
			logger.label = 'EFT: Cancel CED';

			switch (CardService){
				default:
					//MerchantWare
					CEDResource = $resource(mwCEDAPI + '/cancel', params, {query:{method:'GET'},create:{method:'POST'}, update: { method: 'PUT'}});
					CreditDeviceResource = CEDResource.update();
					CreditDeviceResource.$promise.then(function(){
						logger.data.url = mwCEDAPI + '/cancel';
						logger.data.method = 'PUT';
						logger.data.sent = params;
						logger.data.received = 'success';
						LoggingService.LogData.push(logger.output());
						defObj.resolve();
					}, function(data){
						logger.data.url = mwCEDAPI + '/cancel';
						logger.data.method = 'PUT';
						logger.data.sent = params;
						logger.data.received = data;
						LoggingService.LogData.push(logger.output());
						defObj.reject(data);
					});
			}

			return defObj.promise;
		}

        return {
            Credit:{
                Initiate:initiateCreditTransaction,
                Status:getCreditTransactionStatus,
                Update:updateCreditTransaction,
				RemoteDevice:deviceCreditTransaction,
				CancelRemoteDevice:cancelRemoteDevice
            },
            Debit:{
                Initiate:initiateDebitTransaction,
                Status:getDebitTransactionStatus,
                Update:updateDebitTransaction,
				RemoteDevice:deviceDebitTransaction,
				CancelRemoteDevice:cancelRemoteDevice
            },
            Gift:{
                Initiate:initiateGiftTransaction,
				RemoteDevice:deviceGiftTransaction,
				CancelRemoteDevice:cancelRemoteDevice
            },
			ListCardReaders:deviceList,
            SetProvider:setProvider
        };
    }
]);