window.angular.module('prismApp.services.RPServices', [])
    .factory('RPServices',['$http','$q',
        function($http,$q) {
            'use strict';
            // Changes XML to JSON
            return{


            get:function(){
                return $q.all([$http(
                    {
                        method: 'GET',
                        url: '../v1/rest/services',
                        headers:
                        {
                            'Auth-Session': sessionStorage.getItem('PRISMAUTH'),
                            'Accept': 'application/json'
                        }
                    }
                ),
                  $http(
                    {
                      method: 'POST',
                      url: '../api/common?action=services',
                      headers:
                      {
                        'Auth-Session': sessionStorage.getItem('PRISMAUTH'),
                        'Accept': 'application/json, version=2'
                      }
                    }
                  )
                ]).then(function(data){
                  var v1=data[0].data;
                  var v2=data[1].data.data;
                  v2.forEach(function(i){
                    v1.push(i);
                  });
                  return v1;
                },function(error){
                    angular.noop();
                });
            },
            xmlToJson:function(xml) {

                // Create the return object
                var obj = {};

                if (xml.nodeType === 1) { // element
                    // do attributes
                    if (xml.attributes.length > 0) {
                        obj['@attributes'] = {};
                        for (var j = 0; j < xml.attributes.length; j++) {
                            var attribute = xml.attributes.item(j);
                            obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
                        }
                    }
                } else if (xml.nodeType === 3) { // text
                    obj = xml.nodeValue;
                }

                // do children
                if (xml.hasChildNodes()) {
                    for(var i = 0; i < xml.childNodes.length; i++) {
                        var item = xml.childNodes.item(i);
                        var nodeName = item.nodeName;
                        if (typeof(obj[nodeName]) === 'undefined') {
                            obj[nodeName] = this.xmlToJson(item);
                        } else {
                            if (typeof(obj[nodeName].push) === 'undefined') {
                                var old = obj[nodeName];
                                obj[nodeName] = [];
                                obj[nodeName].push(old);
                            }
                            obj[nodeName].push(this.xmlToJson(item));
                        }
                    }
                }
                return obj;
            },
            getRPC:function(rpcName,ver,moduleName){
              if(ver==='2'){
                var version=',version=2';
                return $http(
                  {
                      method: 'GET',
                      url:'/api/' + moduleName + '?action=' + rpcName+'&introspection=true',
                      headers:
                      {
                          'Auth-Session': sessionStorage.getItem('PRISMAUTH'),
                          'Accept': 'application/json' + version
                      }
                  }
                ).then(function(data){
                    return data.data;
                },function(error){
                    angular.noop();
                });
              }else{
                var payload=
                   [
                        {
                            'params':{

                            },
                            'methodname':rpcName,
                            'introspection':true
                        }
                    ];

                return $http(
                    {
                        method: 'POST',
                        url:'../v1/rpc',
                        data:payload,
                        headers:
                        {
                            'Auth-Session': sessionStorage.getItem('PRISMAUTH'),
                            'Accept': 'application/json'
                        }
                    }
                );
              }
            },
            getMeta:function(url){
              var version='';
              if(url.indexOf('api')!==-1){
                version=',version=2';
              }
                return $http(
                    {
                        method: 'GET',
                        url:'../' + url,
                        headers:
                        {
                            'Auth-Session': sessionStorage.getItem('PRISMAUTH'),
                            'Accept': 'application/json' + version
                        }
                    }
                ).then(function(data){
                  if(url.indexOf('api')!==-1) {
                    return data.data;
                  }else{
                    return data;
                  }
                },function(error){
                    angular.noop();
                });
            }
        };
        }]);
