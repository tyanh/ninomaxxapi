window.angular.module('prismApp.common.services.loginMonitor', [])
	.factory('LoginMonitor', [
		function () {
			'use strict';

			var loginMessage = '';

			function clearMessage(){
				loginMessage = '';
			}

			function writeMessage(message, clearFirst){
				if(clearFirst){
					clearMessage();
				}
				loginMessage += message;
			}

			return {
				writeMessage: writeMessage,
				clearData: clearMessage,
				loginData: loginMessage
			};
		}]);