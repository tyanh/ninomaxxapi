/*jshint bitwise: false*/
window.angular.module('prismApp.common.services.PrismUtilities', [])
    .factory('PrismUtilities', ['$q',
        function ($q) {
            'use strict';

            var filterElements = [],
                filterOperators = {
                    equal:'eq',
                    notEqual:'ne',
                    lessThan:'lt',
                    greaterThan:'gt',
                    lessThanOrEqualTo:'le',
                    greaterThanOrEqualTo:'ge',
                    isNull:'nl',
                    isNotNull:'nn',
                    like:'lk',
                    disjunction:'ds',
                    none:'no'
                },
                pluginEvents = [],
                resourceMethods = {
                    query: { method: 'GET', isArray:true},
                    create: { method: 'POST', isArray:true},
                    update: { method: 'PUT', isArray:true},
                    remove: { method: 'DELETE'}
                };

            function generateGUID() {
                return '{xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx}'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16).toUpperCase();
                });
            }

            function objectArrayIndexOf(arr, val, prop){
                for(var i = 0, len = arr.length; i < len; i++) {
                    if (arr[i][prop] === val) {
                        return i;
                    }
                }
                return -1;
            }

            function makeArray(values){
                return Array.isArray(values) ? values : [values];
            }

            function replaceNonNumeric(value){
                var pattern = /[^0-9\.\-\,]/g;
                return value.replace(pattern, '');
            }

            function replaceSpaces(value){
                var pattern = /\s/g;
                return value.replace(pattern, '');
            }

            /*
             Function: responseParser
             The responseParser function is used to strip away the outer layer of {data} objects and return a value that is
             more readily usable.

             Parameters:

             data - A string containing a JSON object.

             Returns:

             The string with an outer layer of {data} removed from it.

             > var myData = {'data':{'myInnerData':'blah'}};
             > var myCleanData = responseParser(myData);
             > //myCleanData will equal {'myInnerData':'blah'}
             */
            function responseParser(data) {
                var dataObject = angular.fromJson(data);
                var output;
                if (dataObject.data) {
                    output = angular.fromJson(dataObject.data);
                } else {
                    output = angular.fromJson(data);
                }
                return output;
            }

            function nullOrEmpty(testValue){
                return !!((testValue === '' || testValue*1 === 0 || testValue === null || testValue === undefined));
            }

            function toBoolean(boolValue){
                if (boolValue !== undefined) {
                    return (boolValue.toString().toLowerCase() === 'true' || boolValue === 1 || boolValue.toString().toLowerCase() === 't');
                }
                return false;
            }

            function isNumber(n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            }

            //4/22/2014 Updated function to check for valid format of ISO 8061 date
            function isValidDate(dateValue){
                var pattern =/(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+)|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d)|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d)/;
                return pattern.test(dateValue);
            }

            function validateTotal(valuesArray, requiredTotal){
                if(valuesArray === undefined){
                    throw new Error('You must provide an array of values!');
                }

                if(requiredTotal === undefined){
                    throw new Error('You must provide a total to validate against!');
                }

                if(!isNumber(requiredTotal)){
                    throw new Error('The total to validate must be a number.');
                }

                var sumOfArray = valuesArray.reduce(
                    function(previousValue, currentValue){
                        return parseFloat(previousValue) + parseFloat(isNumber(currentValue) ? currentValue : 0);
                    }
                );

                return sumOfArray === parseFloat(requiredTotal);
            }

            function addFilterElement(field, operator, term){

                if(field === undefined || operator === undefined || (term === undefined && (operator.toUpperCase() !== 'NL' && operator.toUpperCase() !== 'NN'))){
                    throw new Error('All arguments are required when using addElement.');
                }

                filterElements.push({
                    field:field,
                    operator:operator,
                    term:term
                });
            }

            function addMultipleElements(filter){
                if(filter === undefined){
                    throw new Error('You must provide a filter string.');
                }

                var splitableFilter;

                if(filter.substr(0, 1) === '('){
                    splitableFilter = filter.substr(1);
                } else {
                    splitableFilter = filter;
                }

                if(filter.substr(filter.length - 1) === ')'){
                    splitableFilter = splitableFilter.substr(0, splitableFilter.length - 1);
                }

                var filterArray = splitableFilter.indexOf(')AND(') > 0 ? splitableFilter.split(')AND(') : splitableFilter.split(')and(');

                for(var FA = 0; FA < filterArray.length; FA++){
                    var filterParts = filterArray[FA].split(',');
                    //JP: modified to extend additional values in filter term that are parsed out by array.split(',')
                    addFilterElement(filterParts[0], filterParts[1], (filterParts[3]) ? filterParts.slice(2).join(',') : filterParts[2]);
                }
            }

            function getFilter(joiner){
                var outputArray = [];
                var joinerTerm = joiner || 'AND';

                if(filterElements.length === 1){
                    try{
                        var compiledElement = compileFilterArgument(filterElements[0].field, filterElements[0].operator, filterElements[0].term).replace('(','');
                        outputArray.push(compiledElement.replace(')',''));
                    } catch (err) {
                        throw new Error('RPSFilter construction error: ' + err);
                    } finally {
                        filterElements = [];
                    }
                } else {
                    try {
                        for(var FE = 0; FE < filterElements.length; FE++){
                            outputArray.push(compileFilterArgument(filterElements[FE].field, filterElements[FE].operator, filterElements[FE].term));
                        }
                    } catch (err) {
                        throw new Error('RPSFilter construction error: ' + err);
                    } finally {
                        filterElements = [];
                    }
                }

                return outputArray.join(joinerTerm).replace(',)',')');
            }

            function compileFilterArgument(field, operator, term){
                var compiledElement = '(' + field + ',' + operator + ',';

                try{
                    //numbers don't get touched
                    if(term !== undefined && term !== '' && term !== null) {
                        if (isNumber(term)) {
                            compiledElement += term + ')';
                            return compiledElement;
                        }
                        //dates or booleans don't get touched
                        if (isValidDate(term) || term.toLowerCase() === 'true' || term.toLowerCase() === 'false') {
                            compiledElement += term + ')';
                            return compiledElement;
                        }
                        //strings get operated on
                        var theTerm = term.replace(/#/g, '%23');
                        theTerm = theTerm.replace(/&/g, '%26');
                        theTerm = theTerm.replace(/"/g, '""');
                        compiledElement += '"' + theTerm + '")';
                        return compiledElement;
                    } else {
                        compiledElement += ')';
                        return compiledElement;
                    }
                } catch (err) {
                    throw new Error('RPSFilter construction error: ' + err);
                }
            }

            function unicodeFilter(field, operator, term){
                var compiledElement = '(' + field + ',' + operator + ',';

                if(term !== undefined) {
                    //numbers don't get touched
                    if (isNumber(term)) {
                        compiledElement += term + ')';
                    } else {
                        //dates or booleans don't get touched
                        if (isValidDate(term) || term.toLowerCase() === 'true' || term.toLowerCase() === 'false') {
                            compiledElement += term + ')';
                        } else {
                            //strings get operated on
                            var theTerm = term.replace(/#/g, '%23');
                            theTerm = theTerm.replace(/&/g, '%26');
                            theTerm = theTerm.replace(/"/g, '""');
                            compiledElement += '"' + theTerm + '")';
                        }
                    }
                } else {
                    compiledElement += ')';
                }

                return compiledElement.replace(',)',')');
            }

            function mousePosition(e){
                var x,y;
                try{
                    x= e.pageX;
                    y= e.pageY;
                }catch(err){
                    x=event.clientX + document.body.scrollLeft;
                    y=event.clientY + document.body.scrollTop;
                }
                return {
                    'x':x,
                    'y':y
                };
            }

            function getOffsetRect(elem) {
                var box = elem.getBoundingClientRect();

                var body = document.body;
                var docElem = document.documentElement;

                var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

                var clientTop = docElem.clientTop || body.clientTop || 0;
                var clientLeft = docElem.clientLeft || body.clientLeft || 0;

                var top  = box.top +  scrollTop - clientTop;
                var left = box.left + scrollLeft - clientLeft;

                return { top: Math.round(top), left: Math.round(left) };
            }

            function getOffset(elem) {
                if (elem.getBoundingClientRect) {
                    return getOffsetRect(elem);
                }
            }

            function isClickInObject(obj,event){
                var y_left,y_right,x_top,x_bottom,coords,click;
                coords=getOffset(obj);
                try{
                    click=mousePosition(event);
                }catch(err){
                    click=mousePosition();
                }

                y_left=coords.left;
                y_right=coords.left+obj.offsetWidth;

                x_top=coords.top;
                x_bottom=coords.top+obj.offsetWidth;
                if(click.x>y_right||click.x<y_left||
                    click.y<x_top||click.y>x_bottom){
                    return false;
                }
                return true;
            }

            function toTitleCase(str){
                return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
            }

            function addPluginEvent(eventArray){
                pluginEvents = eventArray;
            }

            function listPluginEvents(){
                return pluginEvents;
            }

            function resolvePluginEvents(eventName, targetObject, dependencyObject){
                var defObj = $q.defer(),
                    thisEventChain = [],
                    pluginEvents = listPluginEvents();

                for(var TE = 0; TE < pluginEvents.length; TE ++){
                    if(pluginEvents[TE].targetObject.toLowerCase() === targetObject && pluginEvents[TE].targetEvent === eventName){
                        thisEventChain.push(pluginEvents[TE]);
                    }
                }

                if(thisEventChain.length){
                    var outsideEvents = callOutsideEvents(thisEventChain, dependencyObject);
                    outsideEvents.then(function(){
                        defObj.resolve();
                    });
                } else {
                    defObj.resolve();
                }

                return defObj.promise;
            }

            function callOutsideEvents(eventObjectArray, dependencyObject){
                var defObj = $q.defer(),
                    thisEvent = eventObjectArray[0];
                eventObjectArray.splice(0, 1);
                var outsideFunction = window[thisEvent.function];
                var thisEventPromise = outsideFunction(dependencyObject, $q);
                thisEventPromise.then(function(data){
                    if(data){
                        console.debug(data);
                    }
                    if(eventObjectArray.length > 0){
                        console.debug('calling outside event: ' + eventObjectArray);
                        callOutsideEvents(eventObjectArray, dependencyObject);
                    } else {
                        defObj.resolve();
                    }
                },function(error){
                    angular.noop();
                });

                return defObj.promise;
            }

            return {
                generateGUID: generateGUID,
                UtilArray: {
                    makeArray:makeArray,
                    objectArrayIndexOf: objectArrayIndexOf
                },
                regex:{
                    replace:{
                        nonNumeric:replaceNonNumeric,
                        spaces:replaceSpaces
                    }
                },
                String: {
                    toTitleCase: toTitleCase
                },
                responseParser:responseParser,
                isNullOrEmpty:nullOrEmpty,
                isClickInObject:isClickInObject,
                toBoolean:toBoolean,
                validDate:isValidDate,
                isNumber:isNumber,
                RPSFilters:{
                    addElement:addFilterElement,
                    addMultiple:addMultipleElements,
                    getFilter:getFilter,
                    operators:filterOperators,
                    unicodeFilter:unicodeFilter
                },
                resourceMethods:resourceMethods,
                validateTotal:validateTotal,
                PluginEvents:{
                    Add:addPluginEvent,
                    List:listPluginEvents,
                    Resolve:resolvePluginEvents
                }
            };
        }]);