window.angular.module('prismApp.common.services.LoggingService', [])
	.factory('LoggingService', [
		function () {
			'use strict';

			var logLines = [];

			function clearLog(){
				return [];
			}

			return {
				CleanLog:clearLog,
				LogData: logLines
			};
		}]);