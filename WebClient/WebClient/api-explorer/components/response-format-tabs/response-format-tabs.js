window.angular.module('prismApp')
    .component('responseFormatTabs', {
        templateUrl:'components/response-format-tabs/response-format-tabs.htm',
        bindings: {
            response: '=',
        },
        controllerAs:'ResponseTabs',
        controller:[function(){
            'use strict';
            var me = this;
            me.$onInit = function(){
                me.treeOn=true;
                me.formOn=false;
                me.rawOn=false;
                me.formResponse=JSON.stringify(me.response,null,4);
                me.rawResponse=JSON.stringify(me.response);
            };
            me.changeFormat = function(val){
                switch (val) {
                    case 1:
                    me.formOn=true;
                    me.treeOn=false;
                    me.rawOn=false;
                        break;
                    case 2:
                    me.rawOn=true;
                    me.formOn=false;
                    me.treeOn=false;
                        break;
                    default:
                    me.treeOn=true;
                    me.formOn=false;
                    me.rawOn=false;
                        break;
                }
            };
        }]
    }
);