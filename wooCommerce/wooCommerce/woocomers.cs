﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WooCommerceNET;
using WooCommerceNET.WooCommerce.v2;
namespace wooCommerce
{
    public class woocomers
    {
        static RestAPI rest = new RestAPI("http://ninomaxx.cf/wp-json/wc/v2/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725");
        public static WooCommerceNET.WooCommerce.Legacy.Variation variations(string prices, string sku)
        {
            WooCommerceNET.WooCommerce.Legacy.Variation atr_ = new WooCommerceNET.WooCommerce.Legacy.Variation();
            atr_.price = decimal.Parse(prices);
            atr_.regular_price = decimal.Parse(prices);
            atr_.stock_quantity = 99;
            atr_.visible = true;
            atr_.sku = sku;

            return atr_;
        }
        public static WooCommerceNET.WooCommerce.Legacy.Attribute atr_woo()
        {
            WooCommerceNET.WooCommerce.Legacy.Attribute atr_ = new WooCommerceNET.WooCommerce.Legacy.Attribute();
            List<string> l_optuon = new List<string>();
                     atr_.position = 0;
            atr_.visible = true;
            atr_.variation = true;
            atr_.options = l_optuon;
            return atr_;
        }
        public async Task inputProductvariable()
        {
            ProductAttributeLine atr_color = new ProductAttributeLine();
            atr_color.name = "Color";
            atr_color.options = new List<string>() { "Red" };
            atr_color.position = 1;
            atr_color.variation = true;

            WCObject wc = new WCObject(rest);
            List<ProductAttributeLine> L_atr = new List<ProductAttributeLine>();
            L_atr.Add(atr_color);
            List<Variation> vrs = new List<Variation>();
            List<VariationAttribute> vatrib = new List<VariationAttribute>()
                { new VariationAttribute() { name="Color",option="901" },
                  new VariationAttribute() { name="size",option="S" } };
            Variation var1 = new Variation()
            {
                regular_price =500000,
                visible = true,
                attributes = vatrib,
                stock_quantity = 5,
                manage_stock = true,
                
                sku = "",
            };
            vrs.Add(var1);
            Product p = new Product()
            {
                name = "The Test " + DateTime.Now.ToString("HHmmss"),
                short_description = "hm this is a <b>short</b> description ",
                description = "well this is a <b>looooooooooooooong</b> description blah blah blah..... ",
                slug = "slug",
                type = "variable",
                status = "publish",
                attributes = L_atr,
                
                // i have more options here....
              
            };
            p = await wc.Product.Add(p);
            var1 = await wc.Product.Variations.Add(var1, p.id.Value);
            p.variations.Add(var1.id.Value);
            await wc.Product.Update(p.id.Value, p);
        }
    }
}