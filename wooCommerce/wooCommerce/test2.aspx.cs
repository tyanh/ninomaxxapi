﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace wooCommerce
{
    public partial class test2 : System.Web.UI.Page
    {
        string url = "http://192.168.9.253/";
        string urlcrm = "http://api.v-need.com/";
        string Auth_SessionCRM = "TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=";
        common cmd = new common();
        string Auth_Session = "";
        protected void Page_Load(object sender, EventArgs e)
        {
             Auth_Session = cmd.GetAuth_Nonce();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();
            updateItemPro(Auth_Session,TextBox1.Text.Trim());
        }
        public string updateItem(string key_login)
        {
           
            string url = "http://192.168.9.253/v1/rest/customer";
           
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = new {
            origin_application = "RProPrismWeb",
            first_name= "test Jack",
            last_name= "Harkness" };
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();

            return "";
        }
        public string updateItemPro(string key_login,string Id)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var urls = "http://192.168.9.253/v1/rest/customer?cols=*&filter=sid,EQ," + Id.Trim() + "&page_no=1&page_size=1";
            var obj = webClient.DownloadString(urls);
            dynamic objs = JsonConvert.DeserializeObject<dynamic>(obj);
            var row_version = "";
            row_version = objs[0].row_version;

             string url = "http://192.168.9.253/v1/rest/customer/"+ Id + "?filter=row_version,eq," + row_version + "";
           // string url = "http://192.168.9.253/api/common/customer/" + Id + "?filter=row_version,eq," + row_version + "";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
           myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = new { last_name = "AlteredLastName"  };
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();

            return "";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();
            string url = "http://192.168.9.253/v1/rest/customer";
          
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Headers.Add("Auth-Session", Auth_Session);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic p = new
            {
                phone_no = "09089776880"

            };
           
            List<dynamic> phone = new List<dynamic>();
            phone.Add(p);
            dynamic l = new {
                origin_application= "RProPrismWeb",
                share_type="1",
                first_name = "AlteredLastName2",
                last_name = "AlteredLastName",
                phones = phone,
            };
           
            o.Add(l);
         
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();

           
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

        
            string Auth_Session = cmd.GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string a = url + "v1/rest/customer?cols=*&filter=sid,EQ," + TextBox1.Text.Trim()+"&page_no=1&page_size=500";
            //string a = url + "v1/rest/document?cols=*&page_no=1&page_size=10";
            var obj = webClient.DownloadString(a);
        }
        public dynamic timhoadonPrm()
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/document?cols=*&filter=sid,EQ," + TextBox1.Text.Trim() + "&page_no=1&page_size=500";

            var obj = webClient.DownloadString(urls);
            return JsonConvert.DeserializeObject<dynamic>(obj);
        }
        public List<dynamic> detailbii(string id)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string a = url + "v1/rest/document/" + TextBox1.Text.Trim() + "/item?cols=lty_orig_points_earned,note4,item_path,discount_reason,is_competing_component,activity2_percent,ship_method_id,order_ship_method_id,employee3_name,item_origin,employee1_sid,employee3_full_name,central_return_commit_state,so_cancel_flag,employee2_login_name,inventory_item_type,sid,item_description3,note8,item_description1,lty_piece_of_tbe_points,hist_discount_perc5,cost,udf_string04,st_address_line3,package_sequence_number,invn_sbs_item_sid,item_status,qty_available_for_return,discounts,original_cost,st_primary,hist_discount_reason4,gift_add_value,note3,lot_type,ref_order_item_sid,gift_transaction_id,hist_discount_amt1,employee1_full_name,total_discount_reason,order_quantity_filled,st_company_name,post_date,ship_method,commission2_amount,inventory_on_hand_quantity,tax_perc_lock,dcs_code,activity3_percent,price,employee1_login_name,central_document_sid,order_ship_method_sid,employee3_login_name,tax2_amount,st_address_line2,fulfill_store_sbs_no,employee5_sid,returned_item_invoice_sid,udf_string01,udf_float01,price_before_detax,ship_id,hist_discount_reason5,employee2_name,commission_code,spif,price_lvl_name,style_sid,commission3_amount,dip_discount_amt,hist_discount_perc2,st_postal_code_extension,employee3_id,commission_percent,item_pos,st_id,activity4_percent,returned_item_qty,detax_flag,custom_flag,st_price_lvl,discount_amt,total_discount_amount,row_version,dip_tax_amount,employee5_id,activation_sid,lty_redeem_applicable_manually,order_ship_method,enhanced_item_pos,item_description2,employee5_name,manual_disc_reason,tax_code_rule2_sid,udf_string05,total_discount_percent,tax_amount,package_number,scan_upc,so_number,shipping_amt_bdt,employee3_sid,invn_item_uid,item_type,employee5_full_name,subsidiary_number,note10,tax_code_rule_sid,employee2_sid,customer_field,tax2_percent,udf_date01,st_address_line5,original_component_item_uid,ref_sale_item_pos,manual_disc_value,tax_code2,original_price_before_detax,lty_points_earned,st_last_name,fulfill_store_sid,st_country,udf_string03,st_first_name,tax_code,hist_discount_reason2,employee4_sid,price_lvl,lty_orig_price_in_points,note2,commission_level,item_lookup,vendor_code,commission5_amount,alu,schedule_number,serial_number,promotion_flag,udf_float02,so_deposit_amt,lty_price_in_points,st_security_lvl,ship_method_sid,created_datetime,hist_discount_amt3,inventory_quantity_per_case,inventory_use_quantity_decimals,commission_amount,hist_discount_perc3,st_address_line1,user_discount_percent,central_item_pos,tracking_number,st_title,ref_order_doc_sid,hist_discount_perc4,tenant_sid,kit_flag,kit_type,lot_number,discount_perc,override_max_disc_perc,modified_datetime,udf_float03,original_tax_amount,commission4_amount,hist_discount_amt4,st_detax_flag,st_tax_area_name,orig_document_number,activity5_percent,archived,package_item_uid,lty_price_in_points_ext,item_size,original_price,store_number,lty_pgm_name,order_type,employee4_id,st_price_lvl_name,attribute,lty_piece_of_tbr_points,orig_subsidiary_number,employee1_name,delete_discount,original_component_item_sid,employee2_id,item_description4,serial_type,activity_percent,employee4_login_name,gift_activation_code,gift_expire_date,tax_char,hist_discount_reason3,document_sid,hist_discount_perc1,udf_string02,created_by,ref_sale_doc_sid,shipping_amt,package_item_sid,note5,st_primary_phone_no,return_reason,employee5_login_name,note9,employee1_id,note1,lty_type,price_lvl_sid,st_address_uid,st_address_line6,apply_type_to_all_items,lty_piece_of_tbr_disc_amt,tax_area_name,promo_disc_modifiedmanually,st_cuid,note6,hist_discount_amt5,orig_sale_price,tax_area2_name,dip_price,modified_by,st_postal_code,st_email,orig_store_number,quantity,note7,hist_discount_amt2,gift_quantity,controller_sid,lty_pgm_sid,employee2_full_name,dip_tax2_amount,origin_application,employee4_full_name,employee4_name,st_tax_area2_name,hist_discount_reason1,manual_disc_type,st_address_line4,tax_percent,tax_char2,fulfill_store_no,from_centrals,st_customer_lookup,maxaccumdiscpercent,maxdiscpercent,promo_gift_item&sort=enhanced_item_pos,desc";
            var obj = webClient.DownloadString(a);
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/document?cols=*&filter=sid,EQ," + TextBox1.Text.Trim() + "&page_no=1&page_size=500";

            var obj = webClient.DownloadString(urls);
            Response.Write(obj);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/customer?cols=*&filter=sid,EQ," + TextBox1.Text.Trim() + "&page_no=1&page_size=500";

            var obj = webClient.DownloadString(urls);
            Response.Write(obj);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/inventory?cols=*&filter=sid,EQ," + TextBox1.Text.Trim() + "&page_no=1&page_size=500";

            var obj = webClient.DownloadString(urls);
            Response.Write(obj);
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string a = url + "v1/rest/document/" + TextBox1.Text.Trim() + "/item?cols=*";
            var obj = webClient.DownloadString(a);
            Response.Write(obj);
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            common cmd = new common();
            var b = timhoadonPrm();
            var c = detailbii(b[0].sid.ToString());
           string ss= cmd.importbillCRM(b[0], Auth_Session, "1");
            sqlcommon sql = new sqlcommon();
            sql.runSQL(ss);
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            common cmd = new common();
            var b = timhoadonPrm();
            var c = detailbii(b[0].sid.ToString());
            string ss = cmd.importbillCRMreturn(b[0], Auth_Session, "1");
            sqlcommon sql = new sqlcommon();
            sql.runSQL(ss);
        }

        protected void Button10_Click(object sender, EventArgs e)
        {

            string Auth_Session = cmd.GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/store?cols=*&filter=store_name,EQ," + TextBox1.Text.Trim() + "&page_no=1&page_size=500";

            var obj = webClient.DownloadString(urls);
            Response.Write(obj);
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            wooApiv2 w = new wooApiv2();
            w.updateattribute(TextBox1.Text.Trim());
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlcrm+ "/Token");
            myHttpWebRequest_.Headers.Add("Authorization", "basic TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=");
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            List<dynamic> o = new List<dynamic>();
            dynamic bodys = new
            {
                UserName="admin",
                Password="123@qaz"

            };
            
            string body = JsonConvert.SerializeObject(bodys);
            var postData = body;

            var data = Encoding.UTF8.GetBytes(body);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            var encoding = ASCIIEncoding.ASCII;
            
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            Response.Write(str);
        }

        protected void Button12_Click(object sender, EventArgs e)
        {

        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            common cmds = new common();
            var a = cmds.Get_product2(int.Parse(TextBox1.Text));
            Response.Write(a);
        }

        protected async void Button15_Click(object sender, EventArgs e)
        {
            common cmd = new common();
            var a = await cmd.Get_product2(int.Parse(TextBox1.Text));
            Response.Write(a);
        }

        protected void Button16_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/inventory?cols=*&filter=upc,EQ," + TextBox1.Text.Trim() + "&page_no=1&page_size=500";

            var obj = webClient.DownloadString(urls);
            Response.Write(obj);
        }

        protected void Button17_Click(object sender, EventArgs e)
        {
            string key_login = cmd.GetAuth_Nonce();
            var ss = codecoup(key_login);
            
           
            Response.Write(ss);
        }
        protected string codecoup(string key_login)
        {
            Uri address = new Uri(url + "api/common/couponsetcoupon?cols=*&page_no=1&page_size=30&sort=issuedstatus,desc&sort=startdate,desc");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            return JsonConvert.SerializeObject(data);
        }
        protected void Button18_Click(object sender, EventArgs e)
        {

            string key_login = cmd.GetAuth_Nonce();
            Uri address = new Uri(url + "api/backoffice/pcppromotion?cols=*&filter=sid,EQ," + TextBox1.Text.Trim() + "");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            Response.Write(data);
        }

        protected void Button19_Click(object sender, EventArgs e)
        {
            common cmd = new common();
            string key_login = cmd.GetAuth_Nonce();
            
            var obj = cmd.getcouponame(TextBox1.Text.Trim(), key_login);
            Response.Write(obj);
        }

        protected void Button20_Click(object sender, EventArgs e)
        {

            common cmd = new common();
            string key_login = cmd.GetAuth_Nonce();

            var obj = cmd.getpromotionId(TextBox1.Text.Trim(), key_login);
            Response.Write(obj);
        }

        protected void Button21_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string uurl = url + "v1/rest/customer/541556359000178213/address?cols=sid,row_version&page_no=1&page_size=10";
            var obj = webClient.DownloadString(uurl);
            Response.Write(obj);
        }

        protected void Button22_Click(object sender, EventArgs e)
        {
            string key_login = cmd.GetAuth_Nonce();
            Uri address = new Uri(url + "/v1/rest/document/transform/532458814000121158?filter=(SID,eq,541457610000101233)&type=preview&sort=item.ENHANCED_ITEM_POS,%20desc");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            Response.Write(data);
        }

        protected void Button23_Click(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = url + "v1/rest/document?cols=*&filter=coupons,GE,0&page_no=1&page_size=1";

            var obj = webClient.DownloadString(urls);
            
            Response.Write(obj);
        }

        protected void Button24_Click(object sender, EventArgs e)
        {
            string key_login = cmd.GetAuth_Nonce();
            Uri address = new Uri(url + "api/common/couponsetcoupon?filter=sid,EQ," + TextBox1.Text.Trim()+"&cols = *&page_no=1&page_size=30&sort=issuedstatus,desc&sort=startdate,desc");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            Response.Write(data);
        }

        protected void Button25_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string uurl = url + "v1/rest/document/542142884000159066/coupon?filter=542142947000139146&cols=*";
            var obj = webClient.DownloadString(uurl);
            Response.Write(obj);
        }

        protected void Button26_Click(object sender, EventArgs e)
        {
            string Auth_Session = cmd.GetAuth_Nonce();

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string uurl = url + "v1/rest/controller/529031818000006255?cols=*";
            var obj = webClient.DownloadString(uurl);
            Response.Write(obj);
        }

        protected void Button27_Click(object sender, EventArgs e)
        {
            string key_login = cmd.GetAuth_Nonce();
            Uri address = new Uri(url + "api/backoffice/pcppromotion?cols=*&filter=tenantsid,EQ," + TextBox1.Text.Trim() + "");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            Response.Write(data);
        }

        protected void Button28_Click(object sender, EventArgs e)
        {
            DateTime a = DateTime.Parse(TextBox1.Text);
            Response.Write(a);
        }
    }
}