﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace wooCommerce
{
    public partial class _default : System.Web.UI.Page
    {

        common cmd = new common();
        common cmdv2 = new common();
        sqlcommon ssql = new sqlcommon();
        protected void Page_Load(object sender, EventArgs e)
        {
            ssql.closesql();
        }

        protected  void Button1_Click(object sender, EventArgs e)
        {
            string key= cmd.GetAuth_Nonce();
            cmd.inventory(key, "1");
        }

        protected async void Button2_Click(object sender, EventArgs e)
        {
            //var a=await cmd.Get_cate("Áo dệt");
            var a = await cmd.Get_product2(2548);
            Response.Write(a);
        }

        protected async void Button3_Click(object sender, EventArgs e)
        {
            // string Auth_Session = cmd.GetAuth_Nonce();
            //cmd.inventoryandinput(Auth_Session,"1");
            woocomers woo = new woocomers();
           await woo.inputProductvariable();
        }
    
        protected async void Button4_Click(object sender, EventArgs e)
        {
          await wooApiv2.Get_update_product_filter();
        }
        
        public string importPromotionCRM(dynamic b)
        {
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            dynamic c = new System.Dynamic.ExpandoObject();
            var check = sql.checkdataExit("DataExchange", "where DataType='4' AND Idprim='" + b.sid.ToString() + "'");
            if (!check)
                delete = 1;
            c.PromotionPrismID = b.sid;
            c.PromotionName = b.generalpromoname;
            c.ValidFrom = b.activationstartdate;
            c.ValidTo = b.activationenddate;
            c.Note = b.generaldescription;
            c.Priority = 1;
            c.CreateDate = b.createddatetime;
            c.IsDelete = delete;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = cmd.Stringcolumnsql("4", resp, b.sid.ToString(),"");
            return sqls;
        }
      
        public string importCopponCRM(dynamic b, string key_login)
        {
            var cp =JsonConvert.DeserializeObject<List<dynamic>>(cmd.getcouponame(b.couponsetsid.ToString(), key_login));
            List<dynamic> pro = JsonConvert.DeserializeObject<List<dynamic>>(cmd.getpromotionId(cp[0].setid.ToString(), key_login));
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            dynamic c = new System.Dynamic.ExpandoObject();
            var check = sql.checkdataExit("DataExchange", "where DataType='4' AND Idprim='" + b.sid.ToString() + "'");
            if (!check)
                delete = 1;

            c.CouponPrismID = b.sid;
            c.PromotionPrism = "";
                 c.CouponValue = "";
            c.coupontype ="";
            if (pro.Count > 0)
            {
                c.PromotionPrism = pro.Select(s=>s["sid"].ToString()).ToList();
                c.CouponValue = pro.Select(s => s["validationsubtotal"].ToString()).ToList();
                c.coupontype = pro.Select(s => s["generalpromotype"].ToString()).ToList();
            }
            c.CouponCode = b.couponcode;
            c.CouponName = cp[0].setname;
            
            c.ValueType = "";
            c.ScoreExchange ="";
            c.active = b.issuedstatus;
            c.CreateDate = b.createddatetime;
            c.IsDelete = delete;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = cmd.Stringcolumnsql("5", resp, b.sid.ToString(),"");
            return sqls;
        }
        public string importStoreCRM(dynamic b)
        {
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            dynamic c = new System.Dynamic.ExpandoObject();
            //var check = sql.checkdataExit("DataExchange", "where DataType='4' AND Idprim='" + b.sid.ToString() + "'");
            //if (!check)
            //    delete = 1;
            c.StorePrismID = b.sid;
            c.StoreCode = b.store_code;
            c.StoreName = b.store_name;
            c.WardPrismID = "";
            c.AddressNo = b.address1;
            c.PhoneNumber = b.phone1;
            c.IsDelete = delete;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = cmd.Stringcolumnsql("10", resp, b.sid.ToString(),"");
            return sqls;
        }
       
        public string reaplacetxt(string a)
        {
            return a.Replace("'", "");
        }
        public string importcustomerCRM(dynamic b,string indext)
        {
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            dynamic c = new System.Dynamic.ExpandoObject();
            //var check = sql.checkdataExit("DataExchange", "where DataType='2' AND Idprim='"+ b.sid.ToString() + "'");
            //if (!check)
            //    delete = 1;
          
            c.CustomerPrismID = b.sid;
            c.FirstName = reaplacetxt(b.first_name.ToString());
            c.LastName = reaplacetxt(b.last_name.ToString());
            c.Gender = 1;
            c.BirthDay = b.udf1_date;
            c.PhoneNumber = b.primary_phone_no;
            c.Email = reaplacetxt(b.email_address.ToString());
            c.AddressNo = reaplacetxt(b.primary_address_line_1.ToString());
            c.AddressReceipts = reaplacetxt(b.primary_address_line_1.ToString());
            c.WardPrism ="";
            c.IsDelete = delete;

            string resp = JsonConvert.SerializeObject(c);
            List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
            List<string> vl = new List<string>() {
               "2",resp,"0","1",indext,DateTime.Now.ToString(),cmd.GetIPAddress(),b.sid.ToString()
            };
            string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
           // string sqlinsert = upadate(resp, b.sid.ToString());
            return sqlinsert;
        }
        public string upadate(string vl,string id)
        {
          string up= "UPDATE DataExchange SET JsonData =N'"+vl+ "' WHERE Idprim='"+id+"'";
            return up;
        }
        public string importproductCRM(dynamic b)
        {
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            dynamic c = new System.Dynamic.ExpandoObject();
            //var check = sql.checkdataExit("DataExchange", "where DataType='3' AND Idprim='" + b.sid.ToString() + "'");
            //if (!check)
            //    delete = 1;
          string name= HttpUtility.HtmlEncode(b.description2.ToString());
            c.ProductPrismID = b.sid;
            c.ProductCode = HttpUtility.HtmlEncode(b.description1.ToString());
            c.ProductBarCode = b.upc;
            c.ProductName = reaplacetxt(b.description2.ToString());
            c.ProductType =b.serial_type;
            c.UnitPrice = b.order_cost;
            c.Size = b.item_size;
            c.Color = reaplacetxt(b.attribute.ToString());
            c.Note = "";
            c.IsDelete = delete;
            string resp = JsonConvert.SerializeObject(c);
            List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
            List<string> vl = new List<string>() {
               "3",resp,"0","1","test",DateTime.Now.ToString(),cmd.GetIPAddress(),b.sid.ToString()
            };
            string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
            return sqlinsert;
        }
        protected void Button5_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            int count = 0;
            int idex = 506;

            List<dynamic> obj = new List<dynamic>();
            obj = cmd.customer(Auth_Session, idex.ToString());
            while (obj.Count>0)
            {
                List<string> sqlstr = new List<string>();
                foreach (var i in obj)
                {
                    string str = importcustomerCRM(i, idex.ToString());
                    sqlstr.Add(str);
                }
                sql.runSQL(string.Join(";", sqlstr));
                Thread.Sleep(2000);
                idex++;
                obj = cmd.customer(Auth_Session, idex.ToString());
                var aaa = obj[0].sid;
            }
            Response.Write(count);    
        }

        protected async void Button6_Click(object sender, EventArgs e)
        {
           await cmd.inputwoocomCatelogy();
            
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            int indext = 11;
            List<dynamic> obj = new List<dynamic>();
            obj = cmd.inventory(Auth_Session, indext.ToString());
            while (obj.Count>0)
            {

                List<string> sqlstr = new List<string>();
                foreach (var i in obj)
                {
                    string str = importproductCRM(i);
                    sqlstr.Add(str);
                }
                sql.runSQL(string.Join(";", sqlstr));
                Thread.Sleep(2000);
                indext++;
                obj = cmd.inventory(Auth_Session, indext.ToString());
                var aaa = obj[0].sid;
            }
        }

        protected async void Button8_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            int c = 0;
            int d = 0;
            int p = 0;
            int i = 1;
            List<dynamic> a = new List<dynamic>();
            //do
            //{
            //    a = cmd.customer(Auth_Session, i.ToString());
            //    c = c + a.Count;
            //    var ee = a.Where(s => s["first_name"].ToString() != "").ToList();
            //    d = d + ee.Count;
            //    p = p + (a.Count - ee.Count);
            //    i++;
            //}
            //while (a.Count > 0);
            a = cmd.customer(Auth_Session, "1");
            foreach (var j in a)
            {
              await cmd.inputwoocomCustomer(a[0]);
            }
        }
        [WebMethod]
        public static string promoo()
        {
            common ccmd = new common();
            sqlcommon sql = new sqlcommon();
            string Auth_Session = ccmd.GetAuth_Nonce();
            //var a = ccmd.Promotions(Auth_Session, "1");
            return "";
        }
        protected void Button9_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            var a = cmd.Promotions(Auth_Session, "1");
            List<string> sqlstr = new List<string>();
            foreach (var i in a)
            {
                var b = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(i));
                string str = importPromotionCRM(b);
                sqlstr.Add(str);
            }
            sql.runSQL(string.Join(";", sqlstr));
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            int indext =1;
            List<dynamic> obj = new List<dynamic>();
            obj = cmd.document(Auth_Session, indext.ToString());
            List<string> sqlstr2 = new List<string>();
            while (obj.Count>0)
            {
                List<string> sqlstr = new List<string>();
                foreach (var i in obj)
                {
                    string str = "";
                    if(i.ref_sale_doc_no.ToString()=="")
                      str = cmd.importbillCRM(i, Auth_Session, indext.ToString());
                    else
                        str = cmd.importbillCRMreturn(i, Auth_Session, indext.ToString());
                    sqlstr.Add(str);
                 
                }
                sql.runSQL(string.Join(";", sqlstr));
                Thread.Sleep(2000);
                indext++;
                obj = cmd.document(Auth_Session, indext.ToString());
                var iddup = sql.checkdataExit("DataExchange", "where DataType=1 AND Idprim='" + obj[0].sid.ToString() + "'");
                if (!iddup)
                {
                    obj = new List<dynamic>();
                    break;
                }
            }
           
            //sql.runSQL(string.Join(";", sqlstr));
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            var a =JsonConvert.DeserializeObject<List<dynamic>>(cmd.couppon(Auth_Session, "1"));
            List<string> sqlstr = new List<string>();
            foreach (var i in a)
            {
                var b =JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(i));
                //string aaa = b.couponsetsid.ToString();
                string str = importCopponCRM(b, Auth_Session);
                //Response.Write(str);
                sqlstr.Add(str);
            }
         sql.runSQL(string.Join(";", sqlstr));
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            var a = cmd.store(Auth_Session, "1");
            List<string> sqlstr = new List<string>();
            foreach (var i in a)
            {
                var b = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(i));
                string str = importStoreCRM(b);
                sqlstr.Add(str);
            }
            sql.runSQL(string.Join(";", sqlstr));
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            sqlcommon sql = new sqlcommon();
            string Auth_Session = cmd.GetAuth_Nonce();
            var a = cmd.store(Auth_Session, "1");
            var b = a.Where(s => s["address1"].ToString() != "").ToList();
            Response.Write(b.Count);
            GridView1.DataSource = b;
            GridView1.DataBind();
        }
    }
}
