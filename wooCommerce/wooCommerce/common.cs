﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using WooCommerceNET;
using WooCommerceNET.WooCommerce.Legacy;

namespace wooCommerce
{
    public class common
    {
        string url = "http://203.205.21.52:8080/";
        static RestAPI rest = new RestAPI("https://ninomaxxconcept.com.vn/wc-api/v3/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725");
        public string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        public string GetAuth_Nonce()
        {
            string Auth_Session = "";
            Uri ourUri = new Uri(url + "v1/rest/auth");
            // Creates an HttpWebRequest for the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(ourUri);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            var header = myHttpWebResponse.Headers;
            string Status_Code = header.Get("Http-Status-Code");
            if (Status_Code == "200")
            {
                string Auth_Nonce = header.Get("Auth-Nonce");
                myHttpWebResponse.Close();
                Auth_Session = Getlogin(int.Parse(Auth_Nonce));

            }
            return Auth_Session;

        }
        public string Getlogin(int Auth_Nonce)
        {
            string Auth_Session = "";
            
                var code_1 = Auth_Nonce / 13;
                var trunc = Math.Truncate(decimal.Parse(code_1.ToString()));
                var Auth_Nonce_reponse = (trunc % 99999) * 17;

                HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url + "v1/rest/auth?usr=sysadmin&pwd=sysadmin");
                WebHeaderCollection header = myHttpWebRequest_.Headers;
                myHttpWebRequest_.Method = "GET";
                header.Add("Auth-Nonce", Auth_Nonce.ToString());
                header.Add("Auth-Nonce-Response", Auth_Nonce_reponse.ToString());
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();
                var header_ = myHttpWebResponse.Headers;
                Auth_Session = header_.Get("Auth-Session");
                myHttpWebResponse.Close();
                wordtaition(Auth_Session);
            
            return Auth_Session;
        }
        public void wordtaition(string key_login)
        {


            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url + "v1/rest/sit?ws=WebClient");
            WebHeaderCollection header = myHttpWebRequest_.Headers;
            myHttpWebRequest_.Method = "GET";
            header.Add("Auth-Session", key_login);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            myHttpWebResponse.Close();
        }
        public List<dynamic> inventory(string key_login, string page_no)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var obj = webClient.DownloadString(url + "v1/rest/inventory?cols=*&page_no=" + page_no + "&page_size=1000");
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public List<dynamic> store(string key_login, string page_no)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var obj = webClient.DownloadString(url + "v1/rest/store?filter=active,EQ,true&cols=*&page_no=" + page_no + "&page_size=1000");
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public dynamic Promotions(string key_login, string page_no)
        {
            
            Uri address = new Uri(url + "api/backoffice/pcppromotion?cols=*");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            return data;
         }
        public string getpromotionId(string id, string key_login)
        {
            Uri address = new Uri(url + "api/backoffice/pcppromotion?cols=sid,validationsubtotal,generalpromotype&filter=rewardcouponsetid,EQ," + id + "");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            return JsonConvert.SerializeObject(data);

        }
        public string getcouponame(string sidid,string key_login)
        {
            Uri address = new Uri(url + "api/common/couponset?cols=*&filter=sid,EQ," + sidid + "");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            return JsonConvert.SerializeObject(data);

        }
        public dynamic couppon(string key_login, string page_no)
        {
            Uri address = new Uri(url + "api/common/couponsetcoupon?cols=*&page_no=1&page_size=30&sort=issuedstatus,desc&sort=startdate,desc");
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            // request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            return JsonConvert.SerializeObject(data);
            

        }
        public string findcoupon(string Auth_Session)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string a = url + "/v1/rest/document/538537886000174047/coupon/538539431000179005";
            var obj = webClient.DownloadString(a);
            return obj;
        }
        public string importbillCRMreturn(dynamic b, string authen, string idex)
        {
            List<dynamic> BillDetails = new List<dynamic>();
            List<dynamic> BillDetail = Itembill(authen, b.sid.ToString());
            foreach (var i in BillDetail)
            {
                double sale = double.Parse(i.quantity.ToString()) * double.Parse(i.price.ToString());
                double dis = double.Parse(i.quantity.ToString()) * (double.Parse(i.original_price.ToString()) - double.Parse(i.price.ToString()));
                dynamic detail = new System.Dynamic.ExpandoObject();
                detail.ProductID = i.invn_sbs_item_sid;
                detail.NumberOfProduct = i.quantity;
                detail.PromotionUnitPrice = i.price;
                detail.UnitPrice = i.original_price;
                detail.TotalAmount = sale;
                detail.StatusID = 1;
                BillDetails.Add(detail);
            }
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            var coupon = b.coupons;
            List<string> cpo = new List<string>();
            foreach (var i in coupon)
            {
                string cp = i.link.ToString();
                string[] cc = cp.Split('/');
                cpo.Add(cc[cc.Count() - 1]);
            }
            dynamic c = new System.Dynamic.ExpandoObject();
            c.BillNo = b.sid;
            c.TotalAmount = b.sale_subtotal;
            c.TotalPaymentAmount = b.transaction_total_amt;
            c.Note = b.reason_description;
            c.CardID = "";
            c.CouponCode = cpo;
            c.UsedMoneyExchange = 0;

            c.PromoteCode = "";
            c.StoreID = b.store_code;
            c.CreateDate = b.created_datetime;
            c.CreateBy = b.created_by;
            c.PaymentTypeID = 1;
            c.CustomerID = b.bt_cuid;
            c.StatusID = 1;
            c.BillDetail = BillDetails;
            c.BillReturn = b.ref_sale_sid;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = Stringcolumnsql("1", resp, b.sid.ToString(), "return");
            string ss = JsonConvert.SerializeObject(b);
            return sqls;
        }
        public string importbillCRM(dynamic b, string authen, string idex)
        {
            List<dynamic> BillDetails = new List<dynamic>();
            List<dynamic> BillDetail = Itembill(authen, b.sid.ToString());
            foreach (var i in BillDetail)
            {
                double sale = double.Parse(i.quantity.ToString()) * double.Parse(i.price.ToString());
                double dis = double.Parse(i.quantity.ToString()) *(double.Parse(i.original_price.ToString()) - double.Parse(i.price.ToString()));
                dynamic detail = new System.Dynamic.ExpandoObject();
                detail.ProductID = i.invn_sbs_item_sid;
                detail.NumberOfProduct = i.quantity;
                detail.PromotionUnitPrice = i.price;
                detail.UnitPrice = i.original_price;
                detail.TotalAmount = sale;
                detail.StatusID = 0;
                BillDetails.Add(detail);
            }
            sqlcommon sql = new sqlcommon();
            int delete = 0;
            var coupon = b.coupons;
            List<string> cpo = new List<string>();
            foreach(var i in coupon)
            {
                string cp = i.link.ToString();
                string[] cc = cp.Split('/');
                cpo.Add(cc[cc.Count() - 1]);
            }
            dynamic c = new System.Dynamic.ExpandoObject();
            c.BillNo = b.sid;
            c.TotalAmount = b.sale_subtotal;
            c.TotalPaymentAmount = b.transaction_total_amt;
            c.Note = b.reason_description;
            c.CardID = "";
            c.CouponCode = cpo;
            c.UsedMoneyExchange = 0;

            c.PromoteCode = "";
            c.StoreID = b.store_code;
            c.CreateDate = b.created_datetime;
            c.CreateBy = b.created_by;
            c.PaymentTypeID = 1;
            c.CustomerID = b.bt_cuid;
            c.StatusID = 0;
            c.BillDetail = BillDetails;
            string resp = JsonConvert.SerializeObject(c);
            string sqls =  Stringcolumnsql("1", resp, b.sid.ToString(), idex);
            return sqls;
        }
        public string Stringcolumnsql(string type, string resp, string id, string idex)
        {
            sqlcommon sql = new sqlcommon();
            List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
            List<string> vl = new List<string>() {
               type,resp,"0","1",idex,DateTime.Now.ToString(),GetIPAddress(),id
            };
            string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);

            return sqlinsert;
        }
        public List<dynamic> Itembill(string authen, string bill)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", authen);
           string a = url + "v1/rest/document/"+ bill + "/item?cols=lty_orig_points_earned,note4,item_path,discount_reason,is_competing_component,activity2_percent,ship_method_id,order_ship_method_id,employee3_name,item_origin,employee1_sid,employee3_full_name,central_return_commit_state,so_cancel_flag,employee2_login_name,inventory_item_type,sid,item_description3,note8,item_description1,lty_piece_of_tbe_points,hist_discount_perc5,cost,udf_string04,st_address_line3,package_sequence_number,invn_sbs_item_sid,item_status,qty_available_for_return,discounts,original_cost,st_primary,hist_discount_reason4,gift_add_value,note3,lot_type,ref_order_item_sid,gift_transaction_id,hist_discount_amt1,employee1_full_name,total_discount_reason,order_quantity_filled,st_company_name,post_date,ship_method,commission2_amount,inventory_on_hand_quantity,tax_perc_lock,dcs_code,activity3_percent,price,employee1_login_name,central_document_sid,order_ship_method_sid,employee3_login_name,tax2_amount,st_address_line2,fulfill_store_sbs_no,employee5_sid,returned_item_invoice_sid,udf_string01,udf_float01,price_before_detax,ship_id,hist_discount_reason5,employee2_name,commission_code,spif,price_lvl_name,style_sid,commission3_amount,dip_discount_amt,hist_discount_perc2,st_postal_code_extension,employee3_id,commission_percent,item_pos,st_id,activity4_percent,returned_item_qty,detax_flag,custom_flag,st_price_lvl,discount_amt,total_discount_amount,row_version,dip_tax_amount,employee5_id,activation_sid,lty_redeem_applicable_manually,order_ship_method,enhanced_item_pos,item_description2,employee5_name,manual_disc_reason,tax_code_rule2_sid,udf_string05,total_discount_percent,tax_amount,package_number,scan_upc,so_number,shipping_amt_bdt,employee3_sid,invn_item_uid,item_type,employee5_full_name,subsidiary_number,note10,tax_code_rule_sid,employee2_sid,customer_field,tax2_percent,udf_date01,st_address_line5,original_component_item_uid,ref_sale_item_pos,manual_disc_value,tax_code2,original_price_before_detax,lty_points_earned,st_last_name,fulfill_store_sid,st_country,udf_string03,st_first_name,tax_code,hist_discount_reason2,employee4_sid,price_lvl,lty_orig_price_in_points,note2,commission_level,item_lookup,vendor_code,commission5_amount,alu,schedule_number,serial_number,promotion_flag,udf_float02,so_deposit_amt,lty_price_in_points,st_security_lvl,ship_method_sid,created_datetime,hist_discount_amt3,inventory_quantity_per_case,inventory_use_quantity_decimals,commission_amount,hist_discount_perc3,st_address_line1,user_discount_percent,central_item_pos,tracking_number,st_title,ref_order_doc_sid,hist_discount_perc4,tenant_sid,kit_flag,kit_type,lot_number,discount_perc,override_max_disc_perc,modified_datetime,udf_float03,original_tax_amount,commission4_amount,hist_discount_amt4,st_detax_flag,st_tax_area_name,orig_document_number,activity5_percent,archived,package_item_uid,lty_price_in_points_ext,item_size,original_price,store_number,lty_pgm_name,order_type,employee4_id,st_price_lvl_name,attribute,lty_piece_of_tbr_points,orig_subsidiary_number,employee1_name,delete_discount,original_component_item_sid,employee2_id,item_description4,serial_type,activity_percent,employee4_login_name,gift_activation_code,gift_expire_date,tax_char,hist_discount_reason3,document_sid,hist_discount_perc1,udf_string02,created_by,ref_sale_doc_sid,shipping_amt,package_item_sid,note5,st_primary_phone_no,return_reason,employee5_login_name,note9,employee1_id,note1,lty_type,price_lvl_sid,st_address_uid,st_address_line6,apply_type_to_all_items,lty_piece_of_tbr_disc_amt,tax_area_name,promo_disc_modifiedmanually,st_cuid,note6,hist_discount_amt5,orig_sale_price,tax_area2_name,dip_price,modified_by,st_postal_code,st_email,orig_store_number,quantity,note7,hist_discount_amt2,gift_quantity,controller_sid,lty_pgm_sid,employee2_full_name,dip_tax2_amount,origin_application,employee4_full_name,employee4_name,st_tax_area2_name,hist_discount_reason1,manual_disc_type,st_address_line4,tax_percent,tax_char2,fulfill_store_no,from_centrals,st_customer_lookup,maxaccumdiscpercent,maxdiscpercent,promo_gift_item&sort=enhanced_item_pos,desc";
            var obj = webClient.DownloadString(a);
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public List<dynamic> document(string key_login, string page_no)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            string a = url + "v1/rest/document?cols=*&filter=created_datetime,ge,2019-01-01T18:23:17.000+07:00&page_no="+page_no+"&page_size=500";
            //string a = url + "v1/rest/document?cols=*&page_no=1&page_size=10";
            var obj = webClient.DownloadString(a);
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public List<dynamic> customer(string key_login, string page_no)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var urls = url + "v1/rest/customer?cols=*&page_no=" + page_no + "&page_size=1000";
           
            var obj = webClient.DownloadString(urls);
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public async void inventoryandinput(string key_login, string page_no)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var obj = webClient.DownloadString(url + "v1/rest/inventory?cols=*&page_no=1&page_size=10");
            var ldnm = JsonConvert.DeserializeObject<List<dynamic>>(obj);
            var pp = 1;
            woocomers woo = new woocomers();
            foreach (var i in ldnm.Take(1))
            {
                var a = pp;
               // await woo.inputProductvariable(i);
                pp++;
            }
        }
        public static WooCommerceNET.WooCommerce.Legacy.Attribute atr_woo(string name,List<string> vl)
        {
            WooCommerceNET.WooCommerce.Legacy.Attribute atr_ = new WooCommerceNET.WooCommerce.Legacy.Attribute();
            List<string> l_optuon = new List<string>();
            foreach(var i in vl)
            {
                l_optuon.Add(i);
            }
            atr_.name = name;
            atr_.slug = name.ToLower();
            atr_.position = 0;
            atr_.visible = true;
            atr_.variation = true;
            atr_.options = l_optuon;
            return atr_;
        }
        public static WooCommerceNET.WooCommerce.Legacy.Variation variations(string prices,string sku)
        {
            WooCommerceNET.WooCommerce.Legacy.Variation atr_ = new WooCommerceNET.WooCommerce.Legacy.Variation();
            atr_.price =decimal.Parse(prices);
            atr_.regular_price = decimal.Parse(prices);
            atr_.stock_quantity = 99;
            atr_.visible = true;
            atr_.sku = sku;

            return atr_;
        }
        public static async Task imagewoocom(string obj,Product p)
        {
            WCObject wc = new WCObject(rest);
            Image img = new Image();
            img.position = 0;
            img.src = obj;
            ImageList ImageLists = new ImageList();
            ImageLists.Add(img);
            await Task.Run(async () =>
            {
                p.images = ImageLists;
            });
            await wc.PostProduct(p);
        }
        public async Task inputwoocomCatelogy()
        {
            WCObject wc = new WCObject(rest);
            await Task.Run(async () =>
            {
                             
                Product_Category p = new Product_Category()
                {
                    name = "test",
                     slug="test",
                     parent=0
                };
                await wc.PostProductCategory(p);
            });
        }
        public async Task inputwoocomCustomer(dynamic obj)
        {
            WCObject wc = new WCObject(rest);
            await Task.Run(async () =>
            {
                var username = obj.webstoreconnect_username.ToString();
                    var password = obj.webstoreconnect_password.ToString();
                var created_at = DateTime.Now;
                var email = obj.email_address.ToString();
                var last_name = obj.last_name.ToString();
                var first_name = obj.first_name.ToString();
                //var shipping_address = obj.addresses[0];

                Customer p = new Customer()
                {
                    username= username,
                    password= password,
                    created_at=DateTime.Now,
                    email= "customer@gmail.com",
                    last_name= last_name,
                    first_name= first_name,
                    //shipping_address= obj.addresses[0]
                };
                await wc.PostCustomer(p);
            });
        }
      
        public async Task inputwoocom(dynamic obj)
        {
            WCObject wc = new WCObject(rest);
            await Task.Run(async () =>
            {
                string nme = obj.description2.ToString();
                string des = obj.description3.ToString();
                string[] code = obj.description1.ToString().Split(',');
                int prc = 0;
                try
                {
                    prc = int.Parse(obj.order_cost.ToString());
                }
                catch
                {

                }
                Image img = new Image();
                img.position = 0;
                img.src = "http://ninomaxx.cf/wp-content/uploads/2019/08/1808005-649K-BLUE-DE-1.jpg";
                ImageList ImageLists = new ImageList();
                ImageLists.Add(img);
                AttributeList L_atr = new AttributeList();
                string size = obj.item_size.ToString();
                string[] sizes = size.Split(',');
                WooCommerceNET.WooCommerce.Legacy.Attribute atr_size = atr_woo("Size", sizes.ToList());
                L_atr.Add(atr_size);
                string[] colors = obj.attribute.ToString().Split(',');
                WooCommerceNET.WooCommerce.Legacy.Attribute atr_color = atr_woo("Color", colors.ToList());
                WooCommerceNET.WooCommerce.Legacy.Attribute atr_code = atr_woo("code", code.ToList());
                L_atr.Add(atr_color);
                L_atr.Add(atr_code);
                //WooCommerceNET.WooCommerce.Legacy.Attribute atr_price = atr_woo("variable_regular_price_0", "10000000");
                string[] cates = obj.subclass_name.ToString().Split(',');
                List<object> cate = new List<object>();

                VariationList listvar = new VariationList();
                WooCommerceNET.WooCommerce.Legacy.Variation vrts = variations(obj.order_cost.ToString(), obj.upc.ToString());
                listvar.Add(vrts);
                foreach (var j in cates)
                {
                    string id = await Get_cate(j);
                    cate.Add(id);
                }

                Product p = new Product()
                {
                    type = "variable",
                    name = nme,
                    title = nme,
                    description = des,
                    //price = prc,
                    //regular_price = prc,
                    //attributes = L_atr,
                    categories = cate,
                    //sku = obj.upc,
                    visible = true,
                    purchase_note = code[0],
                    
                    variations= listvar,
                    //images= ImageLists
                };
                await wc.PostProduct(p);
            });
        }
        public async Task<string> Get_product()
        {
            
            WCObject wc = new WCObject(rest);
            var p = await wc.GetProduct(1985);
            string j = JsonConvert.SerializeObject(p);
            return j;
        }
        public async Task<string> Get_product2(int id)
        {

            WCObject wc = new WCObject(rest);
            var p = await wc.GetProduct(id);
            string j = JsonConvert.SerializeObject(p);
            return j;
        }
        public async Task<string> Get_cate(string name)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("name", name);
            int pageNumber = 1;
            dic.Add("page", pageNumber.ToString());
            WCObject wc = new WCObject(rest);
          
            var p = await wc.GetProductCategories(dic);
            string r = "-1";
            foreach(var i in p)
            {
                string n = i.name;
                if(n==name)
                {
                    r = i.id.ToString();
                    break;
                }
            }
            return r;
        }
        public static async Task Get_product_filter()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("size", "100");
            int pageNumber = 1;
            dic.Add("page", pageNumber.ToString());
            WCObject wc = new WCObject(rest);
            var p = await wc.GetProducts(dic);
            string j = JsonConvert.SerializeObject(p);
        }
        public static async Task Get_customer()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("size", "100");
            int pageNumber = 1;
            dic.Add("page", pageNumber.ToString());
            WCObject wc = new WCObject(rest);
            var p = await wc.GetProducts(dic);
            string j = JsonConvert.SerializeObject(p);
        }
    }
}