﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wooCommerce
{
    public class sqlcommon
    {
        common cmd = new common();
        string cnn = System.Configuration.ConfigurationManager.ConnectionStrings["ReportConnection"].ConnectionString;
        public bool checkdataExit(string tb, string where)
        {
            bool rs = true;
            SqlConnection conn = new SqlConnection(cnn);
            conn.Open();
            string str = "SELECT id FROM " + tb + " " + where;
            SqlCommand cmd = new SqlCommand(str, conn);
            try
            {
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                    rs = false;
            }
            catch { rs = true; }
            conn.Close();
            return rs;
        }
        public string insertsqlstr(string table, List<string> cl, List<string> vl)
        {
            string str = "INSERT INTO";
            str += " " + table + "(";
            str += string.Join(",", cl);
            str += ") values(";
            for (int i = 0; i < vl.Count; i++)
            {
                if (vl[i] == "")
                {
                    vl[i] = "null";
                }
                else if (vl[i].ToLower() == "true" || vl[i].ToLower() == "false")
                {
                    vl[i] = vl[i];
                }
                else
                {
                    try
                    {
                        int.Parse(vl[i]);
                    }
                    catch
                    {

                        vl[i] = "N'" + vl[i] + "'";
                    }
                }
            }
            str += string.Join(",", vl) + ")";
            return str;
        }
        public List<string> keyquery(string tb)
        {
            List<string> namecl = new List<string>();

            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            string str = "SELECT * FROM " + tb + " LIMIT 0";
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            con.Close();
            return namecl;
        }
        
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, SqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public bool closesql()
        {
            SqlConnection conn = new SqlConnection(cnn);
            try
            {
                conn.Close();
            }
            catch
            {

            }
            return true;
        }
        public bool runSQL(string str)
        {
            SqlConnection conn = new SqlConnection(cnn);
            conn.Open();
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.ExecuteNonQuery();
            // Execute the command
            conn.Close();
            return true;
        }
           public List<Dictionary<string, object>> getdataSQLjoinTable(string str)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            string cnn = System.Configuration.ConfigurationManager.ConnectionStrings["ReportConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
    }
}