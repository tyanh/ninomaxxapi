﻿<?php

add_action( 'wp_enqueue_scripts', 'basel_child_enqueue_styles', 1000 );

function basel_child_enqueue_styles() {

    $version = rand( 0, 999999999999 );
    
    if( basel_get_opt( 'minified_css' ) ) {

        wp_enqueue_style( 'basel-style', get_template_directory_uri() . '/style.min.css', array('bootstrap'), $version );

    } else { wp_enqueue_style( 'basel-style', get_template_directory_uri() . '/style.css', array('bootstrap'), $version );

    } wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('bootstrap'), $version );

}

add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'VND': $currency_symbol = 'đ'; break;
    }
    return $currency_symbol;
}
// Add to list of WC Order statuses
function add_checkout_completed( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-awaiting-shipment'] = 'Đã thanh toán';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_checkout_completed' );

/*
 * Add quickbuy button go to cart after click
 */

add_action('woocommerce_after_add_to_cart_button','devvn_quickbuy_after_addtocart_button');
function devvn_quickbuy_after_addtocart_button(){
    global $product;
    ?>
    <button type="button" name="add-to-cart"  class="find_add_to_cart_button button alt" id="find_inventory"><i class="fa fa-map-marker"></i>
        <?php _e('Tìm tại cửa hàng', 'devvn'); ?>
    </button>
    <?php
}
add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout($redirect_url) {
    if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {
        $redirect_url = wc_get_checkout_url();
    }
    return $redirect_url;
}


define('ICO',get_bloginfo('stylesheet_directory')."/icon");

define('THEME_PATH',dirname(__FILE__));

define('THEME_URL',get_bloginfo('stylesheet_directory'));

define('CSS',get_bloginfo('stylesheet_directory')."/dist/css");

define('JS',get_bloginfo('stylesheet_directory')."/dist/js");

define('IMG',get_bloginfo('stylesheet_directory')."/examples/css");


add_action('wp_enqueue_scripts', 'load_js_custom_flatsome');
function load_js_custom_flatsome()
{
    wp_enqueue_script(
        'ninomaxx',
        get_stylesheet_directory_uri() . '/ninomaxx.js',
        array('jquery'),
        '1.0.0',
        true
    );
    wp_enqueue_script('jquery.jInvertScroll.min.js',JS.'/jquery.jInvertScroll.min.js');
    wp_enqueue_script('jquery.jInvertScroll.js',JS.'/jquery.jInvertScroll.js');
    wp_enqueue_script('jquery.zoom.min.js',JS.'/jquery.zoom.min.js');
    wp_enqueue_script('jquery.zoom.js',JS.'/jquery.zoom.js');
}


add_action('wp_default_scripts', function ($scripts) {
    if (!empty($scripts->registered['jquery'])) {
        $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, ['jquery-migrate']);
    }
});

if( ! function_exists( 'td_basel_get_grid_el_class' ) ) {
    function td_basel_get_grid_el_class($loop = 0, $columns = 4, $different_sizes = false, $xs_size = false, $sm_size = 4, $md_size = 3) {
        $classes = '';

        if( ! in_array( $columns, array(1,2,3,4,6,12) ) ) {
            $columns = 4;
        }

        if( ! $xs_size ) {
            $xs_size = apply_filters('basel_grid_xs_default', 6);
        }

        if( $columns < 3) {
            $xs_size = 12;
            if($columns == 1)
                $sm_size = 12;
            else
                $sm_size = 6;
        }       

        $md_size = 12/$columns;

        // every third element make 2 times larger (for isotope grid)
        if( $loop % 3 == 0 )  { // ( $loop + 1 ) % 4  == 0 || 
            $md_size *= 2;
            $sm_size *= 2;
            $classes .= ' product1row ';
        }

        $col = ' col-xs-' . $xs_size . ' col-sm-' . $sm_size . ' col-md-';

        $classes .= $col . $md_size;

        if($loop > 0) {

            //testing
            $classes .= ' loop' . $loop . ' ';

            if ( 0 == ( $loop - 1 ) % $columns || 1 == $columns )
                $classes .= ' first ';
            if ( 0 == $loop % $columns )
                $classes .= ' last ';
        }

        return $classes;
    }
}

if( ! function_exists( 'td_basel_woocommerce_main_loop' ) ) {

    //add_action( 'td_basel_woocommerce_main_loop', 'td_basel_woocommerce_main_loop' );

    function td_basel_woocommerce_main_loop( $fragments = false ) {
        global $paged, $wp_query;

        $max_page = $wp_query->max_num_pages;

        if ( $fragments ) ob_start();
        
        if ( $fragments && isset( $_GET['loop'] ) ) basel_set_loop_prop( 'woocommerce_loop', sanitize_text_field( (int) $_GET['loop'] ) );
        
        if ( have_posts() ) : ?>
            
            <?php if( ! $fragments ) woocommerce_product_loop_start(); ?>
            
            <?php if ( wc_get_loop_prop( 'total' ) || $fragments ): ?>
                
                <?php while ( have_posts() ) : the_post(); ?>
                    
                    <?php wc_get_template_part( 'tdcontent', 'product' ); ?>
                    
                <?php endwhile; // end of the loop. ?>
                
            <?php endif; ?>

            <?php if( ! $fragments ) woocommerce_product_loop_end(); ?>

            <?php
                /**
                 * woocommerce_after_shop_loop hook
                 *
                 * @hooked woocommerce_pagination - 10
                 */
                if( ! $fragments ) do_action( 'woocommerce_after_shop_loop' );
                ?>

                <?php else: ?>

                    <?php 
                /**
                 * Hook: woocommerce_no_products_found.
                 *
                 * @hooked wc_no_products_found - 10
                 */
                do_action( 'woocommerce_no_products_found' );
                ?>

            <?php endif;

            if ( $fragments ) $output = ob_get_clean();

            if( $fragments ) {
                $output =  array(
                    'items' => $output,
                    'status' => ( $max_page > $paged ) ? 'have-posts' : 'no-more-posts',
                    'nextPage' => str_replace( '&', '&', next_posts( $max_page, false ) )
                );

                echo json_encode( $output );
            }
        }
    }

/*Remove product-category*/

add_filter( 'term_link', 'devvn_product_cat_permalink', 10, 3 );
function devvn_product_cat_permalink( $url, $term, $taxonomy ){
    switch ($taxonomy):
        case 'product_cat':
            $taxonomy_slug = 'product-category'; //Thay bằng slug hiện tại của bạn. Mặc định là product-category
            if(strpos($url, $taxonomy_slug) === FALSE) break;
            $url = str_replace('/' . $taxonomy_slug, '', $url);
            break;
    endswitch;
    return $url;
}
// Add our custom product cat rewrite rules
function devvn_product_category_rewrite_rules($flash = false) {
    $terms = get_terms( array(
        'taxonomy' => 'product_cat',
        'post_type' => 'product',
        'hide_empty' => false,
    ));
    if($terms && !is_wp_error($terms)){
        $siteurl = esc_url(home_url('/'));
        foreach ($terms as $term){
            $term_slug = $term->slug;
            $baseterm = str_replace($siteurl,'',get_term_link($term->term_id,'product_cat'));
            add_rewrite_rule($baseterm.'?$','index.php?product_cat='.$term_slug,'top');
            add_rewrite_rule($baseterm.'page/([0-9]{1,})/?$', 'index.php?product_cat='.$term_slug.'&paged=$matches[1]','top');
            add_rewrite_rule($baseterm.'(?:feed/)?(feed|rdf|rss|rss2|atom)/?$', 'index.php?product_cat='.$term_slug.'&feed=$matches[1]','top');
        }
    }
    if ($flash == true)
        flush_rewrite_rules(false);
}
add_action('init', 'devvn_product_category_rewrite_rules');
/*Sửa lỗi khi tạo mới taxomony bị 404*/
add_action( 'create_term', 'devvn_new_product_cat_edit_success', 10, 2 );
function devvn_new_product_cat_edit_success( $term_id, $taxonomy ) {
    devvn_product_category_rewrite_rules(true);
}

function devvn_remove_slug( $post_link, $post ) {
    if ( !in_array( get_post_type($post), array( 'product' ) ) || 'publish' != $post->post_status ) {
        return $post_link;
    }
    if('product' == $post->post_type){
        $post_link = str_replace( '/product/', '/', $post_link ); //Thay cua-hang bằng slug hiện tại của bạn
    }else{
        $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    }
    return $post_link;
}
add_filter( 'post_type_link', 'devvn_remove_slug', 10, 2 );
/*Sửa lỗi 404 sau khi đã remove slug product hoặc cua-hang*/
function devvn_woo_product_rewrite_rules($flash = false) {
    global $wp_post_types, $wpdb;
    $siteLink = esc_url(home_url('/'));
    foreach ($wp_post_types as $type=>$custom_post) {
        if($type == 'product'){
            if ($custom_post->_builtin == false) {
                $querystr = "SELECT {$wpdb->posts}.post_name, {$wpdb->posts}.ID
                            FROM {$wpdb->posts} 
                            WHERE {$wpdb->posts}.post_status = 'publish' 
                            AND {$wpdb->posts}.post_type = '{$type}'";
                $posts = $wpdb->get_results($querystr, OBJECT);
                foreach ($posts as $post) {
                    $current_slug = get_permalink($post->ID);
                    $base_product = str_replace($siteLink,'',$current_slug);
                    add_rewrite_rule($base_product.'?$', "index.php?{$custom_post->query_var}={$post->post_name}", 'top');                    
                    add_rewrite_rule($base_product.'comment-page-([0-9]{1,})/?$', 'index.php?'.$custom_post->query_var.'='.$post->post_name.'&cpage=$matches[1]', 'top');
                    add_rewrite_rule($base_product.'(?:feed/)?(feed|rdf|rss|rss2|atom)/?$', 'index.php?'.$custom_post->query_var.'='.$post->post_name.'&feed=$matches[1]','top');
                }
            }
        }
    }
    if ($flash == true)
        flush_rewrite_rules(false);
}
add_action('init', 'devvn_woo_product_rewrite_rules');
/*Fix lỗi khi tạo sản phẩm mới bị 404*/
function devvn_woo_new_product_post_save($post_id){
    global $wp_post_types;
    $post_type = get_post_type($post_id);
    foreach ($wp_post_types as $type=>$custom_post) {
        if ($custom_post->_builtin == false && $type == $post_type) {
            devvn_woo_product_rewrite_rules(true);
        }
    }
}
add_action('wp_insert_post', 'devvn_woo_new_product_post_save');





function my_ajax_filter_search_scripts() {
    wp_enqueue_script( 'my_ajax_filter_search', get_stylesheet_directory_uri(). '/ninomaxx.js', array(), '1.0', true );
    wp_localize_script( 'my_ajax_filter_search', 'ajax_url', admin_url('admin-ajax.php') );
}
// function my_ajax_filter_search_scripts() {

//     wp_enqueue_script( 'my_ajax_filter_search', get_stylesheet_directory_uri() . '/ninomaxx.js', array('jquery'),'1.0.0',
//      true );

//     wp_localize_script( 'my_ajax_filter_search', 'my_ajax_object',
//             array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
// }
// add_action( 'wp_enqueue_scripts', 'my_ajax_filter_search_scripts' );



 
// Ajax Callback
 
// add_action('wp_ajax_my_ajax_filter_search', 'my_ajax_filter_search_callback');
// add_action('wp_ajax_nopriv_my_ajax_filter_search', 'my_ajax_filter_search_callback');
 
// function my_ajax_filter_search_callback() {
 
//     header("Content-Type: application/json"); 
 
//     $meta_query = array('relation' => 'AND');
 
//     if(isset($_GET['year'])) {
//         $year = sanitize_text_field( $_GET['year'] );
//         $meta_query[] = array(
//             'key' => 'year',
//             'value' => $year,
//             'compare' => '='
//         );
//     }
 
//     if(isset($_GET['rating'])) {
//         $rating = sanitize_text_field( $_GET['rating'] );
//         $meta_query[] = array(
//             'key' => 'rating',
//             'value' => $rating,
//             'compare' => '>='
//         );
//     }
 
//     if(isset($_GET['language'])) {
//         $language = sanitize_text_field( $_GET['language'] );
//         $meta_query[] = array(
//             'key' => 'language',
//             'value' => $language,
//             'compare' => '='
//         );
//     }
 
//     $tax_query = array();
 
//     if(isset($_GET['genre'])) {
//         $genre = sanitize_text_field( $_GET['genre'] );
//         $tax_query[] = array(
//             'taxonomy' => 'category',
//             'field' => 'slug',
//             'terms' => $genre
//         );
//     }
 
//     $args = array(
//         'post_type' => 'movie',
//         'posts_per_page' => -1,
//         'meta_query' => $meta_query,
//         'tax_query' => $tax_query
//     );
 
//     if(isset($_GET['search'])) {
//         $search = sanitize_text_field( $_GET['search'] );
//         $search_query = new WP_Query( array(
//             'post_type' => 'movie',
//             'posts_per_page' => -1,
//             'meta_query' => $meta_query,
//             'tax_query' => $tax_query,
//             's' => $search
//         ) );
//     } else {
//         $search_query = new WP_Query( $args );
//     }
//     if ( $search_query->have_posts() ) {
 
//         $result = array();
 
//         while ( $search_query->have_posts() ) {
//             $search_query->the_post();
 
//             $cats = strip_tags( get_the_category_list(", ") );
//             $result[] = array(
//                 "id" => get_the_ID(),
//                 "title" => get_the_title(),
//                 "content" => get_the_content(),
//                 "permalink" => get_permalink(),
//                 "year" => get_field('year'),
//                 "rating" => get_field('rating'),
//                 "director" => get_field('director'),
//                 "language" => get_field('language'),
//                 "genre" => $cats,
//                 "poster" => wp_get_attachment_url(get_post_thumbnail_id($post->ID),'thumbnail')
//             );
//         }
//         wp_reset_query();
//         echo json_encode($result);
//     } else {
//         // no posts found
//     }
//     wp_die();
// }

// function btn_change_product() {
//     global $product;
//     echo get_field('info_change');
// }


add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +
  
function woo_custom_single_add_to_cart_text() {
  
    return __( 'THÊM VÀO GIỎ', 'woocommerce' );
  
}
function notes_in_cart() {
     global $woocommerce;

    if ( ! $_POST || ( is_admin() && ! is_ajax() ) ) {
        return;
    }

    if ( isset( $_POST['post_data'] ) ) {
        parse_str( $_POST['post_data'], $post_data );
    } else {
        $post_data = $_POST; // fallback for final checkout (non-ajax)
    }

    if ( WC()->cart->needs_shipping() ){

        // set $out_of_stock_exists to false by default
        $out_of_stock_exists = false;
        foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
            if($values['data']->backorders_allowed()){ //check if backorders are allowed on this product
                // get the stock quantity - returns the available amount number
                $stock_info = $values['data']->get_stock_quantity();

                if($stock_info < $values['quantity']){ //thanks to LoicTheAztec for pointing it out in his answer
                    // set $out_of_stock_exists to true and stop foreach execution
                    $out_of_stock_exists = true;
                    break;
                }
            }

        }

        //if cart has items out of stock
        if ($out_of_stock_exists) {
            ?>
            <tr class="ceckoutStockMeta">
                <th>Item Shipments</th>
                <td>
                    <p style="color: red;">*You have one or more items in your cart that are currently out of stock. Please select a custom shipping option for your order.</p><br>
                    <form>

                        <input type="radio" name="stockOp" id="stockOption1" value="ship" />
                        <label for="stockOption1">Ship what is available now</label><br>

                        <input type="radio" name="stockOp" id="stockOption2" value="hold" />
                        <label for="stockOption2">Wait and ship together</label>
                    </form>
                </td>
            </tr>

            <?php

        }
    }
}
add_action( 'woocommerce_cart_totals_after_order_total', 'notes_in_cart' );
add_action( 'woocommerce_review_order_after_order_total', 'notes_in_cart' );

/*
 * Add our Custom Fields to simple products
 */
function mytheme_woo_add_custom_fields() {

    global $woocommerce, $post;

    echo '<div class="options_group">';

    // Text Field
    // woocommerce_wp_text_input(
    //     array(
    //         'id'          => '_text_field',
    //         'label'       => __( 'My Text Field', 'woocommerce' ),
    //         'placeholder' => 'http://',
    //         'desc_tip'    => true,
    //         'description' => __( "Here's some really helpful tooltip text.", "woocommerce" )
    //     )
    // );

    // Number Field
    // woocommerce_wp_text_input(
    //     array(
    //         'id'                => '_number_field',
    //         'label'             => __( 'My Number Field', 'woocommerce' ),
    //         'placeholder'       => '',
    //         'desc_tip'          => false,
    //         'description'       => __( "Here's some really helpful text that appears next to the field.", 'woocommerce' ),
    //         'type'              => 'number',
    //         'custom_attributes' => array(
    //                 'step'  => 'any',
    //                 'min'   => '0'
    //             )
    //     )
    // );

    // Textarea
    // woocommerce_wp_textarea_input(
    //     array(
    //         'id'          => '_textarea',
    //         'label'       => __( 'My Textarea', 'woocommerce' ),
    //         'placeholder' => '',
    //         'desc_tip'    => true,
    //         'description' => __( "Here's some really helpful tooltip text.", "woocommerce" )
    //     )
    // );

    // Select
    // woocommerce_wp_select(
    //     array(
    //         'id'      => '_select',
    //         'label'   => __( 'My Select Field', 'woocommerce' ),
    //         'options' => array(
    //             'one'   => __( 'Option 1', 'woocommerce' ),
    //             'two'   => __( 'Option 2', 'woocommerce' ),
    //             'three' => __( 'Option 3', 'woocommerce' )
    //             )
    //     )
    // );

    // // Checkbox
    // woocommerce_wp_checkbox(
    //     array(
    //         'id'            => '_checkbox',
    //         'wrapper_class' => 'show_if_simple',
    //         'label'         => __('My Checkbox Field', 'woocommerce' ),
    //         'description'   => __( 'Check me!', 'woocommerce' )
    //     )
    // );

    // Hidden field
    woocommerce_wp_hidden_input(
        array(
            'id'    => '_hidden_field',
            'value' => 'hidden_value'
            )
    );

    // Custom field Type
    ?>
    <p class="form-field custom_field_type">
        <label for="custom_field_type"><?php echo __( 'Custom Field Type', 'woocommerce' ); ?></label>
        <span class="wrap">
            <?php $custom_field_type = get_post_meta( $post->ID, '_custom_field_type', true ); ?>
            <input placeholder="<?php _e( 'Field One', 'woocommerce' ); ?>" class="" type="number" name="_field_one" value="<?php echo $custom_field_type[0]; ?>" step="any" min="0" style="width: 100px;" />
            <input placeholder="<?php _e( 'Field Two', 'woocommerce' ); ?>" type="number" name="_field_two" value="<?php echo $custom_field_type[1]; ?>" step="any" min="0" style="width: 100px;" />
        </span>
        <span class="description"><?php _e( 'Place your own description here!', 'woocommerce' ); ?></span>
    </p>
    <?php
    echo '</div>';
}
// General Tab
//add_action( 'woocommerce_product_options_pricing', 'mytheme_woo_add_custom_fields' ); // After pricing fields
//add_action( 'woocommerce_product_options_downloads', 'mytheme_woo_add_custom_fields' ); // After downloadable file fields and only visible when it's a downloable product
//add_action( 'woocommerce_product_options_tax', 'mytheme_woo_add_custom_fields' ); // After tax fields
add_action( 'woocommerce_product_options_general_product_data', 'mytheme_woo_add_custom_fields' ); // After all General default fields
// Inventory tab
//add_action( 'woocommerce_product_options_sku', 'mytheme_woo_add_custom_fields' ); // After SKU field
//add_action( 'woocommerce_product_options_stock', 'mytheme_woo_add_custom_fields' ); // After Manage Stock field
//add_action( 'woocommerce_product_options_stock_fields', 'mytheme_woo_add_custom_fields' ); // After Manage Stock field but only visible is checked
//add_action( 'woocommerce_product_options_stock_status', 'mytheme_woo_add_custom_fields' ); // After Stock Status field
//add_action( 'woocommerce_product_options_sold_individually', 'mytheme_woo_add_custom_fields' ); // After Sold Individually field
//add_action( 'woocommerce_product_options_inventory_product_data', 'mytheme_woo_add_custom_fields' ); // After all Inventory default fields
// Shipping tab
//add_action( 'woocommerce_product_options_dimensions', 'mytheme_woo_add_custom_fields' );  // After Dimensions field
//add_action( 'woocommerce_product_options_shipping', 'mytheme_woo_add_custom_fields' ); // After all Shipping default fields
// Linked Products tab
//add_action( 'woocommerce_product_options_related', 'mytheme_woo_add_custom_fields' ); // After all Linked Products default fields
// Attributes tab
//add_action( 'woocommerce_product_options_attributes', 'mytheme_woo_add_custom_fields' ); // After all Attributes default fields
// Advanced tab
//add_action( 'woocommerce_product_options_reviews', 'mytheme_woo_add_custom_fields' ); // After Enable Reviews field
//add_action( 'woocommerce_product_options_advanced', 'mytheme_woo_add_custom_fields' ); // After all Advanced default fields
/*
 * Save our simple product fields
 */
function mytheme_woo_add_custom_fields_save( $post_id ){
    // Text Field
    $woocommerce_text_field = $_POST['_text_field'];
    update_post_meta( $post_id, '_text_field', esc_attr( $woocommerce_text_field ) );
    // Number Field
    $woocommerce_number_field = $_POST['_number_field'];
    update_post_meta( $post_id, '_number_field', esc_attr( $woocommerce_number_field ) );
    // Textarea
    $woocommerce_textarea = $_POST['_textarea'];
    update_post_meta( $post_id, '_textarea', esc_html( $woocommerce_textarea ) );
    // Select
    $woocommerce_select = $_POST['_select'];
    update_post_meta( $post_id, '_select', esc_attr( $woocommerce_select ) );
    // Checkbox
    $woocommerce_checkbox = isset( $_POST['_checkbox'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_checkbox', $woocommerce_checkbox );
    // Hidden Field
    $woocommerce_hidden_field = $_POST['_hidden_field'];
    update_post_meta( $post_id, '_hidden_field', esc_attr( $woocommerce_hidden_field ) );
    // Custom Field
    $custom_field_type =  array( esc_attr( $_POST['_field_one'] ), esc_attr( $_POST['_field_two'] ) );
    update_post_meta( $post_id, '_custom_field_type', $custom_field_type );
}
add_action( 'woocommerce_process_product_meta', 'mytheme_woo_add_custom_fields_save' );
/*
 * Add our Custom Fields to variable products
 */
function mytheme_woo_add_custom_variation_fields( $loop, $variation_data, $variation ) {
    echo '<div class="options_group form-row form-row-full">';
    // Text Field
    woocommerce_wp_text_input(
        array(
            'id'          => '_variable_text_field[' . $variation->ID . ']',
            'label'       => __( 'My Text Field', 'woocommerce' ),
            'placeholder' => 'http://',
            'desc_tip'    => true,
            'description' => __( "Here's some really helpful tooltip text.", "woocommerce" ),
            'value' => get_post_meta( $variation->ID, '_variable_text_field', true )
        )
    );
    // Add extra custom fields here as necessary...
    echo '</div>';
}
// Variations tab
//add_action( 'woocommerce_variation_options', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After variation Enabled/Downloadable/Virtual/Manage Stock checkboxes
//add_action( 'woocommerce_variation_options_pricing', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After Price fields
//add_action( 'woocommerce_variation_options_inventory', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After Manage Stock fields
//add_action( 'woocommerce_variation_options_dimensions', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After Weight/Dimension fields
//add_action( 'woocommerce_variation_options_tax', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After Shipping/Tax Class fields
//add_action( 'woocommerce_variation_options_download', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After Download fields
add_action( 'woocommerce_product_after_variable_attributes', 'mytheme_woo_add_custom_variation_fields', 10, 3 ); // After all Variation fields
/*
 * Save our variable product fields
 */
function mytheme_woo_add_custom_variation_fields_save( $post_id ){
    // Text Field
    $woocommerce_text_field = $_POST['_variable_text_field'][ $post_id ];
    update_post_meta( $post_id, '_variable_text_field', esc_attr( $woocommerce_text_field ) );
}
add_action( 'woocommerce_save_product_variation', 'mytheme_woo_add_custom_variation_fields_save', 10, 2 );
/*
 * Display our example custom field above the summary on the Single Product Page
 */
function mytheme_display_woo_custom_fields() {
    global $post;
    $ourTextField = get_post_meta( $post->ID, '_text_field', true );
    if ( !empty( $ourTextField ) ) {
        echo '<div>Our Text Field: ' . $ourTextField . '</div>';
    }
}
add_action( 'woocommerce_single_product_summary', 'mytheme_display_woo_custom_fields', 15 );



//remove Coutry billing
function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;
 
    global $paged;
    if(empty($paged)) $paged = 1;
 
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }
 
    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>« First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>‹ Previous</a>";
 
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }
 
        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next ›</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last »</a>";
        echo "</div>\n";
    }
}



register_sidebar(array(
    'name' => 'Sidebar sản phẩm',
    'id' => 'sidebar_sanpham',
    'description' => 'Khu vực sidebar hiển thị bên phải bài viết và danh mục bài viết',
    'before_widget' => '<aside id="%1$s" class="widget_aside %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<p class="widget_title">',
    'after_title' => '</p>'
));


/*Sắp xếp lại thứ tự các field*/
add_filter("woocommerce_checkout_fields", "order_fields");
function order_fields($fields) {
 
  //Shipping
  $order_shipping = array(
    "shipping_first_name",
    "shipping_last_name",
    "shipping_phone",
    "shipping_address_1"
  );
  foreach($order_shipping as $field_shipping)
  {
    $ordered_fields2[$field_shipping] = $fields["shipping"][$field_shipping];
  }
  $fields["shipping"] = $ordered_fields2;
  return $fields;
}
 
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields',99 );
function custom_override_checkout_fields( $fields ) {
  unset($fields['billing']['billing_company']);
  // unset($fields['billing']['billing_first_name']);
  unset($fields['billing']['billing_postcode']);
  unset($fields['billing']['billing_country']);
  unset($fields['billing']['billing_city']);
  unset($fields['billing']['billing_state']);
  unset($fields['billing']['billing_address_2']);
  // $fields['billing']['billing_last_name'] = array(
  //   'label' => __('Họ và tên', 'devvn'),
  //   'placeholder' => _x('Nhập đầy đủ họ và tên của bạn', 'placeholder', 'devvn'),
  //   'required' => true,
  //   'class' => array('form-row-wide'),
  //   'clear' => true
  // );
  $fields['billing']['billing_address_1']['placeholder'] = 'Ví dụ: Số xx Ngõ xx Phú Kiều, Bắc Từ Liêm, Hà Nội';
 
  unset($fields['shipping']['shipping_company']);
  unset($fields['shipping']['shipping_postcode']);
  unset($fields['shipping']['shipping_country']);
  unset($fields['shipping']['shipping_city']);
  unset($fields['shipping']['shipping_state']);
  unset($fields['shipping']['shipping_address_2']);
 
  $fields['shipping']['shipping_phone'] = array(
    'label' => __('Điện thoại', 'devvn'),
    'placeholder' => _x('Số điện thoại người nhận hàng', 'placeholder', 'devvn'),
    'required' => true,
    'class' => array('form-row-wide'),
    'clear' => true
  );
  // $fields['shipping']['shipping_last_name'] = array(
  //   'label' => __('Họ và tên', 'devvn'),
  //   'placeholder' => _x('Nhập đầy đủ họ và tên của người nhận', 'placeholder', 'devvn'),
  //   'required' => true,
  //   'class' => array('form-row-wide'),
  //   'clear' => true
  // );
  $fields['shipping']['shipping_address_1']['placeholder'] = 'Ví dụ: Số xx Ngõ xx Phú Kiều, Bắc Từ Liêm, Hà Nội';
 
  return $fields;
}
 
// add_action( 'woocommerce_admin_order_data_after_shipping_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
// function my_custom_checkout_field_display_admin_order_meta($order){
//   echo '<p><strong>'.__('Số ĐT người nhận').':</strong> <br>' . get_post_meta( $order->id, '_shipping_phone', true ) . '</p>';
// }


function wpb_hook_javascript() {
    return;
  if ($order) { 
    ?>
        <script type="text/javascript">
                alert('Xin chào');
                </script>
    <?php
  }
}
add_action('wp_head', 'wpb_hook_javascript');


/*
add_action( 'woocommerce_after_shop_loop_item', 'bbloomer_show_stock_shop', 10 );
  
function bbloomer_show_stock_shop() {
   global $product;
   echo wc_get_stock_html( $product );
}*/

// Add payment method to order email
 
add_action( 'woocommerce_email_after_order_table', 'wc_add_payment_type_to_emails', 15, 2 );
 
function wc_add_payment_type_to_emails( $order, $is_admin_email ) {
echo '<h2>Phương thức thanh toán:</h2><p> ' . $order->payment_method_title . '</p>';
}


add_action( 'woocommerce_cart_collaterals', 'remove_cart_totals', 9 );
function remove_cart_totals(){
    // Remove cart totals block
    remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );

    // Add back "Proceed to checkout" button (and hooks)
    echo '<div class="cart_totals">';
    do_action( 'woocommerce_before_cart_totals' );

    echo '<div class="wc-proceed-to-checkout">';
    do_action( 'woocommerce_proceed_to_checkout' );
    echo '</div>';

    do_action( 'woocommerce_after_cart_totals' );
    echo '</div><br clear="all">';
}
/*add_action( 'woocommerce_before_add_to_cart_quantity', 'bbloomer_display_dropdown_variation_add_cart' );
 
function bbloomer_display_dropdown_variation_add_cart() {
    
   global $product;
    
   if ( $product->is_type('variable') ) {
       
      ?>
      <script>
      jQuery(document).ready(function($) {
          
         $('input.variable_sku').change( function(){
            if( '' != $('input.variable_sku').val() ) {
                
               var var_id = $('input.variable_sku').val();
               alert('You just selected variation #' + var_id);
                
            }
         });
          
      });
      </script>
      <?php
    
   }
    
}*/


function cw_woo_attribute(){
    global $product;
    $attributes = $product->get_attributes();
    if ( ! $attributes ) {
        return;
    }
    $display_result = '';
    foreach ( $attributes as $attribute ) {
        if ( $attribute->get_variation() ) {
            continue;
        }
        $name = $attribute->get_name();
        if ( $attribute->is_taxonomy() ) {
            $terms = wp_get_post_terms( $product->get_id(), $name, 'all' );
            $cwtax = $terms[0]->taxonomy;
            $cw_object_taxonomy = get_taxonomy($cwtax);
            if ( isset ($cw_object_taxonomy->labels->singular_name) ) {
                $tax_label = $cw_object_taxonomy->labels->singular_name;
            } elseif ( isset( $cw_object_taxonomy->label ) ) {
                $tax_label = $cw_object_taxonomy->label;
                if ( 0 === strpos( $tax_label, 'Product ' ) ) {
                    $tax_label = substr( $tax_label, 8 );
                }
            }
            $display_result .= $tax_label . ': ';
            $tax_terms = array();
            foreach ( $terms as $term ) {
                $single_term = esc_html( $term->name );
                array_push( $tax_terms, $single_term );
            }
            $display_result .= implode(', ', $tax_terms) .  '<br />';
        } else {
            $display_result .= $name . ': ';
            $display_result .= esc_html( implode( ', ', $attribute->get_options() ) ) . '<br />';
        }
    }
    echo $display_result;
}
add_action('woocommerce_single_product_summary', 'cw_woo_attribute', 25);

add_filter( 'use_block_editor_for_post', '__return_false' );


function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
      if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
             $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
      }
      if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
             $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
      }
         return $validation_errors;
}
add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );


/**
* Below code save extra fields.
*/
function wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
                 // Phone input filed which is used in WooCommerce
                 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
          }
      if ( isset( $_POST['billing_first_name'] ) ) {
             //First name field which is by default
             update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
             // First name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
             // Last name field which is by default
             update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

// function cw_change_product_price_display( $price ) {
// global $product;
//         $sku = $product -> get_sku();
//     $price .= '<br/> <div class=""> Mã : <strong>'. $sku.'</strong></div>';
//     return $price;
// }
// add_filter( 'woocommerce_get_price_html', 'cw_change_product_price_display' );
// add_filter( 'woocommerce_cart_item_price', 'cw_change_product_price_display' );


add_action( 'woocommerce_single_product_summary', 'custom_text', 15 );
function custom_text() {
    global $product;
        $sku = $product -> get_sku();

  print '<p class="custom_text" id="fc_mtk" data="'.$sku.'">Mã hàng: '.$sku.'</p>';  
}


function custom_loginlogo() {
echo '<style type="text/css">
h1 a {background-image: url("https://ninomaxx.com.vn/wp-content/themes/basel/images/logo.png") !important;width:200px!important;background-size:100%!important;}
</style>';
}
add_action('login_head', 'custom_loginlogo');



/**
 * Change woocommerce_ajax_variation_threshold
 */
function custom_wc_ajax_variation_threshold( $qty, $product ) {
    return 200;
}
add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 100, 2 );




  
add_action( 'woocommerce_register_form', 'bbloomer_extra_register_select_field' );
  
function bbloomer_extra_register_select_field() {
    
    ?>
  
<p class="form-row form-row-wide">
<label for="gender">Giới tính</label>
<select name="find_where" id="find_where" />
    <option value="goo">Nam</option>
    <option value="fcb">Nữ</option>
</select>
</p>
<p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>
<?php
    
}
  
// -------------------
// 2. Save field on Customer Created action
  
add_action( 'woocommerce_created_customer', 'bbloomer_save_extra_register_select_field' );
   
function bbloomer_save_extra_register_select_field( $customer_id ) {
if ( isset( $_POST['find_where'] ) ) {
        update_user_meta( $customer_id, 'find_where', $_POST['find_where'] );
}
}
  
// -------------------
// 3. Display Select Field @ User Profile (admin) and My Account Edit page (front end)
   
add_action( 'show_user_profile', 'bbloomer_show_extra_register_select_field', 3 );
add_action( 'edit_user_profile', 'bbloomer_show_extra_register_select_field', 3); 
add_action( 'woocommerce_edit_account_form', 'bbloomer_show_extra_register_select_field', 3 );
   
function bbloomer_show_extra_register_select_field($user){ 
    
  if (empty ($user) ) {
  $user_id = get_current_user_id();
  $user = get_userdata( $user_id );
  }
    
?>    
        
<p class="form-row form-row-wide">
<label for="gender">Giới tính</label>
<select name="find_where" id="find_where"  style="line-height: 15px;" />
    <option value="goo" <?php if (get_the_author_meta( 'find_where', $user->ID ) == "goo") echo 'selected="selected" '; ?>>Nam</option>
    <option value="fcb" <?php if (get_the_author_meta( 'find_where', $user->ID ) == "fcb") echo 'selected="selected" '; ?>>Nữ</option>
</select>
</p>
  
<?php
  
}
  
// -------------------
// 4. Save User Field When Changed From the Admin/Front End Forms
   
add_action( 'personal_options_update', 'bbloomer_save_extra_register_select_field_admin' );    
add_action( 'edit_user_profile_update', 'bbloomer_save_extra_register_select_field_admin' );   
add_action( 'woocommerce_save_account_details', 'bbloomer_save_extra_register_select_field_admin' );
   
function bbloomer_save_extra_register_select_field_admin( $customer_id ){
if ( isset( $_POST['find_where'] ) ) {
   update_user_meta( $customer_id, 'find_where', $_POST['find_where'] );
    }
}


add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}


// Customizes the WooCommerce product sorting options
// Available options are: menu_order, rating, date, popularity, price, price-desc
// function custom_woocommerce_product_sorting( $orderby ) {
  // The following removes the rating, date, and the popularity sorting options;
  // feel free to customize and enable/disable the options as needed. 
  // unset($orderby["rating"]);
  // unset($orderby["date"]);
  // unset($orderby["popularity"]);
  // return $orderby;
// }
// add_filter( "woocommerce_catalog_orderby", "custom_woocommerce_product_sorting", 20 );




add_filter( 'woocommerce_variation_option_name', 'display_price_in_variation_option_name' );

function display_price_in_variation_option_name( $term ) {
    global $wpdb, $product;

    if ( empty( $term ) ) return $term;
    if ( empty( $product->id ) ) return $term;

    $id = $product->get_id();

    $result = $wpdb->get_col( "SELECT slug FROM {$wpdb->prefix}terms WHERE name = '$term'" );

    $term_slug = ( !empty( $result ) ) ? $result[0] : $term;

    $query = "SELECT postmeta.post_id AS product_id
                FROM {$wpdb->prefix}postmeta AS postmeta
                    LEFT JOIN {$wpdb->prefix}posts AS products ON ( products.ID = postmeta.post_id )
                WHERE postmeta.meta_key LIKE 'attribute_%'
                    AND postmeta.meta_value = '$term_slug'
                    AND products.post_parent = $id";

    $variation_id = $wpdb->get_col( $query );
    $parent = wp_get_post_parent_id( $variation_id[0] );
        // var_dump($parent);

    if ( $parent > 0 ) {
         $_product = new WC_Product_Variation( $variation_id[0] );

            $_product->get_sku();
    }

    return $term;
    
}
 /* === Add Thumbnails to Posts/Pages List === */
// if ( !function_exists('o99_add_thumbs_column_2_list') && function_exists('add_theme_support') ) {

//     //  // set your post types , here it is post and page...
//     add_theme_support('post-thumbnails', array( 'post', 'page' ) );

//     function o99_add_thumbs_column_2_list($cols) {

//         $cols['thumbnail'] = __('Thumbnail');

//         return $cols;
//     }

//     function o99_add_thumbs_2_column($column_name, $post_id) {

//             $w = (int) 60;
//             $h = (int) 60;

//             if ( 'thumbnail' == $column_name ) {
//                 // back comp x WP 2.9
//                 $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
//                 // from gal
//                 $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
//                 if ($thumbnail_id)
//                     $thumb = wp_get_attachment_image( $thumbnail_id, array($w, $h), true );
//                 elseif ($attachments) {
//                     foreach ( $attachments as $attachment_id => $attachment ) {
//                         $thumb = wp_get_attachment_image( $attachment_id, array($w, $h), true );
//                     }
//                 }
//                     if ( isset($thumb) && $thumb ) {
//                         echo $thumb;
//                     } else {
//                         echo __('None');
//                     }
//             }
//     }

//     // for posts
//     add_filter( 'manage_posts_columns', 'o99_add_thumbs_column_2_list' );
//     add_action( 'manage_posts_custom_column', 'o99_add_thumbs_2_column', 10, 2 );

//     // for pages
//     add_filter( 'manage_pages_columns', 'o99_add_thumbs_column_2_list' );
//     add_action( 'manage_pages_custom_column', 'o99_add_thumbs_2_column', 10, 2 );
// }


add_action( 'woocommerce_before_add_to_cart_form', 'bbloomer_show_sale_percentage_loop', 25 );
  
function bbloomer_show_sale_percentage_loop() {
   global $product;
   if ( ! $product->is_on_sale() ) return;
   if ( $product->is_type( 'simple' ) ) {
      $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
   } elseif ( $product->is_type( 'variable' ) ) {
      $max_percentage = 0;
      foreach ( $product->get_children() as $child_id ) {
         $variation = wc_get_product( $child_id );
         $price = $variation->get_regular_price();
         $sale = $variation->get_sale_price();
         if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
         if ( $percentage > $max_percentage ) {
            $max_percentage = $percentage;
         }
      }
   }
   if ( $max_percentage > 0 ) echo "<div class='sale-perc'>-" . round($max_percentage) . "%</div>"; 
}

// add_action('wp_head','shareImage');
// function shareImage($args){
//      global $product;
//      if($product!=NULL){
//         $attributes=$product->get_variation_attributes("pa_colors");
//          if ($attributes) {
//             $pa_colors_=$attributes["pa_color"];
//              $pa_colors=str_replace(order_replace, replace, strtoupper($pa_colors_[0]));
//              $skumt=$product->get_sku();
//              $imgs_=host_img.$skumt."/".$skumt."_".$pa_colors."_1.jpg";
//              echo '<meta property="og:image" content="'.$imgs_.'" />';
//              echo '<meta property="og:image:secure_url" content="'.$imgs_.'" />';
//              echo '<meta property="og:image:width" content="400">';
//              echo '<meta property="og:image:height" content="400">';
//          }
//     }
     
// }

// // Add a new custom column to admin order list
// add_filter( 'manage_edit-shop_order_columns', 'admin_orders_list_add_column', 10, 1 );
// function admin_orders_list_add_column( $columns ){
//     $columns['custom_column'] = __( 'New Column', 'woocommerce' );

//     return $columns;
// }

// // The data of the new custom column in admin order list
// add_action( 'manage_shop_order_posts_custom_column' , 'admin_orders_list_column_content', 10, 2 );
// function admin_orders_list_column_content( $column, $post_id ){
//     global $the_order;

//     if( 'custom_column' === $column ){
//         $count = 0;

//         // Loop through order items
//         foreach( $the_order->get_items() as $item ) {
//             $product = $item->get_product(); // The WC_Product Object
//             $style   = $count > 0 ? ' style="padding-left:6px;"' : '';

//             // Display product thumbnail
//             printf( '<span%s>%s</span>', $style, $product->get_image( array( 50, 50 ) ) );

//             $count++;
//         }
//     }
// }


add_action( 'woocommerce_single_product_summary', 'bbloomer_show_tags_again_single_product', 40 );
 
function bbloomer_show_tags_again_single_product() {
   global $product;
   ?>
   <div class="product_meta">
   <?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?> 
   </div>
   <?php
}
// add_action('wp_head','shareImage');
// function shareImage($args){

//   global $product;

//   if($product!=NULL){

//   $attributes=$product->get_variation_attributes("pa_colors");
//   $pa_colors_=$attributes["pa_color"];
//   $pa_colors=str_replace(order_replace, replace, strtoupper($pa_colors_[0]));
//   $skumt=$product->get_sku();
//   $imgs_=host_img.$skumt."/".$skumt."_".$pa_colors."_1.jpg";
//      echo '<meta property="og:image" content="'.$imgs_.'" />';
//   }
//  }


    
    

/**
 * Change the placeholder image
 */
// add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

// function custom_woocommerce_placeholder_img_src( $src ) {
//     $upload_dir = wp_upload_dir();
//     $uploads = untrailingslashit( $upload_dir['baseurl'] );
//     // replace with path to your image
//     $src = '';
     
//     return $src;
// }




 add_filter( 'woocommerce_product_get_image', 'offsite_product_images', 10, 5 );
 function offsite_product_images( $image, $product, $size, $attr, $placeholder ){
     if (strpos($image, 'placeholder-') !== false || !$image) {
         $check_product = $product;
         if ($product->is_type( 'variation' )) {
             $check_product = wc_get_product($product->get_parent_id());
         }
         $color=$check_product->get_attribute( 'color' );
         if ($color) {
             $iparr = explode("|", $color);
             $fr_img =explode(",",str_replace(' ', '_', trim($iparr[0]," ")));
             $replace = array(".","/");
             $fr_img_attr=str_replace($replace, '_', trim($fr_img[0]));
             $sku_=$check_product->get_sku();
             $image_url = host_img.$sku_."/".$sku_."_".$fr_img_attr."_1.jpg";
             $image = '<img width="auto" height="auto" class="h_'.$sku_.'" src="'.$image_url.'"/>';
         }
     }
     return $image;
 }


add_filter( 'manage_edit-product_columns', 'misha_brand_column', 20 );
function misha_brand_column( $columns_array ) {
    // I want to display Brand column just after the product name column
    return array_slice( $columns_array, 0, 2, true )
    + array( 'custom_thumb' => 'Ảnh' )
    + array_slice( $columns_array, 2, NULL, true );
}

add_filter( 'manage_edit-product_columns', 'misha_remove_default_thumb', 21 );
function misha_remove_default_thumb( $columns_array ) {
    // I want to display Brand column just after the product name column
    unset($columns_array['thumb']);
    return $columns_array;
}
 
add_action( 'manage_posts_custom_column', 'misha_populate_brands' );
function misha_populate_brands( $column_name ) {
    if( $column_name  == 'custom_thumb' ) {
        // if you suppose to display multiple brands, use foreach();
        global $product;
        $color=$product->get_attribute( 'color' );
        if ($color) {
            $iparr = explode("|", $color);
            $fr_img =explode(",",str_replace(' ', '_', trim($iparr[0]," ")));
            $replace = array(".","/");
            $fr_img_attr=str_replace($replace, '_', trim($fr_img[0]));
            $sku_=$product->get_sku();
            $host = host_img.$sku_."/".$sku_."_".$fr_img_attr."_1.jpg";
            echo '<img width="100px" height="auto" class="h_'.$sku_.'" src="'.$host.'"/>';
        } else {
            $url = wc_placeholder_img_src( 'woocommerce_thumbnail' );
            echo '<img width="100px" height="auto" src="'.$url.'"/>';
        }
        
    }
}

//add_filter( 'woocommerce_placeholder_img_src', 'magik_custom_woocommerce_placeholder', 10 );
//
////  Function to return new placeholder image URL.
//function magik_custom_woocommerce_placeholder( $image_url )
//{
//    global $product;
//    $check_product = $product;
//    if ($product->is_type( 'variation' )) {
//        $check_product = wc_get_product($product->get_parent_id());
//    }
//    $color=$check_product->get_attribute( 'color' );
//    if ($color) {
//        $iparr = explode("|", $color);
//        $fr_img =explode(",",str_replace(' ', '_', trim($iparr[0]," ")));
//        $replace = array(".","/");
//        $fr_img_attr=str_replace($replace, '_', trim($fr_img[0]));
//        $sku_=$check_product->get_sku();
//        $image_url = host_img.$sku_."/".$sku_."_".$fr_img_attr."_1.jpg";
//    }
//    // change  this to the URL to your custom placeholder
//    return $image_url;
//}

add_action('woocommerce_after_order_notes', 'customise_checkout_field');
function customise_checkout_field($checkout)
{
    echo '<div id="td_checkout_notice" class="tt_checkout_notice" style="display: none; color: red;border:1px solid #e7e7e7;"><h2 style="color: red; text-align: center;">' . __('Thông báo') . '</h2>';
?>
    <script>
        jQuery(function ($) {
            var listCity = [725, 724, 726, 731, 740, 742, 737, 803, 804, 807, 802];
            var currentId = $("#billing_city").val();
            if (listCity.includes(parseInt(currentId))) {
                $("#td_checkout_notice").show();
            } else {
                $("#td_checkout_notice").hide();
            }

            $("#billing_city").on('change', function() {
                if (listCity.includes(parseInt(this.value))) {
                    $("#td_checkout_notice").show();
                } else {
                    $("#td_checkout_notice").hide();
                }
            });
            $("#billing_state").on('change', function() {
                $("#td_checkout_notice").hide();
            });
        });
    </script>
    <!--<style>
        #td_checkout_notice p {
            margin-bottom: 0;
            font-size: 130%;
            font-weight: bold;
            text-align: justify;
        }
    </style>-->
    <style type="text/css">
        .tt_checkout_notice table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
          border:1px solid #e7e7e7;
          padding: 10px 6px!important;
          margin-bottom: 15px!important;
        }

        .tt_checkout_notice td, th {
          border: 1px solid #e7e7e7;
          text-align: left;
          padding: 8px;
        }

        .tt_checkout_notice tr:nth-child(even) {
          background-color: #dddddd;
        }
    </style>
    <p>Kính chào quý khách,</p>
    <p>Do nhu cầu nâng cấp vận hành để đảm bảo cung cấp dịch vụ giao vận tốt nhất, chúng tôi sẽ tiến hành tạm dừng dịch vụ Giao hàng tại một số khu vực thuộc tỉnh Bình Dương, Long An và Đồng Nai.</p>
    <p>Cụ thể:</p>
     <table>
        <tr>
            <th>Bình Dương</th>
            <th>Đồng Nai</th>
            <th>Long An</th>
        </tr>
        <tr>
            <td>TP. Thuận An</td>
            <td>TP. Biên Hoà</td>
            <td>Huyện Bến Lức</td>
        </tr>
        <tr>
            <td>TP. Dĩ An</td>
            <td>Huyện Long Thành</td>
            <td>Huyện Thủ Thừa</td>
        </tr>
        <tr>
            <td>Thị xã Tân Uyên</td>
            <td>Huyện Nhơn Trạch</td>
            <td>Huyện Cần Giuộc</td>
        </tr>
        <tr>
            <td></td>
            <td>Huyện Trảng Bom</td>
            <td>Huyện Đức Hoà</td>
        </tr>
    </table>
    <p>Thời gian tạm dừng dịch vụ: Từ 00h 25/4 - hết 01/05</p>
    <p>Những đơn hàng trong thời gian tạm dừng, chúng tôi sẽ giao vào ngày 02/05.</p>
    <p>Chúng tôi mong quý khách thông cảm về sự bất tiện trên và kính mong được sự ủng hộ từ quý khách sau thời gian trên.</p>
    <p>Trân trọng!</p>


 
<?php
    echo '</div>';
}

// Filter SKU
// function sv_add_sku_sorting( $args ) {

//  $orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

//  if ( 'sku' == $orderby_value ) {
//      $args['orderby'] = 'meta_value';
//      $args['order'] = 'desc'; // lists SKUs alphabetically 0-9, a-z; change to desc for reverse alphabetical
//      $args['meta_key'] = '_sku';
//  }

//  return $args;
// }
// add_filter( 'woocommerce_get_catalog_ordering_args', 'sv_add_sku_sorting' );

// function sv_sku_sorting_orderby( $sortby ) {

//     // Change text above as desired; this shows in the sorting dropdown
//     $sortby['sku'] = __( 'Mới nhất', 'textdomain' );

//     return $sortby;
// }
// add_filter( 'woocommerce_catalog_orderby', 'sv_sku_sorting_orderby' );
// add_filter( 'woocommerce_default_catalog_orderby_options', 'sv_sku_sorting_orderby' );

//End Filter By SKU




add_filter( 'woocommerce_catalog_orderby', 'misha_remove_default_sorting_options' ); 
function misha_remove_default_sorting_options( $options ){
    // unset( $options[ 'popularity' ] );
    //unset( $options[ 'menu_order' ] );
    //unset( $options[ 'rating' ] );
    // unset( $options[ 'date' ] );
    //unset( $options[ 'price' ] );
    //unset( $options[ 'price-desc' ] );
    return $options;
}


/**
 * This code should be added to functions.php of your theme
 **/
add_filter('woocommerce_default_catalog_orderby', 'custom_default_catalog_orderby');

function custom_default_catalog_orderby() {
     return 'date'; // Can also use title and price
}




add_filter('woocommerce_get_catalog_ordering_args', 'am_woocommerce_catalog_orderby');
function am_woocommerce_catalog_orderby( $args ) {
    $args['meta_key'] = '_sku';
    $args['orderby'] = 'meta_value';
    $args['order'] = 'desc'; 
    return $args;
}



function array_move($key, $new_index, $array)
{
    if($new_index < 0) return;
    if($new_index >= count($array)) return;
    if(!array_key_exists($key, $array)) return;

    $ret = array();
    $ind = 0;
    foreach($array as $k => $v)
    {
      if($new_index == $ind)
      {
        $ret[$key] = $array[$key];
        $ind++;
      }
      if($k != $key)
      {
        $ret[$k] = $v;
        $ind++;
      }
    }
    // one last check for end indexes
    if($new_index == $ind)
        $ret[$key] = $array[$key];

    return $ret;
}

add_filter('woocommerce_checkout_fields', 'woocommerce_checkout_fields_mod', 10, 1);
function woocommerce_checkout_fields_mod($fields) {
    $fields['billing']['billing_gender'] = array(
            'label' => 'Giới tính',
            'placeholder' => 'Giới tính',
            'type' => 'select',
            'options', 'options' => array(
                ''      => 'Chọn giới tính',
                'male' => 'Nam',
                'female' => 'Nữ',
            ),
            'required' => true,
            'class' => array( 'form-row-wide', 'gender-field')
        );

    $fields['billing'] = array_move('billing_gender', 3, $fields['billing']);
    return $fields;
}
class mishaDateRange{
 
    function __construct(){
 
        // if you do not want to remove default "by month filter", remove/comment this line
        add_filter( 'months_dropdown_results', '__return_empty_array' );
 
        // include CSS/JS, in our case jQuery UI datepicker
        add_action( 'admin_enqueue_scripts', array( $this, 'jqueryui' ) );
 
        // HTML of the filter
        add_action( 'restrict_manage_posts', array( $this, 'form' ) );
 
        // the function that filters posts
        add_action( 'pre_get_posts', array( $this, 'filterquery' ) );
 
    }
 
    /*
     * Add jQuery UI CSS and the datepicker script
     * Everything else should be already included in /wp-admin/ like jquery, jquery-ui-core etc
     * If you use WooCommerce, you can skip this function completely
     */
    function jqueryui(){
        wp_enqueue_style( 'jquery-ui', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
    }
 
    /*
     * Two input fields with CSS/JS
     * If you would like to move CSS and JavaScript to the external file - welcome.
     */
    function form(){
 
        $from = ( isset( $_GET['mishaDateFrom'] ) && $_GET['mishaDateFrom'] ) ? $_GET['mishaDateFrom'] : '';
        $to = ( isset( $_GET['mishaDateTo'] ) && $_GET['mishaDateTo'] ) ? $_GET['mishaDateTo'] : '';
 
        echo '<style>
        input[name="mishaDateFrom"], input[name="mishaDateTo"]{
            line-height: 28px;
            height: 28px;
            margin: 0;
            width:125px;
        }
        </style>
 
        <input type="text" name="mishaDateFrom" placeholder="Từ ngày" value="' . esc_attr( $from ) . '" />
        <input type="text" name="mishaDateTo" placeholder="Đến ngày" value="' . esc_attr( $to ) . '" />
 
        <script>
        jQuery( function($) {
            var from = $(\'input[name="mishaDateFrom"]\'),
                to = $(\'input[name="mishaDateTo"]\');
 
            $( \'input[name="mishaDateFrom"], input[name="mishaDateTo"]\' ).datepicker( {dateFormat : "yy-mm-dd"} );
            // by default, the dates look like this "April 3, 2017"
                // I decided to make it 2017-04-03 with this parameter datepicker({dateFormat : "yy-mm-dd"});
 
 
                // the rest part of the script prevents from choosing incorrect date interval
                from.on( \'change\', function() {
                to.datepicker( \'option\', \'minDate\', from.val() );
            });
 
            to.on( \'change\', function() {
                from.datepicker( \'option\', \'maxDate\', to.val() );
            });
 
        });
        </script>';
 
    }
 
    /*
     * The main function that actually filters the posts
     */
    function filterquery( $admin_query ){
        global $pagenow;
 
        if (
            is_admin()
            && $admin_query->is_main_query()
            // by default filter will be added to all post types, you can operate with $_GET['post_type'] to restrict it for some types
            && in_array( $pagenow, array( 'edit.php', 'upload.php' ) )
            && ( ! empty( $_GET['mishaDateFrom'] ) || ! empty( $_GET['mishaDateTo'] ) )
        ) {
 
            $admin_query->set(
                'date_query', // I love date_query appeared in WordPress 3.7!
                array(
                    'after' => sanitize_text_field( $_GET['mishaDateFrom'] ), // any strtotime()-acceptable format!
                    'before' => sanitize_text_field( $_GET['mishaDateTo'] ),
                    'inclusive' => true, // include the selected days as well
                    'column'    => 'post_date' // 'post_modified', 'post_date_gmt', 'post_modified_gmt'
                )
            );
 
        }
 
        return $admin_query;
 
    }
 
}
new mishaDateRange();


// add_action( 'woocommerce_product_query', 'so_20990199_product_query' );

// function so_20990199_product_query( $q ){

//     $product_ids_on_sale = wc_get_product_ids_on_sale();

//     $q->set( 'post__in', $product_ids_on_sale );

// }



// function wc_add_featured_product_flash() {
// 	global $product;

// if( $product->is_type( 'variable' ) ){ 

// $x =  $product->get_price();;
// $y = 10000;
// $result = fmod($x,$y);
// // $result equals 1, because 2 * 3 + 1 = 7

//     // $label_giamgia =  $product->get_price();

//     // $sodu = $label_giamgia%1000;
// 	if ($result==9000){
// 		echo '<span class="onsale_freeship">Free Ship</span>';
// 	}else {
//         echo '';
//     }
// }
// }
// add_action( 'woocommerce_before_shop_loop_item_title', 'wc_add_featured_product_flash' );
// add_action( 'woocommerce_before_single_product_summary', 'wc_add_featured_product_flash' );




//Max value discount in coupon Stephen dev




add_action( 'woocommerce_coupon_options_usage_limit', 'woocommerce_coupon_options_usage_limit', 10, 2 );
function woocommerce_coupon_options_usage_limit( $coupon_id, $coupon ){
    echo '
';
    // max discount per coupons
    $max_discount =  get_post_meta( $coupon_id, '_max_discount', true );
    woocommerce_wp_text_input( array(
        'id'                => 'max_discount',
        'label'             => __( 'Giá tiền tối đa/coupon', 'woocommerce' ),
        'placeholder'       => esc_attr__( 'Số tiền tối đa khi sử dụng Coupon', 'woocommerce' ),
        'description'       => __( 'The maximum discount this coupon can give.', 'woocommerce' ),
        'type'              => 'number',
        'desc_tip'          => true,
        'class'             => 'short',
        'custom_attributes' => array(
            'step'  => 1,
            'min'   => 0,
        ),
        'value' => $max_discount ? $max_discount : '',
    ) );
    echo '
';
}
add_action( 'woocommerce_coupon_options_save', 'woocommerce_coupon_options_save', 10, 2 );
function woocommerce_coupon_options_save( $coupon_id, $coupon ) {
    update_post_meta( $coupon_id, '_max_discount', wc_format_decimal( $_POST['max_discount'] ) );
}




add_filter( 'woocommerce_coupon_get_discount_amount', 'woocommerce_coupon_get_discount_amount', 20, 5 );
function woocommerce_coupon_get_discount_amount( $discount, $discounting_amount, $cart_item, $single, $coupon ) {
    $max_discount = get_post_meta( $coupon->get_id(), '_max_discount', true );
    if ( is_numeric( $max_discount ) && ! is_null( $cart_item ) && WC()->cart->subtotal_ex_tax ) {
        $cart_item_qty = is_null( $cart_item ) ? 1 : $cart_item['quantity'];
        if ( wc_prices_include_tax() ) {
            $discount_percent = ( wc_get_price_including_tax( $cart_item['data'] ) * $cart_item_qty ) / WC()->cart->subtotal;
        } else {
            $discount_percent = ( wc_get_price_excluding_tax( $cart_item['data'] ) * $cart_item_qty ) / WC()->cart->subtotal_ex_tax;
        }
        $_discount = $max_discount * $discount_percent ;
        $discount = min( $_discount, $discount );
    }
    return $discount;
}
