﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Webhook.Models;

namespace Webhook.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZaloApiController : ControllerBase
    {
        Fileconfig config = new Fileconfig();
        [HttpPost("webzalo")]
        public void webzalo(dynamic data)
        {
            if (data.event_name.ToString() == "follow")
            {
                var follower = JsonConvert.DeserializeObject<dynamic>(data.follower.ToString());
                sendregisterzalo(follower.id.ToString());
            }

            else if (data.event_name.ToString() == "user_send_text")
            {
                var message = JsonConvert.DeserializeObject<dynamic>(data.message.ToString());
                if (message.text.ToString().ToLower() == "#dangky")
                {
                    var sender = JsonConvert.DeserializeObject<dynamic>(data.sender.ToString());
                    var recipient = JsonConvert.DeserializeObject<dynamic>(data.recipient.ToString());
                    string idrecipient = recipient.id.ToString();

                    string idcheck = sender.id.ToString();


                    var checkif = getinforzalo(idrecipient);
                    if (checkif == "0")
                        sendregisterzalo(idcheck);
                    else
                        sendmessagezalo(idcheck);
                }
            }
            else if (data.event_name.ToString() == "oa_send_text")
            {
                var message = JsonConvert.DeserializeObject<dynamic>(data.message.ToString());
                if (message.text.ToString().ToLower() == "#dangky")
                {
                    var recipient = JsonConvert.DeserializeObject<dynamic>(data.recipient.ToString());
                    string idcheck = recipient.id.ToString();
                    sendregisterzalo(idcheck.ToString());

                }
            }
            /* List<string> cl = new List<string>() { "contents" };
              List<string> vl = new List<string>() { JsonConvert.SerializeObject(data) };
              sqlmissdata miss = new sqlmissdata();
              string iser = miss.insertsqlstr("missdata", cl, vl);
              miss.runSQL(iser);*/
        }
        public string getinforzalo(string user_id)
        {
            
            WebClient webClient = new WebClient();
            webClient.Headers.Add("access_token", config.tokentzalo);
            string uurl = config.apizalo + "getprofile?data={'user_id':" + user_id + "}";
            var obj = webClient.DownloadString(uurl);
            var objs = JsonConvert.DeserializeObject<dynamic>(obj);
            if (objs.message.ToString() == "Success")
                return "0";
            return "1";
        }
        public string sendmessagezalo(string user_id)
        {
          
            var data = new
            {
                recipient = new
                {
                    user_id = user_id
                },
                message = new
                {
                    text = "Xin hân hạnh được phục vụ quý khách. Để có thể nhận được đầy đủ các ưu đãi. Quý khách vui lòng 'Follow' / 'Quan tâm' trang Zalo OA chính thức của 'Ninomaxx Concept' và Đăng ký thành viên. Xin chân thành cám ơn quý khách."
                }
            };
            string urlpost = config.apizalo + "message";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            myHttpWebRequest_.Headers.Add("access_token", config.tokentzalo);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            return str;

        }

        public string sendregisterzalo(string user_id)
        {
        
            var data = new
            {
                recipient = new
                {
                    user_id = user_id
                },
                message = new
                {
                    attachment = new
                    {
                        payload = new
                        {
                            elements = new[] {
                                               new {
                                                image_url= config.imgzalo,
                                                subtitle = "Đang yêu cầu thông tin từ bạn",
                                                title= "Đăng ký thành viên"
                                              } }
                                            ,
                            template_type = "request_user_info"
                        },
                        type = "template"
                    },
                    text = "hello, world!"
                }
            };
            string urlpost = config.apizalo + "message";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            myHttpWebRequest_.Headers.Add("access_token", config.tokentzalo);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            return str;

        }

        [HttpGet("testwebzalo")]
        public string testwebzalo(string userid)
        {
            return sendmessagezalo(userid);

        }
    }
}
