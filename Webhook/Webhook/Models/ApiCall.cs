﻿using Newtonsoft.Json;
using System.Net;

namespace Webhook.Models
{
    public class ApiCall
    {
        Fileconfig cf = new Fileconfig();
        public string GetData(string url)
        {
            string l = cf.apihrv + url;
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization", cf.keyhrv);
            var obj = webClient.DownloadString(l);
            return JsonConvert.SerializeObject(obj);
        }
        public string PostData(string url)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            return str;
        }
    }
}
