﻿namespace Webhook.Models
{
    public class Fileconfig
    {
        public string tokentzalo = "";
        public string apizalo = "";
        public string imgzalo = "";
        public string keyhrv = "";
        public string apihrv = "";
        public string apivfc = "";
        public Fileconfig()
        {
            var config = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json").Build();
            
            tokentzalo = config["AppSettings:tokentzalo"];
            apizalo = config["AppSettings:apizalo"];
            imgzalo = config["AppSettings:imgzalo"];
            keyhrv = config["AppSettings:keyhrv"];
            apihrv = config["AppSettings:apihrv"];
            apivfc = config["AppSettings:apivfc"];
        }
        public string CreateFile(string FolderSource, string name, string text)
        {
           
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(FolderSource + name, true))
            {
                file.WriteLine(text);
            }
            return "ok";
        }
    }
}
