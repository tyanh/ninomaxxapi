<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input( array(
		'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
		'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
		'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
	) );

	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>

	<button type="submit" id="addtocard" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
<style>
.woocommerce-variation-add-to-cart{
	display:none;
}
</style>
<script>
window.onload = function(e){ 
  jQuery('.woocommerce-variation-add-to-cart').show();
}
jQuery(document).ready(function(){

  jQuery("#addtocard").click(function(){
		var cl=jQuery('.active_swatch_color').length;
		var size=jQuery('.active_swatch_size').length;
		
		if(cl==0 || size==0){
			alert('Vui lòng chọn màu và size');
			return false;
		}
		
	 	var upc=jQuery(".active_swatch_size").attr("subsku");
		var rs=false;
		jQuery.ajax({
		url: hostapi+ "inventstoresku_v2?sku="+upc,
		async:false,
		complete: function(data) {
			var s=data.responseText;
			if(s=="-1"){
				return false;
			}
			else if(s=="0")
				alert("Sản phẩm đã hết");
			else{
				rs=true	;	
			}				
			},
		error: function (request, status, error) {
			alert("Sản phẩm đã hết");
			return false;
		}
	});
		
			return rs;
  });
});
</script>