<?php
global $woocommerce_wpwoof_common;
include('feed-manage-list.php');
require_once dirname(__FILE__).'/../../inc/feedfbgooglepro.php';

$myListTable = new Wpwoof_Feed_Manage_list();
$myListTable->prepare_items();

?>
    <script>
        function storeWpWoofdata(){
            var data = jQuery('#iDwpwoofGLS').serialize()+"&action=set_wpwoof_global_data";
            jQuery.fn.saveWPWoofParam( data, function () {
                /*$('#idWpWoofGCats').html($('#feed_google_category').val());*/
            });
        }
        jQuery(function($){
            $('#IDextraGlobal input' ).change(storeWpWoofdata);
            $('#IDextraGlobal select').change(storeWpWoofdata);
        });
    </script>
<div class="wpwoof-content-top wpwoof-box headerManagePage">
    <div a>
        <a class="wpwoof-button wpwoof-button-orange1" id="idWpWoofAddNewFeed" href="#">Create New Feed</a>
    </div>
    <div b>
        <vr></vr>
    </div>
    <div c>
         <a a target="_blank" href="https://www.pixelyoursite.com/woocommerce-product-catalog-feed-help">Learn how to use the plugin</a>
         <a target="_blank" href="https://www.pixelyoursite.com/facebook-product-catalog-feed">Learn how to create a Facebook Product Catalog</a>
    </div>
</div>

<form id="contact-filter" method="post">
	<!-- For plugins, we also need to ensure that the form posts back to our current page -->
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
	<?php //$myListTable->search_box('search', 'search_id'); ?>
	<!-- Now we can render the completed list table -->
	<?php $myListTable->display() ?>
</form>
<div class="wpwoof-box">
    <form method="post" action="#" id="iDwpwoofGLS">
        <h3>Global Settings:</h3>
        <table class="form-table manage_global_settings_block">
            <tr>
                <th>Regenerate active feeds:</th>
                <td>
                    <?php $current_interval = $woocommerce_wpwoof_common->getInterval();  ?>
                    <select name="wpwoof_schedule" id="wpwoof_schedule" onchange="jQuery.fn.saveWPWoofParam({'action':'set_wpwoof_shedule','wpwoof_schedule':this.value});">
                        <?php 

                        $intervals = array(
                            /*
                            '604800'    => '1 Week',
                            '86400'     => '24 Hours',
                            '43200'     => '12 Hours',
                            '21600'     => '6 Hours',
                            '3600'      => '1 Hour',
                            '900'       => '15 Minutes',
                            '300'       => '5 Minutes',
                            */
                            '0'         => 'Never',
                            '3600'      => 'Hourly',
                            '86400'     => 'Daily',
                            '43200'     => 'Twice daily',
                            '604800'    => 'Weekly'
                        );
                        foreach($intervals as $interval => $interval_name) {
                            ?><option <?php
                                if($interval==$current_interval OR !$current_interval AND !$interval ) {
                                    echo " selected ";
                                } ?> value="<?php
                                                echo $interval;
                                            ?>"><?php echo $interval_name;
                            ?></option><?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Global Google Taxonomy:</th>
                <td>
                        <?php
                        $data = $woocommerce_wpwoof_common->getGlobalGoogleCategory();
                        ?>
                         <input class="wpwoof_google_category_g_name" type="hidden" name="feed_google_category" value="<?php echo $data['name']; ?>" />
                        <input  class="wpwoof_google_category_g_id"   type="hidden" name='feed_google_category_id' value="<?php echo $data['id']; ?>" />
                        <input type="text"   name="wpwoof_google_category" onchange="storeTaxonomyParams(this);" class="wpwoof_google_category_g" value="" style='display:none;' />
                        <?php
                        $taxSrc = admin_url('admin-ajax.php');
                        $taxSrc = add_query_arg( array( 'action'=>'wpwoofgtaxonmy'), $taxSrc);
                        ?>
                        <script>
                            var WPWOOFpreselect =  '<?php echo $data['id'] ?>';
                            jQuery(function($) {
                                loadTaxomomy(".wpwoof_google_category_g", function(){
                                    var sNames = jQuery('.wpwoof_google_category_g_name').val();
                                    var sIDS   = jQuery('.wpwoof_google_category_g_id').val();
                                    if( WPWOOFpreselect != sIDS ) {
                                        jQuery.fn.saveWPWoofParam({
                                            'action': 'set_wpwoof_category',
                                            'wpwoof_feed_google_category': sNames,
                                            'wpwoof_feed_google_category_id': sIDS
                                        }, function () {
                                            WPWOOFpreselect = sIDS;
                                        });
                                    }
                                });
                            });
                        </script>
                    </td>
            </tr>
            <tr>
                    <th>Global Image:</th>
                    <td>
                        <!-- input type="button" class="button wpfoof-box-upload-button" value="Upload" / -->
                        <?php
                        $value =  $woocommerce_wpwoof_common->getGlobalImg();
                        $image =  empty($value) ? '' : wp_get_attachment_image( $value, 'full', false, array('style' => 'display:block;/*margin-left:auto*/;margin-right:auto;max-width:30%;height:auto;') );
                        ?>
                        <span class="wrap wpwoof-required-value">
                            <input type='hidden' id='_value-Maine-Img'      name='wpfoof-box-media[Maine-Img]'   value='<?php echo $value?>' />
                            <input type='button' id='Maine-Img'        onclick="jQuery.fn.clickWPfoofClickUpload(this);"     class='button wpfoof-box-upload-button'        value='Upload' />
                            <input type='button' id='Maine-Img-remove' onclick="jQuery.fn.clickWPfoofClickRemove(this);" <?php if(empty($image)) {?>style="display:none;"<?php } ?> class='button wpfoof-box-upload-button-remove' value='Remove' />
                         </span>
                        <span  id='IDprev-Maine-Img' class='image-preview'><?php echo ($image) ? ("<br/><br/>".$image."<br/>") : "" ?></span>
                        <span data-size='1200X628'  id='Maine-Img-alert'></span>
                    </td>
             </tr>
            <tr>
                <td colspan="2"><hr class="wpwoof-break" /></td>
            </tr>
            <tr>
                <th>Brand:</th><td><b>The plugin will fill brand in this order:</b></td>
            </tr>

            <tr><td></td>
                <td>
<?php                  ////////////////////////////////////////////////////////////////// Brand BLOCk  //////////////////////////////////////////////////////////////////////////////////////
     $wpwoof_values    = $woocommerce_wpwoof_common->getGlobalData();
     $attributes       = wpwoof_get_all_attributes();

?>

    <p class="p_inline_block stl-facebook stl-google" style="display: inline-block;">This value: </p>
    <select  onchange="storeWpWoofdata();" name="brand[value]"
             class="stl-facebook stl-google wpwoof_mapping wpwoof_mapping_option"  style="display: inline-block;" ><?php
        $html = '';
        $html .= '<option value="">select</option>';
        //$oFeedFBGooglePro->renderFields($all_fields['ID'], $meta_keys, $meta_keys_sort, $attributes, $wpwoof_values);


        $html .= '<optgroup label="Product Attributes">';
        foreach ($attributes as $key => $value) {
            if ($key=='product_visibility') continue; 
            $html .= '<option value="wpwoofattr_' . $key . '" ' . (isset($wpwoof_values['brand']['value']) ? selected('wpwoofattr_' . $key, $wpwoof_values['brand']['value'], false) : '') . ' >' . $value . '</option>';
        }
        $html .= '</optgroup>';
        echo $html;
        ?></select><br><br>

                <?php
                if ( is_plugin_active( WPWOOF_BRAND_YWBA ) ){
                ?>  <label class="stl-facebook stl-google" >
                            <input onchange="storeWpWoofdata();" name="brand[WPWOOF_BRAND_YWBA]" value="1" type="checkbox" <?php
                            if( !empty($wpwoof_values['brand']['WPWOOF_BRAND_YWBA']) || !isset($wpwoof_values['brand']['WPWOOF_BRAND_YWBA']) ) echo ' checked';
                            ?> />  YITH WooCommerce Brands Add-on plugin detected, use it when possible
                        <br><br></label>
                    <?php
                }
                if ( is_plugin_active( WPWOOF_BRAND_PEWB ) ) {
                    ?> <label class="stl-facebook stl-google" >
                        <input onchange="storeWpWoofdata();" type="checkbox" name="brand[WPWOOF_BRAND_PEWB]" value="1" <?php
                        if( !empty($wpwoof_values['brand']['WPWOOF_BRAND_PEWB']) || !isset($wpwoof_values['brand']['WPWOOF_BRAND_PEWB']) ) echo ' checked';
                        ?> /> Perfect WooCommerce Brands. Use it when possible
                        <br><br></label>
                    <?php
                }
                if ( is_plugin_active( WPWOOF_BRAND_PRWB ) ) {
                    ?> <label class="stl-facebook stl-google">
                        <input onchange="storeWpWoofdata();" type="checkbox" name="brand[WPWOOF_BRAND_PRWB]" value="1" <?php
                        if( !empty($wpwoof_values['brand']['WPWOOF_BRAND_PRWB']) || !isset($wpwoof_values['brand']['WPWOOF_BRAND_PRWB']) ) echo ' checked';
                        ?> /> Premmerce WooCommerce Brands. Use it when possible
                        <br><br></label>
                    <?php
                }
                if ( is_plugin_active( WPWOOF_BRAND_PBFW ) ) {
                    ?> <label class="stl-facebook stl-google" >
                        <input onchange="storeWpWoofdata();" type="checkbox" name="brand[WPWOOF_BRAND_PBFW]" value="1" <?php
                        if( !empty($wpwoof_values['brand']['WPWOOF_BRAND_PBFW']) || !isset($wpwoof_values['brand']['WPWOOF_BRAND_PBFW']) ) echo ' checked';
                        ?> /> Product Brands For WooCommerce. Use it when possible
                        <br><br></label>
                    <?php
                }
                ?>
                 <label class="stl-facebook stl-google" >
                        <input name="brand[autodetect]" type="hidden" value="0" />
                        <input onclick="storeWpWoofdata();" name="brand[autodetect]" type="checkbox" value="1" <?php
                        if( !empty($wpwoof_values['brand']['autodetect']) || !isset($wpwoof_values['brand']['autodetect']) ) echo ' checked';
                        ?>/> Possible "brand" field autodetected. Use it when possible
                     <br><br></label>

                    <p class="p_inline_block stl-facebook stl-google">Use this when brand is missing: </p>
                    <input onchange="storeWpWoofdata();" class="stl-facebook stl-google" name="brand[define]" type="text" value="<?php
                       echo !empty($wpwoof_values['brand']['define']) ? $wpwoof_values['brand']['define'] : get_bloginfo( 'name' ) ;  ?>"/>
<?php   ////////////////////////////////////////////////////////////////// END Brand BLOCk  ////////////////////////////////////////////////////////////////////////////////////// ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" id="IDextraGlobal">
                   <?php
                       $all_fields       = wpwoof_get_all_fields();
                       $meta_keys        = wpwoof_get_product_fields();
                       $meta_keys_sort   = wpwoof_get_product_fields_sort();

                       $oFeed            = new FeedFBGooglePro( $meta_keys, $meta_keys_sort, $attributes);


                       if(!isset($wpwoof_values['google'])) {       $wpwoof_values['google']        = array(); }
                       if(!isset($wpwoof_values['adsensecustom'])){ $wpwoof_values['adsensecustom'] = array(); }


                   ?><hr class="wpwoof-break" />
                    <div class="filter_flex_section">
                        <input type="hidden" name="wpfoof-google" value="0">
                        <input type='checkbox' class="ios-switch"  onclick="jQuery.fn.wpwoofOpenCloseFieldList('google',this.checked);" id="id-wpfoof-google" name='wpfoof-google' value="1" <?php
                        echo !empty($wpwoof_values['enable_google']) ? "checked='true'" : ""; ?> />
                        <label for="id-wpfoof-google" >Extra Custom Fields</label>
                        <div id="idgoogleFields" style="display:<?php echo !empty($wpwoof_values['enable_google']) ? 'block' : 'none'; ?>;">
                            <p>You can map the following values to Product Attributes. We also add custom fields you can use on product and category level. This mapping will be used if no custom value is found.</p>
                            <?php
                            $oFeed->renderFieldsForMapping( $all_fields['toedittab'],'google', $wpwoof_values['google'] );
                            ?>
                        </div>
                    </div>
                    <hr class="wpwoof-break" />
                    <div class="filter_flex_section">
                        <input type="hidden" name="wpfoof-adsensecustom" value="0">
                        <input type='checkbox' class="ios-switch"  onclick="jQuery.fn.wpwoofOpenCloseFieldList('adsensecustom',this.checked);"  id='id-wpfoof-adsensecustom' name='wpfoof-adsensecustom' value="1" <?php
                        echo !empty($wpwoof_values['enable_adsensecustom']) ? "checked='true'" : ""; ?> />
                        <label  for="id-wpfoof-adsensecustom" >Extra Custom Fields for Google Ads Custom Feed</label>
                        <div id="idadsensecustomFields" style="display:<?php echo !empty($wpwoof_values['enable_adsensecustom']) ? 'block' : 'none'; ?>;">
                            <?php
                            $oFeed->renderFieldsForMapping( $all_fields['toedittab'],'adsensecustom', $wpwoof_values['adsensecustom'] );
                            ?>
                        </div>
                    </div>


                </td>
            </tr>
        </table>
    </form>
</div>
<?php include('info-settings.php');