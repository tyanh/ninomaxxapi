
<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

//WC 3.4
$post_class = 'post_class';
if ( function_exists( 'WC' ) && WC()->version >= '3.4' ) {
	$post_class = 'wc_product_class';
}


?>
<div class="single-breadcrumbs-wrapper">

<div class="container">

	<div class="breadcrumbs container breadcrumbs_single">
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<p id="breadcrumbs">','</p>');
	}
	?>
	</div>
</div>
</div>

<div class="container" host_api="">
	<?php
		/**
		 * Hook: woocommerce_before_single_product.
		 *
		 * @hooked wc_print_notices - 10
		 */
		do_action( 'woocommerce_before_single_product' );

		if ( post_password_required() ) {
			echo get_the_password_form();
			return;
		}

		$product_images_class  	= basel_product_images_class();
		$product_summary_class 	= basel_product_summary_class();
		$single_product_class  	= basel_single_product_class();
		$content_class 			= basel_get_content_class();
		$product_design 		= basel_product_design();

		$container_summary = 'container';

		if( $product_design == 'sticky' ) {
			$container_summary = 'container-fluid';
		}
			
		?>
	</div>
	<div ivt="" class="attr_pro" val="<?php $product->get_attributes(); ?>" id="product-<?php the_ID(); ?>" <?php $post_class( $single_product_class, $product ); ?>>

		<div class="<?php echo esc_attr( $container_summary ); ?>">

			<div class="row">
				<div class="product-image-summary <?php echo esc_attr( $content_class ); ?>">
					<div class="row">
						<div class="<?php echo esc_attr( $product_images_class ); ?> product-images">
							<?php
							/**
							 * woocommerce_before_single_product_summary hook
							 *
							 * @hooked woocommerce_show_product_sale_flash - 10
							 * @hooked woocommerce_show_product_images - 20
							 */
							//do_action( 'woocommerce_before_single_product_summary' );
							
							?>
							<div class="img_galery_mb">
								<div id="sub_cate" style="float:left">
									<ul id="ul_img">
									
									</ul>

								</div>
								<div style="float:right">
								    <span class='zoom' id='ex1'>
										<img src='' id='main_img' style="width:415px"/>
									</span>
								</div>
							</div>


							<div class="Modern-Slider" id="mobile_sd">

							</div>




<style type="text/css">
	/* ==== Main CSS === */
.img-fill{
  width: 100%;
  display: block;
  overflow: hidden;
  position: relative;
  text-align: center
}
#mobile_sd{display: none;} 

.img-fill img {
  position: relative;
  display: inline-block;
}

*,
*:before,
*:after {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.04);
}




.Modern-Slider .NextArrow:before{
	content: "";
    display: block;
    background-image: url(https://ninomaxx.com.vn/wp-content/themes/basel-child/icon/icon-arrow-right.png);
    width: 15px;
    height: 15px;
    max-width: 50px;
    float: left;
    background-color: transparent;
    background-size: 100%;
    background-repeat: no-repeat;

}

.Modern-Slider .PrevArrow{
  position:absolute;
  top:50%;
  left:0px;
  width:45px;
  height:45px;
  background:rgba(0,0,0,.50);
  border:0 none;
  margin-top:-22.5px;
  text-align:center;
  color:#FFF;
  z-index:5;
}


.Modern-Slider .NextArrow{
  position:absolute;
  top:50%;
  right:0px;
  width:45px;
  height:45px;
  background:rgba(0,0,0,.50);
  border:0 none;
  margin-top:-22.5px;
  text-align:center;
  color:#FFF;
  z-index:5;
}
.Modern-Slider .PrevArrow:before{
	content: "";
    display: block;
    background-image: url(https://ninomaxx.com.vn/wp-content/themes/basel-child/icon/icon-arrow-left.png);
    width: 15px;
    height: 15px;
    max-width: 50px;
    margin-left: -10px;
    float: left;
    background-color: transparent;
    background-size: 100%;
    background-repeat: no-repeat;
}
.Modern-Slider button:hover {
	background: #c4c4c4;
}


.Modern-Slider .slick-dots {
	 display: flex;
	 justify-content: center;
	 margin: 0;
	 padding: 1rem 0;
	 list-style-type: none;
	 bottom: 0;
}
.Modern-Slider .slick-dots li {
	 margin: 0 0.25rem;
}
.Modern-Slider .slick-dots button {
	display: block;
    width: 8px;
    height: 8px;
    padding: 0;
    border: aliceblue;
    border-radius: 100%;
    background-color: #fff;
    text-indent: -9999px;
}
.Modern-Slider .slick-dots li.slick-active button {
	display: block;
    width: 10px;
    height: 10px;
    padding: 0;
    border: 1px solid aliceblue;
    border-radius: 100%;
    background-color: transparent;
    text-indent: -9999px;
    margin-top: -1px;
}
 
.Modern-Slider .item.slick-active{
  animation:Slick-FastSwipeIn 1s both;
}

.Modern-Slider {background:transparent;display: none;}


/* ==== Slick Slider Css Ruls === */
.slick-slider{position:relative;display:block;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent}
.slick-list{position:relative;display:block;overflow:hidden;margin:0;padding:0}
.slick-list:focus{outline:none}.slick-list.dragging{cursor:hand}
.slick-slider .slick-track,.slick-slider .slick-list{-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}
.slick-track{position:relative;top:0;left:0;display:block}
.slick-track:before,.slick-track:after{display:table;content:''}.slick-track:after{clear:both}
.slick-loading .slick-track{visibility:hidden}
.slick-slide{display:none;float:left /* If RTL Make This Right */ ;height:100%;min-height:1px}
.slick-slide.dragging img{pointer-events:none}
.slick-initialized .slick-slide{display:block}
.slick-loading .slick-slide{visibility:hidden}
.slick-vertical .slick-slide{display:block;height:auto;border:1px solid transparent}


@media only screen and (max-width: 549px) {
  .Modern-Slider {
    display: block!important;
    background:transparent;
  }
  .img_galery_mb {display: none!important;}
}
</style>





						</div>


						<div class="<?php echo esc_attr( $product_summary_class ); ?> summary entry-summary content_single_product" id="myHeader">
							<div class="summary-inner <?php if( $product_design == 'compact' ) echo 'basel-scroll'; ?>">
								<div class="basel-scroll-content">
									<?php
									/**
									 * Hook: woocommerce_single_product_summary.
									 *
									 * @hooked woocommerce_template_single_title - 5
									 * @hooked woocommerce_template_single_rating - 10
									 * @hooked woocommerce_template_single_price - 10
									 * @hooked woocommerce_template_single_excerpt - 20
									 * @hooked woocommerce_template_single_add_to_cart - 30
									 * @hooked woocommerce_template_single_meta - 40
									 * @hooked woocommerce_template_single_sharing - 50
									 */
									do_action( 'woocommerce_single_product_summary' );
									?>
									
									<?php if ( $product_design != 'alt' && $product_design != 'sticky' && basel_get_opt( 'product_share' ) ): ?>
										<!--<div class="product-share">
											<span class="share-title"><?php _e('Chia sẻ ', 'basel'); ?></span>
											<?php echo basel_shortcode_social( array( 'type' => basel_get_opt( 'product_share_type' ), 'size' => 'small', 'align' => 'left' ) ); ?>
										</div>-->
									<?php endif ?>
								</div>
							</div>
							<div class="size-guide-btn size-recommender _size-recommender">
								<a class="size-recommender-text _size-recommender-text _label-focusable" tabindex="0" href="#" onclick="document.getElementById('id01').style.display='block'">Cỡ của tôi là gì?</a>
								<div class="bullet"><span class="_size-recommender-value  hidden">?</span>
									<img src='<?php echo ICO;?>/size-recommend.svg' class="size-recommend-icon _size-recommend-icon" alt="Tìm cỡ của tôi">
								</div>
							</div>
							
						<!-- Product more infomation-->
						<div class="product_detail">
							<div class="shipping_rule">
								<ul>
									<li>
										<img src='<?php echo ICO;?>/truck.png' /><b>Miễn phí vận chuyển</b>
										<p>Khu vực TP HCM: từ 300.000đ<br/>
										Các khu vực khác: từ 500.000đ<br/>
										Mua thêm sản phẩm để được freeship nhé khách ơi!</p>
									</li>
								</ul>
							</div>

							<button class="accordion">Chất liệu</button>
							<div class="panel">
							  	<p><?php the_field('chat_lieu') ;?></p>
							</div>

							<button class="accordion">Hướng dẫn sử dụng</button>
							<div class="panel">
							  <p>Giặt máy ở nhiệt độ thường. Không sử dụng chất tẩy. Phơi trong bóng mát. Sấy khô ở nhiệt độ thấp. Là ở nhiệt độ thấp 110 độ C.	Giặt với sản phẩm cùng màu.	Không là lên chi tiết trang trí.</p>
							</div>
							<button class="accordion" onclick="location.href='https://ninomaxx.com.vn/quy-dinh-doi-hang/'" type="button">Chính sách đổi hàng</button>
							<div class="panel">
								<div class="row">
									<?php the_field('info_change') ;?>
								</div>
							</div>
							<button class="accordion">Hướng dẫn giặt ủi</button>
							<div class="panel">
							 <div class="row">
							<p>HƯỚNG DẪN BẢO QUẢN QUẦN JEANS >> <a href="#">Xem chi tiết</a><br/>
							HƯỚNG DẪN BẢO QUẢN ÁO >> <a href="#">Xem chi tiết</a></p>
							</div>
							</div>

							<div class="product-share">
								<span class="share-title"><?php _e('Chia sẻ: ', 'basel'); ?></span>
								<?php echo basel_shortcode_social( array( 'type' => basel_get_opt( 'product_share_type' ), 'size' => 'small', 'align' => 'left' ) ); ?>


								<div class="zalo-share-button" data-href="" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false></div>
							</div>




								<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

							
							<!--<?php
								$current_tags = get_the_terms( get_the_ID(), 'product_tag' );
								if ( $current_tags && ! is_wp_error( $current_tags ) ) { 
								    echo '<ul class="product_tags"> Từ khoá: ';
								    foreach ($current_tags as $tag) {
								        $tag_title = $tag->name; // tag name
								        $tag_link = get_term_link( $tag );// tag archive link
								        echo '<li><a href="'.$tag_link.'">'.$tag_title.', </a></li> ';
								    }
								    echo '</ul>';
								}
							?>--> 
						</div>
        			<!-- <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
                		<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" value_="<?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?>" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>.</span>
        			<?php endif; ?> -->
					</div>
								</div><!-- .summary -->
			</div>

			<?php 
				/**
				 * woocommerce_sidebar hook
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
				?>

			</div>
		</div>

	<!-- 	<?php if ( ( $product_design == 'alt' || $product_design == 'sticky' ) && basel_get_opt( 'product_share' ) ): ?>
		<div class="product-share">
			<?php echo basel_shortcode_social( array( 'type' => basel_get_opt( 'product_share_type' ), 'style' => 'colored' ) ); ?>
		</div>
		<?php endif ?> -->

		<div class="container">
			<?php
			/**
			 * basel_after_product_content hook
			 *
			 * @hooked basel_product_extra_content - 20
			 */
			do_action( 'basel_after_product_content' );
			?>
		</div>

		<?php if( $product_design != 'compact' ): ?>

			<!--Short Description-->			

			<!--<div class="product-tabs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<?php
							/**
							 * woocommerce_after_single_product_summary hook
							 *
							 * @hooked woocommerce_output_product_data_tabs - 10
							 * @hooked woocommerce_upsell_display - 15
							 * @hooked woocommerce_output_related_products - 20
							 */
							//do_action( 'woocommerce_after_single_product_summary' );
							?>
						</div>
					</div>	
				</div>
			</div>-->
			<!--Short Description-->	

		<?php endif ?>

	</div><!-- #product-<?php the_ID(); ?> -->

	<?php do_action( 'woocommerce_after_single_product' ); ?>


	<div id="id01" class="modal">

		<div class="modal-content animate container">

			<div class="imgcontainer">

				<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>

			</div>

			<div class="row content_popup">
				<h3 class="title_popup_single">BẢNG HƯỚNG DẪN CHỌN SIZE</h3>
				<img src='<?php echo ICO;?>/Size-Chart.jpg'/>
			</div>

		</div>

	</div>

	<div id="id02" class="modal">

		<div class="modal-content animate container">

			<div class="imgcontainer">
				<span onclick="jQuery('#id02').hide()" class="close" title="Close Modal">&times;</span>
			</div>
			<h3 class="title_popup_single text-center">KIỂM TRA TÌNH TRẠNG CÒN HÀNG TẠI CỬA HÀNG</h3>
			<div class="row pd-lr-35">
				<div class="col-sm-3 mg-t-20 pd-0-10 find_store_box">
					<img class="s_img img_find" src=""  id="subimg_"><br/>
            		<p class="text-center title_product_popup"><?php the_title(); ?></p>
            		<div class="price_find">Giá: <?php echo number_format($product->get_price()).' đ';?></div>
				</div>
				
				<div class="col-sm-9 mg-t-20 pd-0-10">
					<div class="tab">
					  <button class="tablinks" onclick="openCity(event, 'mienbac')">Miền Bắc</button>
					  <button class="tablinks" onclick="openCity(event, 'mientrung')">Miền Trung</button>
					  <button class="tablinks" onclick="openCity(event, 'miendong')">Miền Đông</button>
					  <button class="tablinks" onclick="openCity(event, 'mientay')">Miền Tây</button>
					  <button class="tablinks" onclick="openCity(event, 'hochiminh')">Hồ Chí Minh</button>
					</div>

					<div id="mienbac" class="tabcontent" style="display: block;">
						<ul class="liststore_all"></ul>
					</div>

					<div id="mientrung" class="tabcontent">
						<ul class="liststore_all"></ul>
					</div>
					<div id="miendong" class="tabcontent">
						<ul class="liststore_all"></ul>
					</div>
					<div id="mientay" class="tabcontent">
						<ul class="liststore_all">	</ul>
					</div>
					<div id="hochiminh" class="tabcontent">
						<ul class="liststore_all"></ul>
					</div>


				<script>
				function openCity(evt, cityName) {
				  var i, tabcontent, tablinks;
				  tabcontent = document.getElementsByClassName("tabcontent");
				  for (i = 0; i < tabcontent.length; i++) {
				    tabcontent[i].style.display = "none";
				  }
				  tablinks = document.getElementsByClassName("tablinks");
				  for (i = 0; i < tablinks.length; i++) {
				    tablinks[i].className = tablinks[i].className.replace(" active", "");
				  }
				  document.getElementById(cityName).style.display = "block";
				  evt.currentTarget.className += " active";
				}
				</script>
<!-- 					<div class="container" style="width: 100%;" id="liststore2"></div>
 -->				<script>

				var $id ="<?php echo $product->get_purchase_note();?>";
				
				
				 //  function Setcolor(o){
				 //  jQuery("#sl_size .option_div").remove();
				 //  	 var c=jQuery(o).attr("size");
					//  var b=jQuery(o).attr("skus");
					//  var size=JSON.parse(c);
					//  var skus=JSON.parse(b);
					//  for(var i=0;i<size.length;i++)
					// 				{
					// 	var op_S="<div class='option_div'  skus='"+skus[i]+"'>"+size[i]+"</div>";
					// 	jQuery(op_S).appendTo("#sl_size");
					// }
				 //  }
				</script>

				<script>
					var acc = document.getElementsByClassName("accordion");
					var i;

					for (i = 0; i < acc.length; i++) {
					  acc[i].addEventListener("click", function() {
					    this.classList.toggle("active");
					    var panel = this.nextElementSibling;
					    if (panel.style.display === "block") {
					      panel.style.display = "none";
					    } else {
					      panel.style.display = "block";
					    }
					  });
					}
					</script>
				</div>
				
			</div>
			<div class="row text-center">
<!--                 <p>*Lưu ý: Giá trên website và tại cửa hàng có thể khác nhau trong một số trường hợp</p>-->			
</div>
			<!--<div class="row content_popup">
			</div>-->
		</div>
	</div>
	<div id="id03" class="modal">
		<div class="modal-content animate container">
			<div class="imgcontainer">
				<span onclick="document.getElementById('id03').style.display='none'" class="close" title="Đóng">&times;</span>
			</div>
			<div class="row content_popup">
<!-- 				<h3 class="uppercase title_popup_single">GỬI VÀ ĐỔI TRẢ HÀNG</h3>
 -->				<?php the_field('info_change') ;?>
			</div>
		</div>
	</div>
<script>
	// When the user clicks on div, open the popup
	function myFunction() {
		var popup = document.getElementById("myPopup");
		popup.classList.toggle("show");
	}
</script>

<style>
.zoom {
    display: inline-block;
    position: relative;
}
.zoom:after {
content: '';
display: block;
width: 33px;
height: 33px;
position: absolute;
top: 0;
right: 0;
/*background: url(icon.png);
*/}

    .zoom img {
        display: block;
    }

        .zoom img::selection {
            background-color: transparent;
        }


	.accordion {
     margin: 0 0 10px 0;
    padding: 0 0 10px 0;
}



.panel {
  padding: 0 18px;
  display: none;
  background-color: white;
  overflow: hidden;
  margin-top: -10px;
}

/* Style the tab */

.tab button.active {
  background-color: #ccc;
}

.tabcontent {
	display: none;
	border: 1px solid #ebebeb;
	border-top: none!important;
}

.container_check input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #000;
}

/* On mouse-over, add a grey background color */
.container_check:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container_check input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container_check input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container_check .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.container_check {
  display: block;
  position: relative;
  padding-left: 25px;
  margin-top: 10px;
  cursor: pointer;
  font-size: 14px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.tab button {
    background-color: inherit;
    float: left;
    border: 1px solid #ebebeb;
    outline: none;
    cursor: pointer;
    background: #121212;
    font-weight: bold;
    color: #fff;
    width: 20%;
    padding: 10px 10px;
    transition: 0.3s;
    font-size: 15px;
}
.tab button:hover {
  background-color: #ddd;
  color:#000!important;
}
img.zoomImg{
	width:1024px!important;
}
#color{
	display: none;
}
#size {
	display: none;
}
.select_btn span{
	background-color:black;
	color:#fff
}
#pa_color{
	display:none;
}
#pa_size{
	display:none;
}
</style>

<script>

var hostapi="<?php echo host_api_https; ?>";
var host="<?php echo host_img; ?>";
var skus_=[];
var ssku=<?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?>;
var list_variation=jQuery(".variations_form").attr("data-product_variations");
var list_variations=JSON.parse(list_variation);

function reaplace(o){

	return o.replace(".","_").replace(/-/g,"_").toUpperCase();
}

function checkInven(){
	var cl=jQuery('.active_swatch_color').length;
		var size=jQuery('.active_swatch_size').length;
		
		if(cl==0 || size==0){
			alert('Vui lòng chọn màu và size');
			return "-1";
		}
	var qlt=0;
	var upc=jQuery(".active_swatch_size").attr("subsku");

	var img_=jQuery(".s_img:eq(0)").attr("src");
	jQuery("#subimg_").attr("src",img_);
	jQuery(".liststore_all").empty();
	jQuery.ajax({
		url: hostapi+ "inventstoresku?sku="+upc,
		async:false,
		complete: function(data) {
			var d=JSON.parse(data.responseText);
			
			for(var i=0;i<d.length;i++){
				var REGION_NAME=d[i].REGION_NAME;
				var li='<li>'+
							"<span class='name_storelist'>" + d[i].namestore+" </span><br/> "+
							"<div class='diachi_store'>" + d[i].diachi + "</div>" +
							"<span class='num_product_store'> Có "+d[i].QTY+" sản phẩm</span>"
						'</li>';
				if(REGION_NAME=="HO CHI MINH")
				{
					jQuery(li).appendTo("#hochiminh .liststore_all");
				}
				else if(REGION_NAME=="MIEN TAY")
				{
					jQuery(li).appendTo("#mientay .liststore_all");
				}
				else if(REGION_NAME=="MIEN DONG")
				{
					jQuery(li).appendTo("#miendong .liststore_all");
				}
				else if(REGION_NAME=="MIEN TRUNG")
				{
					jQuery(li).appendTo("#mientrung .liststore_all");
				}
				else if(REGION_NAME=="MIEN BAC")
				{
					jQuery(li).appendTo("#mienbac .liststore_all");
				}
				qlt=qlt+parseInt(d[i].QTY);
			}
		
		}
	});
	return qlt;
}
function setcheckInven(o){
	
	jQuery(".attr_box_btn").removeClass("select_btn");
	jQuery(o).addClass("select_btn");
	var op=jQuery(o).attr("values");
	jQuery('#size option[value='+op+']').prop("selected",true);
	jQuery('#size').trigger('change');
	return false;

}
function changeImg(o){
	var src=jQuery(o).find("img").attr("src");
	// alert(src);
	jQuery('#main_img').attr("src",src);
	jQuery('#ex1').zoom();
}
function setselectColor(o){
	jQuery("#size option:eq(1)").prop('selected', true);
	jQuery("#color option[value="+o+"]").prop('selected', true).trigger('change');

}
function setselectSize(o){
	var a=jQuery(o).html();
	jQuery("#color option:eq(1)").prop('selected', true);
	jQuery("#size option[value="+a+"]").prop('selected', true).trigger('change');

}
function slider_(){

  jQuery(".Modern-Slider").slick({
    autoplay:false,
    autoplaySpeed:2000,
    speed:500,
    slidesToShow:1,
    slidesToScroll:1,
    pauseOnHover:false,
    dots:true,
    pauseOnDotsHover:true,
     nextArrow:'<button class="NextArrow"></button>', 
     prevArrow:'<button class="PrevArrow"></button>',
    cssEase:'linear',
   fade:true,
  });



}
jQuery(document).ready(function(){
  //slider_();
});

function changeSubImg_1(o){

	jQuery(".Modern-Slider").slick('unslick');
	jQuery("#ul_img").empty();
	jQuery("#mobile_sd").empty();

	var attr_=reaplace(jQuery(o).attr("data-value"));
	for(var i=1;i<5;i++)
	{

		var src=host+ssku+"/"+ssku+"_"+attr_+"_"+i+".jpg";

		var li='<li class="img_prism" onclick="changeImg(this)">'+
		
		'<img class="s_img" src="'+src+'" onerror="this.remove();" style="width:120px;margin-bottom: 10px;position:relative;" />'+
		'</li>';
			jQuery(li).appendTo("#ul_img");
				
			var img_mb='<div class="item"><div class="img-fill"><img src="'+src+'" onerror="this.remove();" alt=""></div></div>'
			jQuery(img_mb).appendTo("#mobile_sd");
		
	}
		//jQuery('#ex1').zoom();
	//jQuery(".Modern-Slider")[0].slick.refresh();
	  slider_();
}
function changeSubImg(o){

	jQuery(".Modern-Slider").slick('unslick');
	jQuery("#ul_img").empty();
	jQuery("#mobile_sd").empty();
	var sku1_=jQuery(o).attr("sku");
	var attr_=reaplace(jQuery(o).attr("atr"));
	for(var i=1;i<5;i++)
	{
	
		var src=host+ssku+"/"+ssku+"_"+attr_+"_"+i+".jpg";

		var li='<li class="img_prism" onclick="changeImg(this)">'+
		
		'<img class="s_img" src="'+src+'" onerror="this.remove();" style="width:120px;margin-bottom: 10px;position:relative;" />'+
		'</li>';
			jQuery(li).appendTo("#ul_img");
			
			var img_mb='<div class="item"><div class="img-fill"><img src="'+src+'" onerror="this.remove();" alt=""></div></div>'
			jQuery(img_mb).appendTo("#mobile_sd");
		
	}
	//jQuery(".Modern-Slider")[0].slick.refresh();
	  slider_();
}
function remove_img_err(o){
	$(o).parent().parent().remove();
}
function reloadefectImg(){
	jQuery("#ex1 img").each(function( index ) {
		if(index>0)
		{
			jQuery(this).remove();
		}
	});
}
function updateimg(o){
	reloadefectImg();
	var sku1_=jQuery(o).attr("sku");
	var attr_=reaplace(jQuery(o).attr("atr"));
	var src=host+ssku+"/"+ssku+"_"+attr_+"_1.jpg";
	jQuery('#main_img').attr("src",src);

	changeSubImg(o);
	setselectColor(attr_);
	jQuery('#ex1').zoom();
}

function sortDropDownListByNum() {
        // Loop for each select element on the page.
        jQuery("#size").each(function() {            
            // Keep track of the selected option.
            var selectedValue = jQuery(this).val();     
            // Sort all the options by text. I could easily sort these by val.
            jQuery(this).html(jQuery("option", jQuery(this)).sort(function(a, b) {
				var c=parseInt(a.text);
				var d=parseInt(b.text);
				
                return c > d ? 0 : c < d ? -1 : 1
            }));     
            // Select one option.
            jQuery(this).val(selectedValue);
        });
    }
	function sortDropDownListByText() {

		var sort_size=["s","m","l","xl","xxl"];
        // Loop for each select element on the page.
		 jQuery("#size").each(function() {            
            // Keep track of the selected option.
            var selectedValue = jQuery(this).val();     
            // Sort all the options by text. I could easily sort these by val.
            jQuery(this).html(jQuery("option", jQuery(this)).sort(function(a, b) {
				var id1=sort_size.indexOf(a.text.toLowerCase());
				var id2=sort_size.indexOf(b.text.toLowerCase());
                return id1 > id2 ? 0 : id1 < id2 ? -1 : 1
            }));     
            // Select one option.
            jQuery(this).val(selectedValue);
        });
    }
	function sortDropDownListByText_div() {
		var sort_size=["s","m","l","xl","xxl"];
	          
            // Keep track of the selected option.
            var selectedValue = jQuery(".swatches-select").html();     
            // Sort all the options by text. I could easily sort these by val.
            jQuery(".swatches-select").html(jQuery("div", jQuery(this)).sort(function(a, b) {
				var id1=sort_size.indexOf(a.attr("data-value").toLowerCase());
				var id2=sort_size.indexOf(b.attr("data-value").toLowerCase());
                return id1 > id2 ? 0 : id1 < id2 ? -1 : 1
            }));     
            // Select one option.
            jQuery(".swatches-select").val(selectedValue);
      
		
    }
jQuery(document).ready(function() {

	jQuery("#find_inventory").click(function(){
		var rs=checkInven();
		if(rs!="-1")
			document.getElementById('id02').style.display='block'
			
	});
	var first_opt=jQuery("#size option:eq(1)").val();
	var isnum=parseInt(first_opt);

	if(isnum>0)
		sortDropDownListByNum();
	else
		sortDropDownListByText();
    var a=jQuery(".variations_form").attr("data-product_variations");
	var b=JSON.parse(a);
	console.log(b)
	for(var i=0;i<b.length;i++)
	{
	
		 skus_.push(b[i].sku);

	}

		p_color=b[0].attributes.attribute_pa_color;

	var attr_=reaplace(p_color).replace(/ /g,"_");

	var color="";
	var sku=b[0].sku;
	for(var i=1;i<5;i++)
	{

		var src=host+ssku+"/"+ssku+"_"+attr_+"_"+i+".jpg";
	
		var li='<li onclick="changeImg(this)"><img class="s_img" src="'+src+'"  onerror="this.remove();" style="width:120px;margin-bottom: 10px;position:relative;"/></li>'
		jQuery(li).appendTo("#ul_img");
		
		var img_mb='<div class="item"><div class="img-fill"><img src="'+src+'" onerror="this.remove();" alt=""></div></div>'
		jQuery(img_mb).appendTo("#mobile_sd");
		
	}
	slider_();
	
	jQuery('#main_img').attr("src",host+ssku+"/"+ssku+"_"+attr_+"_1.jpg");
	jQuery('img.zoomImg').width('1024px');
	jQuery('img.zoomImg').css("height:1537px")
	jQuery('#ex1').zoom();

	var p_cl=jQuery("#color").parent();
	var p_size=jQuery("#size").parent();
	var m=0;
if(jQuery("#color").length>0){
	jQuery("#color option").each(function( index ) {
		var c=jQuery(this).val();
	
		if(c!="")
		{
			var bg=host+ssku+"/"+ssku+"_"+reaplace(c)+"_"+"1.jpg";
	
			var s="<span sku='"+skus_[m]+"' atr='"+c+"' class='span_color "+reaplace(c)+" ' onclick='updateimg(this)' style='background: url("+bg+");' ></span>";
			jQuery(s).appendTo(p_cl);
			//m++;
		}
	});
	var c_="select_btn";
	jQuery("#size option").each(function( index ) {
		var c=jQuery(this).val();
		if(c!="")
		{
			
			var s="<div id='attr_box' class='attr_box_btn "+c_+"' onclick='setcheckInven(this)' sku='"+skus_[m]+"' values='"+c+"'><span  class='size_text btn' style='float:left'>"+c+"</span></div>";
			jQuery(s).appendTo(p_size);
			m++;
			c_="";
		}
	});

	//jQuery("#size option:eq(1)").prop('selected', true);
	//jQuery("#color option:eq(1)").prop('selected', true);
	var bgc=jQuery(".span_color:eq(0)");

	updateimg(bgc);
}
});
</script>

<!-- <script>
// Add active class to the current button (highlight it)
var header = document.getElementById("attr_box");
// var btns = header.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active-swatch");
  current[0].className = current[0].className.replace(" active-swatch", "");
  this.className += " active";
  });
}

</script> -->
