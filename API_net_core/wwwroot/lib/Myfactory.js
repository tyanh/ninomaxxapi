﻿app.factory('myService', ['$http', function ($http) {
    return {
        uploadFile: function (file) {
                return $http({
                url: "/api/Api/UploadFile",
                method: 'POST',
                data: file,
                headers: { 'Content-Type': undefined }, //this is important
                transformRequest: angular.identity //also important
                
            });
        },
        otherFunctionHere: function (url, stuff) {
            return $http.get(url);
        }
    };
}]);