var tows= [
        {
            "ma": "001",
            "ten": "Quận Ba Đình",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "002",
            "ten": "Quận Hoàn Kiếm",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "003",
            "ten": "Quận Tây Hồ",
            "ten Tiếng Anh": "Tay Ho Dist",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "004",
            "ten": "Quận Long Biên",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "005",
            "ten": "Quận Cầu Giấy",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "006",
            "ten": "Quận Đống Đa",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "007",
            "ten": "Quận Hai Bà Trưng",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "008",
            "ten": "Quận Hoàng Mai",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "009",
            "ten": "Quận Thanh Xuân",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "016",
            "ten": "Huyện Sóc Sơn",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "017",
            "ten": "Huyện Đông Anh",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "018",
            "ten": "Huyện Gia Lâm",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "019",
            "ten": "Quận Nam Từ Liêm",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "020",
            "ten": "Huyện Thanh Trì",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "021",
            "ten": "Quận Bắc Từ Liêm",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "250",
            "ten": "Huyện Mê Linh",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "268",
            "ten": "Quận Hà Đông",
            "cap": "Quận",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "269",
            "ten": "Thị xã Sơn Tây",
            "cap": "Thị xã",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "271",
            "ten": "Huyện Ba Vì",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "272",
            "ten": "Huyện Phúc Thọ",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "273",
            "ten": "Huyện Đan Phượng",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "274",
            "ten": "Huyện Hoài Đức",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "275",
            "ten": "Huyện Quốc Oai",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "276",
            "ten": "Huyện Thạch Thất",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "277",
            "ten": "Huyện Chương Mỹ",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "278",
            "ten": "Huyện Thanh Oai",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "279",
            "ten": "Huyện Thường Tín",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "280",
            "ten": "Huyện Phú Xuyên",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "281",
            "ten": "Huyện Ứng Hòa",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "282",
            "ten": "Huyện Mỹ Đức",
            "cap": "Huyện",
            "maT": "01",
            "Tỉnh / Thành Phố": "Thành phố Hà Nội"
        },
        {
            "ma": "024",
            "ten": "Thành phố Hà Giang",
            "cap": "Thành phố",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "026",
            "ten": "Huyện Đồng Văn",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "027",
            "ten": "Huyện Mèo Vạc",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "028",
            "ten": "Huyện Yên Minh",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "029",
            "ten": "Huyện Quản Bạ",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "030",
            "ten": "Huyện Vị Xuyên",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "031",
            "ten": "Huyện Bắc Mê",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "032",
            "ten": "Huyện Hoàng Su Phì",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "033",
            "ten": "Huyện Xín Mần",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "034",
            "ten": "Huyện Bắc Quang",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "035",
            "ten": "Huyện Quang Bình",
            "cap": "Huyện",
            "maT": "02",
            "Tỉnh / Thành Phố": "Tỉnh Hà Giang"
        },
        {
            "ma": "040",
            "ten": "Thành phố Cao Bằng",
            "cap": "Thành phố",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "042",
            "ten": "Huyện Bảo Lâm",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "043",
            "ten": "Huyện Bảo Lạc",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "044",
            "ten": "Huyện Thông Nông",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "045",
            "ten": "Huyện Hà Quảng",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "046",
            "ten": "Huyện Trà Lĩnh",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "047",
            "ten": "Huyện Trùng Khánh",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "048",
            "ten": "Huyện Hạ Lang",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "049",
            "ten": "Huyện Quảng Uyên",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "050",
            "ten": "Huyện Phục Hoà",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "051",
            "ten": "Huyện Hoà An",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "052",
            "ten": "Huyện Nguyên Bình",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "053",
            "ten": "Huyện Thạch An",
            "cap": "Huyện",
            "maT": "04",
            "Tỉnh / Thành Phố": "Tỉnh Cao Bằng"
        },
        {
            "ma": "058",
            "ten": "Thành Phố Bắc Kạn",
            "cap": "Thành phố",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "060",
            "ten": "Huyện Pác Nặm",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "061",
            "ten": "Huyện Ba Bể",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "062",
            "ten": "Huyện Ngân Sơn",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "063",
            "ten": "Huyện Bạch Thông",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "064",
            "ten": "Huyện Chợ Đồn",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "065",
            "ten": "Huyện Chợ Mới",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "066",
            "ten": "Huyện Na Rì",
            "cap": "Huyện",
            "maT": "06",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Kạn"
        },
        {
            "ma": "070",
            "ten": "Thành phố Tuyên Quang",
            "cap": "Thành phố",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "071",
            "ten": "Huyện Lâm Bình",
            "cap": "Huyện",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "072",
            "ten": "Huyện Na Hang",
            "cap": "Huyện",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "073",
            "ten": "Huyện Chiêm Hóa",
            "cap": "Huyện",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "074",
            "ten": "Huyện Hàm Yên",
            "cap": "Huyện",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "075",
            "ten": "Huyện Yên Sơn",
            "cap": "Huyện",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "076",
            "ten": "Huyện Sơn Dương",
            "cap": "Huyện",
            "maT": "08",
            "Tỉnh / Thành Phố": "Tỉnh Tuyên Quang"
        },
        {
            "ma": "080",
            "ten": "Thành phố Lào Cai",
            "ten Tiếng Anh": "Lao Cai City",
            "cap": "Thành phố",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "082",
            "ten": "Huyện Bát Xát",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "083",
            "ten": "Huyện Mường Khương",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "084",
            "ten": "Huyện Si Ma Cai",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "085",
            "ten": "Huyện Bắc Hà",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "086",
            "ten": "Huyện Bảo Thắng",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "087",
            "ten": "Huyện Bảo Yên",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "088",
            "ten": "Huyện Sa Pa",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "089",
            "ten": "Huyện Văn Bàn",
            "cap": "Huyện",
            "maT": "10",
            "Tỉnh / Thành Phố": "Tỉnh Lào Cai"
        },
        {
            "ma": "094",
            "ten": "Thành phố Điện Biên Phủ",
            "cap": "Thành phố",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "095",
            "ten": "Thị Xã Mường Lay",
            "cap": "Thị xã",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "096",
            "ten": "Huyện Mường Nhé",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "097",
            "ten": "Huyện Mường Chà",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "098",
            "ten": "Huyện Tủa Chùa",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "099",
            "ten": "Huyện Tuần Giáo",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "100",
            "ten": "Huyện Điện Biên",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "101",
            "ten": "Huyện Điện Biên Đông",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "102",
            "ten": "Huyện Mường Ảng",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "103",
            "ten": "Huyện Nậm Pồ",
            "cap": "Huyện",
            "maT": "11",
            "Tỉnh / Thành Phố": "Tỉnh Điện Biên"
        },
        {
            "ma": "105",
            "ten": "Thành phố Lai Châu",
            "cap": "Thành phố",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "106",
            "ten": "Huyện Tam Đường",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "107",
            "ten": "Huyện Mường Tè",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "108",
            "ten": "Huyện Sìn Hồ",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "109",
            "ten": "Huyện Phong Thổ",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "110",
            "ten": "Huyện Than Uyên",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "111",
            "ten": "Huyện Tân Uyên",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "112",
            "ten": "Huyện Nậm Nhùn",
            "cap": "Huyện",
            "maT": "12",
            "Tỉnh / Thành Phố": "Tỉnh Lai Châu"
        },
        {
            "ma": "116",
            "ten": "Thành phố Sơn La",
            "cap": "Thành phố",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "118",
            "ten": "Huyện Quỳnh Nhai",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "119",
            "ten": "Huyện Thuận Châu",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "120",
            "ten": "Huyện Mường La",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "121",
            "ten": "Huyện Bắc Yên",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "122",
            "ten": "Huyện Phù Yên",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "123",
            "ten": "Huyện Mộc Châu",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "124",
            "ten": "Huyện Yên Châu",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "125",
            "ten": "Huyện Mai Sơn",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "126",
            "ten": "Huyện Sông ma",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "127",
            "ten": "Huyện Sốp Cộp",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "128",
            "ten": "Huyện Vân Hồ",
            "cap": "Huyện",
            "maT": "14",
            "Tỉnh / Thành Phố": "Tỉnh Sơn La"
        },
        {
            "ma": "132",
            "ten": "Thành phố Yên Bái",
            "cap": "Thành phố",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "133",
            "ten": "Thị xã Nghĩa Lộ",
            "cap": "Thị xã",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "135",
            "ten": "Huyện Lục Yên",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "136",
            "ten": "Huyện Văn Yên",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "137",
            "ten": "Huyện Mù Căng Chải",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "138",
            "ten": "Huyện Trấn Yên",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "139",
            "ten": "Huyện Trạm Tấu",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "140",
            "ten": "Huyện Văn Chấn",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "141",
            "ten": "Huyện Yên Bình",
            "cap": "Huyện",
            "maT": "15",
            "Tỉnh / Thành Phố": "Tỉnh Yên Bái"
        },
        {
            "ma": "148",
            "ten": "Thành phố Hòa Bình",
            "ten Tiếng Anh": "Hòa Bình City",
            "cap": "Thành phố",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "150",
            "ten": "Huyện Đà Bắc",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "151",
            "ten": "Huyện Kỳ Sơn",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "152",
            "ten": "Huyện Lương Sơn",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "153",
            "ten": "Huyện Kim Bôi",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "154",
            "ten": "Huyện Cao Phong",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "155",
            "ten": "Huyện Tân Lạc",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "156",
            "ten": "Huyện Mai Châu",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "157",
            "ten": "Huyện Lạc Sơn",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "158",
            "ten": "Huyện Yên Thủy",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "159",
            "ten": "Huyện Lạc Thủy",
            "cap": "Huyện",
            "maT": "17",
            "Tỉnh / Thành Phố": "Tỉnh Hoà Bình"
        },
        {
            "ma": "164",
            "ten": "Thành phố Thái Nguyên",
            "cap": "Thành phố",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "165",
            "ten": "Thành phố Sông Công",
            "cap": "Thành phố",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "167",
            "ten": "Huyện Định Hóa",
            "cap": "Huyện",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "168",
            "ten": "Huyện Phú Lương",
            "cap": "Huyện",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "169",
            "ten": "Huyện Đồng Hỷ",
            "cap": "Huyện",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "170",
            "ten": "Huyện Võ Nhai",
            "cap": "Huyện",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "171",
            "ten": "Huyện Đại Từ",
            "cap": "Huyện",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "172",
            "ten": "Thị xã Phổ Yên",
            "cap": "Thị xã",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "173",
            "ten": "Huyện Phú Bình",
            "cap": "Huyện",
            "maT": "19",
            "Tỉnh / Thành Phố": "Tỉnh Thái Nguyên"
        },
        {
            "ma": "178",
            "ten": "Thành phố Lạng Sơn",
            "cap": "Thành phố",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "180",
            "ten": "Huyện Tràng Định",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "181",
            "ten": "Huyện Bình Gia",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "182",
            "ten": "Huyện Văn Lãng",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "183",
            "ten": "Huyện Cao Lộc",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "184",
            "ten": "Huyện Văn Quan",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "185",
            "ten": "Huyện Bắc Sơn",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "186",
            "ten": "Huyện Hữu Lũng",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "187",
            "ten": "Huyện Chi Lăng",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "188",
            "ten": "Huyện Lộc Bình",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "189",
            "ten": "Huyện Đình Lập",
            "cap": "Huyện",
            "maT": "20",
            "Tỉnh / Thành Phố": "Tỉnh Lạng Sơn"
        },
        {
            "ma": "193",
            "ten": "Thành phố Hạ Long",
            "cap": "Thành phố",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "194",
            "ten": "Thành phố Móng Cái",
            "cap": "Thành phố",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "195",
            "ten": "Thành phố Cẩm Phả",
            "cap": "Thành phố",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "196",
            "ten": "Thành phố Uông Bí",
            "cap": "Thành phố",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "198",
            "ten": "Huyện Bình Liêu",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "199",
            "ten": "Huyện Tiên Yên",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "200",
            "ten": "Huyện Đầm Hà",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "201",
            "ten": "Huyện Hải Hà",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "202",
            "ten": "Huyện Ba Chẽ",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "203",
            "ten": "Huyện Vân Đồn",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "204",
            "ten": "Huyện Hoành Bồ",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "205",
            "ten": "Thị xã Đông Triều",
            "cap": "Thị xã",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "206",
            "ten": "Thị xã Quảng Yên",
            "cap": "Thị xã",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "207",
            "ten": "Huyện Cô Tô",
            "cap": "Huyện",
            "maT": "22",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ninh"
        },
        {
            "ma": "213",
            "ten": "Thành phố Bắc Giang",
            "ten Tiếng Anh": "Bac Giang city",
            "cap": "Thành phố",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "215",
            "ten": "Huyện Yên Thế",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "216",
            "ten": "Huyện Tân Yên",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "217",
            "ten": "Huyện Lạng Giang",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "218",
            "ten": "Huyện Lục Nam",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "219",
            "ten": "Huyện Lục Ngạn",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "220",
            "ten": "Huyện Sơn Động",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "221",
            "ten": "Huyện Yên Dũng",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "222",
            "ten": "Huyện Việt Yên",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "223",
            "ten": "Huyện Hiệp Hòa",
            "cap": "Huyện",
            "maT": "24",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Giang"
        },
        {
            "ma": "227",
            "ten": "Thành phố Việt Trì",
            "cap": "Thành phố",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "228",
            "ten": "Thị xã Phú Thọ",
            "cap": "Thị xã",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "230",
            "ten": "Huyện Đoan Hùng",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "231",
            "ten": "Huyện Hạ Hoà",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "232",
            "ten": "Huyện Thanh Ba",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "233",
            "ten": "Huyện Phù Ninh",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "234",
            "ten": "Huyện Yên Lập",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "235",
            "ten": "Huyện Cẩm Khê",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "236",
            "ten": "Huyện Tam Nông",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "237",
            "ten": "Huyện Lâm Thao",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "238",
            "ten": "Huyện Thanh Sơn",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "239",
            "ten": "Huyện Thanh Thuỷ",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "240",
            "ten": "Huyện Tân Sơn",
            "cap": "Huyện",
            "maT": "25",
            "Tỉnh / Thành Phố": "Tỉnh Phú Thọ"
        },
        {
            "ma": "243",
            "ten": "Thành phố Vĩnh Yên",
            "ten Tiếng Anh": "Vĩnh Yên City",
            "cap": "Thành phố",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "244",
            "ten": "Thành phố Phúc Yên",
            "cap": "Thành phố",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "246",
            "ten": "Huyện Lập Thạch",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "247",
            "ten": "Huyện Tam Dương",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "248",
            "ten": "Huyện Tam Đảo",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "249",
            "ten": "Huyện Bình Xuyên",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "251",
            "ten": "Huyện Yên Lạc",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "252",
            "ten": "Huyện Vĩnh Tường",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "253",
            "ten": "Huyện Sông Lô",
            "cap": "Huyện",
            "maT": "26",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Phúc"
        },
        {
            "ma": "256",
            "ten": "Thành phố Bắc Ninh",
            "ten Tiếng Anh": "Bac Ninh City",
            "cap": "Thành phố",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "258",
            "ten": "Huyện Yên Phong",
            "cap": "Huyện",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "259",
            "ten": "Huyện Quế Võ",
            "cap": "Huyện",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "260",
            "ten": "Huyện Tiên Du",
            "cap": "Huyện",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "261",
            "ten": "Thị xã Từ Sơn",
            "cap": "Thị xã",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "262",
            "ten": "Huyện Thuận Thành",
            "cap": "Huyện",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "263",
            "ten": "Huyện Gia Bình",
            "cap": "Huyện",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "264",
            "ten": "Huyện Lương Tài",
            "cap": "Huyện",
            "maT": "27",
            "Tỉnh / Thành Phố": "Tỉnh Bắc Ninh"
        },
        {
            "ma": "288",
            "ten": "Thành phố Hải Dương",
            "cap": "Thành phố",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "290",
            "ten": "Thành phố Chí Linh",
            "cap": "Thành phố",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "291",
            "ten": "Huyện Nam Sách",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "292",
            "ten": "Huyện Kinh Môn",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "293",
            "ten": "Huyện Kim Thành",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "294",
            "ten": "Huyện Thanh Hà",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "295",
            "ten": "Huyện Cẩm Giàng",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "296",
            "ten": "Huyện Bình Giang",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "297",
            "ten": "Huyện Gia Lộc",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "298",
            "ten": "Huyện Tứ Kỳ",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "299",
            "ten": "Huyện Ninh Giang",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "300",
            "ten": "Huyện Thanh Miện",
            "cap": "Huyện",
            "maT": "30",
            "Tỉnh / Thành Phố": "Tỉnh Hải Dương"
        },
        {
            "ma": "303",
            "ten": "Quận Hồng Bàng",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "304",
            "ten": "Quận Ngô Quyền",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "305",
            "ten": "Quận Lê Chân",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "306",
            "ten": "Quận Hải An",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "307",
            "ten": "Quận Kiến An",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "308",
            "ten": "Quận Đồ Sơn",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "309",
            "ten": "Quận Dương Kinh",
            "cap": "Quận",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "311",
            "ten": "Huyện Thuỷ Nguyên",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "312",
            "ten": "Huyện An Dương",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "313",
            "ten": "Huyện An Lão",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "314",
            "ten": "Huyện Kiến Thuỵ",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "315",
            "ten": "Huyện Tiên Lãng",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "316",
            "ten": "Huyện Vĩnh Bảo",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "317",
            "ten": "Huyện Cát Hải",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "318",
            "ten": "Huyện Bạch Long Vĩ",
            "cap": "Huyện",
            "maT": "31",
            "Tỉnh / Thành Phố": "Thành phố Hải Phòng"
        },
        {
            "ma": "323",
            "ten": "Thành phố Hưng Yên",
            "cap": "Thành phố",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "325",
            "ten": "Huyện Văn Lâm",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "326",
            "ten": "Huyện Văn Giang",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "327",
            "ten": "Huyện Yên Mỹ",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "328",
            "ten": "Thị xã Mỹ Hào",
            "cap": "Thị xã",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "329",
            "ten": "Huyện Ân Thi",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "330",
            "ten": "Huyện Khoái Châu",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "331",
            "ten": "Huyện Kim Động",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "332",
            "ten": "Huyện Tiên Lữ",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "333",
            "ten": "Huyện Phù Cừ",
            "cap": "Huyện",
            "maT": "33",
            "Tỉnh / Thành Phố": "Tỉnh Hưng Yên"
        },
        {
            "ma": "336",
            "ten": "Thành phố Thái Bình",
            "cap": "Thành phố",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "338",
            "ten": "Huyện Quỳnh Phụ",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "339",
            "ten": "Huyện Hưng Hà",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "340",
            "ten": "Huyện Đông Hưng",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "341",
            "ten": "Huyện Thái Thụy",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "342",
            "ten": "Huyện Tiền Hải",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "343",
            "ten": "Huyện Kiến Xương",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "344",
            "ten": "Huyện Vũ Thư",
            "cap": "Huyện",
            "maT": "34",
            "Tỉnh / Thành Phố": "Tỉnh Thái Bình"
        },
        {
            "ma": "347",
            "ten": "Thành phố Phủ Lý",
            "cap": "Thành phố",
            "maT": "35",
            "Tỉnh / Thành Phố": "Tỉnh Hà Nam"
        },
        {
            "ma": "349",
            "ten": "Huyện Duy Tiên",
            "cap": "Huyện",
            "maT": "35",
            "Tỉnh / Thành Phố": "Tỉnh Hà Nam"
        },
        {
            "ma": "350",
            "ten": "Huyện Kim Bảng",
            "cap": "Huyện",
            "maT": "35",
            "Tỉnh / Thành Phố": "Tỉnh Hà Nam"
        },
        {
            "ma": "351",
            "ten": "Huyện Thanh Liêm",
            "cap": "Huyện",
            "maT": "35",
            "Tỉnh / Thành Phố": "Tỉnh Hà Nam"
        },
        {
            "ma": "352",
            "ten": "Huyện Bình Lục",
            "cap": "Huyện",
            "maT": "35",
            "Tỉnh / Thành Phố": "Tỉnh Hà Nam"
        },
        {
            "ma": "353",
            "ten": "Huyện Lý Nhân",
            "cap": "Huyện",
            "maT": "35",
            "Tỉnh / Thành Phố": "Tỉnh Hà Nam"
        },
        {
            "ma": "356",
            "ten": "Thành phố Nam Định",
            "cap": "Thành phố",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "358",
            "ten": "Huyện Mỹ Lộc",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "359",
            "ten": "Huyện Vụ Bản",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "360",
            "ten": "Huyện Ý Yên",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "361",
            "ten": "Huyện Nghĩa Hưng",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "362",
            "ten": "Huyện Nam Trực",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "363",
            "ten": "Huyện Trực Ninh",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "364",
            "ten": "Huyện Xuân Trường",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "365",
            "ten": "Huyện Giao Thủy",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "366",
            "ten": "Huyện Hải Hậu",
            "cap": "Huyện",
            "maT": "36",
            "Tỉnh / Thành Phố": "Tỉnh Nam Định"
        },
        {
            "ma": "369",
            "ten": "Thành phố Ninh Bình",
            "cap": "Thành phố",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "370",
            "ten": "Thành phố Tam Điệp",
            "cap": "Thành phố",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "372",
            "ten": "Huyện Nho Quan",
            "cap": "Huyện",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "373",
            "ten": "Huyện Gia Viễn",
            "cap": "Huyện",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "374",
            "ten": "Huyện Hoa Lư",
            "cap": "Huyện",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "375",
            "ten": "Huyện Yên Khánh",
            "cap": "Huyện",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "376",
            "ten": "Huyện Kim Sơn",
            "cap": "Huyện",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "377",
            "ten": "Huyện Yên Mô",
            "cap": "Huyện",
            "maT": "37",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Bình"
        },
        {
            "ma": "380",
            "ten": "Thành phố Thanh Hóa",
            "cap": "Thành phố",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "381",
            "ten": "Thị xã Bỉm Sơn",
            "cap": "Thị xã",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "382",
            "ten": "Thành phố Sầm Sơn",
            "cap": "Thành phố",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "384",
            "ten": "Huyện Mường Lát",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "385",
            "ten": "Huyện Quan Hóa",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "386",
            "ten": "Huyện Bá Thước",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "387",
            "ten": "Huyện Quan Sơn",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "388",
            "ten": "Huyện Lang Chánh",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "389",
            "ten": "Huyện Ngọc Lặc",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "390",
            "ten": "Huyện Cẩm Thủy",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "391",
            "ten": "Huyện Thạch Thành",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "392",
            "ten": "Huyện Hà Trung",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "393",
            "ten": "Huyện Vĩnh Lộc",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "394",
            "ten": "Huyện Yên Định",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "395",
            "ten": "Huyện Thọ Xuân",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "396",
            "ten": "Huyện Thường Xuân",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "397",
            "ten": "Huyện Triệu Sơn",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "398",
            "ten": "Huyện Thiệu Hóa",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "399",
            "ten": "Huyện Hoằng Hóa",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "400",
            "ten": "Huyện Hậu Lộc",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "401",
            "ten": "Huyện Nga Sơn",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "402",
            "ten": "Huyện Như Xuân",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "403",
            "ten": "Huyện Như Thanh",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "404",
            "ten": "Huyện Nông Cống",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "405",
            "ten": "Huyện Đông Sơn",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "406",
            "ten": "Huyện Quảng Xương",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "407",
            "ten": "Huyện Tĩnh Gia",
            "cap": "Huyện",
            "maT": "38",
            "Tỉnh / Thành Phố": "Tỉnh Thanh Hóa"
        },
        {
            "ma": "412",
            "ten": "Thành phố Vinh",
            "cap": "Thành phố",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "413",
            "ten": "Thị xã Cửa Lò",
            "cap": "Thị xã",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "414",
            "ten": "Thị xã Thái Hoà",
            "cap": "Thị xã",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "415",
            "ten": "Huyện Quế Phong",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "416",
            "ten": "Huyện Quỳ Châu",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "417",
            "ten": "Huyện Kỳ Sơn",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "418",
            "ten": "Huyện Tương Dương",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "419",
            "ten": "Huyện Nghĩa Đàn",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "420",
            "ten": "Huyện Quỳ Hợp",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "421",
            "ten": "Huyện Quỳnh Lưu",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "422",
            "ten": "Huyện Con Cuông",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "423",
            "ten": "Huyện Tân Kỳ",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "424",
            "ten": "Huyện Anh Sơn",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "425",
            "ten": "Huyện Diễn Châu",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "426",
            "ten": "Huyện Yên Thành",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "427",
            "ten": "Huyện Đô Lương",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "428",
            "ten": "Huyện Thanh Chương",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "429",
            "ten": "Huyện Nghi Lộc",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "430",
            "ten": "Huyện Nam Đàn",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "431",
            "ten": "Huyện Hưng Nguyên",
            "cap": "Huyện",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "432",
            "ten": "Thị xã Hoàng Mai",
            "cap": "Thị xã",
            "maT": "40",
            "Tỉnh / Thành Phố": "Tỉnh Nghệ An"
        },
        {
            "ma": "436",
            "ten": "Thành phố Hà Tĩnh",
            "cap": "Thành phố",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "437",
            "ten": "Thị xã Hồng Lĩnh",
            "cap": "Thị xã",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "439",
            "ten": "Huyện Hương Sơn",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "440",
            "ten": "Huyện Đức Thọ",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "441",
            "ten": "Huyện Vũ Quang",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "442",
            "ten": "Huyện Nghi Xuân",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "443",
            "ten": "Huyện Can Lộc",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "444",
            "ten": "Huyện Hương Khê",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "445",
            "ten": "Huyện Thạch Hà",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "446",
            "ten": "Huyện Cẩm Xuyên",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "447",
            "ten": "Huyện Kỳ Anh",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "448",
            "ten": "Huyện Lộc Hà",
            "cap": "Huyện",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "449",
            "ten": "Thị xã Kỳ Anh",
            "cap": "Thị xã",
            "maT": "42",
            "Tỉnh / Thành Phố": "Tỉnh Hà Tĩnh"
        },
        {
            "ma": "450",
            "ten": "Thành Phố Đồng Hới",
            "ten Tiếng Anh": "Dong Hoi City",
            "cap": "Thành phố",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "452",
            "ten": "Huyện Minh Hóa",
            "cap": "Huyện",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "453",
            "ten": "Huyện Tuyên Hóa",
            "cap": "Huyện",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "454",
            "ten": "Huyện Quảng Trạch",
            "cap": "Thị xã",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "455",
            "ten": "Huyện Bố Trạch",
            "cap": "Huyện",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "456",
            "ten": "Huyện Quảng Ninh",
            "cap": "Huyện",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "457",
            "ten": "Huyện Lệ Thủy",
            "cap": "Huyện",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "458",
            "ten": "Thị xã Ba Đồn",
            "cap": "Huyện",
            "maT": "44",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Bình"
        },
        {
            "ma": "461",
            "ten": "Thành phố Đông Hà",
            "cap": "Thành phố",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "462",
            "ten": "Thị xã Quảng Trị",
            "cap": "Thị xã",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "464",
            "ten": "Huyện Vĩnh Linh",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "465",
            "ten": "Huyện Hướng Hóa",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "466",
            "ten": "Huyện Gio Linh",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "467",
            "ten": "Huyện Đa Krông",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "468",
            "ten": "Huyện Cam Lộ",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "469",
            "ten": "Huyện Triệu Phong",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "470",
            "ten": "Huyện Hải Lăng",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "471",
            "ten": "Huyện Cồn Cỏ",
            "cap": "Huyện",
            "maT": "45",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Trị"
        },
        {
            "ma": "474",
            "ten": "Thành phố Huế",
            "cap": "Thành phố",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "476",
            "ten": "Huyện Phong Điền",
            "cap": "Huyện",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "477",
            "ten": "Huyện Quảng Điền",
            "cap": "Huyện",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "478",
            "ten": "Huyện Phú Vang",
            "cap": "Huyện",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "479",
            "ten": "Thị xã Hương Thủy",
            "cap": "Thị xã",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "480",
            "ten": "Thị xã Hương Trà",
            "cap": "Thị xã",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "481",
            "ten": "Huyện A Lưới",
            "cap": "Huyện",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "482",
            "ten": "Huyện Phú Lộc",
            "cap": "Huyện",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "483",
            "ten": "Huyện Nam Đông",
            "cap": "Huyện",
            "maT": "46",
            "Tỉnh / Thành Phố": "Tỉnh Thừa Thiên Huế"
        },
        {
            "ma": "490",
            "ten": "Quận Liên Chiểu",
            "cap": "Quận",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "491",
            "ten": "Quận Thanh Khê",
            "cap": "Quận",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "492",
            "ten": "Quận Hải Châu",
            "cap": "Quận",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "493",
            "ten": "Quận Sơn Trà",
            "cap": "Quận",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "494",
            "ten": "Quận Ngũ Hành Sơn",
            "cap": "Quận",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "495",
            "ten": "Quận Cẩm Lệ",
            "cap": "Quận",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "497",
            "ten": "Huyện Hòa Vang",
            "cap": "Huyện",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "498",
            "ten": "Huyện Hoàng Sa",
            "cap": "Huyện",
            "maT": "48",
            "Tỉnh / Thành Phố": "Thành phố Đà Nẵng"
        },
        {
            "ma": "502",
            "ten": "Thành phố Tam Kỳ",
            "cap": "Thành phố",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "503",
            "ten": "Thành phố Hội An",
            "cap": "Thành phố",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "504",
            "ten": "Huyện Tây Giang",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "505",
            "ten": "Huyện Đông Giang",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "506",
            "ten": "Huyện Đại Lộc",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "507",
            "ten": "Thị xã Điện Bàn",
            "cap": "Thị xã",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "508",
            "ten": "Huyện Duy Xuyên",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "509",
            "ten": "Huyện Quế Sơn",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "510",
            "ten": "Huyện Nam Giang",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "511",
            "ten": "Huyện Phước Sơn",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "512",
            "ten": "Huyện Hiệp Đức",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "513",
            "ten": "Huyện Thăng Bình",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "514",
            "ten": "Huyện Tiên Phước",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "515",
            "ten": "Huyện Bắc Trà My",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "516",
            "ten": "Huyện Nam Trà My",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "517",
            "ten": "Huyện Núi Thành",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "518",
            "ten": "Huyện Phú Ninh",
            "ten Tiếng Anh": "Phú Ninh District",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "519",
            "ten": "Huyện Nông Sơn",
            "cap": "Huyện",
            "maT": "49",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Nam"
        },
        {
            "ma": "522",
            "ten": "Thành phố Quảng Ngãi",
            "cap": "Thành phố",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "524",
            "ten": "Huyện Bình Sơn",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "525",
            "ten": "Huyện Trà Bồng",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "526",
            "ten": "Huyện Tây Trà",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "527",
            "ten": "Huyện Sơn Tịnh",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "528",
            "ten": "Huyện Tư Nghĩa",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "529",
            "ten": "Huyện Sơn Hà",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "530",
            "ten": "Huyện Sơn Tây",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "531",
            "ten": "Huyện Minh Long",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "532",
            "ten": "Huyện Nghĩa Hành",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "533",
            "ten": "Huyện Mộ Đức",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "534",
            "ten": "Huyện Đức Phổ",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "535",
            "ten": "Huyện Ba Tơ",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "536",
            "ten": "Huyện Lý Sơn",
            "cap": "Huyện",
            "maT": "51",
            "Tỉnh / Thành Phố": "Tỉnh Quảng Ngãi"
        },
        {
            "ma": "540",
            "ten": "Thành phố Qui Nhơn",
            "cap": "Thành phố",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "542",
            "ten": "Huyện An Lão",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "543",
            "ten": "Huyện Hoài Nhơn",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "544",
            "ten": "Huyện Hoài Ân",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "545",
            "ten": "Huyện Phù Mỹ",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "546",
            "ten": "Huyện Vĩnh Thạnh",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "547",
            "ten": "Huyện Tây Sơn",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "548",
            "ten": "Huyện Phù Cát",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "549",
            "ten": "Thị xã An Nhơn",
            "cap": "Thị xã",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "550",
            "ten": "Huyện Tuy Phước",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "551",
            "ten": "Huyện Vân Canh",
            "cap": "Huyện",
            "maT": "52",
            "Tỉnh / Thành Phố": "Tỉnh Bình Định"
        },
        {
            "ma": "555",
            "ten": "Thành phố Tuy Hoà",
            "ten Tiếng Anh": "Tuy Hoa City",
            "cap": "Thành phố",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "557",
            "ten": "Thị xã Sông Cầu",
            "cap": "Thị xã",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "558",
            "ten": "Huyện Đồng Xuân",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "559",
            "ten": "Huyện Tuy An",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "560",
            "ten": "Huyện Sơn Hòa",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "561",
            "ten": "Huyện Sông Hinh",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "562",
            "ten": "Huyện Tây Hoà",
            "ten Tiếng Anh": "Tay Hoa commune",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "563",
            "ten": "Huyện Phú Hoà",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "564",
            "ten": "Huyện Đông Hòa",
            "ten Tiếng Anh": "Dong Hoa district",
            "cap": "Huyện",
            "maT": "54",
            "Tỉnh / Thành Phố": "Tỉnh Phú Yên"
        },
        {
            "ma": "568",
            "ten": "Thành phố Nha Trang",
            "cap": "Thành phố",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "569",
            "ten": "Thành phố Cam Ranh",
            "cap": "Thành phố",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "570",
            "ten": "Huyện Cam Lâm",
            "cap": "Huyện",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "571",
            "ten": "Huyện Vạn Ninh",
            "cap": "Huyện",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "572",
            "ten": "Thị xã Ninh Hòa",
            "cap": "Thị xã",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "573",
            "ten": "Huyện Khánh Vĩnh",
            "cap": "Huyện",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "574",
            "ten": "Huyện Diên Khánh",
            "cap": "Huyện",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "575",
            "ten": "Huyện Khánh Sơn",
            "cap": "Huyện",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "576",
            "ten": "Huyện Trường Sa",
            "cap": "Huyện",
            "maT": "56",
            "Tỉnh / Thành Phố": "Tỉnh Khánh Hòa"
        },
        {
            "ma": "582",
            "ten": "Thành phố Phan Rang-Tháp Chàm",
            "cap": "Thành phố",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "584",
            "ten": "Huyện Bác Ái",
            "cap": "Huyện",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "585",
            "ten": "Huyện Ninh Sơn",
            "cap": "Huyện",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "586",
            "ten": "Huyện Ninh Hải",
            "cap": "Huyện",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "587",
            "ten": "Huyện Ninh Phước",
            "cap": "Huyện",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "588",
            "ten": "Huyện Thuận Bắc",
            "ten Tiếng Anh": "Thuan Bac province",
            "cap": "Huyện",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "589",
            "ten": "Huyện Thuận Nam",
            "cap": "Huyện",
            "maT": "58",
            "Tỉnh / Thành Phố": "Tỉnh Ninh Thuận"
        },
        {
            "ma": "593",
            "ten": "Thành phố Phan Thiết",
            "cap": "Thành phố",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "594",
            "ten": "Thị xã La Gi",
            "cap": "Thị xã",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "595",
            "ten": "Huyện Tuy Phong",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "596",
            "ten": "Huyện Bắc Bình",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "597",
            "ten": "Huyện Hàm Thuận Bắc",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "598",
            "ten": "Huyện Hàm Thuận Nam",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "599",
            "ten": "Huyện Tánh Linh",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "600",
            "ten": "Huyện Đức Linh",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "601",
            "ten": "Huyện Hàm Tân",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "602",
            "ten": "Huyện Phú Quí",
            "cap": "Huyện",
            "maT": "60",
            "Tỉnh / Thành Phố": "Tỉnh Bình Thuận"
        },
        {
            "ma": "608",
            "ten": "Thành phố Kon Tum",
            "cap": "Thành phố",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "610",
            "ten": "Huyện Đắk Glei",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "611",
            "ten": "Huyện Ngọc Hồi",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "612",
            "ten": "Huyện Đắk Tô",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "613",
            "ten": "Huyện Kon Plông",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "614",
            "ten": "Huyện Kon Rẫy",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "615",
            "ten": "Huyện Đắk Hà",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "616",
            "ten": "Huyện Sa Thầy",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "617",
            "ten": "Huyện Tu Mơ Rông",
            "ten Tiếng Anh": "Tu Mo Rong district",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "618",
            "ten": "Huyện Ia H' Drai",
            "cap": "Huyện",
            "maT": "62",
            "Tỉnh / Thành Phố": "Tỉnh Kon Tum"
        },
        {
            "ma": "622",
            "ten": "Thành phố Pleiku",
            "cap": "Thành phố",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "623",
            "ten": "Thị xã An Khê",
            "cap": "Thị xã",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "624",
            "ten": "Thị xã Ayun Pa",
            "cap": "Thị xã",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "625",
            "ten": "Huyện KBang",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "626",
            "ten": "Huyện Đăk Đoa",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "627",
            "ten": "Huyện Chư Păh",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "628",
            "ten": "Huyện Ia Grai",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "629",
            "ten": "Huyện Mang Yang",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "630",
            "ten": "Huyện Kông Chro",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "631",
            "ten": "Huyện Đức Cơ",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "632",
            "ten": "Huyện Chư Prông",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "633",
            "ten": "Huyện Chư Sê",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "634",
            "ten": "Huyện Đăk Pơ",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "635",
            "ten": "Huyện Ia Pa",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "637",
            "ten": "Huyện Krông Pa",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "638",
            "ten": "Huyện Phú Thiện",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "639",
            "ten": "Huyện Chư Pưh",
            "cap": "Huyện",
            "maT": "64",
            "Tỉnh / Thành Phố": "Tỉnh Gia Lai"
        },
        {
            "ma": "643",
            "ten": "Thành phố Buôn Ma Thuột",
            "cap": "Thành phố",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "644",
            "ten": "Thị Xã Buôn Hồ",
            "cap": "Thị xã",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "645",
            "ten": "Huyện Ea H'leo",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "646",
            "ten": "Huyện Ea Súp",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "647",
            "ten": "Huyện Buôn Đôn",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "648",
            "ten": "Huyện Cư M'gar",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "649",
            "ten": "Huyện Krông Búk",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "650",
            "ten": "Huyện Krông Năng",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "651",
            "ten": "Huyện Ea Kar",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "652",
            "ten": "Huyện M'Đrắk",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "653",
            "ten": "Huyện Krông Bông",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "654",
            "ten": "Huyện Krông Pắc",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "655",
            "ten": "Huyện Krông A Na",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "656",
            "ten": "Huyện Lắk",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "657",
            "ten": "Huyện Cư Kuin",
            "cap": "Huyện",
            "maT": "66",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Lắk"
        },
        {
            "ma": "660",
            "ten": "Thị xã Gia Nghĩa",
            "cap": "Thị xã",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "661",
            "ten": "Huyện Đăk Glong",
            "ten Tiếng Anh": "Dak Glong province",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "662",
            "ten": "Huyện Cư Jút",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "663",
            "ten": "Huyện Đắk Mil",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "664",
            "ten": "Huyện Krông Nô",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "665",
            "ten": "Huyện Đắk Song",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "666",
            "ten": "Huyện Đắk R'Lấp",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "667",
            "ten": "Huyện Tuy Đức",
            "cap": "Huyện",
            "maT": "67",
            "Tỉnh / Thành Phố": "Tỉnh Đắk Nông"
        },
        {
            "ma": "672",
            "ten": "Thành phố Đà Lạt",
            "cap": "Thành phố",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "673",
            "ten": "Thành phố Bảo Lộc",
            "cap": "Thành phố",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "674",
            "ten": "Huyện Đam Rông",
            "ten Tiếng Anh": "Dam Rong Province",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "675",
            "ten": "Huyện Lạc Dương",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "676",
            "ten": "Huyện Lâm Hà",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "677",
            "ten": "Huyện Đơn Dương",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "678",
            "ten": "Huyện Đức Trọng",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "679",
            "ten": "Huyện Di Linh",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "680",
            "ten": "Huyện Bảo Lâm",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "681",
            "ten": "Huyện Đạ Huoai",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "682",
            "ten": "Huyện Đạ Tẻh",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "683",
            "ten": "Huyện Cát Tiên",
            "cap": "Huyện",
            "maT": "68",
            "Tỉnh / Thành Phố": "Tỉnh Lâm Đồng"
        },
        {
            "ma": "688",
            "ten": "Thị xã Phước Long",
            "cap": "Thị xã",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "689",
            "ten": "Thành phố Đồng Xoài",
            "cap": "Thành phố",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "690",
            "ten": "Thị xã Bình Long",
            "cap": "Thị xã",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "691",
            "ten": "Huyện Bù Gia Mập",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "692",
            "ten": "Huyện Lộc Ninh",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "693",
            "ten": "Huyện Bù Đốp",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "694",
            "ten": "Huyện Hớn Quản",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "695",
            "ten": "Huyện Đồng Phú",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "696",
            "ten": "Huyện Bù Đăng",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "697",
            "ten": "Huyện Chơn Thành",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "698",
            "ten": "Huyện Phú Riềng",
            "cap": "Huyện",
            "maT": "70",
            "Tỉnh / Thành Phố": "Tỉnh Bình Phước"
        },
        {
            "ma": "703",
            "ten": "Thành phố Tây Ninh",
            "cap": "Thành phố",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "705",
            "ten": "Huyện Tân Biên",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "706",
            "ten": "Huyện Tân Châu",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "707",
            "ten": "Huyện Dương Minh Châu",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "708",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "709",
            "ten": "Huyện Hòa Thành",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "710",
            "ten": "Huyện Gò Dầu",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "711",
            "ten": "Huyện Bến Cầu",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "712",
            "ten": "Huyện Trảng Bàng",
            "cap": "Huyện",
            "maT": "72",
            "Tỉnh / Thành Phố": "Tỉnh Tây Ninh"
        },
        {
            "ma": "718",
            "ten": "Thành phố Thủ Dầu Một",
            "cap": "Thành phố",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "719",
            "ten": "Huyện Bàu Bàng",
            "cap": "Huyện",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "720",
            "ten": "Huyện Dầu Tiếng",
            "cap": "Huyện",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "721",
            "ten": "Thị xã Bến Cát",
            "cap": "Thị xã",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "722",
            "ten": "Huyện Phú Giáo",
            "cap": "Huyện",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "723",
            "ten": "Thị xã Tân Uyên",
            "cap": "Thị xã",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "724",
            "ten": "Thị xã Dĩ An",
            "cap": "Thị xã",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "725",
            "ten": "Thị xã Thuận An",
            "cap": "Thị xã",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "726",
            "ten": "Huyện Bắc Tân Uyên",
            "cap": "Huyện",
            "maT": "74",
            "Tỉnh / Thành Phố": "Tỉnh Bình Dương"
        },
        {
            "ma": "731",
            "ten": "Thành phố Biên Hòa",
            "cap": "Thành phố",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "732",
            "ten": "Thành phố Long Khánh",
            "cap": "Thành phố",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "734",
            "ten": "Huyện Tân Phú",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "735",
            "ten": "Huyện Vĩnh Cửu",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "736",
            "ten": "Huyện Định Quán",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "737",
            "ten": "Huyện Trảng Bom",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "738",
            "ten": "Huyện Thống Nhất",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "739",
            "ten": "Huyện Cẩm Mỹ",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "740",
            "ten": "Huyện Long Thành",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "741",
            "ten": "Huyện Xuân Lộc",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "742",
            "ten": "Huyện Nhơn Trạch",
            "cap": "Huyện",
            "maT": "75",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Nai"
        },
        {
            "ma": "747",
            "ten": "Thành phố Vũng Tàu",
            "cap": "Thành phố",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "748",
            "ten": "Thành phố Bà Rịa",
            "cap": "Thành phố",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "750",
            "ten": "Huyện Châu Đức",
            "cap": "Huyện",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "751",
            "ten": "Huyện Xuyên Mộc",
            "cap": "Huyện",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "752",
            "ten": "Huyện Long Điền",
            "cap": "Huyện",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "753",
            "ten": "Huyện Đất Đỏ",
            "cap": "Huyện",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "754",
            "ten": "Thị xã Phú Mỹ",
            "cap": "Thị xã",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "755",
            "ten": "Huyện Côn Đảo",
            "cap": "Huyện",
            "maT": "77",
            "Tỉnh / Thành Phố": "Tỉnh Bà Rịa - Vũng Tàu"
        },
        {
            "ma": "760",
            "ten": "Quận 1",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "761",
            "ten": "Quận 12",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "762",
            "ten": "Quận Thủ Đức",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "763",
            "ten": "Quận 9",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "764",
            "ten": "Quận Gò Vấp",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "765",
            "ten": "Quận Bình Thạnh",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "766",
            "ten": "Quận Tân Bình",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "767",
            "ten": "Quận Tân Phú",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "768",
            "ten": "Quận Phú Nhuận",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "769",
            "ten": "Quận 2",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "770",
            "ten": "Quận 3",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "771",
            "ten": "Quận 10",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "772",
            "ten": "Quận 11",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "773",
            "ten": "Quận 4",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "774",
            "ten": "Quận 5",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "775",
            "ten": "Quận 6",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "776",
            "ten": "Quận 8",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "777",
            "ten": "Quận Bình Tân",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "778",
            "ten": "Quận 7",
            "cap": "Quận",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "783",
            "ten": "Huyện Củ Chi",
            "cap": "Huyện",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "784",
            "ten": "Huyện Hóc Môn",
            "cap": "Huyện",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "785",
            "ten": "Huyện Bình Chánh",
            "cap": "Huyện",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "786",
            "ten": "Huyện Nhà Bè",
            "cap": "Huyện",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "787",
            "ten": "Huyện Cần Giờ",
            "cap": "Huyện",
            "maT": "79",
            "Tỉnh / Thành Phố": "Thành phố Hồ Chí Minh"
        },
        {
            "ma": "794",
            "ten": "Thành phố Tân An",
            "cap": "Thành phố",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "795",
            "ten": "Thị xã Kiến Tường",
            "cap": "Thị xã",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "796",
            "ten": "Huyện Tân Hưng",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "797",
            "ten": "Huyện Vĩnh Hưng",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "798",
            "ten": "Huyện Mộc Hóa",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "799",
            "ten": "Huyện Tân Thạnh",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "800",
            "ten": "Huyện Thạnh Hóa",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "801",
            "ten": "Huyện Đức Huệ",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "802",
            "ten": "Huyện Đức Hòa",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "803",
            "ten": "Huyện Bến Lức",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "804",
            "ten": "Huyện Thủ Thừa",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "805",
            "ten": "Huyện Tân Trụ",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "806",
            "ten": "Huyện Cần Đước",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "807",
            "ten": "Huyện Cần Giuộc",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "808",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "80",
            "Tỉnh / Thành Phố": "Tỉnh Long An"
        },
        {
            "ma": "815",
            "ten": "Thành phố Mỹ Tho",
            "cap": "Thành phố",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "816",
            "ten": "Thị xã Gò Công",
            "cap": "Thị xã",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "817",
            "ten": "Thị xã Cai Lậy",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "818",
            "ten": "Huyện Tân Phước",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "819",
            "ten": "Huyện Cái Bè",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "820",
            "ten": "Huyện Cai Lậy",
            "cap": "Thị xã",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "821",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "822",
            "ten": "Huyện Chợ Gạo",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "823",
            "ten": "Huyện Gò Công Tây",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "824",
            "ten": "Huyện Gò Công Đông",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "825",
            "ten": "Huyện Tân Phú Đông",
            "cap": "Huyện",
            "maT": "82",
            "Tỉnh / Thành Phố": "Tỉnh Tiền Giang"
        },
        {
            "ma": "829",
            "ten": "Thành phố Bến Tre",
            "cap": "Thành phố",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "831",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "832",
            "ten": "Huyện Chợ Lách",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "833",
            "ten": "Huyện Mỏ Cày Nam",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "834",
            "ten": "Huyện Giồng Trôm",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "835",
            "ten": "Huyện Bình Đại",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "836",
            "ten": "Huyện Ba Tri",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "837",
            "ten": "Huyện Thạnh Phú",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "838",
            "ten": "Huyện Mỏ Cày Bắc",
            "cap": "Huyện",
            "maT": "83",
            "Tỉnh / Thành Phố": "Tỉnh Bến Tre"
        },
        {
            "ma": "842",
            "ten": "Thành phố Trà Vinh",
            "cap": "Thành phố",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "844",
            "ten": "Huyện Càng Long",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "845",
            "ten": "Huyện Cầu Kè",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "846",
            "ten": "Huyện Tiểu Cần",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "847",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "848",
            "ten": "Huyện Cầu Ngang",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "849",
            "ten": "Huyện Trà Cú",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "850",
            "ten": "Huyện Duyên Hải",
            "cap": "Huyện",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "851",
            "ten": "Thị xã Duyên Hải",
            "cap": "Thị xã",
            "maT": "84",
            "Tỉnh / Thành Phố": "Tỉnh Trà Vinh"
        },
        {
            "ma": "855",
            "ten": "Thành phố Vĩnh Long",
            "cap": "Thành phố",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "857",
            "ten": "Huyện Long Hồ",
            "cap": "Huyện",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "858",
            "ten": "Huyện Mang Thít",
            "cap": "Huyện",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "859",
            "ten": "Huyện  Vũng Liêm",
            "cap": "Huyện",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "860",
            "ten": "Huyện Tam Bình",
            "cap": "Huyện",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "861",
            "ten": "Thị xã Bình Minh",
            "cap": "Thị xã",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "862",
            "ten": "Huyện Trà Ôn",
            "cap": "Huyện",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "863",
            "ten": "Huyện Bình Tân",
            "cap": "Huyện",
            "maT": "86",
            "Tỉnh / Thành Phố": "Tỉnh Vĩnh Long"
        },
        {
            "ma": "866",
            "ten": "Thành phố Cao Lãnh",
            "cap": "Thành phố",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "867",
            "ten": "Thành phố Sa Đéc",
            "cap": "Thành phố",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "868",
            "ten": "Thị xã Hồng Ngự",
            "cap": "Thị xã",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "869",
            "ten": "Huyện Tân Hồng",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "870",
            "ten": "Huyện Hồng Ngự",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "871",
            "ten": "Huyện Tam Nông",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "872",
            "ten": "Huyện Tháp Mười",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "873",
            "ten": "Huyện Cao Lãnh",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "874",
            "ten": "Huyện Thanh Bình",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "875",
            "ten": "Huyện Lấp Vò",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "876",
            "ten": "Huyện Lai Vung",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "877",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "87",
            "Tỉnh / Thành Phố": "Tỉnh Đồng Tháp"
        },
        {
            "ma": "883",
            "ten": "Thành phố Long Xuyên",
            "cap": "Thành phố",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "884",
            "ten": "Thành phố Châu Đốc",
            "cap": "Thành phố",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "886",
            "ten": "Huyện An Phú",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "887",
            "ten": "Thị xã Tân Châu",
            "cap": "Thị xã",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "888",
            "ten": "Huyện Phú Tân",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "889",
            "ten": "Huyện Châu Phú",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "890",
            "ten": "Huyện Tịnh Biên",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "891",
            "ten": "Huyện Tri Tôn",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "892",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "893",
            "ten": "Huyện Chợ Mới",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "894",
            "ten": "Huyện Thoại Sơn",
            "cap": "Huyện",
            "maT": "89",
            "Tỉnh / Thành Phố": "Tỉnh An Giang"
        },
        {
            "ma": "899",
            "ten": "Thành phố Rạch Giá",
            "ten Tiếng Anh": "Rach Gia city",
            "cap": "Thành phố",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "900",
            "ten": "Thành phố Hà Tiên",
            "cap": "Thành phố",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "902",
            "ten": "Huyện Kiên Lương",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "903",
            "ten": "Huyện Hòn Đất",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "904",
            "ten": "Huyện Tân Hiệp",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "905",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "906",
            "ten": "Huyện Giồng Riềng",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "907",
            "ten": "Huyện Gò Quao",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "908",
            "ten": "Huyện An Biên",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "909",
            "ten": "Huyện An Minh",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "910",
            "ten": "Huyện Vĩnh Thuận",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "911",
            "ten": "Huyện Phú Quốc",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "912",
            "ten": "Huyện Kiên Hải",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "913",
            "ten": "Huyện U Minh Thượng",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "914",
            "ten": "Huyện Giang Thành",
            "cap": "Huyện",
            "maT": "91",
            "Tỉnh / Thành Phố": "Tỉnh Kiên Giang"
        },
        {
            "ma": "916",
            "ten": "Quận Ninh Kiều",
            "cap": "Quận",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "917",
            "ten": "Quận Ô Môn",
            "cap": "Quận",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "918",
            "ten": "Quận Bình Thuỷ",
            "cap": "Quận",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "919",
            "ten": "Quận Cái Răng",
            "cap": "Quận",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "923",
            "ten": "Quận Thốt Nốt",
            "cap": "Quận",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "924",
            "ten": "Huyện Vĩnh Thạnh",
            "cap": "Huyện",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "925",
            "ten": "Huyện Cờ Đỏ",
            "cap": "Huyện",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "926",
            "ten": "Huyện Phong Điền",
            "cap": "Huyện",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "927",
            "ten": "Huyện Thới Lai",
            "cap": "Huyện",
            "maT": "92",
            "Tỉnh / Thành Phố": "Thành phố Cần Thơ"
        },
        {
            "ma": "930",
            "ten": "Thành phố Vị Thanh",
            "cap": "Thành phố",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "931",
            "ten": "Thị xã Ngã Bảy",
            "cap": "Thị xã",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "932",
            "ten": "Huyện Châu Thành A",
            "cap": "Huyện",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "933",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "934",
            "ten": "Huyện Phụng Hiệp",
            "cap": "Huyện",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "935",
            "ten": "Huyện Vị Thuỷ",
            "cap": "Huyện",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "936",
            "ten": "Huyện Long Mỹ",
            "cap": "Huyện",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "937",
            "ten": "Thị xã Long Mỹ",
            "cap": "Thị xã",
            "maT": "93",
            "Tỉnh / Thành Phố": "Tỉnh Hậu Giang"
        },
        {
            "ma": "941",
            "ten": "Thành phố Sóc Trăng",
            "cap": "Thành phố",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "942",
            "ten": "Huyện Châu Thành",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "943",
            "ten": "Huyện Kế Sách",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "944",
            "ten": "Huyện Mỹ Tú",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "945",
            "ten": "Huyện Cù Lao Dung",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "946",
            "ten": "Huyện Long Phú",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "947",
            "ten": "Huyện Mỹ Xuyên",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "948",
            "ten": "Thị xã Ngã Năm",
            "cap": "Thị xã",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "949",
            "ten": "Huyện Thạnh Trị",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "950",
            "ten": "Thị xã Vĩnh Châu",
            "cap": "Thị xã",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "951",
            "ten": "Huyện Trần Đề",
            "cap": "Huyện",
            "maT": "94",
            "Tỉnh / Thành Phố": "Tỉnh Sóc Trăng"
        },
        {
            "ma": "954",
            "ten": "Thành phố Bạc Liêu",
            "cap": "Thành phố",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "956",
            "ten": "Huyện Hồng Dân",
            "cap": "Huyện",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "957",
            "ten": "Huyện Phước Long",
            "cap": "Huyện",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "958",
            "ten": "Huyện Vĩnh Lợi",
            "cap": "Huyện",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "959",
            "ten": "Thị xã Giá Rai",
            "cap": "Thị xã",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "960",
            "ten": "Huyện Đông Hải",
            "cap": "Huyện",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "961",
            "ten": "Huyện Hoà Bình",
            "ten Tiếng Anh": "Hoa Binh district",
            "cap": "Huyện",
            "maT": "95",
            "Tỉnh / Thành Phố": "Tỉnh Bạc Liêu"
        },
        {
            "ma": "964",
            "ten": "Thành phố Cà Mau",
            "cap": "Thành phố",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "966",
            "ten": "Huyện U Minh",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "967",
            "ten": "Huyện Thới Bình",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "968",
            "ten": "Huyện Trần Văn Thời",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "969",
            "ten": "Huyện Cái Nước",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "970",
            "ten": "Huyện Đầm Dơi",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "971",
            "ten": "Huyện Năm Căn",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "972",
            "ten": "Huyện Phú Tân",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        {
            "ma": "973",
            "ten": "Huyện Ngọc Hiển",
            "cap": "Huyện",
            "maT": "96",
            "Tỉnh / Thành Phố": "Tỉnh Cà Mau"
        },
        
    ]
