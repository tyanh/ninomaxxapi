﻿  [
        {
            "ma": "01",
            "ten": "Thành phố Hà Nội",
            "cap": "Thành phố Trung ương"
        },
        {
            "ma": "02",
            "ten": "Tỉnh Hà Giang",
            "cap": "Tỉnh"
        },
        {
            "ma": "04",
            "ten": "Tỉnh Cao Bằng",
            "cap": "Tỉnh"
        },
        {
            "ma": "06",
            "ten": "Tỉnh Bắc Kạn",
            "cap": "Tỉnh"
        },
        {
            "ma": "08",
            "ten": "Tỉnh Tuyên Quang",
            "cap": "Tỉnh"
        },
        {
            "ma": "10",
            "ten": "Tỉnh Lào Cai",
            "cap": "Tỉnh"
        },
        {
            "ma": "11",
            "ten": "Tỉnh Điện Biên",
            "cap": "Tỉnh"
        },
        {
            "ma": "12",
            "ten": "Tỉnh Lai Châu",
            "cap": "Tỉnh"
        },
        {
            "ma": "14",
            "ten": "Tỉnh Sơn La",
            "cap": "Tỉnh"
        },
        {
            "ma": "15",
            "ten": "Tỉnh Yên Bái",
            "cap": "Tỉnh"
        },
        {
            "ma": "17",
            "ten": "Tỉnh Hoà Bình",
            "cap": "Tỉnh"
        },
        {
            "ma": "19",
            "ten": "Tỉnh Thái Nguyên",
            "cap": "Tỉnh"
        },
        {
            "ma": "20",
            "ten": "Tỉnh Lạng Sơn",
            "cap": "Tỉnh"
        },
        {
            "ma": "22",
            "ten": "Tỉnh Quảng Ninh",
            "cap": "Tỉnh"
        },
        {
            "ma": "24",
            "ten": "Tỉnh Bắc Giang",
            "cap": "Tỉnh"
        },
        {
            "ma": "25",
            "ten": "Tỉnh Phú Thọ",
            "cap": "Tỉnh"
        },
        {
            "ma": "26",
            "ten": "Tỉnh Vĩnh Phúc",
            "cap": "Tỉnh"
        },
        {
            "ma": "27",
            "ten": "Tỉnh Bắc Ninh",
            "cap": "Tỉnh"
        },
        {
            "ma": "30",
            "ten": "Tỉnh Hải Dương",
            "cap": "Tỉnh"
        },
        {
            "ma": "31",
            "ten": "Thành phố Hải Phòng",
            "cap": "Thành phố Trung ương"
        },
        {
            "ma": "33",
            "ten": "Tỉnh Hưng Yên",
            "cap": "Tỉnh"
        },
        {
            "ma": "34",
            "ten": "Tỉnh Thái Bình",
            "cap": "Tỉnh"
        },
        {
            "ma": "35",
            "ten": "Tỉnh Hà Nam",
            "cap": "Tỉnh"
        },
        {
            "ma": "36",
            "ten": "Tỉnh Nam Định",
            "cap": "Tỉnh"
        },
        {
            "ma": "37",
            "ten": "Tỉnh Ninh Bình",
            "cap": "Tỉnh"
        },
        {
            "ma": "38",
            "ten": "Tỉnh Thanh Hóa",
            "cap": "Tỉnh"
        },
        {
            "ma": "40",
            "ten": "Tỉnh Nghệ An",
            "cap": "Tỉnh"
        },
        {
            "ma": "42",
            "ten": "Tỉnh Hà Tĩnh",
            "cap": "Tỉnh"
        },
        {
            "ma": "44",
            "ten": "Tỉnh Quảng Bình",
            "cap": "Tỉnh"
        },
        {
            "ma": "45",
            "ten": "Tỉnh Quảng Trị",
            "cap": "Tỉnh"
        },
        {
            "ma": "46",
            "ten": "Tỉnh Thừa Thiên Huế",
            "cap": "Tỉnh"
        },
        {
            "ma": "48",
            "ten": "Thành phố Đà Nẵng",
            "cap": "Thành phố Trung ương"
        },
        {
            "ma": "49",
            "ten": "Tỉnh Quảng Nam",
            "cap": "Tỉnh"
        },
        {
            "ma": "51",
            "ten": "Tỉnh Quảng Ngãi",
            "cap": "Tỉnh"
        },
        {
            "ma": "52",
            "ten": "Tỉnh Bình Định",
            "cap": "Tỉnh"
        },
        {
            "ma": "54",
            "ten": "Tỉnh Phú Yên",
            "cap": "Tỉnh"
        },
        {
            "ma": "56",
            "ten": "Tỉnh Khánh Hòa",
            "cap": "Tỉnh"
        },
        {
            "ma": "58",
            "ten": "Tỉnh Ninh Thuận",
            "cap": "Tỉnh"
        },
        {
            "ma": "60",
            "ten": "Tỉnh Bình Thuận",
            "cap": "Tỉnh"
        },
        {
            "ma": "62",
            "ten": "Tỉnh Kon Tum",
            "cap": "Tỉnh"
        },
        {
            "ma": "64",
            "ten": "Tỉnh Gia Lai",
            "cap": "Tỉnh"
        },
        {
            "ma": "66",
            "ten": "Tỉnh Đắk Lắk",
            "cap": "Tỉnh"
        },
        {
            "ma": "67",
            "ten": "Tỉnh Đắk Nông",
            "cap": "Tỉnh"
        },
        {
            "ma": "68",
            "ten": "Tỉnh Lâm Đồng",
            "cap": "Tỉnh"
        },
        {
            "ma": "70",
            "ten": "Tỉnh Bình Phước",
            "cap": "Tỉnh"
        },
        {
            "ma": "72",
            "ten": "Tỉnh Tây Ninh",
            "cap": "Tỉnh"
        },
        {
            "ma": "74",
            "ten": "Tỉnh Bình Dương",
            "cap": "Tỉnh"
        },
        {
            "ma": "75",
            "ten": "Tỉnh Đồng Nai",
            "cap": "Tỉnh"
        },
        {
            "ma": "77",
            "ten": "Tỉnh Bà Rịa - Vũng Tàu",
            "cap": "Tỉnh"
        },
        {
            "ma": "79",
            "ten": "Thành phố Hồ Chí Minh",
            "cap": "Thành phố Trung ương"
        },
        {
            "ma": "80",
            "ten": "Tỉnh Long An",
            "cap": "Tỉnh"
        },
        {
            "ma": "82",
            "ten": "Tỉnh Tiền Giang",
            "cap": "Tỉnh"
        },
        {
            "ma": "83",
            "ten": "Tỉnh Bến Tre",
            "cap": "Tỉnh"
        },
        {
            "ma": "84",
            "ten": "Tỉnh Trà Vinh",
            "cap": "Tỉnh"
        },
        {
            "ma": "86",
            "ten": "Tỉnh Vĩnh Long",
            "cap": "Tỉnh"
        },
        {
            "ma": "87",
            "ten": "Tỉnh Đồng Tháp",
            "cap": "Tỉnh"
        },
        {
            "ma": "89",
            "ten": "Tỉnh An Giang",
            "cap": "Tỉnh"
        },
        {
            "ma": "91",
            "ten": "Tỉnh Kiên Giang",
            "cap": "Tỉnh"
        },
        {
            "ma": "92",
            "ten": "Thành phố Cần Thơ",
            "cap": "Thành phố Trung ương"
        },
        {
            "ma": "93",
            "ten": "Tỉnh Hậu Giang",
            "cap": "Tỉnh"
        },
        {
            "ma": "94",
            "ten": "Tỉnh Sóc Trăng",
            "cap": "Tỉnh"
        },
        {
            "ma": "95",
            "ten": "Tỉnh Bạc Liêu",
            "cap": "Tỉnh"
        },
        {
            "ma": "96",
            "ten": "Tỉnh Cà Mau",
            "cap": "Tỉnh"
        }
    ]
