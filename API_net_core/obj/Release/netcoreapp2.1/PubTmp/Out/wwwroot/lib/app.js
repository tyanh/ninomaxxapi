﻿/// <reference path="../../wwwroot/lib/angular.js/angular.js" />
var app = angular.module("myapp", []);
app.controller("initupload", initupload);
initupload.$inject = ['$scope', 'myService'];
function initupload($scope, myService) {
    var infor = JSON.parse(localStorage.getItem('imgInfor'));
    if (infor == null) {
        localStorage.setItem('imgInfor', JSON.stringify({
            isWeb: true,
            isApp: true,
            iswatermark: true,
            widthweb: "0",
            heightweb: "0",
            widthapp: "0",
            heightapp: "0",
            pst: "0"
           
        }));
        infor = JSON.parse(localStorage.getItem('imgInfor'));
    }
    $scope.isWeb = infor.isWeb;
    $scope.isApp = infor.isApp;
    $scope.iswatermark = infor.iswatermark;
    $scope.widthweb = infor.widthweb;
    $scope.heightweb = infor.heightweb;
    $scope.widthapp = infor.widthapp;
    $scope.heightapp = infor.heightapp;
    $scope.pst = infor.pst;
    $scope.result = "";

    $scope.submitData = function () {
        localStorage.setItem('imgInfor', JSON.stringify({
            isWeb: $scope.isWeb,
            isApp: $scope.isApp,
            iswatermark: $scope.iswatermark,
            widthweb: $scope.widthweb,
            heightweb: $scope.heightweb,
            widthapp: $scope.widthapp,
            heightapp: $scope.heightapp,
            pst: $scope.pst
           
        }));

        var fileInput = document.getElementById('fileInput');
        if (fileInput.files.length === 0) return;
        var payload = new FormData();
        var file = [];
     
        for (var i = 0; i < fileInput.files.length;i++) {
            payload.append("files", fileInput.files[i]);
          //  file.push(fileInput.files[0])
        }
        payload.append("isWatermark", $scope.iswatermark);
        payload.append("pst", $scope.pst);
        payload.append("isweb", $scope.isWeb);
        payload.append("isapp", $scope.isApp);
        payload.append("wweb", $scope.widthweb);
        payload.append("hweb", $scope.heightweb);
        payload.append("wapp", $scope.widthapp);
        payload.append("happ", $scope.heightapp);
        var a = myService.uploadFile(payload);
        a.then(function (pl) { $scope.result = pl.data })
    }
}

