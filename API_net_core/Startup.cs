﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3;
using API_net_core.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace API_net_core
{
    public class Startup
    {
        public static string refresh_token = "Hu54ak2u24LZjq7NreWZ85ApKUU9hWCvDxvDrgUCN1uqX0dzXOXFKoMEBhMHZrbrIPGFfl-vDKn4iHk3qRnFO6A_0FVJybmfRF4fxhRdVJG8nYVEkSWg51hdFUBihoSm1SqvbDQD3ZvJnalkvPjn85oNKEF8dmeEVvDnYjlUDnq-xpCd2wDqmus3j8H1hwr-vfFW-o3-l1g4eVAPLigNEP2is9m_tFCxsC6lvItObszP9YJgVrdpj5re";
        readonly string AllowOrigin = "AllowOrigin";
        public static string zl_token = "SryhGgnZqHfvLov0g6BK4tvPBakeUvOP6XmOIeKYx54M6aXgcsJYHaGhRchkLiXnHovtGiL0usft61b5v6s7Mqe641pOSwqmPoOb7Crjc18SBoq_h1g5819EVn-H2uT0CtKlHhK1lMW6QoHIqp-8IWS3C4sDNPjH0I42JAHT-d45959irqFsKtmfSsplQTTaS54tRSG_WcrgImr2npUDP1TJ8Nl0RhPLQr4sUi4BbtqBI3bJZo6IVnvFENcD78n5MbieOjOxtLi3Odj1qW7xRdfCH52k9Sje2a1gCO4Or0iFUqOhlHd31mjPObYcElrATLLeOiOFdKTLHGDECESCpMgWVTKH";
        public static DateTime expxalo=DateTime.Now;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                         .SetBasePath(env.ContentRootPath)
                         .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                         .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddCors(options =>
            {
                options.AddPolicy(AllowOrigin,
                builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());
                
            });
            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromDays(1);
            });
            services.AddMvc();
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory(AllowOrigin));
            });
            string constring = Configuration["ConnectionStrings:connectdata"];
            services.AddDbContext<DataContext>(options => options.UseSqlServer(constring));
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAWSService<IAmazonS3>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
       
            app.UseSession();
            app.UseDeveloperExceptionPage();
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{

            //    app.UseHsts();
            //}
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors(AllowOrigin);
            app.UseMvc(routes =>
            {
 
                routes.MapRoute(
                name: "area",
                template: "{area:exists}/{controller=ToolUpPrism}/{action=Mainmenu}/{id?}");

            });
            app.UseDefaultFiles();
                 //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Home}/{action=Index}/{id?}");
            //});
            
        }
    }
}
