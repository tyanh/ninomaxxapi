﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class uphinhVnG
    {
        fileconfig cf = new fileconfig();
        string PostData(string url, dynamic data)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.ContentType = "application/json;";
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            var header = response.Headers;
            string Status_Code = header.Get("X-Subject-Token");
           
            return Status_Code;
        }
        string PutData(string url,string tokent)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Headers.Add("X-Auth-Token", tokent);
            myHttpWebRequest_.Method = "PUT";
           
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            var header = response.Headers;
            string Status_Code = response.StatusCode.ToString();

            return Status_Code;
        }
        public string DeleteDataImg(string url, string tokent)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "DELETE";
            request.ContentType = "application/octet-stream";
            request.Headers.Add("X-Auth-Token", tokent);
            request.Headers.Add("Content-Type", "image/jpeg");
            Stream dataStream = request.GetRequestStream();
            dataStream.Close();
            WebResponse response = request.GetResponse();


            return "";
        }
        public string PutDataImg(string url, string tokent, byte[] byteArray)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "PUT";
            
            request.ContentType = "application/octet-stream";
            request.ContentLength = byteArray.Length;
            request.Headers.Add("X-Auth-Token", tokent);
            request.Headers.Add("Content-Type", "image/jpeg");
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            
          
            return "";
        }
        public string createFolderVNG(string tk,string fd)
        {
            string link = cf.linkImgVNG+ fd;
            //var put= PutData(link, tk);
            return link;
        }
        public string deletefileVNG(string tk, string fd)
        {
            
            var put= deletefileVNG(tk, fd);
            return "0";
        }
        public string createobjVNG(string tk, string link)
        {
            var put = PutData(link, tk);
            return "";
        }
        public string UploadImgVNG(string url,Image img,string tk)
        {
            using (var ms = new MemoryStream())
            {
                
                img.Save(ms, ImageFormat.Jpeg);
                PutDataImg(url, tk, ms.ToArray());
            }
         
            return "";
        }
        public string tokentVNG() {
            string link = cf.tokentVNG;
            var obj = new
            {
                auth = new
                {
                    identity = new
                    {
                        methods = new[]{
                                "password"
                            },
                        password = new
                        {
                            user = new
                            {
                                domain = new
                                {
                                    name= "default"
                                },
                                name="nguyennhan.ninomaxx@gmail.com",
                                password="wn*pbP8p"
                            }
                        }
                    },
                    scope = new
                    {
                        project = new
                        {
                            domain = new
                            {
                                name = "default"
                            },
                            id = "f34d13925ff54f6ca78e503226f4bd37"
                        }
                        
                    }
                }
            };
            var p=PostData(link, obj);
            return p;
        }
    }
}
