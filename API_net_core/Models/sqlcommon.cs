﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class sqlcommon
    {
        fileconfig cf = new fileconfig();
        // string cnn = "server=nino.com.vn;database=CRM_DataExchange;uid=vfcprism;password=@123456;";
        string cnn = "";
        string cnnCrmlive = "";
        public sqlcommon()
        {
            cnn = cf.sqlCrm;
            cnnCrmlive = cf.sqlcrm2;
        }
        public string gettempleataSms(string id)
        {
            string str = "SELECT top 1 Content FROM CRM_SMSTemplates where TemplateSMSID=" + id+"";

            string results ="";
            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnnCrmlive);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            
            while (reader.Read())
            {
                results=reader.GetString(0);
            }
            con.Close();

            return results;
        }
        public string insertsqlstr(string table, List<string> cl, List<string> vl)
        {
            string str = "INSERT INTO";
            str += " " + table + "(";
            str += string.Join(",", cl);
            str += ") values(";
            for (int i = 0; i < vl.Count; i++)
            {
                if (vl[i] == "")
                {
                    vl[i] = "null";
                }
                else if (vl[i].ToLower() == "true" || vl[i].ToLower() == "false")
                {
                    vl[i] = vl[i];
                }
                else
                {
                    try
                    {
                        int.Parse(vl[i]);
                    }
                    catch
                    {

                        vl[i] = "N'" + vl[i] + "'";
                    }
                }
            }
            str += string.Join(",", vl) + ")";
            return str;
        }
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, SqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public List<Dictionary<string, object>> getdataSQL(string tb, string where)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + where;
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public List<Dictionary<string, object>> getdataSQLlive(string tb, string where)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnnCrmlive);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + where;
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public bool runSQL(string str)
        {
            SqlConnection conn = new SqlConnection(cnn);
            conn.Open();
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.ExecuteNonQuery();
            // Execute the command
            conn.Close();
            return true;
        }
    }
}
