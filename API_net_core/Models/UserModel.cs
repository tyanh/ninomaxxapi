﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class UserModel
    {
        public string name { get; set; }
        public List<IFormFile> files { get; set; }
    }
}
