﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class RestFile
    {
        fileconfig cf = new fileconfig();
        string urlPrism = "";
        string urlCrm = "";
        string urlVfc = "";
        public RestFile()
        {
             urlPrism = cf.prism;
             urlCrm = cf.crm;
            urlVfc = cf.urlvfc;
        }
        public string GetRowvwersioill(string Auth_Session, string sid)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string uurl = urlPrism + "v1/rest/document?filter=sid,eq,"+ sid + "&cols=row_version";
            var obj = webClient.DownloadString(uurl);
            var inven = JsonConvert.DeserializeObject<dynamic>(obj);
            return inven[0].row_version.ToString();
        }
        public string GetTokentcamera()
        {
            fileconfig cf = new fileconfig();
            WebClient webClient = new WebClient { Credentials = new NetworkCredential(cf.usercamera,cf.passcamera) };
            webClient.Headers.Add("Authorization", cf.tokentcmr);
            string uurl = cf.apicmr+ "/access_token";
            var obj = webClient.DownloadString(uurl);
            var tk=JsonConvert.DeserializeObject<dynamic>(obj)["data"]["token"].ToString()
;            return tk;
        }
        public string GetDatacameraL(string f,string t,string cmr,string tken)
        {
            fileconfig cf = new fileconfig();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("X-Api-Key", tken);
            string uurl = cf.apicmr + "/face/results?from="+f+"&to="+t+"&uuids="+cmr;
            var obj = webClient.DownloadString(uurl);
            var data = JsonConvert.DeserializeObject<dynamic>(obj)["data"];
            return JsonConvert.SerializeObject(data);
            
        }
        public string GetDataWebClient(string Auth_Session,string url)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string uurl = urlPrism + url;
            var obj = webClient.DownloadString(uurl);
            
            return obj;
        }
        public string GetDataWebClientVfc(string Auth_Session, string url)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization", "Bearer " + Auth_Session);
            string uurl = urlVfc + url;
            var obj = webClient.DownloadString(uurl);

            return obj;
        }
        public dynamic GetDataWeb(string url)
        {
            WebClient webClient = new WebClient();
            var obj = webClient.DownloadString(url);
            return obj;
        }
        public dynamic GetDataWebClientAndCout(string Auth_Session, string url)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            string uurl = urlPrism + url;
            var obj = webClient.DownloadString(uurl);
            var head = webClient.ResponseHeaders;
            var vl = head.Get("ContentRange").ToString();
            dynamic b = new System.Dynamic.ExpandoObject();
            b.count = vl;
            b.data = obj;
            return b;
        }

        public string GetData(string key_login, string url)
        {
            HttpWebRequest request = WebRequest.Create(urlPrism+url) as HttpWebRequest;
            request.Method = "GET";
            request.Headers.Add("Auth-Session: " + key_login);
            request.Accept = "application /json, text/plain, version=2";
            request.ContentType = "application/json; charset=UTF-8";
            request.ProtocolVersion = System.Net.HttpVersion.Version11;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(myHttpWebResponse.GetResponseStream());
            string str = reader.ReadLine();
            var repon = JsonConvert.DeserializeObject<dynamic>(str);
            var data = repon.data;
            myHttpWebResponse.Close();
            return JsonConvert.SerializeObject(data);
        }
        public string PostDataVFCApi(string url, dynamic data, string token)
        {
            string urlpost = cf.urlvfc + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            string toe = "Bearer " + token;
            myHttpWebRequest_.Headers.Add("Authorization", "Bearer "+ token);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
        public string LoginVFCApi()
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(cf.urlvfc + "nguoi-dung/dang-nhap/");
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            var body = new
            {
                username = cf.uservfc,
                password = cf.passvfc,
            };
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(JsonConvert.SerializeObject(body));
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
        public string PostDataCrm(string url,dynamic data,string token)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlCrm + url);
            myHttpWebRequest_.Headers.Add("Authorization", token);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            string body = JsonConvert.SerializeObject(data);
             using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
           
            return str;
        }
        public string PostDatav_22(string key_login, string url, string item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(item);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
        public string PutDatav_22(string key_login, string url, string item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
           
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(item);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
        public string PostDataVietGuid(string url, dynamic data)
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
        }
        public string PostListItemPrism(string key_login, string url,List<dynamic> item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            
            string jsonstr = JsonConvert.SerializeObject(item);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.StatusCode.ToString();

            return "ok";

        }
        public string PostItemPrism_ctm(string key_login, string url, string item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();


            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(item);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;


        }
        public string PostItemPrism_v2(string key_login, string url, dynamic item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = item;
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;
      

        }
        public string PostItemPrism(string key_login, string url, dynamic item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = item;
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.StatusCode.ToString();

            return "ok";

        }
        public string PostItemPrismcheckin_out(string key_login, string url, dynamic item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = item;
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.LastModified.ToString();

            return returnString;

        }
        public void Serialize(Stream output, object input)
        {
            var ser = new DataContractSerializer(input.GetType());
            ser.WriteObject(output, input);
        }

        public string updateItemcoupon(string key_login, string url, string payload)
        {


            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
        
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(payload);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.StatusCode.ToString();

            return "ok";

        }
        public string updateItem(string key_login, string url, dynamic item)
        {
            var urls = urlPrism + url;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = item;
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
           
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.StatusCode.ToString();

            return "ok";
         
        }

    }
}
