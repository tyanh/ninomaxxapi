﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3;
namespace API_net_core.Models
{
    public class HandleImage
    {

        string _pathWeb = "";
        string _pathApp = "";
        IHostingEnvironment _host=new HostingEnvironment();
        public HandleImage(string _pWeb,string _pApp)
        {
            _pathWeb = _pWeb;
            _pathApp = _pApp;

        }
        private Image CropImage(Image sourceImage, int sourceX, int sourceY, int sourceWidth, int sourceHeight, int destinationWidth, int destinationHeight)
        {
            Image destinationImage = new Bitmap(destinationWidth, destinationHeight);
            Graphics g = Graphics.FromImage(destinationImage);

            g.DrawImage(
              sourceImage,
              new Rectangle(0, 0, destinationWidth, destinationHeight),
              new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
              GraphicsUnit.Pixel
            );

            return destinationImage;
        }
        public void watermarkImgonImage(IFormFile file, int wapp, int happ, int wweb,int hweb,int pst, bool wtm, bool web, bool app)
        {
            string watermarkText = "NinoMaxx";
            string p = Path.GetFullPath("wwwroot/Images/watermark.png");
            Image wtmimg = new Bitmap("E:\\gitproject\\API_net_core\\API_net_core\\wwwroot\\Images\\watermark.png");
            var bitmap = Image.FromStream(file.OpenReadStream(), true, true);
            var bitmap2= Image.FromStream(file.OpenReadStream(), true, true);
            AmazonUtil amz = new AmazonUtil();
    
            List<Image> L_img = new List<Image>();
            L_img.Add(bitmap2);
            int wimg = bitmap.Width;
            int himg = bitmap.Height;
            using (bitmap)
            {
              
                using (var tempBitmap = bitmap)
                {
             
                    using (Graphics grp = Graphics.FromImage(tempBitmap))
                    {
                        //grp.DrawImage(bitmap, 0, 0);
                        //bitmap.Dispose();
                        Brush brush = new SolidBrush(Color.FromArgb(120, 255, 0, 0));
                        Font font = new System.Drawing.Font("Segoe UI", 30, FontStyle.Bold, GraphicsUnit.Pixel);
                        SizeF textSize = grp.MeasureString(watermarkText, font);
                        Point position = positionwtm(pst, wimg, himg);
                        if (wtm)
                        {
                            grp.DrawString(watermarkText, font, brush,position);
                            //grp.DrawImage(wtmimg, position);
                        }
                        if (web)
                        {
                            if (wweb == 0)
                                wweb = wimg;
                            if (hweb == 0)
                                hweb = himg;
                            var objbitmap = new Bitmap(tempBitmap, wweb,hweb);
                            L_img.Add(objbitmap);
                            //objbitmap.Save(_pathWeb + "\\" + file.FileName);
                        }
                        if (app)
                        {
                            if (wapp == 0)
                                wapp = wimg;
                            if (happ == 0)
                                happ = himg;
                            var objbitmap = new Bitmap(tempBitmap, wapp, happ);
                            L_img.Add(objbitmap);
                            //objbitmap.Save(_pathApp + "\\" + file.FileName);
                        }
                    }
                }
            }
             amz.UploadFileAsyncCustom(L_img, file.FileName);
            //return;
        }
        private Point positionwtm(int pst, int w, int h)
        {
            Point position = new Point(0, 0);
            if (pst == 1)
                position = new Point(w-150, 0);
            else if(pst==2)
                position = new Point(0, h/2);
            else if (pst == 3)
                position = new Point(w - 150, h / 2);
            else if (pst == 4)
                position = new Point(0, h-50);
            else if (pst == 5)
                position = new Point(w - 150, h - 50);
            return position;
        }
        public Image watermarkStronImage(string txt,Bitmap bm, string FolderSource_, string fname)
        {
            string watermarkText = txt;
            int l = watermarkText.Length;
            string path = @"C:\GitProject\testimg\";
            AmazonUtil amz = new AmazonUtil();
           
            Image file = (Image)bm;
            using (Graphics grp = Graphics.FromImage(file))
            {
                grp.TranslateTransform(file.Width / 2, file.Height / 2);
                grp.RotateTransform(270);
                int x = ((file.Height / 2)) * -1;
                int y = (file.Width / 2)-40;
                int size = 16;
                if (file.Width <= 800)
                {
                    y = y + 10;
                }
                else
                {
                    
                    size = 24;
                }
                    Brush brush = new SolidBrush(Color.Gray);
                Font font = new Font("UTM Dax", size, GraphicsUnit.Point);
                //SizeF textSize = grp.MeasureString(watermarkText, font);
               
                Point position = new Point(x, y);
                grp.DrawString(watermarkText, font, brush, position);
                var paths = Path.Combine(path, "", fname);
                return file;
                //var stream = new System.IO.MemoryStream();
                //file.Save(stream, ImageFormat.Jpeg);
                //stream.Position = 0;
                //await stream.CopyToAsync(new FileStream(paths, FileMode.Create));

            }
         
        }

        public void watermarkImg(IFormFile file) {
            string watermarkText = "NinoMaxx";
           
            using (var bitmap = Image.FromStream(file.OpenReadStream(), true, true))
            {
                using (var tempBitmap = new Bitmap(bitmap.Width, bitmap.Height))
                {
                    using (Graphics grp = Graphics.FromImage(tempBitmap))
                    {
                        grp.DrawImage(bitmap, 0, 0);
                        bitmap.Dispose();
                        Brush brush = new SolidBrush(Color.FromArgb(120, 255, 0, 0));
                        Font font = new System.Drawing.Font("Segoe UI", 30, FontStyle.Bold, GraphicsUnit.Pixel);
                        SizeF textSize = grp.MeasureString(watermarkText, font);
                        Point position = new Point((tempBitmap.Width - ((int)textSize.Width + 10)),
                            (tempBitmap.Height - ((int)textSize.Height + 10)));
                        grp.DrawString(watermarkText, font, brush, position);
                        tempBitmap.Save(_pathApp+"\\"+ file.FileName);
                    }
                }
            }
        }
    }
}
