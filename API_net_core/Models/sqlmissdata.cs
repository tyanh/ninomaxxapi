﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class sqlmissdata
    {
        string cnn = "server=61.28.229.145;database=missingwoo;user id=sa; pwd=P@SSw0rd*();persistsecurityinfo = True";
        public string insertsqlstr(string table, List<string> cl, List<string> vl)
        {
            string str = "INSERT INTO";
            str += " " + table + "(";
            str += string.Join(",", cl);
            str += ") values(";
            for (int i = 0; i < vl.Count; i++)
            {
                if (vl[i] == "" || vl[i]==null)
                {
                    vl[i] = "null";
                }
                else if (vl[i].ToLower() == "true" || vl[i].ToLower() == "false")
                {
                    vl[i] = vl[i];
                }
                else
                {
                    try
                    {
                        int.Parse(vl[i]);
                    }
                    catch
                    {

                        vl[i] = "N'" + vl[i] + "'";
                    }
                }
            }
            str += string.Join(",", vl) + ")";
            return str;
        }
        public string checkduplicateCart(string idwoo)
        {
            string str = "select top 1 * from missdata where source is not null AND idwoo='" + idwoo+"'";

            int results = -1;
            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();

            while (reader.Read())
            {
                results = 0;
            }
            con.Close();

            return results.ToString();
        }
        public string checkduplicate(string idwoo)
        {
            string str = "select top 1 * from missdata where idwoo=" + idwoo + " AND sid is not null ";
           
            int results = -1;
            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();

            while (reader.Read())
            {
                results =0;
            }
            con.Close();

            return results.ToString();
        }
        public bool runSQL(string str)
        {
            SqlConnection conn = new SqlConnection(cnn);
            conn.Open();
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.ExecuteNonQuery();
            // Execute the command
            conn.Close();
            return true;
        }
    }
}
