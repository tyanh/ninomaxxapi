﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
namespace API_net_core.Models
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public virtual DbSet<hoadon> hoadons { get; set; }
        public virtual DbSet<nguoidung> nguoidungs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<hoadon>().ToTable("hoadon");
            modelBuilder.Entity<nguoidung>().ToTable("nguoidungs");
        }
    }
}
