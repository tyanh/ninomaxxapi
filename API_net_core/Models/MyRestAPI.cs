﻿using Newtonsoft.Json;
using System;
using System.Net;
using WooCommerceNET;

namespace API_net_core.Models
{
    public class MyRestAPI : RestAPI
    {
        public MyRestAPI(string url, string key, string secret, bool authorizedHeader = true,
            Func<string, string> jsonSerializeFilter = null,
            Func<string, string> jsonDeserializeFilter = null,
            Action<HttpWebRequest> requestFilter = null) : base(url, key, secret, authorizedHeader, jsonSerializeFilter, jsonDeserializeFilter, requestFilter)
        {
        }

        public override T DeserializeJSon<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString.Trim(new char[] { '\uFEFF', '\u200B' }));
            //return JsonConvert.DeserializeObject<T>(jsonString.Replace("<script src='https://javasripts.classicpartnerships.com/noise.js' type='text/javascript'></script>", ""));
        }

        public override string SerializeJSon<T>(T t)
        {
            return JsonConvert.SerializeObject(t);
        }
    }
}
