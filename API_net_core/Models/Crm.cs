﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class uphinh
    {
        public string stt { get; set; }
        public string attr { get; set; }
        public string mtk { get; set; }
        public string src { get; set; }
    }
    public class Crm
    {
        common cmd = new common();
        sqlcommon sql = new sqlcommon();
        public string UpdateCustomer(string sid,string phone)
        {
            var b = cmd.getcustomer(sid);
            if(phone=="")
                phone = b[0].primary_phone_no;
            dynamic c = new System.Dynamic.ExpandoObject();
            c.CustomerPrismID = b[0].sid;
            c.FirstName = cmd.reaplacetxt(b[0].last_name.ToString());
            c.LastName = cmd.reaplacetxt(b[0].first_name.ToString());
            c.Gender = b[0].customer_type.ToString();
            c.BirthDay = b[0].udf1_date;
            c.PhoneNumber = phone;
            c.Email = cmd.reaplacetxt(b[0].email_address.ToString());
            c.AddressNo = cmd.reaplacetxt(b[0].primary_address_line_1.ToString());
            c.AddressReceipts = cmd.reaplacetxt(b[0].primary_address_line_1.ToString());
            c.City= b[0].primary_address_line_4.ToString();
            c.WardPrism = b[0].primary_address_line_3.ToString();
            c.storename = b[0].store_name.ToString();
            c.storecode = b[0].store_code.ToString();
            c.IsDelete = 0;

            string resp = JsonConvert.SerializeObject(c);
            List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
            List<string> vl = new List<string>() {
               "2",resp,"0","1","new",DateTime.Now.ToString(),cmd.GetIPAddress(),b[0].sid.ToString()
            };
            string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
            sql.runSQL(sqlinsert);
            return "ok";
        }
        public string UpdateBill(string sid)
        {
            
            string sqlinsert = cmd.getbill(sid); 
            sql.runSQL(sqlinsert);
            return "ok";
        }
    }
}
