﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class storePrism
    {

        string _oraString = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=61.28.229.251)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=RPROODS)));"
                    + "User Id=reportuser;Password=report;";

        public OracleConnection GetConnection()
        {
            var _conn = new OracleConnection(_oraString);
            _conn.Open();
            return _conn;
        }
        public DataTable GET_COUPON_LIST_FOR_CRM()
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_LIST_FOR_CRM";
            command.CommandType = CommandType.StoredProcedure;

            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public DataTable GET_COUPON_INFO(string code)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_INFO";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter P_COUPON_CODE = command.Parameters.Add("P_COUPON_CODE", OracleDbType.Varchar2);
            P_COUPON_CODE.Value = code;

            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public DataTable GET_COUPON_BY_INVOICE(string sid)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_BY_INVOICE";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_DOC_SID = command.Parameters.Add("p_DOC_SID", OracleDbType.Int32);
            p_DOC_SID.Value = sid;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public DataTable GET_COUPON_CODE_FOR_CRM(int qlt,int idset)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_CODE_FOR_CRM";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_NumberOfCode = command.Parameters.Add("p_NumberOfCode", OracleDbType.Int32);
            p_NumberOfCode.Value = qlt;
            OracleParameter p_CouponSetId = command.Parameters.Add("p_CouponSetId", OracleDbType.Int32);
            p_CouponSetId.Value = idset;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public DataTable InventorieLiveall(string sku)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();
  
            command.CommandText = "REPORTUSER.VFC_PRISM_INVENTORY.LOOKUP_OH_QTY_DESC1_SOL_V2";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter v_Descr1 = command.Parameters.Add("v_Descr1", OracleDbType.Varchar2);
            v_Descr1.Value = sku;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public DataTable Getcoupon(int qlt)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_FOR_SN_CRM";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_NumberOfCode = command.Parameters.Add("p_NumberOfCode", OracleDbType.Int32);
            p_NumberOfCode.Value = qlt;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public DataTable GET_COUPON_CELEB(string CELEB_CODE)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_CELEB";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_CELEB_CODE = command.Parameters.Add("p_CELEB_CODE", OracleDbType.Varchar2);
            p_CELEB_CODE.Value = CELEB_CODE;

            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;

        }
        public DataTable GET_COUPON_VONGXOAY(string TYPE)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_COUPON.GET_COUPON_VONGXOAY";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_VX_TYPE = command.Parameters.Add("p_CUST_PHONE ", OracleDbType.Varchar2);
            p_VX_TYPE.Value = TYPE;

            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
            
        }
        public string INSERT_NEW_RECORD_VX_FB(string MESSENGER_ID, string FACEBOOK, string PHONE, string COUPON, string PRICE_DETAIL, string ADDRESS, string CREATED_AT, string COUPON_GET_AT)
        {

            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.PKG_CTKM.INSERT_NEW_RECORD_VX_FB";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_MESSENGER_ID = command.Parameters.Add("p_MESSENGER_ID ", OracleDbType.Varchar2);
            p_MESSENGER_ID.Value = MESSENGER_ID;
            OracleParameter p_FACEBOOK = command.Parameters.Add("p_FACEBOOK ", OracleDbType.Varchar2);
            p_FACEBOOK.Value = FACEBOOK;
            OracleParameter p_PHONE = command.Parameters.Add("p_PHONE ", OracleDbType.Varchar2);
            p_PHONE.Value = PHONE;
            OracleParameter p_COUPON = command.Parameters.Add("p_COUPON ", OracleDbType.Varchar2);
            p_COUPON.Value = COUPON;
            OracleParameter p_PRICE_DETAIL = command.Parameters.Add("p_PRICE_DETAIL ", OracleDbType.Varchar2);
            p_PRICE_DETAIL.Value = PRICE_DETAIL;
            OracleParameter p_ADDRESS = command.Parameters.Add("p_ADDRESS ", OracleDbType.Varchar2);
            p_ADDRESS.Value = ADDRESS;
            OracleParameter p_CREATED_AT = command.Parameters.Add("p_CREATED_AT ", OracleDbType.Varchar2);
            p_CREATED_AT.Value = CREATED_AT;
            OracleParameter p_COUPON_GET_AT = command.Parameters.Add("p_COUPON_GET_AT ", OracleDbType.Varchar2);
            p_COUPON_GET_AT.Value = COUPON_GET_AT;

            command.Parameters.Add("p_RESULT", OracleDbType.Varchar2, 2000, OracleCollectionType.None, ParameterDirection.Output);
            command.ExecuteNonQuery();
            string retunvalue = command.Parameters["p_RESULT"].Value.ToString();
            command.Connection.Close();
            return retunvalue;
        }
        public string CHECK_CUSTOMER_BUYING(string phone)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_CUSTOMER.CHECK_CUSTOMER_BUYING";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_CUST_PHONE = command.Parameters.Add("p_CUST_PHONE ", OracleDbType.Varchar2);
            p_CUST_PHONE.Value = phone;

            command.Parameters.Add("p_RESULT", OracleDbType.Varchar2, 2000, OracleCollectionType.None, ParameterDirection.Output);
            command.ExecuteNonQuery();
            string retunvalue = command.Parameters["p_RESULT"].Value.ToString();
            command.Connection.Close();
            return retunvalue;
        }
        public DataTable InventorieLive_v2(string upc)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_INVENTORY.LOOKUP_OH_QTY_UPC_SOL_V2";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter V_UPC = command.Parameters.Add("V_UPC", OracleDbType.Varchar2);
            V_UPC.Value = upc;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;
        }
        public string AddInventorieLive(string upc,string numberx)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_SOL_ONHAND_QTY.UPDATE_QTY_BY_UPC_ADD";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter p_UPC = command.Parameters.Add("p_UPC", OracleDbType.Varchar2);
            p_UPC.Value = upc;
            OracleParameter p_QTY = command.Parameters.Add("p_QTY", OracleDbType.Int32);
            p_QTY.Value =Int32.Parse(numberx);
            OracleParameter p_RESULT = command.Parameters.Add("p_RESULT", OracleDbType.Varchar2, 32767);
            p_RESULT.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            command.ExecuteNonQuery();
            //da.Fill(dt);
            command.Connection.Close();
            return "ok";


        }
        public DataTable GetInventorieLiveMtk(string mtk)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_SOL_ONHAND_QTY.LOOKUP_OH_QTY_DESC1_SOL_V2";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter v_Descr1 = command.Parameters.Add("v_Descr1", OracleDbType.Varchar2);
            v_Descr1.Value = mtk;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;


        }
        public DataTable GetInventorieLive(string upc)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_SOL_ONHAND_QTY.GET_SUM_QTY_BY_UPC";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter V_UPC = command.Parameters.Add("V_UPC", OracleDbType.Varchar2);
            V_UPC.Value = upc;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;


        }
        public DataTable GetInventorie(string upc)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_INVENTORY.LOOKUP_ONHAND_QTY_UPC";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter V_UPC = command.Parameters.Add("V_UPC", OracleDbType.Varchar2);
            V_UPC.Value = upc;
            OracleParameter V_STORENO = command.Parameters.Add("V_STORENO", OracleDbType.Varchar2);
            V_STORENO.Value = DBNull.Value;
            //V_STORENO.Value = 238;
            OracleParameter V_WHERE = command.Parameters.Add("V_WHERE", OracleDbType.Varchar2);
            V_WHERE.Value = DBNull.Value;
            //V_WHERE.Value = "STORE";
            OracleParameter V_EXACTLY = command.Parameters.Add("V_EXACTLY", OracleDbType.Boolean);
            V_EXACTLY.Value = DBNull.Value;
            //V_EXACTLY.Value = true;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;


            OracleDataAdapter da = new OracleDataAdapter(command);
            //da.SelectCommand = command;
            DataTable dt = new DataTable();
            //dt.Load(command.ExecuteReader());
            da.Fill(dt);
            command.Connection.Close();
            return dt;


        }
        public DataTable GetReport(string _fromDate, string _toDate)
        {
                 OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.VFC_PRISM_INVOICE.LOOKUP_REVENUE_DATE_RANGE";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_FromDate = command.Parameters.Add("v_FromDate", OracleDbType.Varchar2);
                v_FromDate.Value = _toDate;
                OracleParameter v_ToDate = command.Parameters.Add("v_ToDate", OracleDbType.Varchar2);
                v_ToDate.Value = _toDate;
               
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
          

        }
        public DataTable GetSales(string _fromDate, string _toDate, string _storeNo)
        {
            try
            {
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.NN_Invoice.spud_GET_BC_BanHang";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_FromDate = command.Parameters.Add("v_FromDate", OracleDbType.Varchar2);
                v_FromDate.Value = _fromDate;
                OracleParameter v_ToDate = command.Parameters.Add("v_ToDate", OracleDbType.Varchar2);
                v_ToDate.Value = _toDate;
                OracleParameter v_StoreCode = command.Parameters.Add("v_StoreNo", OracleDbType.Int32);
                v_StoreCode.Value = _storeNo;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public DataTable GetReport(string _fromDate, string _toDate, string _storeNo)
        {
            try
            {
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.NN_Invoice.spud_Run_NXT";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_FromDate = command.Parameters.Add("v_FromDate", OracleDbType.Varchar2);
                v_FromDate.Value = _fromDate;
                OracleParameter v_ToDate = command.Parameters.Add("v_ToDate", OracleDbType.Varchar2);
                v_ToDate.Value = _toDate;
                OracleParameter v_StoreCode = command.Parameters.Add("v_StoreNo", OracleDbType.Int32);
                v_StoreCode.Value = _storeNo;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public DataTable Ton_Kho_UPC(string upc, string storeNo, string where)
        {
            try
            {
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.NN_Invoice.spud_GET_TON_KHO_UPC";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_UPC = command.Parameters.Add("v_UPC", OracleDbType.Varchar2);
                v_UPC.Value = upc;
                OracleParameter v_StoreNo = command.Parameters.Add("v_StoreNo", OracleDbType.Varchar2);
                v_StoreNo.Value = storeNo;
                OracleParameter v_Where = command.Parameters.Add("v_Where", OracleDbType.Varchar2);
                v_Where.Value = where;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, OracleDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public List<Dictionary<string, object>> getdataSQL(string str)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();

            OracleConnection con = new OracleConnection(_oraString);
            con.Open();
            OracleCommand sqlCommand = new OracleCommand(str, con);
            OracleDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public List<Dictionary<string, object>> CreateAccout()
        {
            var store = getdataSQL("select STORE_NO,STORE_NAME from VFC_CUAHANG_V");
            return store;
        }
     

    }
}