﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WooCommerceNET;
using WooCommerceNET.Base;
using WooCommerceNET.WooCommerce.v2;

namespace API_net_core.Models
{
    public class wooCart
    {
        public List<dynamic> city = new List<dynamic>();
        public List<dynamic> distr = new List<dynamic>();
        public List<dynamic> phuong_xa = new List<dynamic>();
        fileconfig cf = new fileconfig();
        string trimstr(string json)
        {
            return json.Trim(new char[] { '\uFEFF', '\u200B' });
        }
        static WCObject wc;
        
        public wooCart()
        {
             RestAPI rest = new MyRestAPI(cf.linkcart, cf.keycartck, cf.keycartcs);
             wc = new WCObject(rest);
        }
        public void getaddress()
        {

            city = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot\tinh_thanh.txt")));
            distr = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot/quan_huyen.txt")));
            phuong_xa = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot/phuong_xa.txt")));
        }
        public async Task<Coupon> GetCoupon(string code)
        {
            var s = await wc.Coupon.GetAll(new Dictionary<string, string>() { { "code", code } });
            if (s.Count > 0)
                return s[0];
            else
                return null;
        }
        public async Task<Product> GetProduct(int id)
        {
            var s = await wc.Product.Get(id);
            return s;
        }
        public async Task<Product> GetProductsku(int id,string sku)
        {
            var s = await wc.Product.GetAll(new Dictionary<string, string>() { { "sku", sku } });
            return s[0];
        }
        public async Task<Order> getorder(string id)
        {
            var a = await wc.Order.Get(id);

            return a;
        }
      
        public async Task<string> CreateCoupon(DateTime start, DateTime end, string maximum_amount, string minimum_amount, string discount_type, string description, int amount, string code, double max_dc)
        {
       
            DateTime now = DateTime.Now;
            List<CouponMeta> metas = new List<CouponMeta>();
            CouponMeta meta = new CouponMeta();
            meta.key = "_max_discount";
            meta.value = max_dc;
            metas.Add(meta);
            Coupon cp = new Coupon();
            cp.amount = amount;
            cp.code = code;
            cp.meta_data = metas;
            cp.date_created = start;
            cp.date_expires = end;
            cp.description = description;
            cp.discount_type = discount_type;
            cp.usage_limit = 1;
            cp.minimum_amount = minimum_amount;
            cp.maximum_amount = maximum_amount;

            var s = await wc.Coupon.Add(cp);
            return "0";
        }
    }
}
