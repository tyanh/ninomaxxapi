﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class AmazonUtil
    {
        private string[] bucketName =new string[]{ "sourceroot", "webninomaxx", "appninomax" };
        private const string bucketName1 = "appninomax";
        private const string bucketName2 = "sourceroot";
        private const string bucketName3 = "webninomaxx";
        private const string keyName = "test";
        private const string filePath = "E:\\gitproject\\NetCore\\Images\\Web\\demo.jpg";
        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.APSoutheast1;
        private static IAmazonS3 s3Client;
        private static String accessKey = "AKIAIXSGGK4OYGN7FIJQ";
        private static String accessSecret = "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";
        public AmazonUtil()
        {
            s3Client= new AmazonS3Client(accessKey, accessSecret, bucketRegion);
            //s3Client = new AmazonS3Client(bucketRegion);
           // UploadFileAsync().Wait();
        }
        public void UploadFileAsyncCustom(List<Image> limg,string name)
        {
            //try
            //{
                for (var i = 0; i < 3; i++)
                {
                    var stream = new System.IO.MemoryStream();
                    Image img = limg[i];
                    img.Save(stream, ImageFormat.Bmp);
                    stream.Position = 0;

                    PutObjectRequest request = new PutObjectRequest();
                    request.InputStream = stream;
                    request.BucketName = bucketName[i];
                    request.CannedACL = S3CannedACL.PublicRead;
                    request.Key = name;
                    var response = s3Client.PutObjectAsync(request);
                    string a = response.Status.ToString();
                }
                
            //}
            //catch (AmazonS3Exception e)
            //{
            //    Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            //}

        }
        public async Task UploadFileAsync()
        {
            try
            {
                         
                var fileTransferUtility =new TransferUtility(s3Client);

                // Option 1. Upload a file. The file name is used as the object key name.
                await fileTransferUtility.UploadAsync(filePath, bucketName1);
                Console.WriteLine("Upload 1 completed");

                // Option 2. Specify object key name explicitly.
                await fileTransferUtility.UploadAsync(filePath, bucketName1, keyName);
                Console.WriteLine("Upload 2 completed");

                // Option 3. Upload data from a type of System.IO.Stream.
                using (var fileToUpload =new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    await fileTransferUtility.UploadAsync(fileToUpload,
                                               bucketName1, keyName);
                }
                Console.WriteLine("Upload 3 completed");

                // Option 4. Specify advanced settings.
                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = bucketName1,
                    FilePath = filePath,
                    StorageClass = S3StorageClass.StandardInfrequentAccess,
                    PartSize = 6291456, // 6 MB.
                    Key = keyName,
                    CannedACL = S3CannedACL.PublicRead
                };
                fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
                fileTransferUtilityRequest.Metadata.Add("param2", "Value2");

                await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                Console.WriteLine("Upload 4 completed");
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }

        }
    }
}
