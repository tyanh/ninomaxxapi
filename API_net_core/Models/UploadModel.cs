﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class UploadModel
    {
        public bool isWatermark { get; set; }
        public int pst { get; set; }
        public bool isweb { get; set; }
        public bool isapp { get; set; }
        public int wweb { get; set; }
        public int hweb { get; set; }
        public int wapp { get; set; }
        public int happ { get; set; }
        public List<IFormFile> files { get; set; }
    }

}
