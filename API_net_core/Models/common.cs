﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;

namespace API_net_core.Models
{
    public class common
    {
        
       
        string tokenCrm = "basic TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=";
        string UserNameCrm = "admin";
        string PasswordCrm = "123@qaz";
        public string cnn_web = "server=106.10.41.20;database=mesproj;Charset=utf8;user id=mesuser; pwd=mesuser!123;persistsecurityinfo = True";
        string urlPrism = "";
        string urlCrm = "";
        public string userPrism = "";
        public string passPrism = "";
        fileconfig cf = new fileconfig();
        public string dateAsString(DateTime Date)
        {
            var hour = Date.AddHours(-7);
            return hour.ToString("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
        public string dateAsStringd_m_y(string d)
        {
            string[] ld = d.Split('/');
            DateTime Date = DateTime.Parse(ld[1]+"/"+ ld[0]+"/"+ ld[2]);
            var hour = Date.AddHours(-7);
            return hour.ToString("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
        public string EnddateAsString(DateTime Date)
        {
            return Date.ToString("yyyy-MM-dd'T'16:59:59.SSS'Z'");
        }
        public string EnddateAsString_m_y(string d)
        {
            string[] ld = d.Split('/');
            DateTime Date = DateTime.Parse(ld[1] + "/" + ld[0] + "/" + ld[2]);
            return Date.ToString("yyyy-MM-dd'T'16:59:59.SSS'Z'");
        }
        public string ctmCrmExit(string idctm)
        {

            string tokenCrm = "basic TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=";
            RestFile rest = new RestFile();
            dynamic bodys = new
            {
                UserName = "admin",
                Password = "123@qaz"
            };
            string url = "Token";
            string lg = rest.PostDataCrm(url, bodys, tokenCrm);
            dynamic tkent = JsonConvert.DeserializeObject<dynamic>(lg);
            var tk = tkent.Data.Token;
            string url2 = "Api/GetScore";
            dynamic data = new
            {
                TypeID = 1,
                SearchValue = idctm
            };
            string rs = rest.PostDataCrm(url2, data, "basic " + tk);

            List<dynamic> lrs = new List<dynamic>();
            List<dynamic> lpromo = new List<dynamic>();
            List<dynamic> lcoupon = new List<dynamic>();
            dynamic score = JsonConvert.DeserializeObject<dynamic>(rs);
            var aa = score.data.ToString();
            if(aa=="[]")
            {
               var ctm_= getcustomer(idctm);
                var Gender = "0";
                if (ctm_[0].title.ToString().ToLower() == "ms")
                    Gender = "1";
                string bdcrm = ctm_[0].udf1_date.ToString();
                if (bdcrm != "" && bdcrm != null)
                {
                    bdcrm = DateTime.Parse(bdcrm).ToString("yyyy-MM-dd");
                    try
                    {
                        bdcrm = bdcrm + "T00:00:00+07:00";
                    }
                    catch
                    {
                        bdcrm = "";
                    }

                }
                var c_ = new
                {
                    CustomerPrismID = idctm,
                    FirstName = ctm_[0].first_name,
                    LastName = ctm_[0].last_name,
                    Gender = Gender,
                    BirthDay = bdcrm,
                    PhoneNumber = ctm_[0].primary_phone_no,
                    Email = ctm_[0].email_address,
                    AddressNo = ctm_[0].primary_address_line_1,
                    AddressReceipts = ctm_[0].primary_address_line_1,
                    City = ctm_[0].primary_address_line_4,
                    WardPrism = ctm_[0].primary_address_line_3,
                    storename = ctm_[0].store_name,
                    storecode = ctm_[0].store_code,
                    IsDelete = 0
                };
                var dsds = JsonConvert.SerializeObject(c_);
                try
                {
                    var dsd = JsonConvert.SerializeObject(ctm_[0]);
                    sqlcommon sql = new sqlcommon();
                    List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
                };
                    List<string> vl = new List<string>() {
               "2",JsonConvert.SerializeObject(c_),"0","1","from app",DateTime.Now.ToString(),GetIPAddress(),idctm
                };
                    string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
                    sql.runSQL(sqlinsert);

                }
                catch
                {
                    CreateFile(Path.Combine(@"wwwroot\bill\"), "ctm_createcustomer_" + Guid.NewGuid().ToString() + ".txt", "UpdateCustomerPrim?resp=" + JsonConvert.SerializeObject(c_) + "&sid=" + idctm);

                }
                System.Threading.Thread.Sleep(4000);
                aa = "0";
            }
            return aa;
        }
        public string updatephone(string str, string key,string phone)
        {
            RestFile rest = new RestFile();
           // var str = "v1/rest/customer/" + cusid + "/phone/" + sidphone + "?filter=row_version,eq,"+ version;
            var a = new[]
            {
                new{
                phone_type_sid = "",
                phone_no = phone
                }
            };
            string data = JsonConvert.SerializeObject(a);
            var b = rest.PutDatav_22(key, str, data);
            return "1";
        }
            public string updateaddressctm(string key, string sid, string add, string tp, string qh, string px)
        {
            common cmd = new common();
            RestFile rest = new RestFile();

            dynamic item = new System.Dynamic.ExpandoObject();
            //item.customer_sid = sid;
            item.address_line_1 = add;
            item.address_line_2 = px;
            item.address_line_3 = qh;
            item.address_line_4 = tp;

            string url = "v1/rest/customer/" + sid + "/address?cols=sid,row_version&page_no=1&page_size=10";
            string version = rest.GetDataWebClient(key, url);
            List<dynamic> vsl = JsonConvert.DeserializeObject<List<dynamic>>(version);
            string vs = vsl[0].row_version.ToString();
            string url2 = "v1/rest/customer/" + sid + "/address/" + vsl[0].sid.ToString() + "?filter=row_version,eq," + vs;
            string u = rest.updateItem(key, url2, item);
            return u;
        }
        public string updateEmailctm(string key, string sid, string email)
        {
            common cmd = new common();
            RestFile rest = new RestFile();

            dynamic item = new System.Dynamic.ExpandoObject();
            //item.customer_sid = sid;
            item.email_address = email;

            string url = "v1/rest/customer/" + sid + "/email?cols=sid,row_version&page_no=1&page_size=10";
            string version = rest.GetDataWebClient(key, url);
            List<dynamic> vsl = JsonConvert.DeserializeObject<List<dynamic>>(version);
            if (vsl.Count > 0)
            {
                string vs = vsl[0].row_version.ToString();
                string url2 = "v1/rest/customer/" + sid + "/email/" + vsl[0].sid.ToString() + "?filter=row_version,eq," + vs;
                string u = rest.updateItem(key, url2, item);
                return u;
            }
            else
            {
                string urlnew = "v1/rest/customer/" + sid + "/email";
                var k = rest.PostItemPrism(key, urlnew, item);

            }
            return "";
        }
        public DataTable GetTable()
        {
            DataTable table = new DataTable(); // Tạo 1 datatable
                                               // Khởi tạo các cột
            table.Columns.Add("itemsid", typeof(string));
            table.Columns.Add("qty", typeof(string));
      
            // Thêm 5 dòng dữ liệu vào bảng
            table.Rows.Add("544956795000160157", "1");
            table.Rows.Add("544956795000185179", "2");
          
            return table;
        }
        public string updatecreatedocument(string au, string sid,DateTime create, DateTime post_date, string vesion)
        {
            string usta = "v1/rest/document/" + sid + "?filter=row_version,eq," + vesion;
            dynamic st = new
            {
                created_datetime= create,
                post_date= post_date,
                 modified_datetime = post_date,
                invoice_posted_date = post_date,
                status = 4,
            };
            RestFile rest = new RestFile();
           var a= rest.updateItem(au, usta, st);
            return "";
        }

         public void add_address(dynamic a, dynamic b)
        {

            try { b.address1 = a.address1.ToString(); } catch { }
            try { b.address2 = a.address2.ToString(); } catch { }
            try { b.address3 = a.address3.ToString(); } catch { }
            try { b.address4 = a.address4.ToString(); } catch { }
            try { b.address5 = a.address5.ToString(); } catch { }
            try { b.address6 = a.address6.ToString(); } catch { }
        }
        public string converTimeCrm(string d)
        {
            var a = d.Replace(".000", "");
            var c = a.Replace(" ", "+");
            return c;
        }
        public dynamic CreateCpCrm(List<dynamic> LCpo)
        {
            List<dynamic> cpcrm = new List<dynamic>();
            foreach (var i in LCpo)
            {
                dynamic cp = new System.Dynamic.ExpandoObject();
                var v = findvalcoupon(i.couponcode.ToString());
                cp.CouponPrismID = i.sid.ToString();
                cp.CouponCode = i.couponcode.ToString();
                cp.CouponName = v[0];
                cp.CouponValue = v[1];
                cp.CreateDate = i.createddatetime.ToString();
                cp.ExpiryDate = i.sid.ToString();
                cp.IsActive = false;
                cp.CouponType = v[0];
                cpcrm.Add(cp);
            }
            return cpcrm;
        }
        public string[] findvalcoupon(string code)
        {
            string str1 = code.Split('_')[0].ToString();
            string[] str2 = new string[] {"","","" };
            if (str1.Contains("HT"))
            {
                str2[0] = "HT";
                str2[1]= str1.Split('T')[1];
                str2[2] = "Code hoàn tiền";
            }
            else if (str1.Contains("SN"))
            {
                str2[0] = "SN";
                str2[1] = "20";
                str2[2] = "Code sinh nhật 20%";
            }
            else if (str1.Contains("GG"))
            {
                str2[0] = "GG";
                str2[1] = str1.Split('G')[1];
                str2[2] = "Code giảm giá";
            }
            try
            {
                if(str2[0] != "SN")
                str2[1] = (int.Parse(str2[1]) * 1000).ToString();
            }
            catch
            {
                str2[1] = "0";
            }
            return str2;
        }
        public dynamic findcoupon(string code)
        {
            common cmd = new common();
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", "coupon_list.xlsx");
        
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(1);
            var count = ws2.Count();
            var c_ = ws2.FirstOrDefault(s => s.Cell(1).Value.ToString() == code);
            if (c_ != null)
            {
                dynamic o = new System.Dynamic.ExpandoObject();
                o.CouponType = c_.Cell(1).Value.ToString().Split('_')[0];
                o.CouponValue =(int.Parse(c_.Cell(2).Value.ToString())*1000).ToString();
                return o;
            }
            return null;
        }
         public common()
        {

            urlPrism = cf.prism;
            urlCrm = cf.crm;
        }
        public string replaceChar(string str)
        {
            
            List<string> c = new List<string>() { ".", " ","/", "&" };
            foreach(var i in c)
            {
                str = str.Replace(i, "_");
            }
            return str;
        }
        public string replacenamephoto(string str)
        {
            str = str.Replace(" (", "_(").Replace("-", "_").Replace(" ", "");

            List<string> c = new List<string>() { ".", " ", "/" };
            foreach (var i in c)
            {
                str = str.Replace(i, "_");
            }
            return str;
        }
        public Bitmap GetImg(string str)
        {
            string convert = str.Replace("data:image/png;base64,", String.Empty);
            convert = convert.Replace("data:image/jpeg;base64,", String.Empty);
            convert = convert.Replace(" ", "+");
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
            {
                float width = 1024;
                float height = 768;

                using (Bitmap bm2 = new Bitmap(ms))
                {

                    float scale = Math.Min(width / bm2.Width, height / bm2.Height);
                    var scaleWidth = (int)(bm2.Width * scale);
                    var scaleHeight = (int)(bm2.Height * scale);
                    Bitmap bitmap = new Bitmap(bm2, new Size(bm2.Width, bm2.Height));
                    return bitmap;
                }
            }

            //return name + "." + ext;
        }
        public string CreateFile(string FolderSource, string name, string text)
        {
            //string FolderSource = Path.Combine(@"wwwroot\bill\");
            // These examples assume a "C:\Users\Public\TestFolder" folder on your machine.
            // You can modify the path if necessary.

            // Example #1: Write an array of strings to a file.
            // Create a string array that consists of three lines.
            string[] lines = { "First line", "Second line", "Third line" };
            // WriteAllLines creates a file, writes a collection of strings to the file,
            // and then closes the file.  You do NOT need to call Flush() or Close().
            //System.IO.File.WriteAllLines(FolderSource + name, lines);

            // Example #2: Write one string to a text file.
           
            // WriteAllText creates a file, writes the specified string to the file,
            // and then closes the file.    You do NOT need to call Flush() or Close().
            //System.IO.File.WriteAllText(FolderSource + name, text);

            // Example #3: Write only some strings in an array to a file.
            // The using statement automatically flushes AND CLOSES the stream and calls 
            // IDisposable.Dispose on the stream object.
            // NOTE: do not use FileStream for text files because it writes bytes, but StreamWriter
            // encodes the output as text.
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(FolderSource + name))
            {
                file.WriteLine(text);
                //foreach (string line in lines)
                //{
                //    // If the line doesn't contain the word 'Second', write the line to the file.
                //    if (!line.Contains("Second"))
                //    {
                //        file.WriteLine(line);
                //    }
                //}
            }

            // Example #4: Append new text to an existing file.
            // The using statement automatically flushes AND CLOSES the stream and calls 
            // IDisposable.Dispose on the stream object.
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(FolderSource + name, true))
            {
                file.WriteLine("");
            }
            return "ok";
        }
        public void createCustomerPrism(string au,string phone_,string mail)
        {
            List<dynamic> o = new List<dynamic>();
            dynamic p = new
            {
                phone_no = phone_
            };
            dynamic m = new
            {
                email_address = mail
            };
            List<dynamic> phone = new List<dynamic>();
            phone.Add(p);
            List<dynamic> email = new List<dynamic>();
            email.Add(m);
            dynamic l = new
            {
                origin_application = "RProPrismWeb",
                share_type = "1",
                first_name = "AlteredLastName2",
                last_name = "AlteredLastName",
                phones = phone,
                emails = email,
            };

            o.Add(l);


            RestFile rest = new RestFile();
            rest.PostListItemPrism(au, "v1/rest/customer", o);
        }
        public string Encode(string a)
        {
            return HttpUtility.HtmlEncode(a);
        }
        public string convertdateAsString(DateTime Date)
        {
            return Date.ToString("yyyy-MM-dd'T'HH:mm:sszzz");
        }
        public string convertdate(string o)
        {
            string[] a = o.Split('/');
            return a[1] + "/" + a[0] + "/" + a[2];
        }
        public string convertVn(string a)
        {
            a = a.ToUpper();
            a = a.Replace("QUAN TAY", "QUẦN TÂY").Replace("AO LIEN", "ÁO LIỀN");
            List<string> rs2 = new List<string>();
            List<string> rs = a.Split(' ').ToList();
            string[] txt = new string[] { "AO","KHOAT", "KHOAC", "NU", "DAM", "DET", "QUAN", "KHAN", "KIEU", "DAY","NIT", "DAI", "SO", "VONG","CO", "VAY", "SOMI" };
            string[] txtvn = new string[] { "áo", "khoác", "khoác", "nữ","đầm", "dệt", "quần", "khăn", "kiểu", "dây","nịt","dài","sơ","vòng","cổ","váy","sơmi" };
            foreach (var j in rs)
            {
                int i = txt.ToList().IndexOf(j.ToUpper());
                if (i > -1)
                    rs2.Add(txtvn[i]);
                else
                    rs2.Add(j.ToLower());
            }
            return string.Join(" ",rs2);
        }
        public string insertsqlstr(string table, List<string> cl, List<string> vl)
        {
           
            string str = "INSERT INTO";
            str += " " + table + "(";
            str += string.Join(",", cl);
            str += ") values(";
            for (int i = 0; i < vl.Count; i++)
            {
                if (vl[i] == "")
                {
                    vl[i] = "null";
                }
                else if (vl[i].ToLower() == "true" || vl[i].ToLower() == "false")
                {
                    vl[i] = vl[i];
                }
                else
                {
                    try
                    {
                        int.Parse(vl[i]);
                    }
                    catch
                    {

                        vl[i] = "N'" + vl[i] + "'";
                    }
                }
            }
            str += string.Join(",", vl) + ")";
            return str;
        }
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, MySqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public List<Dictionary<string, object>> getdatamySQL(string tb, string substr)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            string cnn = "cnn_web";
            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + substr;
            MySqlCommand sqlCommand = new MySqlCommand(str, con);
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public List<dynamic> storeWeb()
        {
            List<dynamic> stores = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot\store.txt")));
            return stores;
        }
        public Boolean createFolder(string n)
        {

            if (!Directory.Exists(n))
            {
                Directory.CreateDirectory(n);
            }
            return true;
        }
        public Boolean checknumber(string a)
        {
            try
            {
                if (int.Parse(a) > 100000)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public string Getlogin_employ(int Auth_Nonce)
        {
            if (userPrism == "" && passPrism == "")
            {
                userPrism = cf.userprism;
                passPrism = cf.passprism;
            }
            string Auth_Session = "";
            //try
            //{
            var code_1 = Auth_Nonce / 13;
            var trunc = Math.Truncate(decimal.Parse(code_1.ToString()));
            var Auth_Nonce_reponse = (trunc % 99999) * 17;
            string linklg = urlPrism + "v1/rest/auth?usr=" + userPrism + "&pwd=" + passPrism + "";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlPrism + "v1/rest/auth?usr=" + userPrism + "&pwd=" + passPrism + "");
            WebHeaderCollection header = myHttpWebRequest_.Headers;
            myHttpWebRequest_.Method = "GET";
            header.Add("Auth-Nonce", Auth_Nonce.ToString());
            header.Add("Auth-Nonce-Response", Auth_Nonce_reponse.ToString());
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            var header_ = myHttpWebResponse.Headers;
            Auth_Session = header_.Get("Auth-Session");
            myHttpWebResponse.Close();
            wordtaition(Auth_Session);
            //}
            //catch
            //{

            //}
            return Auth_Session;
        }
        public string GetAuth_Nonce_emp()
        {
            string Auth_Session = "";
            Uri ourUri = new Uri(urlPrism + "v1/rest/auth");
            // Creates an HttpWebRequest for the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(ourUri);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            var header = myHttpWebResponse.Headers;
            string Status_Code = header.Get("Http-Status-Code");
            if (Status_Code == "200")
            {
                string Auth_Nonce = header.Get("Auth-Nonce");
                myHttpWebResponse.Close();
                Auth_Session = Getlogin(int.Parse(Auth_Nonce));

            }
            return Auth_Session;

        }
       public dynamic billCrm(dynamic o, string ReturnBill,List<dynamic> billdt)
        {
            dynamic dy = new System.Dynamic.ExpandoObject();
            dy.BillNo = o._sid;
            dy.TotalAmount = o._sale_subtotal;
            dy.TotalPaymentAmount = o._transaction_total_amt;
            dy.Note = o._reason_description;
            dy.CardID= "";
            dy.CouponCode = "";
            dy.UsedMoneyExchange= 0;
            dy.PromoteCode= "";
            dy.StoreID = o._store_code;
            dy.CreateDate = o._created_datetime;
            dy.CreateBy = o._created_by;
            dy.PaymentTypeID= 1;
            dy.CustomerID = o._bt_cuid;
            dy.StatusID= 0;
            dy.BillDetail=billdt;
            dy.invoice_number=o._eft_invoice_number;
            return dy;
        }
        public static string idpayment(string o)
        {
            o = o.ToLower();
            if (o.Contains("cod"))
                return "1";
            else if (o.Contains("momo"))
                return "2";
            if (o.Contains("vnpay"))
                return "3";
            return "4";
        }
        public dynamic itemCrm(dynamic o, string ReturnBill)
        {

            dynamic dy = new System.Dynamic.ExpandoObject();
            dy.ProductID = o.scan_upc;
            dy.ProductName = o.item_description2;
            dy.ProductSize = o.item_size;
            dy.ProductColor = o.attribute;

            dy.NumberOfProduct = o.quantity;
            dy.PromotionUnitPrice = o.price;
            dy.UnitPrice = o.original_price;
            dy.TotalAmount = o.price* o.quantity;
            if (ReturnBill != "")
            {
                dy.ReturnBill = ReturnBill;
                dy.StatusID = 1;
            }
            return dy;
        }
        public dynamic itemPrismReturn(dynamic o,int order)
        {
      
            dynamic dy = new System.Dynamic.ExpandoObject();
            dy.origin_application = "RProPrismWeb";
            dy.quantity = int.Parse(o.quantity.ToString());
            dy.item_type = 2;
            dy.qty_available_for_return = int.Parse(o.quantity.ToString());
            dy.central_document_sid = o.central_document_sid.ToString();
            dy.central_item_pos = order;
            dy.original_price = o.original_price;
            dy.original_tax_amount = o.original_tax_amount;
            dy.scan_upc = o.scan_upc;
            dy.invn_sbs_item_sid = o.invn_sbs_item_sid;
            dy.invn_item_uid = o.invn_item_uid;
            dy.price = o.price;
            dy.tax_area_name = o.tax_area_name;
            dy.tax_area2_name = o.tax_area2_name;
            dy.tax_code = int.Parse(o.tax_code.ToString());
            dy.tax_code2 = o.tax_code2.ToString();
            dy.tax_percent = int.Parse(o.tax_percent.ToString());
            dy.tax2_percent = o.tax2_percent;
            dy.detax_flag = o.detax_flag;
            dy.kit_flag = o.kit_flag;
            return dy;
        }
        public string GetAuth_Nonce()
        {
            string Auth_Session = "";
            Uri ourUri = new Uri(urlPrism + "v1/rest/auth");
            // Creates an HttpWebRequest for the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(ourUri);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            var header =myHttpWebResponse.Headers;
            string Status_Code = header.Get("Http-Status-Code");
            if (Status_Code == "200")
            {
                string Auth_Nonce = header.Get("Auth-Nonce");
                myHttpWebResponse.Close();
                Auth_Session= Getlogin(int.Parse(Auth_Nonce));
               
            }
            return Auth_Session;
           
        }
        public string Getlogin(int Auth_Nonce)
        {
            if(userPrism == "" && passPrism == "")
            {
                userPrism = cf.userprism;
                passPrism = cf.passprism;
            }
            string Auth_Session = "";
            try
            {
                var code_1 = Auth_Nonce / 13;
                var trunc = Math.Truncate(decimal.Parse(code_1.ToString()));
                var Auth_Nonce_reponse = (trunc % 99999) * 17;
                string linklg = urlPrism + "v1/rest/auth?usr=" + userPrism + "&pwd=" + passPrism + "";
                HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlPrism + "v1/rest/auth?usr="+ userPrism + "&pwd="+passPrism + "");
                WebHeaderCollection header = myHttpWebRequest_.Headers;
                myHttpWebRequest_.Method = "GET";
                header.Add("Auth-Nonce", Auth_Nonce.ToString());
                header.Add("Auth-Nonce-Response", Auth_Nonce_reponse.ToString());
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();
                var header_ = myHttpWebResponse.Headers;
                Auth_Session = header_.Get("Auth-Session");
                string aaa = Getsession(Auth_Session);
                myHttpWebResponse.Close();
                //wordtaition(Auth_Session);
                Auth_Session = aaa;
            }
            catch
            {

            }
            return Auth_Session;
        }
        public string Getsession(string au){

            RestFile reft = new RestFile();
            var obj = reft.GetDataWebClient(au, "v1/rest/session");
            var jsonstr = JsonConvert.DeserializeObject<List<dynamic>>(obj);
            string tokent = jsonstr[0]["token"];
            string jsstr = JsonConvert.SerializeObject(jsonstr);
            var sit= reft.GetDataWebClient(au, "v1/rest/sit?ws=webclient");
            return tokent;
        }
       public void wordtaition(string key_login)
        {
            

            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlPrism + "v1/rest/sit?ws=WebClient");
            WebHeaderCollection header = myHttpWebRequest_.Headers;
            myHttpWebRequest_.Method = "GET";
            header.Add("Auth-Session", key_login);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            myHttpWebResponse.Close();
        }
        public List<dynamic> inventory(string key_login,string page_no)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var obj = webClient.DownloadString(urlPrism + "v1/rest/inventory?cols=*&page_no="+ page_no + "&page_size=1");
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public List<dynamic> storesItem(string key_login)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var obj = webClient.DownloadString(urlPrism + "v1/rest/store?filter=active,EQ,true&cols=store_name,sid,address1,address2,address4,address5,subsidiary_sid&page_no=1&page_size=500");
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public List<dynamic> stores()
        {
            string key_login = GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var obj = webClient.DownloadString(urlPrism + "v1/rest/store?cols=store_name,sid,address1,address2,address4,address5,subsidiary_sid&page_no=1&page_size=500");
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public string GetScoreCrm(string id)
        {
            string tk = GetTokenCrm();
            string bstk = "basic " + tk;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlCrm + "Api/GetScore");
            myHttpWebRequest_.Headers.Add("Authorization", bstk);
            myHttpWebRequest_.Method = "POST";
           // myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            List<dynamic> o = new List<dynamic>();
           
            dynamic bodys = new
            {
                TypeID = 1,
                SearchValue = id

            };

            string body = JsonConvert.SerializeObject(bodys);
            var postData = body;

            var data = Encoding.UTF8.GetBytes(body);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            var encoding = ASCIIEncoding.ASCII;

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            //dynamic dt = JsonConvert.DeserializeObject<dynamic>(str);
            return str;
           
        }
        
        public string GetTokenCrm()
        {
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlCrm + "/Token");
            myHttpWebRequest_.Headers.Add("Authorization", tokenCrm);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            List<dynamic> o = new List<dynamic>();
            dynamic bodys = new
            {
                UserName = UserNameCrm,
                Password = PasswordCrm

            };

            string body = JsonConvert.SerializeObject(bodys);
            var postData = body;

            var data = Encoding.UTF8.GetBytes(body);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            var encoding = ASCIIEncoding.ASCII;

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            dynamic dt = JsonConvert.DeserializeObject<dynamic>(str);
            return dt.Data.Token.ToString();
        }
        public string updateItem(string key_login)
        {
            var a = inventory(key_login, "1");
            string des2 = a[0].description2.ToString();
            string urls = urlPrism + "/v1/rest/inventory/529065132000190011?filter=row_version,eq,14";
            //string url = "/api/backoffice/inventory?col=*&filter=(sid,eq,529065132000190011)";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
            //myHttpWebRequest_.ContentType = "application/xml";
            List<dynamic> o = new List<dynamic>();
            dynamic l = new { image_path = "ĐEN" };
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();

            return "";
        }
        public string updateItem4(string key_login)
        {
            var a = inventory(key_login, "1");
            string des2 = a[0].description2.ToString();
            string url = urlPrism + "api/backoffice/inventory";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);

            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";

            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";

            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;

            myHttpWebRequest_.Method = "PUT";
            List<dynamic> o = new List<dynamic>();
            dynamic l = new { sid = "529065132000190011", description1 = "dsd" };
            //l.last_name = "dsd";
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            // string jsonstr = "{\"sid\":\"529065132000190011\",\"description2\":\"wew\"}";
            //            string jsonstr = "[{" +

            //"\"sid\":\"529065132000190011\"" +

            //",\"style_sid\":\"1\"" +

            //",\"dcs_sid\":\"404042880000170750\"" +

            //",\"subsidiary_sid\":\"404040446000120172\"" +

            //",\"inventory_item_uid\":\"1\"" +

            //",\"origin_application\":\"RProPrismWeb\"" +

            //",\"item_number\":\"1\"" +

            //",\"alu\":\"1\"" +

            //",\"attribute\":\"1\"" +

            //",\"active\":\"1\"" +

            //",\"item_size\":\"1\"" +

            //",\"description1\":\"1\"" +

            //",\"active_price\":\"1.250\"" +

            //",\"cost\":\"1.000\"" +

            //",\"upc\":\"100000000000001\"" +

            //"}]";

            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(postData);
            StreamWriter streamWriter = new StreamWriter(myHttpWebRequest_.GetRequestStream());

            streamWriter.Write(jsonstr);

            streamWriter.Flush();

            streamWriter.Close();

            HttpWebResponse response = (HttpWebResponse)myHttpWebRequest_.GetResponse();


            string jsonresponse = "";

            using (var reader = new StreamReader(response.GetResponseStream()))
            {

                string temp = null;
                while ((temp = reader.ReadLine()) != null)
                {
                    jsonresponse += temp;
                }
            }

            return "";
        }
        public dynamic getcustomer(string id)
        {
           
            string Auth_Session = GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = urlPrism + "v1/rest/customer?cols=*&filter=sid,EQ," + id + "&page_no=1&page_size=1";

            var obj = webClient.DownloadString(urls);
            return JsonConvert.DeserializeObject<dynamic>(obj);
        }
        public async Task<string> getinventstore2()
        {
            string urls = urlPrism + "api/backoffice/inventory?action=InventoryGetItems&filter=(upc,lk,*8934581095150*)AND(active,eq,true)AND(sbssid,eq,529033280000153207)&count=true&page_no=1&page_size=30&cols=*";

            string payload = JsonConvert.SerializeObject(new
            {
                data = new
                {
                    ActiveStoreSid = "529033350000139153",
                    ActivePriceLevelSid = "529033307000187060",
                },

            });

            var client = new HttpClient();
            var content = new StringContent(payload, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(urls, content);
            return "";
        }
        public string getinventstore(string sidItem,string sidstore,string subsidiary_sid)
        {
            string key_login = GetAuth_Nonce();
            string urls = urlPrism + "api/backoffice/inventory?action=InventoryGetItems&filter=(upc,lk,*"+ sidItem + "*)AND(active,eq,true)AND(sbssid,eq,"+ subsidiary_sid + ")&count=true&page_no=1&page_size=30&cols=upc,dcscode,vendorcode,description1,description2,itemsize,attribute,cost,actstrprice,actstrmarginpctg,actstrohqty,vendorname,invnvendor.upc,sid,rowversion&sort=description1,asc;sid,asc";
            List<dynamic> data = new List<dynamic>();
            dynamic l = new { ActiveStoreSid = sidstore, ActivePriceLevelSid = "529033307000187060" };
            //l.last_name = "dsd";
            data.Add(l);
            string payload = JsonConvert.SerializeObject(new
            {
                data

            });
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";

            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";

            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(payload);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string strResponse = reader.ReadToEnd();
            dynamic rs = JsonConvert.DeserializeObject<dynamic>(strResponse);
            var datas = JsonConvert.SerializeObject(rs.data[0]);
            return datas;
         
        }
        public string GetInventory(string key_login, string sidItem, string storesid)
        {
            
            var urls = urlPrism + "api/backoffice/inventory/"+ sidItem + "/invnquantity?&filter=(storesid,eq,"+ storesid + ")AND(invnsbsitemsid,eq,"+ sidItem + ")&page_no=1&page_size=10";

            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            WebHeaderCollection header = myHttpWebRequest_.Headers;
            myHttpWebRequest_.Method = "GET";
            header.Add("Auth-Session", key_login);
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";

            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";

            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
          
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();

           
            Stream dataStream = myHttpWebResponse.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string strResponse = reader.ReadToEnd();

            myHttpWebResponse.Close();
            return strResponse;
        }
        public string inforItems(string sku)
        {
           
       
            string key_login = GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var urls = urlPrism + "v1/rest/inventory?cols=*&filter=description1,EQ," + sku + "";
            var store = storesItem(key_login);
            var obj = webClient.DownloadString(urls);
            List<dynamic> o = JsonConvert.DeserializeObject<List<dynamic>>(obj);
            List<dynamic> l = new List<dynamic>();
            List<dynamic> rs = new List<dynamic>();
           
            foreach (var i in o)
            {
                
                var j = l.FirstOrDefault(s => s.color.ToString() == i.attribute.ToString());
                if (j == null)
                {
                    List<dynamic> size = o.Where(s=>s.attribute.ToString() == i.attribute.ToString()).Select(s => s["item_size"]).ToList();
                    List<dynamic> skus = o.Where(s => s.attribute.ToString() == i.attribute.ToString()).Select(s => s["upc"]).ToList();
                    dynamic objs = new System.Dynamic.ExpandoObject();
                    objs.tonkho = true;
                    objs.size = JsonConvert.SerializeObject(size);
                    objs.color = i.attribute.ToString();
                    objs.sku = JsonConvert.SerializeObject(skus);
                    l.Add(objs);
                }
               
            }
            rs.Add(l);
            rs.Add(store);
            return JsonConvert.SerializeObject(rs);
        }
        public string getinventstore2(string sidItem, string sidstore, string subsidiary_sid)
        {
            string key_login = GetAuth_Nonce();
            string a = urlPrism + "/api/backoffice/inventory/534740958000101018/invnquantity?cols=*&filter=(storesid,eq,533851596000165253)AND(invnsbsitemsid,eq,534740958000101018)";
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            //string a = url + "v1/rest/document/" + bill + "/item?cols=lty_orig_points_earned,note4,item_path,discount_reason,is_competing_component,activity2_percent,ship_method_id,order_ship_method_id,employee3_name,item_origin,employee1_sid,employee3_full_name,central_return_commit_state,so_cancel_flag,employee2_login_name,inventory_item_type,sid,item_description3,note8,item_description1,lty_piece_of_tbe_points,hist_discount_perc5,cost,udf_string04,st_address_line3,package_sequence_number,invn_sbs_item_sid,item_status,qty_available_for_return,discounts,original_cost,st_primary,hist_discount_reason4,gift_add_value,note3,lot_type,ref_order_item_sid,gift_transaction_id,hist_discount_amt1,employee1_full_name,total_discount_reason,order_quantity_filled,st_company_name,post_date,ship_method,commission2_amount,inventory_on_hand_quantity,tax_perc_lock,dcs_code,activity3_percent,price,employee1_login_name,central_document_sid,order_ship_method_sid,employee3_login_name,tax2_amount,st_address_line2,fulfill_store_sbs_no,employee5_sid,returned_item_invoice_sid,udf_string01,udf_float01,price_before_detax,ship_id,hist_discount_reason5,employee2_name,commission_code,spif,price_lvl_name,style_sid,commission3_amount,dip_discount_amt,hist_discount_perc2,st_postal_code_extension,employee3_id,commission_percent,item_pos,st_id,activity4_percent,returned_item_qty,detax_flag,custom_flag,st_price_lvl,discount_amt,total_discount_amount,row_version,dip_tax_amount,employee5_id,activation_sid,lty_redeem_applicable_manually,order_ship_method,enhanced_item_pos,item_description2,employee5_name,manual_disc_reason,tax_code_rule2_sid,udf_string05,total_discount_percent,tax_amount,package_number,scan_upc,so_number,shipping_amt_bdt,employee3_sid,invn_item_uid,item_type,employee5_full_name,subsidiary_number,note10,tax_code_rule_sid,employee2_sid,customer_field,tax2_percent,udf_date01,st_address_line5,original_component_item_uid,ref_sale_item_pos,manual_disc_value,tax_code2,original_price_before_detax,lty_points_earned,st_last_name,fulfill_store_sid,st_country,udf_string03,st_first_name,tax_code,hist_discount_reason2,employee4_sid,price_lvl,lty_orig_price_in_points,note2,commission_level,item_lookup,vendor_code,commission5_amount,alu,schedule_number,serial_number,promotion_flag,udf_float02,so_deposit_amt,lty_price_in_points,st_security_lvl,ship_method_sid,created_datetime,hist_discount_amt3,inventory_quantity_per_case,inventory_use_quantity_decimals,commission_amount,hist_discount_perc3,st_address_line1,user_discount_percent,central_item_pos,tracking_number,st_title,ref_order_doc_sid,hist_discount_perc4,tenant_sid,kit_flag,kit_type,lot_number,discount_perc,override_max_disc_perc,modified_datetime,udf_float03,original_tax_amount,commission4_amount,hist_discount_amt4,st_detax_flag,st_tax_area_name,orig_document_number,activity5_percent,archived,package_item_uid,lty_price_in_points_ext,item_size,original_price,store_number,lty_pgm_name,order_type,employee4_id,st_price_lvl_name,attribute,lty_piece_of_tbr_points,orig_subsidiary_number,employee1_name,delete_discount,original_component_item_sid,employee2_id,item_description4,serial_type,activity_percent,employee4_login_name,gift_activation_code,gift_expire_date,tax_char,hist_discount_reason3,document_sid,hist_discount_perc1,udf_string02,created_by,ref_sale_doc_sid,shipping_amt,package_item_sid,note5,st_primary_phone_no,return_reason,employee5_login_name,note9,employee1_id,note1,lty_type,price_lvl_sid,st_address_uid,st_address_line6,apply_type_to_all_items,lty_piece_of_tbr_disc_amt,tax_area_name,promo_disc_modifiedmanually,st_cuid,note6,hist_discount_amt5,orig_sale_price,tax_area2_name,dip_price,modified_by,st_postal_code,st_email,orig_store_number,quantity,note7,hist_discount_amt2,gift_quantity,controller_sid,lty_pgm_sid,employee2_full_name,dip_tax2_amount,origin_application,employee4_full_name,employee4_name,st_tax_area2_name,hist_discount_reason1,manual_disc_type,st_address_line4,tax_percent,tax_char2,fulfill_store_no,from_centrals,st_customer_lookup,maxaccumdiscpercent,maxdiscpercent,promo_gift_item&sort=enhanced_item_pos,desc";
            var obj = webClient.DownloadString(a);
            return obj;

        }
        public string getbill(string id)
        {
            string Auth_Session = GetAuth_Nonce();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", Auth_Session);
            var urls = urlPrism + "v1/rest/document?cols=*&filter=sid,EQ," + id + "&page_no=1&page_size=1";
            var obj = webClient.DownloadString(urls);
            var i = JsonConvert.DeserializeObject<dynamic>(obj);
            string str = "";
            if (i[0].ref_sale_doc_no.ToString() == "")
                str = importbillCRM(i[0], Auth_Session, "0");
            else
                str = importbillCRMreturn(i[0], Auth_Session, "0");
            return str;
          
        }

        public List<dynamic> Itembill(string authen, string bill)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", authen);
            string a = urlPrism + "v1/rest/document/" + bill + "/item?cols=lty_orig_points_earned,note4,item_path,discount_reason,is_competing_component,activity2_percent,ship_method_id,order_ship_method_id,employee3_name,item_origin,employee1_sid,employee3_full_name,central_return_commit_state,so_cancel_flag,employee2_login_name,inventory_item_type,sid,item_description3,note8,item_description1,lty_piece_of_tbe_points,hist_discount_perc5,cost,udf_string04,st_address_line3,package_sequence_number,invn_sbs_item_sid,item_status,qty_available_for_return,discounts,original_cost,st_primary,hist_discount_reason4,gift_add_value,note3,lot_type,ref_order_item_sid,gift_transaction_id,hist_discount_amt1,employee1_full_name,total_discount_reason,order_quantity_filled,st_company_name,post_date,ship_method,commission2_amount,inventory_on_hand_quantity,tax_perc_lock,dcs_code,activity3_percent,price,employee1_login_name,central_document_sid,order_ship_method_sid,employee3_login_name,tax2_amount,st_address_line2,fulfill_store_sbs_no,employee5_sid,returned_item_invoice_sid,udf_string01,udf_float01,price_before_detax,ship_id,hist_discount_reason5,employee2_name,commission_code,spif,price_lvl_name,style_sid,commission3_amount,dip_discount_amt,hist_discount_perc2,st_postal_code_extension,employee3_id,commission_percent,item_pos,st_id,activity4_percent,returned_item_qty,detax_flag,custom_flag,st_price_lvl,discount_amt,total_discount_amount,row_version,dip_tax_amount,employee5_id,activation_sid,lty_redeem_applicable_manually,order_ship_method,enhanced_item_pos,item_description2,employee5_name,manual_disc_reason,tax_code_rule2_sid,udf_string05,total_discount_percent,tax_amount,package_number,scan_upc,so_number,shipping_amt_bdt,employee3_sid,invn_item_uid,item_type,employee5_full_name,subsidiary_number,note10,tax_code_rule_sid,employee2_sid,customer_field,tax2_percent,udf_date01,st_address_line5,original_component_item_uid,ref_sale_item_pos,manual_disc_value,tax_code2,original_price_before_detax,lty_points_earned,st_last_name,fulfill_store_sid,st_country,udf_string03,st_first_name,tax_code,hist_discount_reason2,employee4_sid,price_lvl,lty_orig_price_in_points,note2,commission_level,item_lookup,vendor_code,commission5_amount,alu,schedule_number,serial_number,promotion_flag,udf_float02,so_deposit_amt,lty_price_in_points,st_security_lvl,ship_method_sid,created_datetime,hist_discount_amt3,inventory_quantity_per_case,inventory_use_quantity_decimals,commission_amount,hist_discount_perc3,st_address_line1,user_discount_percent,central_item_pos,tracking_number,st_title,ref_order_doc_sid,hist_discount_perc4,tenant_sid,kit_flag,kit_type,lot_number,discount_perc,override_max_disc_perc,modified_datetime,udf_float03,original_tax_amount,commission4_amount,hist_discount_amt4,st_detax_flag,st_tax_area_name,orig_document_number,activity5_percent,archived,package_item_uid,lty_price_in_points_ext,item_size,original_price,store_number,lty_pgm_name,order_type,employee4_id,st_price_lvl_name,attribute,lty_piece_of_tbr_points,orig_subsidiary_number,employee1_name,delete_discount,original_component_item_sid,employee2_id,item_description4,serial_type,activity_percent,employee4_login_name,gift_activation_code,gift_expire_date,tax_char,hist_discount_reason3,document_sid,hist_discount_perc1,udf_string02,created_by,ref_sale_doc_sid,shipping_amt,package_item_sid,note5,st_primary_phone_no,return_reason,employee5_login_name,note9,employee1_id,note1,lty_type,price_lvl_sid,st_address_uid,st_address_line6,apply_type_to_all_items,lty_piece_of_tbr_disc_amt,tax_area_name,promo_disc_modifiedmanually,st_cuid,note6,hist_discount_amt5,orig_sale_price,tax_area2_name,dip_price,modified_by,st_postal_code,st_email,orig_store_number,quantity,note7,hist_discount_amt2,gift_quantity,controller_sid,lty_pgm_sid,employee2_full_name,dip_tax2_amount,origin_application,employee4_full_name,employee4_name,st_tax_area2_name,hist_discount_reason1,manual_disc_type,st_address_line4,tax_percent,tax_char2,fulfill_store_no,from_centrals,st_customer_lookup,maxaccumdiscpercent,maxdiscpercent,promo_gift_item&sort=enhanced_item_pos,desc";
            var obj = webClient.DownloadString(a);
            return JsonConvert.DeserializeObject<List<dynamic>>(obj);
        }
        public string importbillCRMreturn(dynamic b, string authen, string idex)
        {
            List<dynamic> BillDetails = new List<dynamic>();
            List<dynamic> BillDetail = Itembill(authen, b.sid.ToString());
            foreach (var i in BillDetail)
            {
                double sale = double.Parse(i.quantity.ToString()) * double.Parse(i.price.ToString());
                double dis = double.Parse(i.quantity.ToString()) * (double.Parse(i.original_price.ToString()) - double.Parse(i.price.ToString()));
                dynamic detail = new System.Dynamic.ExpandoObject();
                detail.ProductID = i.invn_sbs_item_sid;
                detail.NumberOfProduct = i.quantity;
                detail.PromotionUnitPrice = i.price;
                detail.UnitPrice = i.original_price;
                detail.TotalAmount = sale;
                detail.StatusID = 1;
                BillDetails.Add(detail);
            }
            sqlcommon sql = new sqlcommon();
         
            var coupon = b.coupons;
            List<string> cpo = new List<string>();
            foreach (var i in coupon)
            {
                string cp = i.link.ToString();
                string[] cc = cp.Split('/');
                cpo.Add(cc[cc.Count() - 1]);
            }
            dynamic c = new System.Dynamic.ExpandoObject();
            c.BillNo = b.sid;
            c.TotalAmount = b.sale_subtotal;
            c.TotalPaymentAmount = b.transaction_total_amt;
            c.Note = b.reason_description;
            c.CardID = "";
            c.Coupon = cpo;
            c.UsedMoneyExchange = 0;

            c.PromoteCode = "";
            c.StoreID = b.store_code;
            c.CreateDate = b.created_datetime;
            c.CreateBy = b.created_by;
            c.PaymentTypeID = 1;
            c.CustomerID = b.bt_cuid;
            c.StatusID = 1;
            c.BillDetail = BillDetails;
            c.BillReturn = b.ref_sale_sid;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = Stringcolumnsql("1", resp, b.sid.ToString(), "return");
            string ss = JsonConvert.SerializeObject(b);
            return sqls;
        }
        public string importbillCRM(dynamic b, string authen, string idex)
        {
            List<dynamic> BillDetails = new List<dynamic>();
            List<dynamic> BillDetail = Itembill(authen, b.sid.ToString());
            foreach (var i in BillDetail)
            {
                double sale = double.Parse(i.quantity.ToString()) * double.Parse(i.price.ToString());
                double dis = double.Parse(i.quantity.ToString()) * (double.Parse(i.original_price.ToString()) - double.Parse(i.price.ToString()));
                dynamic detail = new System.Dynamic.ExpandoObject();
                detail.ProductID = i.invn_sbs_item_sid;
                detail.NumberOfProduct = i.quantity;
                detail.PromotionUnitPrice = i.price;
                detail.UnitPrice = i.original_price;
                detail.TotalAmount = sale;
                detail.StatusID = 0;
                BillDetails.Add(detail);
            }
            sqlcommon sql = new sqlcommon();
            var coupon = b.coupons;
            List<string> cpo = new List<string>();
            foreach (var i in coupon)
            {
                string cp = i.link.ToString();
                string[] cc = cp.Split('/');
                cpo.Add(cc[cc.Count() - 1]);
            }
            dynamic c = new System.Dynamic.ExpandoObject();
            c.BillNo = b.sid;
            c.TotalAmount = b.sale_subtotal;
            c.TotalPaymentAmount = b.transaction_total_amt;
            c.Note = b.reason_description;
            c.CardID = "";
            c.Coupon = cpo;
            c.UsedMoneyExchange = 0;

            c.PromoteCode = "";
            c.StoreID = b.store_code;
            c.CreateDate = b.created_datetime;
            c.CreateBy = b.created_by;
            c.PaymentTypeID = 1;
            c.CustomerID = b.bt_cuid;
            c.StatusID = 0;
            c.BillDetail = BillDetails;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = Stringcolumnsql("1", resp, b.sid.ToString(), idex);
            return sqls;
        }
        public string Stringcolumnsql(string type, string resp, string id, string idex)
        {
            sqlcommon sql = new sqlcommon();
            List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
            List<string> vl = new List<string>() {
               type,resp,"0","1",idex,DateTime.Now.ToString(),GetIPAddress(),id
            };
            string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);

            return sqlinsert;
        }
        public string reaplacetxt(string a)
        {
            return a.Replace("'", "");
        }
        public string GetIPAddress()
        {

            
            return "";
        }
        public string Getsubarress(string key_login) {

            string uurl = urlPrism + "/v1/rest/customer/541556359000178213/address?cols=sid,row_version&page_no=1&page_size=10";
            //HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(uurl);

            //myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            //myHttpWebRequest_.Method = "GET";
            ////myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            //myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            //myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            //var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(uurl);
            WebHeaderCollection header = myHttpWebRequest_.Headers;
            myHttpWebRequest_.Method = "GET";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            header.Add("Auth-Session", key_login);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            myHttpWebResponse.Close();
            return "w";
        }
     
        public string updateAddress(string key_login, string Id, dynamic item)
        {
            string aa = Getsubarress(key_login);
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var urls = urlPrism + "v1/rest/customer?cols=*&filter=sid,EQ," + Id.Trim() + "&page_no=1&page_size=1";
            var obj = webClient.DownloadString(urls);
            dynamic objs = JsonConvert.DeserializeObject<dynamic>(obj);


           
            var row_version = "";
            //try
            //{
            row_version = objs[0].row_version;
            var addresses = objs[0].addresses[0];
            string urls2 = urlPrism + addresses.link+"?filter=row_version,eq," + row_version + "";

            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls2);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = item;
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.StatusCode.ToString();

            return "cập nhật thành công";
            //}
            //catch
            //{
            //    return "Không tồn tại khách hàng";
            //}
        }
        public void UpItemPro(string sid,string Auth_Session)
        {
            string url =urlPrism+ "v1/rest/document/"+ sid + "/item";

            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
            myHttpWebRequest_.Headers.Add("Auth-Session", Auth_Session);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            List<dynamic> Items = new List<dynamic>();
            dynamic l = new
            {
                
                invn_sbs_item_sid = "529065133000154048",
                item_type = 1,
                kit_type = 0,
                origin_application = "RProPrismWeb",
                quantity = 1,

            };
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string strResponse = reader.ReadToEnd();
        }
        public string CreateinvoiceID()
        {

            string Auth_Session = GetAuth_Nonce();
            string url =urlPrism+ "v1/rest/document";

                HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(url);
                myHttpWebRequest_.Headers.Add("Auth-Session", Auth_Session);
                myHttpWebRequest_.Method = "POST";
                myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
                myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
                myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
                List<dynamic> o = new List<dynamic>();
                List<dynamic> Items = new List<dynamic>();
                dynamic l = new
                {
                    origin_application = "RProPrismWeb",
                    bt_cuid = "588036572245856252",
                    //store_number=310,
                    //store_code="ZLR",
                    //original_store_number="310",
                    //original_store_code="ZLR",
                    //store_name="ZLR",
                    //store_uid="529033350000139153",
                    //original_store_uid="529033350000139153",
                    //workstation_number=310
                };
                o.Add(l);
                string jsonstr = JsonConvert.SerializeObject(o);
                var postData = jsonstr;

                var data = Encoding.UTF8.GetBytes(jsonstr);

                using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
                {
                    requestStream.Write(jsonstr);
                }
                var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            //string returnString = response.StatusCode.ToString();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string strResponse = reader.ReadToEnd();
            List<dynamic> rs = JsonConvert.DeserializeObject<List<dynamic>>(strResponse);
            UpItemPro(rs[0].sid.ToString(), Auth_Session);
            return strResponse;
            
        }
        public List<dynamic> getpromotionCPparent(string key,string setid)
        {
            RestFile rest = new RestFile();
            common cmd = new common();
            string addresspr = "v1/rest/pcpromo?cols=sid,reward_gd_disc_value,general_promo_group&filter=validation_coupon_setid,EQ," + setid + "&page_no=1&page_size=1&sort=created_datetime,desc";
            List<dynamic> pro = JsonConvert.DeserializeObject<List<dynamic>>(rest.GetDataWebClient(key, addresspr));
            return pro;
        }
        public string setpromotionCPSql(List<dynamic> pro, dynamic cp, string setname)
        {
            RestFile rest = new RestFile();
            common cmd = new common();
            dynamic c = new System.Dynamic.ExpandoObject();
            var a = cp.createddatetime.ToString().Replace(".000", "");
            var d = a.Replace(" ", "+");
            c.CouponPrismID = cp.sid;
            c.PromotionPrism = "";
            c.CouponValue = "";
            c.coupontype = "";

            if (pro.Count > 0)
            {
                c.PromotionPrism = pro.Select(s => s["sid"].ToString()).ToList();
                c.CouponValue = pro.Select(s => s["validation_subtotal"].ToString()).ToList();
                c.coupontype = pro.Select(s => s["general_promo_group"].ToString()).ToList();
            }
            c.CouponCode = cp.couponcode;
            c.CouponName = setname;
            c.ValueType = "";
            c.ScoreExchange = "";
            c.active = cp.issuedstatus;
            c.CreateDate = d;
            c.IsDelete = 0;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = cmd.Stringcolumnsql("5", resp, cp.sid.ToString(), "");
            return sqls;
        }
        public string SOLcustomer(string valuectm) {
            common cmd = new common();
            dynamic o = JsonConvert.DeserializeObject<dynamic>(valuectm);
           
            dynamic phone = new
            {
                phone_no = o.primary_phone_no

            };
            dynamic mail = new
            {
                email_address = o.email_address,
                origin_application = "RProPrismWeb",
                primary_flag = "true"
            };
            dynamic address = new
            {
                address_line_1 = o.primary_address_line_1,
                address_line_2 = o.primary_address_line_2,
                address_line_3 = o.primary_address_line_3,
                address_line_4 = o.primary_address_line_4,
            };
            List<dynamic> phones = new List<dynamic>();
            phones.Add(phone);
            List<dynamic> mails = new List<dynamic>();
            mails.Add(mail);
            List<dynamic> add = new List<dynamic>();
            add.Add(address);
            dynamic a = new System.Dynamic.ExpandoObject();
            a.phones = phones;
            a.first_name = "customer";
            a.last_name = "customer";
          
            if (o.first_name != "")
                a.first_name = o.first_name;
            if (o.last_name != "")
                a.last_name = o.last_name;
            if (o.primary_address_line_1 != "")
                a.primary_address_line_1 = o.primary_address_line_1;
            if (o.primary_address_line_2 != "")
                a.primary_address_line_2 = o.primary_address_line_2;
            if (o.primary_address_line_3 != "")
                a.primary_address_line_3 = o.primary_address_line_3;
            if (o.primary_address_line_4 != "")
                a.primary_address_line_4 = o.primary_address_line_4;
           
            a.customer_active = true;

            try
            {
                string bd = cmd.convertdate(o.udf1_date.ToString());
                a.udf1_date = DateTime.Parse(bd);
            }
            catch
            {
                a.udf1_date = "";
            }
            if (o.email_address != "")
                a.emails = mails;
            a.addresses = add;

            a.origin_application = "RProPrismWeb";
            a.share_type = "1";
            RestFile rest = new RestFile();
            string au = cmd.GetAuth_Nonce();
            string ctmstr = "v1/rest/customer?filter=primary_phone_no,eq," + o.primary_phone_no + "&cols=*&page_no=1&page_size=10";
            var Getctm = rest.GetDataWebClient(au, ctmstr);
          
            if (Getctm == "[]")
            {
                a.store_name = o.store_name;
                a.store_code = o.store_code;
                a.store_sid = o.store_sid;
                string url = "v1/rest/customer";
                var k = rest.PostItemPrism(au, url, a);
            }
            else
            {
           
                List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(Getctm);
                string vsion = dnm[0].row_version;
                string urlupdate = "v1/rest/customer/" + dnm[0].sid.ToString() + "?filter=row_version,eq," + vsion + "";
                var up = rest.updateItem(au, urlupdate, a);
                try
                {
                    updateaddressctm(au, dnm[0].sid.ToString(), o.primary_address_line_1.ToString(), o.primary_address_line_4.ToString(), o.primary_address_line_3.ToString(), o.primary_address_line_2.ToString());
                }
                catch { }
                try
                {
                    if (o.email_address.ToString() != "")
                        updateEmailctm(au, dnm[0].sid.ToString(), o.email_address.ToString());
                }
                catch { }
                Crm c = new Crm();
                c.UpdateCustomer(dnm[0].sid.ToString(), "");
            }
            return "0";
        }
    }
}
