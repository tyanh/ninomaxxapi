﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Models
{
    public class fileconfig
    {

        public string prism = "";
        public string crm = "";
        public string sqlcrm2 = "";
        public string userprism = "";
        public string passprism = "";
        public string sqlCrm = "";
        public string urlImg = "";
        public string urlcopy = "";
        public string urlvfc = "";
        public string uservfc = "";
        public string passvfc = "";

        public string linkImgVNG = "";
        public string userVNG = "";
        public string passVNG = "";
        public string idVNG = "";
        public string tokentVNG = "";

        public string userViettel = "";
        public string passViettel = "";
        public string apiViettel = "";

        public string tokentzalo = "";
        public string apizalo = "";
        public string imgzalo = "";
        public string zalooa = "";
        public string secretkeyzalo = "";
        public string appidzalo = "";
        public string codezalo = "";
        public string refeshtokentzalo = "";

        public string linkcart = "";
        public string keycartck = "";
        public string keycartcs = "";
     
        public string usercamera = "";
        public string passcamera = "";
        public string apicmr = "";
        public string tokentcmr = "";
        
        public fileconfig() {
            var config = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json").Build();
            sqlCrm = config["ConnectionStrings:connectdata"];
            sqlcrm2 = config["ConnectionStrings:connectCrm"];
            prism = config["AppSettings:linkApi"];
            crm = config["AppSettings:linkCrm"];
            userprism= config["AppSettings:userPrism"];
            passprism = config["AppSettings:passPrism"];
            urlImg = config["AppSettings:urlImg"];
            urlcopy = config["AppSettings:urlcopy"];

            uservfc = config["AppSettings:uservfc"];
            passvfc = config["AppSettings:passvfc"];
            urlvfc = config["AppSettings:linkvfc"];

            linkImgVNG = config["AppSettings:linkImgVNG"];
            userVNG = config["AppSettings:userVNG"];
            passVNG = config["AppSettings:passVNG"];
            idVNG = config["AppSettings:idVNG"];
            tokentVNG = config["AppSettings:tokentVNG"];

            userViettel = config["AppSettings:userViettel"];
            passViettel = config["AppSettings:passViettel"];
            apiViettel = config["AppSettings:apiViettel"];

            tokentzalo= config["AppSettings:tokentzalo"];
            apizalo = config["AppSettings:apizalo"];
            imgzalo= config["AppSettings:imgzalo"];
            zalooa= config["AppSettings:zalooa"];
            secretkeyzalo = config["AppSettings:secretkeyzalo"];
            appidzalo = config["AppSettings:appidzalo"];
            codezalo = config["AppSettings:codezalo"];
            refeshtokentzalo = config["AppSettings:refeshtokentzalo"];

            linkcart = config["AppSettings:linkcart"];
            keycartck = config["AppSettings:keycartck"];
            keycartcs = config["AppSettings:keycartcs"];

            usercamera = config["AppSettings:usercamera"];
            passcamera = config["AppSettings:passcamera"];
            apicmr = config["AppSettings:apicmr"];
            tokentcmr = config["AppSettings:tokentcmr"];


        }
        public string gettokent() {
            return tokentzalo;
        }
       
    }
}
