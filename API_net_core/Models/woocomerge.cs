﻿using Amazon.S3.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WooCommerceNET;
using WooCommerceNET.Base;
using WooCommerceNET.WooCommerce.v2;

namespace API_net_core.Models
{
    public class woocomerge
    {
        string trimstr(string json)
        {
            return json.Trim(new char[] { '\uFEFF', '\u200B' });
        }
        fileconfig cf = new fileconfig();
       // static RestAPI rest = new RestAPI("acxcx", "ewe", "ewe");

        static RestAPI rest = new RestAPI("https://secure-www.ninomaxxconcept.com/wp-json/wc/v2/", "ck_3b275efb9c9e6bf19528796e771b41105ad62c39", "cs_c5e80d0cb7c7fa89be58600f51e6857aedb7d725");
         WCObject wc = new WCObject(rest);
        public List<dynamic> city = new List<dynamic>();
        public List<dynamic> distr = new List<dynamic>();
        public List<dynamic> phuong_xa = new List<dynamic>();
        public woocomerge()
        {
            rest = new RestAPI(cf.linkcart, cf.keycartck, cf.keycartcs, jsonDeserializeFilter: trimstr);
            wc = new WCObject(rest);
        }
        public async Task<List<Product>> productdiscount(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page },{"on_sale","true"},
                });

            return ct;
        }
       
        public async Task<Coupon> GetCoupon(string code)
        {
            var s = await wc.Coupon.GetAll(new Dictionary<string, string>(){{ "code", code}});
            if (s.Count > 0)
                return s[0];
            else
                return null;
        }
        public async Task<string> CreateCoupon(DateTime start,DateTime end, string maximum_amount, string minimum_amount, string discount_type, string description, int amount,string code,double max_dc)
        {
            DateTime now = DateTime.Now;
            List<CouponMeta> metas = new List<CouponMeta>();
            CouponMeta meta = new CouponMeta();
            meta.key = "_max_discount";
            meta.value = max_dc;
            metas.Add(meta);
            Coupon cp = new Coupon();
            cp.amount = amount;
            cp.code = code;
            cp.meta_data = metas;
            cp.date_created = start;
            cp.date_expires = end;
            cp.description = description;
            cp.discount_type = discount_type;
            cp.usage_limit = 1;
            cp.minimum_amount = minimum_amount;
            cp.maximum_amount = maximum_amount;
            
            var s = await wc.Coupon.Add(cp);
            return "0";
        }
        public async Task<Order> getorderall_()
        {
            var a = await wc.Order.GetAll();

            return a[0];
        }
        public async Task<Order> getorder(string id)
        {
            var a = await wc.Order.Get(id);

            return a;
        }
        public async Task<Customer> getcustomer(string id)
        {
            var a = await wc.Customer.Get(id);

            return a;
        }
        public async Task<Order> getorderwidthCoupon(string coupon)
        {
            var a = await wc.Order.GetAll(new Dictionary<string, string>() { { "coupon_lines", "[{'code':'"+ coupon + "'}]" } });

            return a[0];
        }
        public void getaddress()
        {
      
            city = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot\tinh_thanh.txt")));
            distr = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot/quan_huyen.txt")));
            phuong_xa = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText(Path.Combine(@"wwwroot/phuong_xa.txt")));
        }
        public async Task<List<Variation>> getsanpham(string pr,string p)
        {
           var ct = await wc.Product.Variations.GetAll(pr,new Dictionary<string, string>() {
                { "sku", p},
                });

            return ct;
        }
        public async Task<List<Product>> GProduct(string sku)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku},
                });

            return ct;
        }
        public async Task<List<ProductCategory>> catelogy()
        {
            List<ProductCategory> ct = await wc.Category.GetAll(new Dictionary<string, string>() {
                { "per_page", "100" },
                });

            return ct;
        }
        public async Task<string> removecolor(ProductAttributeTerm p)
        {
            await wc.Attribute.Terms.Delete(int.Parse(p.id.ToString()),1,true);
            return "ok";
        }
        public DateTime cvdate(string d)
        {
            string[] s = d.Split("/");
            string c = s[2] + "-" + s[1] + "-" + s[0];
            DateTime a = DateTime.Parse(c);
            return a;
        }
        public async Task<List<Order>> getOrder(string page,string f,string t)
        {
            var a = await wc.Order.GetAll(new Dictionary<string, string>() {{"after",f},{"before",t},
                { "per_page", "100" },  { "page", page }
                });
                
                return a;
        }
        public async Task<List<ProductAttributeTerm>> getcolor(string page)
        {
             var a =await wc.Attribute.Terms.GetAll(1, new Dictionary<string, string>() {
                { "per_page", "100" },  { "page", page }
                });
            return a;
        }
        public async Task<String> UponeVariations(int p, int id,Variation v)
        {
            await wc.Product.Variations.Update(id, v,p);
            return "";
        }
        public async Task<String> UpVariations(int id,BatchObject<Variation> v)
        {
           await wc.Product.Variations.UpdateRange(id, v);
            return "";
        }
        public async Task<String> deleteproduct(string p)
        {
           
            await wc.Product.Delete(int.Parse(p.ToString()));
            return "0";
        }
        public async Task<String> publishproduct(Product p)
        {
            p.status = "publish";
            await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return "";
        }
        public async Task<String> Upproducts(Product p)
        {
            await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return "0";
        }
        public async Task<String> disablelistpro(BatchObject<Product> vr)
        {
            await wc.Product.UpdateRange(vr);
            return "";
        }
            public async Task<String> disablepro(string sku, string vr)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku },{ "per_page", "100" },
                });
            List<string> l_vari = vr.Split(',').ToList();
            List<string> l_sku = sku.Split(',').ToList();
            List<Product> d_p = new List<Product>();
            foreach (var i in ct)
            {
                List<Variation> va = new List<Variation>();
                var avr = await Variationss(i);
                var ll = i.attributes[0].options;
                int l_ = 0;
                if(avr.Count==0)
                {
                    i.status = "pending";
                    await wc.Product.Update(int.Parse(i.id.ToString()),i);
                    continue;
                }
                foreach (var j in avr)
                {
                    string cl_ = j.attributes[0].option;
                    if (ll.IndexOf(cl_) == -1)
                    {
                        await wc.Product.Variations.Delete(int.Parse(j.id.ToString()), int.Parse(i.id.ToString()));
                        l_++;
                    }
                    else
                    {
                        foreach (var k in l_vari)
                        {
                            string[] ar = k.Split('$');
                            if (ar[0] == i.sku && ar[1] == cl_)
                            {
                                //Variation v = new Variation();
                                //v.id = j.id;
                                //v.visible = false;
                                //va.Add(v);
                                await wc.Product.Variations.Delete(int.Parse(j.id.ToString()), int.Parse(i.id.ToString()));
                                l_++;
                                break;
                            }
                        }
                    }
                        
                }
                if (l_ == ll.Count())
                {
                    i.status = "pending";
                    await wc.Product.Update(int.Parse(i.id.ToString()), i);
                }
                //BatchObject<Variation> o_ = new BatchObject<Variation>();
                //o_.update = va;
                //await wc.Product.Variations.UpdateRange(int.Parse(i.id.ToString()),o_);
                //break;
            }
           
            return "";
        }
        public async Task<List<ProductCategory>> catelogyParent(List<string> id)
        {
            var ct = await wc.Category.GetAll(new Dictionary<string, string>() {
                { "include", string.Join(",",id) },
                });
             return ct;
        }
        public async Task<List<ProductCategory>> catelogyChild(List<string> id)
        {
            var ct = await wc.Category.GetAll(new Dictionary<string, string>() {
                { "include", string.Join(",",id) },
                });
            return ct;
        }
        public async Task<Customer> finduser(string id)
        {
      
            var ct = await wc.Customer.Get(int.Parse(id));
            return ct;
        }
        public async Task<bool> updateuser(Customer u)
        {
            int id = int.Parse(u.id.ToString());
            var ct = await wc.Customer.Update(id, u);
            return true;
        }
        public async Task<List<ProductTag>> getlistTagId()
        {
            List<ProductTag> L_Tag = await wc.Tag.GetAll
                (new Dictionary<string, string>() {
                        { "per_page", "100" },
                });
            return L_Tag;
        }
        public async Task<ProductTag> searchsTagId(string s)
        {
            List<ProductTag> L_Tag = await wc.Tag.GetAll(new Dictionary<string, string>() {{ "search", s }});
            return L_Tag.FirstOrDefault();
        }
        public async Task<List<ProductTag>> createlistTagId(string tags)
        {
            List<ProductTag> L_Tag = new List<ProductTag>();
        
            string[] t = tags.Split(',');
           
            foreach (var i in t)
            {
                ProductTag b =await searchsTagId(i);
                if (b == null)
                {
                    ProductTag Tag = new ProductTag();
                    Tag.name = i;
                    Tag.slug = i;
                    
                    var a = await wc.Tag.Add(Tag);
                    L_Tag.Add(a);
                }
                else
                    L_Tag.Add(b);
            }
            return L_Tag;
        }
        public List<ProductTagLine> listTagId(string tag)
        {
            List<ProductTagLine> L_Tag = new List<ProductTagLine>();
            string[] t = tag.Split(',');
            foreach (var i in t)
            {
                ProductTagLine Tag = new ProductTagLine();
                Tag.name = i;
                Tag.slug = i;
                L_Tag.Add(Tag);
            }
            return L_Tag;
        }

        //public List<ProductMeta> updatemeta(string meta,string value)
        //{
        //    List<ProductMeta> L_meta = new List<ProductMeta>();
        //    ProductMeta meta = new ProductMeta();
        //    meta.id = 89837;
        //    meta.key = "chat_lieu";
        //    meta.value = chatlieu;
        //    L_meta.Add(meta);
        //    return L_meta;
        //}
        public List<ProductMeta> listMetaId(string chatlieu)
        {
            List<ProductMeta> L_meta = new List<ProductMeta>();
            ProductMeta meta = new ProductMeta();
            meta.id = 89837;
            meta.key = "chat_lieu";
            meta.value = chatlieu;
            L_meta.Add(meta);
            return L_meta;
        }
        public List<ProductAttributeLine> listAttrID(List<string> color, List<string> size)
        {
            List<ProductAttributeLine> L_atr = new List<ProductAttributeLine>();

            ProductAttributeLine atr_color = new ProductAttributeLine();
            atr_color.id = 1;
            atr_color.options = color;
            atr_color.position = 0;
            atr_color.variation = true;
            L_atr.Add(atr_color);

            ProductAttributeLine atr_size = new ProductAttributeLine();
            atr_size.id = 2;
            atr_size.options = size;
            atr_size.position = 1;
            atr_size.variation = true;
            L_atr.Add(atr_size);
            return L_atr;
        }
        public List<ProductAttributeLine> listAttr(List<string> color, List<string> size)
        {
            List<ProductAttributeLine> L_atr = new List<ProductAttributeLine>();
        
                ProductAttributeLine atr_color = new ProductAttributeLine();
                atr_color.name = "Color";
                atr_color.options = color;
                atr_color.position = 0;
                atr_color.variation = true;
                L_atr.Add(atr_color);
          
            ProductAttributeLine atr_size = new ProductAttributeLine();
            atr_size.name = "size";
            atr_size.options = size;
            atr_size.position = 0;
            atr_size.variation = true;
            L_atr.Add(atr_size);
            return L_atr;
        }
        public async Task<Product> searchProduct(string sku)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku },
                });

            return ct.FirstOrDefault();
        }
        public async Task<Product> searchProductId(int id)
        {
            var ct = await wc.Product.Get(id);

            return ct;
        }
        public async Task<List<Product>> searchProducts(string sku)
        {

           
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                { "sku", sku},{ "per_page", "100" },
                });

            return ct;
        }
        public async Task<List<Product>> searchProductsCate(int cate,string page)
        {


            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                {"category",cate.ToString()},{ "per_page", "100" },{ "status", "publish"},{ "page", page },
                });

            return ct;
        }
        public async Task<List<Product>> searchProductslistId(List<string> id)
        {
          
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                 { "include", string.Join(",",id) },{ "per_page", "100" },
                });

            return ct;
        }
        public async Task<List<Product>> allProductspublish(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                        { "per_page", "100" },{  "page", page},{ "status", "publish"}
                });

            return ct;
        }
        public async Task<List<Product>> allProducts(string page)
        {
            var ct = await wc.Product.GetAll(new Dictionary<string, string>() {
                        { "per_page", "100" },{  "page", page}
                });

            return ct;
        }
        public async Task<List<Variation>> VariationssId(int sku)
        {
            var ct = await wc.Product.Variations.GetAll(sku, new Dictionary<string, string>() {
                        { "per_page", "100" },
                });

            return ct;
        }
        public async Task<List<Variation>> Variationss(Product sku)
        {
            var ct = await wc.Product.Variations.GetAll(sku.id, new Dictionary<string, string>() {
                        { "per_page", "100" },
                });

            return ct;
        }
        public List<ProductCategoryLine> createcate(List<string> cates)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            foreach (var i in cates)
            {
                try
                {
                    ProductCategoryLine cate = new ProductCategoryLine();
                    cate.id = int.Parse(i);
                    ct.Add(cate);
                }
                catch
                {

                }
            }
            return ct;
        }
        
        public async Task<Product> addproduct(List<ProductAttributeLine> L_atr,string name, string short_des,string sku,List<string> cates,int idedit, string plug, List<ProductTagLine> L_tag) {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            ct = createcate(cates);
            
            Product p = new Product()
            {
                name = name,
                short_description = short_des,
                description = "",
                slug = name + "-" + plug,
                type = "variable",
                status = "publish",
                attributes = L_atr,
                sku= sku,
                categories= ct,
                tags=L_tag,
            };
            p = await wc.Product.Add(p);
            return p;
        }
        public async Task<Coupon> addcoupon(int vlue, string code, string desc,string type)
        {
           
            Coupon p = new Coupon()
            {
                amount= vlue,
                code=code,
               description=desc,
               discount_type= type
            };
            p = await wc.Coupon.Add(p);
            return p;
        }
       
        public ProductMeta bindProductMeta(int id,string key,string vl)
        {
            ProductMeta meta = new ProductMeta();
            meta.id = id;
            meta.key = key;
            meta.value = vl;
            return meta;
          
        }
      
        public async Task<Product> addproduct_excel(List<ProductAttributeLine> L_atr, string name, string short_des,string sku, List<string> cates, List<ProductMeta> L_meta,string plug)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            ct = createcate(cates);

            Product p = new Product()
            {
                name = name,
                short_description = short_des,
                description = short_des,
                slug = name + "-" + plug,
                type = "variable",
                status = "publish",
                attributes = L_atr,
                sku = sku,
                categories = ct,
                meta_data=L_meta
            };
            p = await wc.Product.Add(p);
            return p;
        }
        public async Task<bool> UpAttrproduct(List<ProductAttributeLine> L_atr,List<string> cate,Product p)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            ct = createcate(cate);
            p.attributes = L_atr;
            p.categories = ct;
            await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return true;
        }
        public async Task<bool> UpOrderproduct(int id,int order)
        {
            var pr = await searchProductId(id);
            pr.menu_order = order;
            await wc.Product.Update(id, pr);
            return true;
        }
        public async Task<bool> UpCateproduct(List<string> cate, Product p,List<ProductTagLine> tags)
        {
            List<ProductCategoryLine> ct = new List<ProductCategoryLine>();
            ct = createcate(cate);
             p.categories = ct;
            p.tags = tags;
            await wc.Product.Update(int.Parse(p.id.ToString()), p);
            return true;
        }
        public async Task inputProductvariable(Product p,string price,string upc,string color,string size,string pricedown)
        {
            List<Variation> vrs = new List<Variation>();
            List<VariationAttribute> vatrib = new List<VariationAttribute>()
                { new VariationAttribute() {id=1, name="Color",option=color },
                  new VariationAttribute() {id=2,name="size",option=size }
            };

            //var img = new VariationImage();
            //img.position = 1;
            //img.src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2uiIkpYd2Nr39eLLzm-dmxKAAXiN1_cm4EZKyXwmnCkcL2Ks8Vw&s";
            //img.name = upc;
            Variation var1 = new Variation()
            {
                regular_price = decimal.Parse(price),
                visible = true,
                attributes = vatrib,
                stock_quantity = 5,
                manage_stock = false,
                sku = upc,
              
            };
            if (pricedown != "")
                var1.sale_price = decimal.Parse(pricedown);
            vrs.Add(var1);
            var1 = await wc.Product.Variations.Add(var1, p.id.Value);
            p.variations.Add(var1.id.Value);
            await wc.Product.Update(p.id.Value, p);
        }
    }
}
