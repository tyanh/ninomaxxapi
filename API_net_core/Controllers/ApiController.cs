﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API_net_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Amazon.S3;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using System.Text;
using System.Drawing.Printing;
using System.Drawing;
using WooCommerceNET.WooCommerce.v2;
using API_net_core.Areas.AdminTool.Models;
using ClosedXML.Excel;
using System.Data;
using System.Drawing.Imaging;
using API_net_core.Areas.Tool.Models;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Net.Mail;
using System.Threading;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_net_core.Controllers
{
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ApiController : Controller
    {
        fileconfig cf = new fileconfig();
        private readonly IHostingEnvironment _hostingEnvironment;
        public IConfiguration _configuration;
        private DataContext context;
        string tokenprism = "";
        public ApiController(IConfiguration configuration, DataContext ctx)
        {
            _configuration = configuration;
            context = ctx;
        }
        [HttpGet("pushntft")]
        public string pushntft()
        {
            WebClient webClient = new WebClient();
            var obj = webClient.DownloadString("https://vfc.ninomaxx.com.vn/Index?id=12");
            return obj;
        }
        [HttpGet("dowloadfolder")]
        public string dowloadfolder()
        {
            
            return "";
        }
        [HttpGet("sendmail")]
        public string sendmail(string data)
        {
            /*dynamic test = new System.Dynamic.ExpandoObject();
            test.hoten = "cu ty";
            test.phone = "0908543884";
            test.diachi = "cu ty";
            test.model = "cu ty";
            test.obj = "driver";
            data = JsonConvert.SerializeObject(test);*/

            dynamic d = JsonConvert.DeserializeObject<dynamic>(data);
            var html = "";
            try {
                if (d.obj.ToString() == "driver")
                {
                    html = "<p>ĐĂNG KÝ LÁI THỬ XE HONDA</p>" +
                      "<p>Họ tên:" + d.fullname + "</p>" +
                       "<p>Email:" + d.email + "</p>" +
                      "<p>Điện thoại:" + d.phone + "</p>" +
                      "<p>Địa chỉ:" + d.address + "</p>" +
                      "<p>Dòng xe:" + d.model + "</p>";
                }
                else
                {
                    html = "<p>yêu cầu báo giá</p>" +
                      "<p>Họ tên:" + d.hoten + "</p>" +
                      "<p>Điện thoại:" + d.phone + "</p>" +
                      "<p>Địa chỉ:" + d.diachi + "</p>" +
                      "<p>Dòng xe:" + d.model + "</p>";
                }
            }
            catch
            {
                html = "<p>yêu cầu báo giá</p>" +
                      "<p>Họ tên:" + d.hoten + "</p>" +
                      "<p>Điện thoại:" + d.phone + "</p>" +
                      "<p>Địa chỉ:" + d.diachi + "</p>" +
                      "<p>Dòng xe:" + d.model + "</p>";
            }

            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress("dangkylaithuoto@gmail.com");
            message.Sender = new MailAddress("dangkylaithuoto@gmail.com");
            message.To.Add(new MailAddress("hondasaigonq7@gmail.com"));
      
            message.Subject = "báo giá";
            message.IsBodyHtml = true; //to make message body as html  
            message.Body = html;
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //for gmail host  
            smtp.EnableSsl = true;

            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("dangkylaithuoto@gmail.com", "ocoxewlkltahutnc");
            //smtp.Credentials = new NetworkCredential("minhnhat64@gmail.com", "Ty@641985");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
            return "0";
        }
        [HttpGet("updateperc")]
        public string updateperc(string sid, string au, string per)
        {
            RestFile refs = new RestFile();
            common cmd = new common();
           // au = cmd.GetAuth_Nonce();
            var strrvs = "api/common/employee?filter=sid,eq,"+sid+"&cols=rowversion";
            var rsr =JsonConvert.DeserializeObject<List<dynamic>>(refs.GetData(au, strrvs));
            var str= "api/common/employee/"+ sid;
            var r = rsr[0].rowversion.ToString();
        
            var dt = new
            {
                data = new[] {
                    new{
                        rowversion=Int64.Parse(r),
                        maxdiscperc=Int64.Parse(per),
                     
                    }
                }

            };
            var u = refs.PutDatav_22(au, str, JsonConvert.SerializeObject(dt));
            return "0";
        }

        [HttpGet("OrderProductCate")]
        public async Task<string> OrderProductCate(string order)
        {
            List<string> ids = order.Split(',').ToList();
            woocomerge woo = new woocomerge();
           
            for(int i=0;i<ids.Count;i++)
            {
                await woo.UpOrderproduct(int.Parse(ids[i]),i);
            }
            return "0";
        }
        [HttpGet("GetProductCate")]
        public async Task<string> GetProductCate(int cate)
        {
            woocomerge woo = new woocomerge();
            List<dynamic> imgs = new List<dynamic>();
            int page = 1;
            List<Product> imgsw = new List<Product>();
            do
            {
                var p = await woo.searchProductsCate(cate, page.ToString());
              
                if (p.Count <= 0)
                {
                    page = -1;
                    break;
                }
                else
                {
                    imgsw.AddRange(p);
                    
                    page++;
                }
            } while (page > 0);
            imgsw = imgsw.OrderBy(s => s.menu_order).ToList();
            foreach (var i in imgsw)
            {
                try
                {
                    dynamic img = new System.Dynamic.ExpandoObject();
                    if (i.attributes[0].options[0] != "")
                        img.hinh = (i.sku + "/" + i.sku + "_" + i.attributes[0].options[0] + "_1.jpg").ToString();
                    else
                        img.hinh = "";
                    img.id = i.id.ToString();
                    img.order = i.menu_order.ToString();
                    img.sku = i.sku.ToString();
                    imgs.Add(img);
                }
                catch
                {
                    string a = "";
                }
            }
      
            return JsonConvert.SerializeObject(imgs);
        }
        [HttpGet("UpdatedatestartWoo")]
        public async Task<string> UpdatedatestartWoo(string mtk, string date)
        {
            string d = DateTime.Parse(date).ToString("dd/MM/yyyy");
            woocomerge woo = new woocomerge();

            var p = await woo.searchProduct(mtk);
            if (p == null)
                return "1";
            var mt = p.meta_data;
            List<ProductMeta> meta = new List<ProductMeta>();
            meta.AddRange(mt);
            ProductMeta newt = woo.bindProductMeta(70824, "date_start", d);
            meta.Add(newt);
            p.meta_data = meta;
            await woo.Upproducts(p);
            return "0";
        }
        [HttpGet("getcouponinvoice")]
        public string getcouponinvoice(string invoice)
        {
            storePrism store = new storePrism();
            var store_iv = store.GET_COUPON_BY_INVOICE(invoice);
            return JsonConvert.SerializeObject(store_iv);
        }


        [HttpGet("savevongxoay")]
        public string savevongxoay(string face, string messengeruserid, string phone, string coupon, string pricedetail, string address, string dategetcoupon, string timestamp)
        {
            common cmd = new common();
            //timestamp= cmd.convertdate(timestamp);
            //dategetcoupon = cmd.convertdate(dategetcoupon);
            storePrism store = new storePrism();
            var rs = store.INSERT_NEW_RECORD_VX_FB(messengeruserid, face, phone, coupon, pricedetail, address, timestamp, dategetcoupon);
            return rs;
        }
        [HttpGet("get_coupon_celeb")]
        public string get_coupon_celeb(string type)
        {
            try
            {
                common cmd = new common();
                storePrism store = new storePrism();
                var rs = store.GET_COUPON_CELEB(type);
                List<dynamic> score = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(rs));
                var coew = score[0].COUPON_CODE;
                UpcodeCouponCELEB(coew.ToString(), "0", "30", "vx");
                return coew;
            }
            catch
            {
                return "0";
            }
        }
        [HttpGet("vongxoaycoupon")]
        public string vongxoaycoupon(string type)
        {
            common cmd = new common();
            storePrism store = new storePrism();
            var rs = store.GET_COUPON_VONGXOAY(type);
            List<dynamic> score = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(rs));
            var coew = score[0].COUPON_CODE;
            UpcodeCoupon(coew.ToString(), "14", "28", "vx");
            return coew;
        }
        [HttpGet("CheckCustomer")]
        public string CheckCustomer(string phone)
        {
            common cmd = new common();
            storePrism store = new storePrism();
            var rs = store.CHECK_CUSTOMER_BUYING(phone);
            return rs;
        }
        [HttpGet("CrmGetCouponSN")]
        public string CrmGetCouponSN()
        {
            //return "";
            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.GET_COUPON_BY_INVOICE("563834806000604214");
            string JSONresult = JsonConvert.SerializeObject(store_iv);
            return JSONresult;
        }
        [HttpGet("CrmGetCouponList")]
        public string CrmGetCouponList()
        {
            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.GET_COUPON_LIST_FOR_CRM();
            string JSONresult = JsonConvert.SerializeObject(store_iv);
            return JSONresult;
        }
        [HttpGet("CrmGetCoupon")]
        public string CrmGetCoupon(int qlt, int index)
        {
            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.GET_COUPON_CODE_FOR_CRM(qlt, index);
            string JSONresult = JsonConvert.SerializeObject(store_iv);
            return JSONresult;
        }
        [HttpGet("disableproductweb")]
        public async Task<string> disableproductweb(string sku)
        {
            string err = "-1";
            woocomerge woo = new woocomerge();
            var p = await woo.GProduct(sku);
            if (p.Count > 0)
            {
                p[0].status = "pending";
                err = await woo.Upproducts(p[0]);
            }
            return err;
        }


        [HttpGet("woopostinvoid")]
        public async Task<string> woopostinvoid(string id)
        {
            return "ok";
            woocomerge woo = new woocomerge();
            var pr = await woo.getorder(id);
            var items = pr.line_items;
            foreach (var i in items)
            {
                string sku = i.sku;
                string qlt = ("-") + i.quantity.ToString();
                var iv = EditinventLive(sku, qlt);
            }
            return "ok";
        }
        [HttpGet("testgetcoupont")]
        public string testgetcoupont(string sid)
        {
            common cmd = new common();
            RestFile refs = new RestFile();
            string au = cmd.GetAuth_Nonce();
            string str = "v1/rest/document/561056542000696155/coupon/561056615000610159&cols=*";
            var s = refs.GetData(au, str);
            return "";
        }
        [HttpGet("createvoucher2")]
        public string createvoucher2(string au)
        {
            common cmd = new common();
            RestFile refs = new RestFile();
            au = cmd.GetAuth_Nonce();
            string create = "api/backoffice/receiving";
            var dtcreate = new
            {
                data = new[] {
                    new{
                        originapplication="RProPrismWeb",
                        sbssid="605135163000180340",
                        clerksid="605067254000112260",
                        storesid="605135194000187506",
                        publishstatu=1,
                        voutype=0,
                        vouclass=0,
                       // vendsid="605135181000182648"
                    }
                }

            };
            var rs = refs.PostDatav_22(au, create,JsonConvert.SerializeObject(dtcreate));
            var jcreate = JsonConvert.DeserializeObject<dynamic>(rs);
            var datanew = jcreate["data"][0];
            var sidnew = datanew["sid"].ToString();
            var rowversionn= datanew["rowversion"].ToString();
            var put1 = "api/backoffice/receiving/" + sidnew;
            var dtput1 = new
            {
                data = new[] {
                       new{ 
                           rowversion=int.Parse(rowversionn),
                           vendsid="605135183000185757"
                       }
                }
            };
            var rsput1 = refs.PutDatav_22(au, put1, JsonConvert.SerializeObject(dtput1)) ;
            var post2 = "api/backoffice/receiving/"+ sidnew + "/recvitem";
         
            var dtcreate2 = new
            {
                data = new[] {
                    new{
                       originapplication="RProPrismWeb",
                       itemsid="605144329000155074",
                       cost=20000,
                       qty=1,
                       upc="8934581",
                       description1="ACCESSORIES",
                       serialno=(System.Decimal?) null,
                       lotnumber=(System.Decimal?) null,
                       serialtype=0,
                       lottype=0,
                       vousid=sidnew
                    }
                }

            };
            var rs2 = refs.PostDatav_22(au, post2, JsonConvert.SerializeObject(dtcreate2));

            var getrvs = "api/backoffice/receiving/"+sidnew+"?cols=*,recvterm,recvterm.termtype";
            var getvs = refs.GetData(au, getrvs);
            var jvsion = JsonConvert.DeserializeObject<dynamic>(getvs);
            var newrowversionn = jvsion[0]["rowversion"].ToString();
            var put2 = "api/backoffice/receiving/" + sidnew;
            var dtcreate3 = new
            {
                data = new[] {
                    new{
                      rowversion=int.Parse(newrowversionn),
                      status=4,
                      approvbysid="605067254000112260",
                      approvdate="2022-05-16T02:51:53.959Z",
                      approvstatus=2,
                      publishstatus=2
                    }
                }

            };
            var rs3 = refs.PutDatav_22(au, put2, JsonConvert.SerializeObject(dtcreate3));
            return "";
        }
        [HttpGet("createvoucher")]
        public string createvoucher(string sid, string au)
        {
            common cmd = new common();
            RestFile refs = new RestFile();
             au = cmd.GetAuth_Nonce();

            string strget = "api/backoffice/receiving/" + sid + "?cols=*,recvterm,recvterm.termtype";
            var rs = refs.GetData(au, strget);
            var asn = JsonConvert.DeserializeObject<List<dynamic>>(rs);
            //string ex1 = "api/backoffice/receiving/recvpackage/?cols=*&filter=(vousid,eq," + sid + ")";
            string ex1 = "api/backoffice/receiving/" + sid + "/recvpackage/?cols=*";
            var ex_ = refs.GetData(au, ex1);
            string convertasntovoucher = "api/backoffice/receiving?action=convertasntovoucher";
            var ssid = asn[0].clerksid.ToString();
            var dtcv = new
            {
                clerksid = ssid,
                asnsidlist = sid,
                doupdatevoucher = false,
                originapplication = "RProPrismWeb"
            };
            var x = new
            {
                data = new[] { dtcv }

            };
            string json = JsonConvert.SerializeObject(x);
            var postcv = refs.PostDatav_22(au, convertasntovoucher, json);
            string put = "api/backoffice/receiving/" + sid;

            string urlrvs= "api/backoffice/receiving?filter=sid,eq,"+ sid;
            var newrow = refs.GetData(au, urlrvs);
            List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(newrow);
            string vsion = dnm[0].rowversion.ToString();
            var x2 = new
            {
                data = new[] {
                    new{
                    rowversion=int.Parse(vsion),
                    publishstatus=1
                    }
                }

            };
            string json2 = JsonConvert.SerializeObject(x2);
            var put_ = refs.PutDatav_22(au, put, json2);

            string putend = "api/backoffice/receiving/" + sid;
            var x2end = new
            {
                data = new[] {
                    new{
                    rowversion=int.Parse(vsion)+1,
                    publishstatus=2,
                    status=4
                    }
                }

            };
            string json2end = JsonConvert.SerializeObject(x2end);
            var put_end = refs.PutDatav_22(au, putend, json2end);
            return "0";
        }
        public string ctmCrmExit(string idctm)
        {

            string tokenCrm = "basic TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=";
            RestFile rest = new RestFile();
            dynamic bodys = new
            {
                UserName = "admin",
                Password = "Ninom@xx1998"
            };
            string url = "Token";
            string lg = rest.PostDataCrm(url, bodys, tokenCrm);
            dynamic tkent = JsonConvert.DeserializeObject<dynamic>(lg);
            var tk = tkent.Data.Token;
            string url2 = "Api/GetScore";
            dynamic data = new
            {
                TypeID = 1,
                SearchValue = idctm
            };
            string rs = rest.PostDataCrm(url2, data, "basic " + tk);

            List<dynamic> lrs = new List<dynamic>();
            List<dynamic> lpromo = new List<dynamic>();
            List<dynamic> lcoupon = new List<dynamic>();
            dynamic score = JsonConvert.DeserializeObject<dynamic>(rs);
            var aa = score.data.ToString();
            return aa;
        }
        [HttpGet("testmissdata")]
        public string testmissdata()
        {
            sqlmissdata miss = new sqlmissdata();
            List<string> cl = new List<string>() { "idwoo", "contents", "status", "createdate" };
            List<string> vl = new List<string>() { "1111", "test", "lan 1", DateTime.Now.ToString() };
            string ins = miss.insertsqlstr("missprism", cl, vl);
            string ins2 = miss.insertsqlstr("missdata", cl, vl);
            miss.runSQL(ins);
            miss.runSQL(ins2);
            return "0";
        }
        [HttpGet("testUpStatus")]
        public string testUpStatus()
        {
            common cmd = new common();
            string au = cmd.GetAuth_Nonce();
            RestFile refs = new RestFile();
            string r = refs.GetRowvwersioill(au, "594157298000139108");
            var pustr = "v1/rest/document/594157298000139108?filter=row_version,eq," + r;
             var x2end = new[]
                {
                         new
                         {
                            status=4
                         }
                    };
            string json2end = JsonConvert.SerializeObject(x2end);
            var put_end = refs.PutDatav_22(au, pustr, json2end);
            return "0";
        }
        
        [HttpGet("ReturnIvoice")]
        public string ReturnIvoice(string oldsid, string au,string note,string storeid)
        {
            common cmd = new common();
            if(au==null)
            au = cmd.GetAuth_Nonce();
            RestFile refs = new RestFile();
            string finddoc = "v1/rest/document?filter=sid,eq," + oldsid + "&cols=*";
            var inven = JsonConvert.DeserializeObject<dynamic>(refs.GetDataWebClient(au, finddoc));
            string findItemdoc = "v1/rest/document/"+ oldsid + "/item?cols=*";
            var doc = refs.GetDataWebClient(au, findItemdoc);
            List<dynamic> docs_ = JsonConvert.DeserializeObject<List<dynamic>>(doc);
            List<dynamic> itemadd = new List<dynamic>();
            List<dynamic> itemcrm = new List<dynamic>();
            string cusid = inven[0].bt_cuid.ToString();
            int order = docs_.Count;
            foreach (var d in docs_)
            {
                d.central_document_sid = oldsid;
                itemadd.Add(cmd.itemPrismReturn(d, order));
                itemcrm.Add(cmd.itemCrm(d, oldsid));
                order--;
            }
            //return "";
            string iv = "v1/rest/document";
            string billID = "";
            string rowvs = "";

            var data = new[]
                {
                new{
                    origin_application = "RProPrismWeb",
                    store_uid= storeid,//inven[0].store_uid,
                    original_store_uid=inven[0].store_uid,
                    notes_general=note,
                    bt_cuid=cusid,
                      
                }
                };
                string dt = JsonConvert.SerializeObject(data);
                var i = refs.PostDatav_22(au, iv, dt);
                var jObj = JsonConvert.DeserializeObject<List<dynamic>>(i);
                billID = jObj[0].sid.ToString();
                string in2 = "v1/rpc";
                var Params_ = new{
                    pcustomersid = cusid,
                };
            var data2 = new
             {
                Params= Params_,
                MethodName = "CentralCopyCustomerFromCentral",
            };
            var datarc = new[]
            {
                data2
            };
            var i2 = refs.PostDatav_22(au, in2, JsonConvert.SerializeObject(datarc));
            /*string upctm = "v1/rest/document/"+ billID + "?filter=row_version,eq,"+refs.GetRowvwersioill(au, billID);
            var custup = new[]
              {
                    new{
                        bt_cuid=cusid
                        }
                };
            refs.PutDatav_22(au, upctm, JsonConvert.SerializeObject(custup));*/
            string items_ = JsonConvert.SerializeObject(itemadd);
            foreach (var _d in itemadd)
            {
                List<dynamic> dnmit = new List<dynamic>();
                dnmit.Add(_d);
                string jsonnew = JsonConvert.SerializeObject(dnmit);
                string pItem = "v1/rest/document/" + billID + "/item";
                var p_item = refs.PostDatav_22(au, pItem, jsonnew);
            }
            rowvs = refs.GetRowvwersioill(au, billID);
            string put1 = "v1/rest/document/"+billID+"?filter=row_version,eq,"+ rowvs;
            var dataend = new[]
              {
                    new{
                        row_version=rowvs,
                        fee_amt1=0,
                        fee_type1_sid="",
                        shipping_amt_manual=0,
                        ref_sale_sid=oldsid,
                        }
                };

            string e = JsonConvert.SerializeObject(dataend);
            var rs = refs.PutDatav_22(au, put1, e);
            string in2_ = "v1/rpc";
            var Params1_ = new
            {
                DocumentSid = billID,
            };
            var data2_ = new
            {
                Params = Params1_,
                MethodName = "PCPromoApplyManually",
            };
            var datarc_ = new[]
            {
                data2_
            };
            var i2_ = refs.PostDatav_22(au, in2_, JsonConvert.SerializeObject(datarc_));

            var tenderstr = "v1/rest/document/" + billID + "/tender";
            var ittender = new[]
            {
                new
                {
                   origin_application="RProPrismWeb",
                   tender_type=inven[0].doc_tender_type,
                   document_sid=billID,
                   given=inven[0].transaction_subtotal,
                   tender_name="UDF6",
                   card_type_name="SALE ONLINE"
                }
            };
            var tender = refs.PostDatav_22(au, tenderstr, JsonConvert.SerializeObject(ittender));

            rowvs = refs.GetRowvwersioill(au, billID);
            var pustr = "v1/rest/document/" + billID + "?filter=row_version,eq," + rowvs ;
            var x2end = new[]
            {
                 new
                 {
                 
                    status=4
                 }
              

            };
            string json2end = JsonConvert.SerializeObject(x2end);
            var put_end = refs.PutDatav_22(au, pustr, json2end);

            sqlmissdata miss = new sqlmissdata();
            List<string> cl = new List<string>() { "idnew", "idold", "status", "createdate", "cussid"};
            List<string> vl = new List<string>() { billID, oldsid, "success", DateTime.Now.ToString(), cusid };
            string ins = miss.insertsqlstr("returnprism", cl, vl);
            miss.runSQL(ins);

            string finddoc_ = "v1/rest/document?filter=sid,eq," + billID + "&cols=*";
            var o = JsonConvert.DeserializeObject<List<dynamic>>(refs.GetDataWebClient(au, finddoc));

             dynamic dycrm = new System.Dynamic.ExpandoObject();
             dycrm.BillNo = billID;
             dycrm.TotalAmount = o[0].sale_subtotal;
             dycrm.TotalPaymentAmount = o[0].transaction_total_amt;
             dycrm.Note = o[0].reason_description;
             dycrm.CardID = "";
             dycrm.CouponCode = "";
             dycrm.UsedMoneyExchange = 0;
             dycrm.PromoteCode = "";
             dycrm.StoreID = o[0].store_code;
             dycrm.CreateDate = o[0].created_datetime;
             dycrm.CreateBy = o[0].created_by;
             dycrm.PaymentTypeID = 1;
             dycrm.CustomerID = o[0].bt_cuid;
             dycrm.StatusID = 0;
             dycrm.BillDetail = itemcrm;
             dycrm.invoice_number = o[0].eft_invoice_number;
             updatebillIntoCrm(JsonConvert.SerializeObject(dycrm), cusid, billID);
             return billID;
        }
        [HttpGet("createinvoice")]
        public string createinvoice(string storeId, string note, string cusid, string items, string au,string codecoupon,string employee1_sid,string tender_type,string tender_name,string card_type_name, string note1, string note2, string note3, string note4, string note5)
        {
            common cmd = new common();
            RestFile refs = new RestFile();
            //au = cmd.GetAuth_Nonce();
            List<dynamic> items_ = JsonConvert.DeserializeObject<List<dynamic>>(items);
            //dynamic t1 = new System.Dynamic.ExpandoObject();
            //t1.sid = "544956795000160157";
            //t1.quantity = 2;
            //t1.price = 5000;

            //dynamic t2 = new System.Dynamic.ExpandoObject();
            //t2.sid = "544956795000162159";
            //t2.quantity = 3;
            //t2.price = 10000;
            //items_.Add(t1);
            //items_.Add(t2);
            //storeId = "544935116000173182";
            //cusid = "3951283478001553404";
            //string dt3 = JsonConvert.SerializeObject(items_);
            //au = cmd.GetAuth_Nonce();

            var crm = ctmCrmExit(cusid);
            if (crm == "[]")
            {
                string ctmstr = "v1/rest/customer?filter=sid,eq," + cusid + "&cols=*&page_no=1&page_size=10";
                var Getctm = refs.GetDataWebClient(au, ctmstr);
                List<dynamic> oobjctm = JsonConvert.DeserializeObject<List<dynamic>>(Getctm);
                var octm = oobjctm[0];
                var Gender = octm.title_sid;
                if (Gender == "544935122000172136")
                    Gender = "1";
                else
                    Gender = "0";
                string bdcrm = octm.udf1_date.ToString().Split(' ')[0];
                if (bdcrm != "" && bdcrm != null)
                {
                    try
                    {
                        bdcrm = bdcrm + "T00:00:00+07:00";
                    }
                    catch
                    {
                        bdcrm = "";
                    }
                }
                var ctm_ = new
                {
                    CustomerPrismID = cusid,
                    FirstName = cmd.Encode(octm.first_name.ToString()),
                    LastName = cmd.Encode(octm.last_name.ToString()),
                    Gender = Gender,
                    BirthDay = bdcrm,
                    PhoneNumber = octm.primary_phone_no,
                    Email = octm.email_address,
                    AddressNo = octm.primary_address_line_1,
                    AddressReceipts = octm.primary_address_line_1,
                    City = octm.primary_address_line_4,
                    WardPrism = octm.primary_address_line_3,
                    storename = octm.store_code,
                    storecode = octm.store_code,
                    IsDelete = 0
                };
                try
                {

                    sqlcommon sql = new sqlcommon();
                    List<string> cl1 = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
                };
                    List<string> vl1 = new List<string>() {
               "2",JsonConvert.SerializeObject(ctm_),"0","1","from invoice app",DateTime.Now.ToString(),cmd.GetIPAddress(),cusid
                };
                    string sqlinsert = sql.insertsqlstr("DataExchange", cl1, vl1);
                    sql.runSQL(sqlinsert);

                }
                catch
                {
                    cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), "ctm_createinvoice_" + Guid.NewGuid().ToString() + ".txt", "UpdateCustomerPrim?resp=" + JsonConvert.SerializeObject(ctm_) + "&sid=" + cusid);

                }
            }
            string iv = "v1/rest/document";
            string billID = "";
           // try { 
            var data = new[]
            {
                new{
                    origin_application = "RProPrismWeb",
                    store_uid= storeId,
                    original_store_uid=storeId,
                    notes_general=note,
                    bt_cuid=cusid,
                        //cashier_sid="544935314000109049",
                   employee1_sid=employee1_sid,
                   //demo
                   udf_string1=note1,
                   udf_string2=note2,
                   udf_string3=note3,
                   udf_string4=note4,
                   udf_string5=note5,
                }
            };
            string dt = JsonConvert.SerializeObject(data);

            var i = refs.PostDatav_22(au, iv, dt);
            var jObj = JsonConvert.DeserializeObject<List<dynamic>>(i);
            billID = jObj[0].sid.ToString();
            var itstr = "v1/rest/document/" + billID + "/item";
            int rowvs = 1;
          
            int total = 0;
            foreach (var o in items_)
            {
                List<dynamic> itemadd = new List<dynamic>();
                dynamic dy = new System.Dynamic.ExpandoObject();
                dy.origin_application = "RProPrismWeb";
                dy.invn_sbs_item_sid = o.sid.ToString();
                dy.fulfill_store_sid = storeId;
                dy.document_sid = billID;
                dy.quantity = int.Parse(o.quantity.ToString());
                dy.kit_type = 0;
                dy.item_type = 1;
               int pr_ = int.Parse(o.price.ToString());
               
                itemadd.Add(dy);
                rowvs++;
                string dtits = JsonConvert.SerializeObject(itemadd);
                var itps = refs.PostDatav_22(au, itstr, dtits);
                var jdt = JsonConvert.DeserializeObject<dynamic>(itps);
                if (o.pricesale.ToString() != "")
                {

                    pr_ = int.Parse(o.pricesale.ToString());
                    var newprice = new[]
                    {
                        new
                        {
                              manual_disc_value = int.Parse(o.pricesale.ToString()),
                              manual_disc_type = 0,
                        }
                    };
                    string link_ = jdt[0].link+ "?filter=row_version,eq,"+ jdt[0].row_version;
                    string cutlink = link_.Remove(0, 1);
                    var putprice = refs.PutDatav_22(au, cutlink, JsonConvert.SerializeObject(newprice));
                    //dy.manual_disc_value = int.Parse(o.pricesale.ToString());
                    //dy.manual_disc_type = 0;

                }
                total = total + (pr_ * int.Parse(o.quantity.ToString()));
            }

           // string dtit = JsonConvert.SerializeObject(itemadd);
           // var itp = refs.PostDatav_22(au, itstr, dtit);

                if (codecoupon != null && codecoupon != "")
                {
                    var datacoupon = new[]
                    {
                    new{
                        origin_application = "RProPrismWeb",
                        coupon_code= codecoupon,
                        in_or_out=1,
                        doc_sid=billID,

                    }
                };
                    string strcoupon = "v1/rest/document/" + billID + "/coupon";
                    refs.PostDatav_22(au, strcoupon, JsonConvert.SerializeObject(datacoupon));
                }
            var tenderstr = "v1/rest/document/" + billID + "/tender";
            if (total > 0)
            {
                var ittender = new[]
                {
                new
                {
                   origin_application="RProPrismWeb",
                   tender_type=int.Parse(tender_type),
                   document_sid=billID,
                   taken=total,
                   tender_name=tender_name,
                   card_type_name=card_type_name
                }
            };
                var tender = refs.PostDatav_22(au, tenderstr, JsonConvert.SerializeObject(ittender));

                rowvs++;
            }
            var rowvsion = "v1/rest/document?filter=sid,eq," + billID + "&cols=row_version";
            var grv = refs.GetDataWebClient(au, rowvsion);
            var vs = JsonConvert.DeserializeObject<dynamic>(grv);
            var pustr = "v1/rest/document/" + billID + "?filter=row_version,eq," + vs[0].row_version + "&cols=*";
            var dataend = new[]
           {
                new{
                   status= 4
                  }
            };
            string e = JsonConvert.SerializeObject(dataend);
            var rs = refs.PutDatav_22(au, pustr, e);
                sqlmissdata miss = new sqlmissdata();
                List<string> cl = new List<string>() { "idwoo", "contents", "status", "createdate","note", "idstore" , "cussid" };
                List<string> vl = new List<string>() { billID, items, "success", DateTime.Now.ToString(), note,storeId,cusid };
                string ins = miss.insertsqlstr("missprism", cl, vl);
                miss.runSQL(ins);
           /* }
           catch
            {
                sqlmissdata miss2 = new sqlmissdata();
                List<string> cl2 = new List<string>() { "idwoo", "contents", "status", "createdate", "note", "idstore", "cussid" };
                List<string> vl2 = new List<string>() { "0", items, "lan 1", DateTime.Now.ToString(), note, storeId, cusid };
                string ins2 = miss2.insertsqlstr("missprism", cl2, vl2);
                miss2.runSQL(ins2);
            }*/
            var dmstr = "v1/rest/document?filter=sid,eq," + billID + "&cols=*&page_no=1";
            var ok = refs.GetDataWebClient(au, dmstr);
            var lok = JsonConvert.DeserializeObject<List<dynamic>>(ok);
            List<dynamic> detailcrm_ = new List<dynamic>();
            foreach (var n in lok[0].items)
            {
                string link_ = n.link;
                link_ = link_.Remove(0, 1) + "?cols=*";
                string objit = refs.GetDataWebClient(au, link_);
                var lokit = JsonConvert.DeserializeObject<List<dynamic>>(objit);
                var m = lokit[0];
                string name = HttpUtility.HtmlEncode(m.item_description2);
                var o = new
                {
                    ProductID = m.scan_upc,
                    ProductName = name,
                    ProductSize = m.item_size,
                    ProductColor = HttpUtility.HtmlEncode(m.attribute),
                    NumberOfProduct = m.quantity,
                    PromotionUnitPrice = m.price,
                    UnitPrice = m.original_price,
                    TotalAmount = m.price * m.quantity,
                    StatusID = 0,
                };
                detailcrm_.Add(o);
            }
            var b2 = new {
                BillNo = billID,
                TotalAmount = lok[0].transaction_total_amt,
                TotalPaymentAmount = lok[0].transaction_total_amt,
                CardID = "",
                CouponCode = "[]",
                UsedMoneyExchange = 0,
                PromoteCode = "",
                StoreID = lok[0].store_code,
                CreateDate = lok[0].created_datetime,
                CreateBy = lok[0].created_by,
                PaymentTypeID = 1,
                CustomerID = lok[0].bt_cuid,
                StatusID = 0,
                BillDetail = detailcrm_,
                invoice_number = lok[0].eft_invoice_number

            };
            var gg = JsonConvert.SerializeObject(b2);
            try {
                var sqls = cmd.Stringcolumnsql("1", gg, billID, "");
                sqlcommon sql = new sqlcommon();
                sql.runSQL(sqls);

            }
            catch
            {
                sqlmissdata miss3 = new sqlmissdata();
                List<string> cl3 = new List<string>() { "idwoo", "contents", "status", "createdate" };
                List<string> vl3 = new List<string>() { billID, gg, "crm lan 1", DateTime.Now.ToString() };
                string ins3 = miss3.insertsqlstr("missprism", cl3, vl3);
                miss3.runSQL(ins3);
                
                //cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), billID + ".txt", "api/Api/updatebillIntoCrm?bill=" + gg);

            }

            return billID;
               
        }
        [HttpPost("posttransferslip")]
        public string posttransferslip([FromBody] List<JObject> table,string insbssid, string comment, string outstoresid, string instoresid, string au)
        {
            common cmd = new common();
            //string au = cmd.GetAuth_Nonce();
             List<dynamic> l_ = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(table));
            //JObject l_ = table;
            var datas = new
            {
                originapplication = "RProPrismWeb",
                status = 3,
                insbssid = insbssid,
                instoresid = instoresid,
                outstoresid = outstoresid,
                origstoresid = outstoresid,
                note = comment,
                //workstation = 22,
                //origstoreno = 238,
            };
            var x = new
            {
                data = new[] { datas }

            };
            string json2 = JsonConvert.SerializeObject(x);
            var u = "api/backoffice/transferslip";
            RestFile resf = new RestFile();
            var bill = resf.PostDatav_22(au, u, json2);
            var d = JsonConvert.DeserializeObject<dynamic>(bill);
            var v = d.data[0];
            var jObj = JsonConvert.DeserializeObject<Dictionary<string, object>>(v.ToString());
            var billID = jObj["sid"].ToString();
            ViewBag.id = billID;

            var data = new List<dynamic>();
            int rowversion = 2;
            foreach (var row in l_) // Duyệt từng dòng (DataRow) trong DataTable
            {
                dynamic itemr = new System.Dynamic.ExpandoObject();
                itemr.originapplication = "RProPrismWeb";
                itemr.qty = int.Parse(row["qty"].ToString());
                itemr.itemsid = row["itemsid"].ToString();
                itemr.slipsid = billID;
                data.Add(itemr);
                rowversion++;
            }

            string url = "api/backoffice/transferslip/" + billID + "/slipitem";
            var xq = new
            {
                data
            };
            string jsonw = JsonConvert.SerializeObject(xq);
            var bill2 = resf.PostDatav_22(au, url, jsonw);
            string put = "api/backoffice/transferslip/" + billID;
            var datap = new
            {

                rowversion = rowversion,
                status = 4
            };
            var xp = new
            {
                data = new[] { datap }

            };
            string jsonp = JsonConvert.SerializeObject(xp);
            var billp = resf.PutDatav_22(au, put, jsonp);
            return billID;
        }
        [HttpPost("transferslippost")]
        public string transferslippost(dynamic obj)
        {
            string table = obj.table.ToString();
            string insbssid = obj.insbssid;
            string comment = obj.comment.ToString();
            string outstoresid = obj.outstoresid.ToString();
            string instoresid = obj.instoresid.ToString();
            string au = obj.au.ToString();
            common cmd = new common();
          
            List<dynamic> l_ = JsonConvert.DeserializeObject<List<dynamic>>(Regex.Unescape(table));

            var datas = new
            {
                originapplication = "RProPrismWeb",
                status = 3,
                insbssid = insbssid,
                instoresid = instoresid,
                outstoresid = outstoresid,
                origstoresid = outstoresid,
                note = comment,
                //workstation = 22,
                //origstoreno = 238,
            };
            /*var createtranfer = new
            {
                originapplication = "RProPrismWeb",
                status = 3,
                insbssid = insbssid,
                instoresid = instoresid,
            };*/
            var x = new
            {
                data = new[] { datas }

            };
            string json2 = JsonConvert.SerializeObject(x);
            var u = "api/backoffice/transferslip";
            RestFile resf = new RestFile();

            var bill = resf.PostDatav_22(au, u, json2);

            var d = JsonConvert.DeserializeObject<dynamic>(bill);
            var v = d.data[0];
            var jObj = JsonConvert.DeserializeObject<Dictionary<string, object>>(v.ToString());
            var billID = jObj["sid"].ToString();

            ViewBag.id = billID;

            var data = new List<dynamic>();
            int rowversion = 2;
            foreach (var row in l_) // Duyệt từng dòng (DataRow) trong DataTable
            {
                dynamic itemr = new System.Dynamic.ExpandoObject();
                itemr.originapplication = "RProPrismWeb";
                itemr.qty = int.Parse(row["qty"].ToString());
                itemr.itemsid = row["itemsid"].ToString();
                itemr.slipsid = billID;
                data.Add(itemr);
                rowversion++;
            }

            string url = "api/backoffice/transferslip/" + billID + "/slipitem";
            var xq = new
            {
                data
            };
            string jsonw = JsonConvert.SerializeObject(xq);
            var bill2 = resf.PostDatav_22(au, url, jsonw);
            string put = "api/backoffice/transferslip/" + billID;
            var datap = new
            {

                rowversion = rowversion,
                status = 4
            };
            var xp = new
            {
                data = new[] { datap }

            };
            string jsonp = JsonConvert.SerializeObject(xp);
            var billp = resf.PutDatav_22(au, put, jsonp);
            return billID;
            
        }
            [HttpGet("transferslip")]
        public string transferslip(string table, string comment,string insbssid, string outstoresid, string instoresid, string au)
        {
        
            common cmd = new common();
             //au = "72F3188848914F9F8B028D2F005B576D";
            List<dynamic> l_ = JsonConvert.DeserializeObject<List<dynamic>>(Regex.Unescape(table));

            var datas = new
            {
                originapplication = "RProPrismWeb",
                status = 3,
                insbssid = insbssid,
                instoresid = instoresid,
                outstoresid = outstoresid,
                origstoresid = outstoresid,
                note = comment,
                //workstation = 22,
                //origstoreno = 238,
            };
            /*var createtranfer = new
            {
                originapplication = "RProPrismWeb",
                status = 3,
                insbssid = insbssid,
                instoresid = instoresid,
            };*/
            var x = new
            {
                data = new[] { datas }

            };
            string json2 = JsonConvert.SerializeObject(x);
            var u = "api/backoffice/transferslip";
            RestFile resf = new RestFile();
       
            var bill = resf.PostDatav_22(au, u, json2);
         
            var d = JsonConvert.DeserializeObject<dynamic>(bill);
            var v = d.data[0];
            var jObj = JsonConvert.DeserializeObject<Dictionary<string, object>>(v.ToString());
            var billID = jObj["sid"].ToString();
        
            ViewBag.id = billID;

            var data = new List<dynamic>();
            int rowversion = 2;
            foreach (var row in l_) // Duyệt từng dòng (DataRow) trong DataTable
            {
                dynamic itemr = new System.Dynamic.ExpandoObject();
                itemr.originapplication = "RProPrismWeb";
                itemr.qty = int.Parse(row["qty"].ToString());
                itemr.itemsid = row["itemsid"].ToString();
                itemr.slipsid = billID;
                data.Add(itemr);
                rowversion++;
            }

            string url = "api/backoffice/transferslip/" + billID + "/slipitem";
            var xq = new
            {
                data
            };
            string jsonw = JsonConvert.SerializeObject(xq);
            var bill2 = resf.PostDatav_22(au, url, jsonw);
            string put = "api/backoffice/transferslip/" + billID;
            var datap = new
            {

                rowversion = rowversion,
                status = 4
            };
            var xp = new
            {
                data = new[] { datap }

            };
            string jsonp = JsonConvert.SerializeObject(xp);
            var billp = resf.PutDatav_22(au, put, jsonp);
            return billID;
        }
        [HttpGet("editsinhnhat")]
        public string editsinhnhat(string ctms, string bd)
        {
            common cmd = new common();
            var key = Request.Headers["auth_session"].FirstOrDefault();
            key = cmd.GetAuth_Nonce();
            string b = cmd.convertdate(bd);
            dynamic ctm = JsonConvert.DeserializeObject<dynamic>(ctms);
            try
            {
                ctm.BirthDay = DateTime.Parse(b).ToString("yyyy-MM-dd") + "T00:00:00+07:00";
            }
            catch
            {
                ctm.BirthDay = "";
            }

            RestFile rest = new RestFile();

            dynamic a = new System.Dynamic.ExpandoObject();
            string sid = ctm.CustomerPrismID.ToString();
            a.udf1_date = DateTime.Parse(b);
            string uurl = "v1/rest/customer?filter=sid,EQ," + sid + "&cols=row_version&page_no=1&page_size=1";
            string vs = rest.GetDataWebClient(key, uurl);
            List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(vs);
            string vsion = dnm[0].row_version;
            string urlupdate = "v1/rest/customer/" + sid + "?filter=row_version,eq," + vsion + "";
            var up = rest.updateItem(key, urlupdate, a);
            try
            {

                sqlcommon sql = new sqlcommon();
                List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
                List<string> vl = new List<string>() {
               "2",JsonConvert.SerializeObject(ctm),"0","1","new",DateTime.Now.ToString(),cmd.GetIPAddress(),sid
            };
                string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
                sql.runSQL(sqlinsert);
                return "ok";
            }
            catch
            {
                cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), "ctm_birth" + Guid.NewGuid().ToString() + ".txt", "UpdateCustomerPrim?resp=" + JsonConvert.SerializeObject(ctm) + "&sid=" + sid);
                return "loi";
            }

            return "0";
        }
        [HttpGet("checkoutemployee")]
        public string checkoutemployee(string user, string pass)
        {
            common cmd = new common();
            cmd.userPrism = user;
            cmd.passPrism = pass;
            RestFile rest = new RestFile();
            string au = cmd.GetAuth_Nonce();
            if (au == "")
                return "wrong username or password.";
            string sit = "v1/rest/sit?ws=webclient";
            rest.GetDataWebClient(au, sit);
            string strsession = "v1/rest/session";
            var ss = rest.GetDataWebClient(au, strsession);
            var cout = JsonConvert.DeserializeObject<List<dynamic>>(ss);
            string urlch_in = "v1/rest/timeclock";
            var data = new
            {
                origin_application = "RProPrismWeb",
                workstation_sid = cout[0].workstationid.ToString(),
                subsidiary_sid = cout[0].subsidiarysid.ToString(),
                store_sid = cout[0].storesid.ToString(),
                employee_sid = cout[0].employeesid.ToString(),
                event_datetime = "",
                event_type = 1,
                notes = "",
            };
            try
            {
                var c_out = rest.PostItemPrismcheckin_out(au, urlch_in, data);
                return c_out;
            }
            catch
            {
                return "checking out";
            }
        }
        [HttpGet("getemployeeStore")]
        public string getemployeeStore(string store)
        {
            common cmd = new common();
            var key = Request.Headers["auth_session"].FirstOrDefault();
            key = cmd.GetAuth_Nonce();
            RestFile rest = new RestFile();
            string str = "api/common/employeelist?filter=storename,eq," + store + "&cols=*&page_no=1&page_size=500";
            var r = rest.GetData(key, str);
            List<dynamic> dd = JsonConvert.DeserializeObject<List<dynamic>>(r);
            List<dynamic> ddget = new List<dynamic>();
            List<dynamic> lId = dd.Select(s => s.sid.ToString()).ToList();
            string now = DateTime.Now.ToString("yyyy-MM-dd");
            //string urlch_in = "api/common/timeclock?filter=eventdatetime,GT," + now + "&page_size=500";
            //var datachin = rest.GetData(key, urlch_in);
            //List<dynamic> dnmcheck = JsonConvert.DeserializeObject<List<dynamic>>(datachin);
            //if (datachin == "[]")
            //    dd = new List<dynamic>();
            //else
            //{

            //    foreach (var k in dd)
            //    {
            //        var emplid = dnmcheck.FirstOrDefault(s => s.employeeid.ToString() == k.emplid.ToString());
            //        if (emplid != null)
            //            ddget.Add(k);
            //    }
            //}
            return JsonConvert.SerializeObject(dd.Select(s => s.sid.ToString()).ToList());
        }

        [HttpGet("checkinemployee")]
        public string checkinemployee(string user, string pass)
        {
            common cmd = new common();
            cmd.userPrism = user;
            cmd.passPrism = pass;
            RestFile rest = new RestFile();
            string au = cmd.GetAuth_Nonce();
            if (au == "")
                return "wrong username or password";
            string sit = "v1/rest/sit?ws=webclient";
            var sit_ = rest.GetDataWebClient(au, sit);
            string strsession = "v1/rest/session";
            var ss = rest.GetDataWebClient(au, strsession);
            // string active = "api/common/employee?cols=sid,fullname,username&sort=lastname,asc&filter=(useractive,eq,true)";
            // var ac=rest.GetData(au, active);
            var cout = JsonConvert.DeserializeObject<List<dynamic>>(ss);
            string urlch_in = "v1/rest/timeclock";
            var data = new
            {

                origin_application = "RProPrismWeb",
                workstation_sid = cout[0].workstationid.ToString(),
                subsidiary_sid = cout[0].subsidiarysid.ToString(),
                store_sid = cout[0].storesid.ToString(),
                employee_sid = cout[0].employeesid.ToString(),
                event_datetime = "",
                event_type = 0,
                notes = "",
            };
            try
            {
                var c_in = rest.PostItemPrismcheckin_out(au, urlch_in, data);
                return c_in;
            }
            catch
            {
                return "checking in";
            }
        }

        [HttpGet("updateemployee")]
        public string updateemployee(string infor, string au)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            dynamic a = JsonConvert.DeserializeObject<dynamic>(infor);
            //string au = HttpContext.Session.GetString("au");
            if (au == "null")
                au = cmd.GetAuth_Nonce();

            string sid = a.sid.ToString();
            string urlupdate = "api/common/employee/" + sid + "?cols=*,emplphone.*,empladdress.*,emplemail.*,employeeextend.*,employeestore.*,employeesubsidiary.*,usergroupuser.*";
            var ass = rest.GetData(au, urlupdate);
            List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(ass);
            string vsion = dnm[0].rowversion.ToString();
            var addtmp = dnm[0].empladdress;
            dynamic empladdress = new System.Dynamic.ExpandoObject();

            if (addtmp.Count > 0)
            {
                var addr = dnm[0].empladdress[0];
                var veraddress = addr.rowversion;
                empladdress.rowversion = veraddress;
                empladdress.sid = addr.sid;
                cmd.add_address(a, empladdress);
            }
            else
            {
                empladdress.active = 1;
                empladdress.addressallowcontact = true;
                cmd.add_address(a, empladdress);
            }
            var emplphone = new { phoneno = a.phone };
            var active = true;
            try
            {
                active = bool.Parse(a.active.ToString());
            }
            catch { }
            dynamic data = new System.Dynamic.ExpandoObject();
           // if (a.emplname != null)
               // data.emplname = a.emplname;
            data.rowversion = int.Parse(vsion);
            data.empladdress = new[] { empladdress };
            data.active = active;
            data.useractive = active;
            data.basestoresid = a.storesid;
            data.username = a.username;
            data.firstname = a.firstname;
            data.lastname = a.lastname;
            
            if (a.pass != null)
                data.password = a.pass;
            data.udf1string = a.udf1string;
            data.udf2string = a.udf2string;
            data.email = a.email;
            if (a.jobsid != null)
                data.jobsid = a.jobsid;
            if (a.phone.ToString() != "")
            {
                data.emplphone = new[] { emplphone };
            }

            var x = new
            {
                data = new[] { data }

            };
            string json2 = JsonConvert.SerializeObject(x);

            try
            {
                var rs = rest.PutDatav_22(au, "api/common/employee/" + sid + "?cols=*,emplphone.*,empladdress.*,emplemail.*,employeeextend.*,employeestore.*,employeesubsidiary.*,usergroupuser.*", json2);
                return "0";
            }
            catch
            {
                return "1";
            }
        }
        [HttpGet("createemployee")]
        public string createemployee(string infor, string au)
        {
            common cmd = new common();
            RestFile rest = new RestFile();

            dynamic a = JsonConvert.DeserializeObject<dynamic>(infor);
            var active = true;
            try
            {
                active = bool.Parse(a.active.ToString());
            }
            catch { }
            if (au == "null")
                au = cmd.GetAuth_Nonce();
            string dlc_us = "api/common/employee?filter=username,EQ," + a.username.ToString();
            var ck_ = rest.GetData(au, dlc_us);
            if (ck_ != "[]")
                return "Username đã tồn tại";
            dynamic empladdress = new System.Dynamic.ExpandoObject();
            empladdress.active = 1;
            empladdress.addressallowcontact = true;
            cmd.add_address(a, empladdress);
            var emplphone = new { phoneno = a.phone };
            var emplemail = new { emailaddress = a.email };
            var employeesubsidiary = new
            {
                originapplication = "PrismWeb",
                accessallstores = 1,
                sbssid = a.subsidiary
            };
            string hiredate = cmd.convertdate(a.hiredate.ToString());
            dynamic data = new System.Dynamic.ExpandoObject();
            data.originapplication = "RProPrismWeb";
            data.active = active;
            data.useractive = active;
            data.maxdiscperc = a.maxdisc;
            if (a.email.ToString() != "")
            {
                data.emplemail = new[] { emplemail };
            }
          
            data.employeesubsidiary = new[] { employeesubsidiary };
            if (a.address1.ToString() != "")
            {
                data.empladdress = new[] { empladdress };
            }
            if (a.phone.ToString() != "")
            {
                data.emplphone = new[] { emplphone };
            }
            data.username = a.username;
            data.password = a.pass;
            data.firstname = a.firstname;
            data.lastname = a.lastname;
            data.status = 1;
            data.hiredate = DateTime.Parse(hiredate);
            data.jobsid = a.jobsid;
            data.basestoresid = a.storesid;
            data.udf1string = a.udf1string;
            data.udf2string = a.udf2string;
            
            var x = new
            {
                data = new[] { data }

            };

            string json2 = JsonConvert.SerializeObject(x);

            //try
            //{
            var rs = rest.PostDatav_22(au, "api/common/employee", json2);
            return "0";
            //}
            //catch
            //{
            //    return "1";
            //}
        }
        // GET: api/<controller>
        [HttpGet]
        public string Get()
        {
            var rs = "wewe";
            return "callback('" + JsonConvert.SerializeObject(rs) + "')";
        }
        [HttpGet("ChangePass")]
        public string ChangePass(string au,string sid, string pass)
        {
            common cmd = new common();
            //au = GetAu();
            var str = "api/common/employee/"+ sid + "?action=changeuserpassword";
            var x = new
            {
                data = new[] {
                    new
                    {
                        NewPassword=pass,
                        ConfirmNewPassword=pass,
                        ResetPassword=true
                    }
                 

                }
            };
            RestFile refs = new RestFile();
            string json2 = JsonConvert.SerializeObject(x);
            var i = refs.PostDatav_22(au, str, json2);
            return "ok";
        }
        [HttpGet("Login")]
        public string Login(string user, string pass)
        {
            common cmd = new common();
            cmd.userPrism = user;
            cmd.passPrism = pass;
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            return GetAuth_Nonce;
        }
        [HttpGet("GetAu")]
        public string GetAu()
        {

            common cmd = new common();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            return GetAuth_Nonce;
        }
        [HttpGet("AllTempleatSms")]
        public List<Dictionary<string,object>> AllTempleatSms(string temId)
        {
            sqlcommon sql = new sqlcommon();
            var smss = sql.getdataSQLlive("CRM_SMSTemplates","");
            return smss;
        }
        [HttpGet("TempleatSms")]
        public string TempleatSms(string temId)
        {
            sqlcommon sql = new sqlcommon();
            string smss = sql.gettempleataSms(temId);
            return smss;
        }
        [HttpGet("sendsmsVietGuy")]
        public string sendsmsVietGuy(string temId, string phone, string code, string expiryDate)
        {
            sqlcommon sql = new sqlcommon();
            string smss = sql.gettempleataSms(temId);
            RestFile rest = new RestFile();
            var u = "NINOMAXX";
            var pwd = "8axfp";
            var from = "NINOMAXX";
            var sms = smss.Replace("{CouponCode}", code).Replace("{ExpiryDate}", expiryDate);
            //sms = "Ninomaxx gui tang a/c code NB_HELLO_BX 30% TAT CA SP, HD<2tr. HSD 30/11/2020 . Vui long giu sms thanh toan. 0948840808";
            var bid = Guid.NewGuid().ToString();
            string url = "https://cloudsms.vietguys.biz:4438/api/index.php?u=" + u + "&pwd=" + pwd + "&from=" + from + "&phone=" + phone + "&sms=" + sms + "&bid=" + bid;
            WebClient webClient = new WebClient();
            var obj = webClient.DownloadString(url);
            return obj;
        }

        [HttpGet("sendsmsCrm")]
        public string sendsmsCrm(string TemplateID, string phone, string ctmId, string couponcode)
        {
            //string act = UpcodeCoupon(couponcode,"0","1","");
            //if (act != "0")
            //    return "-1";

            string tokenCrm = "basic TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=";
            RestFile rest = new RestFile();
            int tem = 1;
            if (TemplateID != "HT")
                tem = 2;
            storePrism store = new storePrism();
            var ss = store.GET_COUPON_INFO(couponcode);
            string JSONresult = JsonConvert.SerializeObject(ss);
            var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);

            if (JSONresults.Count > 0)
            {
                string CouponType = "HT";
                if (JSONresults[0].PREFIX.ToString() == "SN_")
                    CouponType = "SN";
                string ValueType = "%";
                if (int.Parse(JSONresults[0].DISCOUNT_AMOUNT.ToString()) > 200)
                    ValueType = "$";
                dynamic CouponCode_ = new
                {
                    CouponPrismID = ctmId,
                    CouponCode = couponcode,
                    CouponType = CouponType,
                    CouponName = JSONresults[0].DESCRIPTION.ToString(),
                    CouponValue = JSONresults[0].DISCOUNT_AMOUNT.ToString(),
                    ValueType = ValueType,
                    CreateDate = DateTime.Now.ToString("yyyy-MM-dd"),
                };
                dynamic bodys = new
                {
                    CustomerPrism = ctmId,
                    PhoneNumber = phone,
                    Content = "",
                    CreateBy = DateTime.Now.ToString(),
                    CouponCode = CouponCode_,
                    TemplateID = tem
                };
                var j = JsonConvert.SerializeObject(bodys);
                string url = "Api/SendSMS";
                string lg = rest.PostDataCrm(url, bodys, tokenCrm);
                return lg;
            }
            return "-1";
        }
        [HttpGet("exportTranfer")]
        public FileResult exportTranfer(string sid)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            string address = "api/backoffice/receiving/" + sid + "/recvitem/?cols=*";
            string pro = rest.GetData(GetAuth_Nonce, address);
            DataTable table = new DataTable("Detail");
            table.Columns.Add("UPC", typeof(string));
            table.Columns.Add("Diễn giải 1", typeof(string));
            table.Columns.Add("Diễn giải 2", typeof(string));
            table.Columns.Add("Kích thước", typeof(string));
            table.Columns.Add("Thuộc tính", typeof(string));
            table.Columns.Add("Số lượng", typeof(float));
            table.Columns.Add("Giá vốn", typeof(float));
            table.Columns.Add("Giá bán", typeof(float));
            table.Columns.Add("Được tạo bởi", typeof(string));
            List<dynamic> dt2 = JsonConvert.DeserializeObject<List<dynamic>>(pro);
            foreach (var row in dt2)
            {
                table.Rows.Add(
                         row.upc.ToString(),
                         row.description1.ToString(),
                         row.description2.ToString(),
                         row.size.ToString(),
                         row.attr.ToString(),
                          row.qty.ToString(),
                          "0",
                            row.price.ToString(),
                             row.createdby.ToString()
                         );


            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "chitiet.xlsx");
                }
            }
            //return pro;
        }
        [HttpGet("PostInvoid")]
        public string PostInvoid(string data)
        {

            return "";
        }
        [HttpGet("exportInvoid")]
        public async Task<FileResult> exportInvoid(string after, string before)
        {

            woocomerge woo = new woocomerge();
            woo.getaddress();
            DataTable table = new DataTable("Invoid");

            table.Columns.Add("ngày", typeof(string));
            table.Columns.Add("ID đơn hàng", typeof(string));
            table.Columns.Add("số đt khách", typeof(string));
            table.Columns.Add("họ", typeof(string));
            table.Columns.Add("tên", typeof(string));
            table.Columns.Add("giới tính", typeof(string));

            table.Columns.Add("địa chỉ", typeof(string));
            table.Columns.Add("xã phường", typeof(string));
            table.Columns.Add("quận huyện", typeof(string));
            table.Columns.Add("thành phố", typeof(string));
            table.Columns.Add("id xã phường", typeof(string));
            table.Columns.Add("id quận huyện", typeof(string));
            table.Columns.Add("id thành phố", typeof(string));
            table.Columns.Add("UPC", typeof(string));
            table.Columns.Add("giá", typeof(float));
            table.Columns.Add("số lượng", typeof(float));
            table.Columns.Add("thành tiền", typeof(float));
            table.Columns.Add("phí ship title", typeof(string));
            table.Columns.Add("phí ship value", typeof(float));
            table.Columns.Add("loại thanh toán", typeof(string));
            table.Columns.Add("Ghi chú", typeof(string));
            table.Columns.Add("trạng thái", typeof(string));

            DateTime f = woo.cvdate(after);
            string fr = f.ToString("yyyy-MM-ddT00:00:00");
            DateTime t = woo.cvdate(before).AddDays(1);
            string tt = t.ToString("yyyy-MM-ddT00:00:00");

            List<Order> orders = new List<Order>();
            int pcolor = 1;
            do
            {
                var obj = await woo.getOrder(pcolor.ToString(), fr, tt);
                if (obj.Count <= 0)
                {
                    pcolor = -1;
                    break;
                }
                else
                {

                    foreach (var i in obj)
                    {

                        orders.Add(i);

                    }

                    pcolor++;
                }
            } while (pcolor > 0);
            int stt = 1;
            foreach (var j in orders)
            {
                var item = j.line_items;
                string gener = "";
                string state = j.shipping.state;
                string ward = j.shipping.city;
                string p_x = j.shipping.address_2;
                string idcity = state; string idward = ward; string idpx = p_x;
                try
                {
                    idcity = woo.city.FirstOrDefault(s => s["ten"].ToString().Contains(state))["ma"].ToString();
                    idward = woo.distr.FirstOrDefault(s => s["ten"].ToString().Contains(ward))["ma"].ToString();
                    idpx = woo.phuong_xa.FirstOrDefault(s => s["ten"].ToString().Contains(p_x))["ma"].ToString();
                }
                catch
                {

                }
                try
                {
                    gener = j.meta_data.FirstOrDefault(s => s.key == "_billing_gender").value.ToString();
                }
                catch { }
                foreach (var row in item)
                {
                    string loaitt = "";
                    float ship = 0;
                    try
                    {
                        loaitt = j.shipping_lines[0].method_title;
                        ship = float.Parse(j.shipping_lines[0].total.ToString());
                    }
                    catch { }
                    table.Rows.Add(

                         j.date_created.ToString(),
                         j.id.ToString(),
                         j.billing.phone.ToString(),
                           j.billing.last_name,
                         j.billing.first_name,
                         gener,
                         j.shipping.address_1,
                         j.shipping.address_2,
                         j.shipping.city,
                         j.shipping.state,
                         idpx,
                       idward,
                       idcity,
                          row.sku,
                          row.price,
                          row.quantity,
                          row.total,
                          loaitt,
                          ship,
                          j.payment_method_title,
                          j.customer_note,
                          j.status.ToString()
                         );

                }

                stt++;
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Invoid.xlsx");
                }
            }
            //return pro;
        }
        [HttpGet("sendsms")]
        public string sendsms(string phone)
        {
            Pushnotifation p = new Pushnotifation();
            p.pushnotifationtodevice("", "4882", "4882", phone, "Mã hoàn tiền là HT001", "Announcements", "test", "test");
            return "";
        }
        [HttpGet("SearchWootest")]
        public async Task<string> SearchWootest(string page)
        {
            woocomerge woo = new woocomerge();
            var a = await woo.allProducts(page);
            return JsonConvert.SerializeObject(a);
        }
        public async Task<bool> InsertSysWeb(upproducweb datas, string web)
        {
            common cmd = new common();
            SysnWeb woo = new SysnWeb(web);
            //woocomerge woo = new woocomerge();
            List<ProductTag> tagss_ = new List<ProductTag>();
            if (datas.tast != "")
            {
                tagss_ = await woo.createlistTagId(datas.tast);
            }
            List<dynamic> d = JsonConvert.DeserializeObject<List<dynamic>>(datas.data);
            string[] des = datas.mtk.Split("-");
            string[] cates = new string[] { };
            string ddes = des[des.Length - 1];
            //string namews= datas.data
            var s = await woo.searchProduct(ddes);
            int? menuorder =-1;
            int? ided = -1;
            var Attribute = await woo.listAttrwoo();
            string idcolor = Attribute.FirstOrDefault(x => x.name.ToLower() == "color").id.ToString();
            string idsize = Attribute.FirstOrDefault(x => x.name.ToLower() == "size").id.ToString();
            if (s != null)
            {
                menuorder=s.menu_order;
                var dele = await woo.deleteproduct(s.id.ToString());
                if (dele == "0")
                {
                    ided = -1;
                    s = null;
                }
            }
                cates = datas.cate_.Split(",");

            List<ProductTagLine> tags_ = new List<ProductTagLine>();
            foreach (var h in tagss_)
            {
                ProductTagLine tg = new ProductTagLine();
                tg.id = h.id;
                tg.name = h.name;
                tg.slug = h.slug;
                tags_.Add(tg);
            }
            if (d.Count <= 0 && s != null)
            {

                await woo.UpCateproduct(cates.ToList(), s, tags_);
                return true;
            }
            foreach (var i in d)
            {

                string n = i.name.ToString();
                var a = i.attr;
                List<string> colors = new List<string>();
                List<string> sizes = new List<string>();
                foreach (var j in a)
                {
                    j.color = cmd.replaceChar(j.color.ToString());
                    colors.Add(j.color.ToString());
                    sizes.Add(j.size.ToString());
                }
                List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
                Product p = new Product();
                if (s == null)
                {
                    attrs =await woo.listAttrID(colors, sizes);
                    var cates_ = await woo.catelogy();
                    List<string> cate = new List<string>();
                    foreach (var c in cates)
                    {
                        cate.Add(c);
                        //cate.Add(cates_.FirstOrDefault(x => x.name.ToLower() == c.ToLower()).id.ToString());
                    }
                    List<ProductMeta> meta = woo.listMetaId(datas.chatlieu.ToString());
                    p = await woo.addproduct(attrs,datas.namewebsite, "", ddes, cate, int.Parse(menuorder.ToString()), i.desc.ToString(), tags_, meta);
                }
                else
                {
                    p = s;
                    p.name = datas.namewebsite;

                    List<ProductAttributeLine> attrss = p.attributes;
                    if (attrss.Count > 0)
                    {
                        foreach (var k in attrss)
                        {
                            var na = k.name;
                            if (na == "Color")
                            {
                                foreach (var g in k.options)
                                {
                                    colors.Add(g);
                                }
                            }
                            else if (na == "Size")
                            {
                                foreach (var g in k.options)
                                {
                                    sizes.Add(g);
                                }
                            }
                        }

                    }
                    attrs =await woo.listAttrID(colors, sizes);
                    await woo.UpAttrproduct(attrs, cates.ToList(), p);
                }
                foreach (var m in a)
                {

                    await woo.inputProductvariable(p, m.invent.ToString(), m.price.ToString(), m.upc.ToString(), m.color.ToString(), m.size.ToString(), m.pricedown.ToString(),idcolor,idsize);
                }

            }
            return true;
        }
        [HttpPost("InsertWoo")]
        public async Task<bool> InsertWoo(upproducweb datas)
        {
            if (datas.webnino)
                await InsertSysWeb(datas,"nino");
            if (datas.webnm)
                await InsertSysWeb(datas, "nm");
            if (datas.weblaginews)
                await InsertSysWeb(datas, "laginew");
            return true;
        }
        [HttpGet("woosavecolor")]
        public string woosavecolor(string id, string vl)
        {
            ProductAttributeTerm a = new ProductAttributeTerm();
            a.id = int.Parse(id);
            a.description = "vl";
            return "we";
        }
        [HttpPost("InsertWooTest")]
        public async Task<bool> InsertWooTest(upproducweb datas)
        {
            common cmd = new common();
            woocomerge woo = new woocomerge();
            List<ProductTag> tagss_ = new List<ProductTag>();
            if (datas.tast != "")
            {
                tagss_ = await woo.createlistTagId(datas.tast);
            }
            List<dynamic> d = JsonConvert.DeserializeObject<List<dynamic>>(datas.data);
            string[] des = datas.mtk.Split("-");
            string[] cates = new string[] { };
            string ddes = des[des.Length - 1];
            //string namews= datas.data
            var s = await woo.searchProduct(ddes);

            int? ided = -1;
            if (s != null)
            {
                var dele = await woo.deleteproduct(s.id.ToString());
                if (dele == "0")
                {
                    ided = -1;
                    s = null;
                }

                //if (s.status != "publish")
                //{
                //    var dele = await woo.deleteproduct(s.id.ToString());
                //    if (dele == "0")
                //    {
                //        ided = -1;
                //        s = null;
                //    }
                //}
                //else
                //{
                //    ided = s.id;
                //}
            }
            cates = datas.cate_.Split(",");

            List<ProductTagLine> tags_ = new List<ProductTagLine>();
            foreach (var h in tagss_)
            {
                ProductTagLine tg = new ProductTagLine();
                tg.id = h.id;
                tg.name = h.name;
                tg.slug = h.slug;
                tags_.Add(tg);
            }
            if (d.Count <= 0 && s != null)
            {

                await woo.UpCateproduct(cates.ToList(), s, tags_);
                return true;
            }
            foreach (var i in d)
            {

                string n = i.name.ToString();
                var a = i.attr;
                List<string> colors = new List<string>();
                List<string> sizes = new List<string>();
                foreach (var j in a)
                {
                    j.color = cmd.replaceChar(j.color.ToString());
                    colors.Add(j.color.ToString());
                    sizes.Add(j.size.ToString());
                }
                List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
                Product p = new Product();
                if (s == null)
                {
                    attrs = woo.listAttrID(colors, sizes);
                    p = await woo.addproduct(attrs, n, "", ddes, cates.ToList(), int.Parse(ided.ToString()), i.desc.ToString(), tags_);
                }
                else
                {
                    p = s;
                    List<ProductAttributeLine> attrss = p.attributes;
                    if (attrss.Count > 0)
                    {
                        foreach (var k in attrss)
                        {
                            var na = k.name;
                            if (na == "Color")
                            {
                                foreach (var g in k.options)
                                {
                                    colors.Add(g);
                                }
                            }
                            else if (na == "Size")
                            {
                                foreach (var g in k.options)
                                {
                                    sizes.Add(g);
                                }
                            }
                        }

                    }
                    attrs = woo.listAttrID(colors, sizes);
                    await woo.UpAttrproduct(attrs, cates.ToList(), p);
                }
                foreach (var m in a)
                {

                    await woo.inputProductvariable(p, m.price.ToString(), m.upc.ToString(), m.color.ToString(), m.size.ToString(), m.pricedown.ToString());
                }

            }
            return true;
        }
        [HttpGet("Printdata")]
        public bool Printdata()
        {

            PrintDocument pd = new PrintDocument();
            pd.PrinterSettings.PrinterName = "Microsoft Print to PDF";
            pd.PrintPage += new PrintPageEventHandler
                  (pd_PrintPage);
            pd.Print();
            return true;
        }
        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            Single yPos = 0;
            Single leftMargin = e.MarginBounds.Left;
            Single topMargin = e.MarginBounds.Top;
            // Image img = Image.FromFile("logo.bmp");
            Rectangle logo = new Rectangle(40, 40, 50, 50);
            using (Font printFont = new Font("Arial", 10.0f))
            {
                //e.Graphics.DrawImage(img, logo);
                e.Graphics.DrawString("Testing!", printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
            }
        }
        [HttpGet("checkPhone")]
        public bool checkPhone(string p)
        {
            // var GetAuth_Nonce = Request.Headers["auth_session"].FirstOrDefault();
            common cmd = new common();
            RestFile rest = new RestFile();
            //if(GetAuth_Nonce==null)
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            string address = "v1/rest/customer?filter=primary_phone_no,EQ," + p + "&cols=*&page_no=1&page_size=1";
            string pro = rest.GetDataWebClient(GetAuth_Nonce, address);
            if (pro != "[]")
                return false;
            return true;
        }

        [HttpGet("checkPhonectm")]
        public string checkPhonectm(string p)
        {

            common cmd = new common();
            RestFile rest = new RestFile();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            string address = "v1/rest/customer?filter=primary_phone_no,EQ," + p + "&cols=*&page_no=1&page_size=1";
            string pro = rest.GetDataWebClient(GetAuth_Nonce, address);

            return pro;
        }
        [HttpGet("testcreate")]
        public string testcreate(string bill)
        {
            common cmd = new common();
            var b = JsonConvert.DeserializeObject<dynamic>(bill);
            cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), b.BillNo.ToString() + ".txt", bill);
            return "";
        }

        [HttpGet("updatebillIntoCrm")]
        public string updatebillIntoCrm(string bill, string newctm, string billid)
        {

            common cmd = new common();
            RestFile rest = new RestFile();
            sqlmissdata sqlmiss = new sqlmissdata();
            if (billid == "" || billid == null)
                billid = Guid.NewGuid().ToString();
            List<string> clcrm = new List<string>() { "cusid", "contents", "createdate", "status", "type", "notes" };
            List<string> vlcrm = new List<string>() { newctm, bill, DateTime.Now.ToString(), "error", "bill", "loi ctm" };
            if (newctm == "-1")
            {
               
                string miss = sqlmiss.insertsqlstr("misscrm", clcrm, vlcrm);
                sqlmiss.runSQL(miss);
                return "loi";
            }
            try
            {

                var b = JsonConvert.DeserializeObject<dynamic>(bill);

                var cp = b.CouponCode.ToString();
                var a = b.CreateDate.ToString().Replace(".000", "");
                var d = a.Replace(" ", "+");
                if (b.CustomerID.ToString() != "")
                {
                    var core = GetScoreCrm(b.CustomerID.ToString(), "");
                    if (core == "-1")
                    {
                        vlcrm[vlcrm.Count-1] = "ko tồn tại ctm";
                        //cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), billid + ".txt", "api/Api/updatebillIntoCrm?bill=" + bill);
                        string miss = sqlmiss.insertsqlstr("misscrm", clcrm, vlcrm);
                        sqlmiss.runSQL(miss);
                        return "ko tồn tại ctm";
                    }
                }
                List<dynamic> lcpon = new List<dynamic>();

                b.CreateDate = d;

                //if (cp.ToString() != "[]")
                //{
                storePrism store = new storePrism();
                var store_iv = store.GET_COUPON_BY_INVOICE(b.BillNo.ToString());
                List<dynamic> lcoups = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(store_iv));
                if (lcoups.Count > 0) {
                    foreach (var cc in lcoups)
                    {
                        var type = cc.TYPE_DISCOUNT.ToString();
                        if (type.ToLower() == "phần trăm")
                            type = "%";
                        else
                            type = "$";
                        var CouponType = "HT";
                        string desc = cc.DESCRIPTION.ToString().ToLower();
                        if (desc.Contains("sinh nhật"))
                            CouponType = "SN";
                        else if (desc.Contains("giảm giá"))
                            CouponType = "GG";
                        dynamic c = new
                        {
                            CouponPrismID = cc.COUPON_SID.ToString(),
                            CouponCode = cc.COUPON_CODE.ToString(),
                            CouponType = CouponType,
                            CouponName = cc.DESCRIPTION.ToString(),
                            CouponValue = cc.DISCOUNT_AMOUNT.ToString(),
                            ValueType = type,
                            CreateDate = DateTime.Now.ToString(),
                        };
                        lcpon.Add(c);
                    }
                }
                //}

                //var prcoup = int.Parse(b.TotalPaymentAmount.ToString()) + sumcoup;
                b.CouponCode = JsonConvert.SerializeObject(lcpon);
                //int sumreturn = 0;
                //int sumb_ = 0;
                // sumreturn= dt_.Where(s=>s.StatusID.ToString()=="1").Sum(s => int.Parse(s.TotalAmount.ToString()));
                //sumb_ = dt_.Where(s => s.StatusID.ToString() == "1").Sum(s => int.Parse(s.TotalAmount.ToString()));
                foreach (var i in b.BillDetail)
                {
                    i.ProductName = HttpUtility.HtmlEncode(i.ProductName.ToString());
                    i.ProductColor = HttpUtility.HtmlEncode(i.ProductColor.ToString());
                }
                string resp = JsonConvert.SerializeObject(b);
                string sqls = "";
                sqls = cmd.Stringcolumnsql("1", resp, b.BillNo.ToString(), "");
                sqlcommon sql = new sqlcommon();
                sql.runSQL(sqls);
                return "ok";
            }
            catch
            {
                vlcrm[vlcrm.Count - 1] = "loi bill";
                string miss = sqlmiss.insertsqlstr("misscrm", clcrm, vlcrm);
                sqlmiss.runSQL(miss);
                //cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), billid + ".txt", "api/Api/updatebillIntoCrm?bill=" + bill);
                return "loi";
            }


        }
        [HttpGet("updateaddressctm")]
        public string updateaddressctm(string data)
        {

            common cmd = new common();
            RestFile rest = new RestFile();
            dynamic ctm = JsonConvert.DeserializeObject<dynamic>(data);
            dynamic item = new System.Dynamic.ExpandoObject();
            item.customer_sid = ctm.id.ToString();
            item.address_line_2 = ctm.Pxa.ToString();
            item.address_line_3 = ctm.Qhuyen.ToString();
            item.address_line_4 = ctm.thanhpho.ToString();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            string url = "v1/rest/customer/" + ctm.id.ToString() + "/address?cols=sid,row_version&page_no=1&page_size=10";
            string version = rest.GetDataWebClient(GetAuth_Nonce, url);
            List<dynamic> vsl = JsonConvert.DeserializeObject<List<dynamic>>(version);
            string vs = vsl[0].row_version.ToString();
            string url2 = "v1/rest/customer/" + ctm.id.ToString() + "/address/" + vsl[0].sid.ToString() + "?filter=row_version,eq," + vs;
            string u = rest.updateItem(GetAuth_Nonce, url2, item);
            return u;
        }
        [HttpGet("listStore")]
        public List<dynamic> listStore()
        {

            common cmd = new common();
            var store = cmd.stores();
            return store;
        }
        [HttpGet("GetcouponSet")]
        public string GetcouponSet(string idpromo)
        {

            common cmd = new common();
            string key = cmd.GetAuth_Nonce();
            RestFile rest = new RestFile();
            string aCoup = "api/common/couponset?cols=*&filter=setid,EQ," + idpromo + "";
            string rsc = rest.GetData(key, aCoup);
            List<dynamic> lc = JsonConvert.DeserializeObject<List<dynamic>>(rsc);
            string sid = lc[0].sid.ToString();
            string address = "api/common/couponsetcoupon?cols=*&filter=couponsetsid,EQ," + sid.Trim() + "&sort=issuedstatus,desc&sort=startdate,desc";
            string rs = rest.GetData(key, address);
            return rs;
        }
        [HttpGet("UpcodeCouponcustom")]
        public string UpcodeCouponcustom(string code, string start, string end, string token)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            string key = cmd.GetAuth_Nonce();
            if (start == null)
            {
                start = "0";
                end = "1";
            }
            string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code.ToUpper() + "&cols=*&page_no=1&page_size=1";
            var a = rest.GetData(key, urlupdate);
            List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
            string vsion = dnm[0].rowversion.ToString();
            if (vsion != "1")
                return "-1";
            string sid = dnm[0].sid.ToString();
            dynamic data = new System.Dynamic.ExpandoObject();
            data.rowversion = 1;
            data.issuedstatus = 1;
            data.startdate = start;// "2020-02-29T11:42:47.000Z";
            data.enddate = end;

            var x = new
            {
                data = new[] { data }
            };

            string json2 = JsonConvert.SerializeObject(x);
            string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
            string u = rest.updateItemcoupon(key, urlupdatei, json2);
            return "0";
        }
        [HttpGet("UpcodeCouponCELEB")]
        public string UpcodeCouponCELEB(string code, string start, string end, string f)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            string key = cmd.GetAuth_Nonce();
            if (start == null)
            {
                start = "0";
                end = "1";
            }
            string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code.ToUpper() + "&cols=*&page_no=1&page_size=1";
            var a = rest.GetData(key, urlupdate);
            List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
            string vsion = dnm[0].rowversion.ToString();
            //if (vsion != "1")
            //    return "-1";
            string sid = dnm[0].sid.ToString();
            dynamic data = new System.Dynamic.ExpandoObject();
            DateTime now = DateTime.Now;
            data.rowversion = int.Parse(vsion);
            data.issuedstatus = 1;
            data.startdate = cmd.dateAsString(now.AddDays(int.Parse(start)));// "2020-02-29T11:42:47.000Z";
            data.enddate = cmd.EnddateAsString(now.AddDays(int.Parse(end)));
            if (f == "vx")
                data.modifieddatetime = cmd.dateAsString(now);
            var x = new
            {
                data = new[] { data }
            };

            string json2 = JsonConvert.SerializeObject(x);
            string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
            string u = rest.updateItemcoupon(key, urlupdatei, json2);
            return "0";
        }
        [HttpGet("UpcodeCoupon")]
        public string UpcodeCoupon(string code, string start, string end, string f)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            string key = cmd.GetAuth_Nonce();
            if (start == null)
            {
                start = "0";
                end = "1";
            }
            string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code.ToUpper() + "&cols=*&page_no=1&page_size=1";
            var a = rest.GetData(key, urlupdate);
            List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
            string vsion = dnm[0].rowversion.ToString();
            //if (vsion != "1")
            //    return "-1";
            string sid = dnm[0].sid.ToString();
            dynamic data = new System.Dynamic.ExpandoObject();
            DateTime now = DateTime.Now;
            data.rowversion = int.Parse(vsion);
            data.issuedstatus = 1;
            if (start != "0")
                data.startdate = cmd.dateAsString(now.AddDays(int.Parse(start)));// "2020-02-29T11:42:47.000Z";
            data.enddate = cmd.EnddateAsString(now.AddDays(int.Parse(end)));
            if (f == "vx")
                data.modifieddatetime = cmd.dateAsString(now);
            var x = new
            {
                data = new[] { data }
            };

            string json2 = JsonConvert.SerializeObject(x);
            string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
            string u = rest.updateItemcoupon(key, urlupdatei, json2);
            return "0";
        }
        [HttpGet("UpCouponSetcoupon")]
        public string UpCouponSetcoupon(string sid, string rowversion)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            string key = cmd.GetAuth_Nonce();

            //string urlupdate = "api/common/couponsetcoupon?filter=sid,EQ," + sid + "&cols=*&page_no=1&page_size=1";
            //var a = rest.GetData(key, urlupdate);
            //List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
            //string vsion = dnm[0].rowversion.ToString();
            dynamic data = new System.Dynamic.ExpandoObject();
            DateTime now = DateTime.Now;
            data.rowversion = 1;
            data.issuedstatus = 1;
            data.startdate = cmd.dateAsString(now);// "2020-02-29T11:42:47.000Z";
            data.enddate = cmd.EnddateAsString(now.AddDays(1));
            var x = new
            {
                data = new[] { data }
            };

            string json2 = JsonConvert.SerializeObject(x);


            string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
            string u = rest.updateItemcoupon(key, urlupdatei, json2);
            return json2;
        }
        public string woopostinvoidprism2(Order pr,string tokent)
        {
            RestFile rest = new RestFile();
            woocomerge woo = new woocomerge();
            woo.getaddress();
            string coupon = "";
            var items = pr.line_items;

            List<dynamic> _items = new List<dynamic>();
            var tk = tokent;
            foreach (var i in items)
            {
                var price = i.subtotal / i.quantity;
                var disc_ = (i.subtotal - (i.price * i.quantity)) / i.quantity;
                dynamic _i = new System.Dynamic.ExpandoObject();
                _i.upc = i.sku.ToString();
                _i.priceRetail = price.ToString();
                _i.qty = i.quantity.ToString();
                _i.discount =Math.Floor(double.Parse(disc_.ToString())).ToString();
                _i.itemSubtotal = i.total.ToString();
                _items.Add(_i);

            }
            string tt = "";
            string qh = "";
            string px = "";
            var custTinhSid = woo.city.FirstOrDefault(s => s["ten"].ToString().Contains(pr.billing.state.ToString()));
            var custHuyenSid = woo.distr.FirstOrDefault(s => s["ten"].ToString().Contains(pr.billing.city.ToString()));
            var custXaSid = woo.phuong_xa.FirstOrDefault(s => s["ten"].ToString().Contains(pr.billing.address_2.ToString()));
            if (custTinhSid != null)
                tt = custTinhSid["ma"].ToString();
            if (custHuyenSid != null)
                qh = custHuyenSid["ma"].ToString();
            if (custXaSid != null)
                px = custXaSid["ma"].ToString();
            decimal? ShippingFee = pr.shipping_lines.Sum(s => s.total);
            //var ctm =woo.getcustomer(pr.customer_id.ToString());
            if (pr.coupon_lines.Count > 0)
                coupon = pr.coupon_lines[0].code;
            dynamic datapris =
            new
            {
                wooNo = pr.id.ToString(),
                couponCode = coupon.ToUpper(),
                statusMainSid = "1",
                statusSubSid = "2",
                custPhone = pr.billing.phone.ToString(),
                custFirstName = pr.billing.first_name.ToString(),
                custLastName = pr.billing.last_name.ToString(),
                custAddress = pr.billing.address_1.ToString(),
                custTinhSid = tt,
                custHuyenSid = qh,
                custXaSid = px,
                custTitle = "",
                custDayOfBirth = "",
                custEmail = pr.billing.email.ToString(),
                custNote = pr.customer_note.ToString(),
                vfcShippingFee = ShippingFee.ToString(),
                extraFee = "0",
                fromSource = "WOO",
                documentItems = _items
            };
            var ip = rest.PostDataVFCApi("saleonlinedocument", datapris, tk);
            return ip;
        }
        [HttpGet("Upwoopostinvoidprism")]
        public async Task<string> Upwoopostinvoidprism(string idorder,string status)
        {
                common cmd = new common();
                wooCart woo = new wooCart();
                sqlmissdata miss = new sqlmissdata();
            /*var c_ = miss.checkduplicate(idorder);
            if (c_ != "0")
                return "-1";*/
            if (status != "wc-da-xac-nhan" && status != "wc-completed")
                return "-20";
            var order = await woo.getorder(idorder);
                
                    
                
                var rs =await woopostinvoidprism(order);
                string o = "update sussess";
                if (rs == "-1")
                    o = "loi 2";
                string now = DateTime.Now.ToString();
                string u = "update missdata set status='" + o + "',lastupdate='"+ now + "',sid='"+ rs + "' where idwoo='" + idorder+"'";
                miss.runSQL(u);
                return "new:"+rs;
        }
        public async Task<string> woopostinvoidprism(Order pr)
        {
            RestFile rest = new RestFile();
            wooCart woo = new wooCart();
            woo.getaddress();
            string coupon = "";
            var items = pr.line_items;

            var lg = rest.LoginVFCApi();
            List<dynamic> _items = new List<dynamic>();
            var tk = JsonConvert.DeserializeObject<dynamic>(lg)["apiToken"].ToString();
           
            ctkm _ctkm = new ctkm();
            //pr= _ctkm.khuyenmaisku(pr);
            DateTime bgd = DateTime.Parse("5/7/2022");
            DateTime endd= DateTime.Parse("5/8/2022").AddDays(1);
            if (pr.date_created >= bgd && pr.date_created < endd)
            {
                foreach (var it in pr.line_items)
                {
                    var namep = it.name;
                  
                            var _pr = await woo.GetProductsku(int.Parse(it.product_id.ToString()),it.sku);

                            it.price = _pr.price;
                            it.subtotal = it.price * it.quantity;
                       
                        
                }
            }
            foreach (var i in pr.line_items)
            {
       
                var disc_ = i.subtotal-i.total;
                if (disc_ < 0)
                    disc_ = disc_ * (-1);
                dynamic _i = new System.Dynamic.ExpandoObject();
                _i.upc = i.sku.ToString();
                _i.priceRetail = (i.subtotal/i.quantity).ToString();
                _i.qty = i.quantity.ToString();
                _i.discount = disc_.ToString();
                if(i.total>= i.subtotal)
                    _i.itemSubtotal = (i.total- disc_).ToString();
                else
                    _i.itemSubtotal = i.total.ToString();
                _items.Add(_i);

            }
            string tt = "";
            string qh = "";
            string px = "";
            var custTinhSid = woo.city.FirstOrDefault(s => s["ten"].ToString().Contains(pr.shipping.state.ToString()));
            var custHuyenSid = woo.distr.FirstOrDefault(s => s["ten"].ToString().Contains(pr.shipping.city.ToString()));
            var custXaSid = woo.phuong_xa.FirstOrDefault(s => s["ten"].ToString().Contains(pr.shipping.address_2.ToString()));
            if (custTinhSid != null)
                tt = custTinhSid["ma"].ToString();
            if (custHuyenSid != null)
                qh = custHuyenSid["ma"].ToString();
            if (custXaSid != null)
                px = custXaSid["ma"].ToString();
            decimal? ShippingFee_ = pr.fee_lines.Where(s=>s.name.Contains("10%")==false).Sum(s => s.total);
            decimal? ShippingFee = pr.shipping_total+ ShippingFee_;
            //var ctm =woo.getcustomer(pr.customer_id.ToString());
            if (pr.coupon_lines.Count > 0)
                coupon = pr.coupon_lines[0].code;
            string shipphone = pr.meta_data.FirstOrDefault(s => s.key.ToString() == "_shipping_phone").value.ToString();
            dynamic datapris =
            new
            {
                wooNo = pr.number.ToString(),
                couponCode = coupon.ToUpper(),
                statusMainSid = "1",
                statusSubSid = "2",
                custPhone = pr.billing.phone.ToString(),
                custFirstName = pr.shipping.first_name.ToString(),
                custLastName = pr.shipping.last_name.ToString(),
                custAddress = pr.shipping.address_1.ToString(),
                custTinhSid = tt,
                custHuyenSid = qh,
                custXaSid = px,
                custTitle = "",
                custDayOfBirth = "",
                custEmail = pr.billing.email.ToString(),
                custNote = pr.customer_note.ToString(),
                vfcShippingFee = ShippingFee.ToString(),
                extraFee = "0",
                fromSource = "WOO",
                documentItems = _items,
                paymentTypeSid =common.idpayment(pr.payment_method.ToString())
            };
            try
            {
                string ddd = JsonConvert.SerializeObject(datapris);
                var ip = rest.PostDataVFCApi("saleonlinedocument", datapris, tk);
                return ip;
            }
            catch(Exception ex)
            {
                string oo = JsonConvert.SerializeObject(datapris);
                return "-1";
            }
        }
        [HttpGet("listorder")]
        public async Task<string> listorder(string idorder)
        {
            DateTime n = DateTime.Now;
            string now = n.AddDays(-1).ToString("yyyy-MM-ddT00:00:00");
            string next = n.AddDays(1).ToString("yyyy-MM-ddT00:00:00");
            woocomerge woo = new woocomerge();
            int page = 1;
            List<Order> orders = new List<Order>();
           RestFile rest = new RestFile();
             var lg = rest.LoginVFCApi();
           var tk = JsonConvert.DeserializeObject<dynamic>(lg)["apiToken"].ToString();

            while (page > 0)
            {
                var order = await woo.getOrder(page.ToString(), now, next);
                if (order.Count <= 0)
                {
                    page = 0;
                    break;
                }
                orders.AddRange(order);
                page++;
            }
            foreach(var i in orders.Take(25))
            {
                Thread.Sleep(1000);
                woopostinvoidprism2(i, tk);
            }
            return "2";
        }
        [HttpGet("ThankyouWeb")]
        public async Task<string> ThankyouWeb(string idorder)
        {
            return "-1";
            common cmd = new common();
            woocomerge woo = new woocomerge();
            sqlmissdata miss = new sqlmissdata();
            var c_ = miss.checkduplicate(idorder);
            if (c_ == "0")
                return "-1";
            var order = await woo.getorder(idorder);
            List<string> cl = new List<string>() { "idwoo", "contents", "status", "createdate", "sid", "source" };
            List<string> vl = new List<string>() { order.id.ToString(), JsonConvert.SerializeObject(order), order.status, DateTime.Now.ToString(), "","" };

            /* if(order.status== "pending")
                return "-2";
            if (order.status == "cancelled")
                return "-3";
           */
            if (order.status != "cancelled" && order.status != "pending")
            {
                string isid =await woopostinvoidprism(order);
                if (isid != "-1")
                {
                    vl[cl.IndexOf("status")] = "success";
                    vl[cl.IndexOf("sid")] = isid.ToString();
                }
            }
            string ins = miss.insertsqlstr("missdata", cl, vl);
            miss.runSQL(ins);


            //cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), "missingwoo-" + order.id + ".txt", JsonConvert.SerializeObject(order));

            try
            {
                if (order.coupon_lines.Count > 0)
                {
                    string key = cmd.GetAuth_Nonce();
                    foreach (var c in order.coupon_lines)
                    {
                        var code = c.code.ToUpper();

                        code = code.ToUpper();

                        RestFile rest = new RestFile();

                        string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code.ToUpper() + "&cols=*&page_no=1&page_size=1";
                        var a = rest.GetData(key, urlupdate);
                        List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
                        string vsion = dnm[0].rowversion.ToString();
                        string usedqty = dnm[0].usedqty.ToString();
                        string sid = dnm[0].sid.ToString();
                        dynamic data = new System.Dynamic.ExpandoObject();
                        DateTime now = DateTime.Now;
                        data.rowversion = int.Parse(vsion);
                        data.usedqty = 1;

                        var x = new
                        {
                            data = new[] { data }
                        };

                        string json2 = JsonConvert.SerializeObject(x);
                        string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
                        string u = rest.updateItemcoupon(key, urlupdatei, json2);
                    }
                }
            }
            catch { }
            return "0";
        }
       
        [HttpGet("ThankyouCart")]
        public async Task<string> ThankyouCart(string idorder,string source)
        {
            return "-1";
            common cmd = new common();
            wooCart woo = new wooCart();
            sqlmissdata miss = new sqlmissdata();
            var c_ = miss.checkduplicateCart(idorder);
            if (c_ == "0")
                return "-1";
            var order = await woo.getorder(idorder);
            List<string> cl = new List<string>() { "idwoo", "contents", "status", "createdate", "sid", "source" };
            List<string> vl = new List<string>() { order.id.ToString(), JsonConvert.SerializeObject(order), order.status, DateTime.Now.ToString(),"",source };
           
            /* if(order.status== "pending")
                return "-2";
            if (order.status == "cancelled")
                return "-3";
           */
            if (order.status != "cancelled" && order.status != "pending")
            {
                string isid =await woopostinvoidprism(order);
                if (isid != "-1")
                {
                    vl[cl.IndexOf("status")] = "success";
                    vl[cl.IndexOf("sid")] = isid.ToString();
                }
            }
            string ins = miss.insertsqlstr("missdata", cl, vl);
            miss.runSQL(ins);

            try
            {
                if (order.coupon_lines.Count > 0)
                {
                    string key = cmd.GetAuth_Nonce();
                    foreach (var c in order.coupon_lines)
                    {
                        var code = c.code.ToUpper();

                        code = code.ToUpper();

                        RestFile rest = new RestFile();
                        string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code.ToUpper() + "&cols=*&page_no=1&page_size=1";
                        var a = rest.GetData(key, urlupdate);
                        List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
                        string vsion = dnm[0].rowversion.ToString();
                        string usedqty = dnm[0].usedqty.ToString();
                        string sid = dnm[0].sid.ToString();
                        dynamic data = new System.Dynamic.ExpandoObject();
                        DateTime now = DateTime.Now;
                        data.rowversion = int.Parse(vsion);
                        data.usedqty = 1;

                        var x = new
                        {
                            data = new[] { data }
                        };

                        string json2 = JsonConvert.SerializeObject(x);
                        string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
                        string u = rest.updateItemcoupon(key, urlupdatei, json2);
                    }
                }
            }
            catch { }
            return "0";
        }

        [HttpGet("getorderwidthCoupon")]
        public async Task<string> getorderwidthCoupon(string code)
        {
            woocomerge woo = new woocomerge();
            var c_ = await woo.getorderwidthCoupon(code);
            return "";
        }
        [HttpGet("GetinforCouponCart")]
        public string GetinforCouponCart(string code)
        {
            storePrism store = new storePrism();
            var ss = store.GET_COUPON_INFO(code);
            string JSONresult = JsonConvert.SerializeObject(ss);
            return JSONresult;
        }
            [HttpGet("GetinforCouponSetcoupondomainCart")]
        public async Task<string> GetinforCouponSetcoupondomainCart(string code)
        {
            code = code.Trim();
            wooCart woo = new wooCart();
            var c_ = await woo.GetCoupon(code);
            if (c_ != null)
            {
                return "0";
            }
            common cmd = new common();
            code = code.ToUpper();
            try
            {

                storePrism store = new storePrism();
                var ss = store.GET_COUPON_INFO(code);
                string JSONresult = JsonConvert.SerializeObject(ss);

                var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);

                if (JSONresults.Count > 0)
                {
                    if (JSONresults[0].START_DATE.ToString() == null)
                        return "-1";
                    DateTime start = DateTime.Parse(JSONresults[0].START_DATE.ToString());
                    DateTime end = DateTime.Parse(JSONresults[0].END_DATE.ToString());
                    if (end < DateTime.Now.AddHours(1))
                        return "-1";
                    int MAX_AMOUNT = int.Parse(JSONresults[0].MAX_AMOUNT.ToString());
                    int DISCOUNT_AMOUNT = int.Parse(JSONresults[0].DISCOUNT_AMOUNT.ToString());
                    double max_dc_tmp = double.Parse(MAX_AMOUNT.ToString()) * DISCOUNT_AMOUNT;
                    double max_dc = max_dc_tmp / 100;

                    string max_amount = JSONresults[0].MAX_AMOUNT.ToString();
                    var type = "percent";

                    string desc = JSONresults[0].DESCRIPTION.ToString();
                    var rs = await woo.CreateCoupon(start, end, max_amount, "0", type, desc, int.Parse(JSONresults[0].DISCOUNT_AMOUNT.ToString()), code, max_dc);
                    return "0";
                }
                else
                {
                    return "-1";
                }
            }
            catch
            {
                return "-2";
            }
        }
        [HttpGet("GetDataCamera")]
        public string GetDataCamera(string idcamera,string f,string t)
        {
            RestFile resf = new RestFile();
            f = DateTimeOffset.Parse(f).ToUnixTimeMilliseconds()+ "000";
            t = DateTimeOffset.Parse(t).AddDays(1).ToUnixTimeMilliseconds() + "000";
            var tk = resf.GetTokentcamera();
                 
            var data = resf.GetDatacameraL(f, t, idcamera, tk);
            return data;
        }
            [HttpGet("GetinforCouponSetcoupon")]
        public async Task<string> GetinforCouponSetcoupon(string code)
        {
            code = code.Trim();
            woocomerge woo = new woocomerge();
            var c_ = await woo.GetCoupon(code);
            if (c_ != null)
            {
                return "0";
            }
            common cmd = new common();
            //RestFile rest = new RestFile();
            ///string key = cmd.GetAuth_Nonce();
            code = code.ToUpper();
            //string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code + "&cols=*&page_no=1&page_size=1";
            //var a = rest.GetData(key, urlupdate);
            //var b = JsonConvert.DeserializeObject<List<dynamic>>(a);
            //if (b.Count <= 0)
            //    return "-1";
            //var c = b[0];
            //if (c.usedqty.ToString() == "0" && c.issuedstatus.ToString() == "1")
            try {

                storePrism store = new storePrism();
                var ss = store.GET_COUPON_INFO(code);
                string JSONresult = JsonConvert.SerializeObject(ss);

                var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);

                if (JSONresults.Count > 0)
                {
                    if (JSONresults[0].START_DATE.ToString() == null)
                        return "-1";
                    DateTime start = DateTime.Parse(JSONresults[0].START_DATE.ToString());
                    DateTime end = DateTime.Parse(JSONresults[0].END_DATE.ToString());
                    if (end < DateTime.Now.AddHours(1))
                        return "-1";
                    int MAX_AMOUNT = int.Parse(JSONresults[0].MAX_AMOUNT.ToString());
                    int DISCOUNT_AMOUNT = int.Parse(JSONresults[0].DISCOUNT_AMOUNT.ToString());
                    double max_dc_tmp = double.Parse(MAX_AMOUNT.ToString()) * DISCOUNT_AMOUNT;
                    double max_dc = max_dc_tmp / 100;

                    string max_amount = JSONresults[0].MAX_AMOUNT.ToString();
                    var type = "percent";

                    string desc = JSONresults[0].DESCRIPTION.ToString();
                    var rs = await woo.CreateCoupon(start, end, max_amount, "0", type, desc, int.Parse(JSONresults[0].DISCOUNT_AMOUNT.ToString()), code, max_dc);
                    return "0";
                }
                else
                {
                    return "-1";
                }
            }
            catch
            {
                return "-2";
            }
        }

        [HttpGet("GetCouponSetcouponCrm")]
        public string GetCouponSetcouponCrm(string au, string type, string qlt)
        {

            //if (au == null)
            //return "";
            try
            {
                common cmd = new common();
                au = cmd.GetAuth_Nonce();
                type = "SN";
                qlt = "100";
                //string GetAuth_Nonce = cmd.GetAuth_Nonce();
                RestFile rest = new RestFile();
                //string a_coponset = "api/common/couponsetcoupon?cols=*&filter=(couponcode,LK,HT*)AND(issuedstatus,EQ,0)&sort=startdate,desc&page_no=1&page_size=500";
                string a_coponset = "api/common/couponsetcoupon?cols=*&filter=couponcode,LK," + type + "*&sort=startdate,desc&page_no=1&page_size=" + qlt;
                string l = rest.GetData(au, a_coponset);
                var LCpo = cmd.CreateCpCrm(JsonConvert.DeserializeObject<List<dynamic>>(l));

                return JsonConvert.SerializeObject(LCpo);
            }
            catch
            {
                return "";
            }
        }
        [HttpGet("GetCouponSetcoupon")]
        public string GetCouponSetcoupon(string au)
        {
            //if (au == null)
            //    return "";
            try
            {
                common cmd = new common();
                string GetAuth_Nonce = cmd.GetAuth_Nonce();
                RestFile rest = new RestFile();
                //string a_coponset = "api/common/couponsetcoupon?cols=*&filter=(couponcode,LK,HT*)AND(issuedstatus,EQ,0)&sort=startdate,desc&page_no=1&page_size=500";
                string a_coponset = "api/common/couponsetcoupon?cols=*&filter=couponcode,LK,HT*&sort=startdate,desc&page_no=1&page_size=500";
                string l = rest.GetData(GetAuth_Nonce, a_coponset);
                //var LCpo = JsonConvert.DeserializeObject<List<dynamic>>(l);
                return l;
            }
            catch
            {
                return "";
            }
        }
        [HttpGet("GetScoreCrm")]
        public string GetScoreCrm(string idctm, string infor)
        {

            string tokenCrm = "basic TlRZUlMtRVNCUkUtQUVWUlQtTU5UVVItUllXU0c=";
            RestFile rest = new RestFile();
            dynamic bodys = new
            {
                UserName = "admin",
                Password = "123@qaz"
            };
            string url = "Token";
            string lg = rest.PostDataCrm(url, bodys, tokenCrm);
            dynamic tkent = JsonConvert.DeserializeObject<dynamic>(lg);
            var tk = tkent.Data.Token;
            string url2 = "Api/GetScore";
            dynamic data = new
            {
                TypeID = 1,
                SearchValue = idctm
            };
            string rs = rest.PostDataCrm(url2, data, "basic " + tk);

            List<dynamic> lrs = new List<dynamic>();
            List<dynamic> lpromo = new List<dynamic>();
            List<dynamic> lcoupon = new List<dynamic>();
            dynamic score = JsonConvert.DeserializeObject<dynamic>(rs);
            var aa = score.data.ToString();
            if (aa != "[]")
            {
                var Scorerank = score.data[0].ScoreRank.ToString();
                dynamic inforctm = new System.Dynamic.ExpandoObject();
                inforctm.diem = Scorerank;
                inforctm.tientichluy = score.data[0].MoneyAccumulation.ToString();
                inforctm.doanhthu = score.data[0].TotalMoney.ToString();
                //if (int.Parse(Scorerank) >= 0)
                //{
                //    common cmd = new common();
                //    string key = cmd.GetAuth_Nonce();
                //    string urlcpo = "v1/rest/pcpromo?cols=*&filter=(validation_subtotal,GT,0)AND(general_active,EQ,1)";
                //    string cp = rest.GetDataWebClient(key, urlcpo);
                //    List<dynamic> ss = JsonConvert.DeserializeObject<List<dynamic>>(cp);
                //    foreach (var i in ss)
                //    {
                //        dynamic a = new System.Dynamic.ExpandoObject();
                //        a.value = i.validation_subtotal;
                //        a.sid = i.sid;
                //        a.rewardcouponsetid = i.reward_coupon_setid;
                //        a.desc = i.general_promo_group;
                //        lpromo.Add(a);
                //    }

                //    dynamic itemdcm = new System.Dynamic.ExpandoObject();
                //    itemdcm.st_address_line6 = rs;
                //}
                lrs.Add(inforctm);
                lrs.Add(score.data[0].Coupon);
            }
            else
            {
                dynamic inforctm = new System.Dynamic.ExpandoObject();
                inforctm.diem = 0;
                inforctm.diem = 0;
                inforctm.tientichluy = 0;
                inforctm.doanhthu = 0;
                lrs.Add(inforctm);
                lrs.Add(null);
                string rn = UpdateCustomerPrim(infor, idctm);

                if (rn == "loi")
                {
                    inforctm.diem = -1;
                    common cmd = new common();
                    cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), "Rectm_" + Guid.NewGuid().ToString() + ".txt", "GetScoreCrm?idctm=" + idctm + "&infor=" + infor);
                    return "-1";
                }
            }
            return JsonConvert.SerializeObject(lrs);
        }
        [HttpGet("inforItem")]
        public string inforItem(string nameItem)
        {
            common cmd = new common();
            var store = cmd.inforItems("JAW-M056-1209NM");
            return store;
        }
        [HttpGet("inventstoreskulive")]
        public string inventstoreskulive(string sku)
        {

            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.GetInventorieLive(sku);
            string JSONresult = JsonConvert.SerializeObject(store_iv);

            var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);

            return JsonConvert.SerializeObject(JSONresults);
        }
        [HttpGet("editqltstoresku")]
        public string editqltstoresku(string sku, string nb)
        {
            return EditinventLive(sku, nb);
        }
        public string EditinventLive(string sku, string nb)
        {
            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.AddInventorieLive(sku, nb);

            return store_iv.ToString();
        }
        [HttpGet("inventstoresku_mtk")]
        public string inventstoresku_mtk(string mtk)
        {

            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.GetInventorieLiveMtk(mtk);
            string JSONresult = JsonConvert.SerializeObject(store_iv);
            var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);
            if (JSONresults.Count() <= 0)
                return "0";
            string a = JSONresults.FirstOrDefault()["AVAILABLE_QTY"].ToString();
            if (int.Parse(a) < 3)
                a = "0";
            return a;
        }
        [EnableCors("AllowOrigin")]
        [HttpGet("inventstoreskuall")]
        public string inventstoreskuall(string sku)
        {
            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.InventorieLiveall(sku);
            string JSONresult = JsonConvert.SerializeObject(store_iv);
            var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);
            if (JSONresults.Count() <= 0)
                return "0";
            return JSONresult;
        }
        [HttpGet("inventstoresku_v2")]
        public string inventstoresku_v2(string sku)
        {

            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.InventorieLive_v2(sku);
            string JSONresult = JsonConvert.SerializeObject(store_iv);
            var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);
            if (JSONresults.Count() <= 0)
                return "0";
            var sum = 0;
            foreach (var j in JSONresults)
            {
                sum += int.Parse(j["OH_QTY"].ToString());

            }
            string a = JSONresults.FirstOrDefault()["OH_QTY"].ToString();
            if (sum < 1)
                sum = 0;
            return sum.ToString();
        }
        [HttpGet("inventstoresku")]
        public string inventstoresku(string sku)
        {

            common cmd = new common();
            storePrism store = new storePrism();
            var store_iv = store.GetInventorie(sku);
            var stores = cmd.storeWeb();
            string JSONresult = JsonConvert.SerializeObject(store_iv);

            var JSONresults = JsonConvert.DeserializeObject<List<dynamic>>(JSONresult);
            JSONresults = JSONresults.Where(s => s["QTY"] > 2).ToList();
            foreach (var i in JSONresults)
            {
                var st = stores.FirstOrDefault(s => s["CODE"].ToString().Trim() == i["STORE_CODE"].ToString().Trim());
                if (st != null)
                {
                    i.namestore = st.name;
                    i.diachi = st.address;
                }
            }
            return JsonConvert.SerializeObject(JSONresults);
        }
        [HttpGet("inventstore")]
        public string inventstore(string sidItem, string sidstore, string subsidiary_sid)
        {
            common cmd = new common();
            var store = cmd.getinventstore2(sidItem, sidstore, subsidiary_sid);
            return store;
        }
        [HttpGet("Updatedocument")]
        public string Updatedocument(string idbill)
        {

            wooApivModel woo = new wooApivModel();
            woo.GetBill(idbill);
            //common cmd = new common();
            //var rs= cmd.CreateinvoiceID();
            return "";
        }
        [HttpGet("UpdateUserwoo")]
        public async Task<string> UpdateUserwoo(string iduser)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
            woocomerge woo = new woocomerge();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            var a = await woo.finduser(iduser);
            var billing = a.billing.phone;
            var idprism = a.billing.state;
            cmd.createCustomerPrism(GetAuth_Nonce, "0909889887", "mminhtest@gmail.com");



            if (idprism != "")
            {
                string address = "v1/rest/customer?cols=*&filter=primary_phone_no,EQ," + billing + "&cols=*";
                var ctm = rest.GetDataWebClient(GetAuth_Nonce, address);
                var obj = JsonConvert.DeserializeObject<dynamic>(ctm);
                if (obj.Count > 0)
                {
                    a.first_name = obj[0].first_name.ToString();
                    a.last_name = obj[0].last_name.ToString();
                    a.billing.state = obj[0].sid.ToString();
                    a.billing.address_1 = obj[0].primary_address_line_1.ToString();
                    await woo.updateuser(a);
                }
            }
            else
            {

            }
            return "";
        }
        [HttpGet("UpdateCustomer")]
        public string UpdateCustomer(string data)
        {

            var header = Request.Headers["Origin"].FirstOrDefault();
            common cmd = new common();

            dynamic ctm = JsonConvert.DeserializeObject<dynamic>(data);
            dynamic item = new System.Dynamic.ExpandoObject();
            //1900-10-03T16:30:09.000+07:00
            item.last_name = ctm.LastName;
            item.first_name = ctm.FirstName;
            item.udf1_date = ctm.BirthDay.ToString().Replace(" ", "");
            item.primary_phone_no = ctm.PhoneNumber;
            item.email_address = ctm.Emai;
            item.primary_address_line_1 = ctm.AddressNo;
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            string rs = updateItemPro(GetAuth_Nonce, ctm.CustomerPrismID.ToString(), item);
            return rs;
        }
        [HttpGet("createcustomeronline")]
        public async Task<string> createcustomeronline(string idctm)
        {
            woocomerge woo = new woocomerge();
            var ctm = await woo.finduser(idctm);
            common cmd = new common();
            DateTime now = DateTime.Now.AddMinutes(-5);
            if (DateTime.Parse(ctm.date_modified.ToString()) < now)
            {
                return "";
            }
            string action = "Save";
            string phone = ctm.billing.phone.ToString();
            var o = new
            {
                primary_phone_no = phone,
                first_name = ctm.first_name,
                last_name = ctm.last_name,
                udf1_date = "",
                email_address = ctm.email,
                primary_address_line_1 = ctm.billing.address_1,
                primary_address_line_3 = ctm.billing.state,
                primary_address_line_4 = ctm.billing.city,
                store_sid = "544935116000191205",
                store_name = "SOL",
                store_code = "SOL",
            };

            var rs = cmd.SOLcustomer(JsonConvert.SerializeObject(o));
            return "";
        }
        [HttpGet("createcustomer2")]
        public string createcustomer2(string valuectm, string au)
        {
            RestFile rest = new RestFile();
            common cmd = new common();
            string key = au;
            key = cmd.GetAuth_Nonce();

            var l = JsonConvert.DeserializeObject<List<dynamic>>(valuectm);
            var o = l[0];
            string u = o.action.ToString();

            var email = new List<dynamic>();
            if (o.email_address.ToString() != "")
            {
                email.Add(new
                {
                    originapplication = "RProPrismWeb",
                    createdby = "PrismRS",
                    primaryflag = true,
                    emailaddress = o.email_address.ToString()
                }
                );
            }
            string sid = "";
            string fname = "test";
            string lname = "test";
            if (o.first_name != "")
                lname = o.first_name.ToString();
            if (o.last_name != "")
                lname = o.last_name.ToString();
            var data = new
            {
                data = new[]
                {
                    new
                    {
                        originapplication="RProPrismWeb",
                        active=1,
                        sbssid="605135163000180340",
                        storesid=o.store_sid.ToString(),
                        employeeascustomer=0,
                        sharetype=0,
                        custextend=new List<dynamic>(),
                        custaddress=new []{
                                new {
                                  originapplication="RProPrismWeb",
                                  createdby="PrismRS",
                                  active=true,
                                  primaryflag=true,
                                  address1=o.primary_address_line_1,
                                  address2=o.primary_address_line_2,
                                 address3=o.primary_address_line_3,
                                  address4=o.primary_address_line_4,
                                }},
                        custphone=new []{new{
                            originapplication="RProPrismWeb",
                            createdby="PrismRS",
                            primaryflag=true,
                            phoneno=o.primary_phone_no}},
                         custemail=email,
                        firstname=fname,
                        titlesid=o.title_sid,
                        lastname=lname,
                        custid=(System.Decimal?) null,
                        custimage =(System.Decimal?) null
                    }
                }
            };

            string url = "api/common/customer";
            string dtctm = JsonConvert.SerializeObject(data);
            var k = rest.PostItemPrism_ctm(key, url, dtctm);
            var jObj = JsonConvert.DeserializeObject<dynamic>(k);
            sid = jObj.data[0].sid.ToString();
            return sid;
        }
        [HttpGet("createcustomer")]
        public string createcustomer(string valuectm,string au)
        {

            RestFile rest = new RestFile();
            common cmd = new common();
             string key =au;
             key = cmd.GetAuth_Nonce();
             var l = JsonConvert.DeserializeObject<List<dynamic>>(valuectm);
            var o = l[0];
            //dynamic o = new System.Dynamic.ExpandoObject();
            //o.action = "save";
            //o.primary_phone_no = "090854388471";
            //o.email_address = "cuty@gmail.com";
            //o.primary_address_line_1 = "";
            //o.primary_address_line_2 = "12";
            //o.primary_address_line_3 = "434";
            //o.primary_address_line_4 = "21";
            //o.first_name = "cu tý";
            //o.last_name = "";
            //o.udf1_date = "";
            //o.title_sid = "544935122000173148";
            //o.sid = "";
            string u = o.action.ToString();
            //string check_p = "v1/rest/customer?filter=primary_phone_no,EQ," + o.primary_phone_no + "&cols=*&page_no=1&page_size=1";
            //string pro = rest.GetDataWebClient(key, check_p);
            
            //    if (pro != "[]")
            //    {
            //        if (u != "save")
            //        {
            //            var ol = JsonConvert.DeserializeObject<List<dynamic>>(pro);
            //            string sids = ol[0].sid.ToString();
            //            if (sids != o.sid.ToString())
            //                return "-1";
            //        }
            //        else
            //                return "-1";
            //    }
     
            dynamic phone = new
            {
                phone_no = o.primary_phone_no

            };
            dynamic mail = new
            {
                email_address = o.email_address,
                origin_application = "RProPrismWeb",
                primary_flag = "true"
            };
            dynamic address = new
            {
                address_line_1 = o.primary_address_line_1,
                address_line_2 = o.primary_address_line_2,
                address_line_3 = o.primary_address_line_3,
                address_line_4 = o.primary_address_line_4,
            };
            List<dynamic> phones = new List<dynamic>();
            phones.Add(phone);
            List<dynamic> mails = new List<dynamic>();
            mails.Add(mail);
            List<dynamic> add = new List<dynamic>();
            add.Add(address);
            dynamic a = new System.Dynamic.ExpandoObject();
            a.phones = phones;
            a.first_name = "customer";
            a.last_name = "customer";
            a.custid = o.primary_phone_no.ToString();
            if (o.first_name != "")
                a.first_name = o.first_name;
            if (o.last_name != "")
                a.last_name = o.last_name;
            if (o.primary_address_line_1 != "")
                a.primary_address_line_1 = o.primary_address_line_1;
            if (o.primary_address_line_2 != "")
                a.primary_address_line_2 = o.primary_address_line_2;
            if (o.primary_address_line_3 != "")
                a.primary_address_line_3 = o.primary_address_line_3;
            if (o.primary_address_line_4 != "")
                a.primary_address_line_4 = o.primary_address_line_4;
            a.title_sid = o.title_sid;
            a.store_sid = o.store_sid;
            a.customer_active =bool.Parse(o.active.ToString());
            try
            {
                string bd = cmd.convertdate(o.udf1_date.ToString());
                a.udf1_date = DateTime.Parse(bd);
            }
            catch
            {
                a.udf1_date = "";
            }

            if (o.email_address.ToString() != "" && o.email_address.ToString() != "null")
                a.emails = mails;
            a.addresses = add;

            a.origin_application = "RProPrismWeb";
            a.share_type = "1";
            //return View();
            string sid = "";
            string storename = "SOL";
         

            if (u == "save")
            {
                string url = "v1/rest/customer";
                var ddsd = JsonConvert.SerializeObject(a);
                var k = rest.PostItemPrism_v2(key, url, a);
                var jObj = JsonConvert.DeserializeObject<List<dynamic>>(k);
                sid = jObj[0].sid.ToString();
            }
            else
            {
                sid = o.sid.ToString();
                string uurl = "v1/rest/customer?filter=sid,EQ," + o.sid.ToString() + "&cols=row_version,phones,primary_phone_no,store_name&page_no=1&page_size=1";
                string vs = rest.GetDataWebClient(key, uurl);
                List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(vs);
                string vsion = dnm[0].row_version;
                storename = dnm[0].store_name;
                if (dnm[0].primary_phone_no.ToString() != o.primary_phone_no.ToString())
                {
                    if (dnm[0].primary_phone_no.ToString()!="")
                    {
                        var strpvs = "v1/rest/customer/" + o.sid.ToString() + "/phone/?cols=sid,row_version&page_no=1&page_size=10";
                        var vss = rest.GetDataWebClient(key, strpvs);
                        List<dynamic> dnmp = JsonConvert.DeserializeObject<List<dynamic>>(vss);
                    
                        string vsionp = dnmp[0].row_version;
                        var str = dnm[0].phones[0].link.ToString();
                        var strp = str.Remove(0, 1) + "?filter=row_version,eq," + vsionp;
                        var upp_ = cmd.updatephone(strp, key, o.primary_phone_no.ToString());
                        
                    }
                    else
                    {
                        string strnew_phone = "v1/rest/customer/"+ o.sid.ToString() + "/phone";
                        var nphone =JsonConvert.SerializeObject(new[]
                        {
                            new
                            {
                                origin_application="RProPrismWeb",
                                customer_sid=o.sid.ToString(),
                                phone_no= o.primary_phone_no.ToString()
                            }
                        });

                        var insertp = rest.PostDatav_22(key, strnew_phone, nphone);
                    }
                    vsion = (int.Parse(vsion) + 1).ToString();
                }
                    string urlupdate = "v1/rest/customer/" + o.sid.ToString() + "?filter=row_version,eq," + vsion + "";
                
                var up = rest.updateItem(key, urlupdate, a);
                try
                {
                    cmd.updateaddressctm(key, o.sid.ToString(), o.primary_address_line_1.ToString(), o.primary_address_line_4.ToString(), o.primary_address_line_3.ToString(), o.primary_address_line_2.ToString());
                }
                catch { }
                try
                {
                    if (o.email_address.ToString() != "")
                        cmd.updateEmailctm(key, o.sid.ToString(), o.email_address.ToString());
                }
                catch { }
                
            }
            var Gender = o.title_sid;
            if (Gender == "544935122000172136")
                Gender = "1";
            else
                Gender = "0";
            string bdcrm =cmd.convertdate(o.udf1_date.ToString());
            if (bdcrm != "" && bdcrm!=null)
            {
                bdcrm = DateTime.Parse(bdcrm).ToString("yyyy-MM-dd");
                try
                {
                    bdcrm = bdcrm + "T00:00:00+07:00";
                }
                catch
                {
                    bdcrm = "";
                }
        
            }
            int IsDelete = 0;
            if(!bool.Parse(o.active.ToString()))
                IsDelete = 1;
            var c_ = new
            {
                CustomerPrismID = sid,
                FirstName = o.first_name,
                LastName = o.last_name,
                Gender = Gender,
                BirthDay = bdcrm,
                PhoneNumber = o.primary_phone_no,
                Email = o.email_address,
                AddressNo = o.primary_address_line_1,
                AddressReceipts = o.primary_address_line_1,
                City = o.primary_address_line_4,
                WardPrism = o.primary_address_line_3,
                storename = storename,
                storecode = storename,
                IsDelete = IsDelete
            };
            try
            {

                sqlcommon sql = new sqlcommon();
                List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
                List<string> vl = new List<string>() {
               "2",JsonConvert.SerializeObject(c_),"0","1","from app",DateTime.Now.ToString(),cmd.GetIPAddress(),sid
            };
                string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
                sql.runSQL(sqlinsert);
                
            }
            catch
            {
                cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), "ctm_createcustomer_" + Guid.NewGuid().ToString() + ".txt", "UpdateCustomerPrim?resp=" + JsonConvert.SerializeObject(c_) + "&sid=" + sid);
           
            }
            return sid;
        }
        [HttpGet("UpdateCustomerPrim")]
        public string UpdateCustomerPrim(string resp, string sid)
        {
            common cmd = new common();
            List<string> lencode = new List<string>() { "FirstName", "LastName" };
            try
            {
                dynamic ctm = JsonConvert.DeserializeObject<dynamic>(resp);
                try
                {
                    //ctm.BirthDay = ctm.BirthDay.ToString().Replace(".000 ", "+");
                    ctm.BirthDay = ctm.BirthDay.ToString().Split('T')[0];
                }
                catch
                {
                    ctm.BirthDay = "";
                }

                foreach (var e in lencode)
                {
                    ctm[e] = cmd.Encode(ctm[e].ToString());
                }
                ctm.CreatedDate = DateTime.Now;
                sqlcommon sql = new sqlcommon();
                List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
                List<string> vl = new List<string>() {
               "2",JsonConvert.SerializeObject(ctm),"0","1","new",DateTime.Now.ToString(),cmd.GetIPAddress(),sid
            };
                string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
                sql.runSQL(sqlinsert);
                return "ok";
            }
            catch
            {
               cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), "ctm_UpdateCustomerPrim_" + Guid.NewGuid().ToString()+".txt", "UpdateCustomerPrim?resp="+resp+ "&sid="+ sid);
                return "loi";
            }
        }
        [HttpGet("UpdateCustomerPrim2")]
        public string UpdateCustomerPrim2(string sid,string phone_no)
        {
           
            Crm c = new Crm();
            return c.UpdateCustomer(sid, phone_no);
        }
        [HttpGet("UpdateBillPrim")]
        public string UpdateBillPrim(string sid)
        {
         
            Crm c = new Crm();
            return c.UpdateBill(sid);
        }
        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return id.ToString();
        }
        [HttpGet("isWeb")]
        public string isWeb(string upc)
        {
            common cmd = new common();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            RestFile rest = new RestFile();
            string urlit = "v1/rest/inventory?cols=*&filter=description1,EQ," + upc + "";
            string item = rest.GetDataWebClient(GetAuth_Nonce, urlit);
            return item;
        }
       
        [HttpPost("UploadFileProcductWeb")]
        public async Task<string> UploadFileProcductWeb(IFormFile files)
        {
            List<string> exid = new List<string>();
            common cmd = new common();
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", files.FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(3).GroupBy(s => s.Cell(2).Value).ToList();
            //var count = ws2.Count();
            //foreach (var row in ws1.Rows()) {
            //    var cell = row.Cell(2);
            //    object value = cell.Value;
            //    string value2 = cell.GetValue<string>();
            //}
            woocomerge woo = new woocomerge();

            var ids = new List<string>();
            var cate = await woo.catelogy();
            
            foreach (var row in ws2)
            {
            
                var y = row;
                var cell = y.Key.ToString();
                string[] des = cell.Split("-");

                string ddes = des[des.Length - 1];
                int id = ids.IndexOf(ddes);
                if (id == -1)
                {


                    ids.Add(ddes);
                    try
                    {
                        List<string> colors = y.Select(s => s.Cell(7).Value.ToString()).ToList();
                        List<string> sizes = y.Select(s => s.Cell(8).Value.ToString()).ToList();
                        List<string> cates = new List<string>();
                        var first = y.FirstOrDefault();
                        var ct1 = cate.FirstOrDefault(s => s.name == first.Cell(4).Value.ToString());
                        var ct2 = cate.FirstOrDefault(s => s.parent == ct1.id && s.name == first.Cell(5).Value.ToString());
                        if (ct1 != null)
                            cates.Add(ct1.id.ToString());
                        if (ct2 != null)
                            cates.Add(ct2.id.ToString());
                        foreach (var k in colors)
                        {
                            string tmp = k;
                            tmp = cmd.replaceChar(tmp);
                        }

                        List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
                        Product p = new Product();

                        attrs = woo.listAttrID(colors, sizes);
                        string des1 = first.Cell(10).Value.ToString();
                        string des2 = first.Cell(12).Value.ToString();
                        string des3 = first.Cell(13).Value.ToString();
                        List<ProductMeta> meta = woo.listMetaId(des3);
                        p = await woo.addproduct_excel(attrs, first.Cell(6).Value.ToString(), des1, ddes, cates.ToList(), meta, cell);

                        List<string> sku_ = new List<string>();
                        foreach (var b in y)
                        {

                            string uupc = b.Cell(1).Value.ToString();
                            if (sku_.IndexOf(uupc) == -1)
                            {
                                await woo.inputProductvariable(p, b.Cell(11).Value.ToString()
                                    , uupc,
                                    b.Cell(7).Value.ToString(),
                                    b.Cell(8).Value.ToString(),"");
                                sku_.Add(uupc);
                            }
                        }
                    }
                    catch
                    {
                        exid.Add(cell);
                    }
                }
                else
                    exid.Add(cell);
            }
            return string.Join(";", exid);
        }
        [HttpPost("UploadFilecoupon")]
        public async Task<string> UploadFilecoupon(List<IFormFile> files)
        {
            woocomerge woo = new woocomerge();
            
            List<string> exid = new List<string>();
            common cmd = new common();
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", files[0].FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files[0].CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(1).ToList();
            List<string> cpcrm = new List<string>();
            sqlcommon sql = new sqlcommon();
            string Now = DateTime.Now.ToString();
            
            foreach (var row in ws2)
            {
                var cp = row.Cell(8).Value.ToString();
                var sid =  row.Cell(1).Value.ToString();
                //var dd = row.Cell(3).Value.ToString();
       
                var oexc = cmd.findvalcoupon(cp.ToString());
                if(oexc[1]!="0")
                {
                    dynamic d = new
                    {
                        CouponPrismID= sid,
                        CouponCode= cp,
                        CouponName= oexc[2],
                        CouponValue= oexc[1],
                        CreateDate = cmd.converTimeCrm(Now),
                        ExpiryDate="",
                        IsActive= false,
                        CouponType= oexc[0],
                    };
                    dynamic o = JsonConvert.SerializeObject(d);
                    string sqls = cmd.Stringcolumnsql("5", o, sid, "");
                    cpcrm.Add(sqls);
                    string type = "percent";
                    if (oexc[0] != "SN")
                    {
                        type = "fixed_cart";
                    }
                    try
                    {
                        await woo.addcoupon(int.Parse(oexc[1]), cp.ToString(), oexc[2], type);
                    }
                    catch { }
                }
            }
            
            sql.runSQL(string.Join(";", cpcrm));
            return string.Join(";", exid);
        }
        [HttpPost("UploadFile2")]
        public string Postfile2(List<IFormFile> files)
        {
            var hd = Request.Headers["etx"][0].ToString()+"_";
            List<string> l_name = new List<string>();
            string FolderSource = Path.Combine(@"wwwroot\Images\");
            int n_= 1;
            foreach (IFormFile file in files)
            {
                if (file == null || file.Length == 0)
                    return "";
                var path = Path.Combine(FolderSource, "", hd+n_+".jpg");
                FileStream fileStream = new FileStream(path, FileMode.Create);
                file.CopyTo(fileStream);
                fileStream.Dispose();
                l_name.Add(hd + n_ + ".jpg");
                n_++;
            }
            return string.Join(",",l_name);
        }
        [HttpGet("UpdatePromotiontoCrm")]
        public string UpdatePromotiontoCrm(string data)
        {
          
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            var aa = o.CreateDate.ToString().Replace(".000", "");
            var da = aa.Replace(" ", "+");
            o.CreateDate = da;
            common cmd = new common();
            string GetAuth_Nonce = cmd.GetAuth_Nonce();
            RestFile rest = new RestFile();
            string setId = o.validation_coupon_setid;
            string a_copon = "api/common/couponset?filter=setid,EQ," + setId + "&cols=sid,setname,startdate,enddate&page_no=1&page_size=1";
            string r = rest.GetData(GetAuth_Nonce, a_copon);
            var dnmCpo = JsonConvert.DeserializeObject<List<dynamic>>(r);
            string a_coponset = "api/common/couponsetcoupon?filter=couponsetsid,EQ," + dnmCpo[0].sid + "&cols=sid,couponcode,issuedstatus,startdate,enddate,createddatetime";
            string l = rest.GetData(GetAuth_Nonce, a_coponset);
            var LCpo = JsonConvert.DeserializeObject<List<dynamic>>(l);
            string sqls = cmd.Stringcolumnsql("4", data, o.PromotionPrismID.ToString(), "");
            List<string> lsql = new List<string>();
            lsql.Add(sqls);
            foreach (var i in LCpo)
            {
                dynamic c = new System.Dynamic.ExpandoObject();
                var a = i.createddatetime.ToString().Replace(".000", "");
                var d = a.Replace(" ", "+");
                c.CouponPrismID = i.sid;
                c.PromotionPrism = o.PromotionPrismID;
                c.CouponValue = o.value;
                c.coupontype = o.general_promo_group;
                c.CouponCode = i.couponcode;
                c.CouponName = dnmCpo[0].setname;
                c.ValueType = "";
                c.ScoreExchange = "";
                c.active = i.issuedstatus;
                c.CreateDate = d;
                c.IsDelete = 0;
                string resp = JsonConvert.SerializeObject(c);
                string sqlss = cmd.Stringcolumnsql("5", resp, i.sid.ToString(), "");
                lsql.Add(sqlss);
            }
            sqlcommon sql = new sqlcommon();
            return sql.runSQL(string.Join(";", lsql)).ToString();
        }
        [HttpGet("GenerateCoupontoCrm")]
        public string GenerateCoupontoCrm(string data)
        {
            
            common cmd = new common();
            string auKey = cmd.GetAuth_Nonce();
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            RestFile rest = new RestFile();
            var paren = cmd.getpromotionCPparent(auKey, o.setid.ToString());
            string addresscp = "api/common/couponsetcoupon?filter=couponsetsid,EQ," + o.CouponID.ToString() + "&cols =*";
            List<dynamic> cp = JsonConvert.DeserializeObject<List<dynamic>>(rest.GetData(auKey, addresscp));
            List<string> sqll = new List<string>();
            foreach (var i in cp)
            {
                string sqls = cmd.setpromotionCPSql(paren, i, o.setname.ToString());
                sqll.Add(sqls);
         
            }
            sqlcommon sql = new sqlcommon();
            return sql.runSQL(string.Join(";", sqll)).ToString();
        }
        [HttpGet("UpdateCoupontoCrm")]
        public string UpdateCoupontoCrm(string data)
        {
         
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            common cmd = new common();
            RestFile rest = new RestFile();
            string auKey = cmd.GetAuth_Nonce();
            string address ="api/common/couponset?cols=*&filter=sid,EQ," + o.coupontype+ "&page_no=1&page_size=1&sort=createddatetime,desc";
            var cp = JsonConvert.DeserializeObject<List<dynamic>>(rest.GetData(auKey, address));
            List<dynamic> paren = cmd.getpromotionCPparent(auKey, cp[0].setid.ToString());
            dynamic c = new System.Dynamic.ExpandoObject();
            var a = o.createddatetime.ToString().Replace(".000", "");
            var d = a.Replace(" ", "+");
            c.CouponPrismID = o.CouponPrismID;
            c.PromotionPrism = "";
            c.CouponValue = "";
            c.coupontype = "";
            if (paren.Count > 0)
            {
                c.PromotionPrism = paren.Select(s => s["sid"].ToString()).ToList();
                c.CouponValue = paren.Select(s => s["validation_subtotal"].ToString()).ToList();
                c.coupontype = paren.Select(s => s["general_promo_group"].ToString()).ToList();
            }
            c.CouponCode = o.CouponCode;
            c.CouponName = cp[0].setname;
            c.ValueType = "";
            c.ScoreExchange = "";
            c.active = o.issuedstatus;
            c.CreateDate = d;
            c.IsDelete = 0;
            string resp = JsonConvert.SerializeObject(c);
            string sqls = cmd.Stringcolumnsql("5", resp, o.CouponPrismID.ToString(), "");
            sqlcommon sql = new sqlcommon();
            return sql.runSQL(sqls).ToString();
 
        }
        [HttpPost("UploadFile")]
        public async Task<IActionResult> Postfile(UploadModel files)
        {
            string FolderWeb = _configuration.GetValue<string>("AppSettings:FolderWeb");
            string FolderApp = _configuration.GetValue<string>("AppSettings:FolderApp");
            string FolderSource = _configuration.GetValue<string>("AppSettings:FolderSource");
            HandleImage _handleImage = new HandleImage(FolderWeb, FolderApp);
            List<dynamic> dnmImg = new List<dynamic>();
            foreach (IFormFile file in files.files)
            {
                var fileName = Path.GetFileName(file.FileName);

                dynamic img =new System.Dynamic.ExpandoObject();
                img.path_source = FolderSource + fileName;
                img.path_web = FolderWeb + fileName;
                img.path_app = FolderApp + fileName;
                dnmImg.Add(img);
                //var path = Path.Combine(FolderSource, "", file.FileName);
                //using (var stream = new FileStream(path, FileMode.Create))
                //{
                //    await file.CopyToAsync(stream);
                //}
                _handleImage.watermarkImgonImage(file, files.wapp, files.happ, files.wweb, files.hweb, files.pst, files.isWatermark, files.isweb, files.isapp);
            }
           
            return Ok(JsonConvert.SerializeObject(dnmImg));
            AmazonUtil amz = new AmazonUtil();
            await amz.UploadFileAsync();
            foreach (IFormFile file in files.files)
            {
                if (file == null || file.Length == 0)
                    return Content("file not selected");
                _handleImage.watermarkImg(file);
                var path = Path.Combine(FolderSource, "",file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            return Ok("ok");
        }
        [HttpPost("register")]
        public void register(nguoidung userst)
        {
            context.nguoidungs.Add(userst);
            context.SaveChanges();
            
        }
        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        [HttpGet("AddColorWoo")]
        public void AddColorWoo(int id)
        {
            woocomerge woo = new woocomerge();
        }
        [HttpGet("diasablewoo")]
        public async Task<string> diasablewoo(string mtk, string vr)
        {
            woocomerge woo = new woocomerge();
            var a =await woo.disablepro(mtk, vr);
            return "";
        }
        [HttpGet("saveImgmiss")]
        public async Task<string> saveImgmiss(string color,string mtk)
        {
            string paths = @"Y:\hinhweb_ninomaxx\"+ mtk;
            common cmd = new common();
            HandleImage img = new HandleImage("", "");
            cmcs3 s3 = new cmcs3();
            int bg = 1;
            DirectoryInfo d = new DirectoryInfo(paths);
            color = color.ToLower();
            string pathsw= @"C:\GitProject\testimg\";
            foreach (var file in d.GetFiles("*.jpg"))
            {
                var name_ = file.Name.ToLower();
                if (name_.Contains(color))
                {
                    string name = mtk + "_" + cmd.replaceChar(color.ToUpper()) + "_" + bg + ".jpg";
                    var bmp = (Bitmap)Image.FromFile(file.FullName);
                    var file_ = img.watermarkStronImage(mtk, bmp, "", "");
                    await s3.UploadFiles_(file_, mtk + "/" + name);
                    bg++;
                }
               
            }
            //string name = obj.mtk + "_" + cmd.replaceChar(obj.attr) + "_" + obj.stt + ".jpg";
            //var bm = cmd.GetImg(obj.src);
            //var file = img.watermarkStronImage(obj.mtk, bm, "", "");

            //await s3.UploadFiles_(file, obj.mtk + "/" + name);
            //string paths = @"C:\GitProject\testimg\";
            //var stream = new System.IO.MemoryStream();
            //file.Save(stream, ImageFormat.Jpeg);
            //stream.Position = 0;
            //await stream.CopyToAsync(new FileStream(paths+@"\"+ name, FileMode.Create));
            return mtk;
        }
        [HttpGet("deletes3")]
        public string deletes3(string link,string order)
        {
            uphinhVnG vng = new uphinhVnG();
            string tk = vng.tokentVNG();
            var fd = vng.createFolderVNG(tk, link);
            vng.DeleteDataImg(fd,tk);
            return fd;
           // cmcs3 s3 = new cmcs3();
            //await s3.deleteFiles3(link,order);
            return "";
        }
        [HttpGet("CreateFolderVNG")]
        public string CreateFolderVNG(string sku)
        {
            uphinhVnG vng = new uphinhVnG();
            string url = @"C:\hinhjnino\"+ sku;
            string[] subdirectoryEntries = Directory.GetDirectories(url);
            string tk = vng.tokentVNG();
            foreach (string subdirectory in subdirectoryEntries)
            {
                string namefolder = subdirectory.Split('\\').Last();
                var fd=vng.createFolderVNG(tk, namefolder);
                string[] fileEntries = Directory.GetFiles(subdirectory);
                foreach (string fileName in fileEntries)
                {
                    string nameimg = fileName.Split('\\').Last();
                   // vng.PutDataImg(fd+ nameimg, tk);
                }
            }
            //vng.createFolderVNG(vng.tokentVNG(),"aab");
            return "";
        }
        [HttpGet("CreatefileVNG")]
        public string CreatefileVNG(string sku)
        {
            uphinhVnG vng = new uphinhVnG();
            string url = @"C:\hinhjnino\" + sku;
            string[] subdirectoryEntries = Directory.GetDirectories(url);
            string tk = vng.tokentVNG();
         
                string namefolder = sku;
                var fd = vng.createFolderVNG(tk, namefolder);
                string[] fileEntries = Directory.GetFiles(url);
                foreach (string fileName in fileEntries)
                {
                    string nameimg = fileName.Split('\\').Last();
                    //vng.PutDataImg(fd + nameimg, tk, fileName);
                }
            
            //vng.createFolderVNG(vng.tokentVNG(),"aab");
            return "";
        }
        [HttpGet("UpfileVNG")]
        public string UpfileVNG()
        {
            uphinhVnG vng = new uphinhVnG();
            string tk = vng.tokentVNG();
            string url = @"C:\Users\tycu\Pictures\ca75b7703ff7e76390fcae810c62e535.jpg";
    
            //var fd = vng.createFolderVNG(tk, "design_code");
            var fd = "https://hcm01.vstorage.vngcloud.vn/v1/AUTH_f34d13925ff54f6ca78e503226f4bd37/dmspimgs/design_code/5026211019201110828064.jpeg";
           // vng.PutDataImg(fd, tk, url);
            return "";
        }
        [HttpGet("DeletefileVNG")]
        public string DeletefileVNG()
        {
            uphinhVnG vng = new uphinhVnG();
            string tk = vng.tokentVNG();
            string url = @"C:\hinhjnino\Untitled.png";
            string nameimg = "Untitled.png";
            var fd = vng.createFolderVNG(tk, "testfolder");
            vng.DeleteDataImg(fd + nameimg, tk);
            return "";
        }
        [HttpGet("GettokentVNG")]
        public string GettokentVNG()
        {
            uphinhVnG vng = new uphinhVnG();
            vng.tokentVNG();
            return "";
        }
        [HttpPost("saveImgbg64")]
        public string saveImgbg64(uphinh obj)
        {
            //string paths = @"C:\GitProject\testimg\";
            string attr = obj.attr.ToString().Trim();
            common cmd = new common();
            HandleImage img = new HandleImage("","");
            //cmcs3 s3 = new cmcs3();
            string name = obj.mtk + "_" + cmd.replaceChar(attr) + "_" + obj.stt + ".jpg";
            var bm = cmd.GetImg(obj.src);
            var file = img.watermarkStronImage(obj.mtk, bm, "", "");

           
            uphinhVnG vng = new uphinhVnG();
            string tk = vng.tokentVNG();
            var fd = vng.createFolderVNG(tk, obj.mtk + "/" + name); 
            vng.UploadImgVNG(fd, file,tk);
            
            ///await s3.UploadFiles_(file, obj.mtk+"/"+name);
            //string paths = @"C:\GitProject\testimg\";
            //var stream = new System.IO.MemoryStream();
            //file.Save(stream, ImageFormat.Jpeg);
            //stream.Position = 0;
            //await stream.CopyToAsync(new FileStream(paths+@"\"+ name, FileMode.Create));
            return fd;
        }
        public string updateItemPro(string key_login,string Id,dynamic item)
        {
        
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", key_login);
            var urls = cf.prism + "v1/rest/customer?cols=*&filter=sid,EQ," + Id.Trim() + "&page_no=1&page_size=1";
            var obj = webClient.DownloadString(urls);
            dynamic objs = JsonConvert.DeserializeObject<dynamic>(obj);
            var row_version = "";
            //try
            //{
                 row_version = objs[0].row_version;
                var addresses=objs[0].addresses[0];
            string urls2 = cf.prism + "v1/rest/customer/"+ Id + "?filter=row_version,eq," + row_version + "";
            
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls2);
            myHttpWebRequest_.Headers.Add("Auth-Session", key_login);
            myHttpWebRequest_.Method = "PUT";
            myHttpWebRequest_.Accept = "application /json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            List<dynamic> o = new List<dynamic>();
            dynamic l = item;
            o.Add(l);
            string jsonstr = JsonConvert.SerializeObject(o);
            var postData = jsonstr;

            var data = Encoding.UTF8.GetBytes(jsonstr);

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(jsonstr);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();
            string returnString = response.StatusCode.ToString();

            return "cập nhật thành công";
            //}
            //catch
            //{
            //    return "Không tồn tại khách hàng";
            //}
        }
    }
}
public class upproducweb {
    public string data { get; set; }
    public string cate_ { get; set; }
    public string mtk { get; set; }
    public string tast { get; set; }
    public string chatlieu { get; set; }
    public string namewebsite { get; set; }
    public bool webnino { get; set; }
    public bool webnm { get; set; }
    public bool weblaginews { get; set; }

}