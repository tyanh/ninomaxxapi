﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_net_core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_net_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CouponApiController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet("CreateCode")]
        public string CreateCode(string couponsetsid,string sbssid,string couponcode, string au)
        {
            common cmd = new common();
            RestFile refs = new RestFile();
            if(au==null)
                au = cmd.GetAuth_Nonce();
            var dtcv = new
            {
                couponcode = couponcode,
                couponsetsid = couponsetsid,
                issuedstatus = 0,
                originapplication = "RProPrismWeb",
                sbssid = sbssid
            };
            var x = new
            {
                data = new[] { dtcv }

            };
            string urls = "api/common/couponsetcoupon";
            string json = JsonConvert.SerializeObject(x);
            try
            {
                var postcv = refs.PostDatav_22(au, urls, json);
                return "0";
            }
            catch
            {
                return "-1";
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        [HttpGet("UpcodeCoupon")]
        public string UpcodeCoupon(string au, string code,string usedqty, string start, string end, string issuedstatus)
        {
            try
            {
                common cmd = new common();
                RestFile rest = new RestFile();
                if (au == null)
                    au = cmd.GetAuth_Nonce();
                if (start == null)
                {
                    start = "0";
                    end = "1";
                }
                string urlupdate = "api/common/couponsetcoupon?filter=couponcode,EQ," + code.ToUpper() + "&cols=*&page_no=1&page_size=1";
                var a = rest.GetData(au, urlupdate);
                List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(a);
                string vsion = dnm[0].rowversion.ToString();
                string sid = dnm[0].sid.ToString();
                dynamic data = new System.Dynamic.ExpandoObject();
                DateTime now = DateTime.Now;
                data.rowversion = int.Parse(vsion);
                if (issuedstatus != null)
                    data.issuedstatus = int.Parse(issuedstatus);
                if (usedqty != null)
                    data.usedqty = int.Parse(usedqty);
                if (start != null)
                    data.startdate = cmd.dateAsStringd_m_y(start);// "2020-02-29T11:42:47.000Z";
                if (end != null)
                    data.enddate = cmd.EnddateAsString_m_y(end);

                data.modifieddatetime = cmd.dateAsString(now);
                var x = new
                {
                    data = new[] { data }
                };

                string json2 = JsonConvert.SerializeObject(x);
                string urlupdatei = "api/common/couponsetcoupon/" + sid + "?filter=rowversion,eq," + 1 + "";
                string u = rest.updateItemcoupon(au, urlupdatei, json2);
                return "0";
            }
            catch
            {
                return "-1";
            }
        }
    }
}
