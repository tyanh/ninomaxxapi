﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using API_net_core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_net_core.Controllers
{
    [Route("api/[controller]")]
    public class NinoBillController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public string Get(string sid)
        {
            storePrism store = new storePrism();
            var store_iv = store.GET_COUPON_BY_INVOICE(sid);
            List<dynamic> we = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(store_iv));
            return JsonConvert.SerializeObject(we);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
       
        [HttpPost("updatebillIntoCrmPosT")]
        public string updatebillIntoCrmPosT(string data)
        {
           
                var bill = data;
                common cmd = new common();
                RestFile rest = new RestFile();
                string billid = Guid.NewGuid().ToString();

                try
                {

                    var b = JsonConvert.DeserializeObject<dynamic>(bill);

                    var cp = b.CouponCode.ToString();
                    var a = b.CreateDate.ToString().Replace(".000", "");
                    var d = a.Replace(" ", "+");
                    if (b.CustomerID.ToString() != "")
                    {
                        var core = cmd.ctmCrmExit(b.CustomerID.ToString());
                        if (core == "[]")
                        {
                            cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), billid + "_thieuctm.txt", "api/NinoBill/updatebillIntoCrm?data=" + bill);

                            return "ko tồn tại ctm";
                        }
                    }
                    List<dynamic> lcpon = new List<dynamic>();
                    b.CreateDate = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:sszzz", System.Globalization.CultureInfo.InvariantCulture);
                storePrism store = new storePrism();
                var store_iv = store.GET_COUPON_BY_INVOICE(b.BillNo.ToString());
                //var store_iv = store.GET_COUPON_BY_INVOICE("575851948000632055");
                List<dynamic> lcoups = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(store_iv));
                if (lcoups.Count >0)
                    {
                                                
                        foreach (var cc in lcoups)
                        {
                            var type = cc.TYPE_DISCOUNT.ToString();
                            if (type.ToLower() == "phần trăm")
                                type = "%";
                            var CouponType = "HT";
                            string desc_ = cc.DESCRIPTION.ToString();
                            if (desc_.Contains("sinh nhật"))
                                CouponType = "SN";
                            else if (desc_.Contains("giảm giá"))
                                CouponType = "GG";
                            dynamic c = new
                            {
                                CouponPrismID = cc.COUPON_SID.ToString(),
                                CouponCode = cc.COUPON_CODE.ToString(),
                                CouponType = CouponType,
                                CouponName = cc.DESCRIPTION.ToString(),
                                CouponValue = cc.DISCOUNT_AMOUNT.ToString(),
                                ValueType = type,
                                CreateDate = DateTime.Now.ToString(),
                            };
                            lcpon.Add(c);
                        }

                    }
                    b.CouponCode = JsonConvert.SerializeObject(lcpon);
                    foreach (var i in b.BillDetail)
                    {
                        i.ProductName = HttpUtility.HtmlEncode(i.ProductName.ToString());
                        i.ProductColor = HttpUtility.HtmlEncode(i.ProductColor.ToString());
                    }
                    string resp = JsonConvert.SerializeObject(b);
                    string sqls = cmd.Stringcolumnsql("1", resp, b.BillNo.ToString(), "");
                    sqlcommon sql = new sqlcommon();
                    sql.runSQL(sqls);
                    return "ok";
                }
                catch
                {
                    cmd.CreateFile(Path.Combine(@"wwwroot\bill\"), billid + ".txt", "api/NinoBill/updatebillIntoCrm?data=" + bill);
                    return "loi";
                }
           
            return "";
        }
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
