﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using API_net_core.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace API_net_core.Controllers
{
    public class HomeController : Controller
    {
        fileconfig cf = new fileconfig();
        public IActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        //public IActionResult Index(string numpage)
        //{
        //    common cmd = new common();
        //    string a = cmd.GetAuth_Nonce();
        //    var o= cmd.inventory(a, numpage);
        //    return View();
        //}
        [HttpPost]
        public IActionResult Index(string numpage)
        {
           
            common cmd = new common();
            string a = cmd.GetAuth_Nonce();

            ViewBag.code = data(a);
            //var o = cmd.updateItem(a);
            return View();
        }
        public string data(string d)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", d);
           // string a = url + "v1/rest/document/529754076000157195/item";
            string a = cf.prism + "v1/rest/document/529754076000157195/item?cols=lty_orig_points_earned,note4,item_path,discount_reason,is_competing_component,activity2_percent,ship_method_id,order_ship_method_id,employee3_name,item_origin,employee1_sid,employee3_full_name,central_return_commit_state,so_cancel_flag,employee2_login_name,inventory_item_type,sid,item_description3,note8,item_description1,lty_piece_of_tbe_points,hist_discount_perc5,cost,udf_string04,st_address_line3,package_sequence_number,invn_sbs_item_sid,item_status,qty_available_for_return,discounts,original_cost,st_primary,hist_discount_reason4,gift_add_value,note3,lot_type,ref_order_item_sid,gift_transaction_id,hist_discount_amt1,employee1_full_name,total_discount_reason,order_quantity_filled,st_company_name,post_date,ship_method,commission2_amount,inventory_on_hand_quantity,tax_perc_lock,dcs_code,activity3_percent,price,employee1_login_name,central_document_sid,order_ship_method_sid,employee3_login_name,tax2_amount,st_address_line2,fulfill_store_sbs_no,employee5_sid,returned_item_invoice_sid,udf_string01,udf_float01,price_before_detax,ship_id,hist_discount_reason5,employee2_name,commission_code,spif,price_lvl_name,style_sid,commission3_amount,dip_discount_amt,hist_discount_perc2,st_postal_code_extension,employee3_id,commission_percent,item_pos,st_id,activity4_percent,returned_item_qty,detax_flag,custom_flag,st_price_lvl,discount_amt,total_discount_amount,row_version,dip_tax_amount,employee5_id,activation_sid,lty_redeem_applicable_manually,order_ship_method,enhanced_item_pos,item_description2,employee5_name,manual_disc_reason,tax_code_rule2_sid,udf_string05,total_discount_percent,tax_amount,package_number,scan_upc,so_number,shipping_amt_bdt,employee3_sid,invn_item_uid,item_type,employee5_full_name,subsidiary_number,note10,tax_code_rule_sid,employee2_sid,customer_field,tax2_percent,udf_date01,st_address_line5,original_component_item_uid,ref_sale_item_pos,manual_disc_value,tax_code2,original_price_before_detax,lty_points_earned,st_last_name,fulfill_store_sid,st_country,udf_string03,st_first_name,tax_code,hist_discount_reason2,employee4_sid,price_lvl,lty_orig_price_in_points,note2,commission_level,item_lookup,vendor_code,commission5_amount,alu,schedule_number,serial_number,promotion_flag,udf_float02,so_deposit_amt,lty_price_in_points,st_security_lvl,ship_method_sid,created_datetime,hist_discount_amt3,inventory_quantity_per_case,inventory_use_quantity_decimals,commission_amount,hist_discount_perc3,st_address_line1,user_discount_percent,central_item_pos,tracking_number,st_title,ref_order_doc_sid,hist_discount_perc4,tenant_sid,kit_flag,kit_type,lot_number,discount_perc,override_max_disc_perc,modified_datetime,udf_float03,original_tax_amount,commission4_amount,hist_discount_amt4,st_detax_flag,st_tax_area_name,orig_document_number,activity5_percent,archived,package_item_uid,lty_price_in_points_ext,item_size,original_price,store_number,lty_pgm_name,order_type,employee4_id,st_price_lvl_name,attribute,lty_piece_of_tbr_points,orig_subsidiary_number,employee1_name,delete_discount,original_component_item_sid,employee2_id,item_description4,serial_type,activity_percent,employee4_login_name,gift_activation_code,gift_expire_date,tax_char,hist_discount_reason3,document_sid,hist_discount_perc1,udf_string02,created_by,ref_sale_doc_sid,shipping_amt,package_item_sid,note5,st_primary_phone_no,return_reason,employee5_login_name,note9,employee1_id,note1,lty_type,price_lvl_sid,st_address_uid,st_address_line6,apply_type_to_all_items,lty_piece_of_tbr_disc_amt,tax_area_name,promo_disc_modifiedmanually,st_cuid,note6,hist_discount_amt5,orig_sale_price,tax_area2_name,dip_price,modified_by,st_postal_code,st_email,orig_store_number,quantity,note7,hist_discount_amt2,gift_quantity,controller_sid,lty_pgm_sid,employee2_full_name,dip_tax2_amount,origin_application,employee4_full_name,employee4_name,st_tax_area2_name,hist_discount_reason1,manual_disc_type,st_address_line4,tax_percent,tax_char2,fulfill_store_no,from_centrals,st_customer_lookup,maxaccumdiscpercent,maxdiscpercent,promo_gift_item&sort=enhanced_item_pos,desc";
            var obj = webClient.DownloadString(a);
            return obj;
        }
        public string databill(string d)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Auth-Session", d);
            string a = cf.prism + "v1/rest/document?cols=*&filter=created_datetime,ge,2019-01-01T18:23:17.000+07:00&page_no=1&page_size=10";
            //string a = url + "v1/rest/document?cols=*&page_no=1&page_size=10";
            var obj = webClient.DownloadString(a);
            return obj;
        }
    }
}