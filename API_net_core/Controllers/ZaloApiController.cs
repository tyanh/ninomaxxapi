﻿using API_net_core.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;

namespace API_net_core.Controllers
{
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ZaloApiController : Controller
    {
       
        [HttpGet("gettokent")]
        public string gettokent()
        {

            var ttk=newtokentzalo();
            return ttk;
        }
        [HttpGet("infozalo")]
        public string infozalo(string idrecipient)
        {
            fileconfig config = new fileconfig();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("access_token", gettokent());
            string uurl = config.apizalo + "getprofile?data={'user_id':" + idrecipient + "}";
            var obj = webClient.DownloadString(uurl);
            var objs = JsonConvert.DeserializeObject<dynamic>(obj);
            if (objs.message.ToString() == "Success")
                return "0";
            return "1";
        }
        [HttpPost("webzalo")]
        public void webzalo(dynamic data)
        {
            if (data.event_name.ToString() == "follow")
            {
                var follower = JsonConvert.DeserializeObject<dynamic>(data.follower.ToString());
                sendregisterzalo(follower.id.ToString());
            }
            else if (data.event_name.ToString() == "user_send_text")
            {
                var message = JsonConvert.DeserializeObject<dynamic>(data.message.ToString());
                if (message.text.ToString().ToLower() == "#dangky")
                {
                    var sender = JsonConvert.DeserializeObject<dynamic>(data.sender.ToString());
                    var recipient = JsonConvert.DeserializeObject<dynamic>(data.recipient.ToString());
                    string idrecipient = recipient.id.ToString();

                    string idcheck = sender.id.ToString();


                    var checkif = getinforzalo(data.user_id_by_app.ToString());
                    if (checkif == "1")
                        sendregisterzalo(idcheck);
                    else
                        sendmessagezalo(idcheck);
                }
                else if (message.text.ToString().ToLower() == "#vongxoaytest")
                {
                    var sender = JsonConvert.DeserializeObject<dynamic>(data.sender.ToString());
                    sendlink(sender.id.ToString());
                    /* List<string> cl = new List<string>() { "contents" };
                     List<string> vl = new List<string>() { JsonConvert.SerializeObject(data) };
                     sqlmissdata miss = new sqlmissdata();
                     string iser = miss.insertsqlstr("missdata", cl, vl);
                     miss.runSQL(iser);*/
                }
            }
            else if (data.event_name.ToString() == "oa_send_text")
            {
                var message = JsonConvert.DeserializeObject<dynamic>(data.message.ToString());
                if (message.text.ToString().ToLower() == "#dangky")
                {
                    var recipient = JsonConvert.DeserializeObject<dynamic>(data.recipient.ToString());
                    string idcheck = recipient.id.ToString();
                    sendregisterzalo(idcheck.ToString());

                }
            }
            List<string> cl = new List<string>() { "contents" };
            List<string> vl = new List<string>() { JsonConvert.SerializeObject(data) };
            sqlmissdata miss = new sqlmissdata();
            string iser = miss.insertsqlstr("missdata", cl, vl);
            miss.runSQL(iser);
        }
        public string getinforzalo(string user_id)
        {
            fileconfig config = new fileconfig();
            WebClient webClient = new WebClient();
            webClient.Headers.Add("access_token", gettokent());
            string uurl = config.apizalo + "getprofile?data={'user_id':" + user_id + "}";
            var obj = webClient.DownloadString(uurl);
            var objs = JsonConvert.DeserializeObject<dynamic>(obj);
            if (objs.message.ToString() == "Success")
                return "0";
            return "1";
        }
        public string sendlink(string user_id)
        {
            fileconfig config = new fileconfig();
            var data = new
               {
                   recipient = new
                   {
                       user_id = user_id
                   },
                   message = new
                   {
                       attachment = new
                       {
                           payload = new
                           {
                               elements = new[] {
                                           new {
                                              
                                                 image_url = config.imgzalo,
                                                 subtitle = "Trở thành thành viên của Ninomaxx Concept để nhận được thông tin và các chương trình khuyến mãi hấp dẫn hàng tháng",
                                                 title= "Vòng xoay may mắn",
                                                 default_action=new {
                                                  type= "oa.open.url",
                                                  url= $"https://lixitet.ninomaxxconcept.com?uid={user_id}&check=true"
                                                }
                                            } }
                                                        ,
                               template_type = "list"
                           },
                           type = "template"
                       },
                       text = "hello, world!"
                   }
               };
            return postzalotouser(JsonConvert.SerializeObject(data));
        }
        public string sendmessagezalo(string user_id)
        {
            fileconfig config = new fileconfig();
            var data = new
            {
                recipient = new
                {
                    user_id = user_id
                },
                message = new
                {
                    text = "Xin hân hạnh được phục vụ quý khách. Để có thể nhận được đầy đủ các ưu đãi. Quý khách vui lòng 'Follow' / 'Quan tâm' trang Zalo OA chính thức của 'Ninomaxx Concept' và Đăng ký thành viên. Xin chân thành cám ơn quý khách."
                }
            };
            string urlpost = config.apizalo + "message";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            myHttpWebRequest_.Headers.Add("access_token", gettokent());
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            string body = JsonConvert.SerializeObject(data);
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(body);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            return str;

        }
        public string newtokentzalo()
        {

            fileconfig config = new fileconfig();
            double n = (DateTime.Now - Startup.expxalo).TotalHours;
            if (n > 20)
                return Startup.zl_token;
            string endPoint = "https://oauth.zaloapp.com/v4/oa/access_token";  
            var client = new HttpClient();

                        client.DefaultRequestHeaders.Add("secret_key", config.secretkeyzalo);
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("refresh_token", config.refeshtokentzalo),
                new KeyValuePair<string, string>("app_id", config.appidzalo),
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
            

             };

                          HttpResponseMessage httpResponseMessage =
                            client.PostAsync(endPoint, new FormUrlEncodedContent(pairs)).GetAwaiter().GetResult();
            var rs =JsonConvert.DeserializeObject<dynamic>(httpResponseMessage.Content.ReadAsStringAsync().Result);
            
            Startup.zl_token = rs.access_token;
            Startup.expxalo = DateTime.Now;
            return rs;

            
        }
        public string postzalotouser(string data)
        {
            fileconfig config = new fileconfig();
            string urlpost = config.apizalo + "message";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            myHttpWebRequest_.Headers.Add("access_token", gettokent());
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(data);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            return str;
        }
            public string sendregisterzalo(string user_id)
        {
            fileconfig config = new fileconfig();
            var data = new
            {
                recipient=new {
                    user_id= user_id
                },
                message=new {
                    attachment=new {
                                        payload=new {
                                            elements=new[] {
                                               new {
                                                image_url= config.imgzalo,
                                                subtitle = "Trở thành thành viên của Ninomaxx Concept để nhận được thông tin và các chương trình khuyến mãi hấp dẫn hàng tháng",
                                                title= "Đăng ký thành viên"
                                              } }
                                            ,
                                            template_type= "request_user_info"
                                    },
                      type= "template"
                                },
                    text= "hello, world!"
                  }
                    };
                string urlpost = config.apizalo+ "message";
                HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
                myHttpWebRequest_.Headers.Add("access_token", gettokent());
                myHttpWebRequest_.Method = "POST";
                myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
                myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
                myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
                string body = JsonConvert.SerializeObject(data);
                using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
                {
                    requestStream.Write(body);
                }
                var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string str = reader.ReadLine();
                return str;
            
        }
        [HttpGet("zalo-tokent")]
        public string zalotokent(string code)
        {
            Startup.refresh_token = code;
            return Startup.refresh_token;

        }
        [HttpGet("testwebzalo")]
        public string testwebzalo(string userid)
        {
            return sendmessagezalo(userid);
       
        }
        [HttpGet("tkwebzalo")]
        public string access() {
            fileconfig config = new fileconfig();

            string urlpost = config.zalooa + "access_token";
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urlpost);
            myHttpWebRequest_.Headers.Add("secret_key", config.secretkeyzalo);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/x-www-form-urlencoded";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;

            var data = new[] {

            new KeyValuePair<string, string>("refresh_token", Startup.refresh_token),
            new KeyValuePair<string, string>("app_id", config.appidzalo),
            new KeyValuePair<string, string>("grant_type", "refresh_token"),
                };

            using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
            {
                requestStream.Write(data);
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();
            return str;
        }
    }
}
