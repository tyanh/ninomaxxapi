﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using API_net_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace API_net_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GhtkController : Controller
    {
        fileconfig cf = new fileconfig();
        string urlPrism = "http://203.205.21.52:8080/";
        string urlCrm = "";
        string url = "https://services.giaohangtietkiem.vn/";
        string token = "db01c677B9875Ec297Df46De8e682551feA41F9a";
        //string url = "https://dev.ghtk.vn/";
        //string token = "7b9fFb974E60AA6755afFF467826C56c46440C2e";
        [HttpGet("Gettrangthai")]
        public string Gettrangthai(string code)
        {
            string url_ = "services/shipment/v2/" + code;
            var obj = Getdata(url_);
            return obj;
        }
        [HttpGet("updateStore")]
        public string updateStore(string sid,string au)
        {
            common cmd = new common();
            au = cmd.GetAuth_Nonce();
            RestFile rest = new RestFile();
            var urls ="v1/rest/store?filter=sid,eq,"+ sid + "&cols=row_version&page_no=1&page_size=10";
            var obj = rest.GetDataWebClient(au, urls);
            var objs = JsonConvert.DeserializeObject<List<dynamic>>(obj);
            var rowvs= objs[0].row_version;
            var _l = "v1/rest/store/"+sid+"?filter=row_version,eq,"+ rowvs;
            var n = new[]
            {
                new{
                address1 = "1223",
                address2= "1223",
                address3= "1223",
                active=false,
                status=1,
                address5="",
                phone1="",
                }
            };
            string data = JsonConvert.SerializeObject(n);
            var u = rest.PutDatav_22(au, _l, data);
            return objs[0].row_version;
        }
        [HttpGet("updateShipment")]
        public string updateShipment(string hash)
        {
            return "0";
        }
        [HttpGet("Dangdon")]
        public string Dangdon(string data)
        {
            string url_ = "services/shipment/order/?ver=1.5";
            var rs = Postdata(data, url_);
            return rs;
        }
        public string Huydon(string code)
        {
            string url_ = "services/shipment/cancel/partner_id:" + code;
            var rs = Postdata("", url_);
            return rs;
        }
        public string Getdata(string urls)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Token", token);
            string uurl = url + urls;
            var obj = webClient.DownloadString(uurl);
            return obj;
        }
        public string Postdata(string data, string urlp)
        {

            var urls = url + urlp;
            HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
            myHttpWebRequest_.Headers.Add("Token", token);
            myHttpWebRequest_.Method = "POST";
            myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
            myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
            myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
            if (data != "")
            {
                using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
                {
                    requestStream.Write(data);
                }
            }
            var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            string str = reader.ReadLine();

            return str;

        }
    }
}