﻿using API_net_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Controllers
{
    [Route("api/[controller]")]
   
    public class TestApiController : Controller
    {
        woocmtest woo = new woocmtest();
        [HttpGet("GetCate")]
        public async Task<string> GetCate() {
            var cate =await woo.catelogy();
            return JsonConvert.SerializeObject(cate);
        }
        [HttpPost("CreateCateParent")]
        public async Task<string> CreateCateParent([FromBody] JObject data)
        {
            var cate = await woo.createcateparent(data);
            return "0";
        }
        public async Task<string> CreateCate()
        {
            var cate = await woo.catelogy();
            return JsonConvert.SerializeObject(cate);
        }
    }
}
