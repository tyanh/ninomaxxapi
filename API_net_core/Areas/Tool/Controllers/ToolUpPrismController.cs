﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using API_net_core.Areas.Tool.Models;
using API_net_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using Microsoft.AspNetCore.Mvc.RazorPages;
using ClosedXML.Excel;
using WooCommerceNET.WooCommerce.v2;
using System.Net;
using System.Text;
using System.Diagnostics;
using WooCommerceNET.Base;
using System.Data;
using DocumentFormat.OpenXml.Office.CustomUI;
using Microsoft.IdentityModel.Tokens;
using HtmlAgilityPack;

namespace API_net_core.Areas.Tool.Controllers
{
    [Area("Tool")]
    public class ToolUpPrismController : Controller
    {
        fileconfig cf = new fileconfig();
        common cmd = new common();
        SysnWeb woo = new SysnWeb("nino");
        string urlItem = "v1/rest/inventory?count=true&cols=sid,upc,description1,description2,attribute,item_size,order_cost&page_no=1&page_size=50&sort=description1,desc";
        public IActionResult invoice()
        {
            //List<Order> o =await woo.getOrder("1");

            return View();
        }
  
        public async Task<IActionResult> exportProduct()
        {
            List<Product> lp = new List<Product>();
            int p = 1;
            do
            {

                var obj = await woo.allProductspublish(p.ToString()) ;
                if (obj.Count <= 0)
                {
                    break;
                }
                else
                {
                    lp.AddRange(obj);
                    p++;
                }
            } while (p>0);
            DataTable table = new DataTable("Detail");
            table.Columns.Add("mtk", typeof(string));
            table.Columns.Add("tên", typeof(string));
            table.Columns.Add("giá bán", typeof(float));
            table.Columns.Add("giá giảm", typeof(float));
            foreach (var row in lp)
            {
                float dow = 0;
                var doc3 = new HtmlDocument();
                doc3.LoadHtml(row.price_html);
                var v = doc3.DocumentNode.SelectNodes("//*[\"del\"]");
                var pp = v[0].InnerText.Replace("đ","").Replace(",","");
                try
                {
                    dow = float.Parse(row.price.ToString());
                }
                catch
                {

                }
                float dow2= 0;
                try
                {
                    dow2 = float.Parse(pp.ToString());
                }
                catch
                {

                }
                table.Rows.Add(
                        row.sku.ToString(),
                        row.name.ToString(),
                        dow2,
                         dow


                );


            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "sanphamwoo.xlsx");
                }
            }
            return View();
        }
        public IActionResult Ghtk()
        {
            //List<Order> o =await woo.getOrder("1");
            string hash = Request.Query["hash"];
            Response.WriteAsync("");
            return View();
        }
        public IActionResult RemoveDiscount()
        {
            RestFile refs = new RestFile();
            string au = cmd.GetAuth_Nonce();
            List<dynamic> stores = new List<dynamic>();
            var page = 2;
            var store = "v1/rest/store?cols=*&page_no=1&page_size=100";
            var items_ = refs.GetDataWebClient(au, store);
            var store_ = JsonConvert.DeserializeObject<List<dynamic>>(items_);
            stores.AddRange(store_);
            /*while (store_.Count > 0)
            {
               
                stores.AddRange(store_);
                store_ = new List<dynamic>();
                var storestr = "v1/rest/store?cols=*&page_no="+page+"&page_size=100";
                var item = refs.GetDataWebClient(au, storestr);
                store_ = JsonConvert.DeserializeObject<List<dynamic>>(items_);
                page++;
            }*/
            ViewBag.txt = "đang chờ";
            return View(stores);
        }
        string createvoucher(string storeId,string fileName)
        {
            common cmd = new common();
            RestFile refs = new RestFile();
            string au = cmd.GetAuth_Nonce();
            string create = "api/backoffice/receiving";
            var dtcreate = new
            {
                data = new[] {
                    new{
                        originapplication="RProPrismWeb",
                        sbssid="605135163000180340",
                        clerksid="605067254000112260",
                        storesid=storeId,
                        publishstatu=1,
                        voutype=0,
                        vouclass=0,
                       // vendsid="605135181000182648"
                    }
                }

            };
            var rs = refs.PostDatav_22(au, create, JsonConvert.SerializeObject(dtcreate));
            var jcreate = JsonConvert.DeserializeObject<dynamic>(rs);
            var datanew = jcreate["data"][0];
            var sidnew = datanew["sid"].ToString();
            var rowversionn = datanew["rowversion"].ToString();
            var put1 = "api/backoffice/receiving/" + sidnew;
            var dtput1 = new
            {
                data = new[] {
                       new{
                           rowversion=int.Parse(rowversionn),
                           vendsid="605135183000185757"
                       }
                }
            };
            var rsput1 = refs.PutDatav_22(au, put1, JsonConvert.SerializeObject(dtput1));

            var post2 = "api/backoffice/receiving/" + sidnew + "/recvitem";

            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().ToList();
            foreach (var row in ws2)
            {
                var upc = row.Cell(1).Value.ToString();
                if (upc == "")
                    break;
                var sididpr = "v1/rest/inventory?filter=upc,eq,"+ upc + "&cols=*&page_no=1&page_size=1";
                var items_ = refs.GetDataWebClient(au, sididpr);
                var sid = JsonConvert.DeserializeObject<List<dynamic>>(items_)[0]["sid"];

                var dtcreate2 = new
                {
                    data = new[] {
                    new{
                       originapplication="RProPrismWeb",
                       itemsid=sid,
                       cost=20000,
                       qty=1,
                       upc=upc,
                       description1="ACCESSORIES",
                       serialno=(System.Decimal?) null,
                       lotnumber=(System.Decimal?) null,
                       serialtype=0,
                       lottype=0,
                       vousid=sidnew
                    }
                }

                };
                var rs2 = refs.PostDatav_22(au, post2, JsonConvert.SerializeObject(dtcreate2));
            }
            var getrvs = "api/backoffice/receiving/" + sidnew + "?cols=*,recvterm,recvterm.termtype";
            var getvs = refs.GetData(au, getrvs);
            var jvsion = JsonConvert.DeserializeObject<dynamic>(getvs);
            var newrowversionn = jvsion[0]["rowversion"].ToString();
            var put2 = "api/backoffice/receiving/" + sidnew;
            var dtcreate3 = new
            {
                data = new[] {
                    new{
                      rowversion=int.Parse(newrowversionn),
                      status=4,
                      approvbysid="605067254000112260",
                      approvdate=cmd.EnddateAsString(DateTime.Now),
                      approvstatus=2,
                      publishstatus=2
                    }
                }

            };
            var rs3 = refs.PutDatav_22(au, put2, JsonConvert.SerializeObject(dtcreate3));
            var js = JsonConvert.DeserializeObject<dynamic>(rs3)["data"][0];
            return JsonConvert.SerializeObject(js);
        }
        [HttpPost]
        public IActionResult RemoveDiscount(IFormFile files, string store)
        {
        List<dynamic> rsgia = new List<dynamic>();
        string FolderSource = Path.Combine(@"wwwroot\Excel\");
        var path = Path.Combine(FolderSource, "", files.FileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);
        files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var rs=createvoucher(store, fileName);
            /* var workbook = new XLWorkbook(fileName);
             var ws1 = workbook.Worksheet(1);
             var ws2 = ws1.Rows().Skip(3).ToList();

            int bg = 0;
                 DataTable table = new DataTable("product");
                    table.Columns.Add("DESCRIPTION1", typeof(string));
                 table.Columns.Add("NGÀY CHIA", typeof(string));
                 table.Columns.Add("giá bán", typeof(int));
                 table.Columns.Add("% GiẢM GIÁ", typeof(string));
                 table.Columns.Add("GIÁ BÁN SAU GiẢM GIÁ", typeof(int));
                 table.Columns.Add("link", typeof(string));
                 woocomerge wo = new woocomerge();
             List<Variation> v_ = new List<Variation>();
                 List<string> lUrl =new List<string>();
                 foreach (var j in ws2)
                 {
                     var ii = j.Cell(1).Value.ToString().Split('-');
                     string mtk = ii[ii.Count() - 1].ToString();
                     var ps = await wo.GProduct(mtk);
                     string link = "";
                     if(ps.Count>0)
                     {
                         link = ps[0].permalink;
                         lUrl.Add(link);
                     }
                     else
                     {
                         lUrl.Add("");
                     }
                     var desc = j.Cell(1).Value.ToString();
                     var date = "";
                     var pr = j.Cell(2).Value.ToString().Replace(",", "");
                     var pt = j.Cell(3).Value.ToString();
                     var d = j.Cell(4).Value.ToString().Replace(",", "");
                     table.Rows.Add(
                         desc,
                            date,
                            pr,
                             pt,
                            d,
                            link
                             );
                 }


                 using (XLWorkbook wb = new XLWorkbook())
                 {
                     wb.Worksheets.Add(table);
                     using (MemoryStream stream = new MemoryStream())
                     {
                         wb.SaveAs(stream);
                         return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "product.xlsx");
                     }
                 }*/
            RestFile refs = new RestFile();
            string au = cmd.GetAuth_Nonce();
            List<dynamic> stores = new List<dynamic>();
            var page = 2;
            var store_2 = "v1/rest/store?cols=*&page_no=1&page_size=100";
            var items_ = refs.GetDataWebClient(au, store_2);
            var store_ = JsonConvert.DeserializeObject<List<dynamic>>(items_);
            stores.AddRange(store_);
            ViewBag.txt = rs;
            return View(stores);
            //return View(lUrl);
        }
        [HttpPost]
        public async Task<IActionResult> invoice(string after, string before)
        {
            ViewBag.after = after;
            ViewBag.before = before;
            List<Order> orders = new List<Order>();
            int pcolor = 1;
            DateTime f = woo.cvdate(after);
            string fr = f.ToString("yyyy-MM-ddT00:00:00");
            DateTime t = woo.cvdate(before).AddDays(1);
            string tt = t.ToString("yyyy-MM-ddT00:00:00");
            do
            {
   
                var obj = await woo.getOrder(pcolor.ToString(),fr,tt);
                if (obj.Count <= 0)
                {
                    pcolor = -1;
                    break;
                }
                else
                {

                    orders.AddRange(obj);
                    pcolor++;
                }
            } while (pcolor > 0);
            return View(orders);
        }
        public async Task<IActionResult> Colorwoo()
        {
            List<ProductAttributeTerm> colors = new List<ProductAttributeTerm>();
            int pcolor = 1;
            do
            {
                var obj = await woo.getcolor(pcolor.ToString());
                if (obj.Count <= 0)
                {
                    pcolor = -1;
                    break;
                }
                else
                {

                    foreach (var i in obj)
                    {

                        colors.Add(i);

                    }

                    pcolor++;
                }
            } while (pcolor > 0);
            
            var objP = await woo.searchProduct("1812161");
            var avr = await woo.Variationss(objP);
            List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
            List<string> colors_ = new List<string>();
            List<string> sizes = new List<string>();
            foreach (var i in avr)
            {
                var ia = i.attributes[0].option.ToLower();
                var ii = colors.FirstOrDefault(s=>s.slug.ToString()==ia);
                var gg = colors.FirstOrDefault(s=>s.slug== "be-str");

            }
            foreach(var gg in colors)
            {
                var d = gg.slug;
            }
            //foreach(var i in o)
            //await woo.removecolor(i);
            return View();
        }
        public IActionResult splitInvoid()
        {

            return View();
        }
        [HttpPost]
        public IActionResult splitInvoid(string o)
        {
            common cmd = new common();
            var table = cmd.GetTable();
            string js = JsonConvert.SerializeObject(table);
            RestFile reft = new RestFile();
            var rr = "http://119.82.135.36/api/Api/tranferInvoid?table=" + js;
            var a = reft.GetDataWebClient("", rr);
            return View();
            foreach (DataRow row in table.Rows) // Duyệt từng dòng (DataRow) trong DataTable
            {
                var dt = row["Drug"].ToString();
                foreach (var item in row.ItemArray) // Duyệt từng cột của dòng hiện tại
                {
                    Console.Write("Item: ");
                    Console.WriteLine(item); // In ra giá trị của ô với dòng và cột tương ứng
                    var s = item;
                }
            }
            return View();
            string au =HttpContext.Session.GetString("au");
            var datas = new {
                originapplication="RProPrismWeb",
                status=3,
                insbssid= "544934950000173213",
                instoresid= "544935116000191205",
                workstation=22,
                outstoresid= "544935114000152223",
                origstoresid= "544935114000152223",
                origstoreno = 238

            };
            var x = new
            {
                data = new[] { datas }

            };
            string json2 = JsonConvert.SerializeObject(x);
            var u = "api/backoffice/transferslip";
            RestFile resf = new RestFile();
            var bill = resf.PostDatav_22(au,u, json2);
           
            var d = JsonConvert.DeserializeObject<dynamic>(bill);
            var v = d.data[0];
            var jObj = JsonConvert.DeserializeObject<Dictionary<string, object>>(v.ToString());
            var billID = jObj["sid"].ToString();
            ViewBag.id = billID;
     
            var data = new List<dynamic>();
            var i1 = new
            {
                originapplication = "RProPrismWeb",
                qty = 1,
                itemsid = "544956795000185179",
                slipsid = billID,
            };
            var i2= new
            {
                originapplication = "RProPrismWeb",
                qty = 1,
                itemsid = "544956795000160157",
                slipsid = billID,
            };
            data.Add(i1);
            data.Add(i2);
            string url = "api/backoffice/transferslip/"+ billID + "/slipitem";
            var xq = new
            {
                data
            };
            string jsonw = JsonConvert.SerializeObject(xq);
            var bill2 = resf.PostDatav_22(au, url, jsonw);
            string put = "api/backoffice/transferslip/" + billID;
            var datap = new
            {
    
                rowversion = 4,
                status = 4
            };
            var xp = new
            {
                data = new[] { datap }

            };
            string jsonp = JsonConvert.SerializeObject(xp);
            var billp = resf.PutDatav_22(au, put, jsonp);
            return View();
        }
        public IActionResult UploadFolder()
        {
            
            return View();
        }
        public IActionResult EditProduct()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> EditProduct(string o)
        {
            var p =await woo.catelogy();
            DataTable table = new DataTable("Detail");
             table.Columns.Add("Diễn giải 1", typeof(string));
            foreach (var row in p)
            {
                table.Rows.Add(row.name.ToString());
                   
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "chitiet.xlsx");
                }
            }
          
            return View();
        }
        public IActionResult discountproduct()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> discountproduct(IFormFile files)
        {
            woocomerge woo = new woocomerge();
            DataTable table = new DataTable("product");
            table.Columns.Add("stt", typeof(string));
            table.Columns.Add("tên sp", typeof(string));
            table.Columns.Add("sku", typeof(string));
            table.Columns.Add("giá bán", typeof(int));
            table.Columns.Add("giá giảm", typeof(int));
            
            List<Product> orders = new List<Product>();
            int p= 1;

            do
            {
                var obj = await woo.productdiscount(p.ToString());
                if(obj.Count<=0)
                {
                    p = -1;
                    break;
                }
                orders.AddRange(obj);
                p++;
            } while (p > 0);
            int stt = 1;
            foreach (var j in orders)
            {
                var text = j.price_html;
                var h = text.Split('>');
                var pr= h[2].Split('<')[0].Replace(",","");

                table.Rows.Add(
                    stt.ToString(),
                        j.name,
                        j.sku,
                        pr,
                        j.price
                      
                        );
                stt++;
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "product.xlsx");
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> discountproduct3(IFormFile files)
        {
            List<dynamic> rsgia = new List<dynamic>();
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", files.FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(1).GroupBy(s => s.Cell(1).Value).ToList();
            ws2 = ws2.Where(s => s.Key.ToString() != "").ToList();
            int pag = ws2.Count / 100;
            int bg = 0;
          
            woocomerge wo = new woocomerge();
           
                
                List<Variation> v_ = new List<Variation>();
                foreach (var j in ws2)
                {
                var ii = j.FirstOrDefault().Cell(3).Value.ToString().Split('-');
                string mtk = ii[ii.Count() - 1].ToString();
                try
                {
                   
                    var idwo = await wo.GProduct(mtk);
                    if (idwo.Count > 0)
                    {
                        var wo_ = await wo.getsanpham(idwo[0].id.ToString(), j.Key.ToString());
                        if (wo_.Count > 0)
                        {

                            var g = wo_[0];
                            if (bool.Parse(g.manage_stock.ToString()) == false)
                            {
                                g.stock_quantity = int.Parse(j.FirstOrDefault().Cell(2).Value.ToString());
                                g.manage_stock = true;
                                await wo.UponeVariations(int.Parse(idwo[0].id.ToString()), int.Parse(g.id.ToString()), g);
                            }
                            
                        }
                        else
                        {
                            dynamic dnm = new System.Dynamic.ExpandoObject();
                            dnm.mtk = j.Key.ToString();
                            dnm.giaban = "upc";
                            dnm.giagiam = "";
                            dnm.idpro = "";
                            dnm.vari = "";
                            rsgia.Add(dnm);
                        }
                    }
                    else
                    {
                        dynamic dnm = new System.Dynamic.ExpandoObject();
                        dnm.mtk = mtk;
                        dnm.giaban = "mtk";
                        dnm.giagiam = "";
                        dnm.idpro = "";
                        dnm.vari = j.Key.ToString();
                        rsgia.Add(dnm);
                    }
                    //var k= ws3.FirstOrDefault(s=>s.)
                }
                catch
                {
                    dynamic dnm = new System.Dynamic.ExpandoObject();
                    dnm.mtk = mtk;
                    dnm.giaban = "mtk loi";
                    dnm.giagiam = "";
                    dnm.idpro = "";
                    dnm.vari = "";
                    rsgia.Add(dnm);
                }

                }
              
           
            return View(rsgia);
        }
        [HttpPost]
        public async Task<IActionResult> discountproduct2(IFormFile files)
        {
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", files.FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(1).GroupBy(s => s.Cell(2).Value).ToList();
            ws2 = ws2.Where(s => s.Key.ToString() != "").ToList();
            int pag = ws2.Count / 100;
            int bg = 0;
            woocomerge wo = new woocomerge();
            List<string> lkey = new List<string>();
            List<string> rs = new List<string>();
            List<dynamic> rsgia = new List<dynamic>();
            foreach (var j in ws2)
            {
                string[] ke = j.Key.ToString().Split('-');
                lkey.Add(ke[ke.Length - 1].ToString());
            }
            for (var i = 0; i <= pag; i++)
            {
                var ws3 = ws2.Skip(bg).Take(100).ToList();
                List<string> l = lkey.Skip(bg).Take(100).ToList();
                var woo = await wo.searchProducts(string.Join(",", l));
                
                foreach (var t in ws3)
                {
                  
                    string[] ke = t.Key.ToString().Split('-');

                    string sku = ke[ke.Length - 1].ToString();
                    var h = woo.FirstOrDefault(s => s.sku == sku && s.status == "publish");
                    if (h == null)
                    {
                        rs.Add(sku);
                    }
                    else
                    {
                        List<Variation> vrdown = new List<Variation>();
                        var y = t;
                        var giaban = y.Select(s => s.Cell(4).Value.ToString()).FirstOrDefault().ToString();
                        var giagiam = y.Select(s => s.Cell(5).Value.ToString()).FirstOrDefault().ToString();
                       foreach(var j in h.variations)
                        {
                            Variation v_ = new Variation();
                            v_.id = j;
                            v_.regular_price = decimal.Parse(giaban.Replace(",", ""));
                            v_.sale_price = decimal.Parse(giagiam.Replace(",",""));
                            vrdown.Add(v_);
                        }
                        BatchObject<Variation> b_ = new BatchObject<Variation>();
                        b_.update = vrdown;
                       
                        await wo.UpVariations(int.Parse(h.id.ToString()), b_);
                        dynamic dnm = new System.Dynamic.ExpandoObject();
                        dnm.mtk = sku;
                        dnm.giaban = giaban;
                        dnm.giagiam = giagiam;
                        dnm.idpro = h.id;
                        dnm.vari =string.Join(";",h.variations);
                        rsgia.Add(dnm);
                    }
                }
                
                bg = bg + 100;
            }
            return View(rsgia);
        }
        public IActionResult checkproduct()
        {

            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> checkproduct(IFormFile files)
        {
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", files.FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(3).GroupBy(s => s.Cell(3).Value).ToList();
            ws2 = ws2.Where(s => s.Key.ToString() != "").ToList();
            int pag = ws2.Count / 100;
            int bg = 0;
            woocomerge wo = new woocomerge();
            List<string> lkey = new List<string>();
            List<string> rs = new List<string>();
            foreach(var j in ws2)
            {
                string[] ke = j.Key.ToString().Split('-');
                lkey.Add(ke[ke.Length - 1].ToString());
            }
            for (var i = 0; i <= pag; i++)
            {
                List<string> l = lkey.Skip(bg).Take(100).ToList();
                var woo =await wo.searchProducts(string.Join(",", l));
                foreach(var t in ws2)
                {
                    string[] ke = t.Key.ToString().Split('-');
                 
                    string sku = ke[ke.Length - 1].ToString();
                    var h = woo.FirstOrDefault(s => s.sku == sku&&s.status=="publish");
                    if(h==null)
                    {
                        rs.Add(sku);
                    }
                }
                bg = bg + 100;
            }
            return View(rs);
        }
        public IActionResult Upcoupon()
        {

            return View();
        }
        public async Task<IActionResult> ProductNM()
        {
            string m = "2104017,2104018,2104019,2104020,2104021,2104022,2104023,2104027,2104028,2104033,2104035,2104125,2104127,2105012,2105013,2105014,2105015,2105016,2103054,2104016,2104111,2104113";
            m += "2105018,2105020,2103050,2104089,2104090,2104097,2104098,2104099,2104100,2104114,2104115,2104126,2105022,2105023,2104003,2104004,2104005,2104006,2104007,2104008,2104009,2104010";
            m += "2104011,2104012,2104013,2104014,2104015,2104024,2104025,2104030,2104031,2104032,2104034,2104087,2104088,2104112,2104122,2105017";
            List<Product> prs = new List<Product>();
            
            var sku = m.Split(',');
            foreach(var s in sku)
            {
                var product = await woo.searchProducts(s);
                if(product.Count>0)
                prs.AddRange(product);
            }
            return View();
        }
        public async Task<IActionResult> Orderpr()
        {
            var cate = await woo.catelogy();
            var parentroot = cate.Where(x => x.parent == 0 && x.count == 0);
            var parent = cate.Except(parentroot).Where(x => x.parent == 0);
            ViewBag.root = parentroot;
            ViewBag.c = cate;
            ViewBag.r = parent;
            //ViewBag.cweb = catep;
            return View();
        }
        [HttpPost]
        public IActionResult UploadFolder(string link,string lbirday)
        {
            string au = HttpContext.Session.GetString("au");
            RestFile rest = new RestFile();
            string dcm = "v1/rest/document?filter=sid,eq," + link + "&cols=*&page_no=1&page_size=1";
            var d = rest.GetDataWebClient(au, dcm);
            List<dynamic> obj = JsonConvert.DeserializeObject<List<dynamic>>(d);
            string status = obj[0].status;
            string version = obj[0].row_version;
            int statuss = int.Parse(status);

            if (statuss >= 4)
            {
                string cvd = cmd.convertdate(lbirday);
                DateTime now = DateTime.Parse(cvd);
                string usta = "v1/rest/document/" + link + "?filter=row_version,eq," + version;
                dynamic st = new
                {
                    invoice_posted_date = now,
                    status = 3,
                };
         
                var ust = rest.updateItem(au, usta, st);
                return View();
                int versionnext = int.Parse(version)+1;
                var t = cmd.updatecreatedocument(au, link, now, now, versionnext.ToString());
            }
           
            return View();
        }
        
        public IActionResult EmployeePrism()
        {
            return View();
        }
        [HttpPost]
        public IActionResult EmployeePrism(string infor)
        {
            dynamic a = JsonConvert.DeserializeObject<dynamic>(infor);
            string au=HttpContext.Session.GetString("au");

            if(au==null)
                a= cmd.GetAuth_Nonce();
            List<dynamic> o = new List<dynamic>();
            dynamic data = new
            {
                originapplication = "RProPrismWeb",
                active = true,
                useractive = true,
                username = a.username,
                password = a.pass,
                firstname = a.firstname,
                lastname = a.lastname,
                status=1,
                hiredate = a.hiredate,
            };
            dynamic empladdress = new
            {
               active=1,
               addressallowcontact=true,
               address1=a.address1,
               
            };
            dynamic emplphone = new
            {
                phoneno = a.phone,
            };
            var x = new
            {
                data = new[] { data, a.address1, emplphone }
            };

            string json2 = JsonConvert.SerializeObject(x);


            RestFile rest = new RestFile();
            rest.PostDatav_22(au, "api/common/employee", json2);
            return View();
        }
        public IActionResult ctmCrm()
        {
            string resp = HttpContext.Request.Query["resp"];
            string sid = HttpContext.Request.Query["sid"];
            dynamic ctm = JsonConvert.DeserializeObject<dynamic>(resp);
            ctm.BirthDay = ctm.BirthDay.ToString().Replace(".000 ", "+");
            common cmd = new common();
            sqlcommon sql = new sqlcommon();
            List<string> cl = new List<string>() {
                "DataType","JsonData","Retry","SysID","Note","CreateDate","HostName","Idprim"
            };
            List<string> vl = new List<string>() {
               "2",JsonConvert.SerializeObject(ctm),"0","1","new",DateTime.Now.ToString(),cmd.GetIPAddress(),sid
            };
            string sqlinsert = sql.insertsqlstr("DataExchange", cl, vl);
            sql.runSQL(sqlinsert);
            return View();
        }
            public IActionResult billCrm()
        {
            string bill= HttpContext.Request.Query["bill"];
            common cmd = new common();
            RestFile rest = new RestFile();
            var b = JsonConvert.DeserializeObject<dynamic>(bill);
            var cp = b.CouponCode;
            var a = b.CreateDate.ToString().Replace(".000", "");
            var d = a.Replace(" ", "+");
            List<dynamic> lcpon = new List<dynamic>();
            b.CreateDate = d;
            if (cp.ToString() != "[]")
            {
                string GetAuth_Nonce = cmd.GetAuth_Nonce();
                string cps = rest.GetDataWebClient(GetAuth_Nonce, "v1/rest/document/" + b.BillNo.ToString() + "/coupon?cols=*");
                List<dynamic> coup = JsonConvert.DeserializeObject<List<dynamic>>(cps);
                foreach (var i in coup)
                {
                    dynamic c = new System.Dynamic.ExpandoObject();
                    var oexc = cmd.findcoupon(i.coupon_code.ToString());
                    if (oexc == null)
                    {
                        string proId = i.promo_sid.ToString();
                        string address = "v1/rest/pcpromo?cols=*&filter=sid,EQ," + proId;
                        string pro = rest.GetDataWebClient(GetAuth_Nonce, address);
                        List<dynamic> prodnm = new List<dynamic>();

                        c.CouponCode = i.coupon_code;
                        if (pro != "[]")
                        {
                            prodnm = JsonConvert.DeserializeObject<List<dynamic>>(pro);
                            c.CouponType = prodnm[0].general_promo_group; //Gửi giúp em danh sách loại coupon và mổ tả của nó nha
                            c.CouponValue = prodnm[0].reward_gd_disc_value;
                        }
                    }
                    else
                    {
                        c.CouponType = oexc.CouponType;
                        c.CouponValue = oexc.CouponValue;
                    }
                    lcpon.Add(c);
                }
            }
            b.CouponCode = JsonConvert.SerializeObject(lcpon);

            string resp = JsonConvert.SerializeObject(b);
            string sqls = "";
            sqls = cmd.Stringcolumnsql("1", resp, b.BillNo.ToString(), "");
            sqlcommon sql = new sqlcommon();
            sql.runSQL(sqls);
            return View();
        }
        public IActionResult searchImg()
        {

            return View();
        }
        public IActionResult Promotion()
        {
           
            return View();
        }
        public IActionResult Mainmenu()
        {
            return View();
        }
        public IActionResult UpFileInFolder()
        {
            return View();
        }
        public void upimgWeb(FileInfo file,int bg)
        {
            string FolderSource = Path.Combine(@"wwwroot\Images\");
            string name = file.Name.Replace("-", "_");
            List<string> names = name.Split("_").ToList();
            if (names.Count <= 1 || names[0]=="")
                return;
            string n = bg + ".jpg";
            names[names.Count - 1] = n;
            string fname = string.Join("_", names);
            var paths = Path.Combine(FolderSource, "", fname);
           // FileStream fileStream = new FileStream(paths, FileMode.Create);
            //file.sa(fileStream.);
            //fileStream.Dispose();
       
            file.CopyTo(paths,true);
            
        }
        [HttpPost]
        public IActionResult UpFileInFolder(string path)
        {
           
            string[] filesindirectory = Directory.GetDirectories(path);
            if (filesindirectory.Length <= 0)
            {
                int bg = 1;
                DirectoryInfo d = new DirectoryInfo(path);
                foreach (var file in d.GetFiles("*.jpg"))
                {
                    upimgWeb(file, bg);
                    bg++;
                }
                return View();
            }
            foreach (var subdir in filesindirectory)
            {

                DirectoryInfo d = new DirectoryInfo(subdir);
                int bg = 1;
                foreach (var file in d.GetFiles("*.jpg"))
                {
                      upimgWeb(file, bg);
                      bg++;
                   
                }
                
            }
                return View();
        }
        public IActionResult LoginTool()
        {
            
            storePrism rp = new storePrism();
            return View();
        }
        public IActionResult Report()
        {
            storePrism rp = new storePrism();
            var a = rp.GetReport("1/1/2019", "1/3/2019");
            return View();
        }
        [HttpPost]
        public IActionResult LoginTool(string user,string pass)
        {
            common cmd = new common();
            //cmd.userPrism = "sysadmin";
            //cmd.passPrism = "N!N0m@xx(1998)";
            cmd.userPrism = user;
            cmd.passPrism = pass;
            string au = cmd.GetAuth_Nonce();
            if (au != "")
            {
                HttpContext.Session.SetString("au", au);
                return RedirectToAction("Mainmenu", "ToolUpPrism", new { area = "Tool" });
            }
            return View();
        }
        public IActionResult Customer()
        {
            ViewBag.au = HttpContext.Session.GetString("au");
            if (ViewBag.au==null)
                return RedirectToAction("LoginTool", "ToolUpPrism", new { area = "Tool" });
            return View();
        }
       
        public string updateaddressctm(string key,string sid, string add, string tp, string qh, string px)
        {
            common cmd = new common();
            RestFile rest = new RestFile();
 
            dynamic item = new System.Dynamic.ExpandoObject();
            //item.customer_sid = sid;
            item.address_line_1 = add;
            item.address_line_2 = px;
            item.address_line_3 = qh;
            item.address_line_4 = tp;
 
            string url = "v1/rest/customer/" + sid + "/address?cols=sid,row_version&page_no=1&page_size=10";
            string version = rest.GetDataWebClient(key, url);
            List<dynamic> vsl = JsonConvert.DeserializeObject<List<dynamic>>(version);
            string vs = vsl[0].row_version.ToString();
            string url2 = "v1/rest/customer/" + sid + "/address/" + vsl[0].sid.ToString() + "?filter=row_version,eq," + vs;
            string u = rest.updateItem(key, url2, item);
            return u;
        }
        public string updateEmailctm(string key, string sid, string email)
        {
            common cmd = new common();
            RestFile rest = new RestFile();

            dynamic item = new System.Dynamic.ExpandoObject();
            //item.customer_sid = sid;
            item.email_address = email;

            string url = "v1/rest/customer/" + sid + "/email?cols=sid,row_version&page_no=1&page_size=10";
            string version = rest.GetDataWebClient(key, url);
            List<dynamic> vsl = JsonConvert.DeserializeObject<List<dynamic>>(version);
            if (vsl.Count > 0)
            {
                string vs = vsl[0].row_version.ToString();
                string url2 = "v1/rest/customer/" + sid + "/email/" + vsl[0].sid.ToString() + "?filter=row_version,eq," + vs;
                string u = rest.updateItem(key, url2, item);
                return u;
            }
            else
            {
                string urlnew = "v1/rest/customer/"+ sid + "/email";
                var k = rest.PostItemPrism(key, urlnew, item);
                
            }
            return "";
        }
        public string GetImg(string str,string p)
        {
            string convert = str.Replace("data:image/png;base64,", String.Empty);
            convert = convert.Replace("data:image/jpeg;base64,", String.Empty);
            convert = convert.Replace(" ", "+");

            string path = Path.Combine(@"wwwroot\cmnd\");

            Guid name = Guid.NewGuid();
            var n = name.ToString().Replace("-", "");
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
            {
                using (Bitmap bm2 = new Bitmap(ms))
                {

                    bm2.Save(path + p + ".jpg");
                }
            }

            return p + ".jpg";
        }

        [HttpPost]
        public IActionResult Customer(string valuectm, string files)
        {
            
            RestFile rest = new RestFile();
            common cmd = new common();
            string key = HttpContext.Session.GetString("au");
            dynamic o = JsonConvert.DeserializeObject<dynamic>(valuectm);
            string u = o.action.ToString();
            string img_ = "";
            if (files != "[]")
            {
                List<string> cmnd = new List<string>();
                List<dynamic> imgs = JsonConvert.DeserializeObject<List<dynamic>>(files);
                foreach(var j in imgs)
                {
                    string gname = Guid.NewGuid().ToString().Replace("-", "");
                    if (j.giatri.ToString() == "new")
                        cmnd.Add(GetImg(j.data.ToString(), gname));
                }
                img_ = string.Join(",",cmnd);
            }

            dynamic phone = new
            {
                phone_no = o.primary_phone_no

            };
            dynamic mail = new
            {
                email_address = o.email_address,
                origin_application = "RProPrismWeb",
                primary_flag = "true"
            };
            dynamic address = new
            {
                address_line_1 = o.primary_address_line_1,
                address_line_2 = o.primary_address_line_2,
                address_line_3 = o.primary_address_line_3,
                address_line_4 = o.primary_address_line_4,
            };
            List<dynamic> phones = new List<dynamic>();
            phones.Add(phone);
            List<dynamic> mails = new List<dynamic>();
            mails.Add(mail);
            List<dynamic> add = new List<dynamic>();
            add.Add(address);
            dynamic a = new System.Dynamic.ExpandoObject();
            a.phones = phones;
            a.first_name = "customer";
            a.last_name = "customer";
            a.udffield01 = img_;
            if (o.first_name!="")
                a.first_name = o.first_name;
            if (o.last_name != "")
                a.last_name = o.last_name;
            if (o.primary_address_line_1 != "")
                a.primary_address_line_1 = o.primary_address_line_1;
            if (o.primary_address_line_2 != "")
                a.primary_address_line_2 = o.primary_address_line_2;
            if (o.primary_address_line_3 != "")
                a.primary_address_line_3 = o.primary_address_line_3;
            if (o.primary_address_line_4 != "")
                a.primary_address_line_4 = o.primary_address_line_4;
            a.title_sid = o.title_sid;
            string active = "false";
            a.customer_active = true;
        
            try
            {
                string bd = cmd.convertdate(o.udf1_date.ToString());
                a.udf1_date = DateTime.Parse(bd);
            }
            catch
            {
                a.udf1_date = "";
            }
            if (o.email_address != "")
                a.emails = mails;
            a.addresses = add;

            a.origin_application = "RProPrismWeb";
            a.share_type = "1";
            //return View();
            if (u == "Save")
            {
                string url = "v1/rest/customer";
                var k = rest.PostItemPrism(key, url, a);
            }
            else {
                string uurl = "v1/rest/customer?filter=sid,EQ," + o.sid.ToString() + "&cols=row_version&page_no=1&page_size=1";
                string vs = rest.GetDataWebClient(key, uurl);
                List<dynamic> dnm = JsonConvert.DeserializeObject<List<dynamic>>(vs);
                string vsion = dnm[0].row_version;
                string urlupdate="v1/rest/customer/" + o.sid.ToString() + "?filter=row_version,eq," + vsion + "";
                var up = rest.updateItem(key, urlupdate, a);
                try
                {
                    updateaddressctm(key, o.sid.ToString(), o.primary_address_line_1.ToString(), o.primary_address_line_4.ToString(), o.primary_address_line_3.ToString(), o.primary_address_line_2.ToString());
                }
                catch { }
                try
                {
                    if(o.email_address.ToString()!="")
                    updateEmailctm(key, o.sid.ToString(), o.email_address.ToString());
                }
                catch { }
                Crm c = new Crm();
                c.UpdateCustomer(o.sid.ToString(),"");
            }
            
            return RedirectToAction("Customer", "ToolUpPrism", new { area = "Tool" });
        }
        public IActionResult Print()
        {
           
            PrintDocument pd = new PrintDocument();
            
           // pd.PrinterSettings.PrinterName = "HP Universal Printing PCL 6";
            pd.PrintPage += new PrintPageEventHandler
                  (pd_PrintPage);
            pd.Print();
            
            return View();
        }
        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            Single yPos = 0;
            Single leftMargin = e.MarginBounds.Left;
            Single topMargin = e.MarginBounds.Top;
            // Image img = Image.FromFile("logo.bmp");
            Rectangle logo = new Rectangle(40, 40, 50, 50);
            using (Font printFont = new Font("Arial", 10.0f))
            {
                //e.Graphics.DrawImage(img, logo);
                e.Graphics.DrawString("Testing!222", printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
            }
        }
        [HttpPost]
        public async Task<IActionResult> Promotion(List<IFormFile> files)
        {
            cmcs3 s3 = new cmcs3();
            //await s3.UploadFileAsync();
            await s3.UploadFiles3(files);
            return View();
            ////string FolderSource = Path.Combine(@"wwwroot\Images\");
            //common cmd = new common();
            //string FolderSource = Path.Combine(@"wwwroot\Images\");
            ////var i = files[0].FileName;
            //int bg = 1;
            //foreach (IFormFile file in files)
            //{
            //    string name = file.FileName.Replace("-", "_").Replace(" ", "").Replace("(", "_(");
            //    List<string> names = name.Split("_").ToList();
            //    List<string> agr = new List<string>();
            //    string n = bg + ".jpg";
            //    if (names[0] == "")
            //        continue;
            //    if (!cmd.checknumber(names[0]))
            //    {
            //        List<string> names_ = new List<string>();
            //        for (int i = 1; i < names.Count(); i++)
            //        {
            //            if (cmd.checknumber(names[i]))
            //            {
            //                int l_ = names.Count;
            //                for (var j = i; j < names.Count; j++)
            //                    names_.Add(names[j]);

            //                names = names_;
            //                break;
            //            }
            //        }
            //    }
            //    string FolderSource_ = Path.Combine(@"" + FolderSource + names[0]);
            //    if (agr.IndexOf(names[0] + names[1]) == -1)
            //    {
            //        cmd.createFolder(FolderSource_);
            //        agr.Add(names[0] + names[1]);

            //    }
            //    string fname = name;
            //    var path = Path.Combine(FolderSource_, "", fname);
            //    FileStream fileStream = new FileStream(path, FileMode.Create);
            //    file.CopyTo(fileStream);
            //    fileStream.Dispose();

            //}

        }
        public IActionResult Inventory()
        {
            return View();
        }
        public  IActionResult Index()
        {
            
           string GetAuth_Nonce = HttpContext.Session.GetString("au");
            if (GetAuth_Nonce == null)
                return RedirectToAction("LoginTool", "ToolUpPrism", new { area = "Tool" });
            Search s = new Search();
            //var cate =await woo.catelogy();
            //var pr = cate.GroupBy(a=>a.parent).Select(g => new { name = g.Key.ToString() }).ToList();
            //var parent = await woo.catelogyParent(pr.Select(a=>a.name).ToList());
            //RestFile rest = new RestFile();
            //string url = "v1/rest/inventory?count=true&cols=sid,upc,description1,description2,attribute,item_size,order_cost&page_no=1&page_size=10&sort=description1,desc";
            //var data = rest.GetDataWebClientAndCout(GetAuth_Nonce, url);
            //string c = data.count.ToString().Split('/')[1];
           
            //ViewBag.page = int.Parse(c) / 10;
            //List<dynamic> d = JsonConvert.DeserializeObject<List<dynamic>>(data.data);
            //foreach (var i in d)
            //{
            //    i.description2 = cmd.convertVn(i.description2.ToString());
            //}
            //s.data = d;
            //ViewBag.c=cate;
            //ViewBag.r = parent;
            return View(s);
        }
        public async Task<string> UploadFileProcductWeb(IFormFile files)
        {
            List<string> exid = new List<string>();
            common cmd = new common();
            string FolderSource = Path.Combine(@"wwwroot\Excel\");
            var path = Path.Combine(FolderSource, "", files.FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().Skip(3).GroupBy(s => s.Cell(2).Value).ToList();
            //var count = ws2.Count();
            //foreach (var row in ws1.Rows()) {
            //    var cell = row.Cell(2);
            //    object value = cell.Value;
            //    string value2 = cell.GetValue<string>();
            //}
            woocomerge woo = new woocomerge();
            var ids =new List<string>();
            var cate = await woo.catelogy();
   
            foreach (var row in ws2)
            {
                var y = row;
                var cell = y.Key.ToString();
                string[] des = cell.Split("-");

                string ddes = des[des.Length - 1];
                int id = ids.IndexOf(ddes);
                if (id == -1)
                {

               
                    ids.Add(ddes);
                    try
                    {
                        List<string> colors = y.Select(s => s.Cell(7).Value.ToString()).ToList();
                        List<string> sizes = y.Select(s => s.Cell(8).Value.ToString()).ToList();
                        List<string> cates = new List<string>();
                        var first = y.FirstOrDefault();
                        var ct1 = cate.FirstOrDefault(s => s.name == first.Cell(4).Value.ToString());
                        var ct2 = cate.FirstOrDefault(s => s.name == first.Cell(5).Value.ToString());
                        if (ct1 != null)
                            cates.Add(ct1.id.ToString());
                        if (ct2 != null)
                            cates.Add(ct2.id.ToString());
                        foreach (var k in colors)
                        {
                            string tmp = k;
                            tmp = cmd.replaceChar(tmp);
                        }

                        List<ProductAttributeLine> attrs = new List<ProductAttributeLine>();
                        Product p = new Product();

                        attrs = woo.listAttrID(colors, sizes);
                        string des1 = first.Cell(10).Value.ToString();
                        string des2 = first.Cell(12).Value.ToString();
                        string des3 = first.Cell(13).Value.ToString();
                        List<ProductMeta> meta = woo.listMetaId(des3);
                        p = await woo.addproduct_excel(attrs, first.Cell(6).Value.ToString(), des1, ddes, cates.ToList(), meta, cell);
                    
                        List<string> sku_ = new List<string>();
                        foreach (var b in y)
                        {
                     
                            string uupc = b.Cell(1).Value.ToString();
                        if (sku_.IndexOf(uupc) == -1)
                        {
                            await woo.inputProductvariable(p, b.Cell(11).Value.ToString()
                                , uupc,
                                b.Cell(7).Value.ToString(),
                                b.Cell(8).Value.ToString(),"");
                                sku_.Add(uupc);
                        }
                        }
                    }
                    catch
                    {
                        exid.Add(cell);
                    }
                }
                else
                    exid.Add(cell);
            }
            return string.Join(";", exid);
        }
        [HttpPost]
        public async Task<IActionResult> Index(string o,string where,string exactly, string web)
        {
            o = o.Trim();
           Search s = new Search();
            //woo = new SysnWeb("nino");
            //if (files.Count > 0)
            //{
            //    var rs = await UploadFileProcductWeb(files[0]);
            //    ViewBag.er = rs;
            //    return View(s);
            //}
            if (o=="" || o == null)
                return View(s);
            s.keyword = o;
            string GetAuth_Nonce = HttpContext.Session.GetString("au");
            if (GetAuth_Nonce == null)
                return RedirectToAction("LoginTool", "ToolUpPrism", new { area = "Tool" });
            
            RestFile rest = new RestFile();
            string urlvfc = "api/v1/inventory/get-onhand-by-mtk?desc1=2201002&storeNo=322";
          
            //string urlit = "v1/rest/inventory?cols=sid,upc,description1,description2,attribute,item_size,order_cost&filter=description1,LK,*" + o + "*)OR(description2,EQ,*" + o + "*)";
            //string urlit = "v1/rest/inventory?cols=*&filter=description1,LK,*" + o + "*";
            string urlit = "v1/rest/inventory?filter=(upc,eq," + o+")AND(active,eq,true)&count=true&cols=*";
            string item = rest.GetDataWebClient(GetAuth_Nonce, urlit);
            List<dynamic> d = JsonConvert.DeserializeObject<List<dynamic>>(item);
            string catep = "";
           /* if (d != null)
            {
                //var obj = await woo.getcolor(p.ToString());
                //color = color.OrderBy(s => s.name).ToList();
                //var o = color.Where(s => s.count == 0).ToList();
               

                string[] wo_ = o.Split('-');
                string p_ = wo_[wo_.Length - 1];
                SysnWeb ps = new SysnWeb("nino");
                foreach (var i in d)
                {
                    i.description2 = cmd.convertVn(i.description2.ToString());
                }
                    var p =await ps.searchProducts(p_);
                if (p.Count > 0)
                {
                    s.giagiam = p[0].sale_price.ToString();
                    var meta =p[0].meta_data;
                    
                    var vrr = p[0].variations;
                    var avr = await ps.Variationss(p[0]);
                    s.namewebsite= p[0].name;
                    foreach (var i in d)
                    {
                        string upc = i.upc.ToString();
                   
                        var o_ = avr.FirstOrDefault(x => x.sku.ToString() == upc);
                        if (o_ != null)
                        {
                            i.sid = -1;
                            i.invent = o_.stock_quantity;
                        }
                        
                        
                    }
                    catep = JsonConvert.SerializeObject(p[0].categories);
                }
                    
            }*/
            s.data = d;
            if (exactly != null)
                s.exactly = "checked";
            //var cate = await woo.catelogy();
            //var pr = cate.Where(x => x.parent == 0 && x.count >0).GroupBy(a => a.parent).Select(g => new { name = g.Key.ToString() }).ToList();
            //var parentroot = cate.Where(x => x.parent == 0 && x.count == 0);
           // var parent = cate.Except(parentroot).Where(x => x.parent == 0);
           // ViewBag.root = parentroot;
           // ViewBag.c = cate;
           // ViewBag.r = parent;
           // ViewBag.cweb = catep;

            return View(s);
        }
    }
}