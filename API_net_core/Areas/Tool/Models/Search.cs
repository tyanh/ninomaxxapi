﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_net_core.Areas.Tool.Models
{
    public class Search
    {
        public string keyword { get; set; }
        public string where { get; set; }
        public string exactly { get; set; }
        public List<dynamic> data { get; set; }
    }
}
