﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace API_net_core.Areas.AdminTool.Models
{
    public class Pushnotifation
    {
        string applicationID = "1:353776557690:android:1249fc63d738b560b765c3";
        string senderId = "353776557690";

        public string pushnotifationtodevice(string content, string condoid, string to, string title, string body, string location, string link, string idItem)
        {

            applicationID = "AAAAUl653no:APA91bFS6eXYx9vdUswRkXYg7oQ5vIQG7zgGHpW0PYS1_1ht7tneH3rLdSAF974IVasgxe_qzQiBzsTvw4UUHUaZl00k1WFi0wkNy6F0Hxsm4tswYtQZg9y8kE2cF2Bx75f5UCNn_pR7";
            string deviceId = "d9eTkB7Kwuc:APA91bEFHTdFXKzmGOU6sKsFIeGKUkrC-48N7_JRfJjuHkxnH7-28XP_mVlMFiCOj8FcYsuB9y7oDYbDBHYTQvug07tAv_CZLe-aIvyXXkkEJljTBp9MLWjLzy3nfaaz5lxOqwaCOuBQ";

            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
                to = deviceId,
                priority = "high",
                notification = new
                {
                    body = body,
                    title = title,
                    sound = "default",
                    badge = "1",
                    click_action = "FCM_PLUGIN_ACTIVITY",
                    android_channel_id= "post_sms"
                    // icon = "",

                },
                data = new
                {
                    location = location,
                    condo = condoid,
                    link = link,
                    idItem = idItem,
                    content = content,
                },

            };

            var json = JsonConvert.SerializeObject(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();
                            string str = sResponseFromServer;
                        }
                    }
                }
            }
            return "push";
        }

        public string testse(string content, string condoid, string topic, string title, string body, string location, string link, string idItem)
        {

           // string deviceId = topic.Trim();
                        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";

            var data = new
            {
               
                notification = new
                {
                    body = body,
                    title = title,
                    sound = "default",
                    click_action = "FCM_PLUGIN_ACTIVITY",
                    badge = "1",

                },
                data = new
                {
                    location = location,
                    condo = condoid,
                    link = link,
                    idItem = idItem,
                    content = "ere",
                },
                priority = "high",
            };
        
            var json = JsonConvert.SerializeObject(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();
                            string str = sResponseFromServer;
                        }
                    }
                }
            }
            return "push";
        }
    }
}
