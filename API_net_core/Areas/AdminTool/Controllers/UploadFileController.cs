﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API_net_core.Areas.AdminTool.Models;
using API_net_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGeneration.CommandLine;
using Newtonsoft.Json.Linq;

namespace API_net_core.Areas.AdminTool.Controllers
{
    [Area("AdminTool")]
    public class UploadFileController : Controller
    {
        public IActionResult Index()
        {
            
            return View();
        }
        [HttpPost]
        public IActionResult Index(string numpage)
        {

            return View();
        }
        public IActionResult pushnoti()
        {
            return View();
        }
         [HttpPost]
        public IActionResult pushnoti(string content)
        {

            Pushnotifation p = new Pushnotifation();
            p.pushnotifationtodevice("", "4882", "4882", "New Announcement", "test annountment", "Announcements", "test", "test");
           // p.pushnotifations("", "4882", "4882", "New Announcement", "test annountment", "Announcements", "test", "test");
            return View();
        }
        public IActionResult UploadImg()
        {

            return View();
        }
        [HttpPost]
        public IActionResult UploadImg(List<IFormFile> files)
        {
            string FolderSource = Path.Combine(@"wwwroot\Images\");
            var i = files[0].FileName;
            foreach (IFormFile file in files)
            {
                var path = Path.Combine(FolderSource, "", file.FileName);
                FileStream fileStream = new FileStream(path, FileMode.Create);
                file.CopyTo(fileStream);
                fileStream.Dispose();

            }
            return View();
        }
    }
}