<?php
/**
 * Plugin Name: connectPrism
 * Plugin URI: 
 * Description: connectPrism
 * Author: ticu
 * Version: 1.0.0
 * Author URI: 
 * Text Domain: 
 * License: GPL2
 */
//https://secure-www.nmclub.com.vn/wp-json/getcart/v1/getcart


add_action('wp_enqueue_scripts','client_assets');
function client_assets() {
    wp_enqueue_script( 'customjs',plugins_url('/connectPrism/clientjs.js?v=1.0.7'), array( 'jquery' ) );
   
}
add_action('admin_enqueue_scripts', 'admin_script');
function admin_script() {
    wp_enqueue_script( 'adminjs',plugins_url('/connectPrism/adminjs.js?v=1.1.0'), array( 'jquery' ) );
}

