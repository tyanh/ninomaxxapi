<?php
/**
 * Plugin Name: merge card data
 * Plugin URI: 
 * Description: merge card data
 * Author: ticu
 * Version: 1.0.0
 * Author URI: 
 * Text Domain: 
 * License: GPL2
 */
defined( 'ABSPATH' ) || exit;
$link_plugins=plugin_dir_path( __FILE__ );

add_action('wp_enqueue_scripts','css_script');
add_action( 'wp_footer', 'woocommerce_clear_cart_url' );

function woocommerce_clear_cart_url() {
   if (sizeof( WC()->cart->get_cart() ) > 0 ) { 
       WC()->cart->empty_cart();
     }
}

function css_script() {
   wp_enqueue_style('cartcss', plugins_url('/cartshopping.css',__FILE__ ));
   wp_enqueue_script( 'addcardjs',plugins_url('/mergeCart/onecartnino.js?v=1.1.6'), array( 'jquery' ) );
  
}
add_action( 'rest_api_init', function () {
  register_rest_route( 'GetProduct', '/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'ProductVarition',
  ) );
  
  register_rest_route( 'PostChatlieu', '/v1', array(
    'methods' => 'POST',
    'callback' => 'PostChatlieu',
  ) );
 register_rest_route( 'PostDate', '/v1', array(
    'methods' => 'POST',
    'callback' => 'PostDate',
  ) );
  
  register_rest_route( 'GETDiscount', '/v1', array(
    'methods' => 'GET',
    'callback' => 'GETDiscount',
  ) );
  register_rest_route( 'Createvariation', '/v1', array(
    'methods' => 'GET',
    'callback' => 'Createvariation',
  ) );
} );
function Createvariation(){
  //$parameters = $request_data->get_params();
 
	// Get products on sale
	$product_ids_on_sale = array_filter( wc_get_product_ids_on_sale() ); // woocommerce_get_product_ids_on_sale() if WC < 2.1

  return $product_ids_on_sale[0];
  //$parent_id = $parameters["parent_id"];
 
}
function GETDiscount() {
global $woocommerce;

	// Get products on sale
	$product_ids_on_sale = array_filter( wc_get_product_ids_on_sale() ); // woocommerce_get_product_ids_on_sale() if WC < 2.1

  return $product_ids_on_sale;
}
function PostDate( $request_data ) {
 
  $parameters = $request_data->get_params();
  $pid=$parameters["pid"];
  $my_args = array(
               'ID' => $pid,
               'post_date' => $parameters["postdate"]
            );

  wp_update_post( $my_args );
  return 0;
}
function PostChatlieu( $request_data ) {
 
  $parameters = $request_data->get_params();
  $sku=$parameters["sku"];
  $post_id=wc_get_product_id_by_sku($sku);
  $chatlieu=$parameters["chatlieu"];

  update_post_meta($post_id, 'chat_lieu', $chatlieu);
}
function ProductVarition($data){
	$id=$data["id"];
	$product = new WC_Product_Variation($id);
	$product->get_sku();
	echo $product;
	die();
}
function cartId(){
	$gaUserIp = $_SERVER['REMOTE_ADDR'];
	$localIP = getHostByName(getHostName());
	$browserAgent = $_SERVER['HTTP_USER_AGENT'];
	$CartId = md5($gaUserIp.$localIP.$browserAgent);
	return $CartId;
}

//add_action('woocommerce_add_cart_item_data', 'custome_add_to_cart');
function custome_add_to_cart() {
 
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'https://secure-www.ninomaxxconcept.com/wp-json/Addcart/v1/',
    CURLOPT_USERAGENT => 'POST',
    CURLOPT_POST => 1,
    CURLOPT_SSL_VERIFYPEER => false, //Bỏ kiểm SSL
    CURLOPT_POSTFIELDS => http_build_query(array(
        'cartid' => cartId(),
        'sku' => 'pistol',
        'title' => 'title',
        'price' => 'price',
        'sku' => 'sku'
    ))
));

$resp = curl_exec($curl);

var_dump($resp);

curl_close($curl);

}