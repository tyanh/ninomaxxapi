<?php
/*
Template name: Bảng giá	
*/
get_header();

?>
<section>
	<div class="row">
		<div class="col large-12 small-12 medium-12">
			<div class="detail__banggia">
				<div class="title__banggia">
					<img src="<?php the_field('image__bigo'); ?>">
					<h2><?php the_field('title__banggia'); ?></h2>
				</div>
				<img src="<?php the_field('banner_price'); ?>">
				<div class="table__banggia"  style="margin-top:20px">
					<table class="table__kimcuong" style="display:none">
						<thead>
						    <th>Mệnh Giá</th>
						    <th>Kim Cương</th>
						 </thead>
						  <tbody>
						  	 <?php
						       if(have_rows("list__banggia__detail")){
						         while (have_rows("list__banggia__detail")) {
						           the_row();
						           ?>
								    <tr>
								      <td data-label="Mệnh giá"><?php the_sub_field('pricing__kimcuong');?> VNĐ</td>
								      <td data-label="Kim Cương"><?php the_sub_field('number__kimcuong');?><img class="kimcuong__" src="<?php echo get_stylesheet_directory_uri(); ?>/image/kimcuong.png"></td>
								    </tr>
								      <?php
								        }
								    }
								?>
						  </tbody>
						</table>
						<div class="form__kimcuong">
							
							<div class="container">
								<div class="row kimcuong_form__detail pd-lr-20">
									<div class="large-6 small-12 col">
											<div class="input-control input__kimcuong">
											<input type="text" id="inputdnm" class="p-abs" onchange="bindPrice(jQuery(this))" placeholder="Số kim cương" style="z-index:2">
											<select id="numkc" class="p-abs">
												<option value="0"></option>
												<?php
												if(have_rows("list__banggia__detail")){
													while (have_rows("list__banggia__detail")) {
													the_row();
													?>
														<option value="<?php the_sub_field('pricing__kimcuong');?>">
														<?php the_sub_field('number__kimcuong');?><img class="kimcuong__" src="<?php echo get_stylesheet_directory_uri(); ?>/image/kimcuong.png">
													</option>
														<?php
															}
														}
													?>
											</select>
											
										</div><!-- /.input-control -->
									</div>
									<div class="large-1 text-center image__arrow">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/arrow.png">
									</div>
									<div class="large-5 small-12 col input__kimcuong">
										<div class="input-control">
										<input type="text" id="price" placeholder="Số tiền" onchange="bindKimcuong(jQuery(this))"><span> VNĐ</span>
										</div><!-- /.input-control -->
									</div>
								</div>
	
								<div class="row kimcuong_form__detail pd-lr-20">
								<div class="small-12 col">
											<div class="input-control input__kimcuong">
											<input type="text" id="fullname" placeholder="Họ tên" >
										</div><!-- /.input-control -->
									</div>
									<div class="large-6 small-12 col">
											<div class="input-control input__kimcuong">
											<input type="text" id="email-buy" placeholder="Email" >
										</div><!-- /.input-control -->
									</div>
									<div class="large-1 text-center image__arrow">
										
									</div>
									<div class="large-5 small-12 col input__kimcuong">
										<div class="input-control">
											 <input type="text" id="phone-buy" placeholder="Phone">
										</div><!-- /.input-control -->
									</div>
								</div>

							

							<div class="input-control controls" style="display: none;">
								<input type="radio" name="method" id="method_add" value="add" checked>
							</div><!-- /.input-control -->
						
							<div class=" btn__napkc">
								<button id="calculate">Nạp ngay </button>
							</div><!-- /.input-control -->
						</div><!-- /.container -->
						<div class="container result">
							<p class="answer">0</p>
						</div><!-- /.container -->
						</div>
				

				</div>
			</div>
			<div class="content__banggia">
				<div class="gioithieu__pricing">
					<?php the_field('gioithieu__banggia');?>
				</div>
				<div class="cachnap__pricing">
					<?php the_field('cachnap__banggia');?>
				</div>
				<div class="loiich__pricing">
					<?php the_field('loiich__banggia');?>
				</div>
				Truy cập trang chủ của Bigo Live <a href=#>Tại đây</a>
			</div>
		</div>
	</div>

</section>
<script type="text/javascript">
	// Get button element by it's ID
var datalist=[
	{kimcuong:230,price:100000},
	{kimcuong:460,price:200000},
	{kimcuong:1150,price:500000}
	
]
jQuery(document).ready(function(){
	
	jQuery('#numkc').on('change', '', function (e) {
		var optionSelected = jQuery("option:selected", this);
    	var valueSelected = this.value;
		jQuery("#price").val(valueSelected);
		jQuery("#inputdnm").val(jQuery(optionSelected).text())
	});
})
function bindPrice(o){
	var kc=jQuery(o).val();
	var pc=2.3;
	if(parseFloat(kc)>100)
		pc=2.305;
	var price=parseFloat(kc)*(pc);
	jQuery("#price").val(Math.round(price)+"000");
}
function bindKimcuong(o){
	var kc=jQuery(o).val();
	var pc=2.3;
	if(parseFloat(kc)>100)
		pc=2.305;
	var price=parseFloat(kc)/(pc);
	jQuery("#inputdnm").val(Math.round(price));
}
function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
var btn = document.querySelector('#calculate');
btn.addEventListener ('click', function () {
	var email=jQuery("#email-buy").val();
	var phone=jQuery("#phone-buy").val();
	var numkc=jQuery('#inputdnm').val();
	var price=jQuery('#numkc').find(":selected").val().replaceAll(',','').replaceAll('.','');
	var fullname=jQuery("#fullname").val();
	
	if(phone=="" || price=="" || price=="0" || fullname=="")
	{
		alert("Vui lòng nhập đủ thông tin");
		return false;
	}
	if(!validateEmail(email)){
		alert("Sai email");
		return false;
	}
	var data={
		email:email,
		phone:phone,
		numkc:numkc,
		price:price,
		fullname:fullname
	}
	
	jQuery.post("/wp-json/PostData/v1/",{data: data}, function(data, status){
    	alert("Data: " + data + "\nStatus: " + status);
 	 });
})
function bindPrice(o){
	var kc=jQuery(o).val();
	var pc=2.3;
	if(parseFloat(kc)>100)
		pc=2.305;
	var price=parseFloat(kc)*(pc);
	jQuery("#price").val(Math.round(price)+"000");
}
var btn2 =null;
// Bind a 'click' event to the button.
btn2.addEventListener ('click', function () {

	// Number(<value>): Convert any <value> into Number (Float or Integer):
	// see also: parseInt (<value>) => Convert any <value> into integer.
	// 					 parseFloat (<value>) => Convert any <value> into float.
	var num1 = Number ( document.querySelector('#num1').value ),
			num2 = Number ( document.querySelector('#num2').value ),
			// The Element to show result in
			rlt = document.querySelector('p.answer');
	
	// Get a checked radio/option button's value.
	// input => tag name.
	// [name="method"] => tag's attribte name and value.
	// :checked => pseudo rule, only find element with checked state.
	// .value => to get a value of that input.
	var method = document.querySelector('input[name="method"]:checked').value;
	var answer = 0;
	
	if ( 'add' === method ) {
		answer = num1 * num2;
	} else if ( 'sub' === method ) {
		answer = num1 - num2;
	} else if ( 'mul' === method ) {
		answer = num1 * num2;
	} else if ( 'div' === method ) {
		answer = num1 / num2;
	}
	
	// Set content of HTML element.
	rlt.innerHTML = answer;
});
</script>
<style type="text/css">
.image__arrow img {
	margin-top: 15px;
}
.input__kimcuong img, .input__kimcuong span {
	    position: absolute;
    top: 15px;
    left: auto;
    right: 40px;
    border-left: 2px solid #ebebeb;
    padding-left: 20px;
    color: #1a1a1a!important;
}
.pd-lr-20 {
	padding-left: 20px!important;
	padding-right: 20px!important;
}
.form__kimcuong {
	    width: 50%;
    display: block;
    margin: auto;
}
.kimcuong_form__detail .col {
	padding: 0 0px!important;
}
.kimcuong_form__detail {
	    background: #E1EDFF;
padding: 30px 30px 20px 30px;
}
.form__kimcuong input{
	box-shadow: unset;
	height: 54px;
}
.btn__napkc button {
	    background: linear-gradient(135deg, rgba(255, 148, 78, 0.75) 8.57%, #E35B00 100%);
    color: #fff!important;
    padding: 15px 80px;
    text-align: center;
    display: block;
    width: auto;
     	  font-family: "SF Pro Display",sans-serif;
     	  font-weight: 500;

    max-width: 250px;
    margin: 30px auto;
}
.table__kimcuong {
    width: 50%;
    border-collapse: collapse;
    border: none;
    margin: auto;
}
.kimcuong__ {
	    padding: 0 10px;
    margin-top: -5px;
}
.table__kimcuong td,
.table__kimcuong th {
	  padding: 15px 15px;
	  border: none;
	  text-align: center;
	  font-size: 16px;
 	  font-family: "SF Pro Display",sans-serif;
 	  color: #1a1a1a;
    font-weight: bold;

}

.table__kimcuong th {
  background-color: #015498;
  color: #ffffff;
 font-family: "SF Pro Display",sans-serif;
 font-weight: 500;
}

.table__kimcuong tbody tr:nth-child(even) {
  background-color: #d1e8f5;
}
.p-abs{
	position: absolute;
	top:0
}
#inputdnm{
	z-index: 2;
    width: 90%;
    height: 39px;
    border-right: 0;
}
/*responsive*/

@media (max-width: 500px) {
  .table__kimcuong thead {
    display: none;
  }

  .table__kimcuong,
  .table__kimcuong tbody,
  .table__kimcuong tr,
  .table__kimcuong td {
    display: block;
    width: 100%;
  }
  .table__kimcuong tr {
    margin-bottom: 15px;
  }
  .table__kimcuong td {
    padding-left: 50%;
    text-align: left;
    position: relative;
  }
  .table__kimcuong td::before {
    content: attr(data-label);
    position: absolute;
    left: 0;
    width: 50%;
    padding-left: 15px;
    font-size: 15px;
    font-weight: bold;
    text-align: left;
  }
}

	.title__banggia h2 {
		font-family: "SF Pro Display",sans-serif;
	    font-size: 60px;
	    font-style: normal;
	    font-weight: 700;
	    line-height: 78px;
	    letter-spacing: -0.01em;
	    text-align: center;
	    color: #015498;
	}
	.title__banggia img {
		width: auto;
	    max-width: 120px;
	    margin: auto;
	    display: block;
	    margin-top: 20px;
	}
	.content__banggia {
		background: #F9FBFF;
		padding: 30px 50px;
		color: #1a1a1a;
	}
	.gioithieu__pricing p {
		margin-bottom: 20px!important;
	}
	.content__banggia h3 {
		font-family: "SF Pro Display",sans-serif;
	    font-size: 32px;
	    font-style: normal;
	    font-weight: 700;
	    line-height: 38px;
	    letter-spacing: 0;
	    text-align: left;
	    margin: 20px 0;
	    color: #015498;
	}
	.content__banggia p {
		font-family: "SF Pro Display",sans-serif;
		font-size: 16px;
		font-style: normal;
		font-weight: 400;
		line-height: 24px;
		letter-spacing: 0em;
		text-align: left;
		color: #1A1A1A;
		margin-bottom: 10px;
	}
	.cachnap__pricing li  {
		margin-left: 0;
	    font-size: 16px;
	    color: #1a1a1a;
	    list-style: none;
	}
	.loiich__pricing li {
		margin-left: 15px;
		list-style: none;
		padding-left: 20px;

	}


.loiich__pricing { counter-reset: li; }

.loiich__pricing li { position: relative; }

.loiich__pricing li:before { 
	    content: counter(li);
    counter-increment: li;
	background: linear-gradient(137.29deg, #8AE0FF 6.86%, #00A0DA 96.29%);
    color: #fff;
   	font-family: "SF Pro Display",sans-serif!important;
    height: 25px;
    text-align: center;
    width: 25px;
    position: absolute;

    left: -15px;
    top: 0px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 50%;
    -webkit-box-shadow: unset;
}

	.cachnap__pricing li::before {
	  content: "\2022";
	  color: #8AE0FF ;
	  display: inline-block; 
	  width: 1em;
}
</style>

<?php
get_footer();
?>