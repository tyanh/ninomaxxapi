﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace giaohangtk
{
    public class RestFile
    {
        //string url = "https://services.giaohangtietkiem.vn/";
        //string token = "db01c677B9875Ec297Df46De8e682551feA41F9a";
        string url = "https://dev.ghtk.vn/";
        string token = "7b9fFb974E60AA6755afFF467826C56c46440C2e";
        public string Gettrangthai(string code)
        {
            string url_ = "services/shipment/v2/"+code;
            var obj = Getdata(url_);
            return obj;
        }
        public string Dangdon(string data)
        {
            string url_ = "services/shipment/order/?ver=1.5";
            var rs= Postdata(data, url_);
            return rs;
        }
        public string Huydon(string code)
        {
            string url_ = "services/shipment/cancel/partner_id:" + code;
            var rs = Postdata("", url_);
            return rs;
        }
        public string Getdata(string urls)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Token", token);
            string uurl = url + urls;
            var obj = webClient.DownloadString(uurl);
            return obj;
        }
        public string Postdata(string data, string urlp)
        {
          
                var urls = url + urlp;
                HttpWebRequest myHttpWebRequest_ = (HttpWebRequest)WebRequest.Create(urls);
                myHttpWebRequest_.Headers.Add("Token", token);
                myHttpWebRequest_.Method = "POST";
                myHttpWebRequest_.Accept = "application/json, text/plain, version=2";
                myHttpWebRequest_.ContentType = "application/json; charset=UTF-8";
                myHttpWebRequest_.ProtocolVersion = System.Net.HttpVersion.Version11;
                if (data != "")
                {
                    using (StreamWriter requestStream = new StreamWriter(myHttpWebRequest_.GetRequestStream()))
                    {
                        requestStream.Write(data);
                    }
                }
                var response = (HttpWebResponse)myHttpWebRequest_.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string str = reader.ReadLine();

                return str;
          
        }
    }
}
