﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using vongxoay.Models;

namespace vongxoay.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {

            var rootFolder = Directory.GetCurrentDirectory();
            var file = "/wwwroot/List_Store.txt";
            List<string> lines = new List<string>();
            foreach (string line in System.IO.File.ReadLines(rootFolder + file))
            {
                lines.Add(line.Replace("\t", " "));
                
            }

            return View(lines);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}