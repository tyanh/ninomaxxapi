﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using WooCommerceNET.WooCommerce.v2;
using System.Linq;
using API_net_core.Models;

namespace newapi.Models
{
    public class ctkm
    {
        List<string> twoitem = new List<string>() { "299000", "349000", "349000" };
        List<string> threeitem = new List<string>() { "279000", "319000", "319000" };
        List<int> qltdc = new List<int>() { 0, 0, 0 };
        List<int> pricess = new List<int>() { 0, 0, 0 };
        List<string> skudc = new List<string>() { "2103050", "2104034", "2104033" };
        sqlcommon sql = new sqlcommon();
        RestFile rest = new RestFile();
        string cartlink = "https://secure-www.ninomaxxconcept.com";
        public Order khuyenmaisku(Order pr)
        {
            string apimtk = cartlink + "/wp-json/Getmtk/v1/";

            foreach (var i in pr.line_items)
            {
                string linkmtk = apimtk + i.product_id;
                var mtk = JsonConvert.DeserializeObject<dynamic>(rest.GetDataWebClientnotkey(linkmtk)).ToString();
                int indexdc = skudc.IndexOf(mtk);
                OrderMeta metatk = new OrderMeta();
                metatk.value = mtk;
                metatk.key = "mtk_";
                i.meta_data.Add(metatk);
                if (indexdc != -1)
                {
                    OrderMeta metadc = new OrderMeta();
                    metadc.value = "0";
                    metadc.key = "discount";
                    i.meta_data.Add(metadc);
                    if (i.quantity == 2)
                        i.subtotal = decimal.Parse(twoitem[indexdc]) * i.quantity;
                    else if (i.quantity >= 3)
                        i.subtotal = decimal.Parse(threeitem[indexdc]) * i.quantity;

                }
            }
            return pr;
        }
        public Order khuyenmai1tang1(Order pr)
        {
            string file = Path.Combine(@"wwwroot\spkhuyenmai.txt");
            FileStream fileStream = new FileStream(file, FileMode.Open);
            List<string> sku = new List<string>();
            using (StreamReader reader = new StreamReader(fileStream))
            {
                while (!reader.EndOfStream)
                {
                    string[] line = reader.ReadLine().Trim().Split('-');
                    sku.Add(line[line.Length - 1]);
                }
            }

            var items = new List<OrderLineItem>();
            foreach (var it in pr.line_items)
            {
                string mtk = it.meta_data.FirstOrDefault(s => s.key == "mtk_").value.ToString();
                if (sku.IndexOf(mtk) != -1)
                {
                    items.Add(it);
                }
            }
            pr.line_items = pr.line_items.Except(items).ToList();
            int bg = 0;
            int next = 0;
            foreach (var it in items)
            {
                int qlt = int.Parse(it.quantity.ToString()) - bg;
                bg = qlt % 2;
                if (bg > 0 && next > 0)
                    qlt = qlt - 1;
                else if (qlt == 1 && next == 0)
                    qlt = qlt + 1;
                it.subtotal = it.price * (qlt / 2);
                next++;
            }
            pr.line_items.AddRange(items);
            return pr;
        }
    }
}
