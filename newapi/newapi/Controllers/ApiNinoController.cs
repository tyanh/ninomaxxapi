﻿using API_net_core.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using newapi.Models;
using Newtonsoft.Json;
using WooCommerceNET.WooCommerce.v2;

namespace newapi.Controllers
{
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ApiNinoController : ControllerBase
    {
        public async Task<string> woopostinvoidprism(Order pr)
        {
            RestFile rest = new RestFile();
            wooCart woo = new wooCart();
            woo.getaddress();
            string coupon = "";
            var items = pr.line_items;

            var lg = rest.LoginVFCApi();
            List<dynamic> _items = new List<dynamic>();
            var tk = JsonConvert.DeserializeObject<dynamic>(lg)["apiToken"].ToString();
            foreach (var it in pr.line_items)
            {
                var _pr = await woo.searchProduct(it.sku);
                it.price = _pr.price;
            }
            ctkm _ctkm = new ctkm();
            pr = _ctkm.khuyenmaisku(pr);
            pr = _ctkm.khuyenmai1tang1(pr);
            foreach (var i in items)
            {

                var disc_ = i.subtotal - (i.price * i.quantity);
                if (disc_ < 0)
                    disc_ = disc_ * (-1);
                dynamic _i = new System.Dynamic.ExpandoObject();
                _i.upc = i.sku.ToString();
                _i.priceRetail = i.price.ToString();
                _i.qty = i.quantity.ToString();
                _i.discount = disc_.ToString();
                _i.itemSubtotal = i.subtotal.ToString();
                _items.Add(_i);

            }
            string tt = "";
            string qh = "";
            string px = "";
            var custTinhSid = woo.city.FirstOrDefault(s => s["ten"].ToString().Contains(pr.shipping.state.ToString()));
            var custHuyenSid = woo.distr.FirstOrDefault(s => s["ten"].ToString().Contains(pr.shipping.city.ToString()));
            var custXaSid = woo.phuong_xa.FirstOrDefault(s => s["ten"].ToString().Contains(pr.shipping.address_2.ToString()));
            if (custTinhSid != null)
                tt = custTinhSid["ma"].ToString();
            if (custHuyenSid != null)
                qh = custHuyenSid["ma"].ToString();
            if (custXaSid != null)
                px = custXaSid["ma"].ToString();
            decimal? ShippingFee = pr.shipping_lines.Sum(s => s.total);
            //var ctm =woo.getcustomer(pr.customer_id.ToString());
            if (pr.coupon_lines.Count > 0)
                coupon = pr.coupon_lines[0].code;
            string shipphone = pr.meta_data.FirstOrDefault(s => s.key.ToString() == "_shipping_phone").value.ToString();
            dynamic datapris =
            new
            {
                wooNo = pr.id.ToString(),
                couponCode = coupon.ToUpper(),
                statusMainSid = "1",
                statusSubSid = "2",
                custPhone = pr.billing.phone.ToString(),
                custFirstName = pr.shipping.first_name.ToString(),
                custLastName = pr.shipping.last_name.ToString(),
                custAddress = pr.shipping.address_1.ToString(),
                custTinhSid = tt,
                custHuyenSid = qh,
                custXaSid = px,
                custTitle = "",
                custDayOfBirth = "",
                custEmail = pr.billing.email.ToString(),
                custNote = pr.customer_note.ToString(),
                vfcShippingFee = ShippingFee.ToString(),
                extraFee = "0",
                fromSource = "WOO",
                documentItems = _items,
                paymentTypeSid = common.idpayment(pr.payment_method.ToString())
            };
            try
            {
                var ip = rest.PostDataVFCApi("saleonlinedocument", datapris, tk);
                return ip;
            }
            catch
            {
                string oo = JsonConvert.SerializeObject(datapris);
                return "-1";
            }
        }
        [HttpGet("apitest")]
        public string apitest()
        {
            return "232";
        }
        [HttpGet("Upwoopostinvoidprism")]
        public async Task<string> Upwoopostinvoidprism(string idorder)
        {
            common cmd = new common();
            wooCart woo = new wooCart();
            sqlmissdata miss = new sqlmissdata();
            /*var c_ = miss.checkduplicate(idorder);
            if (c_ != "0")
                return "-1";*/
            var order = await woo.getorder(idorder);
            if (order.status == "pending")
                return "-2";
            if (order.status == "cancelled")
                return "-3";
            var rs = await woopostinvoidprism(order);
            string o = "update sussess";
            if (rs == "-1")
                o = "loi 2";
            string now = DateTime.Now.ToString();
            string u = "update missdata set status='" + o + "',lastupdate='" + now + "',sid='" + rs + "' where idwoo=" + idorder + "";
            miss.runSQL(u);
            return rs;
        }
    }
}
