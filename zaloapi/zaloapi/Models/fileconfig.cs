﻿namespace zaloapi.Models
{
    public class fileconfig
    {
        public string tokentzalo = "";
        public string apizalo = "";
        public string imgzalo = "";
        public fileconfig()
        {
            var config = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json").Build();
      
            tokentzalo = config["AppSettings:tokentzalo"];
            apizalo = config["AppSettings:apizalo"];
            imgzalo = config["AppSettings:imgzalo"];

        }
    }
}
