﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
  
    [Route("api/[controller]")]
    [ApiController]
    public class ApiAppController : ControllerBase
    {
        [HttpGet("testnew")]
        public string testnew()
        {
            return "fdf";
        }
    }
}
