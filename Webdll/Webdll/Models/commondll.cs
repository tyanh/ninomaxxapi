﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;


namespace Webdll.Models
{
    public class commondll
    {
        public string path = "";
        bool checkdirectory(string f)
        {
            String paths = path+"//"+f;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return true;
        }
        public Image watermarkBarcodeonImage(string txt, Bitmap bm)
        {
            string watermarkText = txt;
            Image myimg = Code128Rendering.MakeBarcodeImage(watermarkText, 1, true);
            Image file = (Image)bm;
            using (Graphics grp = Graphics.FromImage(file))
            {
                Point position = new Point(0, 0);
                if (txt != "")
                    grp.DrawImage(myimg, position);
                Brush brush = new SolidBrush(Color.Gray);
                Font font = new Font("UTM Dax", 16, GraphicsUnit.Point);

                //Point positiontext = new Point(10, 50);
                //grp.DrawString(watermarkText, font, brush, positiontext);
                return file;

            }

        }

        public Image GetImgBitmap(string str,bool isbarcode,string txt)
        {
            string convert = str.Replace("data:image/png;base64,", String.Empty);
            convert = convert.Replace("data:image/jpeg;base64,", String.Empty);
            convert = convert.Replace(" ", "+");
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
            {
                using (Bitmap bm2 = new Bitmap(ms))
                {

                    //float scale = Math.Min(width / bm2.Width, height / bm2.Height);
                    //var scaleWidth = (int)(bm2.Width * scale);
                    //var scaleHeight = (int)(bm2.Height * scale);
                    Bitmap bitmap = new Bitmap(bm2, new Size(bm2.Width, bm2.Height));
                    if (isbarcode)
                    {
                       return watermarkBarcodeonImage(txt, bitmap);
                    }
                    return bitmap;
                }
            }

            //return name + "." + ext;
        }

        public async Task<bool> uploadImg(string stringImg, string imgname, string savepath, string listfolder,bool isbarcode, string strbarcode)
        {
            if (listfolder != "")
            {
                path = savepath;
                string[] lfolder = listfolder.Split(',');
                foreach (var l in lfolder)
                {
                    checkdirectory(l);
                }
            }
            var bipmap = GetImgBitmap(stringImg, isbarcode, strbarcode);
            string[] imgext = imgname.Split('.');
            string ext = imgext[imgext.Length - 1];
            var stream = new System.IO.MemoryStream();
            if (ext.ToLower() == "png")
                bipmap.Save(stream, ImageFormat.Png);
            if (ext.ToLower() == "gif")
                bipmap.Save(stream, ImageFormat.Gif);
            else
                bipmap.Save(stream, ImageFormat.Jpeg);
            stream.Position = 0;
            await stream.CopyToAsync(new FileStream(savepath + @"\" + imgname, FileMode.Create));
            stream.Close();
            stream.Dispose();
            return true;

        }
        public bool makfolderFtp(string user,string pass)
        {
            String paths = "ftp://localhost/imgWeb/san_pham/";
            WebRequest request = WebRequest.Create(paths);
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.Credentials = new NetworkCredential(user, pass);
            using (var resp = (FtpWebResponse)request.GetResponse())
            {
                Console.WriteLine(resp.StatusCode);
            }
            return true;
        }
        public bool uploadftp(string username,string pass, string stringImg, string imgname, string savepath, string listfolder, bool isbarcode, string strbarcode)
        {
            if (listfolder != "")
            {
                path = savepath;
                string[] lfolder = listfolder.Split(',');
                foreach (var l in lfolder)
                {
                    //makfolderFtp(l, username,pass);
                }
            }
            var bipmap = GetImgBitmap(stringImg, isbarcode, strbarcode);
            string[] imgext = imgname.Split('.');
            string ext = imgext[imgext.Length - 1];
            var stream = new System.IO.MemoryStream();
            if (ext.ToLower() == "png")
                bipmap.Save(stream, ImageFormat.Png);
            if (ext.ToLower() == "gif")
                bipmap.Save(stream, ImageFormat.Gif);
            else
                bipmap.Save(stream, ImageFormat.Jpeg);
            WebRequest request = WebRequest.Create(path + imgname);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(username, pass);
            using (Stream ftpStream = request.GetRequestStream())
            {
                stream.Position = 0;
                stream.CopyTo(ftpStream);
                stream.Close();
                stream.Dispose();

            }
           
            return true;

        }

      
        public string downloadfile(string frompath,string imgname,string savepath,string listfolder, bool isbarcode, string strbarcode)
        {
            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(frompath);
                Bitmap bitmap; bitmap = new Bitmap(stream);
                if (listfolder != "")
                {
                    path = savepath;
                    string[] lfolder = listfolder.Split(',');
                    foreach (var l in lfolder)
                    {
                        checkdirectory(l);
                    }
                }
                if (bitmap != null)
                {
                    if (isbarcode)
                    {
                       var tmap = watermarkBarcodeonImage(strbarcode, bitmap);

                        tmap.Save(stream, ImageFormat.Jpeg);
                        stream.Position = 0;
                        stream.CopyTo(new FileStream(savepath + @"\" + imgname, FileMode.Create));
                       

                    }
                    else
                    bitmap.Save(savepath + imgname, ImageFormat.Jpeg);
                }

                stream.Flush();
                stream.Close();
                client.Dispose();
                return "0";
            }
            catch
            {
                return "-1";
            }
        }
    }
}