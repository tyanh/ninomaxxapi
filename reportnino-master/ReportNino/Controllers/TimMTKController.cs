﻿using ReportNino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportNino.Controllers
{
    public class TimMTKController : Controller
    {
        common cmd = new common();
        public ActionResult Index()
        {
            AccoutModel acount = cmd.getlogin();
            string now = DateTime.Now.ToString("dd/MM/yyyy");
            TonUPCModel _model = new TonUPCModel();
            try
            {
                _model.store = acount.store_no.ToString();
            }
            catch { }
            return View(_model);
        }
        [HttpPost]
        public ActionResult Index(string upc)
        {

            TonUPCModel ton_upc = new TonUPCModel();
            var data = cmd.LOOKUP_OH_QTY_DESC1_SOL_V2(upc);
            //var data = cmd.Ton_Kho_UPC(upc, "238", position);
            if (data != null)
            {

                ton_upc.data = data.Rows.Cast<System.Data.DataRow>().ToList();
                //ton_upc.data = ton_upc.data.Where(s => s["STORE_CODE"].ToString() == "NT8").ToList();
            }
            return View(ton_upc);
        }
    }
}