﻿using ReportNino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportNino.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        common cmd = new common();
        public ActionResult Index()
        {
            AccoutModel acount = cmd.getlogin();
            ReportinventoryModel _model = new ReportinventoryModel();
            try
            {
                string now = cmd.getnow();
                _model.from = now;
                _model.to = now;
                _model.total = 0;
                _model.store_no = acount.store_no.ToString();
                _model.store_name = acount.store_name;
            }
            catch { }
            return View(_model);
       
        }
        [HttpPost]
        public ActionResult Index(ReportinventoryModel model)
        {
            model.Search(model.from, model.to, model.store_no);
            return View(model);
        }
    }
}