﻿using Newtonsoft.Json;
using ReportNino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportNino.Controllers
{
    public class LoginController : Controller
    {
        common cmd = new common();
        // GET: Login
        public ActionResult Index()
        {
            cmd.logOut();
            return View();
        }
        [HttpPost]
        public ActionResult Index(string username, string password)
        {
       
            var acc = cmd.CreateAccoutlogin(username, password);
            if (acc.store_name == null)
                return View();
            else
            {
                string str_ = JsonConvert.SerializeObject(acc);
                cmd.login(str_);
                return RedirectToAction("Index", "Default");
            }

        }
    }
}