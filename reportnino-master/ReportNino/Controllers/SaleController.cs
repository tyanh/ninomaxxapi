﻿using ReportNino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportNino.Controllers
{
    public class SaleController : Controller
    {
        // GET: Sale
        common cmd = new common();
        public ActionResult Index()
        {
            SaleModel s_model = new SaleModel();
            try
            {
                AccoutModel acount = cmd.getlogin();
                string now = DateTime.Now.ToString("dd/MM/yyyy");
                s_model.from = now;
                s_model.to = now;
                s_model.store_no = acount.store_no.ToString();
                s_model.store_name = acount.store_name.ToString();
            }
            catch { }
            return View(s_model);
         
        }
        [HttpPost]
        public ActionResult Index(SaleModel model)
        {
           model.Search_(model.from,model.to,model.store_no);
           return View(model);
        }
    }
}