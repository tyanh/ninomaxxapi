﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ReportNino.Models
{
    public class ReportinventoryModel
    {
        public string from { get; set; }
        public string to { get; set; }
        public int total { get; set; }
        public string store_no { get; set; }
        public string store_name { get; set; }
        public List<DataRow> data { get; set; }
        public int sum_dk { get; set; }
        public int sum_nhap { get; set; }
        public int sum_xuat { get; set; }
        public int sum_ban { get; set; }
        public int sum_dc { get; set; }
        public int sum_ton { get; set; }
        public int sum_giatri { get; set; }
        public void Search(string from, string to, string store)
        {
            common cmd = new common();
            var datas = cmd.GetReport(from, to, store);
            data = datas.Rows.Cast<System.Data.DataRow>().ToList();
            if (data != null)
            {
                sum_dk = data.Sum(s => int.Parse(s["DK"].ToString()));
                sum_nhap = data.Sum(s => int.Parse(s["PS_PN"].ToString()));
                sum_xuat = data.Sum(s => int.Parse(s["PS_PX"].ToString()));
                sum_ban = data.Sum(s => int.Parse(s["PS_HD"].ToString()));
                sum_dc = data.Sum(s => int.Parse(s["PS_DC"].ToString()));
                sum_ton = data.Sum(s => int.Parse(s["TON"].ToString()));
                sum_giatri = 0;
            }
        }
    }
}