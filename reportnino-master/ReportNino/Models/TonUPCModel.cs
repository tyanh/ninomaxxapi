﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ReportNino.Models
{
    public class TonUPCModel
    {
        public string upc { get; set; }
        public string position { get; set; }
        public string store { get; set; }
        public List<DataRow> data { get; set; }
    }
}