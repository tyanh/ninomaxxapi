﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ReportNino.Models
{
    public class common
    {
        string _oraString = "Data Source=(DESCRIPTION="
                      + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=203.205.21.52)(PORT=1521)))"
                      + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=RPROODS)));"
                      + "User Id=reportuser;Password=report;";
        public void login(string loginNino)
        {
            HttpContext.Current.Session["loginNino"] = loginNino;
        }
      
        public string getnow()
        {
            string now = DateTime.Now.ToString("dd/MM/yyyy");
            return now;
        }
        public AccoutModel getlogin()
        {
            if(HttpContext.Current.Session["loginNino"]==null)
                return null;
            string acstr = HttpContext.Current.Session["loginNino"].ToString();
            AccoutModel acount = new AccoutModel();
            acount = JsonConvert.DeserializeObject<AccoutModel>(acstr);
            return acount;
            

        }
        public bool logOut()
        {
            HttpContext.Current.Session.Remove("loginNino");
            return true;
        }
        public OracleConnection GetConnection()
        {
            var _conn = new OracleConnection(_oraString);

            try
            {
                _conn.Open();
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }

            return _conn;
        }
        public DataTable LOOKUP_OH_QTY_UPC_SOL_V2(string upc)
        {
           
            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_INVENTORY.LOOKUP_OH_QTY_UPC_SOL_V2";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter v_Upc= command.Parameters.Add("v_Upc", OracleDbType.Varchar2);
            v_Upc.Value = upc;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            command.Connection.Close();
            return dt;
           
        }
        public DataTable LOOKUP_OH_QTY_DESC1_SOL_V2(string desc)
        {

            OracleCommand command = new OracleCommand();

            command.Connection = GetConnection();

            command.CommandText = "REPORTUSER.VFC_PRISM_INVENTORY.LOOKUP_OH_QTY_DESC1_SOL_V2";
            command.CommandType = CommandType.StoredProcedure;
            OracleParameter v_Descr1 = command.Parameters.Add("v_Descr1", OracleDbType.Varchar2);
            v_Descr1.Value = desc;
            OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
            my_cursor.Direction = ParameterDirection.Output;
            OracleDataAdapter da = new OracleDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            command.Connection.Close();
            return dt;

        }
        public DataTable Ton_Kho_MTK(string upc,string where)
        {
            //try
            //{
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.VFC_PRISM_INVENTORY.LOOKUP_ONHAND_QTY_DESCR1_V2";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_Descr1 = command.Parameters.Add("v_Descr1", OracleDbType.Varchar2);
                v_Descr1.Value = upc;
                OracleParameter v_Where = command.Parameters.Add("v_Where", OracleDbType.Varchar2);
                v_Where.Value = where;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            //}
            //catch (Exception ex)
            //{
            //    return null;
            //}
        }
        public DataTable Ton_Kho_UPC(string upc, string storeNo, string where)
        {
            //try
            //{
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.NN_Invoice.spud_GET_TON_KHO_UPC";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_UPC = command.Parameters.Add("v_UPC", OracleDbType.Varchar2);
                v_UPC.Value = upc;
                OracleParameter v_StoreNo = command.Parameters.Add("v_StoreNo", OracleDbType.Varchar2);
                v_StoreNo.Value = storeNo;
                OracleParameter v_Where = command.Parameters.Add("v_Where", OracleDbType.Varchar2);
                v_Where.Value = where;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            //}
            //catch (Exception ex)
            //{
            //    return null;
            //}
        }
        public DataTable GetSales(string _fromDate, string _toDate, string _storeNo)
        {
            try
            {
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.NN_Invoice.spud_GET_BC_BanHang";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_FromDate = command.Parameters.Add("v_FromDate", OracleDbType.Varchar2);
                v_FromDate.Value = _fromDate;
                OracleParameter v_ToDate = command.Parameters.Add("v_ToDate", OracleDbType.Varchar2);
                v_ToDate.Value = _toDate;
                OracleParameter v_StoreCode = command.Parameters.Add("v_StoreNo", OracleDbType.Int32);
                v_StoreCode.Value = _storeNo;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public DataTable GetReport(string _fromDate, string _toDate, string _storeNo)
        {
            try
            {
                OracleCommand command = new OracleCommand();

                command.Connection = GetConnection();

                command.CommandText = "REPORTUSER.NN_Invoice.spud_Run_NXT";
                command.CommandType = CommandType.StoredProcedure;
                OracleParameter v_FromDate = command.Parameters.Add("v_FromDate", OracleDbType.Varchar2);
                v_FromDate.Value = _fromDate;
                OracleParameter v_ToDate = command.Parameters.Add("v_ToDate", OracleDbType.Varchar2);
                v_ToDate.Value = _toDate;
                OracleParameter v_StoreCode = command.Parameters.Add("v_StoreNo", OracleDbType.Int32);
                v_StoreCode.Value = _storeNo;
                OracleParameter my_cursor = command.Parameters.Add("my_cursor", OracleDbType.RefCursor);
                my_cursor.Direction = ParameterDirection.Output;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                da.Fill(dt);
                command.Connection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public AccoutModel CreateAccoutlogin(string usern, string passw)
        {
            AccoutModel acount = new AccoutModel();
            List<Dictionary<string, object>> obj = new List<Dictionary<string, object>>();
            string[] pass = new string[] { "ZB094F", "D2FLWA", "CJJWJK", "WJEU2Q", "8U5VDZ", "HNKTI3", "0H71C7", "DSL7HS", "AFS3R2", "UX0BGL", "CL9ITA", "MBVC8C", "H1PVJR", "RE0FIF", "X2DZW1", "X5SFD1", "MA079F", "9V8STC" };
            string[] user = new string[] { "NT6", "LM2", "SH1", "VVC", "NA1", "PVD", "LBB", "VHM", "ATP", "ABT", "VTD", "LGV", "HG1", "NT8", "HBT", "C83", "QT2", "CRM" };
            string[] store = new string[] { "31", "18", "131", "313", "176", "349", "345", "333", "323", "322", "319", "318", "300", "238", "15", "9", "161", "149" };

            int lg = -1;
            for (int i = 0; i < user.Length; i++)
            {
                if (usern == user[i])
                {
                    if (pass[i] == passw)
                    {
                        lg = i;
                        break;
                    }
                }
            }
            if (lg > -1)
            {

                acount.store_no = int.Parse(store[lg]);
                acount.store_name = user[lg];
            }
            return acount;
        }
    }
}