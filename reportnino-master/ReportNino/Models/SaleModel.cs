﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ReportNino.Models
{
    public class SaleModel
    {
        public string from { get; set; }
        public string to { get; set; }
        public List<List<DataRow>> datasale { get; set; }
        public int sum_qlt { get; set; }
        public int sum_money { get; set; }
        public int sum_count_dow { get; set; }
        public string store_no { get; set; }
        public string store_name { get; set; }

        public void Search_(string from, string to, string store)
        {
            common cmd = new common();
            var data = cmd.GetSales(from, to, store);
            if (data != null)
            {
                datasale = new List<List<System.Data.DataRow>>();
                var boy = data.Rows.Cast<System.Data.DataRow>().Where(s => s["GENDER"].ToString().ToLower() == "boy").ToList();
                var girl = data.Rows.Cast<System.Data.DataRow>().Where(s => s["GENDER"].ToString().ToLower() == "girl").ToList();
                var men = data.Rows.Cast<System.Data.DataRow>().Where(s => s["GENDER"].ToString().ToLower() == "men").ToList();
                var women = data.Rows.Cast<System.Data.DataRow>().Where(s => s["GENDER"].ToString().ToLower() == "wom").ToList();
                if (boy.Count > 0)
                    datasale.Add(boy);
                if (girl.Count > 0)
                    datasale.Add(girl);
                if (men.Count > 0)
                    datasale.Add(men);
                if (women.Count > 0)
                    datasale.Add(women);
            }
            if (data != null)
            {
                try
                {
                    sum_money = data.Rows.Cast<System.Data.DataRow>().Sum(s => int.Parse(s["GIA_RPRO"].ToString()));
                }
                catch { }
                sum_qlt = data.Rows.Cast<System.Data.DataRow>().Sum(s => int.Parse(s["SLB"].ToString()));
                sum_count_dow = data.Rows.Cast<System.Data.DataRow>().Sum(s => int.Parse(s["GIAM_GIA"].ToString()));
            }
            
        }
    }
}